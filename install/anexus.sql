-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Oct 03, 2014 at 07:47 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `anexus`
-- 
-- --------------------------------------------------------


-- 
-- Table structure for table `customer`
-- 

CREATE TABLE `customer` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `status` int(5) NOT NULL default '1',
  `company_name` varchar(255) NOT NULL,
  `custom_code` varchar(50) NOT NULL,
  `account_no` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `postal` varchar(15) NOT NULL,
  `country` varchar(50) NOT NULL,
  `city` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact_person` varchar(20) NOT NULL,
  `contact_info` varchar(20) NOT NULL,
  `primary_attention_to` varchar(50) NOT NULL,
  `primary_contact_info` varchar(50) NOT NULL,
  `primary_contact_email` varchar(255) NOT NULL,
  `secondary_attention_to` varchar(50) NOT NULL,
  `secondary_contact_info` varchar(50) NOT NULL,
  `secondary_contact_email` varchar(255) NOT NULL,
  `delivery_postal` varchar(30) NOT NULL,
  `delivery_address` text NOT NULL,
  `use_default_price` tinyint(1) NOT NULL,
  `price_list` text NOT NULL,
  `currency` varchar(3) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `customer`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `employee`
-- 

CREATE TABLE `employee` (
  `id` int(15) NOT NULL auto_increment,
  `user_id` int(15) NOT NULL,
  `employee_no` varchar(30) NOT NULL,
  `role_id` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `department_id` int(8) NOT NULL,
  `status` int(5) NOT NULL,
  `bankname` varchar(30) NOT NULL,
  `bank_code` int(4) NOT NULL,
  `bank_branch` int(3) NOT NULL,
  `account_no` varchar(50) NOT NULL,
  `full_name` varchar(30) NOT NULL,
  `email` varchar(255) default NULL,
  `ic_no` varchar(50) NOT NULL,
  `date_of_birth` date NOT NULL,
  `home_address` varchar(255) NOT NULL,
  `contact_info` varchar(30) NOT NULL,
  `passport_no` varchar(255) NOT NULL,
  `expired_date` date NOT NULL,
  `emergency_contact_name` varchar(30) NOT NULL,
  `emergency_contact_relationship` varchar(30) NOT NULL,
  `emergency_contact_no` varchar(30) NOT NULL,
  `assign_customer` varchar(255) default NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `employee`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `product`
-- 

CREATE TABLE `product` (
  `id` int(16) NOT NULL auto_increment,
  `product_no` varchar(255) NOT NULL,
  `part_no` varchar(50) NOT NULL,
  `model_no` varchar(50) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_category` int(8) NOT NULL,
  `pic_path` varchar(255) NOT NULL,
  `serial` varchar(255) NOT NULL,
  `supplier` varchar(255) NOT NULL,
  `status` int(5) NOT NULL COMMENT '1 for active; 0 for suspended',
  `description` text NOT NULL,
  `uom` varchar(50) NOT NULL,
  `cost_price` decimal(10,2) NOT NULL,
  `selling_price` decimal(10,2) NOT NULL,
  `promo_price` decimal(10,2) default NULL,
  `currency` varchar(3) NOT NULL,
  `length` varchar(10) NOT NULL,
  `breadth` varchar(10) NOT NULL,
  `height` varchar(10) NOT NULL,
  `weight` varchar(10) NOT NULL,
  `est_cost` varchar(10) NOT NULL,
  `current_stock_qty` int(15) NOT NULL,
  `qty_reminder` int(15) NOT NULL,
  `unit_price` varchar(10) NOT NULL,
  `additional_fee` varchar(10) NOT NULL,
  `type` int(1) NOT NULL COMMENT '1 for product; 2 for service',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `product`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `role`
-- 

CREATE TABLE `role` (
  `id` int(8) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `parentId` int(8) NOT NULL,
  `permission_id` int(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `role`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `settings`
-- 

CREATE TABLE `settings` (
  `id` int(8) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `value` text NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `settings`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `user`
-- 

CREATE TABLE `user` (
  `id` int(11) NOT NULL auto_increment,
  `role_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `passwd` varchar(255) NOT NULL,
  `employee_id` int(15) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `user`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `job`
-- 

CREATE TABLE `job` (
  `id` int(15) NOT NULL auto_increment,
  `parent_id` int(15) NOT NULL,
  `order_no` varchar(30) NOT NULL,
  `serial` varchar(15) NOT NULL COMMENT 'job id',
  `user_id` int(8) NOT NULL COMMENT 'been assigned to',
  `customer_id` int(15) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `awaiting_role_id` varchar(50) NOT NULL,
  `status_id` int(5) NOT NULL,
  `requestor_id` int(15) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `job_name` varchar(50) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  `display` tinyint(1) NOT NULL default '1',
  `is_completed` tinyint(1) NOT NULL,
  `status_text` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `type_id` varchar(255) NOT NULL COMMENT '作為edit使用的id',
  `type_path` varchar(255) NOT NULL COMMENT '去到edit的path',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `job`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `workflow`
-- 

CREATE TABLE `workflow` (
  `id` int(15) NOT NULL auto_increment,
  `title` varchar(50) NOT NULL,
  `table_name` varchar(255) NOT NULL,
  `status_json` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `workflow`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `workflow_order`
-- 

CREATE TABLE `workflow_order` (
  `id` int(15) NOT NULL auto_increment,
  `status_id` int(5) NOT NULL,
  `action` varchar(50) collate utf8_unicode_ci NOT NULL,
  `requestor_edit` tinyint(1) NOT NULL,
  `role_id` varchar(50) collate utf8_unicode_ci NOT NULL,
  `workflow_id` int(8) NOT NULL,
  `need_det` tinyint(1) NOT NULL,
  `formula` text collate utf8_unicode_ci NOT NULL,
  `target_colum` text collate utf8_unicode_ci NOT NULL,
  `action_before_complete` varchar(255) collate utf8_unicode_ci NOT NULL,
  `next_status_id` int(5) NOT NULL,
  `next_status_text` varchar(255) collate utf8_unicode_ci NOT NULL,
  `ad_hoc` tinyint(1) NOT NULL,
  `who_email` varchar(50) collate utf8_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `workflow_order`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `permission`
-- 

CREATE TABLE `permission` (
  `id` int(5) NOT NULL auto_increment,
  `quotation_control` varchar(50) NOT NULL,
  `tender_control` varchar(50) NOT NULL,
  `purchase_control` varchar(50) NOT NULL,
  `service_control` varchar(50) NOT NULL,
  `commercial_invoice_control` varchar(50) NOT NULL,
  `delivery_order_control` varchar(50) NOT NULL,
  `service_order_control` varchar(50) NOT NULL,
  `inventory_control` varchar(50) NOT NULL,
  `rma_control` varchar(50) NOT NULL,
  `credit_note_control` varchar(50) NOT NULL,
  `manage_employee_control` varchar(50) NOT NULL,
  `manage_customer_control` varchar(50) NOT NULL,
  `manage_suppliers_control` varchar(50) NOT NULL,
  `manage_inventory_control` varchar(50) NOT NULL,
  `manage_shipping_control` varchar(50) NOT NULL,
  `settings_control` varchar(50) NOT NULL,
  `invoice_control` varchar(50) NOT NULL,
  `all_job_control` varchar(50) NOT NULL,
  `all_customer_control` varchar(50) NOT NULL,
  `admin_service_control` varchar(50) NOT NULL,
  `department_control` varchar(50) NOT NULL,
  `trip_control` varchar(50) NOT NULL,
  `reporting_control` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `ad_hoc` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `permission`
-- 


-- --------------------------------------------------------------------------------------------------------------

-- 
-- Table structure for table `audit_log`
-- 

CREATE TABLE `audit_log` (
  `id` int(15) NOT NULL auto_increment,
  `log_no` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user_trigger` int(15) NOT NULL,
  `table_affect` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `audit_log`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `order`
-- 

CREATE TABLE `order` (
  `id` int(15) NOT NULL auto_increment,
  `order_no` varchar(30) NOT NULL,
  `order_date` date NOT NULL,
  `customer_id` int(15) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `remark` text NOT NULL,
  `confirm_file` varchar(255) NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `gst_amount` decimal(10,2) NOT NULL,
  `job_id` int(15) NOT NULL,
  `latest_job_id` int(15) NOT NULL,
  `status` int(5) NOT NULL,
  `new_status` tinyint(1) NOT NULL default '0',
  `requestor_id` int(15) NOT NULL,
  `create_user_id` int(8) NOT NULL,
  `lastupdate_user_id` int(8) NOT NULL,
  `awaiting_table` varchar(255) NOT NULL,
  `no_approve_user` int(8) NOT NULL,
  `no_approve_reason` text NOT NULL,
  `customer_create` tinyint(1) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `postal` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- 
-- Dumping data for table `order`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `order_item`
-- 

CREATE TABLE `order_item` (
  `id` int(8) NOT NULL auto_increment,
  `order_id` int(8) NOT NULL,
  `product_id` int(8) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `quantity` int(5) NOT NULL,
  `quantity_order` int(5) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- 
-- Dumping data for table `order_item`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `delivery_order`
-- 

CREATE TABLE `delivery_order` (
  `id` int(15) NOT NULL auto_increment,
  `order_id` int(15) NOT NULL,
  `packing_id` int(15) NOT NULL,
  `order_no` varchar(30) NOT NULL,
  `do_no` varchar(30) NOT NULL,
  `do_date` date NOT NULL,
  `customer_id` int(15) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `instructions` text NOT NULL,
  `confirm_file` varchar(255) NOT NULL,
  `job_id` int(15) NOT NULL,
  `latest_job_id` int(15) NOT NULL,
  `status` int(5) NOT NULL,
  `staff_name` varchar(255) NOT NULL,
  `staff_role` varchar(255) NOT NULL,
  `requestor_id` int(15) NOT NULL,
  `create_user_id` int(8) NOT NULL,
  `lastupdate_user_id` int(8) NOT NULL,
  `awaiting_table` varchar(255) NOT NULL,
  `no_approve_user` int(8) NOT NULL,
  `no_approve_reason` text NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- 
-- Dumping data for table `delivery_order`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `delivery_order_item`
-- 

CREATE TABLE `delivery_order_item` (
  `id` int(8) NOT NULL auto_increment,
  `do_id` int(15) NOT NULL,
  `product_id` int(8) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `quantity_del` int(5) NOT NULL,
  `quantity_bal` int(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- 
-- Dumping data for table `delivery_order_item`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `packing_list`
-- 

CREATE TABLE `packing_list` (
  `id` int(15) NOT NULL auto_increment,
  `order_id` int(15) NOT NULL,
  `order_no` varchar(30) NOT NULL,
  `packing_no` varchar(30) NOT NULL,
  `packing_date` date NOT NULL,
  `customer_id` int(15) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `remark` text NOT NULL,
  `confirm_file` varchar(255) NOT NULL,
  `job_id` int(15) NOT NULL,
  `latest_job_id` int(15) NOT NULL,
  `status` int(5) NOT NULL,
  `staff_name` varchar(255) NOT NULL,
  `staff_role` varchar(255) NOT NULL,
  `requestor_id` int(15) NOT NULL,
  `create_user_id` int(8) NOT NULL,
  `lastupdate_user_id` int(8) NOT NULL,
  `awaiting_table` varchar(255) NOT NULL,
  `no_approve_user` int(8) NOT NULL,
  `no_approve_reason` text NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- 
-- Dumping data for table `packing_list`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `packing_list_item`
-- 

CREATE TABLE `packing_list_item` (
  `id` int(8) NOT NULL auto_increment,
  `order_id` int(15) NOT NULL,
  `packing_list_id` int(8) NOT NULL,
  `product_id` int(8) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `quantity_order` int(5) NOT NULL,
  `quantity_stock` int(5) NOT NULL,
  `quantity_pack` int(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- 
-- Dumping data for table `packing_list_item`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `product_category`
-- 

CREATE TABLE `product_category` (
  `id` int(8) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- 
-- Dumping data for table `product_category`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `product_log`
-- 

CREATE TABLE `product_log` (
  `id` int(15) NOT NULL auto_increment,
  `product_id` int(8) NOT NULL,
  `qty` int(8) NOT NULL,
  `reason` text NOT NULL,
  `employee_no` varchar(30) NOT NULL,
  `employee_name` varchar(30) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- 
-- Dumping data for table `product_log`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `promotion`
-- 

CREATE TABLE `promotion` (
  `id` int(8) NOT NULL,
  `json_content` text NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- 
-- Dumping data for table `promotion`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `invoice`
-- 

CREATE TABLE `invoice` (
  `id` int(15) NOT NULL auto_increment,
  `order_id` int(15) NOT NULL,
  `do_id` int(15) NOT NULL,
  `order_no` varchar(30) NOT NULL,
  `invoice_no` varchar(30) NOT NULL,
  `invoice_date` date NOT NULL,
  `customer_id` int(15) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `notes` text NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `gst_amount` decimal(10,2) NOT NULL,
  `nett_amount` decimal(10,2) NOT NULL,
  `confirm_file` varchar(255) NOT NULL,
  `job_id` int(15) NOT NULL,
  `latest_job_id` int(15) NOT NULL,
  `status` int(5) NOT NULL,
  `staff_name` varchar(255) NOT NULL,
  `staff_role` int(5) NOT NULL,
  `requestor_id` int(15) NOT NULL,
  `create_user_id` int(8) NOT NULL,
  `lastupdate_user_id` int(8) NOT NULL,
  `awaiting_table` varchar(255) NOT NULL,
  `no_approve_user` int(8) NOT NULL,
  `no_approve_reason` text NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- 
-- Dumping data for table `invoice`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `invoice_item`
-- 

CREATE TABLE `invoice_item` (
  `id` int(8) NOT NULL auto_increment,
  `invoice_id` int(15) NOT NULL,
  `product_id` int(8) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `quantity` int(5) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- 
-- Dumping data for table `invoice_item`
-- 



