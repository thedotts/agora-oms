CREATE TABLE `customer` (
`id` int(11) NOT NULL auto_increment,
`user_id` int(11) NOT NULL,
`status` int(5) NOT NULL default '1',
`company_name` varchar(255) NOT NULL,
`custom_code` varchar(50) NOT NULL,
`account_no` varchar(255) NOT NULL,
`address` varchar(255) NOT NULL,
`postal` varchar(15) NOT NULL,
`country` varchar(50) NOT NULL,
`city` varchar(255) NOT NULL,
`email` varchar(50) NOT NULL,
`contact_person` varchar(20) NOT NULL,
`contact_info` varchar(20) NOT NULL,
`primary_attention_to` varchar(50) NOT NULL,
`primary_contact_info` varchar(50) NOT NULL,
`primary_contact_email` varchar(255) NOT NULL,
`secondary_attention_to` varchar(50) NOT NULL,
`secondary_contact_info` varchar(50) NOT NULL,
`secondary_contact_email` varchar(255) NOT NULL,
`delivery_postal` varchar(30) NOT NULL,
`delivery_address` text NOT NULL,
`use_default_price` tinyint(1) NOT NULL,
`price_list` text NOT NULL,
`currency` varchar(3) NOT NULL,
`create_user_id` int(11) NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
PRIMARY KEY  (`id`),
FOREIGN KEY (`user_id`) REFERENCES `user`(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `employee` (
`id` int(15) NOT NULL auto_increment,
`user_id` int(15) NOT NULL,
`employee_no` varchar(30) NOT NULL,
`role_id` varchar(255) NOT NULL,
`customer_id` int(11) NOT NULL,
`department_id` int(8) NOT NULL,
`status` int(5) NOT NULL,
`bankname` varchar(30) NOT NULL,
`bank_code` int(4) NOT NULL,
`bank_branch` int(3) NOT NULL,
`account_no` varchar(50) NOT NULL,
`full_name` varchar(30) NOT NULL,
`email` varchar(255) default NULL,
`ic_no` varchar(50) NOT NULL,
`date_of_birth` date NOT NULL,
`home_address` varchar(255) NOT NULL,
`contact_info` varchar(30) NOT NULL,
`passport_no` varchar(255) NOT NULL,
`expired_date` date NOT NULL,
`emergency_contact_name` varchar(30) NOT NULL,
`emergency_contact_relationship` varchar(30) NOT NULL,
`emergency_contact_no` varchar(30) NOT NULL,
`assign_customer` varchar(255) default NULL,
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
PRIMARY KEY  (`id`),
FOREIGN KEY (`user_id`) REFERENCES `user`(`id`),
FOREIGN KEY (`role_id`) REFERENCES `role`(`id`),
FOREIGN KEY (`customer_id`) REFERENCES `customer`(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `product` (
`id` int(16) NOT NULL auto_increment,
`product_no` varchar(255) NOT NULL,
`part_no` varchar(50) NOT NULL,
`model_no` varchar(50) NOT NULL,
`model_name` varchar(255) NOT NULL,
`product_name` varchar(255) NOT NULL,
`product_category` int(8) NOT NULL,
`pic_path` varchar(255) NOT NULL,
`serial` varchar(255) NOT NULL,
`supplier` varchar(255) NOT NULL,
`status` int(5) NOT NULL COMMENT '1 for active; 0 for suspended',
`description` text NOT NULL,
`uom` varchar(50) NOT NULL,
`cost_price` decimal(10,2) NOT NULL,
`selling_price` decimal(10,2) NOT NULL,
`promo_price` decimal(10,2) default NULL,
`currency` varchar(3) NOT NULL,
`length` varchar(10) NOT NULL,
`breadth` varchar(10) NOT NULL,
`height` varchar(10) NOT NULL,
`weight` varchar(10) NOT NULL,
`est_cost` varchar(10) NOT NULL,
`current_stock_qty` int(15) NOT NULL,
`qty_reminder` int(15) NOT NULL,
`unit_price` varchar(10) NOT NULL,
`additional_fee` varchar(10) NOT NULL,
`type` int(1) NOT NULL COMMENT '1 for product; 2 for service',
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `role` (
`id` int(8) NOT NULL auto_increment,
`name` varchar(50) NOT NULL,
`parentId` int(8) NOT NULL,
`permission_id` int(5) NOT NULL,
`created_date` datetime NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
PRIMARY KEY  (`id`),
FOREIGN KEY (`permission_id`) REFERENCES `permission`(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `settings` (
`id` int(8) NOT NULL auto_increment,
`name` varchar(255) NOT NULL,
`description` text NOT NULL,
`value` text NOT NULL,
`created_date` datetime NOT NULL,
PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `user` (
`id` int(11) NOT NULL auto_increment,
`role_id` varchar(255) NOT NULL,
`name` varchar(255) NOT NULL,
`email` varchar(255) NOT NULL,
`passwd` varchar(255) NOT NULL,
`employee_id` int(15) NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
PRIMARY KEY  (`id`),
FOREIGN KEY (`role_id`) REFERENCES `role`(`id`),
FOREIGN KEY (`employee_id`) REFERENCES `employee`(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `job` (
`id` int(15) NOT NULL auto_increment,
`parent_id` int(15) NOT NULL,
`order_no` varchar(30) NOT NULL,
`serial` varchar(15) NOT NULL COMMENT 'job id',
`user_id` int(8) NOT NULL COMMENT 'been assigned to',
`customer_id` int(15) NOT NULL,
`customer_name` varchar(255) NOT NULL,
`awaiting_role_id` varchar(50) NOT NULL,
`status_id` int(5) NOT NULL,
`requestor_id` int(15) NOT NULL,
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`job_name` varchar(50) NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
`display` tinyint(1) NOT NULL default '1',
`is_completed` tinyint(1) NOT NULL,
`status_text` varchar(255) NOT NULL,
`type` varchar(255) NOT NULL,
`type_id` varchar(255) NOT NULL COMMENT '作為edit使用的id',
`type_path` varchar(255) NOT NULL COMMENT '去到edit的path',
PRIMARY KEY  (`id`),
FOREIGN KEY (`user_id`) REFERENCES `user`(`id`),
FOREIGN KEY (`customer_id`) REFERENCES `customer`(`id`),
FOREIGN KEY (`requestor_id`) REFERENCES `employee`(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `workflow` (
`id` int(15) NOT NULL auto_increment,
`title` varchar(50) NOT NULL,
`table_name` varchar(255) NOT NULL,
`status_json` text NOT NULL,
PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `workflow_order` (
`id` int(15) NOT NULL auto_increment,
`status_id` int(5) NOT NULL,
`action` varchar(50) collate utf8_unicode_ci NOT NULL,
`requestor_edit` tinyint(1) NOT NULL,
`role_id` varchar(50) collate utf8_unicode_ci NOT NULL,
`workflow_id` int(8) NOT NULL,
`need_det` tinyint(1) NOT NULL,
`formula` text collate utf8_unicode_ci NOT NULL,
`target_colum` text collate utf8_unicode_ci NOT NULL,
`action_before_complete` varchar(255) collate utf8_unicode_ci NOT NULL,
`next_status_id` int(5) NOT NULL,
`next_status_text` varchar(255) collate utf8_unicode_ci NOT NULL,
`ad_hoc` tinyint(1) NOT NULL,
`who_email` varchar(50) collate utf8_unicode_ci NOT NULL,
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`is_deleted` tinyint(1) NOT NULL,
PRIMARY KEY  (`id`),
FOREIGN KEY (`role_id`) REFERENCES `role`(`id`),
FOREIGN KEY (`workflow_id`) REFERENCES `workflow`(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE `permission` (
`id` int(5) NOT NULL auto_increment,
`quotation_control` varchar(50) NOT NULL,
`tender_control` varchar(50) NOT NULL,
`purchase_control` varchar(50) NOT NULL,
`service_control` varchar(50) NOT NULL,
`commercial_invoice_control` varchar(50) NOT NULL,
`delivery_order_control` varchar(50) NOT NULL,
`service_order_control` varchar(50) NOT NULL,
`inventory_control` varchar(50) NOT NULL,
`rma_control` varchar(50) NOT NULL,
`credit_note_control` varchar(50) NOT NULL,
`manage_employee_control` varchar(50) NOT NULL,
`manage_customer_control` varchar(50) NOT NULL,
`manage_suppliers_control` varchar(50) NOT NULL,
`manage_inventory_control` varchar(50) NOT NULL,
`manage_shipping_control` varchar(50) NOT NULL,
`settings_control` varchar(50) NOT NULL,
`invoice_control` varchar(50) NOT NULL,
`all_job_control` varchar(50) NOT NULL,
`all_customer_control` varchar(50) NOT NULL,
`admin_service_control` varchar(50) NOT NULL,
`department_control` varchar(50) NOT NULL,
`trip_control` varchar(50) NOT NULL,
`reporting_control` varchar(50) NOT NULL,
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`ad_hoc` tinyint(1) NOT NULL,
PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `audit_log` (
`id` int(15) NOT NULL auto_increment,
`log_no` varchar(255) NOT NULL,
`ip_address` varchar(255) NOT NULL,
`user_trigger` int(15) NOT NULL,
`table_affect` varchar(255) NOT NULL,
`description` text NOT NULL,
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `order` (
`id` int(15) NOT NULL auto_increment,
`order_no` varchar(30) NOT NULL,
`order_date` date NOT NULL,
`customer_id` int(15) NOT NULL,
`customer_name` varchar(255) NOT NULL,
`remark` text NOT NULL,
`confirm_file` varchar(255) NOT NULL,
`total_price` decimal(10,2) NOT NULL,
`gst_amount` decimal(10,2) NOT NULL,
`job_id` int(15) NOT NULL,
`latest_job_id` int(15) NOT NULL,
`status` int(5) NOT NULL,
`new_status` tinyint(1) NOT NULL default '0',
`requestor_id` int(15) NOT NULL,
`create_user_id` int(8) NOT NULL,
`lastupdate_user_id` int(8) NOT NULL,
`awaiting_table` varchar(255) NOT NULL,
`no_approve_user` int(8) NOT NULL,
`no_approve_reason` text NOT NULL,
`customer_create` tinyint(1) NOT NULL,
`alias` varchar(255) NOT NULL,
`address` varchar(255) NOT NULL,
`postal` varchar(255) NOT NULL,
`country` varchar(255) NOT NULL,
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
PRIMARY KEY  (`id`),
FOREIGN KEY (`customer_id`) REFERENCES `customer`(`id`),
FOREIGN KEY (`job_id`) REFERENCES `job`(`id`),
FOREIGN KEY (`requestor_id`) REFERENCES `employee`(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `order_item` (
`id` int(8) NOT NULL auto_increment,
`order_id` int(8) NOT NULL,
`product_id` int(8) NOT NULL,
`model_name` varchar(255) NOT NULL,
`quantity` int(5) NOT NULL,
`quantity_order` int(5) NOT NULL,
`price` decimal(10,2) NOT NULL,
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
PRIMARY KEY  (`id`),
FOREIGN KEY (`order_id`) REFERENCES `order`(`id`),
FOREIGN KEY (`product_id`) REFERENCES `product`(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `delivery_order` (
`id` int(15) NOT NULL auto_increment,
`order_id` int(15) NOT NULL,
`packing_id` int(15) NOT NULL,
`order_no` varchar(30) NOT NULL,
`do_no` varchar(30) NOT NULL,
`do_date` date NOT NULL,
`customer_id` int(15) NOT NULL,
`customer_name` varchar(255) NOT NULL,
`alias` varchar(255) NOT NULL,
`instructions` text NOT NULL,
`confirm_file` varchar(255) NOT NULL,
`job_id` int(15) NOT NULL,
`latest_job_id` int(15) NOT NULL,
`status` int(5) NOT NULL,
`staff_name` varchar(255) NOT NULL,
`staff_role` varchar(255) NOT NULL,
`requestor_id` int(15) NOT NULL,
`create_user_id` int(8) NOT NULL,
`lastupdate_user_id` int(8) NOT NULL,
`awaiting_table` varchar(255) NOT NULL,
`no_approve_user` int(8) NOT NULL,
`no_approve_reason` text NOT NULL,
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
PRIMARY KEY  (`id`),
FOREIGN KEY (`order_id`) REFERENCES `order`(`id`),
FOREIGN KEY (`customer_id`) REFERENCES `customer`(`id`),
FOREIGN KEY (`job_id`) REFERENCES `job`(`id`),
FOREIGN KEY (`requestor_id`) REFERENCES `employee`(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `delivery_order_item` (
`id` int(8) NOT NULL auto_increment,
`do_id` int(15) NOT NULL,
`product_id` int(8) NOT NULL,
`model_name` varchar(255) NOT NULL,
`price` decimal(10,2) NOT NULL,
`quantity_del` int(5) NOT NULL,
`quantity_bal` int(5) NOT NULL,
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
PRIMARY KEY  (`id`),
FOREIGN KEY (`product_id`) REFERENCES `product`(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `packing_list` (
`id` int(15) NOT NULL auto_increment,
`order_id` int(15) NOT NULL,
`order_no` varchar(30) NOT NULL,
`packing_no` varchar(30) NOT NULL,
`packing_date` date NOT NULL,
`customer_id` int(15) NOT NULL,
`customer_name` varchar(255) NOT NULL,
`remark` text NOT NULL,
`confirm_file` varchar(255) NOT NULL,
`job_id` int(15) NOT NULL,
`latest_job_id` int(15) NOT NULL,
`status` int(5) NOT NULL,
`staff_name` varchar(255) NOT NULL,
`staff_role` varchar(255) NOT NULL,
`requestor_id` int(15) NOT NULL,
`create_user_id` int(8) NOT NULL,
`lastupdate_user_id` int(8) NOT NULL,
`awaiting_table` varchar(255) NOT NULL,
`no_approve_user` int(8) NOT NULL,
`no_approve_reason` text NOT NULL,
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
PRIMARY KEY  (`id`),
FOREIGN KEY (`order_id`) REFERENCES `order`(`id`),
FOREIGN KEY (`customer_id`) REFERENCES `customer`(`id`),
FOREIGN KEY (`job_id`) REFERENCES `job`(`id`),
FOREIGN KEY (`requestor_id`) REFERENCES `employee`(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `packing_list_item` (
`id` int(8) NOT NULL auto_increment,
`order_id` int(15) NOT NULL,
`packing_list_id` int(8) NOT NULL,
`product_id` int(8) NOT NULL,
`model_name` varchar(255) NOT NULL,
`price` decimal(10,2) NOT NULL,
`quantity_order` int(5) NOT NULL,
`quantity_stock` int(5) NOT NULL,
`quantity_pack` int(5) NOT NULL,
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
PRIMARY KEY  (`id`),
FOREIGN KEY (`order_id`) REFERENCES `order`(`id`),
FOREIGN KEY (`packing_list_id`) REFERENCES `packing_list`(`id`),
FOREIGN KEY (`product_id`) REFERENCES `product`(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `product_category` (
`id` int(8) NOT NULL auto_increment,
`name` varchar(255) NOT NULL,
`description` text NOT NULL,
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `product_log` (
`id` int(15) NOT NULL auto_increment,
`product_id` int(8) NOT NULL,
`qty` int(8) NOT NULL,
`reason` text NOT NULL,
`employee_no` varchar(30) NOT NULL,
`employee_name` varchar(30) NOT NULL,
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
PRIMARY KEY  (`id`),
FOREIGN KEY (`product_id`) REFERENCES `product`(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `promotion` (
`id` int(8) NOT NULL,
`json_content` text NOT NULL,
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `invoice` (
`id` int(15) NOT NULL auto_increment,
`order_id` int(15) NOT NULL,
`do_id` int(15) NOT NULL,
`order_no` varchar(30) NOT NULL,
`invoice_no` varchar(30) NOT NULL,
`invoice_date` date NOT NULL,
`customer_id` int(15) NOT NULL,
`customer_name` varchar(255) NOT NULL,
`notes` text NOT NULL,
`total_amount` decimal(10,2) NOT NULL,
`gst_amount` decimal(10,2) NOT NULL,
`nett_amount` decimal(10,2) NOT NULL,
`confirm_file` varchar(255) NOT NULL,
`job_id` int(15) NOT NULL,
`latest_job_id` int(15) NOT NULL,
`status` int(5) NOT NULL,
`staff_name` varchar(255) NOT NULL,
`staff_role` int(5) NOT NULL,
`requestor_id` int(15) NOT NULL,
`create_user_id` int(8) NOT NULL,
`lastupdate_user_id` int(8) NOT NULL,
`awaiting_table` varchar(255) NOT NULL,
`no_approve_user` int(8) NOT NULL,
`no_approve_reason` text NOT NULL,
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
PRIMARY KEY  (`id`),
FOREIGN KEY (`order_id`) REFERENCES `order`(`id`),
FOREIGN KEY (`customer_id`) REFERENCES `customer`(`id`),
FOREIGN KEY (`job_id`) REFERENCES `job`(`id`),
FOREIGN KEY (`requestor_id`) REFERENCES `employee`(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `invoice_item` (
`id` int(8) NOT NULL auto_increment,
`invoice_id` int(15) NOT NULL,
`product_id` int(8) NOT NULL,
`model_name` varchar(255) NOT NULL,
`quantity` int(5) NOT NULL,
`price` decimal(10,2) NOT NULL,
`total_price` decimal(10,2) NOT NULL,
`created_date` datetime NOT NULL,
`modified_date` datetime NOT NULL,
`is_deleted` tinyint(1) NOT NULL default '0',
PRIMARY KEY  (`id`),
FOREIGN KEY (`invoice_id`) REFERENCES `invoice`(`id`),
FOREIGN KEY (`product_id`) REFERENCES `product`(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `settings` VALUES ('', 'site name', '', 'Pac-fung Agora', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('', 'logo url', '', '', '2016-03-29 18:25:29');
INSERT INTO `user` VALUES ('', '1', 'Admin', 'elvinong@thedottsolutions.com', 'f003c879ab6229dce2b8013a2cb81a20', '1', '0', '2016-03-29 18:25:29', '0000-00-00 00:00:00');
INSERT INTO `employee` VALUES ('', '1', '00001', '1', '', '', '1', '', '', '', '', 'Admin', '', '', '', '', '', '', '', '', '','','', '2016-03-29 18:25:29', '0000-00-00 00:00:00','0');
INSERT INTO `settings` VALUES ('', 'quotation_sale_value', 'The amount of sales value that requires approval by manager /management', '150000', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('', 'purchase_value', 'The amount of sales value that requires approval by manager /management', '0', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('', 'gst_percentage', 'The percentage of GST', '7', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('', 'default_email', 'Default Email Address of Company', 'test@gmail.com', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('', 'main_contact_person', 'Main Contact Person of Company', 'Jason Tian', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('', 'main_contact_no', 'Main Contact No.', '0722334455', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('', 'payment_information', 'To be used in invoice', '', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('', 'purchase_order_terms', 'To be used in Purchase Order', '', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('', 'payment_terms', '', '', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('', 'qty_for_reminder', 'Default Quantity when drop below this amount to send an email reminder to default company email account', '', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('', 'prefix', 'Admin to be able to change all pre-fixed alphabet to be displayed in front of all serial numbers. ', '[{"table_name":"order","prefix":"O","YY":1},{"table_name":"packing_list","prefix":"PL","YY":0},{"table_name":"delivery_order","prefix":"DO","YY":0},{"table_name":"invoice","prefix":"I","YY":0}]', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('', 'reminder_alert', 'Admin to be able to change reminder alert for PO generation (date generated), Quotation Generation (date generated) and Waiting for Good (use date of delivery from Purchase)', '[{"action":"PO generation","date":""},{"action":"Quotation Generation","date":""},{"action":"Waiting for Good ","date":""}]', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('', 'company_name', '', 'Agora', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('16', 'company address', '', '18 Mandai Estate', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('25', 'company postal', '', '729910', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('26', 'reminded_1', '', '', '2016-03-29 18:25:29');
INSERT INTO `settings` VALUES ('27', 'reminded_2', '', '', '2016-03-29 18:25:29');
INSERT INTO `promotion` VALUES ('1', '','2016-03-29 18:25:29','','0');
INSERT INTO `role` VALUES ('1', 'Super Admin', '0', '', '2016-03-29 18:25:29', '0');
INSERT INTO `role` VALUES ('2', 'Sales Manager', '0', '', '2016-03-29 18:25:29', '0');
INSERT INTO `role` VALUES ('3', 'Sales', '2', '', '2016-03-29 18:25:29', '0');
INSERT INTO `role` VALUES ('4', 'Ground Staff', '2', '', '2016-03-29 18:25:29', '0');
INSERT INTO `role` VALUES ('5', 'Admin', '2', '', '2016-03-29 18:25:29', '0');
INSERT INTO `role` VALUES ('6', 'Finance', '2', '', '2016-03-29 18:25:29', '0');
INSERT INTO `role` VALUES ('7', 'Customer', '0', '', '2016-03-29 18:25:29', '0');
INSERT INTO `role` VALUES ('8', 'OPS Manager', '0', '', '2016-03-29 18:25:29', '0');
INSERT INTO `workflow` VALUES ('1', 'Order', 'order', '["No Approved","Pending Approval By Sales","Completed"]');
INSERT INTO `workflow` VALUES ('2', 'Packing List', 'packing_list', '["No Approved","Completed"]');
INSERT INTO `workflow` VALUES ('3', 'Delivery Order', 'delivery_order', '["No Approved","Pending Upload Confirm File By Admin","Completed"]');
INSERT INTO `workflow` VALUES ('4', 'Invoice', 'invoice', '["No Approved","Completed"]');
INSERT INTO `permission` VALUES('1', 'C,R,U,D', 'C,R,U,D','C,R,U,D','C,R,U,D','C,R,U,D','C,R,U,D', 'C,R,U,D','C,R,U,D','C,R,U,D', 'C,R,U,D', 'C,R,U,D', 'C,R,U,D', 'C,R,U,D', 'C,R,U,D', 'C,R,U,D', 'C,R,U,D', 'C,R,U,D', 'R', 'R', 'C,R,U,D', 'C,R,U,D', 'C,R,U,D', 'R', '2016-03-29 18:25:29', '','');
UPDATE `role` SET `permission_id` = '1' WHERE `id` = '1';
INSERT INTO `permission` VALUES('2', 'R', 'R','R','R','R','R', 'R','R','R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', '2016-03-29 18:25:29', '','');
UPDATE `role` SET `permission_id` = '2' WHERE `id` = '2';
INSERT INTO `permission` VALUES('3', 'X', 'X','X','R','X','X', 'C,R,U','C,R,U','C,R,U', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'R', 'R', 'X', 'X', 'C,R,U', 'R', '2016-03-29 18:25:29', '','');
UPDATE `role` SET `permission_id` = '3' WHERE `id` = '3';
INSERT INTO `permission` VALUES('4', 'X', 'X','X','R','X','X', 'C,OR','C,OR,OU','C,OR', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'R', 'R', 'X', 'X', 'C,OR,OU', 'X', '2016-03-29 18:25:29', '','');
UPDATE `role` SET `permission_id` = '4' WHERE `id` = '4';
INSERT INTO `permission` VALUES('5', 'X', 'X','X','R','X','X', 'X','X','X', 'C,R,U', 'X', 'X', 'C,R,U', 'X', 'X', 'X', 'C,R,U', 'R', 'R', 'X', 'X', 'X', 'X', '2016-03-29 18:25:29', '','');
UPDATE `role` SET `permission_id` = '5' WHERE `id` = '5';
INSERT INTO `permission` VALUES('6', 'X', 'X','C,R,U','X','C,R,U','C,R,U', 'X','C,R,U','X', 'X', 'X', 'X', 'X', 'C,R,U', 'C,R,U', 'X', 'X', 'R', 'R', 'X', 'X', 'X', 'X', '2016-03-29 18:25:29', '','');
UPDATE `role` SET `permission_id` = '6' WHERE `id` = '6';
INSERT INTO `permission` VALUES('7', 'C,R,U', 'C,R,U','C,R,U','C,R,U','X','X', 'X','X','X', 'X', 'X', 'C,R,U', 'X', 'X', 'X', 'X', 'X', 'R', 'R', 'X', 'X', 'C,R,U', 'R', '2016-03-29 18:25:29', '','');
UPDATE `role` SET `permission_id` = '7' WHERE `id` = '7';
INSERT INTO `workflow_order` VALUES ('', '-1', 'create', '0', '1', '1', '0', "", "", '0', '2', 'Completed', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '-1', 'create', '0', '7', '1', '0', "", "", '0', '1', 'Pending Approval By Sales', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '-1', 'create', '0', '3', '1', '0', "", "", '0', '2', 'Completed', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '1', 'approval_update_s', '0', '3', '1', '0', "", "", '0', '2', 'Completed', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '2', 'complete', '0', '3', '1', '0', "", "", '0', '2', 'Completed', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '0', 'no_approve', '0', '0', '1', '0', "", "", '0', '0', 'No Approved', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '-1', 'create', '0', '1', '2', '0', "", "", '0', '1', 'Completed', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '-1', 'create', '0', '8', '2', '0', "", "", '0', '1', 'Completed', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '-1', 'create', '0', '4', '2', '0', "", "", '0', '1', 'Completed', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '1', 'complete', '0', '4', '2', '0', "", "", 'pdf', '1', 'Completed', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '0', 'no_approve', '0', '0', '2', '0', "", "", '0', '0', 'No Approved', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '-1', 'create', '0', '1', '3', '0', "", "", '0', '2', 'Completed', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '-1', 'create', '0', '8', '3', '0', "", "", '0', '2', 'Completed', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '-1', 'create', '0', '5', '3', '0', "", "", '0', '2', 'Completed', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '1', 'upload_update', '0', '5', '3', '0', "", "", '', '2', 'Completed', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '2', 'complete', '0', '5', '3', '0', "", "", 'upload', '2', 'Completed', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '0', 'no_approve', '0', '0', '3', '0', "", "", '0', '0', 'No Approved', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '-1', 'create', '0', '6', '4', '0', "", "", '0', '1', 'Completed', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '1', 'complete', '0', '6', '4', '0', "", "", 'pdf', '1', 'Completed', '0', '','','','');
INSERT INTO `workflow_order` VALUES ('', '0', 'no_approve', '0', '0', '4', '0', "", "", '0', '0', 'No Approved', '0', '','','','');
