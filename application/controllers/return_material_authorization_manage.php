<?php

class Return_material_authorization_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();  
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Customers_model');
			$this->load->model('Return_material_authorization_model');
			$this->load->model('Return_material_item_model');
			$this->load->model('Quotation_model');
			$this->load->model('Quotation_item_model');
			$this->load->model('Products_model');
			$this->load->model('Suppliers_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Settings_model');
			$this->load->model('Employee_model');
			$this->load->model('Workflow_model');
			$this->load->model('Workflow_order_model');
			

            $this->data['init'] = $this->function_model->page_init();
			$this->data['status_list'] = $this->Return_material_authorization_model->status_list();
			
			$this->data['role_list'] = $this->Role_model->getIDKeyArray('name');
			
			//get status list
			$rma_status_list = $this->Workflow_model->get(6);
			$this->data['rma_status_list'] = json_decode($rma_status_list['status_json']);
			
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$this->data['userdata'] = $this->session->userdata("userdata");
			}         
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			$this->data['group_name'] = "service";  
			$this->data['model_name'] = "return_material_authorization";  
			$this->data['common_name'] = "Return Material Authorization";   
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}
			
			//related role
		    define('SUPERADMIN', 1);
			define('SALES_MANAGER', 7);
		    define('SALES_EXECUTIVE', 8);
		    define('SERVICE_MANAGER', 3);
		    define('SERVICE_EXECUTIVE', 4);
			define('GENERAL_MANAGER', 2); 
           
      }
   
      public function index($q="ALL", $status="ALL", $page=1) {  
          			
            $this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/anexus/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/'.$status.'/';
			if($q == 'ALL') {
				$q = "";
			}
			$this->data['q'] = $q;
			
			if($status != 'ALL') {
				$filter['status'] = $status;	
			}
			$this->data['status'] = $status;			
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Return_material_authorization_model->record_count($filter, $q);
			
			//get particular ranged list
			$this->data['results'] = $this->Return_material_authorization_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start);
					
			if(!empty($this->data['results'])){
				foreach($this->data['results'] as $k => $v){
					if(!empty($v['awaiting_table'])){
							$this->data['results'][$k]['awaiting_table']= explode(',',$v['awaiting_table']);
					}
				}
			}		
				
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->data['customer_list'] = $this->Customers_model->getIDKeyArray("company_name");
			$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
			
			$this->session->set_userdata("lastpage", $url.$page);
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  
	  public function add($id=false) {  
	  
          	$role_id = $this->data['userdata']['role_id'];
			
			if($id !== false) {
				$this->data['mode'] = 'Edit';
				
				$this->data['result'] = $this->Return_material_authorization_model->get($id);
				
				$requestor_employee = $this->Employee_model->get($this->data['result']['requestor_id']);
			    $requestor_role = $requestor_employee['role_id'];
				$this->data['requestor_role'] = $requestor_role;
				//echo $requestor_role;exit;
				
				
				$this->data['customer'] = $this->Customers_model->get($this->data['result']['customer_id']);
				$this->data['related_item'] = json_encode($this->Return_material_item_model->get_related_item($id));
				//print_r($this->data['result']);exit;								
				
				
				$this->data['result']['awaiting_table'] = explode(',',$this->data['result']['awaiting_table']);
				
				$default = 0;
				
				//////////////////////////////////////////////////////////////////////////////////
				
				   		//get related workflow order data
						$workflow_flitter = array(
							'status_id' => $this->data['result']['status'],
							'workflow_id' => 6,
						);
						
						$type='';
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$type = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$type = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
							$type = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
									$type = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(6);
						$status = json_decode($workflow['status_json'],true);
						
						$this->data['workflow_title'] = $workflow['title'];
					
					$this->data['btn_type'] = $type;
					//print_r($workflow_order);exit;

					switch($type){
						case 'edit':
							$this->data['head_title'] = 'Edit '.$workflow['title'].' Request';
						break;
						case 'approval':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
						break;
						case 'approval_update':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'update':
							$this->data['head_title'] = 'Update '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'send_email':
							$this->data['head_title'] = 'Waiting to Send '.$workflow['title'].' Request';
						break;
						case 'sendmail_update':
							$this->data['head_title'] = 'Waiting to Send/Edit '.$workflow['title'].' Request';
						break;
						case 'upload':
							$this->data['head_title'] = 'Upload Confirm File '.$workflow['title'].' Request';
						break;
						case 'complete':
							$this->data['head_title'] = $workflow['title'].' Request';
							$this->data['last_action'] = $workflow_order['action_before_complete'];
						break;
						case 'upload_collection':
							$this->data['head_title'] = 'Upload Collection '.$workflow['title'].' Request';
						break;
						case 'upload_delivery':
							$this->data['head_title'] = 'Upload Delivery '.$workflow['title'].' Request';
						break;
						default:
							$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					foreach(json_decode($workflow['status_json'],true) as $k => $v){
						
						if($v == 'Completed'){
							$this->data['completed_status_id'] = $k;
							break;
						}
					}
				//////////////////////////////////////////////////////////////////////////////////////
				/*
				//test data
				//$this->data['result']['status'] = 3;
				//$this->data['requestor_role'] = 8;
				//$this->data['userdata']['role_id'] = 6;
				
				
				if($this->data['result']['status'] == 1){
					
					if($role_id == SALES_EXECUTIVE){
						$this->data['head_title'] = 'Edit Return Material Authorization';
					}else if($role_id == SALES_MANAGER){
						$this->data['head_title'] = 'Confirm Return Material Authorization';
					}else{
						$default = 1;
					}
				
				}else if($this->data['result']['status'] == 2){
					
					if($role_id == SERVICE_EXECUTIVE){
						$this->data['head_title'] = 'Edit Return Material Authorization';
					}else if($role_id == SERVICE_MANAGER){
						$this->data['head_title'] = 'Confirm Return Material Authorization';
					}else{
						$default = 1;
					}
					
				}else if($this->data['result']['status'] == 3){
					
					if($role_id == SALES_MANAGER || $role_id == SERVICE_MANAGER){
						$this->data['head_title'] = 'Edit Return Material Authorization';
					}else if($role_id == GENERAL_MANAGER){
						$this->data['head_title'] = 'Confirm Return Material Authorization';
					}else{
						$default = 1;
					}
					
				}else if($this->data['result']['status'] == 4){
					
					if(($role_id == SALES_MANAGER || $role_id == SERVICE_MANAGER || $role_id == SERVICE_EXECUTIVE ||  $role_id == SALES_EXECUTIVE) && $requestor_role == $role_id){
						$this->data['head_title'] = 'Send Return Material Authorization';
					}else{
						$default = 1;
					}
					
				}else if($this->data['result']['status'] == 5){
					
					if(($role_id == SALES_MANAGER || $role_id == SERVICE_MANAGER || $role_id == SERVICE_EXECUTIVE ||  $role_id == SALES_EXECUTIVE) && $requestor_role == $role_id){
						$this->data['head_title'] = 'Complete Collection Return Material Authorization';
					}else{
						$default = 1;
					}
					
				}else if($this->data['result']['status'] == 6){
					
					if(($role_id == SALES_MANAGER || $role_id == SERVICE_MANAGER || $role_id == SERVICE_EXECUTIVE ||  $role_id == SALES_EXECUTIVE) && $requestor_role == $role_id){
						$this->data['head_title'] = 'Complete Delivery Return Material Authorization';
					}else{
						$default = 1;
					}
					
				}else{
					$default = 1;
				}
				
				//no related person
				if($default == 1){
					$this->data['head_title'] = 'View Return Material Authorization';	
				}
				*/
				
			} else {
				$this->data['mode'] = 'Add';
				$this->data['head_title'] = 'Create Return Material Authorization';	
				
				//get related workflow order data
					$workflow_flitter = array(
						'role_id' => $role_id,
						'status_id' => -1,
						'workflow_id' => 6,
					);
					
					//data form database
					$workflow = $this->Workflow_model->get(6);
					$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
					
					//get view bottom layout
					$type = '';
					if(!empty($workflow_order)){
						$type = $workflow_order['action'];
						if($workflow_order['action'] == 'create'){
							$this->data['head_title'] = 'Create '.$workflow['title'].' Request';
						}else{
							$this->data['head_title'] = $workflow['title'].' Request';
						}
					}else{
						$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					$this->data['btn_type'] = $type;
					//echo $type;exit;
			}
			
			
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		
            
			
      }	  	  
	  
	  public function submit(){
		  $now = date("Y-m-d H:i:s");
		  $mode = $this->input->post("mode", true);
		  $id = $this->input->post("id", true);
		  		  		 
		  $role_id = $this->data['userdata']['role_id'];
		  
		  //get value approve or not approve
		  $confirm = $this->input->post("confirm", true);
		  $correct_check = $this->input->post("correct_check", true);
				
		  $next = 0;
		  $last = 0;
		  $status_change = 0;
				
						 
		  $type = $this->input->post("type", true);	
		  $type_value = $this->input->post("type_value", true);			 

		  
		   if($mode == 'Add'){
			  
			  $customer_id = $this->input->post("customer_id", true);
		  	  
		  }else{
		  
			  //rma
			  $rma_data = $this->Return_material_authorization_model->get($id);
		  	  $customer_id = $rma_data['customer_id'];
			  
		  }	
		  
		  
 		    //job
		  	$jobArray = array(
				'parent_id' =>0,
				'user_id' =>$this->data['userdata']['id'],
				'customer_id' =>$customer_id,
				'type' => 'Return Material Authorization',
				'type_path' =>'service/return_material_authorization_edit/',
		 	 );	
			 
 			//main
		  $array = array(	
		  	'type_of_return' => $this->input->post("type_return", true),  
			'reason_of_return' => $this->input->post("reason_return", true), 
			'customer_id' => $this->input->post("customer_id", true), 
			'customer_name' => $this->input->post("customer_name", true),
			'type_of_return' => $this->input->post("type_return", true), 
			'requestor_id' => $this->input->post("requestor_id", true),	
		  );
		  
		  	//mail
			$mail_array = array(
		  	'creater_name' =>$this->data['userdata']['name'],
		  	);
		  
		  if($type == 1){
			  
			  $collection_date = $this->input->post("date_collection", true);
			  if(!empty($collection_date)){

			  $tmp_date = explode('/',$collection_date);
			  $collection_date = $tmp_date[2].'-'.$tmp_date[1].'-'.$tmp_date[0];

			  }
			  //exit;
			  $array['collection_date'] = $collection_date;
			  $array['collection_special_instruction'] = $this->input->post("special_instruct_collect", true);
			  
			  if(isset($_FILES['collect_file'])){
				
				if ( $_FILES['collect_file']['error'] == 0 && $_FILES['collect_file']['name'] != ''  ){									
						$pathinfo = pathinfo($_FILES['collect_file']['name']);
						$ext = $pathinfo['extension'];
						$ext = strtolower($ext);
											
						$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
						$path = "./uploads/".$filename.'.'.$ext;
						$save_path = base_url()."uploads/".$filename.'.'.$ext;
						$result = move_uploaded_file($_FILES['collect_file']['tmp_name'], $path);
						if(!$result) {
							die("cannot upload file");
						}	
					
						$array['collection_confirm_ref']= $save_path;															
				}	
			  }
			  
		  }else if($type == 2){
			  
			  $return_date = $this->input->post("date_return", true);
			  if(!empty($return_date)){
			  $tmp_date = explode('/',$return_date);
			  $return_date=$tmp_date[2].'-'.$tmp_date[1].'-'.$tmp_date[0];
			  }
			  
			  $array['return_date'] = $return_date;
			  $array['return_special_instruction'] = $this->input->post("special_instruct_return", true);
			  
			  if(isset($_FILES['return_file'])){
				
				if ( $_FILES['return_file']['error'] == 0 && $_FILES['return_file']['name'] != ''  ){									
						$pathinfo = pathinfo($_FILES['return_file']['name']);
						$ext = $pathinfo['extension'];
						$ext = strtolower($ext);
											
						$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
						$path = "./uploads/".$filename.'.'.$ext;
						$save_path = base_url()."uploads/".$filename.'.'.$ext;
						$result = move_uploaded_file($_FILES['return_file']['tmp_name'], $path);
						if(!$result) {
							die("cannot upload file");
						}	
					
						$array['return_confirm_ref']= $save_path;															
				}	
			  }
			  
		  }else if($type == 3){
			  
			  $collection_date = $this->input->post("date_collection", true);
			  if(!empty($collection_date)){

			  $tmp_date = explode('/',$collection_date);
			  $collection_date = $tmp_date[2].'-'.$tmp_date[1].'-'.$tmp_date[0];

			  }
			  //exit;
			  $array['collection_date'] = $collection_date;
			  $array['collection_special_instruction'] = $this->input->post("special_instruct_collect", true);
			  
			  if(isset($_FILES['collect_file'])){
				
				if ( $_FILES['collect_file']['error'] == 0 && $_FILES['collect_file']['name'] != ''  ){									
						$pathinfo = pathinfo($_FILES['collect_file']['name']);
						$ext = $pathinfo['extension'];
						$ext = strtolower($ext);
											
						$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
						$path = "./uploads/".$filename.'.'.$ext;
						$save_path = base_url()."uploads/".$filename.'.'.$ext;
						$result = move_uploaded_file($_FILES['collect_file']['tmp_name'], $path);
						if(!$result) {
							die("cannot upload file");
						}	
					
						$array['collection_confirm_ref']= $save_path;															
				}	
			  }
			  
			  
			  $return_date = $this->input->post("date_return", true);
			  if(!empty($return_date)){
			  $tmp_date = explode('/',$return_date);
			  $return_date=$tmp_date[2].'-'.$tmp_date[1].'-'.$tmp_date[0];
			  }
			  
			  $array['return_date'] = $return_date;
			  $array['return_special_instruction'] = $this->input->post("special_instruct_return", true);
			  
			  if(isset($_FILES['return_file'])){
				
				if ( $_FILES['return_file']['error'] == 0 && $_FILES['return_file']['name'] != ''  ){									
						$pathinfo = pathinfo($_FILES['return_file']['name']);
						$ext = $pathinfo['extension'];
						$ext = strtolower($ext);
											
						$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
						$path = "./uploads/".$filename.'.'.$ext;
						$save_path = base_url()."uploads/".$filename.'.'.$ext;
						$result = move_uploaded_file($_FILES['return_file']['tmp_name'], $path);
						if(!$result) {
							die("cannot upload file");
						}	
					
						$array['return_confirm_ref']= $save_path;															
				}	
			  }
			  
		  }
		  
		  
		  
		  //Add
		  if($mode == 'Add') {			  			  
			  
			  /*
			  //Sales Executive
			  if($role_id == SALES_EXECUTIVE){
			  	  
				    $array['awaiting_table']  = SALES_MANAGER;
					$array['status']  = 1;
					//job
					$jobArray['awaiting_role_id'] = SALES_MANAGER.',';
					$jobArray['status_id'] = 1;
					$jobArray['status_text'] = $this->data['rma_status_list'][1];
					//mail
					$mail_array['related_role'] = array($role_id,SALES_MANAGER);
				  
			  }else if($role_id == SERVICE_EXECUTIVE){
			  	  
				    $array['awaiting_table']  = SERVICE_MANAGER;
					$array['status']  = 2;
					//job
					$jobArray['awaiting_role_id'] = SERVICE_MANAGER.',';
					$jobArray['status_id'] = 2;
					$jobArray['status_text'] = $this->data['rma_status_list'][2];
					//mail
					$mail_array['related_role'] = array($role_id,SERVICE_MANAGER);

			  }else if($role_id == SALES_MANAGER || $role_id == SERVICE_MANAGER){
			  	  
					$array['awaiting_table']  = GENERAL_MANAGER;
					$array['status']  = 3;
					//job
					$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
					$jobArray['status_id'] = 3;
					$jobArray['status_text'] = $this->data['rma_status_list'][3];
					//mail
					$mail_array['related_role'] = array($role_id,GENERAL_MANAGER);
					
			  }
			  
			  //$array['create_user_id'] = $this->data['userdata']['id'];
			  $array['requestor_id'] = $this->data['userdata']['employee_id'];
			  $array['created_date'] = $now;
			  $insert_id = $this->Return_material_authorization_model->insert($array);	
			  $return_material_serial_no = $this->Return_material_authorization_model->zerofill($insert_id, 5);
			  
			  $jobArray['serial'] =$return_material_serial_no;
			  $jobArray['type_id'] =$insert_id;
			  $jobArray['created_date'] = $now;
			  $jobArray['modified_date'] = $now;
			  $job_id = $this->Job_model->insert($jobArray);
			  
			  //after insert job,update job id,qr serial....
			  $array =array(
			  		'job_id'	=> $job_id,
			  		'return_material_serial_no'  => $return_material_serial_no,
					'latest_job_id'	=> $job_id,
			  );
			  $this->Return_material_authorization_model->update($insert_id, $array);
				
			
			  //mail
			  $mail_array['title'] = $return_material_serial_no.' '.$this->data['rma_status_list'][1];
			  */
	  
	          ////////////////////////////////////////////////////////////////////////////////////
				
				//get related workflow order data
				$workflow_flitter = array(
					'role_id' => $role_id,
					'status_id' => -1,
					'workflow_id' => 6,
				);
				
				//data form database
				$workflow = $this->Workflow_model->get(6);
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
				$status = json_decode($workflow['status_json'],true);
				
				
				
				if(!empty($workflow_order)){
				
				//ad_hoc
				if($workflow_order['ad_hoc'] == 1){
					$array['ad_hoc'] = 1;
				}
				
				//purchaser
				$is_adhoc = false;
				if($role_id == 10){
					$is_adhoc = true;
				}
				
				//next status
				$next_status = $workflow_order['next_status_id'];
				$next_status_text = $status[$workflow_order['next_status_id']];
				
			  	//if need_det = 1
				if($workflow_order['need_det'] == 1){
					
					$formula = json_decode($workflow_order['formula'],true);
					
					//check if product or service price over value
					foreach($formula as $k => $v){
					
						$price = $this->input->post($v['target_colum'], true);
						$det_temp = $price." ".$v['logic']." ".$v['value'];
						$check = $this->parse_boolean($det_temp);
						
						//check price over value
						if($check){
							$next_status = $v['next_status_id'];
							$next_status_text = $status[$v['next_status_id']];
							break;
						}
						
					}
					
				}
				
				//get next status workflow order data
				$next_workflow_flitter = array(
					'status_id' => $next_status,
					'workflow_id' => 6,
				);
				
				//next status data
				$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
				$awating_person = $next_status_data['role_id'];
				
				
				
				if($awating_person == 'requestor'){
					$awating_person = $this->input->post("requestor_id", true);
				}
				//echo $awating_person;exit;
				
				//main
				$array['awaiting_table']  = $awating_person;
				$array['status'] = $next_status;
				//main
				$jobArray['awaiting_role_id'] = $awating_person.',';
				$jobArray['status_id'] = $next_status;
				$jobArray['status_text'] = $next_status_text; 	
				  
				  //insert main data
					$array['created_date'] = $now;
					$array['modified_date'] = $now;
					$array['create_user_id'] = $this->data['userdata']['id'];
					$array['lastupdate_user_id'] = $this->data['userdata']['id'];

					//新增資料
					$insert_id = $this->Return_material_authorization_model->insert($array);					
					//echo $this->db->last_query();
					//print_r($array);exit;

					
					//更新purchase serial
			  		$return_material_serial_no = $this->Return_material_authorization_model->zerofill($insert_id, 5);
					
					//insert job
					$jobArray['serial'] = $return_material_serial_no;
					$jobArray['type_id'] = $insert_id;
					
					$jobArray['created_date'] = $now;
					$jobArray['modified_date'] = $now;
					
					$job_id = $this->Job_model->insert($jobArray);
				  
					//after insert job,update job id,qr serial....
					$array =array(
						'job_id'	=> $job_id,
						'return_material_serial_no'  => $return_material_serial_no,
						'latest_job_id'	=> $job_id,
					);
					$this->Return_material_authorization_model->update($insert_id, $array);
					
				  //mail
				  $related_role = $this->Job_model->getRelatedRoleID($job_id);
				  $mail_array['related_role'] = $related_role;		
				  $mail_array['title'] = $workflow['title']." ".$return_material_serial_no." ".$next_status_text; 
				  	
				}
				
				////////////////////////////////////////////////////////////////////////////////////
	  			
			  
		  //Edit	  			  
		  } else {
			  
			    $rma_data = $this->Return_material_authorization_model->get($id);
			  	$rma_status = $rma_data['status'];
			  
			  	$requestor_employee = $this->Employee_model->get($rma_data['requestor_id']);
			    $requestor_role = $requestor_employee['role_id'];
			  
			  	$jobArray['serial'] =$rma_data['return_material_serial_no'];
			  	$jobArray['type_id'] =$rma_data['id'];
			  	$jobArray['parent_id'] = $rma_data['latest_job_id'];
				$jobArray['created_date'] = $now;
				$jobArray['modified_date'] = $now;
				
				$next = 0;
			  	$last = 0;
			  	$status_change = 0;
				
				/////////////////////////////////////////////////////////////////////////////////
			  
			  //can edit status
			  $edit_status = $this->Workflow_order_model->get_status_edit(6);
			  
			  //requestor edit
			  if(in_array($rma_status, $edit_status) && $role_id == $requestor_role){
			  	  
						//get related workflow order data
						$workflow_flitter = array(
							'role_id' => $role_id,
							'status_id' => -1,
							'workflow_id' => 6,
						);
						
						//data form database
						$workflow = $this->Workflow_model->get(6);
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						$status = json_decode($workflow['status_json'],true);
						
						if(!empty($workflow_order)){
						
						//next status
						$next_status = $workflow_order['next_status_id'];
						$next_status_text = $status[$workflow_order['next_status_id']];
						
						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];
								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						//get next status workflow order data
						$next_workflow_flitter = array(
							'status_id' => $next_status,
							'workflow_id' => 6,
						);
						
						//next status data
						$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
						$awating_person = $next_status_data['role_id'];
						
						//if awating person equal requestor ,asign requestor for it
						if($awating_person == 'requestor'){
							$awating_person = $requestor_role;
						}
						
						//main
						$array['awaiting_table']  = $awating_person;
						$array['status'] = $next_status;
						
						//job
						$jobArray['awaiting_role_id'] = $awating_person.',';
						$jobArray['status_id'] = $next_status;
						$jobArray['status_text'] = $next_status_text;
						
						
						}
						
				  
			  }else{
				  		
												
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $rma_status,
							'workflow_id' => 6,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(6);
						$status = json_decode($workflow['status_json'],true);
						
						//print_r($workflow_order);exit;
						if(isset($action)){
						
						//action
						switch ($action) {
							case 'approval':
							
								//clear data infront
								$array = array();
							
							
								//approved
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];

										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
										
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'approval_sendmail':
							
								//clear data infront
								$array = array();
							
								//approved
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];

										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
										
										//sent mail to service manager
										$role_service_manager = $this->Employee_model->get_related_role(array($workflow_order['who_email']));
										$sitename = $this->Settings_model->get_settings(1);
										$default_email = $this->Settings_model->get_settings(6);
										
										$this->load->library('email');
										
										if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
											foreach($role_service_manager as $k => $v){
												
												$this->email->clear();
												$this->email->from($default_email['value'], $sitename['value']);
												$this->email->to($v['email']); 
												$this->email->subject('Service request completed');
												$this->email->message($sr_data['service_serial'].' Service request had completed');
												$this->email->send();
											  
											}
										}
										
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'update':
								
								//next status
								$next_status = $workflow_order['next_status_id'];
								$next_status_text = $status[$workflow_order['next_status_id']];
								
								$array = array(
								);
								
								//check last job
								if($workflow_order['next_status_text'] == 'Completed'){
									$last = 1;
								}
								
								$next = 1;
								
								break;
							case 'approval_update':
								
								//clear data infront
								$array = array();
								
								if($confirm == 1){
									
									if($correct_check == 'on'){
										
										//next status
										$next_status = $workflow_order['next_status_id'];
										$next_status_text = $status[$workflow_order['next_status_id']];
										
										$target_colum = json_decode($workflow_order['target_colum'],true);
										
										//check update colum single or multiple
										foreach($target_colum as $k => $v){
										
											if($v['type'] == 'single'){
												
												if($v['data_type'] == 'string'){
													
												$array[$v['name']] = $this->input->post($v['name'],true);
												
												}else if($v['data_type'] == 'date'){
													
													$date = $this->input->post($v['name'], true);
													if(!empty($date)) {
													  $tmp = explode("/", $date);
													  $date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
													  $array[$v['name']] = $date;
													}
													
												}
											
											}else{
												
											}
											
										}
										$next = 1;
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
									}
									
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
							case 'upload':
							
								//clear data infront
								$array = array();
								
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'comfirm_file'	=> $save_path,
											);	
											
											//check last job
											if($workflow_order['next_status_text'] == 'Completed'){
												$last = 1;
											}
											
											//next status
											$next_status = $workflow_order['next_status_id'];
											$next_status_text = $status[$workflow_order['next_status_id']];	
											$next = 1;
																							
									}	
									
								}
								
								break;	
							case 'complete':
							
								//clear data infront
								$array = array();
							
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'comfirm_file'	=> $save_path,
											);	
										
									}	
								}
								
								break;	
							case 'upload_collection':
							
								//clear data infront
								$array = array();
								
								//complete collect
						if($type_value == 'Complete Collection'){
							
							if(isset($_FILES['collect_file'])){
				
								if ( $_FILES['collect_file']['error'] == 0 && $_FILES['collect_file']['name'] != ''  ){									
										$pathinfo = pathinfo($_FILES['collect_file']['name']);
										$ext = $pathinfo['extension'];
										$ext = strtolower($ext);
															
										$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
										$path = "./uploads/".$filename.'.'.$ext;
										$save_path = base_url()."uploads/".$filename.'.'.$ext;
										$result = move_uploaded_file($_FILES['collect_file']['tmp_name'], $path);
										if(!$result) {
											die("cannot upload file");
										}	
									
										$array['collection_confirm_ref']= $save_path;	
										
										//next status
										$next_status = $workflow_order['next_status_id'];
										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;														
								}	
							}
							
							
								//check last job
								if($workflow_order['next_status_text'] == 'Completed'){
										$last = 1;
								}
								
						
						//no approve	
						}else if($type_value == 'Not Approved'){
							
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	$next = 1;
						  	$last = 1;	
							
						}
								
								
								break;	
							case 'upload_delivery':
							
								//clear data infront
								$array = array();
								
								if($type_value == 'Complete Delivery'){
							
							if(isset($_FILES['return_file'])){
				
								if ( $_FILES['return_file']['error'] == 0 && $_FILES['return_file']['name'] != ''  ){									
										$pathinfo = pathinfo($_FILES['return_file']['name']);
										$ext = $pathinfo['extension'];
										$ext = strtolower($ext);
															
										$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
										$path = "./uploads/".$filename.'.'.$ext;
										$save_path = base_url()."uploads/".$filename.'.'.$ext;
										$result = move_uploaded_file($_FILES['return_file']['tmp_name'], $path);
										if(!$result) {
											die("cannot upload file");
										}	
									
										$array['return_confirm_ref']= $save_path;	
										
										//next status
										$next_status = $workflow_order['next_status_id'];
										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;														
								}	
							 }
							
								//check last job
								if($workflow_order['next_status_text'] == 'Completed'){
										$last = 1;
								}
								
						
						//no approve	
						}else if($type_value == 'Not Approved'){
							
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;

						  	$next = 1;
						  	$last = 1;	
							
						}
								
								
								
								break;	
						}
						
						//print_r($workflow_order);exit;
						//if need_det = 1
						
						if($workflow_order['need_det'] == 1){

							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$value = $this->input->post($v['target_colum'], true);
								//string or number
								if(is_numeric($v['value'])){
									$det_temp = $value." ".$v['logic']." ".$v['value'];
								}else{
									
									if($v['value'] == 'original'){
										$string = $sr_data['term_condition'];
									}else{
										$string = $v['value'];
									}
									
									$det_temp = "'".md5(trim($value))."' ".$v['logic']." '".md5(trim($string))."'";
								}
								
								
								$check = $this->parse_boolean($det_temp);
								
								//check
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
							}
							
						}

						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 6,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
						}
						
						}
						
				  
			  }
			  
			 	if(isset($next_status)){
					//main
					$array['awaiting_table']  = $awating_person;
					$array['status'] = $next_status;
					//job
					$jobArray['awaiting_role_id'] = $awating_person.',';
					$jobArray['status_id'] = $next_status;
					$jobArray['status_text'] = $next_status_text;
				}
				//print_r($array);exit;
			  //////////////////////////////////////////////////////////////////////////////////////
				
				/*
				//判断status 给予下个job
				//status 1
				if($rma_status == 1){
					//edit
					if($role_id == SALES_EXECUTIVE){
						
						$array['awaiting_table']  = SALES_MANAGER;
						$array['status']  = 1;
						//job
						$jobArray['awaiting_role_id'] = SALES_MANAGER.',';
						$jobArray['status_id'] = 1;
						$jobArray['status_text'] = $this->data['rma_status_list'][1];
						//mail
						$mail_array['related_role'] = array($role_id,SALES_MANAGER);
						$mail_array['title'] = $rma_data['return_material_serial_no'].' '.$this->data['rma_status_list'][1];
						
					//sales manager
					}else if($role_id == SALES_MANAGER){
						
						//sales manager denied
						if($confirm == 0){
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	//mail
							$mail_array['related_role'] = array($role_id,SALES_EXECUTIVE);
							$mail_array['title'] = $rma_data['return_material_serial_no'].' '.$this->data['rma_status_list'][0];
						  
						  	$next = 1;
						  	$last = 1;	
						//sales manager approved	
						} else if($correct_check == 'on'){
							
							$array['awaiting_table']  = SALES_EXECUTIVE;
							$array['status']  = 4;
							$jobArray['awaiting_role_id'] = SALES_EXECUTIVE.',';
							$jobArray['status_id'] = 4;
							$jobArray['status_text'] = $this->data['rma_status_list'][4];
							
							//mail
							$mail_array['related_role'] = array(SALES_EXECUTIVE,SALES_MANAGER);
							$mail_array['title'] = $rma_data['return_material_serial_no'].' '.$this->data['rma_status_list'][4];							
							$next = 1;													
						}
						
						
						
					}
				//status 2	
				}else if($rma_status == 2){
					//edit
					if($role_id == SERVICE_EXECUTIVE){
						
						$array['awaiting_table']  = SERVICE_MANAGER;
						$array['status']  = 2;
						//job
						$jobArray['awaiting_role_id'] = SERVICE_MANAGER.',';
						$jobArray['status_id'] = 2;
						$jobArray['status_text'] = $this->data['rma_status_list'][2];
						//mail
						$mail_array['related_role'] = array($role_id,SERVICE_MANAGER);
						$mail_array['title'] = $rma_data['return_material_serial_no'].' '.$this->data['rma_status_list'][2];
						
					//service manager
					}else if($role_id == SERVICE_MANAGER){
						//service manager denied
						if($confirm == 0){
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	//mail
							$mail_array['related_role'] = array($role_id,SALES_EXECUTIVE);
							$mail_array['title'] = $rma_data['return_material_serial_no'].' '.$this->data['rma_status_list'][0];
						  
						  	$next = 1;
						  	$last = 1;	
							
						//service manager approved	
						} else if($correct_check == 'on'){
							
							$array['awaiting_table']  = SERVICE_EXECUTIVE;
							$array['status']  = 4;
							$jobArray['awaiting_role_id'] = SERVICE_EXECUTIVE.',';
							$jobArray['status_id'] = 4;
							$jobArray['status_text'] = $this->data['rma_status_list'][4];
							
							//mail
							$mail_array['related_role'] = array($role_id,SERVICE_EXECUTIVE);
							$mail_array['title'] = $rma_data['return_material_serial_no'].' '.$this->data['rma_status_list'][4];							
							$next = 1;													
						}
						
					}	
				//status 3
				}else if($rma_status == 3){
					//edit
					if($role_id == SERVICE_MANAGER || $role_id == SALES_MANAGER){
						
						$array['awaiting_table']  = GENERAL_MANAGER;
						$array['status']  = 3;
						//job
						$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
						$jobArray['status_id'] = 3;
						$jobArray['status_text'] = $this->data['rma_status_list'][3];
						//mail
						$mail_array['related_role'] = array($role_id,GENERAL_MANAGER);
						$mail_array['title'] = $rma_data['return_material_serial_no'].' '.$this->data['rma_status_list'][1];
						
					//GENERAL_MANAGER
					}else if($role_id == GENERAL_MANAGER){
						//GENERAL_MANAGER denied
						if($confirm == 0){
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	//mail
							$mail_array['related_role'] = array($role_id,SALES_EXECUTIVE);
							$mail_array['title'] = $rma_data['return_material_serial_no'].' '.$this->data['rma_status_list'][0];
						  
						  	$next = 1;
						  	$last = 1;	
							
						//GENERAL_MANAGER approved	
						} else if($correct_check == 'on'){
							
							$array['awaiting_table']  = $requestor_role;
							$array['status']  = 4;
							$jobArray['awaiting_role_id'] = $requestor_role.',';
							$jobArray['status_id'] = 4;
							$jobArray['status_text'] = $this->data['rma_status_list'][4];
							
							//mail
							$mail_array['related_role'] = array($role_id,$requestor_role);
							$mail_array['title'] = $rma_data['return_material_serial_no'].' '.$this->data['rma_status_list'][4];							
							$next = 1;													
						}
						
					}
				//status 5
				}else if($rma_status == 5){
					
					if($role_id == $requestor_role){
						
						//complete collect
						if($type_value == 'Complete Collection'){
							
							if(isset($_FILES['collect_file'])){
				
								if ( $_FILES['collect_file']['error'] == 0 && $_FILES['collect_file']['name'] != ''  ){									
										$pathinfo = pathinfo($_FILES['collect_file']['name']);
										$ext = $pathinfo['extension'];
										$ext = strtolower($ext);
															
										$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
										$path = "./uploads/".$filename.'.'.$ext;
										$save_path = base_url()."uploads/".$filename.'.'.$ext;
										$result = move_uploaded_file($_FILES['collect_file']['tmp_name'], $path);
										if(!$result) {
											die("cannot upload file");
										}	
									
										$array['collection_confirm_ref']= $save_path;															
								}	
							}
							
							$array['awaiting_table']  = $requestor_role;
							$array['status']  = 6;
							//job
							$jobArray['awaiting_role_id'] = $requestor_role.',';
							$jobArray['status_id'] = 6;
							$jobArray['status_text'] = $this->data['rma_status_list'][6];
							//mail
							$mail_array['related_role'] = array($requestor_role);
							$mail_array['title'] = $rma_data['return_material_serial_no'].' '.$this->data['rma_status_list'][6];
							$next = 1;
						
						//no approve	
						}else if($type_value == 'Not Approved'){
							
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	//mail
							$mail_array['related_role'] = array($role_id);
							$mail_array['title'] = $rma_data['return_material_serial_no'].' '.$this->data['rma_status_list'][0];
						  
						  	$next = 1;
						  	$last = 1;	
							
						}
					
						
						
					}
					
				//status 6
				}else if($rma_status == 6){
					
					if($role_id == $requestor_role){
						//echo $type_value;exit;
						//complete delivery
						if($type_value == 'Complete Delivery'){
							
							if(isset($_FILES['return_file'])){
				
								if ( $_FILES['return_file']['error'] == 0 && $_FILES['return_file']['name'] != ''  ){									
										$pathinfo = pathinfo($_FILES['return_file']['name']);
										$ext = $pathinfo['extension'];
										$ext = strtolower($ext);
															
										$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
										$path = "./uploads/".$filename.'.'.$ext;
										$save_path = base_url()."uploads/".$filename.'.'.$ext;
										$result = move_uploaded_file($_FILES['return_file']['tmp_name'], $path);
										if(!$result) {
											die("cannot upload file");
										}	
									
										$array['return_confirm_ref']= $save_path;															
								}	
							 }
							
							$array['awaiting_table']  = $requestor_role;
							$array['status']  = 7;
							//job
							$jobArray['awaiting_role_id'] = $requestor_role.',';
							$jobArray['status_id'] = 7;
							$jobArray['status_text'] = $this->data['rma_status_list'][7];
							//mail
							$mail_array['related_role'] = array($requestor_role);
							$mail_array['title'] = $rma_data['return_material_serial_no'].' '.$this->data['rma_status_list'][7];
							$next = 1;
							$last = 1;
						
						//no approve	
						}else if($type_value == 'Not Approved'){
							
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	//mail
							$mail_array['related_role'] = array($role_id);
							$mail_array['title'] = $rma_data['return_material_serial_no'].' '.$this->data['rma_status_list'][0];
						  
						  	$next = 1;
						  	$last = 1;	
							
						}
						
					}
				}	
				*/
				
			  if($next == 1){
				  $this->Job_model->update($jobArray['parent_id'], array(
				  	'is_completed' => 1,
					'display' => 0,
				  ));
				  //不是最後
				  if($last == 0){
				  	$new_job_id = $this->Job_model->insert($jobArray);				  	
					$array['latest_job_id'] = $new_job_id;
				  }
			  }
						
				
			  //$array['lastupdate_user_id'] = $this->data['userdata']['id'];
			  $array['modified_date'] = $now;
			  $insert_id = $id;
			  //print_r($array);exit;
			  
			  $this->Return_material_authorization_model->update($id, $array);
			  
			  //mail
				  if(isset($new_job_id)){
				  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
				  $mail_array['related_role'] = $related_role;		
				  $mail_array['title'] = $workflow['title']." ".$rma_data['return_material_serial_no']." ".$next_status_text;
				  }
			    
		  }
		  
		  $item_type = 0;
		  if($mode == 'Add'){
				
				$item_type = 1;
			
		  }else if($mode == 'Edit' && $role_id == $requestor_role){
				
				if($rma_status < 4 && $rma_status != 0){
					$item_type = 1;
				}
					
		  }
		  
		  if($item_type == 1){
		  
		  //print_r($_POST);exit;
		  foreach($_POST['product_id'] as $k => $v){
			  
			  $item_array = array(
			  	'return_material_auth_id' => $insert_id,
				'product_id' => $_POST['product_id'][$k],
				'item_name' => $_POST['model_no'][$k],
				'quantity' => $_POST['quantity'][$k],
				'part_number' => $_POST['part_no'][$k],
				'created_date' => $now,
			  );
			  
			  if($_POST['item_id'][$k] == 0 && $_POST['is_del'][$k] == 0 ){
			  	$this->Return_material_item_model->insert($item_array);	
			  }if($_POST['item_id'][$k] != 0 && $_POST['is_del'][$k] != 0 ){
			  	$this->Return_material_item_model->delete($_POST['item_id'][$k]);	
			  }
			 // print_r($item_array);exit;
			  
		  }
		  
		  }

		  //sent mail
		  if(isset($mail_array['related_role'])){
		  	$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
			$sitename = $this->Settings_model->get_settings(1);
		  	$default_email = $this->Settings_model->get_settings(6);
			
		  	$this->load->library('email');
			
			if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
		  			  	
				foreach($related_role_data as $k => $v){
			 	$this->email->clear();
				$this->email->from($default_email['value'], $sitename['value']);
				$this->email->to($v['email']); 
				
				$this->email->subject($mail_array['title']);
				$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
				$this->email->send();	
				  
				}
			
			}
		  }
		  
		  
		  
		  		  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		  
		  
	  }
	  
	  public function del($id){
		  $this->Return_material_authorization_model->delete($id);
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		
	  }  
	  
	  public function ajax_getQuotation(){
		  
		  	$keyword = $_POST['keyword'];
			$limit = $_POST['limit'];
			$page = $_POST['page'];
			
			$start = ($page-1) * $limit;
		  	//我們只取已經confirm的quotation
		  	$record_count = $this->Quotation_model->ajax_record_count($keyword, array(
				'status'	=> 6
			));
		  	$data = $this->Quotation_model->ajax_quotation($keyword, $limit, $start, array(
				'status'	=> 6
			));
		  	$paging = $this->get_paging($limit,10,$record_count,$page);
		  	
			$quotation_status = $this->Quotation_model->quotation_status_list();
			
			//顯示Status為狀態文字, 不是數字
			if(!empty($data)) {
				foreach($data as $k=>$v) {
					$data[$k]['status'] = $quotation_status[$v['status']];
				}
			}
			
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					'paging'	=> $paging,
			);
			
			echo json_encode($temp);	
			exit;
	  }	  
	  
	  public function ajax_getQuotationData(){
		  
		  	$id = $_POST['id'];
		  	$data = $this->Quotation_model->get($id);
			
			
			//get quotation related item
			$related_quotation_item = $this->Quotation_item_model->getRelatedProduct($id);
			$customer_data = $this->Customers_model->get($data['customer_id']);
			
			foreach($related_quotation_item as $k => $v){
				$product_data = $this->Products_model->get($v['product_id']);
				$supplier_data = $this->Suppliers_model->get($product_data['supplier']);
				$related_quotation_item[$k]['model_no'] = $product_data['model_no'];
				$related_quotation_item[$k]['part_no'] = $product_data['part_no'];
				$related_quotation_item[$k]['supplier_id'] = $product_data['supplier'];
				$related_quotation_item[$k]['supplier_name'] = $supplier_data['company_name'];
				$related_quotation_item[$k]['supplier_pic'] = $supplier_data['contact_person'];
			}
			
		
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					'customer_data'   => $customer_data,
					'product' => $related_quotation_item,
			);
			
			
			
			echo json_encode($temp);	
			exit;
	  }	 
	  
	  public function get_paging($item_per_page,$pagenum,$total_item,$page)
	  {
	
		$start = (int)(($page-1)/$pagenum)*$pagenum+1;
		$end = $start+$pagenum-1;
		$next = $page+1;
		$pre = $page-1;
		
		$total_page = ceil( $total_item / $item_per_page );
                $paging = '';
		if($total_item > $item_per_page){
                    $paging .= '<ul class="pagination">';

                    if($page > 1){
                            $paging .= '<li><a href="javascript:changePage(1)">&laquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$pre.')">&lsaquo;</li>';
                    }

                    if($total_page <= $pagenum){

                            for($i=$start;$i<=$total_page;$i++){
                                    if($i == $page){

                                            $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                    }else{

                                            $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                    }
                            }
                    }else{
                            if($page > 5){
                                    $end = $page+5;
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    $start = $end - ($pagenum - 1);

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }else{
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }	
                    }

                    if($page < $total_page){
                            $paging .= '<li><a href="javascript:changePage('.$next.')">&rsaquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$total_page.')">&raquo;</a></li>';
                    }

                    $paging .= '</ul>';
                }
		
		return $paging;
	  }	  
	  	  
	  
	  private function PDF_generation($id, $mode='I',$type) {
		  
		  $crma_data = $this->Return_material_authorization_model->get($id);
		  $crma_related_item = $this->Return_material_item_model->get_related_item($id);
		  $customer = $this->Customers_model->get($crma_data['customer_id']);
		  
		  if($type == 1){
			  $type_title = 'Collection Information';
			  $type_name = 'Collection';
			  $date_collect = $crma_data['collection_date']!='0000-00-00 00:00:00'?date('d/m/Y',strtotime($crma_data['collection_date'])):'';
			  $special_instructions = $crma_data['collection_special_instruction'];
		  }else{
			  $type_title = 'Delivery and Return Information';
			  $type_name = 'Return';
			  $date_collect = $crma_data['return_date']!='0000-00-00 00:00:00'?date('d/m/Y',strtotime($crma_data['return_date'])):'';
			  $special_instructions = $crma_data['return_special_instruction'];
		  }
		  
		  $item_group = '';
		  foreach($crma_related_item as $k => $v){
			  $item_group .='<tr><td>'.($k+1).'</td><td>'.$v['item_name'].'</td><td>'.$v['quantity'].'</td><td>'.$v['part_number'].'</td></tr>';
		  }
		  
		  $this->load->library("Pdf");		
          $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		  
		  // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Anexus');
            $pdf->SetTitle("Return Material Authorization");
            $pdf->SetSubject("");
            $pdf->SetKeywords("");

            // remove default header/footer
            $pdf->setPrintHeader(false);			
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(10, 10, 10);

            // set auto page breaks
            //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
            }

            // ---------------------------------------------------------

            // set font
            $pdf->SetFont('times', '', 10);

            

            // set font
            $pdf->SetFont('times', '', 10);
			
			// add a page
       		$pdf->AddPage();
			
			
			$html = '			
			<h1>Return Material Authorization</h1>
			
			<table width="100%" cellpadding="5">
				<tr>
					<td><table width="100%"  border="1" cellpadding="5">
			
				<tr>
					<th><h3>Return Material Authorization Information</h3></th>
				</tr>
				<tr>
					<th><h4>Serial No.</h4></th>
				</tr>
				<tr>
					<td>'.$crma_data['return_material_serial_no'].'</td>
				</tr>
				<tr>
					<th><h4>Type of Return</h4></th>
				</tr>
				<tr>
					<td>'.$crma_data['type_of_return'].'</td>
				</tr>
				<tr>
					<th><h4>Reason for Return</h4></th>
				</tr>
				<tr>
					<td>'.$crma_data['reason_of_return'].'</td>
				</tr>

			</table></td>
					<td><table width="100%"  border="1" cellpadding="5">
			
				<tr>
					<th><h3>Customer Information</h3></th>
				</tr>
				<tr>
					<th><h4>Customer</h4></th>
				</tr>
				<tr>
					<td>'.$customer['company_name'].'</td>
				</tr>
				<tr>
					<th><h4>Name</h4></th>
				</tr>
				<tr>
					<td>'.$customer['contact_person'].'</td>
				</tr>
				<tr>
					<th><h4>Contact No.</h4></th>
				</tr>
				<tr>
					<td>'.$customer['contact_info'].'</td>
				</tr>

			</table></td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%"  border="1" cellpadding="5">			
							<tr>
								<th colspan="4"><h3>'.$type_title.'</h3></th>
							</tr>
							<tr>
								<th colspan="2"><h4>Date of '.$type_name.'</h4></th>
								<td colspan="2"><h4>Special Instructions</h4></td>
							</tr>
							<tr>
								<td colspan="2">'.$date_collect.'</td>
								<td colspan="2">'.$special_instructions.'</td>
							</tr>
							
							<tr>
								<th colspan="4"><h3>Items</h3></th>
							</tr>
							<tr>
								<td>#</td>
								<td>Item</td>
								<td>Quantity</td>
								<td>Part Number</td>
							</tr>'.$item_group.'
						</table>
					</td>
				</tr>

			</table>			
			';
			
			$pdf->writeHTML($html, true, false, true, false, '');
						
			
			$file_name = date("YmdHis").rand(1000,9999);
			
			if($mode == 'I') {
				$pdf->Output($file_name.'.pdf', 'I'); 	
				return $file_name.'.pdf';
			} else if ($mode == 'f') {				
				$pdf->Output('./uploads/'.$file_name.'.pdf', 'f'); 
				return './uploads/'.$file_name.'.pdf';
			}
			
		  		  
		  
	  }
	  
	  public function export_pdf($id,$type){		  
		  $this->PDF_generation($id, 'I',$type);			
	  }
	  
	  public function sent_mail($id,$type){
		  
		   	$filename = $this->PDF_generation($id, 'f',$type);	 		  
			
			$rma_data = $this->Return_material_authorization_model->get($id);

		  	$customer = $this->Customers_model->get($rma_data['customer_id']);
		  
		  	$send_to = $customer['email'];
			
			$sitename = $this->Settings_model->get_settings(1);
		  	$default_email = $this->Settings_model->get_settings(6);
			
			//$requestor_role 
			$requestor_employee = $this->Employee_model->get($rma_data['requestor_id']);
			$requestor_role = $requestor_employee['role_id'];
			$role_id = $this->data['userdata']['role_id'];
			
			///////////////////////////////////////////////////////////
			
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $rma_data['status'],
							'workflow_id' => 6,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(6);
						$status = json_decode($workflow['status_json'],true);
						
						if(isset($action)){

		
						if($action == 'send_email'){
							
							//next status
							$next_status = $workflow_order['next_status_id'];
							$next_status_text = $status[$workflow_order['next_status_id']];
							$who_email = $workflow_order['who_email'];
	
						}

						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];
								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 6,
							);
							
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
							
							
							$jobArray = array(
								'parent_id' 		=>$rma_data['latest_job_id'],
								'user_id' 			=>$this->data['userdata']['id'],
								'customer_id' 		=>$rma_data['customer_id'],
								'type' => 'Return Material Authorization',
								'type_path' =>'service/return_material_authorization_edit/',
								'awaiting_role_id' 	=>$awating_person.',',
								'status_id' 		=>$next_status,
								'status_text'		=>$next_status_text,
								'serial' 			=>$rma_data['return_material_serial_no'],
								'type_id' 			=>$rma_data['id'],
								'created_date'		=>date("Y-m-d H:i:s"),
								'modified_date'		=>date("Y-m-d H:i:s"),
							);			
					  
						    $array = array(
							'awaiting_table' => $awating_person.',',
							'status' => $next_status,
							'modified_date' => date('Y-m-d H:i:s'),
							'lastupdate_user_id' => $this->data['userdata']['id'],
						    );
							
						  //print_r($array);exit;
							
						
						  //更新上一個JOB的狀態					  
						  $this->Job_model->update($jobArray['parent_id'], array(
							'is_completed' => 1,
							'display' => 0,
						  ));
						  
						  //新增一個新的JOB
						  $new_job_id = $this->Job_model->insert($jobArray);
						  $array['latest_job_id'] = $new_job_id;
						  
						  $this->Return_material_authorization_model->update($rma_data['id'], $array);
							
							
						}
						
						}
			
			
			if(isset($who_email)){
			
				$this->load->library('email');
				
				switch($who_email){
					case 'customer':
							
							//Send Notification
							if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
										
								$this->email->from($default_email['value'], $sitename['value']);
								$this->email->to($send_to); 
							
								$this->email->subject('Anexus '.$workflow['title'].': '.$rma_data['return_material_serial_no']);
								$this->email->message($rma_data['return_material_serial_no']);	
								$this->email->attach($filename);
							
								$this->email->send();
							
							}
							
					break;	
					case 'supplier':
						
							  //count supplier
							  $supplier_count = $this->Delivery_order_item_model->get_supplier_groupBy($id);
							  
							  foreach($supplier_count as $k => $v){
									
								  $supplier = $this->Suppliers_model->get($v['supplier_id']);
								  
								  $file_name = $this->PDF_generation($id, 'f',$v['supplier_id']);	
										 
								  if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
									
									$this->load->library('email');
									$this->email->clear();	
									$this->email->from($default_email['value'], $sitename['value']);
									$this->email->to($supplier ['email']); 
									
									$this->email->subject('Anexus '.$workflow['title'].' request '.$rma_data['return_material_serial_no']);
									$this->email->message($rma_data['return_material_serial_no']);	
									$this->email->attach($file_name);
									
									$this->email->send();
									
								  }
							  
							  }
							
					break;
					case 'employee':
					break;
				
				}
				
			}
			
			 //mail
			 $mail_array = array(
				'creater_name' =>$this->data['userdata']['name'],
			 );
			 
			 if(isset($new_job_id)){
			  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
			  $mail_array['related_role'] = $related_role;		
			  $mail_array['title'] = $workflow['title']." ".$rma_data['return_material_serial_no']." ".$next_status_text;
			 }
			 
			 //sent Notification to related role						  			
		  	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local" && isset($mail_array['related_role'])) {
				$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
			  				  	
			  	foreach($related_role_data as $k => $v){
				  
				  	$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
					
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->send();	
				  
		 		}
			}
			
			///////////////////////////////////////////////////////////
			
			
			/*
			
		  	$this->load->library('email');
			
			//Send Notification
		  	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
		  			  	
				$this->email->from($default_email['value'], $sitename['value']);
				$this->email->to($send_to); 
			
				$this->email->subject('Anexus return material authorization '.$rma_data['return_material_serial_no']);
				$this->email->message($rma_data['return_material_serial_no']);	
				$this->email->attach($filename);
			
				$this->email->send();
			
			}
			
			define('GENERAL_MANAGER', 2);
		    define('LOGISTIC', 6);
		    define('SALES_MANAGER', 7);
		    define('SALES_EXECUTIVE', 8);	
			
			//sale service executive send_out
			if($rma_data['status'] == 4){
				  if($this->data['userdata']['role_id'] == $requestor_role){
					  			 		 
								 
					  $jobArray = array(
						'parent_id' =>$rma_data['latest_job_id'],
						'user_id' =>$this->data['userdata']['id'],
						'customer_id' =>$rma_data['customer_id'],
						'type' => 'Return Material Authorization',
						'type_path' =>'service/return_material_authorization_edit/',
						'awaiting_role_id' => $requestor_role.',',
						'status_id' => 5,
						'status_text'=> $this->data['rma_status_list'][5],
						'serial' => $rma_data['return_material_serial_no'],
			  			'type_id' => $rma_data['id'],
						'created_date'	=> date("Y-m-d H:i:s"),
						'modified_date'	=> date("Y-m-d H:i:s"),
					  );			
					  
					  $array = array(
					  	'awaiting_table' => $requestor_role,
						'status' => 5,
					  );
					  

					  //更新上一個JOB的狀態					  
					  $this->Job_model->update($jobArray['parent_id'], array(
					  	'is_completed' => 1,
					  ));
					  
					  //新增一個新的JOB
					  $new_job_id = $this->Job_model->insert($jobArray);
					  $array['latest_job_id'] = $new_job_id;
					  
					  $this->Return_material_authorization_model->update($rma_data['id'], $array);
				  }
				  
			}
			
			//mail
			$mail_array = array(
				'creater_name' =>$this->data['userdata']['name'],
				'title' => $rma_data['return_material_serial_no'].' '.$this->data['rma_status_list'][5],
				'related_role' => array($requestor_role),
			 );
					
				  
	  
			//sent Notification to related role						  			
		  	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
				$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
			  				  	
			  	foreach($related_role_data as $k => $v){
				  
				  	$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
					
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->send();	
				  
		 		}
			}
			*/
			
			$lastpage = $this->session->userdata("lastpage");
			if(!empty($lastpage)) {
			  redirect($lastpage,'refresh');  
			} else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
			}		 			
		  
	  }
	  

}

?>