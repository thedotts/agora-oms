<?php

class Purchase_request_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();  
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Customers_model');
			$this->load->model('Purchase_request_model');
			$this->load->model('Purchase_request_item_model');
			$this->load->model('Quotation_model');
			$this->load->model('Employee_model');
			$this->load->model('Department_model');
			$this->load->model('Quotation_item_model');
			$this->load->model('Products_model');
			$this->load->model('Suppliers_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Settings_model');
			$this->load->model('Workflow_model');
			$this->load->model('Workflow_order_model');

			$this->data['employee_list'] = $this->Employee_model->getIDKeyArray('full_name');
			$this->data['role_list'] = $this->Role_model->getIDKeyArray('name');
			$pr_status_list = $this->Workflow_model->get(2);
			$this->data['pr_status_list'] = json_decode($pr_status_list['status_json']);
			$this->data['status_list'] = $this->Quotation_model->status_list();
            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$this->data['userdata'] = $this->session->userdata("userdata");
			} else {
				redirect(base_url(),'refresh');	
			}
			
			$this->data['group_name'] = "sales";  
			$this->data['model_name'] = "purchase";  
			$this->data['common_name'] = "Purchase Request";   
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);  
			
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}
			
			define('SUPERADMIN', 1);
			define('GENERAL_MANAGER', 2);
		    define('FINANCE', 5);
			define('LOGISTIC', 6);
		    define('SALES_MANAGER', 7);
		    define('SALES_EXECUTIVE', 8);	
			define('PURCHASER', 10);	
			
           
      }
   
      public function index($q="ALL", $status="ALL", $page=1) {  
          			
            $this->data['title'] = ucfirst("Purchase Request"); // Capitalize the first letter	
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/anexus/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/'.$status.'/';
			if($q == 'ALL') {
				$q = "";
			}
			$this->data['q'] = $q;
			
			if($status != 'ALL') {
				$filter['status'] = $status;	
			}
			$this->data['status'] = $status;			
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			
			$this->data["total"] = $this->Purchase_request_model->record_count($filter, $q);
			
			//get particular ranged list
			$this->data['results'] = $this->Purchase_request_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start);
			
			if(!empty($this->data['results'])){
				foreach($this->data['results'] as $k => $v){
					if(!empty($v['awaiting_table'])){
							$this->data['results'][$k]['awaiting_table']= explode(',',$v['awaiting_table']);
					}
				}
			}
			
			
			//print_r($this->data['status_list']);exit;	
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
			
			$this->session->set_userdata("lastpage", $url.$page);
				
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  
	  public function add($id=false,$job_id=false) {
			
			$role_id = $this->data['userdata']['role_id'];
			
		    //for logistic use
			$supplier = $this->Products_model->getAll_groupby();
			$supplier_id = array();
			foreach($supplier as $k => $v){
				$supplier_id[] = $v['supplier'];
			}
			
			$this->data['supplier_list'] = $this->Suppliers_model->getSupplierList($supplier_id);
			//print_r($this->data['supplier_list']);exit;
			
			if($id !== false && $job_id === false){
				
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Purchase_request_model->get($id);
				$this->data['department_list'] = $this->Department_model->getIDKeyArray("name");
				$this->data['product_list'] = $this->Products_model->getIDKeyArray("model_no");
				
				$this->data['pr_data'] = $this->Purchase_request_model->get($id);
				$this->data['originator'] = $this->Employee_model->get($this->data['pr_data']['requestor_id']);
				$this->data['customer'] = $this->Customers_model->get($this->data['pr_data']['customer_id']);
				
				
				
				$requestor_employee = $this->Employee_model->get($this->data['result']['requestor_id']);
			    $requestor_role = $requestor_employee['role_id'];
				$this->data['requestor_role'] = $requestor_role;
				
				//print_r($this->data['result']['requestor_id']);exit;
				$this->data['result']['awaiting_table'] = explode(',',$this->data['result']['awaiting_table']);
				//print_r($this->data['result']);exit;
				$default = 0;
				
				//test data
				//$this->data['result']['status'] = 6;
				//$this->data['requestor_role'] = 8;
				//$this->data['userdata']['role_id'] = 6;
				/*
				//sales manager
				if($this->data['result']['status'] == 1){
					
					if($role_id == 7){
					$this->data['head_title'] = 'Confirm Purchase Order';
					}else if($role_id == 8){
					$this->data['head_title'] = 'Edit Purchase Request';
					}else{
					$default = 1;
					}
				//gm
				}else if($this->data['result']['status'] == 2){
					if($role_id == 6 || $role_id == 7 || $role_id == 10){
					$this->data['head_title'] = 'Edit Purchase Request';
					}else if($role_id == 2){
					$this->data['head_title'] = 'Confirm Purchase Order';	
					}else{
					$default = 1;
					}
				//finance
				}else if($this->data['result']['status'] == 3){
					if($role_id == 5){
					$this->data['head_title'] = 'Confirm Purchase Order';
					}else{
					$default = 1;
					}
				//logistic
				}else if($this->data['result']['status'] == 4){
					if($role_id == 6){
					$this->data['head_title'] = 'Confirm Purchase Order';
					}else{
					$default = 1;
					}
				//logistic send email to supplier
				}else if($this->data['result']['status'] == 5){
					if($role_id == 6){
					$this->data['head_title'] = 'Send Purchase Order';
					
					//count supplier
					$supplier_count = $this->Purchase_request_item_model->get_supplier_groupBy($id);
					
					foreach($supplier_count as $k => $v){
						$companyName_tmp = $this->Suppliers_model->get($v['supplier_id']);
						$supplier_count[$k]['company_name'] = $companyName_tmp['company_name'];
					}
					
					$this->data['supplier_count'] = $supplier_count;
					//print_r($this->data['supplier_count']);exit;
					
					}else{
					$default = 1;
					}
				//logistic update qty	
				}else if($this->data['result']['status'] == 6){
					if($role_id == 6){
					$this->data['head_title'] = 'Confirm In-Delivery';
					
					//count supplier
					$supplier_count = $this->Purchase_request_item_model->get_supplier_groupBy($id);
					
					foreach($supplier_count as $k => $v){
						$companyName_tmp = $this->Suppliers_model->get($v['supplier_id']);
						$supplier_count[$k]['company_name'] = $companyName_tmp['company_name'];
					}
					
					$this->data['supplier_count'] = $supplier_count;
					
					}else{
					$default = 1;
					}
				//upload confirm file	
				}else if($this->data['result']['status'] == 7){
					if($role_id == $requestor_role){
					$this->data['head_title'] = 'Confirm Purchase Order';
					
					//count supplier
					$supplier_count = $this->Purchase_request_item_model->get_supplier_groupBy($id);
					
					foreach($supplier_count as $k => $v){
						$companyName_tmp = $this->Suppliers_model->get($v['supplier_id']);
						$supplier_count[$k]['company_name'] = $companyName_tmp['company_name'];
					}
					
					$this->data['supplier_count'] = $supplier_count;
					
					}else{
					$default = 1;
					}
				//reupload confirm file	
				}else if($this->data['result']['status'] == 8){
					if($role_id == $requestor_role){
					$this->data['head_title'] = 'Confirm Purchase Order';
					
					//count supplier
					$supplier_count = $this->Purchase_request_item_model->get_supplier_groupBy($id);
					
					foreach($supplier_count as $k => $v){
						$companyName_tmp = $this->Suppliers_model->get($v['supplier_id']);
						$supplier_count[$k]['company_name'] = $companyName_tmp['company_name'];
					}
					
					$this->data['supplier_count'] = $supplier_count;
					
					}else{
					$default = 1;
					}
				}else{
					$default = 1;
				}
				//no related person
				if($default == 1){
					$this->data['head_title'] = 'View Purchase Order';	
				}
				*/
				
				//////////////////////////////////////////////////////////////////////////////////
				
				   		//get related workflow order data
						$workflow_flitter = array(
							'status_id' => $this->data['result']['status'],
							'workflow_id' => 2,
						);
						
						$type='';
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$type = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$type = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
							$type = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
									$type = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(2);
						$status = json_decode($workflow['status_json'],true);
						
						$this->data['workflow_title'] = $workflow['title'];
					
					$this->data['btn_type'] = $type;
					//print_r($workflow_order);exit;

					switch($type){
						case 'edit':
							$this->data['head_title'] = 'Edit '.$workflow['title'].' Request';
						break;
						case 'approval':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
						break;
						case 'approval_update':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'update':
							$this->data['head_title'] = 'Update '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'send_email':
							$this->data['head_title'] = 'Waiting to Send '.$workflow['title'].' Request';
						break;
						case 'upload':
							$this->data['head_title'] = 'Upload Confirm File '.$workflow['title'].' Request';
						break;
						case 'complete':
							$this->data['head_title'] = $workflow['title'].' Request';
							$this->data['last_action'] = $workflow_order['action_before_complete'];
						break;
						default:
							$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					foreach(json_decode($workflow['status_json'],true) as $k => $v){
						
						if($v == 'Completed'){
							$this->data['completed_status_id'] = $k;
							break;
						}
					}
				//////////////////////////////////////////////////////////////////////////////////////
				
				$related_product = $this->Purchase_request_item_model->getRelatedProduct($id);
				
				$this->data['related_product'] = array();
				
				//item page add
				foreach($related_product as $k => $v){
					
					if(isset($this->data['related_product'][$v['supplier_id']])){
						$this->data['related_product'][$v['supplier_id']][] = $v;
					}else{
						$this->data['related_product'][$v['supplier_id']] = array();
						$this->data['related_product'][$v['supplier_id']][] = $v;
					}
				}
				
				//item page add2
				foreach($related_product as $k => $v){
					
					$product = $this->Products_model->get($v['product_id']);
					$related_product[$k]['item_desc'] = $product['description'];
					
				}
				$this->data['related_product2'] = $related_product;
				
				//print_r($related_product);exit;
	
				
			} else {
				$this->data['mode'] = 'Add';
				$this->data['term'] = $this->Settings_model->get_settings(11);
				
				//get related workflow order data
					$workflow_flitter = array(
						'role_id' => $role_id,
						'status_id' => -1,
						'workflow_id' => 2,
					);
					
					//data form database
					$workflow = $this->Workflow_model->get(2);
					$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
					
					$this->data['workflow_title'] = $workflow['title'];
					
					//get view bottom layout
					$type = '';
					if(!empty($workflow_order)){
						$type = $workflow_order['action'];
						if($workflow_order['action'] == 'create'){
							$this->data['head_title'] = 'Create '.$workflow['title'].' Request';
						}else{
							$this->data['head_title'] = $workflow['title'].' Request';
						}
					}else{
						$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					$this->data['btn_type'] = $type;
				
					//auto select quotation id
					$this->data['qr_id'] = 0;
					if($id !== false){
					$this->data['qr_id'] = $id;
					}
					//for complete the job
					$this->data['job_id'] = $job_id;
					
			}
			
			$this->data['currency_list']=$this->Customers_model->currency_list();
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			$department_data = $this->Department_model->get($this->data['staff_info']['department_id']);
			$this->data['staff_info']['department_name'] = isset($department_data['name'])?$department_data['name']:'';
			$this->data['staff_json'] = json_encode($this->data['staff_info']);
			$this->data['company_list'] = json_encode($this->Customers_model->getAll());
			
          	$this->load->view('anexus/header', $this->data);
		
			if($this->data['mode'] == 'Add'){
				
				//Purchaser有不同的新增介面
				if($role_id == PURCHASER | $role_id == LOGISTIC) {
					$this->load->view('anexus/'.$this->data['model_name'].'/add_adhoc', $this->data);
				} else {				
					$this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
				}

			}else{
				if($this->data['result']['status'] > 2){
				$this->load->view('anexus/'.$this->data['model_name'].'/add2', $this->data);
				}else{
				$this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
				}	
			}
			
            
            $this->load->view('anexus/footer', $this->data);		
            
			
      }
	  
	  public function submit(){
		  
		  	//print_r($_POST);exit;
		  	
		  	$now = date("Y-m-d H:i:s");
		  	$mode = $this->input->post("mode", true);
		 	$id = $this->input->post("id", true);
			
			$role_id = $this->data['userdata']['role_id'];
			
			//get value approve or not approve
		    $confirm = $this->input->post("confirm", true);
		    $correct_check = $this->input->post("correct_check", true);
			//echo $correct_check;exit;
			
			$dateRequisition = $this->input->post("dateRequisition", true);
			if(!empty($dateRequisition)) {
			  $tmp = explode("/", $dateRequisition);
			  $dateRequisition = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
		  	}else{
			  $dateRequisition = '';
			}
				
				
		  if($mode == 'Add'){
			  
			  $customer_id = $this->input->post("customer_id", true);
			  $requestor_id = $this->input->post("requestorId", true);
			  
		  }else{
		  
			  //quota
			  $pr_data = $this->Purchase_request_model->get($id);
		  	  $customer_id = $pr_data['customer_id'];
			  $requestor_id = $pr_data['requestor_id'];
			  
		  }	
			
					
			//job
		  	$jobArray = array(
				'parent_id' =>0,
				'user_id' =>$this->data['userdata']['id'],
				'customer_id' =>$customer_id,
				'type' => 'Purchase Request',
				'type_path' =>'sales/purchase_edit/',
		 	 );
			 
			//main		
			$array = array(
				'quotation_id' => $this->input->post("quotation_id", true),
				'customer_id' => $customer_id,
				'customer_name' => $this->input->post("customer_name", true),
				'requesting_date' => $dateRequisition,
				'purchase_order_ref' => $this->input->post("purchase_order_ref", true),
				'sale_order_ref' => $this->input->post("salesOrderRef", true),
				'requestor_id' => $requestor_id,
				'employee_ref' => $this->input->post("employeeRef", true),
				'department_id' => $this->input->post("departmentId", true),
				'service_request_form' => $this->input->post("service-request", true),
				'consignee' => $this->input->post("consignee", true),
				'uncrating_service_required' => $this->input->post("uncrating", true),
				'reference_document' => $this->input->post("originalFile", true),
				'remark'	=> $this->input->post("remark", true),
				'term_condition'	=> $this->input->post("term_condition", true),
			);
			
			//print_r($array);exit;
			
			 
			//mail
			$mail_array = array(
		  	'creater_name' =>$this->data['userdata']['name'],
		  	);
			
			
			if(isset($_FILES['uploadFile'])){
				
				if ( $_FILES['uploadFile']['error'] == 0 && $_FILES['uploadFile']['name'] != ''  ){									
						$pathinfo = pathinfo($_FILES['uploadFile']['name']);
						$ext = $pathinfo['extension'];
						$ext = strtolower($ext);
											
						$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
						$path = "./uploads/".$filename.'.'.$ext;
						$save_path = base_url()."uploads/".$filename.'.'.$ext;
						$result = move_uploaded_file($_FILES['uploadFile']['tmp_name'], $path);
						if(!$result) {
							die("cannot upload file");
						}	
					
						$array['reference_document']= $save_path;															
				}	
			}
			
			
			//新增
			if($mode == 'Add') {
				
				////////////////////////////////////////////////////////////////////////////////////
				
				//get related workflow order data
				$workflow_flitter = array(
					'role_id' => $role_id,
					'status_id' => -1,
					'workflow_id' => 2,
				);
				
				//data form database
				$workflow = $this->Workflow_model->get(1);
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
				$status = json_decode($workflow['status_json'],true);
				
				if(!empty($workflow_order)){
				
				//ad_hoc
				if($workflow_order['ad_hoc'] == 1){
					$array['ad_hoc'] = 1;
				}
				
				//purchaser
				$is_adhoc = false;
				if($role_id == 10){
					$is_adhoc = true;
				}
				
				//next status
				$next_status = $workflow_order['next_status_id'];
				$next_status_text = $status[$workflow_order['next_status_id']];
				
			  	//if need_det = 1
				if($workflow_order['need_det'] == 1){
					
					$formula = json_decode($workflow_order['formula'],true);
					
					//check if product or service price over value
					foreach($formula as $k => $v){
					
						$price = $this->input->post($v['target_colum'], true);
						$det_temp = $price." ".$v['logic']." ".$v['value'];
						$check = $this->parse_boolean($det_temp);
						
						//check price over value
						if($check){
							$next_status = $v['next_status_id'];
							$next_status_text = $status[$v['next_status_id']];
							break;
						}
						
					}
					
				}
				
				//get next status workflow order data
				$next_workflow_flitter = array(
					'status_id' => $next_status,
					'workflow_id' => 2,
				);
				
				//next status data
				$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
				$awating_person = $next_status_data['role_id'];
				
				//main
				$array['awaiting_table']  = $awating_person;
				$array['status'] = $next_status;
				//main
				$jobArray['awaiting_role_id'] = $awating_person.',';
				$jobArray['status_id'] = $next_status;
				$jobArray['status_text'] = $next_status_text; 	
				  
				  //insert main data
					$array['created_date'] = $now;
					$array['modified_date'] = $now;
					$array['create_user_id'] = $this->data['userdata']['id'];
					$array['lastupdate_user_id'] = $this->data['userdata']['id'];

					//新增資料
					$insert_id = $this->Purchase_request_model->insert($array);				
					
					//更新purchase serial
					$purchase_serial = $this->Purchase_request_model->zerofill($insert_id, 5, $is_adhoc);
					
					//insert job
					$jobArray['serial'] = $purchase_serial;
					$jobArray['type_id'] = $insert_id;
					
					$jobArray['created_date'] = $now;
					$jobArray['modified_date'] = $now;
					
					$job_id = $this->Job_model->insert($jobArray);
				  
					//after insert job,update job id,qr serial....
					$array =array(
						'job_id'	=> $job_id,
						'purchase_serial'  => $purchase_serial,
						'latest_job_id'	=> $job_id,
					);
					$this->Purchase_request_model->update($insert_id, $array);
					
				  //mail
				  $related_role = $this->Job_model->getRelatedRoleID($job_id);
				  $mail_array['related_role'] = $related_role;		
				  $mail_array['title'] = $workflow['title']." ".$purchase_serial." ".$next_status_text; 
				  
				  //task of auto create job quotation completed if job id != ''
				  $task_qr_job_id = $this->input->post('job_id',true);
				  if($task_qr_job_id != ''){
				  $this->Job_model->update($task_qr_job_id, array(
				  	'is_completed' => 1,
					'display' => 0));
				  }
				  	
				}
				
				////////////////////////////////////////////////////////////////////////////////////
				
				/*
				$is_adhoc = false;
				if($role_id == SALES_EXECUTIVE){
					
					//main
					$array['awaiting_table']  = SALES_MANAGER;
					$array['status']  = 1;
					//job
					$jobArray['awaiting_role_id'] = SALES_MANAGER.',';
					$jobArray['status_id'] = 1;
					$jobArray['status_text'] = $this->data['pr_status_list'][1];
					//mail
					$mail_array['related_role'] = array(SALES_EXECUTIVE,SALES_MANAGER);
					
				//SM建立交給GM去審核	
				} else if($role_id == SALES_MANAGER){
					
					//main
					$array['awaiting_table']  = GENERAL_MANAGER;
					$array['status']  = 2;
					//job
					$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
					$jobArray['status_id'] = 2;
					$jobArray['status_text'] = $this->data['pr_status_list'][2];
					//mail
					$mail_array['related_role'] = array(SALES_MANAGER,GENERAL_MANAGER);
					
				//LOGISTIC 建立交給GM去審核	(ADHOC)
			  	} else if ($role_id == LOGISTIC){
					
					//main
					$array['awaiting_table']  = GENERAL_MANAGER;
					$array['status']  = 2;
					$array['ad_hoc']  = 1;
					//job
					$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
					$jobArray['status_id'] = 2;
					$jobArray['status_text'] = $this->data['pr_status_list'][2];
					//mail
					$mail_array['related_role'] = array(LOGISTIC,GENERAL_MANAGER);
				
				//PURCHASER 建立交給GM審核 (ADHOC)	
				} else if ($role_id == PURCHASER){
					
					//main
					$array['awaiting_table']  = GENERAL_MANAGER;
					$array['status']  = 2;
					$array['ad_hoc']  = 1;
					$is_adhoc = true;
					//job
					$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
					$jobArray['status_id'] = 2;
					$jobArray['status_text'] = $this->data['pr_status_list'][2];
					//mail
					$mail_array['related_role'] = array(PURCHASER,LOGISTIC,GENERAL_MANAGER);
					
				}
				
				$mail_array['title'] = 'create purchase request';
				
				//insert main data
				$array['created_date'] = $now;
				$array['modified_date'] = $now;
				$array['create_user_id'] = $this->data['userdata']['id'];
				$array['lastupdate_user_id'] = $this->data['userdata']['id'];
				//print_r($array);exit;
				//新增資料
				$insert_id = $this->Purchase_request_model->insert($array);				
				
				//更新purchase serial
				$purchase_serial = $this->Purchase_request_model->zerofill($insert_id, 5, $is_adhoc);
				
				//insert job
				$jobArray['serial'] = $purchase_serial;
			    $jobArray['type_id'] = $insert_id;
				
				$jobArray['created_date'] = $now;
				$jobArray['modified_date'] = $now;
				
			    $job_id = $this->Job_model->insert($jobArray);
			  
			    //after insert job,update job id,qr serial....
			    $array =array(
			  		'job_id'	=> $job_id,
			  		'purchase_serial'  => $purchase_serial,
					'latest_job_id'	=> $job_id,
			    );
			    $this->Purchase_request_model->update($insert_id, $array);
				*/	
				
			//Edit	
			}else{
								
				
				$pr_data = $this->Purchase_request_model->get($id);
			 	$requestor_employee = $this->Employee_model->get($pr_data['requestor_id']);
			  	$requestor_role = $requestor_employee['role_id'];
			  	$pr_status = $pr_data['status'];
			  
			  	$jobArray['serial'] =$pr_data['purchase_serial'];
			  	$jobArray['type_id'] =$pr_data['id'];
			  	$jobArray['parent_id'] = $pr_data['latest_job_id'];
				$jobArray['created_date'] = $now;
				$jobArray['modified_date'] = $now;
				
				$next = 0;
			  	$last = 0;
			  	$status_change = 0;
				
				/*
				//被 SM 或 GM同意之後才能夠更新以下欄位
				if($pr_status > 2){
					
					$created_date = $this->input->post("created_date", true);
					if(!empty($created_date)) {
					  $tmp = explode("/", $created_date);
					  $created_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
					}
					
					$array = array(
						'created_date' => $created_date,
						'ot_company_name' => $this->input->post("ot_company_name", true),
						'ot_address' => $this->input->post("ot_address", true),
						'ot_postal' => $this->input->post("ot_postal", true),
						'ot_attention' => $this->input->post("ot_attention", true),
						'ot_cc' => $this->input->post("ot_cc", true),
						'ot_contact' => $this->input->post("ot_contact", true),
						'bt_company_name' => $this->input->post("bt_company_name", true),
						'bt_address' => $this->input->post("bt_address", true),
						'bt_postal' => $this->input->post("bt_postal", true),
						'bt_attention' => $this->input->post("bt_attention", true),
						'bt_cc' => $this->input->post("bt_cc", true),
						'bt_contact' => $this->input->post("bt_contact", true),
						'bt_email' => $this->input->post("bt_email", true),
						'st_company_name' => $this->input->post("st_name", true),
						'st_address' => $this->input->post("st_address", true),
						'st_postal' => $this->input->post("st_postal", true),
						'st_attention' => $this->input->post("st_attention", true),
						'st_cc' => $this->input->post("st_cc", true),
						'st_contact' => $this->input->post("st_contact", true),
						'st_email' => $this->input->post("st_email", true),						
					);
					
				}
				*/
				
				/////////////////////////////////////////////////////////////////////////////////
			  
			  //can edit status
			  $edit_status = $this->Workflow_order_model->get_status_edit(2);
			  
			  //requestor edit
			  if(in_array($pr_status, $edit_status) && $role_id == $requestor_role){
			  	  
						//get related workflow order data
						$workflow_flitter = array(
							'role_id' => $role_id,
							'status_id' => -1,
							'workflow_id' => 2,
						);
						
						//data form database
						$workflow = $this->Workflow_model->get(2);
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						$status = json_decode($workflow['status_json'],true);
						
						if(!empty($workflow_order)){
						
						//next status
						$next_status = $workflow_order['next_status_id'];
						$next_status_text = $status[$workflow_order['next_status_id']];
						
						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];
								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						//get next status workflow order data
						$next_workflow_flitter = array(
							'status_id' => $next_status,
							'workflow_id' => 2,
						);
						
						//next status data
						$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
						$awating_person = $next_status_data['role_id'];
						
						//if awating person equal requestor ,asign requestor for it
						if($awating_person == 'requestor'){
							$awating_person = $requestor_role;
						}
						
						//main
						$array['awaiting_table']  = $awating_person;
						$array['status'] = $next_status;
						
						//job
						$jobArray['awaiting_role_id'] = $awating_person.',';
						$jobArray['status_id'] = $next_status;
						$jobArray['status_text'] = $next_status_text;
						
						
						}
						
				  
			  }else{
				  		
												
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $pr_status,
							'workflow_id' => 2,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(2);
						$status = json_decode($workflow['status_json'],true);
						
						//print_r($workflow_order);exit;
						if(isset($action)){
						
						//action
						switch ($action) {
							case 'approval':
							
								//clear data infront
								$array = array();
								
								//approved
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];
										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'update':
								
								//next status
								$next_status = $workflow_order['next_status_id'];
								$next_status_text = $status[$workflow_order['next_status_id']];
								
								$array = array(
								);
								//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
								$next = 1;
								
								break;
							case 'approval_update':
								//clear data infront
								$array = array();
								
								if($confirm == 1){
									
									if($correct_check == 'on'){
										
										//next status
										$next_status = $workflow_order['next_status_id'];
										$next_status_text = $status[$workflow_order['next_status_id']];
										
										$target_colum = json_decode($workflow_order['target_colum'],true);
										
										//check update colum single or multiple
										$array = array();
										foreach($target_colum as $k => $v){
										
											if($v['type'] == 'single'){
												
												if($v['data_type'] == 'string'){
													
												$array[$v['name']] = $this->input->post($v['name'],true);
												
												}else if($v['data_type'] == 'date'){
													
													$date = $this->input->post($v['name'], true);
													if(!empty($date)) {
													  $tmp = explode("/", $date);
													  $date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
													  $array[$v['name']] = $date;
													}
													
												}
											
											}else{
												
											}
											
										}
										$next = 1;
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
									}
									
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
							case 'upload':
							
								//clear data infront
								$array = array();
								
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'confirm_file'	=> $save_path,
											);	
											
											//next status
											$next_status = $workflow_order['next_status_id'];
											$next_status_text = $status[$workflow_order['next_status_id']];	
											$next = 1;
											//check last job
											if($workflow_order['next_status_text'] == 'Completed'){
												$last = 1;
											}												
									}	
								}
								
								break;	
							case 'complete':
							
								//clear data infront
								$array = array();
							
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'confirm_file'	=> $save_path,
											);	
										
									}	
								}
								
								break;	
								
						}
						
						//print_r($workflow_order);exit;
						//if need_det = 1
						
						if($workflow_order['need_det'] == 1){

							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$value = $this->input->post($v['target_colum'], true);
								//string or number
								if(is_numeric($v['value'])){
									$det_temp = $value." ".$v['logic']." ".$v['value'];
								}else{
									
									if($v['value'] == 'original'){
										$string = $pr_data['term_condition'];
									}else{
										$string = $v['value'];
									}
									
									$det_temp = "'".md5(trim($value))."' ".$v['logic']." '".md5(trim($string))."'";
								}
								
								
								$check = $this->parse_boolean($det_temp);
								
								//check
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
							}
							
						}

						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 2,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
						}
						
						}
				  
			  }
			  
			 	if(isset($next_status)){
					//main
					$array['awaiting_table']  = $awating_person;
					$array['status'] = $next_status;
					//job
					$jobArray['awaiting_role_id'] = $awating_person.',';
					$jobArray['status_id'] = $next_status;
					$jobArray['status_text'] = $next_status_text;
				}
				//print_r($array);exit;
			  //////////////////////////////////////////////////////////////////////////////////////
				
				/*
				//判断status 给予下个job
				//代表 waiting approval from Sales Manager
				if($pr_status == 1){										
					
					//sale executive edit	
					if($role_id == SALES_EXECUTIVE){
						
						$array['awaiting_table']  = SALES_MANAGER;
						$array['status']  = 1;
						//job
						$jobArray['awaiting_role_id'] = SALES_MANAGER.',';
						$jobArray['status_id'] = 1;
						$jobArray['status_text'] = $this->data['pr_status_list'][1];
						//mail
						$mail_array['related_role'] = $related_roles;
						$mail_array['title'] = $pr_data['purchase_serial'].' '.$this->data['pr_status_list'][1];
						
					//sale manager approved/denied
					}else if($role_id == SALES_MANAGER){
						
						//sales manager denied
						if($confirm == 0){
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	//mail
							$mail_array['related_role'] = $related_roles;
							$mail_array['title'] = $pr_data['purchase_serial'].' '.$this->data['pr_status_list'][0];
						  
						  	$next = 1;
						  	$last = 1;	
						//sales maneger approved
						} else if($correct_check == 'on'){
							
							//check if the term condition has deviation => give GM
							if(md5(trim($pr_data['term_condition'])) != md5(trim($array['term_condition']))) {
								
								$array['awaiting_table']  = GENERAL_MANAGER;
								$array['status']  = 2;
								$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
								$jobArray['status_id'] = 2;
								$jobArray['status_text'] = $this->data['pr_status_list'][2];
							
								//mail
								$mail_array['related_role'] = $related_roles;
								$mail_array['title'] = $pr_data['purchase_serial'].' '.$this->data['pr_status_list'][2];								
								
							//nothing change => give finance
							} else {
								
								$array['awaiting_table']  = FINANCE;
								$array['status']  = 3;
								$jobArray['awaiting_role_id'] = FINANCE.',';
								$jobArray['status_id'] = 3;
								$jobArray['status_text'] = $this->data['pr_status_list'][3];
							
								//mail
								$mail_array['related_role'] = $related_roles;
								$mail_array['title'] = $pr_data['purchase_serial'].' '.$this->data['pr_status_list'][3];								
								
							}
							$next = 1;							
						
						} 
						
					}
				//waiting approval from GM	
				}else if ($pr_status == 2){
					
					//GM denied
					if($confirm == 0){
						$array['awaiting_table']  = '';
					  	$array['status']  = 0;
						  
					  	//mail
						$mail_array['related_role'] = $related_roles;
						$mail_array['title'] = $pr_data['purchase_serial'].' '.$this->data['pr_status_list'][0];
						  
						$next = 1;
						$last = 1;	
					//GM approved
					} else if($correct_check == 'on'){
							
						$array['awaiting_table']  = FINANCE;
						$array['status']  = 3;
						$jobArray['awaiting_role_id'] = FINANCE.',';
						$jobArray['status_id'] = 3;
						$jobArray['status_text'] = $this->data['pr_status_list'][3];
						
						//mail
						$mail_array['related_role'] = $related_roles;
						$mail_array['title'] = $pr_data['purchase_serial'].' '.$this->data['pr_status_list'][3];
						
						$next = 1;							
						
					}						
					
				//finance approved/denied	
				}else if($pr_status == 3){
					
					if($role_id == FINANCE) {						
						//finance denied
						if($confirm == 0){
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	//mail
							$mail_array['related_role'] = $related_roles;
							$mail_array['title'] = $pr_data['purchase_serial'].' '.$this->data['pr_status_list'][0];
						  
						  	$next = 1;
						  	$last = 1;	
						//finance approved	
						} else if($correct_check == 'on'){
							
							$array['awaiting_table']  = LOGISTIC;
							$array['status']  = 4;
							$jobArray['awaiting_role_id'] = LOGISTIC.',';
							$jobArray['status_id'] = 4;
							$jobArray['status_text'] = $this->data['pr_status_list'][4];
							
							//mail
							$mail_array['related_role'] = $related_roles;
							$mail_array['title'] = $pr_data['purchase_serial'].' '.$this->data['pr_status_list'][4];
							
							$next = 1;
							
						
						}
					}
					
					
				//logistic approved/denied
				}else if($pr_status == 4){
					
					if($role_id == LOGISTIC) {
						//logistic denied
						if($confirm == 0){
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
							
							//mail
							$mail_array['related_role'] = $related_roles;
							$mail_array['title'] = $pr_data['purchase_serial'].' '.$this->data['pr_status_list'][0];
						  
						  	$next = 1;
						  	$last = 1;	
						//logistic approved	
						} else if($correct_check == 'on'){
							
							$array['awaiting_table']  = LOGISTIC;
							$array['status']  = 5;
							$jobArray['awaiting_role_id'] = LOGISTIC.',';
							$jobArray['status_id'] = 5;
							$jobArray['status_text'] = $this->data['pr_status_list'][5];
							
							//mail
							$mail_array['related_role'] = $related_roles;
							$mail_array['title'] = $pr_data['purchase_serial'].' '.$this->data['pr_status_list'][5];
							
							$next = 1;
							
						}
					}
				
				//logistic send mail
				}else if($pr_status == 5){
					
					//This part will be written on "sent_mail" method					
						
				//logistic update qty
				}else if($pr_status == 6){
					
					if($role_id == LOGISTIC) {	
						//當初是甚麼角色建立的就由他來上傳confirm doc
						$array['awaiting_table']  = $requestor_role;												
						$array['status']  = 7;
						$jobArray['awaiting_role_id'] = $requestor_role.',';
						$jobArray['status_id'] = 7;
						$jobArray['status_text'] = $this->data['pr_status_list'][7];
												
					    //mail
					    $mail_array['related_role'] = $related_roles;
					    $mail_array['title'] = $pr_data['purchase_serial'].' '.$this->data['pr_status_list'][7];
							    
					    $next = 1;
					}
						
				//requestor role upload confirmation		
				} else if($pr_status == 7){
					
					if($role_id == $requestor_role){
						
						if(isset($_FILES['confirm_file'])){
				
							if ( $_FILES['confirm_file']['error'] == 0 && $_FILES['confirm_file']['name'] != ''  ){									
									$pathinfo = pathinfo($_FILES['confirm_file']['name']);
									$ext = $pathinfo['extension'];
									$ext = strtolower($ext);
														
									$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
									$path = "./uploads/".$filename.'.'.$ext;
									$save_path = base_url()."uploads/".$filename.'.'.$ext;
									$result = move_uploaded_file($_FILES['confirm_file']['tmp_name'], $path);
									if(!$result) {
										die("cannot upload file");
									}	
								
									$array['confirm_file']= $save_path;	
									$array['awaiting_table']  = "";
									$array['status']  = 8;		
									
									$mail_array['related_role'] = $related_roles;
					    			$mail_array['title'] = $pr_data['purchase_serial'].' '.$this->data['pr_status_list'][7];
									
									$next = 1;
								    $last = 1;											
							}	
					    }
						
						
						
						
					}
					
				//requestor role upload confirmation		
				} else if($pr_status == 8){
					
					if($role_id == $requestor_role){
						
						if(isset($_FILES['confirm_file'])){
				
							if ( $_FILES['confirm_file']['error'] == 0 && $_FILES['confirm_file']['name'] != ''  ){									
									$pathinfo = pathinfo($_FILES['confirm_file']['name']);
									$ext = $pathinfo['extension'];
									$ext = strtolower($ext);
														
									$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
									$path = "./uploads/".$filename.'.'.$ext;
									$save_path = base_url()."uploads/".$filename.'.'.$ext;
									$result = move_uploaded_file($_FILES['confirm_file']['tmp_name'], $path);
									if(!$result) {
										die("cannot upload file");
									}	
								
									$array = array(
										'confirm_file'=> $save_path,	
									);
																			
							}	
					    }
						
						
						
					}
					
				}
				*/
				
				//print_r($array);exit;
				
				$array['modified_date'] = $now;
				$array['lastupdate_user_id'] = $this->data['userdata']['id'];
				
				if($next == 1){
					
				  $this->Job_model->update($jobArray['parent_id'], array(
				  	'is_completed' => 1,
					'display' => 0,
				  ));
				  if($last == 0){
				  	$new_job_id = $this->Job_model->insert($jobArray);
				  	$array['latest_job_id'] = $new_job_id;
				  }
			  	}
				//print_r($array);
				$this->Purchase_request_model->update($id, $array);
				$insert_id = $id;

				//echo $this->db->last_query();
				//print_r($array);exit;
				
				//mail
				  if(isset($new_job_id)){
				  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
				  $mail_array['related_role'] = $related_role;		
				  $mail_array['title'] = $workflow['title']." ".$pr_data['purchase_serial']." ".$next_status_text;
				  }
				
			}
			
			$item_type = 0;
			if($mode == 'Add'){
				
				$item_type = 1;
			
			}else if($mode == 'Edit'){
				
				if($pr_status > 2 && $pr_status != 0){
					$item_type = 2;
				}
					
			}
			
			
			//暂时先写死 日后如有需要再自动化
			//item for add 1(supplier 给予company name)
			if($item_type == 1){
				
				$counter_item = 1;
				foreach($_POST['company_id'] as $k => $v){
					$array = array(
						'purchase_request_id' =>$insert_id ,
						'supplier_id' =>$v,
						'company_name'=>$_POST['company_name'][$k],
						'person_in_charge'=>$_POST['pic'][$k],
						'purchase_order_reg'=>$_POST['order_Reg'][$k],
						'remarks'=>$_POST['supplier_remark'][$k],		
					);
					foreach($_POST['productId'.$counter_item] as $x => $y){
						$array['product_id'] = $y;
						$array['quantity'] = $_POST['quantity'.$counter_item][$x];
						$array['part_no'] = $_POST['partNo'.$counter_item][$x];
						$array['unit_cost'] = $_POST['unitPrice'.$counter_item][$x];
						$array['total_cost'] = $_POST['totalPrice'.$counter_item][$x];
						//$array['currency'] = $_POST['currency'.$counter_item][$x];
						
						
						if($mode == 'Add') {
						$array['created_date'] = $now;
						$this->Purchase_request_item_model->insert($array);
						}else{
						$array['modified_date'] = $now;
				
						//print_r($array);exit;
						$id = $_POST['itemId'.$counter_item][$x];
						$this->Purchase_request_item_model->update($id,$array);
						}
						
					}
					$counter_item++;
					
				}
			
			//item for add 2
			}else{
				//logistic update quantity/remark/delivery date
				if($pr_status == 6 && $role_id == LOGISTIC){
					
					$item_counter = $this->input->post("item_count", true);
					if($item_counter > 0){
						for($i=0;$i<$item_counter;$i++){
							
							$item_id = $_POST['item_id'.$i];
							$item_quantity = $_POST['item_quantity'.$i];
							$delivery_date = $_POST['delivery_date'.$i];
							$item_remark = $_POST['item_remark'.$i];
							
							if(!empty($delivery_date)) {
							  $tmp = explode("/", $delivery_date);
							  $delivery_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
							}
							
							$item_array = array(
								'quantity' =>$item_quantity,
								'delivery_date' =>$delivery_date,
								'remarks' =>$item_remark,
								'modified_date'=>$now,
							);
							
							$this->Purchase_request_item_model->update($item_id,$item_array);
							
						}
					}
				
				}
				
			}
			
			//Send Notification
			if(isset($mail_array['related_role'])){
		  	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
		  
			  	$sitename = $this->Settings_model->get_settings(1);
			  	$default_email = $this->Settings_model->get_settings(6);
			
		  		$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
		  		$this->load->library('email');
		  		foreach($related_role_data as $k => $v){
			  		
					$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
			
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->send();	
			  
		  		}
				
			}
			}

			
			//print_r($array);exit;
			
			$lastpage = $this->session->userdata("lastpage");
			if(!empty($lastpage)) {
				redirect($lastpage,'refresh');  
			} else {
				redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
			}		
			  
	  }
	  
	  public function del($id){
		  $this->Purchase_request_model->delete($id);
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		
	  }
	  
	  public function ajax_getQuotation(){
		  
		  	$keyword = $_POST['keyword'];
			$limit = $_POST['limit'];
			$page = $_POST['page'];
			
			$start = ($page-1) * $limit;
			//我們只取已經confirm的quotation
		  	$record_count = $this->Quotation_model->ajax_record_count($keyword, array(
				'status'	=> 6
			));
		  	$data = $this->Quotation_model->ajax_quotation($keyword, $limit, $start, array(
				'status'	=> 6
			));
		  	$paging = $this->get_paging($limit,10,$record_count,$page);
			
			$quotation_status = $this->Quotation_model->quotation_status_list();
			
			//顯示Status為狀態文字, 不是數字
			foreach($data as $k=>$v) {
				$data[$k]['status'] = $quotation_status[$v['status']];
			}
		  	
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					'paging'	=> $paging,
			);
			
			echo json_encode($temp);	
			exit;
	  }	  
	  
	  public function ajax_getQuotationData(){
		  
		  	$id = $_POST['id'];
		  	$data = $this->Quotation_model->get($id);
			$employee = $this->Employee_model->get($data['create_user_id']);
			$department = $this->Department_model->get($employee['department_id']);
			$data['employee_no'] = $employee['employee_no'];
			$data['employee_id'] = $employee['id'];
			$data['employee_name'] = $employee['full_name'];
			$data['department'] = $department['name'];
			
			
			//get quotation related item
			$related_quotation_item = $this->Quotation_item_model->getRelatedProduct($id);
			
			foreach($related_quotation_item as $k => $v){
				$product_data = $this->Products_model->get($v['product_id']);
				$supplier_data = $this->Suppliers_model->get($product_data['supplier']);
				$related_quotation_item[$k]['model_no'] = $product_data['model_no'];
				$related_quotation_item[$k]['part_no'] = $product_data['part_no'];
				$related_quotation_item[$k]['supplier_id'] = $product_data['supplier'];
				$related_quotation_item[$k]['cost_price'] = $product_data['cost_price'];
				$related_quotation_item[$k]['supplier_name'] = $supplier_data['company_name'];
				$related_quotation_item[$k]['supplier_pic'] = $supplier_data['contact_person'];
			}
			
			//把同一個supplier id 存在同一個array
			$groupData = array();
			foreach($related_quotation_item as $k => $v){
				
				if(isset($groupData[$v['supplier_id']])){
					$groupData[$v['supplier_id']][]=$v;
				}else{
					$groupData[$v['supplier_id']] = array();
					$groupData[$v['supplier_id']][]=$v;
				}
				
			}
		
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					//'product'   => $related_quotation_item,
					'product' => $groupData,
			);
			
			
			
			echo json_encode($temp);	
			exit;
	  }	 
	  
	  public function get_paging($item_per_page,$pagenum,$total_item,$page)
	  {
	
		$start = (int)(($page-1)/$pagenum)*$pagenum+1;
		$end = $start+$pagenum-1;
		$next = $page+1;
		$pre = $page-1;
		
		$total_page = ceil( $total_item / $item_per_page );
                $paging = '';
		if($total_item > $item_per_page){
                    $paging .= '<ul class="pagination">';

                    if($page > 1){
                            $paging .= '<li><a href="javascript:changePage(1)">&laquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$pre.')">&lsaquo;</li>';
                    }

                    if($total_page <= $pagenum){

                            for($i=$start;$i<=$total_page;$i++){
                                    if($i == $page){

                                            $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                    }else{

                                            $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                    }
                            }
                    }else{
                            if($page > 5){
                                    $end = $page+5;
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    $start = $end - ($pagenum - 1);

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }else{
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }	
                    }

                    if($page < $total_page){
                            $paging .= '<li><a href="javascript:changePage('.$next.')">&rsaquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$total_page.')">&raquo;</a></li>';
                    }

                    $paging .= '</ul>';
                }
		
		return $paging;
	  }
	  
	  public function PDF_generation($id, $mode='I',$supplier_id) {
		  
		  $pr_data = $this->Purchase_request_model->get($id);
		  $customer = $this->Customers_model->get($pr_data['customer_id']);
		  $originator = $this->Employee_model->get($pr_data['requestor_id']);	
		  $related_product = $this->Purchase_request_item_model->getRelatedProduct_bySupplier($id,$supplier_id);		
		  
		  //get item desc
		  foreach($related_product as $k => $v){
			$product = $this->Products_model->get($v['product_id']);
			$related_product[$k]['item_desc'] = $product['description'];		
		  }
		  
		  
		  
		  //print_r($product);exit;
		  $item_group = '';
		  foreach($related_product as $k => $v){
			  $item_group .='<tr><td>'.($k+1).'</td><td>'.$v['quantity'].'</td><td>'.$v['item_desc'].'</td><td>'.$v['unit_cost'].'</td><td>'.$v['total_cost'].'</td><td>'.($v['delivery_date']!="0000-00-00"?date('d/m/Y',strtotime($v['delivery_date'])):'').'</td><td>'.$v['remarks'].'</td></tr>';
		  }
		  
		  $this->load->library("Pdf");		
          $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		  
		  // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Anexus');
            $pdf->SetTitle($pr_data['purchase_serial']);
            $pdf->SetSubject("");
            $pdf->SetKeywords("");

            // remove default header/footer
            $pdf->setPrintHeader(false);			
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(10, 10, 10);

            // set auto page breaks
            //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
            }

            // ---------------------------------------------------------

            // set font
            $pdf->SetFont('times', '', 10);

            

            // set font
            $pdf->SetFont('times', '', 10);
			
			// add a page
       		$pdf->AddPage();
			
			$html = '			
			<h1>Purchase Order</h1>
			
			<table width="100%" cellpadding="5">
				<tr>
					<td>
						<table width="100%"  border="1" cellpadding="5">
			
							<tr>
								<th><h3>Purchase Order Information</h3></th>
							</tr>
							<tr>
								<th><h4>Purchase Order No.</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['purchase_serial'].'</td>
							</tr>
							<tr>
								<th><h4>Date of Creation</h4></th>
							</tr>
							<tr>
								<td>'.date('d/m/Y',strtotime($pr_data['created_date'])).'</td>
							</tr>
							<tr>
								<th><h4>Originator</h4></th>
							</tr>
							<tr>
								<td>'.$originator['full_name'].'</td>
							</tr>
							<tr>
								<th><h4>Contact No.</h4></th>
							</tr>
							<tr>
								<td>'.$originator['contact_info'].'</td>
							</tr>
							<tr>
								<th><h4>Quote Reference</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['sale_order_ref'].'</td>
							</tr>
							<tr>
								<th><h4>Currency</h4></th>
							</tr>
							<tr>
								<td>'.$customer['currency'].'</td>
							</tr>
	
						</table>
					</td>
					
					<td>
						<table  width="100%"  border="1" cellpadding="5">
			
							<tr>
								<th><h3>Order To Information</h3></th>
							</tr>
							<tr>
								<th><h4>Company Name</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['ot_company_name'].'</td>
							</tr>
							<tr>
								<th><h4>Address</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['ot_address'].'</td>
							</tr>
							<tr>
								<th><h4>Postal Code</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['ot_postal'].'</td>
							</tr>
							<tr>
								<th><h4>Attention</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['ot_attention'].'</td>
							</tr>
							<tr>
								<th><h4>CC</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['ot_cc'].'</td>
							</tr>
							<tr>
								<th><h4>Contact</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['ot_contact'].'</td>
							</tr>
		
						</table>
					</td>
				</tr>
				
				<tr>
					<td>
						<table width="100%"  border="1" cellpadding="5">
			
							<tr>
								<th><h3>Bill to</h3></th>
							</tr>
							<tr>
								<th><h4>Company Name</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['bt_company_name'].'</td>
							</tr>
							<tr>
								<th><h4>Address</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['bt_address'].'</td>
							</tr>
							<tr>
								<th><h4>Postal Code</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['bt_postal'].'</td>
							</tr>
							<tr>
								<th><h4>Attention</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['bt_attention'].'</td>
							</tr>
							<tr>
								<th><h4>CC</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['bt_cc'].'</td>
							</tr>
							<tr>
								<th><h4>Contact</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['bt_contact'].'</td>
							</tr>
							<tr>
								<th><h4>Email</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['bt_email'].'</td>
							</tr>
	
						</table>
					</td>
					
					<td>
						<table width="100%"  border="1" cellpadding="5">
			
							<tr>
								<th><h3>Ship to</h3></th>
							</tr>
							<tr>
								<th><h4>Company Name</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['st_company_name'].'</td>
							</tr>
							<tr>
								<th><h4>Address</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['st_address'].'</td>
							</tr>
							<tr>
								<th><h4>Postal Code</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['st_postal'].'</td>
							</tr>
							<tr>
								<th><h4>Attention</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['st_attention'].'</td>
							</tr>
							<tr>
								<th><h4>CC</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['st_cc'].'</td>
							</tr>
							<tr>
								<th><h4>Contact</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['st_contact'].'</td>
							</tr>
							<tr>
								<th><h4>Email</h4></th>
							</tr>
							<tr>
								<td>'.$pr_data['st_email'].'</td>
							</tr>
	
						</table>
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
						
						<table width="100%" border="1" cellpadding="5">
				
							<tr>
								<th colspan="7"><h3>Items</h3></th>
							</tr>
							<tr>
								<td>#</td>
								<td>Quantity</td>
								<td>Item Description</td>
								<td>Unit Price</td>
								<td>Total Price</td>
								<td>Delivery Date</td>
								<td>Remarks</td>
							</tr>'.$item_group.'
						</table>
						
					</td>
				</tr>
				<tr>
					<td colspan="2">
					
						<table width="100%" border="1" cellpadding="5">
				
							<tr>
								<th><h3>Additional Information</h3></th>
							</tr>
							<tr>
								<td><h4>Remarks</h4></td>
							</tr>
							<tr>	
								<td>'.nl2br($pr_data['remark']).'</td>
							</tr>
							<tr>
								<td><h4>General Terms & Conditions</h4></td>
							</tr>
							<tr>
								<td>'.nl2br($pr_data['term_condition']).'</td>
							</tr>
						</table>
					
					
					</td>
				</tr>

			</table>
			
			';
			
			$pdf->writeHTML($html, true, false, true, false, '');
												
			
			$file_name = date("YmdHis").rand(1000,9999);
			
			if($mode == 'I') {
				$pdf->Output($file_name.'.pdf', 'I'); 	
				return $file_name.'.pdf';
			} else if ($mode == 'f') {				
				$pdf->Output('./uploads/'.$file_name.'.pdf', 'f'); 
				return './uploads/'.$file_name.'.pdf';
			}
			
			//$pdf->Output(date("YmdHis").'.pdf', 'I'); 
		  
		  
		  
	  }
	  
	  public function export_pdf($id,$supplier_id){		  
		  $this->PDF_generation($id, 'I',$supplier_id);		  
	  }
	  
	  //status == 5 的情況下可以使用這個METHOD
	  public function sent_mail($id){
		  
		  $pr_data = $this->Purchase_request_model->get($id);
		  $customer = $this->Customers_model->get($pr_data['customer_id']);		
		  $related_roles = $this->Job_model->getRelatedRoleID($pr_data['latest_job_id']);
		  
		  $sitename = $this->Settings_model->get_settings(1);
		  $default_email = $this->Settings_model->get_settings(6);
		  
		    //$requestor_role
			$requestor_employee = $this->Employee_model->get($pr_data['requestor_id']);
			$requestor_role = $requestor_employee['role_id'];
		  
		  $role_id = $this->data['userdata']['role_id'];
		  $pr_status = $pr_data['status'];
		  
		  /*
		  //count supplier
		  $supplier_count = $this->Purchase_request_item_model->get_supplier_groupBy($id);
		  
		  foreach($supplier_count as $k => $v){
		  		
			  $supplier = $this->Suppliers_model->get($v['supplier_id']);
			  
			  $file_name = $this->PDF_generation($id, 'f',$v['supplier_id']);	
					 
			  if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
				
				$this->load->library('email');
				$this->email->clear();	
				$this->email->from($default_email['value'], $sitename['value']);
				$this->email->to($supplier ['email']); 
				
				$this->email->subject('Anexus Purchase request '.$pr_data['purchase_serial']);
				$this->email->message($pr_data['purchase_serial']);	
				$this->email->attach($file_name);
				
				$this->email->send();
				
			  }
		  
		  }
		  
		  
			//logistic send mail	
			if($pr_status == 5){
				
				if($role_id == LOGISTIC) {
					
					  $jobArray = array(
						'parent_id' =>$pr_data['latest_job_id'],
						'user_id' =>$this->data['userdata']['id'],
						'customer_id' =>$pr_data['customer_id'],
						'type' => 'Purchase request',
						'type_path' =>'sales/purchase_edit/',
						'awaiting_role_id' => LOGISTIC.',',
						'status_id' => 6,
						'status_text'=>$this->data['pr_status_list'][6],
						'serial' =>$pr_data['purchase_serial'],
			  			'type_id' =>$pr_data['id'],
						'created_date'	=> date("Y-m-d H:i:s"),
						'modified_date'	=> date("Y-m-d H:i:s"),
					  );	
					  
					  //set last job completed
					  $this->Job_model->update($jobArray['parent_id'], array(
					  	'is_completed' => 1,
					  ));
					  
					  //insert new job
					  $new_job_id = $this->Job_model->insert($jobArray);
					  
					  //update pr data
					  $array = array(
					 	'awaiting_table' 	=> LOGISTIC.',',
						'status' 			=> 6,
						'latest_job_id'		=> $new_job_id,
					  );					  
					  $this->Purchase_request_model->update($pr_data['id'], $array);
					  
					  //sent mail
					  $mail_array = array(
					    'creater_name' =>$this->data['userdata']['name'],
					    'title' => $pr_data['purchase_serial'].' '.$this->data['pr_status_list'][6],
						'related_role' => $related_roles,
					  );					  
					  $related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
					  
					  if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
						  $this->load->library('email');						  
						  foreach($related_role_data as $k => $v){
							$this->email->clear();  
							$this->email->from($default_email['value'], $sitename['value']);
							$this->email->to($v['email']); 
							
							$this->email->subject($mail_array['title']);
							$this->email->message($mail_array['creater_name'].'<br/>'.$mail_array['title']);
							$this->email->send();	
							  
						  }
					  }
					  	
				}
					    
			}
			*/
			
			///////////////////////////////////////////////////////////
			
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $pr_status,
							'workflow_id' => 2,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(2);
						$status = json_decode($workflow['status_json'],true);
						
						if(isset($action)){

		
						if($action == 'send_email'){
							
							//next status
							$next_status = $workflow_order['next_status_id'];
							$next_status_text = $status[$workflow_order['next_status_id']];
							$who_email = $workflow_order['who_email'];
	
						}

						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];
								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 2,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
							
							$jobArray = array(
								'parent_id' 		=>$pr_data['latest_job_id'],
								'user_id' 			=>$this->data['userdata']['id'],
								'customer_id' 		=>$pr_data['customer_id'],
								'type' 				=> 'Purchase request',
								'type_path' 		=>'sales/purchase_edit/',
								'awaiting_role_id' 	=>$awating_person.',',
								'status_id' 		=>$next_status,
								'status_text'		=>$next_status_text,
								'serial' 			=>$pr_data['purchase_serial'],
								'type_id' 			=>$pr_data['id'],
								'created_date'		=>date("Y-m-d H:i:s"),
								'modified_date'		=>date("Y-m-d H:i:s"),
							);			
					  
						    $array = array(
							'awaiting_table' => $awating_person.',',
							'status' => $next_status,
							'modified_date' => date('Y-m-d H:i:s'),
							'lastupdate_user_id' => $this->data['userdata']['id'],
						    );
						
							
						
						  //更新上一個JOB的狀態					  
						  $this->Job_model->update($jobArray['parent_id'], array(
							'is_completed' => 1,
							'display' => 0,
						  ));
						  
						  //新增一個新的JOB
						  $new_job_id = $this->Job_model->insert($jobArray);
						  $array['latest_job_id'] = $new_job_id;
						  
						  $this->Purchase_request_model->update($pr_data['id'], $array);
							
							
						}
						
						}
			
			
			if(isset($who_email)){
			
				$this->load->library('email');
				
				switch($who_email){
					case 'customer':
							
							//Send Notification
							if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
										
								$this->email->from($default_email['value'], $sitename['value']);
								$this->email->to($send_to); 
							
								$this->email->subject('Anexus '.$workflow['title'].': '.$pr_data['purchase_serial']);
								$this->email->message($pr_data['purchase_serial']);	
								$this->email->attach($filename);
							
								$this->email->send();
							
							}
							
					break;	
					case 'supplier':
						
							  //count supplier
							  $supplier_count = $this->Purchase_request_item_model->get_supplier_groupBy($id);
							  
							  foreach($supplier_count as $k => $v){
									
								  $supplier = $this->Suppliers_model->get($v['supplier_id']);
								  
								  $file_name = $this->PDF_generation($id, 'f',$v['supplier_id']);	
										 
								  if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
									
									$this->load->library('email');
									$this->email->clear();	
									$this->email->from($default_email['value'], $sitename['value']);
									$this->email->to($supplier ['email']); 
									
									$this->email->subject('Anexus Purchase request '.$pr_data['purchase_serial']);
									$this->email->message($pr_data['purchase_serial']);	
									$this->email->attach($file_name);
									
									$this->email->send();
									
								  }
							  
							  }
							
					break;
					case 'employee':
					break;
				
				}
				
			}
			
			 //mail
			 $mail_array = array(
				'creater_name' =>$this->data['userdata']['name'],
				'title' =>'Purchase '.$pr_data['purchase_serial'].' has been send out',
			 );
			 
			 if(isset($new_job_id)){
			  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
			  $mail_array['related_role'] = $related_role;		
			  $mail_array['title'] = $workflow['title']." ".$pr_data['purchase_serial']." ".$next_status_text;
			 }
			 
			 //sent Notification to related role						  			
		  	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local" && isset($mail_array['related_role'])) {
				$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
			  				  	
			  	foreach($related_role_data as $k => $v){
				  
				  	$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
					
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->send();	
				  
		 		}
			}
			
			///////////////////////////////////////////////////////////
			
			$lastpage = $this->session->userdata("lastpage");
			if(!empty($lastpage)) {
				  redirect($lastpage,'refresh');  
			}else {
				  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
			}		 
			
		  
	  }
	  
	  public function ajax_getSupplier(){
		  
		    $id = json_decode($_POST['id'],true);
			
			
		  	$supplier_data = $this->Suppliers_model->getSupplierList($id);
			
			
			$temp = array(
				'status' => 'OK',
				'supplier' => $supplier_data,
			);
			
			echo json_encode($temp);	
			exit;
			
	  }
	  
	  public function ajax_getItem(){
		  
		    $id = $_POST['id'];
			$supplier_related_item = $this->Products_model->ajax_model($id);
			
			
			$temp = array(
				'status' => 'OK',
				'item' => $supplier_related_item,
			);
			
			echo json_encode($temp);	
			exit;
			
	  }
	  
	  public function ajax_getItemDetail(){
		  
		    $id = $_POST['id'];
			$item_detail = $this->Products_model->get($id);
			
			
			$temp = array(
				'status' => 'OK',
				'item_detail' => $item_detail,
			);
			
			echo json_encode($temp);	
			exit;
			
	  }
	  
	  public function ajax_getCompanyDetail(){
		  
		    $id = $_POST['id'];
			$company_detail = $this->Customers_model->get($id);
			
			$temp = array(
				'status' => 'OK',
				'company_detail' => $company_detail,
			);
			
			echo json_encode($temp);	
			exit;
			
	  }
	  
	  private function parse_boolean($string){
		  //echo $string;
		  return eval("return (".$string.");");
	  }
	  
	  
	  

}

?>