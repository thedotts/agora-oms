<?php

class Field_service_order_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();  
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Customers_model');
			$this->load->model('Field_service_order_model');
			$this->load->model('Service_request_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Settings_model');
			$this->load->model('Employee_model');
			$this->load->model('Workflow_model');
			$this->load->model('Workflow_order_model');

            $this->data['init'] = $this->function_model->page_init();
			
			//get status list
			$fso_status_list = $this->Workflow_model->get(8);
			$this->data['fso_status_list'] = json_decode($fso_status_list['status_json']);
			
			$this->data['role_list'] = $this->Role_model->getIDKeyArray('name');
			$this->data['status_list'] = $this->Field_service_order_model->status_list();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$this->data['userdata'] = $this->session->userdata("userdata");
			}         
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			$this->data['group_name'] = "service";  
			$this->data['model_name'] = "field_service_order";  
			$this->data['common_name'] = "Field Service Order";   
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}
			
			define('SUPERADMIN', 1);
			define('GENERAL_MANAGER', 2);
			define('SERVICE_MANAGER', 3);
			define('SERVICE_EXECUTIVE', 4);			
		    define('FINANCE', 5);
			define('LOGISTIC', 6);
		    define('SALES_MANAGER', 7);
		    define('SALES_EXECUTIVE', 8);	

      }
   
      public function index($q="ALL", $status="ALL", $page=1) {  
          			
            $this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/anexus/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/'.$status.'/';
			if($q == 'ALL') {
				$q = "";
			}
			$this->data['q'] = $q;
			
			if($status != 'ALL') {
				$filter['status'] = $status;	
			}
			$this->data['status'] = $status;			
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Field_service_order_model->record_count($filter, $q);
			
			//get particular ranged list
			$this->data['results'] = $this->Field_service_order_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start);
			if(!empty($this->data['results'])){
				foreach($this->data['results'] as $k => $v){
					if(!empty($v['awaiting_table'])){
							$this->data['results'][$k]['awaiting_table']= explode(',',$v['awaiting_table']);
					}
				}
			}
						
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->data['customer_list'] = $this->Customers_model->getIDKeyArray("company_name");
			$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
			
			$this->session->set_userdata("lastpage", $url.$page);
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  
	  public function add($id=false) {  
          	
			$role_id = $this->data['userdata']['role_id'];
			
			if($id !== false) {
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Field_service_order_model->get($id);	
				$this->data['customer'] = $this->Customers_model->get($this->data['result']['customer_id']);						
				
				$this->data['result']['awaiting_table'] = explode(',',$this->data['result']['awaiting_table']);
				
				$requestor_employee = $this->Employee_model->get($this->data['result']['requestor_id']);
			  	$requestor_role = $requestor_employee['role_id'];
				
				$default = 0;
				
				
				/*
				//waiting sm approve
				if($this->data['result']['status'] == 1){
					
					//se edit
					if($role_id == SERVICE_EXECUTIVE){
						$this->data['head_title'] = 'Edit Field Service Form';
					//sm comfirm
					}else if($role_id == SERVICE_MANAGER){
						$this->data['head_title'] = 'Confirm Field Service Form';
					}else{
						$default = 1;
					}
				
				//waiting gm approve
				}else if($this->data['result']['status'] == 2){
					
					//sm edit
					if($role_id == SERVICE_MANAGER){
						$this->data['head_title'] = 'Edit Field Service Form';
					//gm comfirm
					}else if($role_id == GENERAL_MANAGER){
						$this->data['head_title'] = 'Confirm Field Service Form';
					}else{
						$default = 1;
					}
					
				//finance approve
				}else if($this->data['result']['status'] == 3){
					
					//sr edit
					if($role_id == FINANCE){
						$this->data['head_title'] = 'Confirm Field Service Form';
					}else{
						$default = 1;
					}
					
				}else{
					$default = 1;
				}
				
				//no related person
				if($default == 1){
					$this->data['head_title'] = 'Field Service Form';	
				}
				*/
				
				//////////////////////////////////////////////////////////////////////////////////
				
				   		//get related workflow order data
						$workflow_flitter = array(
							'status_id' => $this->data['result']['status'],
							'workflow_id' => 8,
						);
						
						$type='';
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$type = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$type = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							//print_r($requestor_role);exit;
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
							$type = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
									$type = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(8);
						$status = json_decode($workflow['status_json'],true);
						
						$this->data['workflow_title'] = $workflow['title'];
					
					$this->data['btn_type'] = $type;
					//print_r($workflow_order);exit;

					switch($type){
						case 'edit':
							$this->data['head_title'] = 'Edit '.$workflow['title'].' Request';
						break;
						case 'approval':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
						break;
						case 'approval_update':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'update':
							$this->data['head_title'] = 'Update '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'send_email':
							$this->data['head_title'] = 'Waiting to Send '.$workflow['title'].' Request';
						break;
						case 'sendmail_update':
							$this->data['head_title'] = 'Waiting to Send/Edit '.$workflow['title'].' Request';
						break;
						case 'upload':
							$this->data['head_title'] = 'Upload Confirm File '.$workflow['title'].' Request';
						break;
						case 'complete':
							$this->data['head_title'] = $workflow['title'].' Request';
							$this->data['last_action'] = $workflow_order['action_before_complete'];
						break;
						default:
							$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					foreach(json_decode($workflow['status_json'],true) as $k => $v){
						
						if($v == 'Completed'){
							$this->data['completed_status_id'] = $k;
							break;
						}
					}
				//////////////////////////////////////////////////////////////////////////////////////
				
			} else {
				$this->data['mode'] = 'Add';	
				$this->data['head_title'] = 'Create Field Service Form';	
				
				//get related workflow order data
					$workflow_flitter = array(
						'role_id' => $role_id,
						'status_id' => -1,
						'workflow_id' => 8,
					);
					
					//data form database
					$workflow = $this->Workflow_model->get(8);
					$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
					
					//get view bottom layout
					$type = '';
					if(!empty($workflow_order)){
						$type = $workflow_order['action'];
						if($workflow_order['action'] == 'create'){
							$this->data['head_title'] = 'Create '.$workflow['title'].' Request';
						}else{
							$this->data['head_title'] = $workflow['title'].' Request';
						}
					}else{
						$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					$this->data['btn_type'] = $type;
			}
			
			
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		
            
			
      }	  	  
	  
	  public function submit(){
		  $now = date("Y-m-d H:i:s");
		  $mode = $this->input->post("mode", true);
		  $id = $this->input->post("id", true);
		  		  		 
 		  $role_id = $this->data['userdata']['role_id'];
		   
		  //get value approve or not approve
		  $confirm = $this->input->post("confirm", true);
		  $correct_check = $this->input->post("correct_check", true);
		  
		  
		  if($mode == 'Add'){
			  
			  $customer_id = $this->input->post("customer_id", true);
		  	  
		  }else{
		  
			  //fso
			  $fso_data = $this->Field_service_order_model->get($id);
		  	  $customer_id = $fso_data['customer_id'];
			  
		  }	
		  
		    //job
		  	$jobArray = array(
				'parent_id' =>0,
				'user_id' =>$this->data['userdata']['id'],
				'customer_id' =>$customer_id,
				'type' => 'Field Service Order',
				'type_path' =>'service/field_service_order_edit/',
				'created_date'	=> $now,
				'modified_date'	=> $now,
		 	 );
		  
		  //main
		  $array = array(	
		  	'product_serial' => $this->input->post("product_serial", true),
			'service' => $this->input->post("service", true),
			'service_type' => $this->input->post("service_type", true),
			'tool_id' => $this->input->post("tool_id", true),
			'customer_id' => $this->input->post("customer_id", true),
			'customer_name' => $this->input->post("company_name", true),
			'faults' => $this->input->post("faults", true),
			'actions_remedy' => $this->input->post("act_remedy", true),
			'spare_part_used' => $this->input->post("spare_part", true),	
			'requestor_id' => $this->input->post("requestor_id", true),  	
		  );
		  
		    //mail
			$mail_array = array(
		  	'creater_name' =>$this->data['userdata']['name'],
		  	);	
		  
		  $addCounter = $this->input->post("addCounter", true);
		  $schedule_json = array();
		  if($addCounter > 0){
			  for($i=0;$i<=$addCounter;$i++){
				  
				  if(isset($_POST['schedule_date'.$i]) && $_POST['schedule_date'.$i] != ''){
				  $group_schedule = array(
						'schedule_date' =>isset($_POST['schedule_date'.$i])?$_POST['schedule_date'.$i]:'',
						'schedule_day' =>isset($_POST['schedule_day'.$i])?$_POST['schedule_day'.$i]:'',
						'schedule_startTime' =>isset($_POST['schedule_startTime'.$i])?$_POST['schedule_startTime'.$i]:'',
						'schedule_endTime1' =>isset($_POST['schedule_endTime'.$i])?$_POST['schedule_endTime'.$i]:'',
						'totalHour' =>isset($_POST['totalHour'.$i])?$_POST['totalHour'.$i]:'',
				  );
				  
				  $schedule_json[] = $group_schedule;
				  }
				  
			  }
			  $array['time_schedule'] = json_encode($schedule_json);
		  }
		  
		  //print_r($array);exit;
		  
		  
		  
		  if(isset($_FILES['faults_img'])){
				
				if ( $_FILES['faults_img']['error'] == 0 && $_FILES['faults_img']['name'] != ''  ){									
						$pathinfo = pathinfo($_FILES['faults_img']['name']);
						$ext = $pathinfo['extension'];
						$ext = strtolower($ext);
											
						$filename = 'img_'.date("YmdHis")."_".rand(1000,9999);
						$path = "./uploads/".$filename.'.'.$ext;
						$save_path = base_url()."uploads/".$filename.'.'.$ext;
						$result = move_uploaded_file($_FILES['faults_img']['tmp_name'], $path);
						if(!$result) {
							die("cannot upload file");
						}	
					
						$array['faults_reference']= $save_path;															
				}	
		  }
		  
		  if(isset($_FILES['act_remedy_img'])){
				
				if ( $_FILES['act_remedy_img']['error'] == 0 && $_FILES['act_remedy_img']['name'] != ''  ){									
						$pathinfo = pathinfo($_FILES['act_remedy_img']['name']);
						$ext = $pathinfo['extension'];
						$ext = strtolower($ext);
											
						$filename = 'img_'.date("YmdHis")."_".rand(1000,9999);
						$path = "./uploads/".$filename.'.'.$ext;
						$save_path = base_url()."uploads/".$filename.'.'.$ext;
						$result = move_uploaded_file($_FILES['act_remedy_img']['tmp_name'], $path);
						if(!$result) {
							die("cannot upload file");
						}	
					
						$array['actions_remedy_ref']= $save_path;															
				}	
		  }
		  
		  if(isset($_FILES['spare_part_img'])){
				
				if ( $_FILES['spare_part_img']['error'] == 0 && $_FILES['spare_part_img']['name'] != ''  ){									
						$pathinfo = pathinfo($_FILES['spare_part_img']['name']);
						$ext = $pathinfo['extension'];
						$ext = strtolower($ext);
											
						$filename = 'img_'.date("YmdHis")."_".rand(1000,9999);
						$path = "./uploads/".$filename.'.'.$ext;
						$save_path = base_url()."uploads/".$filename.'.'.$ext;
						$result = move_uploaded_file($_FILES['spare_part_img']['tmp_name'], $path);
						if(!$result) {
							die("cannot upload file");
						}	
					
						$array['spare_part_ref']= $save_path;															
				}	
		  }
		  
		  if(isset($_FILES['confirm_doc'])){
				
				if ( $_FILES['confirm_doc']['error'] == 0 && $_FILES['confirm_doc']['name'] != ''  ){									
						$pathinfo = pathinfo($_FILES['confirm_doc']['name']);
						$ext = $pathinfo['extension'];
						$ext = strtolower($ext);
											
						$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
						$path = "./uploads/".$filename.'.'.$ext;
						$save_path = base_url()."uploads/".$filename.'.'.$ext;
						$result = move_uploaded_file($_FILES['confirm_doc']['tmp_name'], $path);
						if(!$result) {
							die("cannot upload file");
						}	
					
						$array['customer_confirmation_document']= $save_path;															
				}	
		  }
		  
		  
		  
		  //print_r($array);exit;
		  
		  
		  //Add
		  if($mode == 'Add') {			  			  
			  
			  
			  /*
			  //create by service executive
			  	if($this->data['userdata']['role_id'] == SERVICE_EXECUTIVE){
					
			  		//main
					$array['awaiting_table']  = SERVICE_MANAGER;
					$array['status']  = 1;
					//job
					$jobArray['awaiting_role_id'] = SERVICE_MANAGER.',';
					$jobArray['status_id'] = 1;
					$jobArray['status_text'] = $this->data['fso_status_list'][1];
					//mail
					$mail_array['related_role'] = array(SERVICE_EXECUTIVE,SERVICE_MANAGER);
					
				//create by service manager
				}else if($this->data['userdata']['role_id'] == SERVICE_MANAGER){
					
					//main
					$array['awaiting_table']  = GENERAL_MANAGER;
					$array['status']  = 2;
					//job
					$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
					$jobArray['status_id'] = 2;
					$jobArray['status_text'] = $this->data['fso_status_list'][2];
					//mail
					$mail_array['related_role'] = array(SERVICE_MANAGER,GENERAL_MANAGER);
					
				}
			  
			  $mail_array['title'] = 'create Field Service Form';
			  
			  $array['create_user_id'] = $this->data['userdata']['id'];
			  $array['created_date'] = $now;
			  $array['lastupdate_user_id'] = $this->data['userdata']['id'];
			  $array['modified_date'] = $now;
			  			  
			  $insert_id = $this->Field_service_order_model->insert($array);
			  $field_service_no = $this->Field_service_order_model->zerofill($insert_id, 5);
			  
			  //insert job
			  $jobArray['serial'] =$field_service_no;
			  $jobArray['type_id'] =$insert_id;
			  $job_id = $this->Job_model->insert($jobArray);
			  
			  //after insert job,update job id,qr serial....
			  $array =array(
			  	'job_id'			=> $job_id,
			  	'field_service_no'  => $field_service_no,
				'latest_job_id'		=> $job_id,
			  );
			  $this->Field_service_order_model->update($insert_id, $array);		
			  */
			  ////////////////////////////////////////////////////////////////////////////////////
				
				//get related workflow order data
				$workflow_flitter = array(
					'role_id' => $role_id,
					'status_id' => -1,
					'workflow_id' => 8,
				);
				
				//data form database
				$workflow = $this->Workflow_model->get(8);
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
				$status = json_decode($workflow['status_json'],true);
				
				if(!empty($workflow_order)){
				
				//ad_hoc
				if($workflow_order['ad_hoc'] == 1){
					$array['ad_hoc'] = 1;
				}
				
				//purchaser
				$is_adhoc = false;
				if($role_id == 10){
					$is_adhoc = true;
				}
				
				//next status
				$next_status = $workflow_order['next_status_id'];
				$next_status_text = $status[$workflow_order['next_status_id']];
				
			  	//if need_det = 1
				if($workflow_order['need_det'] == 1){
					
					$formula = json_decode($workflow_order['formula'],true);
					
					//check if product or service price over value
					foreach($formula as $k => $v){
					
						$price = $this->input->post($v['target_colum'], true);
						$det_temp = $price." ".$v['logic']." ".$v['value'];
						$check = $this->parse_boolean($det_temp);
						
						//check price over value
						if($check){
							$next_status = $v['next_status_id'];
							$next_status_text = $status[$v['next_status_id']];
							break;
						}
						
					}
					
				}
				
				//get next status workflow order data
				$next_workflow_flitter = array(
					'status_id' => $next_status,
					'workflow_id' => 8,
				);
				
				//next status data
				$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
				$awating_person = $next_status_data['role_id'];
				
				
				
				if($awating_person == 'requestor'){
					$awating_person = $this->input->post("requestor_id", true);
				}
				//echo $awating_person;exit;
				
				//main
				$array['awaiting_table']  = $awating_person;
				$array['status'] = $next_status;
				//main
				$jobArray['awaiting_role_id'] = $awating_person.',';
				$jobArray['status_id'] = $next_status;
				$jobArray['status_text'] = $next_status_text; 	
				  
				  //insert main data
					$array['created_date'] = $now;
					$array['modified_date'] = $now;
					$array['create_user_id'] = $this->data['userdata']['id'];
					$array['lastupdate_user_id'] = $this->data['userdata']['id'];

					//新增資料
					$insert_id = $this->Field_service_order_model->insert($array);				
					
					//更新purchase serial
					$field_service_no = $this->Field_service_order_model->zerofill($insert_id, 5);
					
					//insert job
					$jobArray['serial'] = $field_service_no;
					$jobArray['type_id'] = $insert_id;
					
					$jobArray['created_date'] = $now;
					$jobArray['modified_date'] = $now;
					
					$job_id = $this->Job_model->insert($jobArray);
				  
					//after insert job,update job id,qr serial....
					$array =array(
						'job_id'	=> $job_id,
						'field_service_no'  => $field_service_no,
						'latest_job_id'	=> $job_id,
					);
					$this->Field_service_order_model->update($insert_id, $array);
					
				  //mail
				  $related_role = $this->Job_model->getRelatedRoleID($job_id);
				  $mail_array['related_role'] = $related_role;		
				  $mail_array['title'] = $workflow['title']." ".$field_service_no." ".$next_status_text; 
				  	
				}
				
				////////////////////////////////////////////////////////////////////////////////////	  	  
			  
		  //Edit	  			  
		  } else {
			  	
			    $fso_data = $this->Field_service_order_model->get($id);
			  	$fso_status = $fso_data['status'];
				
				$requestor_employee = $this->Employee_model->get($fso_data['requestor_id']);
			  	$requestor_role = $requestor_employee['role_id'];
			  
			  	$jobArray['serial'] =$fso_data['field_service_no'];
			  	$jobArray['type_id'] =$fso_data['id'];
			  	$jobArray['parent_id'] = $fso_data['latest_job_id'];
				
				$next = 0;
			  	$last = 0;
			  	$status_change = 0;
				/*
				$related_roles = $this->Job_model->getRelatedRoleID($fso_data['latest_job_id']);	
				
				//判断status 给予下个job
				if($fso_status == 1){
					
					//se edit	
					if($role_id == SERVICE_EXECUTIVE){
						
						$array['awaiting_table']  = SERVICE_MANAGER;
						$array['status']  = 1;
						//job
						$jobArray['awaiting_role_id'] = SERVICE_MANAGER.',';
						$jobArray['status_id'] = 1;
						$jobArray['status_text'] = $this->data['fso_status_list'][1];
						//mail
						$mail_array['related_role'] = array(SERVICE_EXECUTIVE,SERVICE_MANAGER);
						$mail_array['title'] = $fso_data['field_service_no'].' '.$this->data['fso_status_list'][1];
						
					//sm approved/denied
					}else if($role_id == SERVICE_MANAGER){
						
						//sm manager denied
						if($confirm == 0){
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	//mail
							$mail_array['related_role'] = array(SERVICE_EXECUTIVE,SERVICE_MANAGER);
							$mail_array['title'] = $fso_data['field_service_no'].' '.$this->data['fso_status_list'][0];
						  
						  	$next = 1;
						  	$last = 1;	
						//sm maneger approved
						}else if($correct_check == 'on'){
							
							$array['awaiting_table']  = FINANCE;
							$array['status']  = 3;
							$jobArray['awaiting_role_id'] = FINANCE.',';
							$jobArray['status_id'] = 3;
							$jobArray['status_text'] = $this->data['fso_status_list'][3];
							
							//mail
							$mail_array['related_role'] = array(SERVICE_EXECUTIVE,SERVICE_MANAGER,FINANCE);
							$mail_array['title'] = $fso_data['field_service_no'].' '.$this->data['fso_status_list'][3];
							
							$next = 1;
						}
						
					}
				//general approved/denied	
				}else if($fso_status == 2){
					
					//sm edit	
					if($role_id == SERVICE_MANAGER){
						
						$array['awaiting_table']  = GENERAL_MANAGER;
						$array['status']  = 2;
						//job
						$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
						$jobArray['status_id'] = 2;
						$jobArray['status_text'] = $this->data['fso_status_list'][2];
						//mail
						$mail_array['related_role'] = array(GENERAL_MANAGER,SERVICE_MANAGER);
						$mail_array['title'] = $fso_data['field_service_no'].' '.$this->data['fso_status_list'][2];
						
					//gm approved/denied
					}else if($role_id == GENERAL_MANAGER){
						
						
						//gm manager denied
						if($confirm == 0){
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	//mail
							$mail_array['related_role'] = array(SERVICE_MANAGER,GENERAL_MANAGER);
							$mail_array['title'] = $fso_data['field_service_no'].' '.$this->data['fso_status_list'][0];
						  
						  	$next = 1;
						  	$last = 1;	
						//gm maneger approved
						}else if($correct_check == 'on'){
							
							$array['awaiting_table']  = FINANCE;
							$array['status']  = 3;
							$jobArray['awaiting_role_id'] = FINANCE.',';
							$jobArray['status_id'] = 3;
							$jobArray['status_text'] = $this->data['fso_status_list'][3];
							
							$related_roles[] = FINANCE;
							//mail
							$mail_array['related_role'] = $related_roles;
							$mail_array['title'] = $fso_data['field_service_no'].' '.$this->data['fso_status_list'][3];
							
							$next = 1;
						}
						
					}
					
					
				//upload comfirm file	
				} else if($fso_status == 4 && in_array($role_id, array(FINANCE,SERVICE_EXECUTIVE,SERVICE_MANAGER))){
					
							$array['awaiting_table']  = '';
							$array['status']  = 5;
							
							
							
							if(isset($_FILES['comfirm_file'])){
				
								if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
										$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
										$ext = $pathinfo['extension'];
										$ext = strtolower($ext);
															
										$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
										$path = "./uploads/".$filename.'.'.$ext;
										$save_path = base_url()."uploads/".$filename.'.'.$ext;
										$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
										if(!$result) {
											die("cannot upload file");
										}	
										
										$array['customer_confirmation_document']= $save_path;														
										//mail
										$mail_array['related_role'] = $related_roles;
										$mail_array['title'] = $fso_data['field_service_no'].' '.$this->data['fso_status_list'][5];
										
								}	
							}
							
							
							
							$next = 1;
							$last = 1;
					
				//reupload comfirm file	
				} else if($fso_status == 5 && in_array($role_id, array(FINANCE,SERVICE_EXECUTIVE,SERVICE_MANAGER))){
					

							if(isset($_FILES['comfirm_file'])){
				
								if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
										$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
										$ext = $pathinfo['extension'];
										$ext = strtolower($ext);
															
										$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
										$path = "./uploads/".$filename.'.'.$ext;
										$save_path = base_url()."uploads/".$filename.'.'.$ext;
										$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
										if(!$result) {
											die("cannot upload file");
										}	
										
										$array['customer_confirmation_document']= $save_path;														
										
								}	
							}
							
					
				}*/
			  
			  /////////////////////////////////////////////////////////////////////////////////
			  
			  //can edit status
			  $edit_status = $this->Workflow_order_model->get_status_edit(8);
			  
			  //requestor edit
			  if(in_array($fso_status, $edit_status) && $role_id == $requestor_role){
			  	  
						//get related workflow order data
						$workflow_flitter = array(
							'role_id' => $role_id,
							'status_id' => -1,
							'workflow_id' => 8,
						);
						
						//data form database
						$workflow = $this->Workflow_model->get(8);
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						$status = json_decode($workflow['status_json'],true);
						
						if(!empty($workflow_order)){
						
						//next status
						$next_status = $workflow_order['next_status_id'];
						$next_status_text = $status[$workflow_order['next_status_id']];
						
						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];
								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						//get next status workflow order data
						$next_workflow_flitter = array(
							'status_id' => $next_status,
							'workflow_id' => 8,
						);
						
						//next status data
						$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
						$awating_person = $next_status_data['role_id'];
						
						//if awating person equal requestor ,asign requestor for it
						if($awating_person == 'requestor'){
							$awating_person = $requestor_role;
						}
						
						//main
						$array['awaiting_table']  = $awating_person;
						$array['status'] = $next_status;
						
						//job
						$jobArray['awaiting_role_id'] = $awating_person.',';
						$jobArray['status_id'] = $next_status;
						$jobArray['status_text'] = $next_status_text;
						
						
						}
						
				  
			  }else{
				  		
												
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $fso_status,
							'workflow_id' => 8,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(8);
						$status = json_decode($workflow['status_json'],true);
						
						//print_r($workflow_order);exit;
						if(isset($action)){
						
						//action
						switch ($action) {
							case 'approval':
							
								//clear data infront
								$array = array();
							
								//approved
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];

										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
										
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'approval_sendmail':
							
								//clear data infront
								$array = array();
							
								//approved
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];

										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
										
										//sent mail to service manager
										$role_service_manager = $this->Employee_model->get_related_role(array($workflow_order['who_email']));
										$sitename = $this->Settings_model->get_settings(1);
										$default_email = $this->Settings_model->get_settings(6);
										
										$this->load->library('email');
										
										if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
											foreach($role_service_manager as $k => $v){
												
												$this->email->clear();
												$this->email->from($default_email['value'], $sitename['value']);
												$this->email->to($v['email']); 
												$this->email->subject('Service request completed');
												$this->email->message($sr_data['service_serial'].' Service request had completed');
												$this->email->send();
											  
											}
										}
										
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'update':
								
								//next status
								$next_status = $workflow_order['next_status_id'];
								$next_status_text = $status[$workflow_order['next_status_id']];
								
								$array = array(
								);
								
								//check last job
								if($workflow_order['next_status_text'] == 'Completed'){
									$last = 1;
								}
								
								$next = 1;
								
								break;
							case 'approval_update':
								
								//clear data infront
								$array = array();
								
								if($confirm == 1){
									
									if($correct_check == 'on'){
										
										//next status
										$next_status = $workflow_order['next_status_id'];
										$next_status_text = $status[$workflow_order['next_status_id']];
										
										$target_colum = json_decode($workflow_order['target_colum'],true);
										
										//check update colum single or multiple
										foreach($target_colum as $k => $v){
										
											if($v['type'] == 'single'){
												
												if($v['data_type'] == 'string'){
													
												$array[$v['name']] = $this->input->post($v['name'],true);
												
												}else if($v['data_type'] == 'date'){
													
													$date = $this->input->post($v['name'], true);
													if(!empty($date)) {
													  $tmp = explode("/", $date);
													  $date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
													  $array[$v['name']] = $date;
													}
													
												}
											
											}else{
												
											}
											
										}
										$next = 1;
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
									}
									
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
							case 'upload':
							
								//clear data infront
								$array = array();
								
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'customer_confirmation_document'	=> $save_path,
											);	
											
											//check last job
											if($workflow_order['next_status_text'] == 'Completed'){
												$last = 1;
											}
											
											//next status
											$next_status = $workflow_order['next_status_id'];
											$next_status_text = $status[$workflow_order['next_status_id']];	
											$next = 1;
																							
									}	
									
								}
								
								break;	
							case 'complete':
							
								//clear data infront
								$array = array();
							
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'customer_confirmation_document'	=> $save_path,
											);	
										
									}	
								}
								
								break;	
								
						}
						
						//print_r($workflow_order);exit;
						//if need_det = 1
						
						if($workflow_order['need_det'] == 1){

							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$value = $this->input->post($v['target_colum'], true);
								//string or number
								if(is_numeric($v['value'])){
									$det_temp = $value." ".$v['logic']." ".$v['value'];
								}else{
									
									if($v['value'] == 'original'){
										$string = $sr_data['term_condition'];
									}else{
										$string = $v['value'];
									}
									
									$det_temp = "'".md5(trim($value))."' ".$v['logic']." '".md5(trim($string))."'";
								}
								
								
								$check = $this->parse_boolean($det_temp);
								
								//check
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
							}
							
						}

						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 8,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
						}
						
						}
						
				  
			  }
			  
			 	if(isset($next_status)){
					//main
					$array['awaiting_table']  = $awating_person;
					$array['status'] = $next_status;
					//job
					$jobArray['awaiting_role_id'] = $awating_person.',';
					$jobArray['status_id'] = $next_status;
					$jobArray['status_text'] = $next_status_text;
				}
				//print_r($array);exit;
			  //////////////////////////////////////////////////////////////////////////////////////
			  
			  $array['lastupdate_user_id'] = $this->data['userdata']['id'];
			  $array['modified_date'] = $now;
			  
			    if($next == 1){
				  $lastjob_update = array(
				  	'is_completed' => 1,
					'display' => 0,
				  );
				  $this->Job_model->update($jobArray['parent_id'], $lastjob_update);
				  if($last == 0){
				  $new_job_id = $this->Job_model->insert($jobArray);
				  $array['latest_job_id'] = $new_job_id;
				  }
			  	}	  
			  
			  $insert_id = $id;
			  $this->Field_service_order_model->update($id, $array);
			  
			   	  //mail
				  if(isset($new_job_id)){
				  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
				  $mail_array['related_role'] = $related_role;		
				  $mail_array['title'] = $workflow['title']." ".$fso_data['field_service_no']." ".$next_status_text;
				  }
			    
		  }
		  
		  //sent mail
		  if(isset($mail_array['related_role']) && !empty($mail_array['related_role'])) {
		  
		  	$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
			$sitename = $this->Settings_model->get_settings(1);
		  	$default_email = $this->Settings_model->get_settings(6);
			
		  	$this->load->library('email');
			
			if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
		  			  	
				foreach($related_role_data as $k => $v){
			 	$this->email->clear();
				$this->email->from($default_email['value'], $sitename['value']);
				$this->email->to($v['email']); 
				
				$this->email->subject($mail_array['title']);
				$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
				$this->email->send();	
				  
				}
			
			}
		  }
		  		  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		  
		  
	  }
	  
	  public function del($id){
		  $this->Field_service_order_model->delete($id);
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		
	  }  
	  
	  public function ajax_getServiceRequest(){
		  
		  	$keyword = $_POST['keyword'];
			$limit = $_POST['limit'];
			$page = $_POST['page'];
			
			$start = ($page-1) * $limit;
		  	$record_count = $this->Service_request_model->ajax_record_count($keyword,array(
				'status' => 4,
			));
		  	$data = $this->Service_request_model->ajax_purchase_request($keyword, $limit, $start,array(
				'status' => 4,
			));
			
			$sr_status_list = $this->Service_request_model->sr_status_list();
			
			//顯示Status為狀態文字, 不是數字
			foreach($data as $k=>$v) {
				$data[$k]['status'] = $sr_status_list[$v['status']];
			}
			
		  	$paging = $this->get_paging($limit,10,$record_count,$page);
		  	
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					'paging'	=> $paging,
			);
			
			echo json_encode($temp);	
			exit;
	  }	  
	  
	  public function ajax_getSR(){
		  
		  	$id = $_POST['id'];
		  	$data = $this->Service_request_model->get($id);
			$customer_id = $data['customer_id'];
			$customer_data = $this->Customers_model->get($customer_id);
			
		
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					'customer_data' =>$customer_data,
			);
				
			echo json_encode($temp);	
			exit;
	  }	 
	  
	  public function get_paging($item_per_page,$pagenum,$total_item,$page)
	  {
	
		$start = (int)(($page-1)/$pagenum)*$pagenum+1;
		$end = $start+$pagenum-1;
		$next = $page+1;
		$pre = $page-1;
		
		$total_page = ceil( $total_item / $item_per_page );
                $paging = '';
		if($total_item > $item_per_page){
                    $paging .= '<ul class="pagination">';

                    if($page > 1){
                            $paging .= '<li><a href="javascript:changePage(1)">&laquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$pre.')">&lsaquo;</li>';
                    }

                    if($total_page <= $pagenum){

                            for($i=$start;$i<=$total_page;$i++){
                                    if($i == $page){

                                            $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                    }else{

                                            $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                    }
                            }
                    }else{
                            if($page > 5){
                                    $end = $page+5;
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    $start = $end - ($pagenum - 1);

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }else{
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }	
                    }

                    if($page < $total_page){
                            $paging .= '<li><a href="javascript:changePage('.$next.')">&rsaquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$total_page.')">&raquo;</a></li>';
                    }

                    $paging .= '</ul>';
                }
		
		return $paging;
	  }	  
	  
	  private function PDF_generation($id, $mode='I') {
		  
		  $fsr_data = $this->Field_service_order_model->get($id);	
		  $customer = $this->Customers_model->get($fsr_data['customer_id']);
		  
		  $time_schedule = json_decode($fsr_data['time_schedule'],true);
		  
		  $timeGroup = '';
		  foreach($time_schedule as $k => $v){
			  $timeGroup .='<tr><td>'.$v['schedule_date'].'</td><td>'.$v['schedule_day'].'</td><td>'.$v['schedule_startTime'].'</td><td>'.$v['schedule_endTime1'].'</td><td>'.$v['totalHour'].'</td></tr>';
		  }
		  
		  $this->load->library("Pdf");		
          $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		  
		  // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Anexus');
            $pdf->SetTitle("Return Material Authorization");
            $pdf->SetSubject("");
            $pdf->SetKeywords("");

            // remove default header/footer
            $pdf->setPrintHeader(false);			
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(10, 10, 10);

            // set auto page breaks
            //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
            }

            // ---------------------------------------------------------

            // set font
            $pdf->SetFont('times', '', 10);

            

            // set font
            $pdf->SetFont('times', '', 10);
			
			// add a page
       		$pdf->AddPage();
			
			
			$html = '			
			<h1>Field Service Form</h1>
			
			<table width="100%" cellpadding="5">
				<tr>
					<td>
						<table  width="100%" cellpadding="5" border="1">
			
							<tr>
								<th><h3>Field Service Information</h3></th>
							</tr>
							<tr>
								<th><h4>Serial No.</h4></th>
							</tr>
							<tr>
								<td>'.$fsr_data['field_service_no'].'</td>
							</tr>
							<tr>
								<th><h4>Product/Model</h4></th>
							</tr>
							<tr>
								<td>aNexus</td>
							</tr>
							<tr>
								<th><h4>Serial No</h4></th>
							</tr>
							<tr>
								<td>'.$fsr_data['product_serial'].'</td>
							</tr>
							<tr>
								<th><h4>Service</h4></th>
							</tr>
							<tr>
								<td>'.$fsr_data['service'].'</td>
							</tr>
							<tr>
								<th><h4>Service Type</h4></th>
							</tr>
							<tr>
								<td>'.$fsr_data['service_type'].'</td>
							</tr>
							<tr>
								<th><h4>Tool ID</h4></th>
							</tr>
							<tr>
								<td>'.$fsr_data['tool_id'].'</td>
							</tr>
						</table>
					</td>
					
					<td>
						<table  width="100%"  cellpadding="5" border="1">
			
							<tr>
								<th><h3>Customer Information</h3></th>
							</tr>
							<tr>
								<th><h4>Company Name *</h4></th>
							</tr>
							<tr>
								<td>'.$customer['company_name'].'</td>
							</tr>
							<tr>
								<th><h4>Name</h4></th>
							</tr>
							<tr>
								<td>'.$customer['contact_person'].'</td>
							</tr>
							<tr>
								<th><h4>Address</h4></th>
							</tr>
							<tr>
								<td>'.$customer['address'].'</td>
							</tr>
							<tr>
								<th><h4>Postal Code</h4></th>
							</tr>
							<tr>
								<td>'.$customer['postal'].'</td>
							</tr>
							<tr>
								<th><h4>Email</h4></th>
							</tr>
							<tr>
								<td>'.$customer['email'].'</td>
							</tr>
							<tr>
								<th><h4>Contact</h4></th>
							</tr>
							<tr>
								<td>'.$customer['contact_info'].'</td>
							</tr>
	
						</table>
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
						<table  width="100%"  cellpadding="5" border="1">
			
							<tr>
								<th><h3>More Information</h3></th>
							</tr>
							<tr>
								<th><h4>Faults</h4></th>
							</tr>
							<tr>
								<td>'.$fsr_data['faults'].'</td>
							</tr>
							<tr>
								<th><h4>Fault\'s Reference Images</h4></th>
							</tr>
							<tr>
								<td><img width="200px" src="'.$fsr_data['faults_reference'].'"/></td>
							</tr>
							<tr>
								<th><h4>Actions and Remedy</h4></th>
							</tr>
							<tr>
								<td>'.$fsr_data['actions_remedy'].'</td>
							</tr>
							<tr>
								<th><h4>Actions and Remedy\'s Reference Images</h4></th>
							</tr>
							<tr>
								<td><img width="200px" src="'.$fsr_data['actions_remedy_ref'].'"/></td>
							</tr>
							<tr>
								<th><h4>Spare Parts Used</h4></th>
							</tr>
							<tr>
								<td>'.$fsr_data['spare_part_used'].'</td>
							</tr>
							<tr>
								<th><h4>Spare Parts Used\'s Reference Images</h4></th>
							</tr>
							<tr>
								<td><img width="200px" src="'.$fsr_data['spare_part_ref'].'"/></td>
							</tr>
	
						</table>
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
						<table  width="100%"  cellpadding="5" border="1">
			
							<tr>
								<th colspan="5"><h3>Time Schedule</h3></th>
							</tr>
							<tr>
								<th><h4>Date</h4></th>
								<th><h4>Day</h4></th>
								<th><h4>Start Time</h4></th>
								<th><h4>End Time</h4></th>
								<th><h4>Total Hours</h4></th>
							</tr>'.$timeGroup.'
	
						</table>
					</td>
				</tr>
				
			</table>
			
			';						
			
			$pdf->writeHTML($html, true, false, true, false, '');
			
			$file_name = date("YmdHis").rand(1000,9999);
			
			if($mode == 'I') {
				$pdf->Output($file_name.'.pdf', 'I'); 	
				return $file_name.'.pdf';
			} else if ($mode == 'f') {				
				$pdf->Output('./uploads/'.$file_name.'.pdf', 'f'); 
				return './uploads/'.$file_name.'.pdf';
			}
			
		  		  
		  
	  }
	  
	  public function export_pdf($id){		  
		  $this->PDF_generation($id, 'I');			
	  }
	  
	  public function sent_mail($id,$product_total=0,$service_total=0){
		  
		   	$filename = $this->PDF_generation($id, 'f');	 		  
			
			$fso_data = $this->Field_service_order_model->get($id);
		  	$customer = $this->Customers_model->get($fso_data['customer_id']);
		  
		  	$send_to = $customer['email'];
			
			//$requestor_role
			$requestor_employee = $this->Employee_model->get($fso_data['requestor_id']);
			$requestor_role = $requestor_employee['role_id'];		
			
			$role_id = $this->data['userdata']['role_id'];
			
			$sitename = $this->Settings_model->get_settings(1);
		  	$default_email = $this->Settings_model->get_settings(6);
			
			//$related_roles = $this->Job_model->getRelatedRoleID($fso_data['latest_job_id']);	
			
			
			///////////////////////////////////////////////////////////
			
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $fso_data['status'],
							'workflow_id' => 8,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);

						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(8);
						$status = json_decode($workflow['status_json'],true);
						
						if(isset($action)){

		
						if($action == 'send_email'){
							
							//next status
							$next_status = $workflow_order['next_status_id'];
							$next_status_text = $status[$workflow_order['next_status_id']];
							$who_email = $workflow_order['who_email'];
	
						}

						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];
								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 8,
							);
							
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
							
							
							$jobArray = array(
								'parent_id' 		=>$fso_data['latest_job_id'],
								'user_id' 			=>$this->data['userdata']['id'],
								'customer_id' 		=>$fso_data['customer_id'],
								'type' => 'Field Service Order',
								'type_path' =>'service/field_service_order_edit/',
								'awaiting_role_id' 	=>$awating_person.',',
								'status_id' 		=>$next_status,
								'status_text'		=>$next_status_text,
								'serial' 			=>$fso_data['field_service_no'],
								'type_id' 			=>$fso_data['id'],
								'created_date'		=>date("Y-m-d H:i:s"),
								'modified_date'		=>date("Y-m-d H:i:s"),
							);			
					  
						    $array = array(
							'awaiting_table' => $awating_person.',',
							'status' => $next_status,
							'modified_date' => date('Y-m-d H:i:s'),
							'lastupdate_user_id' => $this->data['userdata']['id'],
						    );
							
						  //print_r($array);exit;
							
						
						  //更新上一個JOB的狀態					  
						  $this->Job_model->update($jobArray['parent_id'], array(
							'is_completed' => 1,
							'display' => 0,
						  ));
						  
						  //新增一個新的JOB
						  $new_job_id = $this->Job_model->insert($jobArray);
						  $array['latest_job_id'] = $new_job_id;
						  
						  $this->Field_service_order_model->update($fso_data['id'], $array);
							
							
						}
						
						}
			
			
			if(isset($who_email)){
			
				$this->load->library('email');
				
				switch($who_email){
					case 'customer':
							
							//Send Notification
							if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
										
								$this->email->from($default_email['value'], $sitename['value']);
								$this->email->to($send_to); 
							
								$this->email->subject('Anexus '.$workflow['title'].': '.$fso_data['field_service_no']);
								$this->email->message($fso_data['field_service_no']);	
								$this->email->attach($filename);
							
								$this->email->send();
							
							}
							
					break;	
					case 'supplier':
						
							  //count supplier
							  $supplier_count = $this->Delivery_order_item_model->get_supplier_groupBy($id);
							  
							  foreach($supplier_count as $k => $v){
									
								  $supplier = $this->Suppliers_model->get($v['supplier_id']);
								  
								  $file_name = $this->PDF_generation($id, 'f',$v['supplier_id']);	
										 
								  if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
									
									$this->load->library('email');
									$this->email->clear();	
									$this->email->from($default_email['value'], $sitename['value']);
									$this->email->to($supplier ['email']); 
									
									$this->email->subject('Anexus '.$workflow['title'].' request '.$fso_data['field_service_no']);
									$this->email->message($fso_data['field_service_no']);	
									$this->email->attach($file_name);
									
									$this->email->send();
									
								  }
							  
							  }
							
					break;
					case 'employee':
					break;
				
				}
				
			}
			
			 //mail
			 $mail_array = array(
				'creater_name' =>$this->data['userdata']['name'],
			 );
			 
			 if(isset($new_job_id)){
			  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
			  $mail_array['related_role'] = $related_role;		
			  $mail_array['title'] = $workflow['title']." ".$fso_data['field_service_no']." ".$next_status_text;
			 }
			 
			 //sent Notification to related role						  			
		  	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local" && isset($mail_array['related_role'])) {
				$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
			  				  	
			  	foreach($related_role_data as $k => $v){
				  
				  	$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
					
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->send();	
				  
		 		}
			}
			
			///////////////////////////////////////////////////////////
			
			//$requestor_role
			//$requestor_employee = $this->Employee_model->get($fso_data['requestor_id']);
			//$requestor_role = $requestor_employee['role_id'];
			
			/*
		  	$this->load->library('email');
			
			//Send Notification
		  	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
		  			  	
				$this->email->from($default_email['value'], $sitename['value']);
				$this->email->to($send_to); 
			
				$this->email->subject('Anexus Quotation: '.$fso_data['field_service_no']);
				$this->email->message($fso_data['field_service_no']);	
				$this->email->attach($filename);
			
				$this->email->send();
			
			}
			
			
			define('GENERAL_MANAGER', 2);
		    define('LOGISTIC', 6);
		    define('SALES_MANAGER', 7);
		    define('SALES_EXECUTIVE', 8);	
			
			
			//finance send_out
			if($fso_data['status'] == 3){
				  if(in_array($this->data['userdata']['role_id'], array(FINANCE))){
					  			 
					  $jobArray = array(
						'parent_id' =>$fso_data['latest_job_id'],
						'user_id' =>$this->data['userdata']['id'],
						'customer_id' =>$fso_data['customer_id'],
						'type' => 'Field Service Order',
						'type_path' =>'service/field_service_order_edit/',
						'awaiting_role_id' => FINANCE.','.SALES_MANAGER.','.SALES_EXECUTIVE.',',
						'status_id' => 4,
						'status_text'=>$this->data['fso_status_list'][4],
						'serial' =>$fso_data['field_service_no'],
			  			'type_id' =>$fso_data['id'],
						'created_date'	=> date("Y-m-d H:i:s"),
						'modified_date'	=> date("Y-m-d H:i:s"),
					  );			
					  
					  $array = array(
					  	'awaiting_table' => FINANCE.','.SALES_MANAGER.','.SALES_EXECUTIVE.',',
						'status' => 4,
					  );
					  

					  //更新上一個JOB的狀態					  
					  $this->Job_model->update($jobArray['parent_id'], array(
					  	'is_completed' => 1,
					  ));
					  
					  //新增一個新的JOB
					  $new_job_id = $this->Job_model->insert($jobArray);
					  $array['latest_job_id'] = $new_job_id;
					  
					  $this->Field_service_order_model->update($fso_data['id'], $array);
				  }
				  
			}
			
			$mail_array = array(
				'creater_name' =>$this->data['userdata']['name'],
				'title' =>$fso_data['field_service_no'].' '.$this->data['fso_status_list'][4],
				'related_role' =>$related_roles,
			 );
					  
					  
			//sent Notification to related role						  			
		  	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
				$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
			  				  	
			  	foreach($related_role_data as $k => $v){
				  
				  	$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
					
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->send();	
				  
		 		}
			}
			*/
			
			$lastpage = $this->session->userdata("lastpage");
			if(!empty($lastpage)) {
			  redirect($lastpage,'refresh');  
			} else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
			}		 			
		  
	  }

}

?>