<?php

class Administrator_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();      
			$this->load->model('User_model');
            $this->load->model('function_model');

            $this->data['init'] = $this->function_model->page_init();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$this->data['userdata'] = $this->session->userdata("userdata");
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}  
           
      }
   
      public function index() {  
          			
            
			
      }
	  
	  public function employee() {  
          			
            $this->data['title'] = ucfirst("Employee"); // Capitalize the first letter		
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/administrator/manage-employee', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }
	  
	  public function customers() {  
          			
            $this->data['title'] = ucfirst("Customers"); // Capitalize the first letter		
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/administrator/manage-customers', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	
	  
	  public function suppliers() {  
          			
            $this->data['title'] = ucfirst("Suppliers"); // Capitalize the first letter		
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/administrator/manage-suppliers', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }
	  
	  public function products() {  
          			
            $this->data['title'] = ucfirst("Customers"); // Capitalize the first letter		
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/administrator/manage-products', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }  
	  
	  public function services() {  
          			
            $this->data['title'] = ucfirst("Customers"); // Capitalize the first letter		
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/administrator/manage-services', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  	  

}

?>