<?php

class Service_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();      
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');

            $this->data['init'] = $this->function_model->page_init();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$this->data['userdata'] = $this->session->userdata("userdata");
			}  
			
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			} 
			   
           
      }
   
      public function index() {  
          			
            
			
      }
	  
	  public function field_service_order() {  
          			
            $this->data['title'] = ucfirst("Field Service Order"); // Capitalize the first letter		
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/service/field-service-order', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }
	  
	  public function inventory_request() {  
          			
            $this->data['title'] = ucfirst("Commercial Invoice"); // Capitalize the first letter		
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/service/inventory-request', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  	  
	  
	  public function return_material_authorization(){
		  
		  	$this->data['title'] = ucfirst("Return Material Authorization"); // Capitalize the first letter		
          	$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/service/return-material-authorization', $this->data);
            $this->load->view('anexus/footer', $this->data);
		  
	  }

}

?>