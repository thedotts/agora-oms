<?php

class Employee_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();  
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Employee_model');
			$this->load->model('Role_model');
			$this->load->model('Department_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Audit_log_model');
			$this->load->model('Customers_model');
			
			
            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$userdata = $this->session->userdata("userdata");
				
				
				if(strpos($userdata['role_id'],',')){
					$userdata['role_id'] = explode(',',$userdata['role_id']);	
				}else{
					$userdata['role_id'] = array($userdata['role_id']);	
				}
				
				
				$this->data['userdata'] = $userdata;
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}  
			
			$this->data['role_list'] = $this->Role_model->getIDKeyArray("name");
			$this->data['status_list'] = $this->Employee_model->status_list(false);
			
			$this->data['group_name'] = "administrator";  
			$this->data['model_name'] = "employee";  
			$this->data['common_name'] = "Employee Account";   
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			if(in_array(3,$this->data['userdata']['role_id'])){
				
				
				$a_csutomer = $this->data['staff_info']['assign_customer'];
				
				if($a_csutomer != ''){
				
					if(strpos($a_csutomer,',')){
						$a_customer_array = explode(',',$a_csutomer);	
					}else{
						$a_customer_array = array($a_csutomer);	
					}
				
				}else{
					$a_customer_array = array();	
				}
				
				$this->data['task_display_count'] = $this->Job_model->record_count2(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
				),$this->data['userdata']['role_id'],$a_customer_array);
				
				
				

			}else{
			
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			}
			
			/*
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}
			*/
           
      }
   
      public function index($q="ALL", $page=1, $alert=0) {  
          		
			$this->data['alert'] = $alert;	
            $this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
				'role_id !='	=> 7,
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/';
			if($q == 'ALL') {
				$q = "";
			}else{
				$q = urldecode($q);
			}
			$this->data['q'] = urldecode($q);
									
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Employee_model->record_count($filter, $q);
			
			//get particular ranged list
			$result = $this->Employee_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start);
			
			if(!empty($result)){
			
			foreach($result as $k => $v){
				
				if(strpos($v['role_id'],',')){
					$result[$k]['role_id'] = explode(',',$v['role_id']);	
				}else{
					$result[$k]['role_id'] = array($v['role_id']);	
				}
				
			}
			
			}
			
			$this->data['results'] = $result;
					
			//print_r($this->data['results']);exit;
						
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->session->set_userdata("lastpage", $url.$page);
			
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  
	  public function add($id=false) {  
	  
          	if($id !== false) {
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Employee_model->get($id);	
				$this->data['result']['role_id'] = explode(',',$this->data['result']['role_id']);
			} else {
				$this->data['mode'] = 'Add';
			}
			
			//print_r($this->data['result']);exit;
			
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		            
			
      }	  	
	  
	  public function del($id) {
		  
		  $employee_data = $this->Employee_model->get($id);		  
		  $this->Employee_model->delete($id);
		  $this->User_model->delete($employee_data['user_id']);
		  
		  //audit log
		  $log_array = array(
		  	'ip_address'	=> $this->input->ip_address(),
			'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
			'table_affect'	=> 'employee',
			'description'	=> 'Delete employee',
			'created_date'	=> date('Y-m-d H:i:s'),
		  );
		  
		  $audit_id = $this->Audit_log_model->insert($log_array);	
		  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
		  $update_array = array(
			 'log_no'	=> $custom_code,
		  );
		  $this->Audit_log_model->update($audit_id, $update_array);		
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage.'/3','refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/3'));
		  }		
		  
	  }  
	  
	  public function submit(){
		  
		  //print_r($_POST);exit;
		  
		  $mode = $this->input->post("mode", true);
		  $id = $this->input->post("id", true);
		  
		  
		  //multi role
		  $role = $this->input->post("role", true);
		  if(!empty($role)){
		  	$role_id = implode(',',$role);
		  }else{
			$role_id = '';
		  }
		  
		  //print_r($role_id);exit;
		  
		  $status = $this->input->post("status", true);
		  $email = $this->input->post("email", true);
		  
		  $password = $this->input->post("password", true);
		  $copassword = $this->input->post("copassword", true);
		  //$department = $this->input->post("department", true);
		  
		  $bankname = $this->input->post("bankname", true);
		  $bank_code = $this->input->post("bank_code", true);
		  $bank_branch = $this->input->post("bank_branch", true);
		  $account_no = $this->input->post("account_no", true);
		  
		  $full_name = $this->input->post("full_name", true);
		  $ic_no = $this->input->post("ic_no", true);
		  $date_of_birth = $this->input->post("date_of_birth", true);
		  $dob = "";
		  if(!empty($date_of_birth)) {
			  $tmp = explode("/", $date_of_birth);
			  $dob = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
		  }
		  
		  
		  $home_address = $this->input->post("home_address", true);
		  $contact_info = $this->input->post("contact_info", true);
		  $passport_no = $this->input->post("passport_no", true);
		  
		  $expired_date = $this->input->post("expired_date", true);
		  
		  $exp = "";
		  if(!empty($expired_date)) {
			  $tmp = explode("/", $expired_date);
			  $exp = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
		  }
		  
		  $emergency_contact_name = $this->input->post("emergency_contact_name", true);
		  $emergency_contact_relationship = $this->input->post("emergency_contact_relationship", true);
		  $emergency_contact_no = $this->input->post("emergency_contact_no", true);
		  
		  $iu_array = array(
		  	'role_id'						=> $role_id,
			'status'						=> $status,
			'email'							=> $email,
			'bankname'						=> $bankname,
			'bank_code'						=> $bank_code,
			'bank_branch'					=> $bank_branch,
			'account_no'					=> $account_no,
			'full_name'						=> $full_name,
			'ic_no'							=> $ic_no,
			'date_of_birth'					=> $dob,
			'home_address'					=> $home_address,
			'contact_info'					=> $contact_info,
			'passport_no'					=> $passport_no,
			'expired_date'					=> $exp,
			'emergency_contact_name'		=> $emergency_contact_name,
			'emergency_contact_relationship'=> $emergency_contact_relationship,
			'emergency_contact_no'			=> $emergency_contact_no,
		  );
		  
		  
		  //Add
		  if($mode == 'Add') {			  			  
			  
			  //Check whether this email is already exists
			  $email_exists = $this->Employee_model->get_where(array(
		  		'email'	=> $email,
				'is_deleted'=>0,
		  	  ));
			  if(count($email_exists) > 0) {
				show_error("This email is already exists in the system, please change another email");  
			  }		
			  
			  $iu_array['created_date'] = date("Y-m-d H:i:s");
			  
			  $employee_id = $this->Employee_model->insert($iu_array);
			  
			  $usr_array = array(
				'role_id'		=> $role_id,
				'email'			=> $email,
				'name'			=> $full_name,
				'passwd'		=> md5($password),
				'employee_id'	=> $employee_id,
				'created_date'	=> date("Y-m-d H:i:s"),				
			  );
			  			  
			  
			  
			  //when we get user_id, we update employee table			  
			  $user_id = $this->User_model->insert($usr_array);			  
			  $u_array = array(
			  	'user_id' 		=> $user_id,
				'employee_no'	=> $this->Employee_model->zerofill($employee_id, 5),
			  );
			  $this->Employee_model->update($employee_id, $u_array);	
			  
			  //audit log
			  $log_array = array(
			  	'ip_address'	=> $this->input->ip_address(),
				'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
				'table_affect'	=> 'employee',
				'description'	=> 'Added new employee',
				'created_date'	=> date('Y-m-d H:i:s'),
			  );
			  
			  $audit_id = $this->Audit_log_model->insert($log_array);	
			  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
			  $update_array = array(
			  	'log_no'	=> $custom_code,
			  );
			  $this->Audit_log_model->update($audit_id, $update_array);		  	  			  
			  
		  //Edit	  			  
		  } else {
			  
			  $iu_array['modified_date'] = date("Y-m-d H:i:s");
			  $iu_array['role_id'] = $role_id;
			  
			  $this->Employee_model->update($id, $iu_array);
			  
			  $usr_array = array(
				'role_id'		=> $role_id,
				//'email'			=> $email,
				//'name'			=> $full_name,				
				'employee_id'	=> $id,
				'modified_date'	=> date("Y-m-d H:i:s"),				
			  );
			  
			  if(!empty($password)) {
					$usr_array['passwd'] = md5($password);					  
			  }
			  
			  $employee_data = $this->Employee_model->get($id);
			  
			  			  
			  $this->User_model->update($employee_data['user_id'], $usr_array);
			  //echo $this->db->last_query();exit;
			  //audit log
			  $log_array = array(
			  	'ip_address'	=> $this->input->ip_address(),
				'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
				'table_affect'	=> 'employee',
				'description'	=> 'Edit employee',
				'created_date'	=> date('Y-m-d H:i:s'),
			  );

			  $audit_id = $this->Audit_log_model->insert($log_array);	
			  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
			  $update_array = array(
			  	'log_no'	=> $custom_code,
			  );
			  $this->Audit_log_model->update($audit_id, $update_array);	
			  
			  
		  }
		  		  		 		  		  
		  //alert
		  if($mode == 'Add'){
			$alert_type = '/1';
		  }else{
			$alert_type = '/2';
		  }
		  
		  		  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage.$alert_type,'refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].$alert_type));
		  }		  
		  
	  }
	  
	  public function check_email(){
			
			$userdata = $this->session->userdata("userdata");
			$email = $_POST['email'];
			$user_id = $_POST['id'];
			
			$check_val = $this->User_model->getUserByEmail2($email,$user_id);
			//echo $this->db->last_query();
			$check_val2 = $this->Customers_model->getCustomerByEmail2($email,$user_id);
			//echo $this->db->last_query();exit;
			
			$email_isset = 0;
			if($check_val != false || $check_val2 != false){
				$email_isset = 1;
			}
			
			$json_array = array(
				'status' 	=> 'ok',
				'email'		=> $email_isset,
			);
			
			echo json_encode($json_array);exit;
			
		  
	  }
	  

}

?>