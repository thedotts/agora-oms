<?php

class Logistics_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();           
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');

            $this->data['init'] = $this->function_model->page_init();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$this->data['userdata'] = $this->session->userdata("userdata");
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}  
			
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			} 
           
      }
   
      public function index() {  
          			
            
			
      }
	  
	  public function delivery_order() {  
          			
            $this->data['title'] = ucfirst("Delivery Order"); // Capitalize the first letter		
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/logistics/delivery-order', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }
	  
	  public function commercial_invoice() {  
          			
            $this->data['title'] = ucfirst("Commercial Invoice"); // Capitalize the first letter		
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/logistics/commercial-invoice', $this->data);
            $this->load->view('anexus/footer', $this->data);	
      }	  	  

}

?>