<?php

class Invoice_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();  
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Customers_model');
			$this->load->model('Tender_model');
			$this->load->model('Tender_item_model');
			$this->load->model('Employee_model');
			$this->load->model('Department_model');
			$this->load->model('Products_model');
			$this->load->model('Suppliers_model');
			$this->load->model('Shipping_model');
			$this->load->model('Products_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Settings_model');
			$this->load->model('Quotation_model');
			$this->load->model('Quotation_item_model');
			$this->load->model('Workflow_model');
			$this->load->model('Workflow_order_model');
			$this->load->model('Order_model');
			$this->load->model('Product_log_model');
			$this->load->model('Order_item_model');
			$this->load->model('Packing_list_model');
			$this->load->model('Packing_list_item_model');
			$this->load->model('Delivery_order_model');
			$this->load->model('Delivery_order_item_model');
			$this->load->model('Invoice_model');
			$this->load->model('Invoice_item_model');
			$this->load->model('Audit_log_model');
			
            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
			
			$this->data['role_list'] = $this->Role_model->getIDKeyArray('name');
			$status_list = $this->Workflow_model->get(4);
			$this->data['status_list'] = json_decode($status_list['status_json']);
			
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$this->data['userdata'] = $this->session->userdata("userdata");
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}          
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			$this->data['group_name'] = "finance";  
			$this->data['model_name'] = "invoice";  
			$this->data['common_name'] = "Invoice";   
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
					
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}  
            
			//related role
		    define('SUPERADMIN', 1);
		    define('SALES_MANAGER', 7);
		    define('SALES_EXECUTIVE', 8);
		   
      }
   
      public function index($q="ALL", $page=1, $alert=0) {  
          		
			$this->data['alert'] = $alert;		
			$this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/';
			if($q == 'ALL') {
				$q = "";
			}
			$this->data['q'] = $q;
					
			$limit_start = ($page-1)*$this->data['item_per_page'];

			//count total Data
			$this->data["total"] = $this->Invoice_model->record_count($filter, $q);
			
			//get particular ranged list
			$this->data['results'] = $this->Invoice_model->fetch($q, $this->data['item_per_page'], $limit_start);
			
			//抓目前status
			if(!empty($this->data['results'])){
			foreach($this->data['results'] as $k => $v){
				
				//status is completed
				if($v['status'] == 1){
					
					$related_packing = $this->Packing_list_model->get_where(array('is_deleted'=>0,'status'=>1,'order_id'=>$v['order_id']));
					
					if(!empty($related_packing)){

						//order 的數量
						$order_item = $this->Order_item_model->getRelatedItem($v['order_id']);
						$order_qty = 0;
						if(!empty($order_item)){
							foreach($order_item as $k2=>$v2){
								$order_qty += $v2['quantity'];
							}
						}
						
						//packing 數量
						$packing_qty = 0;
						foreach($related_packing as $k2 => $v2){
							
							$packing_item = $this->Packing_list_item_model->getRelatedItem($v2['id']);
							
							foreach($packing_item as $k3 => $v3){
								$packing_qty += $v3['quantity_pack'];
							}
							
						}
						
						//判斷item pack完了沒
						if($order_qty == $packing_qty){
							
							//packing list 的數量
							$pack_count = count($related_packing);
							
							//目前有的delivery order
							$related_do = count($this->Delivery_order_model->get_where(array('is_deleted'=>0,'status'=>2,'order_id'=>$v['order_id'])));
							
							if($related_do == 0){
								$this->data['results'][$k]['status_text'] = 'Pending Delivery Order';
							//代表do已處理完畢
							}else if($related_do == $pack_count){
								
								//do 數量
								$do_count = $related_do;
								
								//invoice 數量
								$related_invoice = count($this->Invoice_model->get_where(array('is_deleted'=>0,'status'=>1,'order_id'=>$v['order_id'])));
								
								if($related_invoice == 0){
									$this->data['results'][$k]['status_text'] = 'Pending Invoice';
								}else if($do_count == $related_invoice){
									$this->data['results'][$k]['status_text'] = 'Completed';
								}else{
									$this->data['results'][$k]['status_text'] = 'Pending Generate Invoice ['.$related_invoice.' of '.$do_count.']';
								}
								
							}else{
								$this->data['results'][$k]['status_text'] = 'Pending Generate Delivery Order ['.$related_do.' of '.$pack_count.']';
							}
							
						}else{
							$this->data['results'][$k]['status_text'] = 'Pending Generate Packing List ['.$packing_qty.' of '.$order_qty.']';
						}
						
					}else{
						$this->data['results'][$k]['status_text'] = 'Pending Packing List';
					}
					
				}//endif status
				
			}//end foreach
			}
			
			if(!empty($this->data['results'])) {		
				foreach($this->data['results'] as $k => $v){
					if(!empty($v['awaiting_table'])){
							$this->data['results'][$k]['awaiting_table']= explode(',',$v['awaiting_table']);
					}
				}		
			}
				
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->data['customer_list'] = $this->Customers_model->getIDKeyArray("company_name");
			$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
			
			$this->session->set_userdata("lastpage", $url.$page);		
						
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  
	  public function add($id=false,$job_id=false) {  
          			
			$role_id = $this->data['userdata']['role_id'];
			$this->data['gst'] = $this->Settings_model->get_settings(5);
			//print_r($gst);exit;
					
			if($id !== false && $job_id===false) {
				
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Invoice_model->get($id);
					
				$invoice_date = $this->data['result']['invoice_date'];
				$tmp = explode('-',$invoice_date);
				$this->data['result']['invoice_date'] = $tmp[2]."/".$tmp[1]."/".$tmp[0];
				
				//customer infomation
				$this->data['customer_info'] = $this->Customers_model->get($this->data['result']['customer_id']);
				
				$product = $this->Invoice_item_model->getRelatedItem($id);
				$this->data['result']['product'] = $product;
				$this->data['product_list'] = $this->Products_model->getIDKeyArray('product_name');
				
				$requestor_employee = $this->Employee_model->get($this->data['result']['requestor_id']);
			    $requestor_role = $requestor_employee['role_id'];
				$this->data['requestor_role'] = $requestor_role;
				if(!empty($this->data['result']['awaiting_table'])){
				$this->data['result']['awaiting_table'] = explode(',',$this->data['result']['awaiting_table']);
				}
				
				$default = 0;
				
				//get related workflow order data
				$workflow_flitter = array(
					'status_id' => $this->data['result']['status'],
					'workflow_id' => 4,
				);
						
				$type='';
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						
				//print_r($requestor_role);exit;
				//$workflow_order['role_id'] == 'requestor'
				if($workflow_order['role_id'] == 'requestor'){
							
				//requestor edit
				if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
				$type = 'edit';	
								
				//user role = current status role
				}else if($role_id == $requestor_role){
					$type = $workflow_order['action'];
				}
							
				//$workflow_order['role_id'] != 'requestor'						
				}else{
							
					//user role = current status role
					if ($workflow_order['role_id'] == $role_id){
						$type = $workflow_order['action'];	
							
					//requestor edit															
					}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
						$type = 'edit';	
					}
						
				}
						
				//data form database
				$workflow = $this->Workflow_model->get(4);
				$status = json_decode($workflow['status_json'],true);
						
				$this->data['workflow_title'] = $workflow['title'];
					
				$this->data['btn_type'] = $type;
				//print_r($workflow_order);exit;

					switch($type){
						case 'edit':
							$this->data['head_title'] = 'Edit '.$workflow['title'].' Request';
						break;
						case 'approval':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
						break;
						case 'approval_update':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'update':
							$this->data['head_title'] = 'Update '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'send_email':
							$this->data['head_title'] = 'Waiting to Send '.$workflow['title'].' Request';
						break;
						case 'sendmail_update':
							$this->data['head_title'] = 'Waiting to Send/Edit '.$workflow['title'].' Request';
						break;
						case 'upload':
						case 'upload_update':
							$this->data['head_title'] = 'Upload Confirm File '.$workflow['title'].' Request';
						break;
						case 'complete':
							$this->data['head_title'] = $workflow['title'].' Request';
							$this->data['last_action'] = $workflow_order['action_before_complete'];
						break;
						default:
							$this->data['head_title'] = $workflow['title'].' Request';
					}
					
				foreach(json_decode($workflow['status_json'],true) as $k => $v){
						
					if($v == 'Completed'){
						$this->data['completed_status_id'] = $k;
						break;
					}
				}

				
			} else {
				
				$this->data['mode'] = 'Add';
				$this->data['head_title'] = 'New Order';	

				//get related workflow order data
					$workflow_flitter = array(
						'role_id' => $role_id,
						'status_id' => -1,
						'workflow_id' => 4,
					);
					
					//data form database
					$workflow = $this->Workflow_model->get(4);
					$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
					
					//get view bottom layout
					$type = '';
					if(!empty($workflow_order)){
						$type = $workflow_order['action'];
						if($workflow_order['action'] == 'create'){
							$this->data['head_title'] = 'Create '.$workflow['title'].' Request';
						}else{
							$this->data['head_title'] = $workflow['title'].' Request';
						}
					}else{
						$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					$this->data['btn_type'] = $type;
					
					//auto select packing
					$this->data['do_id'] = 0;
					if($id !== false){
					$this->data['do_id'] = $id;
					}
					//for complete the job
					$this->data['job_id'] = $job_id;
					
			}		
			
			//all product 
			$products = $this->Products_model->get();
			//sum product by id
			foreach($products as $k => $v){
				$products[$k]['qty'] = $this->Product_log_model->sum_by_id($v['id']);
			}
			
			$this->data['product'] = json_encode($products);
			
			$this->data['company_list'] = json_encode($this->Customers_model->getAll());
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);	
			
      }
	  
	  public function submit(){
		  
		  $now = date("Y-m-d H:i:s");
		  $mode = $this->input->post("mode", true);
		  $id = $this->input->post("id", true);
		  
		  $role_id = $this->data['userdata']['role_id'];

		  //form data
		  $requestor_id 	= $this->input->post("requestor_id", true);
		  $invoice_date 	= $this->input->post("invoice_date", true);
		  $customer_id 		= $this->input->post("customer_id", true);
		  $customer_name 	= $this->input->post("customer_name", true);
		  $delivery_address = $this->input->post("delivery_address", true);
		  $do_id 			= $this->input->post("do_id", true);
		  $order_id 		= $this->input->post("order_id", true);
		  $order_no 		= $this->input->post("order_no", true);
		  $staff_name 		= $this->input->post("staff_name", true);
		  $staff_role 		= $this->input->post("staff_role", true);
		  $notes 			= $this->input->post("notes", true);
		  $total_amount 	= $this->input->post("total_amount", true);
		  $gst_amount 		= $this->input->post("gst_amount", true);
		  $nett_amount 		= $this->input->post("nett_amount", true);
		  

		  //get value approve or not approve
		  $confirm = $this->input->post("confirm", true);
		  $correct_check = $this->input->post("check_correct", true);
		  
		  $next = 0;
		  $last = 0;
		  $status_change = 0;
		  
		  if(!empty($invoice_date)) {
			  $tmp = explode("/", $invoice_date);
			  $invoice_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
		  }
		  
 		  //job
		  $jobArray = array(
			'parent_id' =>0,
			'user_id' =>$this->data['userdata']['id'],
			'customer_id' =>isset($customer_id)?$customer_id:'',
			'customer_name' =>isset($customer_name)?$customer_name:'',
			'type' => 'Invoice',
			'type_path' =>'finance/invoice_edit/',
	 	  );
 
 		  //main
		  $array = array(
		  	'order_no'			=> $order_no,
			'do_id'				=> $do_id,
		  	'order_id'			=> $order_id,
		  	'requestor_id'		=> $requestor_id,
		  	'invoice_date'		=> $invoice_date,
			'customer_id'		=> isset($customer_id)?$customer_id:'',
			'customer_name'		=> isset($customer_name)?$customer_name:'',
			'staff_name'		=> $staff_name,
			'staff_role'		=> $staff_role,
			'notes'				=> $notes,
			'total_amount'		=> isset($total_amount)?$total_amount:'',
			'gst_amount'		=> isset($gst_amount)?$gst_amount:'',
			'nett_amount'		=> isset($nett_amount)?$nett_amount:'',
		  );
		  
		  //mail
		  $mail_array = array(
		  'creater_name' =>$this->data['userdata']['name'],
		  );
		  
		  
		  $tender_id = "";
		  //Add
		  if($mode == 'Add'){
				
				//get related workflow order data
				$workflow_flitter = array(
					'role_id' => $role_id,
					'status_id' => -1,
					'workflow_id' => 4,
				);
				
				//data form database
				$workflow = $this->Workflow_model->get(4);
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
				$status = json_decode($workflow['status_json'],true);
				
				if(!empty($workflow_order)){
				
				//ad_hoc
				if($workflow_order['ad_hoc'] == 1){
					$array['ad_hoc'] = 1;
				}
				
				//purchaser
				$is_adhoc = false;
				if($role_id == 10){
					$is_adhoc = true;
				}
				
				//next status
				$next_status = $workflow_order['next_status_id'];
				$next_status_text = $status[$workflow_order['next_status_id']];
				
			  	//if need_det = 1
				if($workflow_order['need_det'] == 1){
					
					$formula = json_decode($workflow_order['formula'],true);
					
					//check if product or service price over value
					foreach($formula as $k => $v){
					
						$price = $this->input->post($v['target_colum'], true);
						$det_temp = $price." ".$v['logic']." ".$v['value'];
						$check = $this->parse_boolean($det_temp);
						
						//check price over value
						if($check){
							$next_status = $v['next_status_id'];
							$next_status_text = $status[$v['next_status_id']];
							break;
						}
						
					}
					
				}
				
				//get next status workflow order data
				$next_workflow_flitter = array(
					'status_id' => $next_status,
					'workflow_id' => 4,
				);
				
				//next status data
				$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
				$awating_person = $next_status_data['role_id'];
				
				if($awating_person == 'requestor'){
					$awating_person = $this->input->post("requestor_id", true);
				}
				
				//main
				$array['awaiting_table']  = $awating_person;
				$array['status'] = $next_status;
				//main
				$jobArray['awaiting_role_id'] = $awating_person.',';
				$jobArray['status_id'] = $next_status;
				$jobArray['status_text'] = $next_status_text; 	
				
				
				//if do_id != 0 ,get the latest job id of do
				if($array['do_id'] != ''){
					$data_previous_flow = $this->Delivery_order_model->get($array['do_id']);
					$jobArray['parent_id'] = $data_previous_flow['latest_job_id'];
					
					//update order job completed
					$last_job_array = array('is_completed'=>1);
					$this->Job_model->update($data_previous_flow['latest_job_id'],$last_job_array);
				}
				
				
				//update order job completed
				$data_previous_flow = $this->Delivery_order_model->get($array['do_id']);
				$last_job_array = array('is_completed'=>1);
				$this->Job_model->update($data_previous_flow['latest_job_id'],$last_job_array);
				  
				//insert main data
				$array['created_date'] = $now;
				$array['modified_date'] = $now;
				$array['create_user_id'] = $this->data['userdata']['id'];
				$array['lastupdate_user_id'] = $this->data['userdata']['id'];

				//新增資料
				$data_id = $this->Invoice_model->insert($array);	
	
				$p_id = $data_id;
					
				//更新serial
				$data_serial = $this->Invoice_model->zerofill($data_id, 5, $is_adhoc);
					
				//insert job
				$jobArray['serial'] = $data_serial;
				$jobArray['order_no'] = $array['order_no'];
				$jobArray['type_id'] = $data_id;
					
				$jobArray['created_date'] = $now;
				$jobArray['modified_date'] = $now;
				
				/*
				//if next action == complete,create job auto complete
				if($next_status_data['action'] == 'complete'){
					$jobArray['is_completed'] = 1;
				}
				*/
				$job_id = $this->input->post('job_id',true);
				if($workflow_order['next_status_text'] != 'Completed'){
					$job_id = $this->Job_model->insert($jobArray);
				}
				  
				//after insert job,update job id,qr serial....
					$array =array(
						'job_id'	=> $job_id,
						'invoice_no'  	=> $data_serial,
						'latest_job_id'	=> $job_id,
					);
					$this->Invoice_model->update($data_id, $array);
					
				//mail
				$related_role = $this->Job_model->getRelatedRoleID($job_id);
				$mail_array['related_role'] = $related_role;		
				$mail_array['title'] = $workflow['title']." ".$data_serial." ".$next_status_text; 
				  	
				}
				
				//check order completed
				$new_status = 0;
				
				/*
				//get related item
				$related_item = $this->Order_item_model->getRelatedItem($order_id);

				//sum product by id
				foreach($related_item as $k => $v){
					
					//get total packed
					$related_item[$k]['packed'] = $this->Packing_list_item_model->sumRelated_packing($order_id,$v['product_id']);		
					if($related_item[$k]['packed'] >= $v['quantity']){
						$new_status = 1;
					}
					
				}
				*/
				
				//get related do
				$related_do = $this->Delivery_order_model->get_where(array('is_deleted'=>0,'order_id'=>$order_id,'status'=>2));
				
				//get related invoice
				$related_invoice = $this->Invoice_model->get_where(array('is_deleted'=>0,'order_id'=>$order_id,'status'=>1));
				
				//echo count($related_invoice);echo count($related_do);exit;
				//do count = invoice ,mean completed
				if(count($related_do) == count($related_invoice)){
					$new_status = 1;
				}
				
				//order had completed
				if($new_status == 1){
					$this->Order_model->update($order_id,array('new_status' => 1));	
				}
				
				//task of auto create job invoice completed if job id != ''
				  $task_job_id = $this->input->post('job_id',true);
				  if($task_job_id != ''){
				  $this->Job_model->update($task_job_id, array(
				  	'is_completed' => 1,
					'display' => 0));
				  }
				
				//audit log
			  $log_array = array(
				'ip_address'	=> $this->input->ip_address(),
				'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
				'table_affect'	=> 'invoice',
				'description'	=> 'Create Invoice',
				'created_date'	=> date('Y-m-d H:i:s'),
			  );
				  
			  $audit_id = $this->Audit_log_model->insert($log_array);	
			  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
			  $update_array = array(
				 'log_no'	=> $custom_code,
			  );
			  $this->Audit_log_model->update($audit_id, $update_array);
				
				
		  //Edit	  			  
		  } else {
			  	
			  	$pre_data = $this->Delivery_order_model->get($id);
			 	$requestor_employee = $this->Employee_model->get($pre_data['requestor_id']);
			  	$requestor_role = $requestor_employee['role_id'];
			  	$pre_status = $pre_data['status'];
			  
			  	$jobArray['serial'] = $pre_data['invoice_no'];
				$jobArray['order_no'] = $pre_data['order_no'];
			  	$jobArray['type_id'] =$pre_data['id'];
			  	$jobArray['parent_id'] = $pre_data['latest_job_id'];
				
				//item parent id
				$p_id = $id;
				
			    //can edit status
			    $edit_status = $this->Workflow_order_model->get_status_edit(4);
				
			  
		//requestor edit
		if(in_array($pre_status, $edit_status) && $role_id == $requestor_role){

				//get related workflow order data
				$workflow_flitter = array(
					'role_id' => $role_id,
					'status_id' => -1,
					'workflow_id' => 4,
				);
						
				//data form database
				$workflow = $this->Workflow_model->get(4);
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
				$status = json_decode($workflow['status_json'],true);
				
					
				if(!empty($workflow_order)){
					print_r($workflow_order);exit;
					//next status
					$next_status = $workflow_order['next_status_id'];
					$next_status_text = $status[$workflow_order['next_status_id']];
						
					//if need_det = 1
					if($workflow_order['need_det'] == 1){
							
						$formula = json_decode($workflow_order['formula'],true);
							
						//check if product or service price over value
						foreach($formula as $k => $v){
							
							$price = $this->input->post($v['target_colum'], true);
							$det_temp = $price." ".$v['logic']." ".$v['value'];
							$check = $this->parse_boolean($det_temp);
								
							//check price over value
							if($check){
								$next_status = $v['next_status_id'];
								$next_status_text = $status[$v['next_status_id']];
								break;
							}
								
						}
							
					}
						
					//get next status workflow order data
					$next_workflow_flitter = array(
						'status_id' => $next_status,
						'workflow_id' => 4,
					);
						
					//next status data
					$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
					$awating_person = $next_status_data['role_id'];
						
					//if awating person equal requestor ,asign requestor for it
					if($awating_person == 'requestor'){
						$awating_person = $requestor_role;
					}
						
					//main
					$array['awaiting_table']  = $awating_person;
					$array['status'] = $next_status;
						
					//job
					$jobArray['awaiting_role_id'] = $awating_person.',';
					$jobArray['status_id'] = $next_status;
					$jobArray['status_text'] = $next_status_text;
						
						
				}
						
				  
		}else{
				  		
											
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $pre_status,
							'workflow_id' => 4,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(4);
						$status = json_decode($workflow['status_json'],true);
							
						//print_r($workflow_order);exit;
						if(isset($action)){
						
						//action
						switch ($action) {
							case 'approval':
							
								//clear data infront
								$array = array();
							
								//approved
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];

										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'update':
								
								//next status
								$next_status = $workflow_order['next_status_id'];
								$next_status_text = $status[$workflow_order['next_status_id']];
								
								$array = array(
								);
								
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
								$next = 1;
								
								break;
							case 'approval_update':
								
								//clear data infront
								$array = array();
								
								if($confirm == 1){
									
									if($correct_check == 'on'){
										
										//next status
										$next_status = $workflow_order['next_status_id'];
										$next_status_text = $status[$workflow_order['next_status_id']];
										
										$target_colum = json_decode($workflow_order['target_colum'],true);
										
										//check update colum single or multiple
										$array = array();
										foreach($target_colum as $k => $v){
										
											if($v['type'] == 'single'){
												
												if($v['data_type'] == 'string'){
													
												$array[$v['name']] = $this->input->post($v['name'],true);
												
												}else if($v['data_type'] == 'date'){
													
													$date = $this->input->post($v['name'], true);
													if(!empty($date)) {
													  $tmp = explode("/", $date);
													  $date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
													  $array[$v['name']] = $date;
													}
													
												}
											
											}else{
												
											}
											
										}
										$next = 1;
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
									}
									
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
							case 'upload':
								
								//clear data infront
								$array = array();
								
								if($confirm == 1){
								
								if(isset($_FILES['confirm_file'])){
				
									if ( $_FILES['confirm_file']['error'] == 0 && $_FILES['confirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['confirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['confirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'confirm_file'	=> $save_path,
											);	
											
											//next status
											$next_status = $workflow_order['next_status_id'];
											$next_status_text = $status[$workflow_order['next_status_id']];	
											$next = 1;
											//check last job
											if($workflow_order['next_status_text'] == 'Completed'){
												$last = 1;
											}		
											//echo $workflow_order['next_status_text'];exit;										
									}	
									
								}
								
								//no approve
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
							
							case 'upload_update':
								
								if($confirm == 1){
								
								if(isset($_FILES['confirm_file'])){
				
									if ( $_FILES['confirm_file']['error'] == 0 && $_FILES['confirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['confirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['confirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array['confirm_file']= $save_path;
											
											//next status
											$next_status = $workflow_order['next_status_id'];
											$next_status_text = $status[$workflow_order['next_status_id']];	
											$next = 1;
											//check last job
											if($workflow_order['next_status_text'] == 'Completed'){
												$last = 1;
											}		
											//echo $workflow_order['next_status_text'];exit;										
									}	
									
								}
								
								//no approve
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
									
							case 'complete':
							
								//clear data infront
								$array = array();
							
								if(isset($_FILES['confirm_file'])){
				
									if ( $_FILES['confirm_file']['error'] == 0 && $_FILES['confirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['confirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['confirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'confirm_file'	=> $save_path,
											);	
										
									}	
								}
								
								break;	
								
						}
						
						//print_r($workflow_order);exit;
						//if need_det = 1
						
						if($workflow_order['need_det'] == 1){

							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$value = $this->input->post($v['target_colum'], true);
								//string or number
								if(is_numeric($v['value'])){
									$det_temp = $value." ".$v['logic']." ".$v['value'];
								}else{
									
									if($v['value'] == 'original'){
										$string = $tender_data['term_condition'];
									}else{
										$string = $v['value'];
									}
									
									$det_temp = "'".md5(trim($value))."' ".$v['logic']." '".md5(trim($string))."'";
								}
								
								
								$check = $this->parse_boolean($det_temp);
								
								//check
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
							}
							
						}

						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 3,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
						}
						
						}
						
				  
			  }
			  
			  if(isset($next_status)){
				//main
				$array['awaiting_table']  = $awating_person;
				$array['status'] = $next_status;
				//job
				$jobArray['awaiting_role_id'] = $awating_person.',';
				$jobArray['status_id'] = $next_status;
				$jobArray['status_text'] = $next_status_text;
			  }

			  
			  $array['lastupdate_user_id'] = $this->data['userdata']['id'];
			  $array['modified_date'] = $now;
			  
			  
			    if($next == 1){
				  $lastjob_update = array(
				  	'is_completed' => 1,
					'display' => 0,
				  );
				  $this->Job_model->update($jobArray['parent_id'], $lastjob_update);
				  if($last == 0){
				  $new_job_id = $this->Job_model->insert($jobArray);
				  $array['latest_job_id'] = $new_job_id;
				  }
			  	}
				
			  $this->Invoice_model->update($id, $array);

			 //mail
			 if(isset($new_job_id)){
				$related_role = $this->Job_model->getRelatedRoleID($new_job_id);
				$mail_array['related_role'] = array($related_role);		
				$mail_array['title'] = $workflow['title']." ".$pre_data['order_no']." ".$next_status_text;
			 }
			 
			 //audit log
			  $log_array = array(
				'ip_address'	=> $this->input->ip_address(),
				'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
				'table_affect'	=> 'invoice',
				'description'	=> 'Update Invoice',
				'created_date'	=> date('Y-m-d H:i:s'),
			  );
				  
			  $audit_id = $this->Audit_log_model->insert($log_array);	
			  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
			  $update_array = array(
				 'log_no'	=> $custom_code,
			  );
			  $this->Audit_log_model->update($audit_id, $update_array);
			  
			    
		  }
		  
		  $item_type = 0;
		  
		  
		  if($mode == 'Add'){
				
				$item_type = 1;
			
		  }else if($mode == 'Edit'){
				if($pre_status < 1 && $pre_status != 0){
					$item_type = 1;
				}	
		  }
		  
		  if($item_type == 1){
		  //針對底下的ITEM做處理
		  
		  	 $item_counter = $this->input->post("item_counter", true);
		  
			  //print_r($_POST);exit;
			  
			  for($j=0;$j<$item_counter;$j++){
				  
					//print_r($tmp);exit;
					$data = array(
						'invoice_id' 		=> $p_id,
						'product_id' 	=> $_POST['product_id'.$j],
						'model_name' 	=> $_POST['model_name'.$j],
						'quantity' 		=> $_POST['quantity'.$j],
						'price' 		=> $_POST['price'.$j],
						'total_price' 	=> $_POST['total_price'.$j],
						'created_date' 	=> $now,
					);
						
					$this->Invoice_item_model->insert($data);
					//echo $this->db->last_query();exit;
			  }
		  
		  	/*
		    //sent mail
			if(isset($mail_array['related_role'])){
			
				$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
				$sitename = $this->Settings_model->get_settings(1);
				$default_email = $this->Settings_model->get_settings(6);
				
				$this->load->library('email');
				
				if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
							
					foreach($related_role_data as $k => $v){
					$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
					
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->send();	
					  
					}
				
				}
			
			}*/
			
		  }
		  		  		 		  		  
		  //alert
		  if($mode == 'Add'){
			$alert_type = '/1';
		  }else{
			$alert_type = '/2';
		  }
		    		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage.$alert_type,'refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].$alert_type));
		  }	  
		  
	  }
	  
	  public function del($id){
		  $new_status =0;
		  $data = $this->Invoice_model->get($id);
		  
		  //get related do
				$related_do = $this->Delivery_order_model->get_where(array('is_deleted'=>0,'order_id'=>$data['order_id'],'status'=>2));
				
			//get related invoice
				$related_invoice = $this->Invoice_model->get_where(array('is_deleted'=>0,'order_id'=>$data['order_id'],'status'=>1));
				
				//echo count($related_invoice);echo count($related_do);exit;
				//do count = invoice ,mean completed
				if(count($related_do) == (count($related_invoice)-1)){
					$new_status = 1;
				}
				
				//order had completed
				if($new_status != 1){
					$this->Order_model->update($data['order_id'],array('new_status' => 0));	
				}
		  
		  $this->Invoice_model->delete($id);
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage.'/3','refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/3'));
		  }		
	  }
	  
	  public function ajax_getSearch(){
		  
		  	$keyword = $_POST['keyword'];
			$limit = $_POST['limit'];
			$page = $_POST['page'];
			
			$start = ($page-1) * $limit;
			//我們只取已經complete
		  	$record_count = $this->Delivery_order_model->ajax_record_count($keyword, array(
				'status'	=> 2
			));
		  	$data = $this->Delivery_order_model->ajax_search($keyword, $limit, $start, array(
				'status'	=> 2
			));
		  	$paging = $this->get_paging($limit,10,$record_count,$page);
			
			$status = $this->Delivery_order_model->status_list();
			
			//顯示Status為狀態文字, 不是數字
			if(!empty($data)) {
				foreach($data as $k=>$v) {
					$data[$k]['status'] = $status[$v['status']];
				}
			}
		  	
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					'paging'	=> $paging,
			);
			
			echo json_encode($temp);	
			exit;
	  }	  
	  
	  public function ajax_getSearchData(){
		  
		  	$id = $_POST['id'];
		  	$data = $this->Delivery_order_model->get($id);
			
			$customer = $this->Customers_model->get($data['customer_id']);
			$data['attention'] = $customer['primary_attention_to'];
			$data['email'] = $customer['primary_contact_email'];
			$data['contact_no'] = $customer['primary_contact_info'];
			$data['address'] = $customer['address'];
			$data['postal'] = $customer['postal'];
			$data['country'] = $customer['country'];
			
			//price list
			$price_list = $customer['price_list'];
			
			if($price_list != ''){
				$price_list = json_decode($price_list,true);	
			}else{
				$price_list = array();
			}
			
			
			//get quotation related item
			$related_item = $this->Delivery_order_item_model->getRelatedItem($id);
			
			//get product name
			foreach($related_item as $k => $v){
				$product_data = $this->Products_model->get($v['product_id']);
				$related_item[$k]['product_name'] = $product_data['product_name'];
				
				//search price in price list 
				$price = 0;
				foreach($price_list as $k2 => $v2){
					if($v2['id'] == $v['product_id']){
						$price = $v2['price'];
					}
				}
				
				//if price = 0,get price form product
				if($price == 0){
					$related_item[$k]['price2'] = $product_data['selling_price'];
				}else{
					$related_item[$k]['price2'] = $price;
				}
			}
			
	  		$temp = array(
				'status'	=> 'OK',
				'data'		=> $data,
				'product' => $related_item ,
			);
			
			
			echo json_encode($temp);	
			exit;
	  }	 
	  
	  public function get_paging($item_per_page,$pagenum,$total_item,$page)
	  {
	
		$start = (int)(($page-1)/$pagenum)*$pagenum+1;
		$end = $start+$pagenum-1;
		$next = $page+1;
		$pre = $page-1;
		
		$total_page = ceil( $total_item / $item_per_page );
                $paging = '';
		if($total_item > $item_per_page){
                    $paging .= '<ul class="pagination">';

                    if($page > 1){
                            $paging .= '<li><a href="javascript:changePage(1)">&laquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$pre.')">&lsaquo;</li>';
                    }

                    if($total_page <= $pagenum){

                            for($i=$start;$i<=$total_page;$i++){
                                    if($i == $page){

                                            $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                    }else{

                                            $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                    }
                            }
                    }else{
                            if($page > 5){
                                    $end = $page+5;
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    $start = $end - ($pagenum - 1);

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }else{
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }	
                    }

                    if($page < $total_page){
                            $paging .= '<li><a href="javascript:changePage('.$next.')">&rsaquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$total_page.')">&raquo;</a></li>';
                    }

                    $paging .= '</ul>';
                }
		
		return $paging;
	  }
	  
	 private function PDF_generation($id, $mode='I') {
		  
		  
			$data = $this->Invoice_model->get($id);
			
			//product list
			$product_list = $this->Products_model->getIDKeyArray('product_name');
			
			//date		
		 	$date = $data['invoice_date'];
			$tmp = explode('-',$date);
			$date = $tmp[2]."/".$tmp[1]."/".$tmp[0];
			
			
			$requestor = $this->Employee_model->get($data['requestor_id']);
			$data['requestor_name'] = $requestor['full_name'];

			$product = $this->Invoice_item_model->getRelatedItem($id);
				
		 	$customer = $this->Customers_model->get($data['customer_id']);
		  
		  	$item_group = '';
		  	foreach($product as $k => $v){
			  	$item_group .='<tr><td>'.($k+1).'</td><td>'.$product_list[$v['product_id']].'</td><td>'.$v['model_name'].'</td><td>'.$v['quantity'].'</td><td>'.$v['price'].'</td><td>'.$v['total_price'].'</td></tr>';
		  	}
		  
		  
		  $this->load->library("Pdf");		
          $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		  
		  // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Anexus');
            $pdf->SetTitle("Invoice");
            $pdf->SetSubject("");
            $pdf->SetKeywords("");

            // remove default header/footer
            $pdf->setPrintHeader(false);			
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(10, 10, 10);

            // set auto page breaks
            //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
            }

            // ---------------------------------------------------------

            // set font
            $pdf->SetFont('times', '', 10);

            

            // set font
            $pdf->SetFont('times', '', 10);
			
			// add a page
       		$pdf->AddPage();
			
			
			$html = '			
			<h1>Invoice</h1>
			
			<table width="100%" cellpadding="5">
				<tr>
					<td>
						<table width="100%"  border="1" cellpadding="5">
			
							<tr>
								<th colspan="2"><h3>Invoice Information</h3></th>
							</tr>
							<tr>
								<th colspan="2"><h4>Invoice No.</h4></th>
							</tr>
							<tr>
								<td colspan="2">'.$data['invoice_no'].'</td>
							</tr>
							<tr>
								<th colspan="2"><h4>Date of Invoice</h4></th>
							</tr>
							<tr>
								<td colspan="2">'.$date.'</td>
							</tr>
							
						</table>
						
						<table width="100%"  border="1" cellpadding="5">
			
							<tr>
								<th><h3>Staff Information</h3></th>
							</tr>
							<tr>
								<th><h4>Staff Name</h4></th>
							</tr>
							<tr>
								<td>'.$data['staff_name'].'</td>
							</tr>
							<tr>
								<th><h4>Role</h4></th>
							</tr>
							<tr>
								<td>'.$this->data['role_list'][$data['staff_role']].'</td>
							</tr>
	
						</table>

					</td>
					
					<td>
						<table width="100%"  border="1" cellpadding="5">
			
							<tr>
								<th><h3>Customer Information</h3></th>
							</tr>
							<tr>
								<th><h4>Company Name</h4></th>
							</tr>
							<tr>
								<td>'.$customer['company_name'].'</td>
							</tr>
							<tr>
								<th><h4>Attention</h4></th>
							</tr>
							<tr>
								<td>'.$customer['primary_attention_to'].'</td>
							</tr>
							<tr>
								<th><h4>Email</h4></th>
							</tr>
							<tr>
								<td>'.$customer['primary_contact_email'].'</td>
							</tr>
							<tr>
								<th><h4>Contact</h4></th>
							</tr>
							<tr>
								<td>'.$customer['primary_contact_info'].'</td>
							</tr>
							<tr>
								<th><h4>Address *</h4></th>
							</tr>
							<tr>
								<td>'.$customer['address'].'</td>
							</tr>
							<tr>
								<th><h4>Postal Code</h4></th>
							</tr>
							<tr>
								<td>'.$customer['postal'].'</td>
							</tr>
							<tr>
								<th><h4>Country</h4></th>
							</tr>
							<tr>
								<td>'.$customer['country'].'</td>
							</tr>
		
						</table>
						
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
						
						<table width="100%" border="1" cellpadding="5">
				
							<tr>
								<th colspan="6"><h3>Items</h3></th>
							</tr>
							<tr>
								<td>#</td>
								<td>Product Name</td>
								<td>Model Name</td>
								<td>Quantity</td>
								<td>Unit Price</td>
								<td>Price</td>
							</tr>'.$item_group.'
						</table>
						
					</td>
				</tr>
				<tr>
					<td colspan="2">
						
						<table width="100%" border="1" cellpadding="5">
				
							<tr>
								<th><h3>Special Instructions</h3></th>
							</tr>
							<tr>
								<td>'.$data['notes'].'</td>
							</tr>
						</table>
						
					</td>
				</tr>

			</table>';
			
			$pdf->writeHTML($html, true, false, true, false, '');
									
			
			$file_name = date("YmdHis").rand(1000,9999);
			
			if($mode == 'I') {
				$pdf->Output($file_name.'.pdf', 'I'); 	
				return $file_name.'.pdf';
			} else if ($mode == 'f') {				
				$pdf->Output('./uploads/'.$file_name.'.pdf', 'f'); 
				return './uploads/'.$file_name.'.pdf';
			}
			
		  		  
		  
	  }
	  
	  public function export_pdf($id){		  
		  $this->PDF_generation($id, 'I');			
	  }
	  
	  public function sent_mail($id,$product_total=0,$service_total=0){
		  
		   	$filename = $this->PDF_generation($id, 'f');	 		  
		  
		    $tender_data = $this->Tender_model->get($id);
		  	$customer = $this->Customers_model->get($tender_data['customer_id']);
		  
		  	$send_to = $customer['email'];
			
			$sitename = $this->Settings_model->get_settings(1);
		  	$default_email = $this->Settings_model->get_settings(6);
			
			 //$requestor_role
			$requestor_employee = $this->Employee_model->get($tender_data['requestor_id']);
			$requestor_role = $requestor_employee['role_id'];
		  
		 	$role_id = $this->data['userdata']['role_id'];
			
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $tender_data['status'],
							'workflow_id' => 3,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(3);
						$status = json_decode($workflow['status_json'],true);
						
						if(isset($action)){

		
						if($action == 'send_email' || $action == 'sendmail_update'){
							
							//next status
							$next_status = $workflow_order['next_status_id'];
							$next_status_text = $status[$workflow_order['next_status_id']];
							$who_email = $workflow_order['who_email'];
	
						}

						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];

								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 1,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
							
							$jobArray = array(
								'parent_id' 		=>$tender_data['latest_job_id'],
								'user_id' 			=>$this->data['userdata']['id'],
								'customer_id' 		=>$tender_data['customer_id'],
								'type' 				=> 'Tender',
								'type_path' 		=>'sales/tender_edit/',
								'awaiting_role_id' 	=>$awating_person.',',
								'status_id' 		=>$next_status,
								'status_text'		=>$next_status_text,
								'serial' 			=>$tender_data['qr_serial'],
								'type_id' 			=>$tender_data['id'],
								'created_date'		=>date("Y-m-d H:i:s"),
								'modified_date'		=>date("Y-m-d H:i:s"),
							);			
					  
						    $array = array(
							'awaiting_table' => $awating_person.',',
							'status' => $next_status,
							'modified_date' => date('Y-m-d H:i:s'),
							'lastupdate_user_id' => $this->data['userdata']['id'],
						    );
							
							//print_r($jobArray);exit;

						
						  //更新上一個JOB的狀態					  
						  $this->Job_model->update($jobArray['parent_id'], array(
							'is_completed' => 1,
							'display' => 0,
						  ));
						  
						  //新增一個新的JOB
						  $new_job_id = $this->Job_model->insert($jobArray);
						  $array['latest_job_id'] = $new_job_id;
						  
						  $this->Tender_model->update($tender_data['id'], $array);
							
							
						}
						
						}
			
			
			if(isset($who_email)){
			
				$this->load->library('email');
				
				switch($who_email){
					case 'customer':
							
							//Send Notification
							if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
										
								$this->email->from($default_email['value'], $sitename['value']);
								$this->email->to($send_to); 
							
								$this->email->subject('Anexus '.$workflow['title'].': '.$tender_data['qr_serial']);
								$this->email->message($tender_data['qr_serial']);	
								$this->email->attach($filename);
							

								$this->email->send();
							
							}
							
					break;	
					case 'supplier':
						
							  //count supplier
							  $supplier_count = $this->Purchase_request_item_model->get_supplier_groupBy($id);
							  
							  foreach($supplier_count as $k => $v){
									
								  $supplier = $this->Suppliers_model->get($v['supplier_id']);
								  
								  $file_name = $this->PDF_generation($id, 'f',$v['supplier_id']);	
										 
								  if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
									
									$this->load->library('email');
									$this->email->clear();	
									$this->email->from($default_email['value'], $sitename['value']);
									$this->email->to($supplier ['email']); 
									
									$this->email->subject('Anexus Purchase request '.$tender_data['qr_serial']);
									$this->email->message($tender_data['qr_serial']);	
									$this->email->attach($file_name);
									
									$this->email->send();
									
								  }
							  
							  }
							
					break;
					case 'employee':
					break;
				
				}
				
			}
			
			 //mail
			 $mail_array = array(
				'creater_name' =>$this->data['userdata']['name'],
				'title' =>'Purchase '.$tender_data['qr_serial'].' has been send out',
			 );
			 
			 if(isset($new_job_id)){
			  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
			  $mail_array['related_role'] = $related_role;		
			  $mail_array['title'] = $workflow['title']." ".$tender_data['qr_serial']." ".$next_status_text;
			 }
			 echo $_SERVER['HTTP_HOST'];exit;
			 //sent Notification to related role						  			
		  	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local" && isset($mail_array['related_role'])) {
				$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
			  				  	
			  	foreach($related_role_data as $k => $v){
				  
				  	$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
					
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->send();	
				  
		 		}
			}
			
			
			$lastpage = $this->session->userdata("lastpage");
			if(!empty($lastpage)) {
			  redirect($lastpage,'refresh');  
			} else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
			}		 			
		  
	  }

}

?>