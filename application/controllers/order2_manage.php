<?php

class Order2_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();  
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Customers_model');
			$this->load->model('Tender_model');
			$this->load->model('Tender_item_model');
			$this->load->model('Employee_model');
			$this->load->model('Department_model');
			$this->load->model('Products_model');
			$this->load->model('Suppliers_model');
			$this->load->model('Shipping_model');
			$this->load->model('Products_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Settings_model');
			$this->load->model('Quotation_model');
			$this->load->model('Quotation_item_model');
			$this->load->model('Workflow_model');
			$this->load->model('Workflow_order_model');
			$this->load->model('Order_model');
			$this->load->model('Product_log_model');
			$this->load->model('Order_item_model');
			$this->load->model('Audit_log_model');
			$this->load->model('Packing_list_model');
			$this->load->model('Order_item_model');
			$this->load->model('Packing_list_item_model');
			$this->load->model('Delivery_order_model');
			$this->load->model('Invoice_model');
			
            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
			
			$this->data['role_list'] = $this->Role_model->getIDKeyArray('name');
			$status_list = $this->Workflow_model->get(1);
			$this->data['status_list'] = json_decode($status_list['status_json']);
			
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$userdata = $this->session->userdata("userdata");
				
				
				if(strpos($userdata['role_id'],',')){
					$userdata['role_id'] = explode(',',$userdata['role_id']);	
				}else{
					$userdata['role_id'] = array($userdata['role_id']);	
				}
				
				
				$this->data['userdata'] = $userdata;
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}    
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			$this->data['group_name'] = "sales";  
			$this->data['model_name'] = "order";  
			$this->data['common_name'] = "Order";   
			
			if(in_array(3,$this->data['userdata']['role_id'])){
				
				
				$a_csutomer = $this->data['staff_info']['assign_customer'];
				
				if($a_csutomer != ''){
				
					if(strpos($a_csutomer,',')){
						$a_customer_array = explode(',',$a_csutomer);	
					}else{
						$a_customer_array = array($a_csutomer);	
					}
				
				}else{
					$a_customer_array = array();	
				}
				
				$this->data['task_display_count'] = $this->Job_model->record_count2(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
				),$this->data['userdata']['role_id'],$a_customer_array);
				
				
				

			}else{
			
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
		    }
			
			/*
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
					
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}  
            
			//related role
		    define('SUPERADMIN', 1);
		    define('SALES_MANAGER', 7);
		    define('SALES_EXECUTIVE', 8);
			*/
		   
      }
   
      public function index($q="ALL", $page=1, $alert=0) {  
          		
			$this->data['alert'] = $alert;
          	$this->data['page'] = $page;
			$this->data['title'] = ucfirst($this->data['model_name']);
			
			//assign customer
			if(in_array(3,$this->data['userdata']['role_id']) && !in_array(1,$this->data['userdata']['role_id'])){
				
				if($this->data['staff_info']['assign_customer'] != ''){
					
					if(strpos($this->data['staff_info']['assign_customer'],',')){
						//echo 1;exit;
						$assign_customer = explode(',',$this->data['staff_info']['assign_customer']);
					}else{
						$assign_customer = array($this->data['staff_info']['assign_customer']);
					}
					
				}else{

					$assign_customer = array(0);
					//print_r($assign_customer);exit;
				}
			}else{
				$assign_customer = '';
			}
			
			//print_r($assign_customer);exit;
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			if(in_array(3,$this->data['userdata']['role_id']) && !in_array(1,$this->data['userdata']['role_id'])){
				$filter['create_user_id'] = $this->data['userdata']['id'];
			}
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/';
			if($q == 'ALL') {
				$q = "";
			}else{
				$q = urldecode($q);
			}
			$this->data['q'] = urldecode($q);
					
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Order_model->record_count($filter, $q,$assign_customer);
			
			//get particular ranged list
			
			if(in_array(3,$this->data['userdata']['role_id']) && !in_array(1,$this->data['userdata']['role_id'])){
				$this->data['results'] = $this->Order_model->fetch($q, $this->data['item_per_page'], $limit_start,$assign_customer,$this->data['userdata']['id']);
			}else{
				$this->data['results'] = $this->Order_model->fetch($q, $this->data['item_per_page'], $limit_start,$assign_customer);
			}
			
			//echo $this->data['userdata']['role_id'];
			//print_r(!empty($assign_customer));exit;
			//echo $this->db->last_query();exit;
			
			
			//抓目前status
			if(!empty($this->data['results'])){
			foreach($this->data['results'] as $k => $v){
				
				//status is completed
				if($v['status'] == 2){
					
					$related_packing = $this->Packing_list_model->get_where(array('is_deleted'=>0,'status'=>1,'order_id'=>$v['id']));
					
					if(!empty($related_packing)){

						//order 的數量
						$order_item = $this->Order_item_model->getRelatedItem($v['id']);
						$order_qty = 0;
						if(!empty($order_item)){
							foreach($order_item as $k2=>$v2){
								$order_qty += $v2['quantity'];
							}
						}
						
						//packing 數量
						$packing_qty = 0;
						foreach($related_packing as $k2 => $v2){
							
							$packing_item = $this->Packing_list_item_model->getRelatedItem($v2['id']);
							
							foreach($packing_item as $k3 => $v3){
								$packing_qty += $v3['quantity_pack'];
							}
							
						}
						
						//packing list 的數量
						$pack_count = count($related_packing);
						
						//判斷item pack完了沒
						if($order_qty == $packing_qty){
							
							//目前有的delivery order
							$related_do = count($this->Delivery_order_model->get_where(array('is_deleted'=>0,'status'=>2,'order_id'=>$v['id'])));
							
							if($related_do == 0){
								$this->data['results'][$k]['status_text'] = 'Pending Delivery Order';
							//代表do已處理完畢
							}else if($related_do == $pack_count){
								$this->data['results'][$k]['status_text'] = 'Completed';
								/*
								//do 數量
								$do_count = $related_do;
								
								//invoice 數量
								$related_invoice = count($this->Invoice_model->get_where(array('is_deleted'=>0,'status'=>1,'order_id'=>$v['id'])));
								
								if($related_invoice == 0){
									$this->data['results'][$k]['status_text'] = 'Pending Invoice';
								}else if($do_count == $related_invoice){
									$this->data['results'][$k]['status_text'] = 'Completed';
								}else{
									$this->data['results'][$k]['status_text'] = 'Pending Generate Invoice ['.$related_invoice.' of '.$do_count.']';
								}
								*/
								
							}else{
								$this->data['results'][$k]['status_text'] = 'Pending Generate Delivery Order ['.($related_do+1).' of '.$pack_count.']';
							}
							
						}else{
							$this->data['results'][$k]['status_text'] = 'Pending Generate Packing List ['.($pack_count+1).' of '.($pack_count+1).']';
						}
						
					}else{
						$this->data['results'][$k]['status_text'] = 'Pending Packing List';
					}
					
				}//endif status
				
			}//end foreach
			
			}
			
			
			if(!empty($this->data['results'])) {		
				foreach($this->data['results'] as $k => $v){
					if(!empty($v['awaiting_table'])){
							$this->data['results'][$k]['awaiting_table']= explode(',',$v['awaiting_table']);
					}
				}		
			}
				
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->data['customer_list'] = $this->Customers_model->getIDKeyArray("company_name");
			$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
			
			$this->session->set_userdata("lastpage", $url.$page);		
						
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  
	  public function add($id=false) {  
          			
			$role_id = $this->data['userdata']['role_id'];
			$this->data['gst'] = $this->Settings_model->get_settings(5);
					
			if($id !== false) {
				
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Order_model->get($id);
					
				$order_date = $this->data['result']['order_date'];
				$tmp = explode('-',$order_date);
				$this->data['result']['order_date'] = $tmp[2]."/".$tmp[1]."/".$tmp[0];
				
				$product = $this->Order_item_model->getRelatedItem($id);
				$this->data['result']['product'] = json_encode($product);
				
				$requestor_employee = $this->Employee_model->get($this->data['result']['requestor_id']);
			    $requestor_role = $requestor_employee['role_id'];
				$this->data['requestor_role'] = $requestor_role;
				if(!empty($this->data['result']['awaiting_table'])){
				$this->data['result']['awaiting_table'] = explode(',',$this->data['result']['awaiting_table']);
				}
				
				$default = 0;
				
				//get related workflow order data
				$workflow_flitter = array(
					'status_id' => $this->data['result']['status'],
					'workflow_id' => 1,
				);
						
				$type='';
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						
				//print_r($requestor_role);exit;
				//$workflow_order['role_id'] == 'requestor'
				if($workflow_order['role_id'] == 'requestor'){
							
				//requestor edit
				if(in_array($requestor_role,$role_id) && $workflow_order['requestor_edit']) {
				$type = 'edit';	
								
				//user role = current status role
				}else if(in_array($requestor_role,$role_id)){
					$type = $workflow_order['action'];
				}
							
				//$workflow_order['role_id'] != 'requestor'						
				}else{
							
					//user role = current status role
					if (in_array($workflow_order['role_id'],$role_id)){
						$type = $workflow_order['action'];	
							
					//requestor edit															
					}else if(in_array($requestor_role,$role_id) && $workflow_order['requestor_edit']) {
						$type = 'edit';	
					}
						
				}
						
				//data form database
				$workflow = $this->Workflow_model->get(1);
				$status = json_decode($workflow['status_json'],true);
						
				$this->data['workflow_title'] = $workflow['title'];
					
				$this->data['btn_type'] = $type;
				//print_r($workflow_order);exit;

					switch($type){
						case 'edit':
							$this->data['head_title'] = 'Edit '.$workflow['title'].' Request';
						break;
						case 'approval':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
						break;
						case 'approval_update':
						case 'approval_update_s':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'update':
							$this->data['head_title'] = 'Update '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'send_email':
							$this->data['head_title'] = 'Waiting to Send '.$workflow['title'].' Request';
						break;
						case 'sendmail_update':
							$this->data['head_title'] = 'Waiting to Send/Edit '.$workflow['title'].' Request';
						break;
						case 'upload':
						case 'upload_update':
							$this->data['head_title'] = 'Upload Confirm File '.$workflow['title'].' Request';
						break;
						case 'complete':
							$this->data['head_title'] = $workflow['title'].' Request';
							$this->data['last_action'] = $workflow_order['action_before_complete'];
						break;
						default:
							$this->data['head_title'] = $workflow['title'].' Request';
					}
					
				foreach(json_decode($workflow['status_json'],true) as $k => $v){
						
					if($v == 'Completed'){
						$this->data['completed_status_id'] = $k;
						break;
					}
				}

				
			} else {
				
				$this->data['mode'] = 'Add';
				$this->data['head_title'] = 'New Order';	

				//get related workflow order data
					$workflow_flitter = array(
						//'role_id' => $role_id,
						'status_id' => -1,
						'workflow_id' => 1,
					);
					
					//data form database
					$workflow = $this->Workflow_model->get(1);
					$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter,$role_id);
					
					//get view bottom layout
					$type = '';
					if(!empty($workflow_order)){
						$type = $workflow_order['action'];
						if($workflow_order['action'] == 'create'){
							$this->data['head_title'] = 'Create '.$workflow['title'].' Request';
						}else{
							$this->data['head_title'] = $workflow['title'].' Request';
						}
					}else{
						$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					$this->data['btn_type'] = $type;
					
			}		
			
			//all product 
			$products = $this->Products_model->get();
			//sum product by id
			foreach($products as $k => $v){
				$products[$k]['qty'] = $this->Product_log_model->sum_by_id($v['id']);
				
				//promo限制數量
				$p_start_date = $v['p_start_date'];
				$p_end_date = $v['p_end_date'];
				$p_qty = $v['p_qty'];
						
				$order_item_qty = $this->Order_item_model->count_promo_qty($v['id'],$p_start_date,$p_end_date);
				$promo_val = 0;
				if($order_item_qty >= $p_qty || strtotime($p_end_date)<strtotime(date('Y-m-d'))|| $p_qty==0 ){
					$promo_val = 1;
				}
				
				$products[$k]['promo_val'] = $promo_val;
				
			}
			
			//print_r($products);exit;
			
			$this->data['product'] = json_encode($products);
			$product_name = $this->Products_model->get_groupBy();
			$this->data['product_name'] = json_encode($product_name);
			
			//get assign customer
			$company_list = $this->Customers_model->getAll();
			$assign_customer = $this->data['staff_info']['assign_customer'];
			
			if(strpos($assign_customer,',')){
				$assign_customer = explode(",",$assign_customer);
			}else{
				$assign_customer = array($assign_customer);
			}
			
			//只有是sales 的時候才給予指定customer
			if(in_array(3,$this->data['userdata']['role_id']) && !in_array(1,$this->data['userdata']['role_id'])){
			
			$new_company_list = array();
			foreach($company_list as $k => $v){
				
				foreach($assign_customer as $k2 => $v2){
					if($v2 == $v['id']){
						$new_company_list[] = $v;
					}
				}
				
			}
			
			}else{
				$new_company_list = $company_list;
			}
			
			$this->data['company_list'] = json_encode($new_company_list);
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);	
			
      }
	  
	  public function submit(){
		  
		  
		  
		  $now = date("Y-m-d H:i:s");
		  $mode = $this->input->post("mode", true);
		  $id = $this->input->post("id", true);
		  
		  $role_id = $this->data['userdata']['role_id'];

		  //form data
		  $requestor_id = $this->input->post("requestor_id", true);
		  $order_date = $this->input->post("order_date", true);
		  $company = $this->input->post("company", true);
		  $remarks = $this->input->post("remarks", true);
		  $total_amount = $this->input->post("total_amount", true);
		  $gst_amount = $this->input->post("gst_amount", true);
		  
		  //delivery address
		  $alias = $this->input->post("delivery_address", true);
		  $address2 = $this->input->post("address2", true);
		  $postal2 = $this->input->post("postal2", true);
		  $country2 = $this->input->post("country2", true);

		  //get value approve or not approve
		  $confirm = $this->input->post("confirm", true);
		  $correct_check = $this->input->post("check_correct", true);
		  
		  $next = 0;
		  $last = 0;
		  $status_change = 0;
		  
		  if(!empty($order_date)) {
			  $tmp = explode("/", $order_date);
			  $order_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
		  }
		  
		  if($mode == 'Add'){
			  
			  if($company != '-') {
			  $tmp = explode("|", $company);
			  $customer_id = $tmp[0];
			  $customer_name = $tmp[1];
		  	  }
			  
		  }else{
		  
			  //order
			  $data = $this->Order_model->get($id);
		  	  $customer_id = $data['customer_id'];
			  $customer_name = $data['customer_name'];
			  $requestor_id = $data['requestor_id'];
			  
		  }	
		  
 		  //job
		  $jobArray = array(
			'parent_id' =>0,
			'user_id' =>$this->data['userdata']['id'],
			'customer_id' =>isset($customer_id)?$customer_id:'',
			'customer_name' =>isset($customer_name)?$customer_name:'',
			'type' => 'Order',
			'type_path' =>'sales/order_edit/',
	 	  );
 
 		  //main
		  $array = array(
		  	'requestor_id'		=> $requestor_id,
		  	'order_date'		=> $order_date,
			'total_price'		=> $total_amount,
			'gst_amount'		=> $gst_amount,
			'customer_id'		=> isset($customer_id)?$customer_id:'',
			'customer_name'		=> isset($customer_name)?$customer_name:'',
			'remark'			=> $remarks,
			'alias'				=> $alias,
			'address'			=> $address2,
			'postal'			=> $postal2,
			'country'			=> $country2,
		  );
		  
		  //print_r($array);exit;
		  
		  //mail
		  $mail_array = array(
		  'creater_name' =>$this->data['userdata']['name'],
		  );
		  
		  
		  $tender_id = "";
		  //Add
		  if($mode == 'Add'){
				
				//get related workflow order data
				$workflow_flitter = array(
					//'role_id' => $role_id,
					'status_id' => -1,
					'workflow_id' => 1,
				);
				
				//data form database
				$workflow = $this->Workflow_model->get(1);
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter,$role_id);
				$status = json_decode($workflow['status_json'],true);
				
				if(!empty($workflow_order)){
				
				//ad_hoc
				if($workflow_order['ad_hoc'] == 1){
					$array['ad_hoc'] = 1;
				}
				
				//purchaser
				$is_adhoc = false;
				if(in_array(10,$role_id)){
					$is_adhoc = true;
				}
				
				//next status
				$next_status = $workflow_order['next_status_id'];
				$next_status_text = $status[$workflow_order['next_status_id']];
				
			  	//if need_det = 1
				if($workflow_order['need_det'] == 1){
					
					$formula = json_decode($workflow_order['formula'],true);
					
					//check if product or service price over value
					foreach($formula as $k => $v){
					
						$price = $this->input->post($v['target_colum'], true);
						$det_temp = $price." ".$v['logic']." ".$v['value'];
						$check = $this->parse_boolean($det_temp);
						
						//check price over value
						if($check){
							$next_status = $v['next_status_id'];
							$next_status_text = $status[$v['next_status_id']];
							break;
						}
						
					}
					
				}
				
				//get next status workflow order data
				$next_workflow_flitter = array(
					'status_id' => $next_status,
					'workflow_id' => 1,
				);
				
				//next status data
				$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
				$awating_person = $next_status_data['role_id'];
				
				if($awating_person == 'requestor'){
					$awating_person = $this->input->post("requestor_id", true);
				}
				
				//main
				$array['awaiting_table']  = $awating_person;
				$array['status'] = $next_status;
				//main
				$jobArray['awaiting_role_id'] = $awating_person.',';
				$jobArray['status_id'] = $next_status;
				$jobArray['status_text'] = $next_status_text; 	
				  
				//insert main data
				$array['created_date'] = $now;
				$array['modified_date'] = $now;
				$array['create_user_id'] = $this->data['userdata']['id'];
				$array['lastupdate_user_id'] = $this->data['userdata']['id'];

				//新增資料
				$data_id = $this->Order_model->insert($array);				
				$p_id = $data_id;
					
				//更新serial
				$data_serial = $this->Order_model->zerofill($data_id, 5, $is_adhoc);
					
				//insert job
				$jobArray['serial'] = $data_serial;
				$jobArray['order_no'] = $data_serial;
				$jobArray['type_id'] = $data_id;
					
				$jobArray['created_date'] = $now;
				$jobArray['modified_date'] = $now;
				
				/*	
				//if next action == complete,create job auto complete
				if($next_status_data['action'] == 'complete'){
					$jobArray['is_completed'] = 1;
				}
				*/
					
				if($workflow_order['next_status_text'] == 'Completed'){
					
					   //job
					  $newJob = array(
						'parent_id' 		=>0,
						'user_id' 			=>$this->data['userdata']['id'],
						'customer_id'		=> $customer_id,
						'customer_name'		=> $customer_name, 
						'type'		 		=> 'Packing List',
						'status_id' 		=> -1,
						'status_text' 		=>'Pending Create Packing List',
						'created_date'		=>date('Y-m-d H:i:s'),
						'modified_date'		=>date('Y-m-d H:i:s'),
						'awaiting_role_id'	=>'4,',
						'serial'			=>$data_serial,
						'order_no'			=>$data_serial,
					  );
					  
					  //insert job
					  $new_job_id = $this->Job_model->insert($newJob);
					  
					  //job
					  $newJob = array(
						'type_path' 	=>'ground_staff/packing_list_add/'.$data_id.'/'.$new_job_id,
					  );
					  
					  //update job
					  $this->Job_model->update($new_job_id,$newJob);
					  
					  $job_id = $new_job_id;
					  
					
				}else{
					$job_id = $this->Job_model->insert($jobArray);
				}
					
				//after insert job,update job id,qr serial....
				$array =array(
					'job_id'	=> $job_id,
					'order_no'  => $data_serial,
					'latest_job_id'	=> $job_id,
				);
				$this->Order_model->update($data_id, $array);
					
				//mail
				$related_role = $this->Job_model->getRelatedRoleID($job_id);
				$mail_array['related_role'] = $related_role;		
				$mail_array['title'] = $workflow['title']." ".$data_serial." ".$next_status_text; 
				  	
				}
				
			  //audit log
			  $log_array = array(
				'ip_address'	=> $this->input->ip_address(),
				'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
				'table_affect'	=> 'order',
				'description'	=> 'Create order',
				'created_date'	=> date('Y-m-d H:i:s'),
			  );
				  
			  $audit_id = $this->Audit_log_model->insert($log_array);	
			  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
			  $update_array = array(
				 'log_no'	=> $custom_code,
			  );
			  $this->Audit_log_model->update($audit_id, $update_array);
				
		  //Edit	  			  
		  } else {
			  	
			  	$pre_data = $this->Order_model->get($id);
			 	$requestor_employee = $this->Employee_model->get($pre_data['requestor_id']);
			  	$requestor_role = $requestor_employee['role_id'];
			  	$pre_status = $pre_data['status'];
			  
			  	$jobArray['serial'] =$pre_data['order_no'];
				$jobArray['order_no'] = $pre_data['order_no'];
			  	$jobArray['type_id'] =$pre_data['id'];
			  	$jobArray['parent_id'] = $pre_data['latest_job_id'];
				
				//item parent id
				$p_id = $id;
				
			    //can edit status
			    $edit_status = $this->Workflow_order_model->get_status_edit(1);
				
			  
		//requestor edit
		if(in_array($pre_status, $edit_status) && in_array($requestor_role,$role_id)){

				//get related workflow order data
				$workflow_flitter = array(
					//'role_id' => $role_id,
					'status_id' => -1,
					'workflow_id' => 1,
				);
						
				//data form database
				$workflow = $this->Workflow_model->get(1);
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter,$role_id);
				$status = json_decode($workflow['status_json'],true);
				
					
				if(!empty($workflow_order)){
					print_r($workflow_order);exit;
					//next status
					$next_status = $workflow_order['next_status_id'];
					$next_status_text = $status[$workflow_order['next_status_id']];
						
					//if need_det = 1
					if($workflow_order['need_det'] == 1){
							
						$formula = json_decode($workflow_order['formula'],true);
							
						//check if product or service price over value
						foreach($formula as $k => $v){
							
							$price = $this->input->post($v['target_colum'], true);
							$det_temp = $price." ".$v['logic']." ".$v['value'];
							$check = $this->parse_boolean($det_temp);
								
							//check price over value
							if($check){
								$next_status = $v['next_status_id'];
								$next_status_text = $status[$v['next_status_id']];
								break;
							}
								
						}
							
					}
						
					//get next status workflow order data
					$next_workflow_flitter = array(
						'status_id' => $next_status,
						'workflow_id' => 1,
					);
						
					//next status data
					$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
					$awating_person = $next_status_data['role_id'];
						
					//if awating person equal requestor ,asign requestor for it
					if($awating_person == 'requestor'){
						$awating_person = $requestor_role;
					}
						
					//main
					$array['awaiting_table']  = $awating_person;
					$array['status'] = $next_status;
						
					//job
					$jobArray['awaiting_role_id'] = $awating_person.',';
					$jobArray['status_id'] = $next_status;
					$jobArray['status_text'] = $next_status_text;
						
						
				}
						
				  
		}else{
				  			
												
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $pre_status,
							'workflow_id' => 1,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if(in_array($requestor_role,$role_id) && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if(in_array($requestor_role,$role_id)){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if (in_array($workflow_order['role_id'],$role_id)){
								$action = $workflow_order['action'];	
							
							//requestor edit															
							}else if(in_array($requestor_role,$role_id) && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(1);
						$status = json_decode($workflow['status_json'],true);
						
						
						if(isset($action)){
						
						//action
						switch ($action) {
							case 'approval':
							
								//clear data infront
								$array = array();
							
								//approved
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];

										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'update':
								
								//next status
								$next_status = $workflow_order['next_status_id'];
								$next_status_text = $status[$workflow_order['next_status_id']];
								
								$array = array(
								);
								
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
								$next = 1;
								
								break;
							case 'approval_update':
								
								//clear data infront
								$array = array();
								
								if($confirm == 1){
									
									if($correct_check == 'on'){
										
										//next status
										$next_status = $workflow_order['next_status_id'];
										$next_status_text = $status[$workflow_order['next_status_id']];
										
										$target_colum = json_decode($workflow_order['target_colum'],true);
										
										//check update colum single or multiple
										$array = array();
										foreach($target_colum as $k => $v){
										
											if($v['type'] == 'single'){
												
												if($v['data_type'] == 'string'){
													
												$array[$v['name']] = $this->input->post($v['name'],true);
												
												}else if($v['data_type'] == 'date'){
													
													$date = $this->input->post($v['name'], true);
													if(!empty($date)) {
													  $tmp = explode("/", $date);
													  $date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
													  $array[$v['name']] = $date;
													}
													
												}
											
											}else{
												
											}
											
										}
										$next = 1;
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
									}
									
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
							case 'approval_update_s':
								
								//clear data infront
								$array = array();
								
								$array['remark'] = $remarks;
								
								if($confirm == 1){
									
									if($correct_check == 'on'){
										//echo $correct_check;exit;
										//next status
										$next_status = $workflow_order['next_status_id'];
										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
									}
									
								}else if($confirm == 2){
									
									
										
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
							case 'upload':
								
								//clear data infront
								$array = array();
								
								if($confirm == 1){
								
								if(isset($_FILES['confirm_file'])){
				
									if ( $_FILES['confirm_file']['error'] == 0 && $_FILES['confirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['confirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['confirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'confirm_file'	=> $save_path,
											);	
											
											//next status
											$next_status = $workflow_order['next_status_id'];
											$next_status_text = $status[$workflow_order['next_status_id']];	
											$next = 1;
											//check last job
											if($workflow_order['next_status_text'] == 'Completed'){
												$last = 1;
											}		
											//echo $workflow_order['next_status_text'];exit;										
									}	
									
								}
								
								//no approve
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
							
							case 'upload_update':
								
								if($confirm == 1){
								
								if(isset($_FILES['confirm_file'])){
				
									if ( $_FILES['confirm_file']['error'] == 0 && $_FILES['confirm_file']['name'] != ''  ){										
											$pathinfo = pathinfo($_FILES['confirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['confirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array['confirm_file']= $save_path;
											
											//next status
											$next_status = $workflow_order['next_status_id'];
											$next_status_text = $status[$workflow_order['next_status_id']];	
											$next = 1;
											//check last job
											if($workflow_order['next_status_text'] == 'Completed'){
												$last = 1;
											}		
											//echo $workflow_order['next_status_text'];exit;										
									}	
									
								}
								
								//no approve
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
									
							case 'complete':
							
								//clear data infront
								$array = array();
							
								if(isset($_FILES['confirm_file'])){
				
									if ( $_FILES['confirm_file']['error'] == 0 && $_FILES['confirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['confirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['confirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'confirm_file'	=> $save_path,
											);	
										
									}	
								}
								
								break;	
								
						}
						
						//print_r($workflow_order);exit;
						//if need_det = 1
						
						if($workflow_order['need_det'] == 1){

							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$value = $this->input->post($v['target_colum'], true);
								//string or number
								if(is_numeric($v['value'])){
									$det_temp = $value." ".$v['logic']." ".$v['value'];
								}else{
									
									if($v['value'] == 'original'){
										$string = $tender_data['term_condition'];
									}else{
										$string = $v['value'];
									}
									
									$det_temp = "'".md5(trim($value))."' ".$v['logic']." '".md5(trim($string))."'";
								}
								
								
								$check = $this->parse_boolean($det_temp);
								
								//check
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
							}
							
						}

						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 1,
							);
							
							//next status data
							$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
						}
						
						}
						
					  //audit log
					  $log_array = array(
						'ip_address'	=> $this->input->ip_address(),
						'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
						'table_affect'	=> 'order',
						'description'	=> 'Update order',
						'created_date'	=> date('Y-m-d H:i:s'),
					  );
						  
					  $audit_id = $this->Audit_log_model->insert($log_array);	
					  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
					  $update_array = array(
						 'log_no'	=> $custom_code,
					  );
					  $this->Audit_log_model->update($audit_id, $update_array);
						
				  
			  }
			  
			  if(isset($next_status)){
				//main
				$array['awaiting_table']  = $awating_person;
				$array['status'] = $next_status;
				//job
				$jobArray['awaiting_role_id'] = $awating_person.',';
				$jobArray['status_id'] = $next_status;
				$jobArray['status_text'] = $next_status_text;
			  }

			  
			  $array['lastupdate_user_id'] = $this->data['userdata']['id'];
			  $array['modified_date'] = $now;
			  
			  
			    if($next == 1){
				  $lastjob_update = array(
				  	'is_completed' => 1,
					'display' => 0,
				  );
				  $this->Job_model->update($jobArray['parent_id'], $lastjob_update);
				  if($last == 0){
				  $new_job_id = $this->Job_model->insert($jobArray);
				  $array['latest_job_id'] = $new_job_id;
				  }else{
					  
					if($workflow_order['next_status_text'] == 'Completed'){
					
					   //job
					  $newJob = array(
						'parent_id' 		=>$pre_data['latest_job_id'],
						'user_id' 			=>$this->data['userdata']['id'],
						'customer_id'		=> $customer_id,
						'customer_name'		=> $customer_name,
						'type'		 		=> 'Packing List',
						'status_id' 		=> -1,
						'status_text' 		=>'Pending Create Packing List',
						'created_date'		=>date('Y-m-d H:i:s'),
						'modified_date'		=>date('Y-m-d H:i:s'),
						'awaiting_role_id'	=>'4,',
						'serial'			=>$pre_data['order_no'],
						'order_no'			=>$pre_data['order_no'],
					  );
					  
					  //insert job
					  $new_job_id = $this->Job_model->insert($newJob);
					  
					  //job
					  $newJob = array(
						'type_path' 	=>'ground_staff/packing_list_add/'.$pre_data['id'].'/'.$new_job_id,
					  );
					  
					  //update job
					  $this->Job_model->update($new_job_id,$newJob);
					  
					  $job_id = $new_job_id;
					  
					  //update quotation data
					  $order_array = array(
						'latest_job_id' => $new_job_id,
					  );  
					  $this->Order_model->update($pre_data['id'], $order_array);
					  
					
					}
					
					  
				  }
				  
				  
			  	}
				
			  $this->Order_model->update($id, $array);
			  //echo $this->db->last_query();exit;
			  
			  //no approve 加回庫存
			  if(isset($array['status']) && $array['status'] == 0){
				  
				  //刪除order增加商品庫存
				  $item = $this->Order_item_model->getRelatedItem($id);
									
				  foreach($item as $k => $v){
					  
					  $log_data = array(
						'product_id' => $v['product_id'],
						'qty' => $v['quantity'],
						'reason' => 'no approve order',
						'created_date' => date('Y-m-d H:i:s'),
					  );
										
					  $this->Product_log_model->insert($log_data);
					  //echo $this->db->last_query();exit;
				  
				  }
			  }
			  
			  
			 
			 //mail
			 if(isset($new_job_id)){
				$related_role = $this->Job_model->getRelatedRoleID($new_job_id);
				$mail_array['related_role'] = array($related_role);		
				$mail_array['title'] = $workflow['title']." ".$pre_data['order_no']." ".$next_status_text;
			 }
			  
			    
		  }
		  
		  $item_type = 0;
		  if($mode == 'Add'){
				
				$item_type = 1;
			
		  }else if($mode == 'Edit'){
				if($pre_status < 2 && $pre_status != 0){
					$item_type = 1;
				}	
		  }
		  
		  //print_r($_POST);exit;
		  
		  if($item_type == 1 && isset($_POST['product'])){
		  //針對底下的ITEM做處理
		  
		  foreach($_POST['product'] as $k => $v){
			  
			  if($v != '-'){
					
					//print_r($tmp);exit;
					$data = array(
						'order_id' => $p_id,
						'product_id' => $v,
						'model_name' => isset($_POST['model_name'][$k])?$_POST['model_name'][$k]:'',
						'quantity' => isset($_POST['qty'][$k])?$_POST['qty'][$k]:'',
						'quantity_order' => isset($_POST['qty'][$k])?$_POST['qty'][$k]:'',
						'promo' => isset($_POST['promo'][$k])?$_POST['promo'][$k]:'',
						'price' => isset($_POST['price'][$k])?$_POST['price'][$k]:'',
						'created_date' => $now,
					);
					
					//customer price list
					$customer = $this->Customers_model->get($customer_id);
					$price_list = json_decode($customer['price_list'],true);
					
					/*
					$product_tmp = $this->Products_model->get($v);
					$data['price'] = $product_tmp['selling_price'];
					
					foreach($price_list as $k2 => $v2){
						
						if($v == $v2['id']){
								
							$data['price'] = $v2['price'];
							
						}
						//set promo price
						if($product_tmp['promo_price'] != '0.00'){
							if($data['price']>$product_tmp['promo_price']){
								$data['price'] = $product_tmp['promo_price'];
							}
						}
					}
					*/
					
					
					//有deleted 又有old_id 代表要刪除資料
					if(isset($_POST['is_del'][$k]) && !empty($_POST['is_del'][$k]) && isset($_POST['old_id'][$k]) && !empty($_POST['old_id'][$k])){
						
						

						
						$this->Order_item_model->delete($_POST['old_id'][$k]);
						
					//沒有deleted 但有old_id 代表要更新這個資料
					} else if ( isset($_POST['is_del'][$k]) && empty($_POST['is_del'][$k]) && isset($_POST['old_id'][$k]) && !empty($_POST['old_id'][$k])) {
						
						//先刪除商品加回庫存
						$orderItem = $this->Order_item_model->get($_POST['old_id'][$k]);
						
						//order item 的 product 跟 post product 不一樣代表物品更改
						if($orderItem['product_id'] == $_POST['product'][$k]){
						
							$this->Order_item_model->update($_POST['old_id'][$k], $data);
						
						}else{
							
							$this->Order_item_model->delete($_POST['old_id'][$k]);
							
							
							//至少要有product_id才能insert
							if(!empty($data['product_id']) && !empty($data['model_name'])) {
								
								$this->Order_item_model->insert($data);
							
							}
							
							
						}

					//沒有deleted 也沒有old_id 代表要新增這個資料	
					} else if ( isset($_POST['is_del'][$k]) && empty($_POST['is_del'][$k]) && isset($_POST['old_id'][$k]) && empty($_POST['old_id'][$k])){
						
						//至少要有product_id才能insert
						if(!empty($data['product_id']) && !empty($data['model_name'])) {
							
							$this->Order_item_model->insert($data);
						
						}
						
					}

			   }
			}
		  	//print_r($_POST);exit;
		  }

			
		  //sent mail agora
		  if(isset($workflow_order)){
				
				$this->load->library('email');
				
				//send mail to sale and customer
				if($workflow_order['action'] == 'create'){
					
					$role = array(3);
					
					$all_employee = $this->Employee_model->get_where(array('is_deleted'=>0));
					
					$related_role_data = array();
					foreach($all_employee as $k => $v){
						if(strpos($v['role_id'],',')){
							$tmp_role = explode(',',$v['role_id']);	
						}else{
							$tmp_role = array($v['role_id']);	
						}
						
						if(in_array(3,$tmp_role) && !in_array(1,$tmp_role)){
							$related_role_data[] = $v;
						}
					}
					
					//$related_role_data = $this->Employee_model->get_related_role($role);
					$sitename = $this->Settings_model->get_settings(1);
					$default_email = $this->Settings_model->get_settings(6);
					
					
					if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
								
						//Send Notification sales			
						foreach($related_role_data as $k => $v){
							
							$this->email->clear();
									
							$this->email->from($default_email['value'], $sitename['value']);
							$this->email->to($v['email']); 
									
							$this->email->subject('Agora '.$workflow['title'].': '.$jobArray['serial']);
							$this->email->message('Create order '.$jobArray['serial'].'<br>'.$customer_name);
							$this->email->set_newline("\r\n");
							$this->email->send();
							
							//echo $this->email->print_debugger();
						
						}
						
						
						//customer
						$customer_email = $customer['primary_contact_email'];
						
						$this->email->clear();
									
						$this->email->from($default_email['value'], $sitename['value']);
						$this->email->to($customer_email); 
						//$this->email->to('jwtan69@i-tea.com.tw'); 
									
						$this->email->subject('Agora '.$workflow['title'].': '.$jobArray['serial']);
						$this->email->message('Dear '.$customer['primary_attention_to'].',<br>Your order ('.$jobArray['serial'].') has been received by our team.
Thank you!<br><br>This is an automatically generated email. Please do not reply to it.');
						$this->email->set_newline("\r\n");	
						$this->email->send();
						
						
						//echo $this->email->print_debugger();exit;
					}
					
				}else if($workflow_order['action'] == 'approval_update_s' && $confirm == 1){
					
					$role = array(4);
					
					$all_employee = $this->Employee_model->get_where(array('is_deleted'=>0));
					
					$related_role_data = array();
					foreach($all_employee as $k => $v){
						if(strpos($v['role_id'],',')){
							$tmp_role = explode(',',$v['role_id']);	
						}else{
							$tmp_role = array($v['role_id']);	
						}
						
						if(in_array(4,$tmp_role) && !in_array(1,$tmp_role)){
							$related_role_data[] = $v;
						}
					}
					
					
					//$related_role_data = $this->Employee_model->get_related_role($role);
					$sitename = $this->Settings_model->get_settings(1);
					$default_email = $this->Settings_model->get_settings(6);
					
					
					if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
								
						//Send Notification ground staff			
						foreach($related_role_data as $k => $v){
							
							$this->email->clear();
									
							$this->email->from($default_email['value'], $sitename['value']);
							$this->email->to($v['email']); 
									
							$this->email->subject('Agora '.$workflow['title'].': '.$jobArray['serial']);
							$this->email->message('Packing List'.$jobArray['serial'].'<br>'.$customer_name);
							$this->email->set_newline("\r\n");
							$this->email->send();
						//echo $this->email->print_debugger();
						}
						
						//customer acknowledge
						$customer_email = $customer['primary_contact_email'];
						
						$this->email->clear();
									
						$this->email->from($default_email['value'], $sitename['value']);
						$this->email->to($customer_email); 
						//$this->email->to('jwtan69@i-tea.com.tw'); 
									
						$this->email->subject('Agora '.$workflow['title'].': '.$jobArray['serial']);
						$this->email->message('Dear '.$customer['primary_attention_to'].',<br>Your order ('.$jobArray['serial'].') has been received by our team.
Thank you!<br><br>This is an automatically generated email. Please do not reply to it.');
						$this->email->set_newline("\r\n");		
						$this->email->send();
						//echo $this->email->print_debugger();exit;
					}
					
				}
		  }
		  
		  
		  //alert
		  if($mode == 'Add'){
			$alert_type = '/1';
		  }else{
			  if($correct_check == 'on'){
			  	$alert_type = '/2';
			  }else{
				$alert_type = '';  
			  }
		  }
		  		  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
			  //echo 1;exit;
		  	  redirect($lastpage.$alert_type,'refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].$alert_type));
		  }		  
		  
	  }
	  
	  public function del($id){
		  
		  $data = $this->Order_model->get($id);
		  
		  //job 也要一起del
		  $this->Job_model->update($data['latest_job_id'],array('is_deleted'=>1));
		  
		  $this->Order_model->delete($id);
		  
		  //del order item
		  $this->Order_item_model->delete_by_order_id($id);
		  
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage.'/3','refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/3'));
		  }		
	  }
	  
	  public function ajax_getQuotation(){
		  
		  	$keyword = $_POST['keyword'];
			$limit = $_POST['limit'];
			$page = $_POST['page'];
			
			$start = ($page-1) * $limit;
			//我們只取已經confirm的quotation
		  	$record_count = $this->Quotation_model->ajax_record_count($keyword, array(
				'status'	=> 6
			));
		  	$data = $this->Quotation_model->ajax_quotation($keyword, $limit, $start, array(
				'status'	=> 6
			));
		  	$paging = $this->get_paging($limit,10,$record_count,$page);
			
			$quotation_status = $this->Quotation_model->quotation_status_list();
			
			//顯示Status為狀態文字, 不是數字
			if(!empty($data)) {
				foreach($data as $k=>$v) {
					$data[$k]['status'] = $quotation_status[$v['status']];
				}
			}
		  	
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					'paging'	=> $paging,
			);
			
			echo json_encode($temp);	
			exit;
	  }	  
	  
	  public function ajax_getQuotationData(){
		  
		  	$id = $_POST['id'];
		  	$data = $this->Quotation_model->get($id);
			$employee = $this->Employee_model->get($data['create_user_id']);
			$department = $this->Department_model->get($employee['department_id']);
			$data['employee_no'] = $employee['employee_no'];
			$data['department'] = $department['name'];
			
			
			//get quotation related item
			$related_quotation_item = $this->Quotation_item_model->getRelatedItem($id);
			
			foreach($related_quotation_item as $k => $v){
				$product_data = $this->Products_model->get($v['product_id']);
				$supplier_data = $this->Suppliers_model->get($product_data['supplier']);
				$related_quotation_item[$k]['model_no'] = $product_data['model_no'];
				$related_quotation_item[$k]['part_no'] = $product_data['part_no'];
				$related_quotation_item[$k]['supplier_id'] = $product_data['supplier'];
				$related_quotation_item[$k]['cost_price'] = $product_data['cost_price'];
				$related_quotation_item[$k]['supplier_name'] = $supplier_data['company_name'];
				$related_quotation_item[$k]['supplier_pic'] = $supplier_data['contact_person'];
			}
			

		

	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					//'product'   => $related_quotation_item,
					'product' => $related_quotation_item ,
			);
			
			
			
			echo json_encode($temp);	
			exit;
	  }	 
	  
	  public function get_paging($item_per_page,$pagenum,$total_item,$page)
	  {
	
		$start = (int)(($page-1)/$pagenum)*$pagenum+1;
		$end = $start+$pagenum-1;
		$next = $page+1;
		$pre = $page-1;
		
		$total_page = ceil( $total_item / $item_per_page );
                $paging = '';
		if($total_item > $item_per_page){
                    $paging .= '<ul class="pagination">';

                    if($page > 1){
                            $paging .= '<li><a href="javascript:changePage(1)">&laquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$pre.')">&lsaquo;</li>';
                    }

                    if($total_page <= $pagenum){

                            for($i=$start;$i<=$total_page;$i++){
                                    if($i == $page){

                                            $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                    }else{

                                            $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                    }
                            }
                    }else{
                            if($page > 5){
                                    $end = $page+5;
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    $start = $end - ($pagenum - 1);

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }else{
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }	
                    }

                    if($page < $total_page){
                            $paging .= '<li><a href="javascript:changePage('.$next.')">&rsaquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$total_page.')">&raquo;</a></li>';
                    }

                    $paging .= '</ul>';
                }
		
		return $paging;
	  }
	  
	  
	  public function ajax_getModel(){
		  
		  	$id = $_POST['id'];
			$type = $this->input->post("type", true);
			
			//Products
			if($type == 1) {

		  		$data = $this->Products_model->ajax_model($id, $type);
			
			//Service
			} else if ($type == 2) {
				
				$data = $this->Products_model->getService();
				
			}
		  	
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					'type'		=> $type,
			);
			
			echo json_encode($temp);	
			exit;
	  }	   
	  
	  public function ajax_estShipping(){
		  
		  	$companyId = $_POST['companyId'];	
		  	$shipperId = $_POST['shipperId'];		
			$groupItem = json_decode($_POST['groupItem'],true);

			$shippingItem = $this->Shipping_model->get($shipperId);

			if( strpos($shippingItem['country'], ",") !== FALSE ) {
			
				$shippingCountry = explode(",",$shippingItem['country']);
			
			} else {
				
				$shippingCountry = array();
				$shippingCountry[] = $shippingItem['country'];
					
			}
			
			$shippingType = $shippingItem['shipping_type'];
			$shippingRate = json_decode($shippingItem['cost_json'],true);
			
			$estShipping = 0;
			
			foreach($groupItem as $k => $v){
				
				if($v['item_type'] == 'product'){
					
				$customer = $this->Customers_model->get($companyId);
				
				$countryCheck = 0;
				foreach($shippingCountry as $x){
					if($customer['country'] == $x){
						$countryCheck = 1;
						break;
					}
				}
				
				if($countryCheck == 1){
					
					$product = $this->Products_model->get($v['modelNo']);
					$actualWeight = $product['weight'];
					$dimensionWeight = ($product['length']*$product['breadth']*$product['height'])/6000; 
					//抓取weight
					if($actualWeight > $dimensionWeight){
						$weight = $actualWeight;
					}else{
						$weight = $dimensionWeight;
					}
					if($v['quantity']=='select'){
						$weight = 0;
					}else{
						$weight = $weight*$v['quantity'];
					}
					
					$min_diff = 999;
					$target = 0;
					$count_shippingRate = count($shippingRate);
					
					//針對每個shippingrate去看最小差距
					foreach($shippingRate as $x => $y){
						$diff_weight = abs($weight-$y['weight']);
						if($diff_weight < $min_diff){
							$min_diff = $diff_weight; //最小的差距
							$target = $x; //抓最小差距的 array(key)
						}
					}
					
					//拿搜尋的weight減掉最小差距的weight
					$weightL = $weight - $shippingRate[$target]['weight'];
					
					//如果是大於零, 代表我們需要看他的下一層是否存在
					if($weightL > 0){

						//他的下一層有存在
						if(isset($shippingRate[($target+1)]['weight'])) {
							$cost = $shippingRate[($target+1)]['cost'];
						//不存在就取當下的這層	
						} else {
							$cost = $shippingRate[$target]['cost'];	
						}
					//如果等於零, 代表剛剛好是這層							
					}else{
						$cost = $shippingRate[$target]['cost'];
					}
					
					if($cost > $estShipping){
						$estShipping = $cost;
					}
				//管理者根本沒有設定這個國家的運費	
				}else{
					$estShipping = 0;
				}
				
				}
				
			}
			
		  	
	  		$temp = array(
					'status'	=> 'OK',
					'estShipping'		=> $estShipping,
			);
			
			echo json_encode($temp);	
			exit;
	  }	   
	  
	  
	  public function ajax_getProductDetail(){
		  	$id = $_POST['id'];
			
			$product = $this->Products_model->get($id);
			
			$temp = array(
					'status'	=> 'OK',
					'product'		=> $product,
			);
			
			echo json_encode($temp);	
			exit;
			
			
	  }
	  
	  private function PDF_generation($id, $mode='I') {
		  
		    //letter head
		  	$company_name = $this->Settings_model->get_settings(15);
			$d_email = $this->Settings_model->get_settings(6);
			$reg_no = $this->Settings_model->get_settings(29);
			$d_web = $this->Settings_model->get_settings(28);
			$postal = $this->Settings_model->get_settings(25);
			$address = $this->Settings_model->get_settings(16);
			$contact_no = $this->Settings_model->get_settings(8);
		  
		  
		  	$gst = $this->Settings_model->get_settings(5);
			$data = $this->Order_model->get($id);
			
			//product list
			$product_list = $this->Products_model->getIDKeyArray('product_name');
			
			//date		
		 	$date = $data['order_date'];
			$tmp = explode('-',$date);
			$date = $tmp[2]."/".$tmp[1]."/".$tmp[0];
			
			
			$requestor = $this->Employee_model->get($data['requestor_id']);
			$data['requestor_name'] = $requestor['full_name'];
				
			$product = $this->Order_item_model->getRelatedItem2($id);
				
			//sum product by id
			foreach($product as $k => $v){
				
				$tmp_qty = $this->Product_log_model->sum_by_id($v['product_id']);
				
				if($tmp_qty != ''){
					$product[$k]['qty'] = $tmp_qty;
				}else{
					$product[$k]['qty'] = 0;
				}
			}
				
		 	$customer = $this->Customers_model->get($data['customer_id']);
		  	  
		  
		  	$item_group = '';
			$total = 0;
		  	foreach($product as $k => $v){
			  	$item_group .='<tr><td>'.($k+1).'</td><td>'.$product_list[$v['product_id']].'</td><td>'.$v['model_name'].'</td><td>'.$v['qty'].' '.$v['uom'].'(s)'.'</td><td>'.$v['quantity_order'].' '.$v['uom'].'(s)'.'</td><td>'.$v['price'].'</td><td>'.$v['price']*$v['quantity_order'].'</td></tr>';
				$total += $v['price']*$v['quantity_order'];
		  	}
			
			
		  	$logo = $this->Settings_model->get_settings(2);
		  
		  $this->load->library("Pdf");		
          $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		  
		  // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Anexus');
            $pdf->SetTitle("Packing List");
            $pdf->SetSubject("");
            $pdf->SetKeywords("");

            // remove default header/footer
            $pdf->setPrintHeader(false);			
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(10, 10, 10);

            // set auto page breaks
            //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
            }

            // ---------------------------------------------------------

            // set font
            $pdf->SetFont('times', '', 10);

            

            // set font
            $pdf->SetFont('times', '', 10);
			
			// add a page
       		$pdf->AddPage();
			
			
			$html = '
			<table width="100%" cellpadding="5">
			
				<tr>
					<td width="60%">
						<table  width="100%">
							<tr>
								<td>
									<b>'.$company_name['value'].'</b><br>
									'.$d_email['value'].'<br>
									'.$d_web['value'].'<br>
									Company Registration No. '.$reg_no['value'].'<br>
									'.$address['value'].'<br>
									'.$postal['value'].'<br>
									Contact No. '.$contact_no['value'].'<br>
								</td>
							</tr>
						</table>
					</td>
					
					<td>
						<img height="80px" width="" src="'.$logo['value'].'">
					</td>
				</tr>
			</table>
			
			<table width="100%" cellpadding="5">
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="">
				
							<tr>
								<th colspan="4"><h1 style="color:#428bca">Order</h1></th>
							</tr>
							<tr>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td colspan="1" width="20%"><h4>Order To</h4></td>
								<td colspan="1" width="50%"><h4>Delivery To</h4></td>
								<td colspan="1" width="15%">Order No.</td>
								<td colspan="1">'.$data['order_no'].'</td>
							</tr>
							<tr>
								<td colspan="1" width="20%">'.$customer['primary_attention_to'].'</td>
								<td colspan="1" width="50%">'.$data['alias'].'</td>
								<td colspan="1" width="15%">Date</td>
								<td colspan="1">'.$date.'</td>
							</tr>
							<tr>
								<td>'.$customer['company_name'].'</td>
								<td>'.$customer['company_name'].'</td>
							</tr>
							<tr>
								<td>'.$customer['address'].'</td>
								<td>'.$data['address'].'</td>
							</tr>
							<tr>
								<td>'.$customer['country'].' '.$customer['postal'].'</td>
								<td>'.$data['country'].' '.$data['postal'].'</td>
							</tr>
							
						</table>

					</td>

				</tr>
				
				<tr>
					<td colspan="2">
						
						<table width="100%" cellpadding="5">
				
							<tr style="background-color:#B6DDFD;color:#62A7DE">
								<td width="5%">#</td>
								<td width="15%">Product Name</td>
								<td width="15%">Country</td>
								<td width="20%">Inventory Balance</td>
								<td width="15%">Quantity</td>
								<td width="15%">Unit Price</td>
								<td width="15%">Sub Total</td>
							</tr>'.$item_group.'
							<tr>
								<td colspan="5"></td>
								<td>Gst '.$gst['value'].'%</td>
								<td>'.(($total*$gst['value'])/100).'</td>
							</tr>
							<tr>
								<td colspan="5"></td>
								<td>Total</td>
								<td>'.((($total*$gst['value'])/100)+$total).'</td>
							</tr>
							
						</table>
						
					</td>
				</tr>
				<tr>
					<td colspan="2">
						
						<table width="100%" cellpadding="5">
				
							<tr>
								<th><h4>Remarks</h4></th>
							</tr>
							<tr>
								<td>'.$data['remark'].'</td>
							</tr>
						</table>
						
					</td>
				</tr>

			</table>';
			
			$pdf->writeHTML($html, true, false, true, false, '');
									
			
			$file_name = date("YmdHis").rand(1000,9999);
			
			if($mode == 'I') {
				$pdf->Output($file_name.'.pdf', 'I'); 	
				return $file_name.'.pdf';
			} else if ($mode == 'f') {				
				$pdf->Output('./uploads/'.$file_name.'.pdf', 'f'); 
				return './uploads/'.$file_name.'.pdf';
			}
			
		  		  
		  
	  }
	  
	  public function export_pdf($id){		  
		  $this->PDF_generation($id, 'I');			
	  }
	  
	  public function sent_mail($id,$product_total=0,$service_total=0){
		  
		   	$filename = $this->PDF_generation($id, 'f');	 		  
		  
		    $tender_data = $this->Tender_model->get($id);
		  	$customer = $this->Customers_model->get($tender_data['customer_id']);
		  
		  	$send_to = $customer['email'];
			
			$sitename = $this->Settings_model->get_settings(1);
		  	$default_email = $this->Settings_model->get_settings(6);
			
			 //$requestor_role
			$requestor_employee = $this->Employee_model->get($tender_data['requestor_id']);
			$requestor_role = $requestor_employee['role_id'];
		  
		 	$role_id = $this->data['userdata']['role_id'];
			
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $tender_data['status'],
							'workflow_id' => 3,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(3);
						$status = json_decode($workflow['status_json'],true);
						
						if(isset($action)){

		
						if($action == 'send_email' || $action == 'sendmail_update'){
							
							//next status
							$next_status = $workflow_order['next_status_id'];
							$next_status_text = $status[$workflow_order['next_status_id']];
							$who_email = $workflow_order['who_email'];
	
						}

						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];

								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 1,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
							
							$jobArray = array(
								'parent_id' 		=>$tender_data['latest_job_id'],
								'user_id' 			=>$this->data['userdata']['id'],
								'customer_id' 		=>$tender_data['customer_id'],
								'type' 				=> 'Tender',
								'type_path' 		=>'sales/tender_edit/',
								'awaiting_role_id' 	=>$awating_person.',',
								'status_id' 		=>$next_status,
								'status_text'		=>$next_status_text,
								'serial' 			=>$tender_data['qr_serial'],
								'type_id' 			=>$tender_data['id'],
								'created_date'		=>date("Y-m-d H:i:s"),
								'modified_date'		=>date("Y-m-d H:i:s"),
							);			
					  
						    $array = array(
							'awaiting_table' => $awating_person.',',
							'status' => $next_status,
							'modified_date' => date('Y-m-d H:i:s'),
							'lastupdate_user_id' => $this->data['userdata']['id'],
						    );
							
							//print_r($jobArray);exit;

						
						  //更新上一個JOB的狀態					  
						  $this->Job_model->update($jobArray['parent_id'], array(
							'is_completed' => 1,
							'display' => 0,
						  ));
						  
						  //新增一個新的JOB
						  $new_job_id = $this->Job_model->insert($jobArray);
						  $array['latest_job_id'] = $new_job_id;
						  
						  $this->Tender_model->update($tender_data['id'], $array);
							
							
						}
						
						}
			
			
			if(isset($who_email)){
			
				$this->load->library('email');
				
				switch($who_email){
					case 'customer':
							
							//Send Notification
							if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
										
								$this->email->from($default_email['value'], $sitename['value']);
								$this->email->to($send_to); 
							
								$this->email->subject('Anexus '.$workflow['title'].': '.$tender_data['qr_serial']);
								$this->email->message($tender_data['qr_serial']);	
								$this->email->attach($filename);
								$this->email->set_newline("\r\n");
								
								$this->email->send();
							
							}
							
					break;	
					case 'supplier':
						
							  //count supplier
							  $supplier_count = $this->Purchase_request_item_model->get_supplier_groupBy($id);
							  
							  foreach($supplier_count as $k => $v){
									
								  $supplier = $this->Suppliers_model->get($v['supplier_id']);
								  
								  $file_name = $this->PDF_generation($id, 'f',$v['supplier_id']);	
										 
								  if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
									
									$this->load->library('email');
									$this->email->clear();	
									$this->email->from($default_email['value'], $sitename['value']);
									$this->email->to($supplier ['email']); 
									
									$this->email->subject('Anexus Purchase request '.$tender_data['qr_serial']);
									$this->email->message($tender_data['qr_serial']);	
									$this->email->attach($file_name);
									$this->email->set_newline("\r\n");
									
									$this->email->send();
									
								  }
							  
							  }
							
					break;
					case 'employee':
					break;
				
				}
				
			}
			
			 //mail
			 $mail_array = array(
				'creater_name' =>$this->data['userdata']['name'],
				'title' =>'Purchase '.$tender_data['qr_serial'].' has been send out',
			 );
			 
			 if(isset($new_job_id)){
			  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
			  $mail_array['related_role'] = $related_role;		
			  $mail_array['title'] = $workflow['title']." ".$tender_data['qr_serial']." ".$next_status_text;
			 }
			 echo $_SERVER['HTTP_HOST'];exit;
			 //sent Notification to related role						  			
		  	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local" && isset($mail_array['related_role'])) {
				$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
			  				  	
			  	foreach($related_role_data as $k => $v){
				  
				  	$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
					
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->set_newline("\r\n");
					$this->email->send();	
				  
		 		}
			}
			
			
			$lastpage = $this->session->userdata("lastpage");
			if(!empty($lastpage)) {
			  redirect($lastpage,'refresh');  
			} else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
			}		 			
		  
	  }
	  
	  
	  public function searchProduct($q=""){

		$product_name = $this->Products_model->get_groupBy_like($q);

      	$tmp = array();
      	foreach($product_name as $v) {
      		$tmp[] = array(
      			'value' => $v['product_name'],
      			'label' => $v['product_name'],
      		);
      	}

      	echo json_encode($tmp);

      }

}

?>