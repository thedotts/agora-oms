<?php

class Inventory_request_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();  
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Customers_model');
			$this->load->model('Inventory_request_model');
			$this->load->model('Purchase_request_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Employee_model');
			$this->load->model('Settings_model');
			$this->load->model('Workflow_model');
			$this->load->model('Workflow_order_model');

            $this->data['init'] = $this->function_model->page_init();
			$this->data['status_list'] = $this->Inventory_request_model->status_list();
			
			$this->data['role_list'] = $this->Role_model->getIDKeyArray('name');
			
			//get status list
			$ir_status_list = $this->Workflow_model->get(7);
			$this->data['ir_status_list'] = json_decode($ir_status_list['status_json']);
			
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$this->data['userdata'] = $this->session->userdata("userdata");
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}           
			
			$this->data['group_name'] = "service";  
			$this->data['model_name'] = "inventory_request";  
			$this->data['common_name'] = "Inventory Request";   
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			} 
			
			//related role
		    define('SUPERADMIN', 1);
		    define('SERVICE_MANAGER', 3);
		    define('SERVICE_EXECUTIVE', 4);
			define('GENERAL_MANAGER', 2);
			
           
      }
   
      public function index($q="ALL", $status="ALL", $page=1) {  
          			
            $this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/anexus/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/'.$status.'/';
			if($q == 'ALL') {
				$q = "";
			}
			$this->data['q'] = $q;
			
			if($status != 'ALL') {
				$filter['status'] = $status;	
			}
			$this->data['status'] = $status;			
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Inventory_request_model->record_count($filter, $q);
			
			//get particular ranged list
			$this->data['results'] = $this->Inventory_request_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start);
			if(!empty($this->data['results'])){			
				foreach($this->data['results'] as $k => $v){
					if(!empty($v['awaiting_table'])){
							$this->data['results'][$k]['awaiting_table']= explode(',',$v['awaiting_table']);
					}
				}
			}
						
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->data['customer_list'] = $this->Customers_model->getIDKeyArray("company_name");
			$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
			
			$this->session->set_userdata("lastpage", $url.$page);
			if(!empty($this->data['results'])){		
				foreach($this->data['results'] as $k => $v){
					$purchase_data = $this->Purchase_request_model->get($v['purchase_request_id']);
					$this->data['results'][$k]['customer_name']='';
					if(!empty($purchase_data)){
					$this->data['results'][$k]['customer_name'] = $purchase_data['customer_name'];
					}
				}
			}
			
			//print_r($this->data['results']);exit;
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  
	  public function add($id=false) {  
          	
			$role_id = $this->data['userdata']['role_id'];
			
			if($id !== false) {
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Inventory_request_model->get($id);
				
				$requestor_employee = $this->Employee_model->get($this->data['result']['requestor_id']);
			    $requestor_role = $requestor_employee['role_id'];
				$this->data['requestor_role'] = $requestor_role;
				if(!empty($this->data['result']['awaiting_table'])){
				$this->data['result']['awaiting_table'] = explode(',',$this->data['result']['awaiting_table']);
				}
				
				$default = 0;
				//////////////////////////////////////////////////////////////////////////////////
				
				   		//get related workflow order data
						$workflow_flitter = array(
							'status_id' => $this->data['result']['status'],
							'workflow_id' => 7,
						);
						
						
						
						$type='';
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$type = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$type = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
							$type = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
									$type = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(7);
						$status = json_decode($workflow['status_json'],true);
						
						$this->data['workflow_title'] = $workflow['title'];
					
					$this->data['btn_type'] = $type;
					//print_r($workflow_order);exit;
					
					switch($type){
						case 'edit':
							$this->data['head_title'] = 'Edit '.$workflow['title'].' Request';
						break;
						case 'approval':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
						break;
						case 'approval_update':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'update':
							$this->data['head_title'] = 'Update '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'send_email':
							$this->data['head_title'] = 'Waiting to Send '.$workflow['title'].' Request';
						break;
						case 'sendmail_update':
							$this->data['head_title'] = 'Waiting to Send/Edit '.$workflow['title'].' Request';
						break;
						case 'upload':
							$this->data['head_title'] = 'Upload Confirm File '.$workflow['title'].' Request';
						break;
						case 'complete':
							$this->data['head_title'] = $workflow['title'].' Request';
							$this->data['last_action'] = $workflow_order['action_before_complete'];
						break;
						default:
							$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					foreach(json_decode($workflow['status_json'],true) as $k => $v){
						
						if($v == 'Completed'){
							$this->data['completed_status_id'] = $k;
							break;
						}
					}
				//////////////////////////////////////////////////////////////////////////////////////
				
				/*
				if($this->data['result']['status'] == 1){
					//se edit
					if($role_id == SERVICE_EXECUTIVE){
						$this->data['head_title'] = 'Edit Inventory Request';
					//sm approved	
					}else if($role_id == SERVICE_MANAGER){
						$this->data['head_title'] = 'Approve Inventory Request';
					}else{
						$default = 1;
					}
				
				}else if($this->data['result']['status'] == 2){
					
					//sm edit
					if($role_id == SERVICE_MANAGER){
						$this->data['head_title'] = 'Edit Inventory Request';
					//gm approved	
					}else if($role_id == GENERAL_MANAGER){
						$this->data['head_title'] = 'Approve Inventory Request';
					}else{
						$default = 1;
					}
					
				}else{
					$default = 1;
				}
				
				//no related person
				if($default == 1){
					$this->data['head_title'] = 'Inventory Request';	
				}
				*/
												
				
			} else {
				$this->data['mode'] = 'Add';	
				$this->data['head_title'] = 'Create Inventory Request';
				
				//get related workflow order data
					$workflow_flitter = array(
						'role_id' => $role_id,
						'status_id' => -1,
						'workflow_id' => 7,
					);
					
					//data form database
					$workflow = $this->Workflow_model->get(7);
					$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
					
					//get view bottom layout
					$type = '';
					if(!empty($workflow_order)){
						$type = $workflow_order['action'];
						if($workflow_order['action'] == 'create'){
							$this->data['head_title'] = 'Create '.$workflow['title'].' Request';
						}else{
							$this->data['head_title'] = $workflow['title'].' Request';
						}
					}else{
						$this->data['head_title'] = $workflow['title'].' Request';
					}
					$this->data['btn_type'] = $type;
			}
			
			
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		
            
			
      }	  	  
	  
	  public function submit(){
		  //print_r($_POST);exit;
		  $now = date("Y-m-d H:i:s");
		  $mode = $this->input->post("mode", true);
		  $id = $this->input->post("id", true);
		  
		  $role_id = $this->data['userdata']['role_id'];
		  
		  //get value approve or not approve
		  $confirm = $this->input->post("confirm", true);
		  $correct_check = $this->input->post("correct_check", true);
		  
		  $next = 0;
		  $last = 0;
		  $status_change = 0;
		  
		    //job
		  	$jobArray = array(
				'parent_id' =>0,
				'user_id' =>$this->data['userdata']['id'],
				'customer_id' =>0,
				'type' => 'Inventory Request',
				'type_path' =>'service/inventory_request_edit/',
		 	 );
		
		  $array = array(	
		  	'purchase_request_no' => $this->input->post("purchase_request_no", true),
			'item' => $this->input->post("item", true),
			'qty' => $this->input->post("qty", true),
			'reason' => $this->input->post("reason", true),
			'requestor_id' => $this->data['userdata']['id'],	
		  );
		  
		    //mail
			$mail_array = array(
		  	'creater_name' =>$this->data['userdata']['name'],
		  	);
		  
		  $purchase_serial = $this->input->post("purchase_request_no", true);
		  $purchase_data = $this->Purchase_request_model->getBySerial($purchase_serial);
		  $array ['purchase_request_id'] = 0;
		  if(!empty($purchase_data)){
			  $array ['purchase_request_id'] = $purchase_data['id'];
		  }

		  //如果是SuperAdmin就可以直接控制status
		  if($role_id == SUPERADMIN){
			  $array['status'] = $this->input->post("status", true);
		  }
		  
		  
		  //Add
		  if($mode == 'Add') {			  			  
			  
			  /*
			  if($role_id == SERVICE_EXECUTIVE){
					
					//main
					$array['awaiting_table']  = SERVICE_MANAGER;
					$array['status']  = 1;
					//job
					$jobArray['awaiting_role_id'] = SERVICE_MANAGER.',';
					$jobArray['status_id'] = 1;
					$jobArray['status_text'] = $this->data['ir_status_list'][1];
					//mail
					$mail_array['related_role'] = array(SERVICE_EXECUTIVE,SERVICE_MANAGER);
					$mail_array['title'] = 'create tender';
					
			  }else if($role_id == SERVICE_MANAGER){
				  
				    //main
					$array['awaiting_table']  = GENERAL_MANAGER;
					$array['status']  = 2;
					//job
					$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
					$jobArray['status_id'] = 2;
					$jobArray['status_text'] = $this->data['ir_status_list'][2];
					//mail
					$mail_array['related_role'] = array(SERVICE_MANAGER,GENERAL_MANAGER);
					$mail_array['title'] = 'create tender';
			  }
			  */
			  
			  ////////////////////////////////////////////////////////////////////////////////////
				
				//get related workflow order data
				$workflow_flitter = array(
					'role_id' => $role_id,
					'status_id' => -1,
					'workflow_id' => 7,
				);
				
				
				
				//data form database
				$workflow = $this->Workflow_model->get(7);
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
				$status = json_decode($workflow['status_json'],true);
				
				
				
				if(!empty($workflow_order)){
				
				//ad_hoc
				if($workflow_order['ad_hoc'] == 1){
					$array['ad_hoc'] = 1;
				}
				
				//purchaser
				$is_adhoc = false;
				if($role_id == 10){
					$is_adhoc = true;
				}
				
				//next status
				$next_status = $workflow_order['next_status_id'];
				$next_status_text = $status[$workflow_order['next_status_id']];
				
			  	//if need_det = 1
				if($workflow_order['need_det'] == 1){
					
					$formula = json_decode($workflow_order['formula'],true);
					
					//check if product or service price over value
					foreach($formula as $k => $v){
					
						$price = $this->input->post($v['target_colum'], true);
						$det_temp = $price." ".$v['logic']." ".$v['value'];
						$check = $this->parse_boolean($det_temp);
						
						//check price over value
						if($check){
							$next_status = $v['next_status_id'];
							$next_status_text = $status[$v['next_status_id']];
							break;
						}
						
					}
					
				}
				
				//get next status workflow order data
				$next_workflow_flitter = array(
					'status_id' => $next_status,
					'workflow_id' => 7,
				);
				
				//next status data
				$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
				$awating_person = $next_status_data['role_id'];
				
				
				
				if($awating_person == 'requestor'){
					$awating_person = $this->input->post("requestor_id", true);
				}
				//echo $awating_person;exit;
				
				//main
				$array['awaiting_table']  = $awating_person;
				$array['status'] = $next_status;
				//main
				$jobArray['awaiting_role_id'] = $awating_person.',';
				$jobArray['status_id'] = $next_status;
				$jobArray['status_text'] = $next_status_text; 	
				  
				  //insert main data
					$array['created_date'] = $now;
					$array['modified_date'] = $now;
					$array['create_user_id'] = $this->data['userdata']['id'];
					$array['lastupdate_user_id'] = $this->data['userdata']['id'];

					//新增資料
					$insert_id = $this->Inventory_request_model->insert($array);				
					
					//更新purchase serial
			  		$serial_no = $this->Inventory_request_model->zerofill($insert_id, 5);
					
					//insert job
					$jobArray['serial'] = $serial_no;
					$jobArray['type_id'] = $insert_id;
					
					$jobArray['created_date'] = $now;
					$jobArray['modified_date'] = $now;
					
					$job_id = $this->Job_model->insert($jobArray);
				  
					//after insert job,update job id,qr serial....
					$array =array(
						'job_id'	=> $job_id,
						'serial_no'  => $serial_no,
						'latest_job_id'	=> $job_id,
					);
					$this->Inventory_request_model->update($insert_id, $array);
					
				  //mail
				  $related_role = $this->Job_model->getRelatedRoleID($job_id);
				  $mail_array['related_role'] = $related_role;		
				  $mail_array['title'] = $workflow['title']." ".$serial_no." ".$next_status_text; 
				  	
				}
				
				////////////////////////////////////////////////////////////////////////////////////
			  
			  /*
			  $array['lastupdate_user_id'] = $this->data['userdata']['id'];
			  $array['create_user_id'] = $this->data['userdata']['id'];
			  $array['created_date'] = $now;
			  $array['modified_date'] = $now;

				
			  			  //print_r($array);exit;
			  $insert_id = $this->Inventory_request_model->insert($array);	
			  $serial_no = $this->Inventory_request_model->zerofill($insert_id, 5);
			  
			  
			  
			  //insert job
				$jobArray['created_date'] = date("Y-m-d H:i:s");
			    $jobArray['modified_date'] = date("Y-m-d H:i:s");
				$jobArray['serial'] =$serial_no;
			    $jobArray['type_id'] =$insert_id;
			    $job_id = $this->Job_model->insert($jobArray);
			  	
			    //after insert job,update job id,qr serial....
			    $array =array(
			  	'job_id'	=> $job_id,
			  	'serial_no'  => $serial_no,
				'latest_job_id'	=> $job_id,
			    );

				
			  	$this->Inventory_request_model->update($insert_id, $array);		
				//print_r($array);exit;
				*/
				
			  
		  //Edit	  			  
		  } else {
			  
			    $ir_data = $this->Inventory_request_model->get($id);
			 	$requestor_employee = $this->Employee_model->get($ir_data['requestor_id']);
			  	$requestor_role = $requestor_employee['role_id'];
			  	$ir_status = $ir_data['status'];
			  
			  	$jobArray['serial'] =$ir_data['serial_no'];
			  	$jobArray['type_id'] =$ir_data['id'];
			  	$jobArray['parent_id'] = $ir_data['latest_job_id'];
				
				$next = 0;
			  	$last = 0;
			  	$status_change = 0;
				
				/*
				//判断status 给予下个job
			  
				//se edit or sm comfirm
				if($ir_status == 1){
					
						if($role_id == SERVICE_MANAGER){
							
							//sm denied
							if($confirm == 0){
								$array['awaiting_table']  = '';
								$array['status']  = 0;
							  
								//mail
								$mail_array['related_role'] = array(SERVICE_MANAGER,SERVICE_EXECUTIVE);
								$mail_array['title'] = 'Denied tender';
							  
								
								$next = 1;
								$last = 1;
								
							//sm approved
							}else if($correct_check == 'on'){
												
								$array['awaiting_table']  = '';
								$array['status']  = 3;
											
								//mail
								$mail_array['related_role'] = array(SERVICE_MANAGER,SERVICE_EXECUTIVE);
								$mail_array['title'] = 'Approve tender';
												
								$next = 1;	
								$last = 1;	
								
							}
							
						}
						
				//sm edit or gm comfirm
				}else if($ir_status == 2){
						
						if($role_id == GENERAL_MANAGER){
							
							//gm denied
							if($confirm == 0){
								$array['awaiting_table']  = '';
								$array['status']  = 0;
							  
								//mail
								$mail_array['related_role'] = array(SERVICE_MANAGER,GENERAL_MANAGER);
								$mail_array['title'] = 'Denied tender';
							  
								
								$next = 1;
								$last = 1;
								
							//gm approved
							}else if($correct_check == 'on'){
												
								$array['awaiting_table']  = '';
								$array['status']  = 3;
											
								//mail
								$mail_array['related_role'] = array(SERVICE_MANAGER,GENERAL_MANAGER);
								$mail_array['title'] = 'Approve tender';
												
								$next = 1;	
								$last = 1;	
								
							}
							
						}
						//print_r($array);exit;
						
				}
			  */
			  
			  /////////////////////////////////////////////////////////////////////////////////
			  
			  //can edit status
			  $edit_status = $this->Workflow_order_model->get_status_edit(7);
			  
			  //requestor edit
			  if(in_array($ir_status, $edit_status) && $role_id == $requestor_role){
			  	  
						//get related workflow order data
						$workflow_flitter = array(
							'role_id' => $role_id,
							'status_id' => -1,
							'workflow_id' => 7,
						);
						
						//data form database
						$workflow = $this->Workflow_model->get(7);
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						$status = json_decode($workflow['status_json'],true);
						
						if(!empty($workflow_order)){
						
						//next status
						$next_status = $workflow_order['next_status_id'];
						$next_status_text = $status[$workflow_order['next_status_id']];
						
						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];
								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						//get next status workflow order data
						$next_workflow_flitter = array(
							'status_id' => $next_status,
							'workflow_id' => 7,
						);
						
						//next status data
						$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
						$awating_person = $next_status_data['role_id'];
						
						//if awating person equal requestor ,asign requestor for it
						if($awating_person == 'requestor'){
							$awating_person = $requestor_role;
						}
						
						//main
						$array['awaiting_table']  = $awating_person;
						$array['status'] = $next_status;
						
						//job
						$jobArray['awaiting_role_id'] = $awating_person.',';
						$jobArray['status_id'] = $next_status;
						$jobArray['status_text'] = $next_status_text;
						
						
						}
						
				  
			  }else{
				  		
												
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $ir_status,
							'workflow_id' => 7,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(5);
						$status = json_decode($workflow['status_json'],true);
						
						//print_r($workflow_order);exit;
						if(isset($action)){
						
						//action
						switch ($action) {
							case 'approval':
							
								//clear data infront
								$array = array();
							
								//approved
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];

										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
										
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'approval_sendmail':
							
								//clear data infront
								$array = array();
							
								//approved
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];

										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
										
										//sent mail to service manager
										$role_service_manager = $this->Employee_model->get_related_role(array($workflow_order['who_email']));
										$sitename = $this->Settings_model->get_settings(1);
										$default_email = $this->Settings_model->get_settings(6);
										
										$this->load->library('email');
										
										if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
											foreach($role_service_manager as $k => $v){
												
												$this->email->clear();
												$this->email->from($default_email['value'], $sitename['value']);
												$this->email->to($v['email']); 
												$this->email->subject('Service request completed');
												$this->email->message($sr_data['service_serial'].' Service request had completed');
												$this->email->send();
											  
											}
										}
										
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'update':
								
								//next status
								$next_status = $workflow_order['next_status_id'];
								$next_status_text = $status[$workflow_order['next_status_id']];
								
								$array = array(
								);
								
								//check last job
								if($workflow_order['next_status_text'] == 'Completed'){
									$last = 1;
								}
								
								$next = 1;
								
								break;
							case 'approval_update':
								
								//clear data infront
								$array = array();
								
								if($confirm == 1){
									
									if($correct_check == 'on'){
										
										//next status
										$next_status = $workflow_order['next_status_id'];
										$next_status_text = $status[$workflow_order['next_status_id']];
										
										$target_colum = json_decode($workflow_order['target_colum'],true);
										
										//check update colum single or multiple
										foreach($target_colum as $k => $v){
										
											if($v['type'] == 'single'){
												
												if($v['data_type'] == 'string'){
													
												$array[$v['name']] = $this->input->post($v['name'],true);
												
												}else if($v['data_type'] == 'date'){
													
													$date = $this->input->post($v['name'], true);
													if(!empty($date)) {
													  $tmp = explode("/", $date);
													  $date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
													  $array[$v['name']] = $date;
													}
													
												}
											
											}else{
												
											}
											
										}
										$next = 1;
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
									}
									
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
							case 'upload':
							
								//clear data infront
								$array = array();
								
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'comfirm_file'	=> $save_path,
											);	
											
											//check last job
											if($workflow_order['next_status_text'] == 'Completed'){
												$last = 1;
											}
											
											//next status
											$next_status = $workflow_order['next_status_id'];
											$next_status_text = $status[$workflow_order['next_status_id']];	
											$next = 1;
																							
									}	
									
								}
								
								break;	
							case 'complete':
							
								//clear data infront
								$array = array();
							
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'comfirm_file'	=> $save_path,
											);	
										
									}	
								}
								
								break;	
								
						}
						
						//print_r($workflow_order);exit;
						//if need_det = 1
						
						if($workflow_order['need_det'] == 1){

							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$value = $this->input->post($v['target_colum'], true);
								//string or number
								if(is_numeric($v['value'])){
									$det_temp = $value." ".$v['logic']." ".$v['value'];
								}else{
									
									if($v['value'] == 'original'){
										$string = $sr_data['term_condition'];
									}else{
										$string = $v['value'];
									}
									
									$det_temp = "'".md5(trim($value))."' ".$v['logic']." '".md5(trim($string))."'";
								}
								
								
								$check = $this->parse_boolean($det_temp);
								
								//check
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
							}
							
						}

						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 5,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
						}
						
						}
						
				  
			  }
			  
			 	if(isset($next_status)){
					//main
					$array['awaiting_table']  = $awating_person;
					$array['status'] = $next_status;
					//job
					$jobArray['awaiting_role_id'] = $awating_person.',';
					$jobArray['status_id'] = $next_status;
					$jobArray['status_text'] = $next_status_text;
				}
				//print_r($array);exit;
			  //////////////////////////////////////////////////////////////////////////////////////
			  
			  $array['lastupdate_user_id'] = $this->data['userdata']['id'];
			  $array['modified_date'] = $now;
			  
			  if($next == 1){
				  $lastjob_update = array(
				  	'is_completed' => 1,
					'display' => 0,
				  );
				  $this->Job_model->update($jobArray['parent_id'], $lastjob_update);
				  if($last == 0){
				  $new_job_id = $this->Job_model->insert($jobArray);
				  $array['latest_job_id'] = $new_job_id;
				  }
			  	}
				
			  $tender_id = $id;
			  
			  $this->Inventory_request_model->update($id, $array);
			  
			   //mail
				  if(isset($new_job_id)){
				  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
				  $mail_array['related_role'] = $related_role;		
				  $mail_array['title'] = $workflow['title']." ".$ir_data['serial_no']." ".$next_status_text;
				  }
			    
		  }
		  		
		    //sent mail
			if(isset($mail_array['related_role'])){
				
				if(isset($mail_array['related_role']) && !empty($mail_array['related_role'])) {
					$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
					$sitename = $this->Settings_model->get_settings(1);
					$default_email = $this->Settings_model->get_settings(6);
					
					$this->load->library('email');
					
					if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
								
						foreach($related_role_data as $k => $v){
						$this->email->clear();
						$this->email->from($default_email['value'], $sitename['value']);
						$this->email->to($v['email']); 
						
						$this->email->subject($mail_array['title']);
						$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
						$this->email->send();	
						  
						}
					
					}
				}
			
			}
				  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		  
		  
	  }
	  
	  public function del($id){
		  $this->Inventory_request_model->delete($id);
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		
	  }  	  
	  

}

?>