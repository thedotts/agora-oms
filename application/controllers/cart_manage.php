<?php

class Cart_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();  
			$this->load->helper('cookie');
			
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Customers_model');
			$this->load->model('Tender_model');
			$this->load->model('Tender_item_model');
			$this->load->model('Employee_model');
			$this->load->model('Department_model');
			$this->load->model('Products_model');
			$this->load->model('Product_category_model');
			$this->load->model('Suppliers_model');
			$this->load->model('Shipping_model');
			$this->load->model('Products_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Settings_model');
			$this->load->model('Quotation_model');
			$this->load->model('Quotation_item_model');
			$this->load->model('Workflow_model');
			$this->load->model('Workflow_order_model');
			$this->load->model('Order_model');
			$this->load->model('Product_log_model');
			$this->load->model('Order_item_model');
			$this->load->model('Customers_model');
			
            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
			
			$this->data['role_list'] = $this->Role_model->getIDKeyArray('name');
			$status_list = $this->Workflow_model->get(1);
			$this->data['status_list'] = json_decode($status_list['status_json']);
			
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$userdata = $this->session->userdata("userdata");
				
				//print_r($userdata);exit;
				if(strpos($userdata['role_id'],',')){
					$userdata['role_id'] = explode(',',$userdata['role_id']);	
				}else{
					$userdata['role_id'] = array($userdata['role_id']);	
				}
				
				
				$this->data['userdata'] = $userdata;
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}           
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			//print_r($this->data['staff_info']);exit;
			
			$this->data['group_name'] = "customer";  
			$this->data['model_name'] = "cart";  
			$this->data['common_name'] = "Shopping Cart";   
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			/*
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
					
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}  
			*/
            
			//related role
		    define('SUPERADMIN', 1);
		    define('SALES_MANAGER', 7);
		    define('SALES_EXECUTIVE', 8);
		   
      }
   
      public function index() {  
          	
			//gst
			$this->data['gst'] = $this->Settings_model->get_settings(5);
			
			//cart
			$this->data['cart'] = json_decode($this->input->cookie('Cart', TRUE),true);	
			
			//customer
			$customer = $this->Customers_model->get($this->data['staff_info']['customer_id']);
			$this->data['alias'] = $customer['delivery_address'];
			
			//print_r($this->data['staff_info']);exit;
			
			//Grouping URL			
			//$url = base_url().$this->data['init']['langu'].'/agora/'.$this->data['group_name'].'/'.$this->data['model_name'];
			
			//$this->session->set_userdata("lastpage", $url);		
						
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  
	  public function add($id=false) {  
          			
			$role_id = $this->data['userdata']['role_id'];
					
			if($id !== false) {
				
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Order_model->get($id);
					
				$order_date = $this->data['result']['order_date'];
				$tmp = explode('-',$order_date);
				$this->data['result']['order_date'] = $tmp[2]."/".$tmp[1]."/".$tmp[0];
				
				$product = $this->Order_item_model->getRelatedItem($id);
				$this->data['result']['product'] = json_encode($product);
				
				$requestor_employee = $this->Employee_model->get($this->data['result']['requestor_id']);
			    $requestor_role = $requestor_employee['role_id'];
				$this->data['requestor_role'] = $requestor_role;
				if(!empty($this->data['result']['awaiting_table'])){
				$this->data['result']['awaiting_table'] = explode(',',$this->data['result']['awaiting_table']);
				}
				
				$default = 0;
				
				//get related workflow order data
				$workflow_flitter = array(
					'status_id' => $this->data['result']['status'],
					'workflow_id' => 1,
				);
						
				$type='';
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						
				//print_r($requestor_role);exit;
				//$workflow_order['role_id'] == 'requestor'
				if($workflow_order['role_id'] == 'requestor'){
							
				//requestor edit
				if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
				$type = 'edit';	
								
				//user role = current status role
				}else if($role_id == $requestor_role){
					$type = $workflow_order['action'];
				}
							
				//$workflow_order['role_id'] != 'requestor'						
				}else{
							
					//user role = current status role
					if ($workflow_order['role_id'] == $role_id){
						$type = $workflow_order['action'];	
							
					//requestor edit															
					}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
						$type = 'edit';	
					}
						
				}
						
				//data form database
				$workflow = $this->Workflow_model->get(1);
				$status = json_decode($workflow['status_json'],true);
						
				$this->data['workflow_title'] = $workflow['title'];
					
				$this->data['btn_type'] = $type;
				//print_r($workflow_order);exit;

					switch($type){
						case 'edit':
							$this->data['head_title'] = 'Edit '.$workflow['title'].' Request';
						break;
						case 'approval':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
						break;
						case 'approval_update':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'update':
							$this->data['head_title'] = 'Update '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'send_email':
							$this->data['head_title'] = 'Waiting to Send '.$workflow['title'].' Request';
						break;
						case 'sendmail_update':
							$this->data['head_title'] = 'Waiting to Send/Edit '.$workflow['title'].' Request';
						break;
						case 'upload':
						case 'upload_update':
							$this->data['head_title'] = 'Upload Confirm File '.$workflow['title'].' Request';
						break;
						case 'complete':
							$this->data['head_title'] = $workflow['title'].' Request';
							$this->data['last_action'] = $workflow_order['action_before_complete'];
						break;
						default:
							$this->data['head_title'] = $workflow['title'].' Request';
					}
					
				foreach(json_decode($workflow['status_json'],true) as $k => $v){
						
					if($v == 'Completed'){
						$this->data['completed_status_id'] = $k;
						break;
					}
				}

				
			} else {
				
				$this->data['mode'] = 'Add';
				$this->data['head_title'] = 'New Order';	

				//get related workflow order data
					$workflow_flitter = array(
						'role_id' => $role_id,
						'status_id' => -1,
						'workflow_id' => 1,
					);
					
					//data form database
					$workflow = $this->Workflow_model->get(1);
					$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
					
					//get view bottom layout
					$type = '';
					if(!empty($workflow_order)){
						$type = $workflow_order['action'];
						if($workflow_order['action'] == 'create'){
							$this->data['head_title'] = 'Create '.$workflow['title'].' Request';
						}else{
							$this->data['head_title'] = $workflow['title'].' Request';
						}
					}else{
						$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					$this->data['btn_type'] = $type;
					
			}		
			
			//all product 
			$products = $this->Products_model->get();
			//sum product by id
			foreach($products as $k => $v){
				$products[$k]['qty'] = $this->Product_log_model->sum_by_id($v['id']);
			}
			
			$this->data['product'] = json_encode($products);
			
			//get assign customer
			$company_list = $this->Customers_model->getAll();
			$assign_customer = $this->data['staff_info']['assign_customer'];
			
			if(strpos($assign_customer,',')){
				$assign_customer = explode(",",$assign_customer);
			}else{
				$assign_customer = array($assign_customer);
			}
			
			//只有是sales 的時候才給予指定customer
			if($this->data['userdata']['role_id'] == 3){
			
			$new_company_list = array();
			foreach($company_list as $k => $v){
				
				foreach($assign_customer as $k2 => $v2){
					if($v2 == $v['id']){
						$new_company_list[] = $v;
					}
				}
				
			}
			
			}else{
				$new_company_list = $company_list;
			}
			
			$this->data['company_list'] = json_encode($new_company_list);
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);	
			
      }
	  
	  public function submit(){
		  
		  
		  
		  $now = date("Y-m-d H:i:s");
		  $mode = $this->input->post("mode", true);
		  $id = $this->input->post("id", true);
		  
		  $role_id = $this->data['userdata']['role_id'];

		  //form data
		  $requestor_id = $this->data['staff_info']['id'];
		  $order_date = date('d/m/Y');
		  $customer_id = $this->data['staff_info']['customer_id'];
		  $customer = $this->Customers_model->get($customer_id);
		  $customer_name = $customer['company_name'];
		  $total = $this->input->post("total", true);
		  $gst_amount = $this->input->post("gst_amount", true);
		  $alias = $this->input->post("alias_name", true);
		  $address = $this->input->post("address", true);
		  $postal = $this->input->post("postal", true);
		  $country = $this->input->post("country", true);

		  //get value approve or not approve
		  $confirm = $this->input->post("confirm", true);
		  $correct_check = $this->input->post("check_correct", true);
		  
		  $next = 0;
		  $last = 0;
		  $status_change = 0;
		  
		  //print_r($_POST);exit;
		  
		  if(!empty($order_date)) {
			  $tmp = explode("/", $order_date);
			  $order_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
		  }
		  
 		  //job
		  $jobArray = array(
			'parent_id' =>0,
			'user_id' =>$this->data['userdata']['id'],
			'customer_id' =>isset($customer_id)?$customer_id:'',
			'customer_name' =>isset($customer_name)?$customer_name:'',
			'type' => 'Order',
			'type_path' =>'sales/order_edit/',
	 	  );
 
 		  //main
		  $array = array(
		  	'requestor_id'		=> $requestor_id,
		  	'order_date'		=> $order_date,
			'customer_id'		=> isset($customer_id)?$customer_id:'',
			'customer_name'		=> isset($customer_name)?$customer_name:'',
			'total_price'		=> $total,
			'customer_create'	=> 1,
			'gst_amount'		=> $gst_amount,
			'alias'				=> $alias,
			'address'			=> $address,
			'postal'			=> $postal,
			'country'			=> $country,
		  );
		  
		  //print_r($array);exit;
		  
		  //mail
		  $mail_array = array(
		  'creater_name' =>$this->data['userdata']['name'],
		  );
		  
		  
		  $tender_id = "";
		  //Add
		  if($mode == 'Add'){
				
				//get related workflow order data
				$workflow_flitter = array(
					//'role_id' => $role_id,
					'status_id' => -1,
					'workflow_id' => 1,
				);
				
				//data form database
				$workflow = $this->Workflow_model->get(1);
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter,$role_id);
				$status = json_decode($workflow['status_json'],true);
				
				if(!empty($workflow_order)){
				
				//ad_hoc
				if($workflow_order['ad_hoc'] == 1){
					$array['ad_hoc'] = 1;
				}
				
				//purchaser
				$is_adhoc = false;
				if(in_array(10,$role_id)){
					$is_adhoc = true;
				}
				
				//next status
				$next_status = $workflow_order['next_status_id'];
				$next_status_text = $status[$workflow_order['next_status_id']];
				
			  	//if need_det = 1
				if($workflow_order['need_det'] == 1){
					
					$formula = json_decode($workflow_order['formula'],true);
					
					//check if product or service price over value
					foreach($formula as $k => $v){
					
						$price = $this->input->post($v['target_colum'], true);
						$det_temp = $price." ".$v['logic']." ".$v['value'];
						$check = $this->parse_boolean($det_temp);
						
						//check price over value
						if($check){
							$next_status = $v['next_status_id'];
							$next_status_text = $status[$v['next_status_id']];
							break;
						}
						
					}
					
				}
				
				//get next status workflow order data
				$next_workflow_flitter = array(
					'status_id' => $next_status,
					'workflow_id' => 1,
				);
				
				//next status data
				$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
				$awating_person = $next_status_data['role_id'];
				
				if($awating_person == 'requestor'){
					$awating_person = $this->input->post("requestor_id", true);
				}
				
				//main
				$array['awaiting_table']  = $awating_person;
				$array['status'] = $next_status;
				//main
				$jobArray['awaiting_role_id'] = $awating_person.',';
				$jobArray['status_id'] = $next_status;
				$jobArray['status_text'] = $next_status_text; 	
				  
				//insert main data
				$array['created_date'] = $now;
				$array['modified_date'] = $now;
				$array['create_user_id'] = $this->data['userdata']['id'];
				$array['lastupdate_user_id'] = $this->data['userdata']['id'];

				//新增資料
				$data_id = $this->Order_model->insert($array);				
				$p_id = $data_id;
					
				//更新serial
				$data_serial = $this->Order_model->zerofill($data_id, 5, $is_adhoc);
					
				//insert job
				$jobArray['serial'] = $data_serial;
				$jobArray['order_no'] = $data_serial;
				$jobArray['type_id'] = $data_id;
					
				$jobArray['created_date'] = $now;
				$jobArray['modified_date'] = $now;
				
				/*	
				//if next action == complete,create job auto complete
				if($next_status_data['action'] == 'complete'){
					$jobArray['is_completed'] = 1;
				}
				*/
					
				$job_id = $this->Job_model->insert($jobArray);
				  
				//after insert job,update job id,qr serial....
				$array =array(
					'job_id'	=> $job_id,
					'order_no'  => $data_serial,
					'latest_job_id'	=> $job_id,
				);
				$this->Order_model->update($data_id, $array);
					
				//mail
				$related_role = $this->Job_model->getRelatedRoleID($job_id);
				$mail_array['related_role'] = $related_role;		
				$mail_array['title'] = $workflow['title']." ".$data_serial." ".$next_status_text; 
				  	
				}
				
		  //Edit	  			  
		  } else {
			  	
			  	$pre_data = $this->Order_model->get($id);
			 	$requestor_employee = $this->Employee_model->get($pre_data['requestor_id']);
			  	$requestor_role = $requestor_employee['role_id'];
			  	$pre_status = $pre_data['status'];
			  
			  	$jobArray['serial'] =$pre_data['order_no'];
				$jobArray['order_no'] = $pre_data['order_no'];
			  	$jobArray['type_id'] =$pre_data['id'];
			  	$jobArray['parent_id'] = $pre_data['latest_job_id'];
				
				//item parent id
				$p_id = $id;
				
			    //can edit status
			    $edit_status = $this->Workflow_order_model->get_status_edit(1);
				
			  
		//requestor edit
		if(in_array($pre_status, $edit_status) && in_array($requestor_role,$role_id)){

				//get related workflow order data
				$workflow_flitter = array(
					//'role_id' => $role_id,
					'status_id' => -1,
					'workflow_id' => 1,
				);
						
				//data form database
				$workflow = $this->Workflow_model->get(1);
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter,$role_id);
				$status = json_decode($workflow['status_json'],true);
				
					
				if(!empty($workflow_order)){
					print_r($workflow_order);exit;
					//next status
					$next_status = $workflow_order['next_status_id'];
					$next_status_text = $status[$workflow_order['next_status_id']];
						
					//if need_det = 1
					if($workflow_order['need_det'] == 1){
							
						$formula = json_decode($workflow_order['formula'],true);
							
						//check if product or service price over value
						foreach($formula as $k => $v){
							
							$price = $this->input->post($v['target_colum'], true);
							$det_temp = $price." ".$v['logic']." ".$v['value'];
							$check = $this->parse_boolean($det_temp);
								
							//check price over value
							if($check){
								$next_status = $v['next_status_id'];
								$next_status_text = $status[$v['next_status_id']];
								break;
							}
								
						}
							
					}
						
					//get next status workflow order data
					$next_workflow_flitter = array(
						'status_id' => $next_status,
						'workflow_id' => 1,
					);
						
					//next status data
					$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
					$awating_person = $next_status_data['role_id'];
						
					//if awating person equal requestor ,asign requestor for it
					if($awating_person == 'requestor'){
						$awating_person = $requestor_role;
					}
						
					//main
					$array['awaiting_table']  = $awating_person;
					$array['status'] = $next_status;
						
					//job
					$jobArray['awaiting_role_id'] = $awating_person.',';
					$jobArray['status_id'] = $next_status;
					$jobArray['status_text'] = $next_status_text;
						
						
				}
						
				  
		}else{
				  		
												
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $pre_status,
							'workflow_id' => 1,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if(in_array($requestor_role,$role_id) && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if(in_array($requestor_role,$role_id)){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if (in_array($workflow_order['role_id'],$role_id)){
								$action = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(1);
						$status = json_decode($workflow['status_json'],true);
						
						//print_r($workflow_order);exit;
						if(isset($action)){
						
						//action
						switch ($action) {
							case 'approval':
							
								//clear data infront
								$array = array();
							
								//approved
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];

										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'update':
								
								//next status
								$next_status = $workflow_order['next_status_id'];
								$next_status_text = $status[$workflow_order['next_status_id']];
								
								$array = array(
								);
								
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
								$next = 1;
								
								break;
							case 'approval_update':
								
								//clear data infront
								$array = array();
								
								if($confirm == 1){
									
									if($correct_check == 'on'){
										
										//next status
										$next_status = $workflow_order['next_status_id'];
										$next_status_text = $status[$workflow_order['next_status_id']];
										
										$target_colum = json_decode($workflow_order['target_colum'],true);
										
										//check update colum single or multiple
										$array = array();
										foreach($target_colum as $k => $v){
										
											if($v['type'] == 'single'){
												
												if($v['data_type'] == 'string'){
													
												$array[$v['name']] = $this->input->post($v['name'],true);
												
												}else if($v['data_type'] == 'date'){
													
													$date = $this->input->post($v['name'], true);
													if(!empty($date)) {
													  $tmp = explode("/", $date);
													  $date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
													  $array[$v['name']] = $date;
													}
													
												}
											
											}else{
												
											}
											
										}
										$next = 1;
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
									}
									
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
							case 'upload':
								
								//clear data infront
								$array = array();
								
								if($confirm == 1){
								
								if(isset($_FILES['confirm_file'])){
				
									if ( $_FILES['confirm_file']['error'] == 0 && $_FILES['confirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['confirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['confirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'confirm_file'	=> $save_path,
											);	
											
											//next status
											$next_status = $workflow_order['next_status_id'];
											$next_status_text = $status[$workflow_order['next_status_id']];	
											$next = 1;
											//check last job
											if($workflow_order['next_status_text'] == 'Completed'){
												$last = 1;
											}		
											//echo $workflow_order['next_status_text'];exit;										
									}	
									
								}
								
								//no approve
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
							
							case 'upload_update':

								if($confirm == 1){
								
								if(isset($_FILES['confirm_file'])){
				
									if ( $_FILES['confirm_file']['error'] == 0 && $_FILES['confirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['confirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['confirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array['confirm_file']= $save_path;
											
											//next status
											$next_status = $workflow_order['next_status_id'];
											$next_status_text = $status[$workflow_order['next_status_id']];	
											$next = 1;
											//check last job
											if($workflow_order['next_status_text'] == 'Completed'){
												$last = 1;
											}		
											//echo $workflow_order['next_status_text'];exit;										
									}	
									
								}
								
								//no approve
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
									
							case 'complete':
							
								//clear data infront
								$array = array();
							
								if(isset($_FILES['confirm_file'])){
				
									if ( $_FILES['confirm_file']['error'] == 0 && $_FILES['confirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['confirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['confirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'confirm_file'	=> $save_path,
											);	
										
									}	
								}
								
								break;	
								
						}
						
						//print_r($workflow_order);exit;
						//if need_det = 1
						
						if($workflow_order['need_det'] == 1){

							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$value = $this->input->post($v['target_colum'], true);
								//string or number
								if(is_numeric($v['value'])){
									$det_temp = $value." ".$v['logic']." ".$v['value'];
								}else{
									
									if($v['value'] == 'original'){
										$string = $tender_data['term_condition'];
									}else{
										$string = $v['value'];
									}
									
									$det_temp = "'".md5(trim($value))."' ".$v['logic']." '".md5(trim($string))."'";
								}
								
								
								$check = $this->parse_boolean($det_temp);
								
								//check
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
							}
							
						}

						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 1,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
						}
						
						}
						
				  
			  }
			  
			  if(isset($next_status)){
				//main
				$array['awaiting_table']  = $awating_person;
				$array['status'] = $next_status;
				//job
				$jobArray['awaiting_role_id'] = $awating_person.',';
				$jobArray['status_id'] = $next_status;
				$jobArray['status_text'] = $next_status_text;
			  }

			  
			  $array['lastupdate_user_id'] = $this->data['userdata']['id'];
			  $array['modified_date'] = $now;
			  
			  
			    if($next == 1){
				  $lastjob_update = array(
				  	'is_completed' => $workflow_order['action']!='complete'?1:0,
					'display' => 0,
				  );
				  $this->Job_model->update($jobArray['parent_id'], $lastjob_update);
				  if($last == 0){
				  $new_job_id = $this->Job_model->insert($jobArray);
				  $array['latest_job_id'] = $new_job_id;
				  }
			  	}
				
			  $this->Order_model->update($id, $array);
			  
			  //no approve 加回庫存
			  if(isset($array['status']) && $array['status'] == 0){
				  
				  //刪除order增加商品庫存
				  $item = $this->Order_item_model->getRelatedItem($id);
									
				  foreach($item as $k => $v){
					  
					  $log_data = array(
						'product_id' => $v['product_id'],
						'qty' => $v['quantity'],
						'reason' => 'no approve order',
						'created_date' => date('Y-m-d H:i:s'),
					  );
										
					  $this->Product_log_model->insert($log_data);
					  //echo $this->db->last_query();exit;
				  
				  }
			  }
			  
			  
			 
			 //mail
			 if(isset($new_job_id)){
				$related_role = $this->Job_model->getRelatedRoleID($new_job_id);
				$mail_array['related_role'] = array($related_role);		
				$mail_array['title'] = $workflow['title']." ".$pre_data['order_no']." ".$next_status_text;
			 }
			  
			    
		  }
		  
		  $item_type = 0;
		  if($mode == 'Add'){
				
				$item_type = 1;
			
		  }else if($mode == 'Edit'){
				if($pre_status < 2 && $pre_status != 0){
					$item_type = 1;
				}	
		  }
		  
		  //print_r($_POST);exit;
		  
		  if($item_type == 1 && isset($_POST['p_id'])){
		  //針對底下的ITEM做處理
		  
		  foreach($_POST['p_id'] as $k => $v){
			  
			  if($v != ''){
					
					//print_r($tmp);exit;
					$data = array(
						'order_id' => $p_id,
						'product_id' => $v,
						'model_name' => isset($_POST['model_name'][$k])?$_POST['model_name'][$k]:'',
						'quantity' => isset($_POST['qty'][$k])?$_POST['qty'][$k]:'',
						'quantity_order' => isset($_POST['qty'][$k])?$_POST['qty'][$k]:'',
						'price' => isset($_POST['price'][$k])?$_POST['price'][$k]:'',
						'created_date' => $now,
					);
					
					
					//判斷是不是promo item
					$product_tmp = $this->Products_model->get($v);
					if($product_tmp['promo_price']== $data['price']){
						$data['promo'] = 1;
					}
					
					
					
					//至少要有product_id才能insert
						if(!empty($data['product_id']) && !empty($data['model_name'])) {

							$this->Order_item_model->insert($data);
							
							
						}

			   }
			}
		  
			
			//sent mail agora
			
		  if(isset($workflow_order)){
				
				$this->load->library('email');
				
				//send mail to sale and customer
				if($workflow_order['action'] == 'create'){
					
					$role = array(3);
					
					$all_employee = $this->Employee_model->get_where(array('is_deleted'=>0));
					
					$related_role_data = array();
					foreach($all_employee as $k => $v){
						if(strpos($v['role_id'],',')){
							$tmp_role = explode(',',$v['role_id']);	
						}else{
							$tmp_role = array($v['role_id']);	
						}
						
						if(in_array(3,$tmp_role)){
							$related_role_data[] = $v;
						}
					}
					
					$sitename = $this->Settings_model->get_settings(1);
					$default_email = $this->Settings_model->get_settings(6);
					
					//print_r($related_role_data);
					if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
								
						//Send Notification sales			
						foreach($related_role_data as $k => $v){
							
							$this->email->clear();
									
							$this->email->from($default_email['value'], $sitename['value']);
							$this->email->to($v['email']); 
									
							$this->email->subject('Agora '.$workflow['title'].': '.$jobArray['serial']);
							$this->email->message('Create order '.$jobArray['serial']);
							$this->email->set_newline("\r\n");
							$this->email->send();
							//echo $this->email->print_debugger();
						
						}
						
						/*
						//customer
						$customer_email = $customer['primary_contact_email'];
						//print_r($customer_email);exit;
						$this->email->clear();
									
						$this->email->from($default_email['value'], $sitename['value']);
						$this->email->to($customer_email); 
									
						$this->email->subject('Agora '.$workflow['title'].': '.$jobArray['serial']);
						$this->email->message('Create order '.$jobArray['serial']);
						$this->email->set_newline("\r\n");	
						$this->email->send();
						*/
					}
					
				}else if($workflow_order['action'] == 'approval_update_s' && $confirm == 1){
					
					$role = array(4);
					
					$related_role_data = $this->Employee_model->get_related_role($role);
					$sitename = $this->Settings_model->get_settings(1);
					$default_email = $this->Settings_model->get_settings(6);
					
					
					if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
								
						//Send Notification ground staff	
						if(!empty($related_role_data)){		
						foreach($related_role_data as $k => $v){
							
							$this->email->clear();
									
							$this->email->from($default_email['value'], $sitename['value']);
							$this->email->to($v['email']); 
									
							$this->email->subject('Agora '.$workflow['title'].': '.$jobArray['serial']);
							$this->email->message('Packing List'.$jobArray['serial']);
							$this->email->set_newline("\r\n");
							$this->email->send();
						
						}
						}
						
						/*
						//customer acknowledge
						$customer_email = $customer['primary_contact_email'];
						
						$this->email->clear();
									
						$this->email->from($default_email['value'], $sitename['value']);
						$this->email->to($customer_email); 
									
						$this->email->subject('Agora '.$workflow['title'].': '.$jobArray['serial']);
						$this->email->message('Acknowledge order '.$jobArray['serial']);
						$this->email->set_newline("\r\n");		
						$this->email->send();
						*/
					}
					
				}
		  }
			
		  }
		  
		  //del cart
		  delete_cookie('Cart');
				  		 		  		  
		  redirect(base_url('en/agora/customer/pending_order/ALL/1/1'));		  
		  
	  }
	  
	  public function del(){
		  
		  $id = $_POST['p_id'];
		  //get cookie
		  $cart = $this->input->cookie('Cart', TRUE);
		  $cart = json_decode($cart,true);
		
		  //del item
		  unset($cart[$id]);
		  
		  $cookie = array(
			'name'   => 'Cart',
			'value'  => json_encode($cart),
			'expire' => '3600',
		  );
		  
		  $this->input->set_cookie($cookie);
		  
		  $tmp_array = array();
		  foreach($cart as $k => $v){
			  $array = array(
			  	'id'			=>	$k,
			  	'name' 			=>	$v['name'],
				'qty' 			=>	$v['qty'],
				'price' 		=>	$v['price'],
				'model_name'	=>	$v['model_name'],
			  );
			  $tmp_array[] = $array;
		  }
		  
		  echo json_encode($tmp_array);exit;
		  
	  }
	  
	  public function update(){
		  
		  $data = $_POST['data'];
		  $data = json_decode($data,true);
		  
		  //get cookie
		  $cart = $this->input->cookie('Cart', TRUE);
		  $cart = json_decode($cart,true);
		
		  //update qty
		  foreach($data as $k => $v){
			  if(!empty($v)){
			  	$cart[$v['id']]['qty'] = $v['qty'];
			  }
		  }
		  
		  $cookie = array(
			'name'   => 'Cart',
			'value'  => json_encode($cart),
			'expire' => '3600',
		  );
		  
		  $this->input->set_cookie($cookie);
		  
		  $tmp_array = array();
		  foreach($cart as $k => $v){
			  $array = array(
			  	'id'			=>	$k,
			  	'name' 			=>	$v['name'],
				'qty' 			=>	$v['qty'],
				'price' 		=>	$v['price'],
				'model_name'	=>	$v['model_name'],
			  );
			  $tmp_array[] = $array;
		  }
		  
		  echo json_encode($tmp_array);exit;
		  
	  }
	  
	  public function ajax_getQuotation(){
		  
		  	$keyword = $_POST['keyword'];
			$limit = $_POST['limit'];
			$page = $_POST['page'];
			
			$start = ($page-1) * $limit;
			//我們只取已經confirm的quotation
		  	$record_count = $this->Quotation_model->ajax_record_count($keyword, array(
				'status'	=> 6
			));
		  	$data = $this->Quotation_model->ajax_quotation($keyword, $limit, $start, array(
				'status'	=> 6
			));
		  	$paging = $this->get_paging($limit,10,$record_count,$page);
			
			$quotation_status = $this->Quotation_model->quotation_status_list();
			
			//顯示Status為狀態文字, 不是數字
			if(!empty($data)) {
				foreach($data as $k=>$v) {
					$data[$k]['status'] = $quotation_status[$v['status']];
				}
			}
		  	
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					'paging'	=> $paging,
			);
			
			echo json_encode($temp);	
			exit;
	  }	  
	  
	  public function ajax_getQuotationData(){
		  
		  	$id = $_POST['id'];
		  	$data = $this->Quotation_model->get($id);
			$employee = $this->Employee_model->get($data['create_user_id']);
			$department = $this->Department_model->get($employee['department_id']);
			$data['employee_no'] = $employee['employee_no'];
			$data['department'] = $department['name'];
			
			
			//get quotation related item
			$related_quotation_item = $this->Quotation_item_model->getRelatedItem($id);
			
			foreach($related_quotation_item as $k => $v){
				$product_data = $this->Products_model->get($v['product_id']);
				$supplier_data = $this->Suppliers_model->get($product_data['supplier']);
				$related_quotation_item[$k]['model_no'] = $product_data['model_no'];
				$related_quotation_item[$k]['part_no'] = $product_data['part_no'];
				$related_quotation_item[$k]['supplier_id'] = $product_data['supplier'];
				$related_quotation_item[$k]['cost_price'] = $product_data['cost_price'];
				$related_quotation_item[$k]['supplier_name'] = $supplier_data['company_name'];
				$related_quotation_item[$k]['supplier_pic'] = $supplier_data['contact_person'];
			}
			

		

	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					//'product'   => $related_quotation_item,
					'product' => $related_quotation_item ,
			);
			
			
			
			echo json_encode($temp);	
			exit;
	  }	 
	  
	  public function get_paging($item_per_page,$pagenum,$total_item,$page)
	  {
	
		$start = (int)(($page-1)/$pagenum)*$pagenum+1;
		$end = $start+$pagenum-1;
		$next = $page+1;
		$pre = $page-1;
		
		$total_page = ceil( $total_item / $item_per_page );
                $paging = '';
		if($total_item > $item_per_page){
                    $paging .= '<ul class="pagination">';

                    if($page > 1){
                            $paging .= '<li><a href="javascript:changePage(1)">&laquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$pre.')">&lsaquo;</li>';
                    }

                    if($total_page <= $pagenum){

                            for($i=$start;$i<=$total_page;$i++){
                                    if($i == $page){

                                            $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                    }else{

                                            $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                    }
                            }
                    }else{
                            if($page > 5){
                                    $end = $page+5;
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    $start = $end - ($pagenum - 1);

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }else{
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }	
                    }

                    if($page < $total_page){
                            $paging .= '<li><a href="javascript:changePage('.$next.')">&rsaquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$total_page.')">&raquo;</a></li>';
                    }

                    $paging .= '</ul>';
                }
		
		return $paging;
	  }
	  
	  
	  public function ajax_getModel(){
		  
		  	$id = $_POST['id'];
			$type = $this->input->post("type", true);
			
			//Products
			if($type == 1) {

		  		$data = $this->Products_model->ajax_model($id, $type);
			
			//Service
			} else if ($type == 2) {
				
				$data = $this->Products_model->getService();
				
			}
		  	
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					'type'		=> $type,
			);
			
			echo json_encode($temp);	
			exit;
	  }	   
	  
	  public function ajax_estShipping(){
		  
		  	$companyId = $_POST['companyId'];	
		  	$shipperId = $_POST['shipperId'];		
			$groupItem = json_decode($_POST['groupItem'],true);

			$shippingItem = $this->Shipping_model->get($shipperId);

			if( strpos($shippingItem['country'], ",") !== FALSE ) {
			
				$shippingCountry = explode(",",$shippingItem['country']);
			
			} else {
				
				$shippingCountry = array();
				$shippingCountry[] = $shippingItem['country'];
					
			}
			
			$shippingType = $shippingItem['shipping_type'];
			$shippingRate = json_decode($shippingItem['cost_json'],true);
			
			$estShipping = 0;
			
			foreach($groupItem as $k => $v){
				
				if($v['item_type'] == 'product'){
					
				$customer = $this->Customers_model->get($companyId);
				
				$countryCheck = 0;
				foreach($shippingCountry as $x){
					if($customer['country'] == $x){
						$countryCheck = 1;
						break;
					}
				}
				
				if($countryCheck == 1){
					
					$product = $this->Products_model->get($v['modelNo']);
					$actualWeight = $product['weight'];
					$dimensionWeight = ($product['length']*$product['breadth']*$product['height'])/6000; 
					//抓取weight
					if($actualWeight > $dimensionWeight){
						$weight = $actualWeight;
					}else{
						$weight = $dimensionWeight;
					}
					if($v['quantity']=='select'){
						$weight = 0;
					}else{
						$weight = $weight*$v['quantity'];
					}
					
					$min_diff = 999;
					$target = 0;
					$count_shippingRate = count($shippingRate);
					
					//針對每個shippingrate去看最小差距
					foreach($shippingRate as $x => $y){
						$diff_weight = abs($weight-$y['weight']);
						if($diff_weight < $min_diff){
							$min_diff = $diff_weight; //最小的差距
							$target = $x; //抓最小差距的 array(key)
						}
					}
					
					//拿搜尋的weight減掉最小差距的weight
					$weightL = $weight - $shippingRate[$target]['weight'];
					
					//如果是大於零, 代表我們需要看他的下一層是否存在
					if($weightL > 0){

						//他的下一層有存在
						if(isset($shippingRate[($target+1)]['weight'])) {
							$cost = $shippingRate[($target+1)]['cost'];
						//不存在就取當下的這層	
						} else {
							$cost = $shippingRate[$target]['cost'];	
						}
					//如果等於零, 代表剛剛好是這層							
					}else{
						$cost = $shippingRate[$target]['cost'];
					}
					
					if($cost > $estShipping){
						$estShipping = $cost;
					}
				//管理者根本沒有設定這個國家的運費	
				}else{
					$estShipping = 0;
				}
				
				}
				
			}
			
		  	
	  		$temp = array(
					'status'	=> 'OK',
					'estShipping'		=> $estShipping,
			);
			
			echo json_encode($temp);	
			exit;
	  }	   
	  
	  
	  public function ajax_getProductDetail(){
		  	$id = $_POST['id'];
			
			$product = $this->Products_model->get($id);
			
			$temp = array(
					'status'	=> 'OK',
					'product'		=> $product,
			);
			
			echo json_encode($temp);	
			exit;
			
			
	  }
	  
	  private function PDF_generation($id, $mode='I') {
		  
		  
		  		$tender_data = $this->Tender_model->get($id);
					
		  		$receivedDate = $tender_data['received_date'];
				$tmp = explode('-',$receivedDate);
				$receivedDate = $tmp[2]."/".$tmp[1]."/".$tmp[0];
				
				$requestDate = $tender_data['request_date'];
				$tmp = explode('-',$requestDate);
				$requestDate = $tmp[2]."/".$tmp[1]."/".$tmp[0];
				
				$requestor = $this->Employee_model->get($tender_data['requestor_id']);
				$tender_data['requestor_name'] = $requestor['full_name'];
				$requestor_department = $this->Department_model->get($requestor['department_id']);
				$tender_data['requestor_department_name'] = $requestor_department['name'];
				
				$product = $this->Tender_item_model->getRelatedItem($id);
				
				foreach($product as $k => $v){
					$product_data = $this->Products_model->get($v['product_id']);
			  		$supplier_tmp = $this->Suppliers_model->get($product_data['supplier']);
			 		$product[$k]['supplier_name'] = $supplier_tmp['company_name'];
				}

		 		$customer = $this->Customers_model->get($tender_data['customer_id']);
		  		$shipper_data = $this->Shipping_model->get($tender_data['shipper']);
		  	  
		  
		  $item_group = '';
		  foreach($product as $k => $v){
			  $item_group .='<tr><td>'.($k+1).'</td><td>'.$v['supplier_name'].'</td><td>'.$v['model_no'].'</td><td>'.$v['quantity'].'</td><td>'.$v['unit_price'].'</td><td>'.$v['extended_price'].'</td></tr>';
		  }
		  
		  
		  $this->load->library("Pdf");		
          $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		  
		  // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Anexus');
            $pdf->SetTitle("Tender");
            $pdf->SetSubject("");
            $pdf->SetKeywords("");

            // remove default header/footer
            $pdf->setPrintHeader(false);			
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(10, 10, 10);

            // set auto page breaks
            //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
            }

            // ---------------------------------------------------------

            // set font
            $pdf->SetFont('times', '', 10);

            

            // set font
            $pdf->SetFont('times', '', 10);
			
			// add a page
       		$pdf->AddPage();
			
			
			$html = '			
			<h1>Tender</h1>
			
			<table width="100%" cellpadding="5">
				<tr>
					<td>
						<table width="100%"  border="1" cellpadding="5">
			
							<tr>
								<th colspan="2"><h3>Tender Information</h3></th>
							</tr>
							<tr>
								<th colspan="2"><h4>QR Serial No.</h4></th>
							</tr>
							<tr>
								<td colspan="2">'.$tender_data['qr_serial'].'</td>
							</tr>
							<tr>
								<th colspan="2"><h4>Date Received</h4></th>
							</tr>
							<tr>
								<td colspan="2">'.$receivedDate.'</td>
							</tr>
							<tr>
								<th colspan="2"><h4>Payment Terms</h4></th>
							</tr>
							<tr>
								<td>Milestone</td>
								<td>Percentage</td>
							</tr>
							<tr>
								<td>Sign-Off</td>
								<td>'.$tender_data['tender_payment_sign_off'].'</td>
							</tr>
							<tr>
								<td>Before Delivery</td>
								<td>'.$tender_data['tender_payment_before_delivery'].'</td>
							</tr>
							<tr>
								<td>After Delivery</td>
								<td>'.$tender_data['tender_payment_after_delivery'].'</td>
							</tr>
							<tr>
								<td>Completed</td>
								<td>'.$tender_data['tender_payment_completed'].'</td>
							</tr>
							<tr>
								<td>Warranty</td>
								<td>'.$tender_data['tender_payment_warrantly'].'</td>
							</tr>
							<tr>
								<th colspan="2"><h4>Legal Terms</h4></th>
							</tr>
							<tr>
								<td colspan="2">'.$tender_data['tender_legal_term'].'</td>
							</tr>
	
						</table>
					</td>
					
					<td>
						<table width="100%"  border="1" cellpadding="5">
			
							<tr>
								<th><h3>Customer Information</h3></th>
							</tr>
							<tr>
								<th><h4>Company Name</h4></th>
							</tr>
							<tr>
								<td>'.$customer['company_name'].'</td>
							</tr>
							<tr>
								<th><h4>Attention</h4></th>
							</tr>
							<tr>
								<td>'.$customer['primary_attention_to'].'</td>
							</tr>
							<tr>
								<th><h4>Address</h4></th>
							</tr>
							<tr>
								<td>'.$customer['address'].'</td>
							</tr>
							<tr>
								<th><h4>Postal Code</h4></th>
							</tr>
							<tr>
								<td>'.$customer['postal'].'</td>
							</tr>
							<tr>
								<th><h4>Email</h4></th>
							</tr>
							<tr>
								<td>'.$customer['email'].'</td>
							</tr>
							<tr>
								<th><h4>Contact</h4></th>
							</tr>
							<tr>
								<td>'.$customer['contact_info'].'</td>
							</tr>
		
						</table>
						
					</td>
				</tr>
				
				<tr>
					<td>
						
						<table width="100%"  border="1" cellpadding="5">
			
							<tr>
								<th><h3>Shipper Information</h3></th>
							</tr>
							<tr>
								<th><h4>Shipper</h4></th>
							</tr>
							<tr>
								<td>'.$shipper_data['shipper_name'].'</td>
							</tr>
							<tr>
								<th><h4>Est. Shipping</h4></th>
							</tr>
							<tr>
								<td>'.$tender_data['est_shipping'].'</td>
							</tr>
		
						</table>
					</td>
					
					<td>
						<table width="100%"  border="1" cellpadding="5">
			
							<tr>
								<th><h3>Staff Information</h3></th>
							</tr>
							<tr>
								<th><h4>Requestor Name</h4></th>
							</tr>
							<tr>
								<td>'.$tender_data['requestor_name'].'</td>
							</tr>
							<tr>
								<th><h4>Date of Request</h4></th>
							</tr>
							<tr>
								<td>'.$requestDate.'</td>
							</tr>
							<tr>
								<th><h4>Department</h4></th>
							</tr>
							<tr>
								<td>'.$tender_data['requestor_department_name'].'</td>
							</tr>
							<tr>
								<th><h4>Requestor To</h4></th>
							</tr>
							<tr>
								<td>'.$tender_data['requestor_to'].'</td>
							</tr>
	
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						
						<table width="100%" border="1" cellpadding="5">
				
							<tr>
								<th colspan="6"><h3>Items</h3></th>
							</tr>
							<tr>
								<td>#</td>
								<td>Principal/Supplier</td>
								<td>Model No</td>
								<td>Quantity</td>
								<td>Unit Price</td>
								<td>Extended Price</td>
							</tr>'.$item_group.'
						</table>
						
					</td>
				</tr>
				<tr>
					<td colspan="2">
						
						<table width="100%" border="1" cellpadding="5">
				
							<tr>
								<th><h3>Remarks</h3></th>
							</tr>
							<tr>
								<td>'.$tender_data['qr_remarks'].'</td>
							</tr>
						</table>
						
					</td>
				</tr>

			</table>';
			
			$pdf->writeHTML($html, true, false, true, false, '');
									
			
			$file_name = date("YmdHis").rand(1000,9999);
			
			if($mode == 'I') {
				$pdf->Output($file_name.'.pdf', 'I'); 	
				return $file_name.'.pdf';
			} else if ($mode == 'f') {				
				$pdf->Output('./uploads/'.$file_name.'.pdf', 'f'); 
				return './uploads/'.$file_name.'.pdf';
			}
			
		  		  
		  
	  }
	  
	  public function export_pdf($id){		  
		  $this->PDF_generation($id, 'I');			
	  }
	  
	  public function sent_mail($id,$product_total=0,$service_total=0){
		  
		   	$filename = $this->PDF_generation($id, 'f');	 		  
		  
		    $tender_data = $this->Tender_model->get($id);
		  	$customer = $this->Customers_model->get($tender_data['customer_id']);
		  
		  	$send_to = $customer['email'];
			
			$sitename = $this->Settings_model->get_settings(1);
		  	$default_email = $this->Settings_model->get_settings(6);
			
			 //$requestor_role
			$requestor_employee = $this->Employee_model->get($tender_data['requestor_id']);
			$requestor_role = $requestor_employee['role_id'];
		  
		 	$role_id = $this->data['userdata']['role_id'];
			
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $tender_data['status'],
							'workflow_id' => 3,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(3);
						$status = json_decode($workflow['status_json'],true);
						
						if(isset($action)){

		
						if($action == 'send_email' || $action == 'sendmail_update'){
							
							//next status
							$next_status = $workflow_order['next_status_id'];
							$next_status_text = $status[$workflow_order['next_status_id']];
							$who_email = $workflow_order['who_email'];
	
						}

						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];

								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 1,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
							
							$jobArray = array(
								'parent_id' 		=>$tender_data['latest_job_id'],
								'user_id' 			=>$this->data['userdata']['id'],
								'customer_id' 		=>$tender_data['customer_id'],
								'type' 				=> 'Tender',
								'type_path' 		=>'sales/tender_edit/',
								'awaiting_role_id' 	=>$awating_person.',',
								'status_id' 		=>$next_status,
								'status_text'		=>$next_status_text,
								'serial' 			=>$tender_data['qr_serial'],
								'type_id' 			=>$tender_data['id'],
								'created_date'		=>date("Y-m-d H:i:s"),
								'modified_date'		=>date("Y-m-d H:i:s"),
							);			
					  
						    $array = array(
							'awaiting_table' => $awating_person.',',
							'status' => $next_status,
							'modified_date' => date('Y-m-d H:i:s'),
							'lastupdate_user_id' => $this->data['userdata']['id'],
						    );
							
							//print_r($jobArray);exit;

						
						  //更新上一個JOB的狀態					  
						  $this->Job_model->update($jobArray['parent_id'], array(
							'is_completed' => 1,
							'display' => 0,
						  ));
						  
						  //新增一個新的JOB
						  $new_job_id = $this->Job_model->insert($jobArray);
						  $array['latest_job_id'] = $new_job_id;
						  
						  $this->Tender_model->update($tender_data['id'], $array);
							
							
						}
						
						}
			
			
			if(isset($who_email)){
			
				$this->load->library('email');
				
				switch($who_email){
					case 'customer':
							
							//Send Notification
							if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
										
								$this->email->from($default_email['value'], $sitename['value']);
								$this->email->to($send_to); 
							
								$this->email->subject('Anexus '.$workflow['title'].': '.$tender_data['qr_serial']);
								$this->email->message($tender_data['qr_serial']);	
								$this->email->attach($filename);
								$this->email->set_newline("\r\n");
								$this->email->send();
							
							}
							
					break;	
					case 'supplier':
						
							  //count supplier
							  $supplier_count = $this->Purchase_request_item_model->get_supplier_groupBy($id);
							  
							  foreach($supplier_count as $k => $v){
									
								  $supplier = $this->Suppliers_model->get($v['supplier_id']);
								  
								  $file_name = $this->PDF_generation($id, 'f',$v['supplier_id']);	
										 
								  if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
									
									$this->load->library('email');
									$this->email->clear();	
									$this->email->from($default_email['value'], $sitename['value']);
									$this->email->to($supplier ['email']); 
									
									$this->email->subject('Anexus Purchase request '.$tender_data['qr_serial']);
									$this->email->message($tender_data['qr_serial']);	
									$this->email->attach($file_name);
									$this->email->set_newline("\r\n");
									$this->email->send();
									
								  }
							  
							  }
							
					break;
					case 'employee':
					break;
				
				}
				
			}
			
			 //mail
			 $mail_array = array(
				'creater_name' =>$this->data['userdata']['name'],
				'title' =>'Purchase '.$tender_data['qr_serial'].' has been send out',
			 );
			 
			 if(isset($new_job_id)){
			  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
			  $mail_array['related_role'] = $related_role;		
			  $mail_array['title'] = $workflow['title']." ".$tender_data['qr_serial']." ".$next_status_text;
			 }
			 echo $_SERVER['HTTP_HOST'];exit;
			 //sent Notification to related role						  			
		  	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local" && isset($mail_array['related_role'])) {
				$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
			  				  	
			  	foreach($related_role_data as $k => $v){
				  
				  	$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
					
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->set_newline("\r\n");
					$this->email->send();	
				  
		 		}
			}
			
			
			$lastpage = $this->session->userdata("lastpage");
			if(!empty($lastpage)) {
			  redirect($lastpage,'refresh');  
			} else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
			}		 			
		  
	  }
	  
	  function ajax_search(){
		  
		  $keyword = $_POST['keyword'];
		  $category = $_POST['category'];
		  
		  $result = $this->Products_model->get_where(array('product_category'=> $category),$keyword);
		  
		    //customer price list
			$customer = $this->Customers_model->get($this->data['staff_info']['customer_id']);
			$price_list = json_decode($customer['price_list'],true);
			
			foreach($result as $k => $v){
				
				foreach($price_list as $k2 => $v2){
						
					if($v['id'] == $v2['id']){
							
						$result[$k]['selling_price'] = $v2['price'];
							
					}
				}
			}
		  
		  $json = array(
		  	'status' => 'OK',
			'result' => $result,
		  );
		  
		  echo json_encode($json);
		  exit;
		  
	  }
	  
	  function addItem(){
		  
		  //get cookie
		  $data = $this->input->cookie('Cart', TRUE);
		  
		  
		  //check cookie isset,else declare array
		  if(isset($data)){
		  	$tmp = json_decode($data,true);
		  }else{
			$tmp = array();
		  }
		  
		  //insert new data
		  $tmp[$_POST['p_id']] = array(
		  	'name' 			=>	$_POST['p_name'],
			'qty' 			=>	$_POST['qty'],
			'price' 		=>	$_POST['price'],
			'model_name'	=>	$_POST['model_name'],
		  );
		  
		  $cookie = array(
			'name'   => 'Cart',
			'value'  => json_encode($tmp),
			'expire' => '3600',
		  );
		  
		  $this->input->set_cookie($cookie);
		  
		  $json = array(
		  	'status' => 'OK',
		  );
		  
		  echo json_encode($json);exit;
		  
	  }
	  
	  public function reorder(){
		  
		  $orderId = $_POST['orderId'];
		  $related_item = $this->Order_item_model->getRelatedItem_join($orderId);
		  
		  //print_r($related_item);exit;
		  //customer price list
		  $customer = $this->Customers_model->get($this->data['staff_info']['customer_id']);
		  $price_list = json_decode($customer['price_list'],true);
		  
		  //設定物品價格
		  $tmp_data = $related_item;
		  foreach($tmp_data as $k => $v){
					
				//取自定價格
				if($customer['use_default_price'] != 1){
					
					foreach($price_list as $k2 => $v2){
						
						//如果有就替代，沒有就按照預設
						if($v['product_id'] == $v2['id']){
								
							$tmp_data[$k]['selling_price'] = $v2['price'];
							
						}
					}
					
				}
					
				//如果有promo price 且小於 selling price
				if($v['promo_price'] != '0.00'){
							
					if($tmp_data[$k]['selling_price']>$v['promo_price']){
						$tmp_data[$k]['selling_price'] = $v['promo_price'];
					}
				}
					
		 }
		  
		  
		 //get cookie
		 $data = $this->input->cookie('Cart', TRUE);
		  
		 //check cookie isset,else declare array
		 if(!empty($data)){
		  	$tmp = json_decode($data,true);
			
			  //check item isset
			  //print_r($tmp);exit;
			  foreach($tmp_data as $x => $y){
			  
				  $isset_id = 0;
				  foreach($tmp as $k => $v){
					  
					  //isset id
					  if($k == $y['product_id']){
						  
						  $isset_id = $k;
						  
					  }
						
				  }
				  
				  //added to cart b4
				  if($isset_id != 0){
						
					  $ori_qty = $tmp[$isset_id]['qty'];
					  $new_qty = $y['quantity_order']+$ori_qty;
					  $tmp[$isset_id]['qty'] = $new_qty;
				
				  //new item added		 
				  }else{
					
					  //insert new data
					  $tmp[$y['product_id']] = array(
						'name' 			=>	$y['product_name'],
						'qty' 			=>	$y['quantity_order'],
						'price' 		=>	$y['selling_price'],
						'model_name'	=>	$y['model_name'],
					  );
					  
				  }
			  
			  }
			
		  }else{
			  
				$tmp = array();
				foreach($tmp_data as $x => $y){
					
					//insert new data
					$tmp[$y['product_id']] = array(
						'name' 			=>	$y['product_name'],
						'qty' 			=>	$y['quantity_order'],
						'price' 		=>	$y['selling_price'],
						'model_name'	=>	$y['model_name'],
					);
				
				}
				
				
		 }//endif

		 
		 $cookie = array(
			'name'   => 'Cart',
			'value'  => json_encode($tmp),
			'expire' => '3600',
		  );
		  
		 $this->input->set_cookie($cookie);
		 
		  
		  //回傳cart資料
		  $tmp_array = array();
		  foreach($tmp as $k => $v){
			  $array = array(
			  	'id'			=>	$k,
			  	'name' 			=>	$v['name'],
				'qty' 			=>	$v['qty'],
				'price' 		=>	$v['price'],
				'model_name'	=>	$v['model_name'],
			  );
			  $tmp_array[] = $array;
		  }
		  
		  $json = array(
		  	'status' => 'OK',
			'data'	 => $tmp_array,
		  );
		  
		  echo json_encode($json);exit;
		  
		  
	  }

}

?>