<?php

class Business_trip_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();  
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Customers_model');
			$this->load->model('Business_trip_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Employee_model');
			$this->load->model('Workflow_model');
			$this->load->model('Workflow_order_model');

			$this->data['role_list'] = $this->Role_model->getIDKeyArray('name');
			
			//get status list
			$btr_status_list = $this->Workflow_model->get(12);
			$this->data['btr_status_list'] = json_decode($btr_status_list['status_json']);
			
            $this->data['init'] = $this->function_model->page_init();
			$this->data['status_list'] = $this->Business_trip_model->status_list();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$this->data['userdata'] = $this->session->userdata("userdata");
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}          
			
			$this->data['staff_info'] = $this->Employee_model->get($this->data['userdata']['id']); 
			
			$this->data['group_name'] = "finance";  
			$this->data['model_name'] = "business_trip";  
			$this->data['common_name'] = "Business Trip";   
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}
			
			define('SUPERADMIN', 1);
			define('GENERAL_MANAGER', 2);
			define('SERVICE_MANAGER', 3);
			define('SERVICE_EXECUTIVE', 4);
		    define('FINANCE', 5);
			define('SALES_MANAGER', 7);
			define('SALES_EXECUTIVE', 8);
           
      }
   
      public function index($q="ALL", $status="ALL", $page=1) {  
          			
            $this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/anexus/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/'.$status.'/';
			if($q == 'ALL') {
				$q = "";
			}
			$this->data['q'] = $q;
			
			if($status != 'ALL') {
				$filter['status'] = $status;	
			}
			$this->data['status'] = $status;			
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Business_trip_model->record_count($filter, $q);
			
			//get particular ranged list
			$this->data['results'] = $this->Business_trip_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start);
			
			//awaiting
			if(!empty($this->data['results'])){
				foreach($this->data['results'] as $k => $v){
					if(!empty($v['awaiting_table'])){
							$this->data['results'][$k]['awaiting_table']= explode(',',$v['awaiting_table']);
					}
				}
			}
						
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->data['customer_list'] = $this->Customers_model->getIDKeyArray("company_name");
			$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
			
			$this->session->set_userdata("lastpage", $url.$page);
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  
	  public function add($id=false) {  
          	
			$role_id = $this->data['userdata']['role_id'];
			
			if($id !== false) {
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Business_trip_model->get($id);	
				$this->data['result']['awaiting_table'] = explode(',',$this->data['result']['awaiting_table']);
				
				$requestor_employee = $this->Employee_model->get($this->data['result']['create_user_id']);
			    $requestor_role = $requestor_employee['role_id'];
				
			    $default = 0;
				
				
				//////////////////////////////////////////////////////////////////////////////////
				
				   		//get related workflow order data
						$workflow_flitter = array(
							'status_id' => $this->data['result']['status'],
							'workflow_id' => 12,
						);
						
						$type='';
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$type = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$type = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
							$type = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
									$type = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(12);
						$status = json_decode($workflow['status_json'],true);
						
						$this->data['workflow_title'] = $workflow['title'];
					
					$this->data['btn_type'] = $type;
					//print_r($workflow_order);exit;

					switch($type){
						case 'edit':
							$this->data['head_title'] = 'Edit '.$workflow['title'].' Request';
						break;
						case 'approval':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
						break;
						case 'approval_update':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'update':
							$this->data['head_title'] = 'Update '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'send_email':
							$this->data['head_title'] = 'Waiting to Send '.$workflow['title'].' Request';
						break;
						case 'sendmail_update':
							$this->data['head_title'] = 'Waiting to Send/Edit '.$workflow['title'].' Request';
						break;
						case 'upload':
							$this->data['head_title'] = 'Upload Confirm File '.$workflow['title'].' Request';
						break;
						case 'complete':
							$this->data['head_title'] = $workflow['title'].' Request';
							$this->data['last_action'] = $workflow_order['action_before_complete'];
						break;
						default:
							$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					foreach(json_decode($workflow['status_json'],true) as $k => $v){
						
						if($v == 'Completed'){
							$this->data['completed_status_id'] = $k;
							break;
						}
					}
				//////////////////////////////////////////////////////////////////////////////////////
				
				/*
				if($this->data['result']['status'] == 1){
					
					if($role_id == SALES_EXECUTIVE){
					$this->data['head_title'] = 'Edit Business Trip Report';
					}else if($role_id == SALES_MANAGER){
					$this->data['head_title'] = 'Confirm Business Trip Report';
					}else{
					$default = 1;
					}
					
				}else if($this->data['result']['status'] == 2){
					
					if($role_id == SERVICE_EXECUTIVE){
					$this->data['head_title'] = 'Edit Business Trip Report';
					}else if($role_id == SALES_MANAGER){
					$this->data['head_title'] = 'Confirm Business Trip Report';
					}else{
					$default = 1;
					}
					
				}else if($this->data['result']['status'] == 3){
					
					if($role_id == SERVICE_MANAGER || $role_id == SALES_MANAGER || $role_id == FINANCE){
					$this->data['head_title'] = 'Edit Business Trip Report';
					}else if($role_id == GENERAL_MANAGER){
					$this->data['head_title'] = 'Confirm Business Trip Report';
					}else{
					$default = 1;
					}
					
				}else{
					$default = 1;
				}
				
				//no related person
				if($default == 1){
					$this->data['head_title'] = 'View Business Trip Report';	
				}	
				*/							
				
			} else {
				$this->data['mode'] = 'Add';	
				$this->data['head_title'] = 'Create Business Trip Report';
				
				//get related workflow order data
					$workflow_flitter = array(
						'role_id' => $role_id,
						'status_id' => -1,
						'workflow_id' => 12,
					);
					
					//data form database
					$workflow = $this->Workflow_model->get(12);
					$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
					
					//get view bottom layout
					$type = '';
					if(!empty($workflow_order)){
						$type = $workflow_order['action'];
						if($workflow_order['action'] == 'create'){
							$this->data['head_title'] = 'Create '.$workflow['title'].' Request';
						}else{
							$this->data['head_title'] = $workflow['title'].' Request';
						}
					}else{
						$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					$this->data['btn_type'] = $type;
			}
			
			$this->data['company_list'] = json_encode($this->Customers_model->getAll());
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		
            
			
      }	  	  
	  
	  public function submit(){
		  $now = date("Y-m-d H:i:s");
		  $mode = $this->input->post("mode", true);
		  $id = $this->input->post("id", true);
		  	
		  $role_id = $this->data['userdata']['role_id'];
		  
		  //get value approve or not approve
		  $confirm = $this->input->post("confirm", true);
		  $correct_check = $this->input->post("correct_check", true);	  		 
 
 		  $customer_tmp = $this->input->post("customer_id", true);
		  if(isset($customer_tmp)){
		  $customer_tmp = explode('|',$customer_tmp);
		  }
		  
		  if($mode == 'Add'){
			  
			  $customer_id = $customer_tmp[0];
		  	  
		  }else{
		  
			  //btr
			  $btr_data = $this->Business_trip_model->get($id);
		  	  $customer_id = $btr_data['customer_id'];
			  
		  }	
		  
		  
		  //job
		  	$jobArray = array(
				'parent_id' =>0,
				'user_id' =>$this->data['userdata']['id'],
				'customer_id' =>isset($customer_tmp[0])?$customer_tmp[0]:'',
				'type' => 'Business Trip Report',
				'type_path' =>'finance/business_trip_edit/',
		 	 );
		  
		  //main
		  $array = array(		  	
		  	'customer_id' => isset($customer_tmp[0])?$customer_tmp[0]:'',
			'company_name' => isset($customer_tmp[2])?$customer_tmp[2]:'',
			'purpose' => $this->input->post("purpose", true),
			'requestor_id' => $this->input->post("requestor_id", true),
			//'company_name' => $this->input->post("id", true),
		  );
		  
		  //mail
			$mail_array = array(
		  	'creater_name' =>$this->data['userdata']['name'],
		  	);
		  
		  if(isset($_FILES['doc'])){
				
				if ( $_FILES['doc']['error'] == 0 && $_FILES['doc']['name'] != ''  ){									
						$pathinfo = pathinfo($_FILES['doc']['name']);
						$ext = $pathinfo['extension'];
						$ext = strtolower($ext);
											
						$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
						$path = "./uploads/".$filename.'.'.$ext;
						$save_path = base_url()."uploads/".$filename.'.'.$ext;
						$result = move_uploaded_file($_FILES['doc']['tmp_name'], $path);
						if(!$result) {
							die("cannot upload file");
						}	
					
						$array['report_upload']= $save_path;															
				}	
			}
		  
		  
		  
		  
		  //Add
		  if($mode == 'Add') {			  			  
			  
			  /*
			  //main
			  if($role_id == SALES_EXECUTIVE){
				  
				  $array['awaiting_table']  = SALES_MANAGER;
				  $array['status']  = 1;
				  
				  $jobArray['awaiting_role_id'] = SALES_MANAGER.',';
				  $jobArray['status_id'] = 1;
				  $jobArray['status_text'] = $this->data['btr_status_list'][1];
				  
			  }else if($role_id == SERVICE_EXECUTIVE){
				  
				  $array['awaiting_table']  = SERVICE_MANAGER;
				  $array['status']  = 2;
				  
				  $jobArray['awaiting_role_id'] = SERVICE_MANAGER.',';
				  $jobArray['status_id'] = 2;
				  $jobArray['status_text'] = $this->data['btr_status_list'][2];
				  
			  }else if($role_id == SALES_MANAGER || $role_id == SERVICE_MANAGER || $role_id == FINANCE){
				  
				  $array['awaiting_table']  = GENERAL_MANAGER;
				  $array['status']  = 3;
				  
				  $jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
				  $jobArray['status_id'] = 3;
				  $jobArray['status_text'] = $this->data['btr_status_list'][3];
				  
			  }
			  
			  
			  $array['requestor_id'] = $this->data['userdata']['employee_id'];
			  $array['create_user_id'] = $this->data['userdata']['id'];
			  $array['lastupdate_user_id'] = $this->data['userdata']['id'];
			  $array['created_date'] = $now;
			  $array['modified_date'] = $now;
			  
			  			  
			  $insert_id = $this->Business_trip_model->insert($array);		
			  $report_no = $this->Business_trip_model->zerofill($insert_id, 5);
			  
			  //insert job
			  $jobArray['serial'] =$report_no;
			  $jobArray['type_id'] =$insert_id;
			  $jobArray['created_date'] = $now;
			  $jobArray['modified_date'] = $now;
			  
			  $job_id = $this->Job_model->insert($jobArray);
			  
			  //after insert job,update job id,qr serial....
			  $array =array(
			  		'job_id'	=> $job_id,
			  		'report_no'  => $report_no,
					'latest_job_id'	=> $job_id,
			  );

			  	$this->Business_trip_model->update($insert_id, $array);
				//exit;		
				
			*/
			////////////////////////////////////////////////////////////////////////////////////
				
				//get related workflow order data
				$workflow_flitter = array(
					'role_id' => $role_id,
					'status_id' => -1,
					'workflow_id' => 12,
				);
				
				//data form database
				$workflow = $this->Workflow_model->get(12);
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
				$status = json_decode($workflow['status_json'],true);
				
				if(!empty($workflow_order)){
				
				//ad_hoc
				if($workflow_order['ad_hoc'] == 1){
					$array['ad_hoc'] = 1;
				}
				
				//purchaser
				$is_adhoc = false;
				if($role_id == 10){
					$is_adhoc = true;
				}
				
				//next status
				$next_status = $workflow_order['next_status_id'];
				$next_status_text = $status[$workflow_order['next_status_id']];
				
			  	//if need_det = 1
				if($workflow_order['need_det'] == 1){
					
					$formula = json_decode($workflow_order['formula'],true);
					
					//check if product or service price over value
					foreach($formula as $k => $v){
					
						$price = $this->input->post($v['target_colum'], true);
						$det_temp = $price." ".$v['logic']." ".$v['value'];
						$check = $this->parse_boolean($det_temp);
						
						//check price over value
						if($check){
							$next_status = $v['next_status_id'];
							$next_status_text = $status[$v['next_status_id']];
							break;
						}
						
					}
					
				}
				
				//get next status workflow order data
				$next_workflow_flitter = array(
					'status_id' => $next_status,
					'workflow_id' => 12,
				);
				
				//next status data
				$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
				$awating_person = $next_status_data['role_id'];
				
				
				
				if($awating_person == 'requestor'){
					$awating_person = $this->input->post("requestor_id", true);
				}
				//echo $awating_person;exit;
				
				//main
				$array['awaiting_table']  = $awating_person;
				$array['status'] = $next_status;
				//main
				$jobArray['awaiting_role_id'] = $awating_person.',';
				$jobArray['status_id'] = $next_status;
				$jobArray['status_text'] = $next_status_text; 	
				  
				  //insert main data
					$array['created_date'] = $now;
					$array['modified_date'] = $now;
					$array['create_user_id'] = $this->data['userdata']['id'];
					$array['lastupdate_user_id'] = $this->data['userdata']['id'];

					//新增資料
					$insert_id = $this->Business_trip_model->insert($array);					
					
					//更新 serial
					$report_no = $this->Business_trip_model->zerofill($insert_id, 5);
					
					//insert job
					$jobArray['serial'] = $report_no;
					$jobArray['type_id'] = $insert_id;
					
					$jobArray['created_date'] = $now;
					$jobArray['modified_date'] = $now;
					
					$job_id = $this->Job_model->insert($jobArray);
				  
					//after insert job,update job id,qr serial....
					$array =array(
						'job_id'	=> $job_id,
						'report_no'  => $report_no,
						'latest_job_id'	=> $job_id,
					);
					$this->Business_trip_model->update($insert_id, $array);
					
				  //mail
				  $related_role = $this->Job_model->getRelatedRoleID($job_id);
				  $mail_array['related_role'] = $related_role;		
				  $mail_array['title'] = $workflow['title']." ".$report_no." ".$next_status_text; 
				  	
				}
				
				////////////////////////////////////////////////////////////////////////////////////	
				  	  	  
			  
		  //Edit	  			  
		  } else {
			  
			    $btr_data = $this->Business_trip_model->get($id);
			  	$btr_status = $btr_data['status'];
			  
			    $requestor_employee = $this->Employee_model->get($btr_data['requestor_id']);
			    $requestor_role = $requestor_employee['role_id'];
			  
			  	$jobArray['serial'] =$btr_data['report_no'];
			  	$jobArray['type_id'] =$btr_data['id'];
			  	$jobArray['parent_id'] = $btr_data['latest_job_id'];
				$jobArray['created_date'] = $now;
				$jobArray['modified_date'] = $now;
				
				$next = 0;
			  	$last = 0;
			  	$status_change = 0;
			  
			  	/////////////////////////////////////////////////////////////////////////////////
			  
			  //can edit status
			  $edit_status = $this->Workflow_order_model->get_status_edit(12);
			  
			  //requestor edit
			  if(in_array($btr_data['status'], $edit_status) && $role_id == $requestor_role){
			  	  
						//get related workflow order data
						$workflow_flitter = array(
							'role_id' => $role_id,
							'status_id' => -1,
							'workflow_id' => 12,
						);
						
						//data form database
						$workflow = $this->Workflow_model->get(12);
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						$status = json_decode($workflow['status_json'],true);
						
						if(!empty($workflow_order)){
						
						//next status
						$next_status = $workflow_order['next_status_id'];
						$next_status_text = $status[$workflow_order['next_status_id']];
						
						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];
								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						//get next status workflow order data
						$next_workflow_flitter = array(
							'status_id' => $next_status,
							'workflow_id' => 12,
						);
						
						//next status data
						$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
						$awating_person = $next_status_data['role_id'];
						
						//if awating person equal requestor ,asign requestor for it
						if($awating_person == 'requestor'){
							$awating_person = $requestor_role;
						}
						
						//main
						$array['awaiting_table']  = $awating_person;
						$array['status'] = $next_status;
						
						//job
						$jobArray['awaiting_role_id'] = $awating_person.',';
						$jobArray['status_id'] = $next_status;
						$jobArray['status_text'] = $next_status_text;
						
						
						}
						
				  
			  }else{
				  		
												
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $btr_data['status'],
							'workflow_id' => 12,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(12);
						$status = json_decode($workflow['status_json'],true);
						
						//print_r($workflow_order);exit;
						if(isset($action)){
						
						//action
						switch ($action) {
							case 'approval':
							
								//approved
								
								//clear data infront
								$array = array();
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];

										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
										
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'approval_sendmail':
							
								//approved
								
								//clear data infront
								$array = array();
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];

										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
										
										//sent mail to service manager
										$role_service_manager = $this->Employee_model->get_related_role(array($workflow_order['who_email']));
										$sitename = $this->Settings_model->get_settings(1);
										$default_email = $this->Settings_model->get_settings(6);
										
										$this->load->library('email');
										
										if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
											foreach($role_service_manager as $k => $v){
												
												$this->email->clear();
												$this->email->from($default_email['value'], $sitename['value']);
												$this->email->to($v['email']); 
												$this->email->subject('Service request completed');
												$this->email->message($sr_data['service_serial'].' Service request had completed');
												$this->email->send();
											  
											}
										}
										
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'update':
								
								//next status
								$next_status = $workflow_order['next_status_id'];
								$next_status_text = $status[$workflow_order['next_status_id']];
								
								$array = array(
								);
								
								//check last job
								if($workflow_order['next_status_text'] == 'Completed'){
									$last = 1;
								}
								
								$next = 1;
								
								break;
							case 'approval_update':
								
								if($confirm == 1){
									
									if($correct_check == 'on'){
										
										//next status
										$next_status = $workflow_order['next_status_id'];
										$next_status_text = $status[$workflow_order['next_status_id']];
										
										$target_colum = json_decode($workflow_order['target_colum'],true);
										
										//check update colum single or multiple
										$array = array();
										foreach($target_colum as $k => $v){
										
											if($v['type'] == 'single'){
												
												if($v['data_type'] == 'string'){
													
												$array[$v['name']] = $this->input->post($v['name'],true);
												
												}else if($v['data_type'] == 'date'){
													
													$date = $this->input->post($v['name'], true);
													if(!empty($date)) {
													  $tmp = explode("/", $date);
													  $date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
													  $array[$v['name']] = $date;
													}
													
												}
											
											}else{
												
											}
											
										}
										$next = 1;
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
									}
									
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
							case 'upload':
							
								//clear data infront
								$array = array();
								
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'comfirm_file'	=> $save_path,
											);	
											
											//check last job
											if($workflow_order['next_status_text'] == 'Completed'){
												$last = 1;
											}
											
											//next status
											$next_status = $workflow_order['next_status_id'];
											$next_status_text = $status[$workflow_order['next_status_id']];	
											$next = 1;
																							
									}	
									
								}
								
								break;	
							case 'complete':
							
								//clear data infront
								$array = array();
							
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'comfirm_file'	=> $save_path,
											);	
										
									}	
								}
								
								break;	
								
						}
						
						//print_r($workflow_order);exit;
						//if need_det = 1
						
						if($workflow_order['need_det'] == 1){

							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$value = $this->input->post($v['target_colum'], true);
								//string or number
								if(is_numeric($v['value'])){
									$det_temp = $value." ".$v['logic']." ".$v['value'];
								}else{
									
									if($v['value'] == 'original'){
										$string = $sr_data['term_condition'];
									}else{
										$string = $v['value'];
									}
									
									$det_temp = "'".md5(trim($value))."' ".$v['logic']." '".md5(trim($string))."'";
								}
								
								
								$check = $this->parse_boolean($det_temp);
								
								//check
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
							}
							
						}

						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 12,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
						}
						
						}
						
				  
			  }
			  
			 	if(isset($next_status)){
					//main
					$array['awaiting_table']  = $awating_person;
					$array['status'] = $next_status;
					//job
					$jobArray['awaiting_role_id'] = $awating_person.',';
					$jobArray['status_id'] = $next_status;
					$jobArray['status_text'] = $next_status_text;
				}
				//print_r($array);exit;
			  //////////////////////////////////////////////////////////////////////////////////////
			  
			  /*
			  //判断status 给予下个job
				if($btr_status == 1){
					
					//SALES_EXECUTIVE edit	
					if($role_id == SALES_EXECUTIVE){
						
						$array['awaiting_table']  = SALES_MANAGER;
						$array['status']  = 1;
						//job
						$jobArray['awaiting_role_id'] = SALES_MANAGER.',';
						$jobArray['status_id'] = 1;
						$jobArray['status_text'] = $this->data['btr_status_list'][1];
						//mail
						$mail_array['related_role'] = array(SALES_EXECUTIVE,SALES_MANAGER);
						$mail_array['title'] = $btr_data['report_no'].' '.$this->data['btr_status_list'][1];
						
					//SALES manager approved/denied
					}else if($role_id == SALES_MANAGER){
						
						//SALES_MANAGER denied
						if($confirm == 0){
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	//mail
							$mail_array['related_role'] = array(SALES_EXECUTIVE,SALES_MANAGER);
							$mail_array['title'] = $btr_data['report_no'].' '.$this->data['btr_status_list'][0];
						  
						  	$next = 1;
							$last = 1;

						//SALES_MANAGER approved	
						} else if($correct_check == 'on'){
							
							$array['awaiting_table']  = '';
							$array['status']  = 4;
							$jobArray['awaiting_role_id'] = '';
							$jobArray['status_id'] = 4;
							$jobArray['status_text'] = $this->data['btr_status_list'][4];
							
							//mail
							$mail_array['related_role'] = array(GENERAL_MANAGER,FINANCE);
							$mail_array['title'] = $btr_data['report_no'].' '.$this->data['btr_status_list'][4];							
							$next = 1;	
							$last = 1;
													
						}
						
					}
				}else if($btr_status == 2){
					
					//service EXECUTIVE edit	
					if($role_id == SERVICE_EXECUTIVE){
						
						$array['awaiting_table']  = SERVICE_MANAGER;
						$array['status']  = 2;
						//job
						$jobArray['awaiting_role_id'] = SERVICE_MANAGER.',';
						$jobArray['status_id'] = 2;
						$jobArray['status_text'] = $this->data['btr_status_list'][2];
						//mail
						$mail_array['related_role'] = array(SERVICE_EXECUTIVE,SERVICE_MANAGER);
						$mail_array['title'] = $btr_data['report_no'].' '.$this->data['btr_status_list'][2];
						
					//service manager approved/denied
					}else if($role_id == SERVICE_MANAGER){
						
						//SERVICE_MANAGER denied
						if($confirm == 0){
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	//mail
							$mail_array['related_role'] = array(SERVICE_MANAGER,SERVICE_MANAGER);
							$mail_array['title'] = $btr_data['report_no'].' '.$this->data['btr_status_list'][0];
						  
						  	$next = 1;
							$last = 1;

						//SERVICE_MANAGER approved	
						} else if($correct_check == 'on'){
							
							$array['awaiting_table']  = '';
							$array['status']  = 4;
							$jobArray['awaiting_role_id'] = '';
							$jobArray['status_id'] = 4;
							$jobArray['status_text'] = $this->data['btr_status_list'][4];
							
							//mail
							$mail_array['related_role'] = array(GENERAL_MANAGER,FINANCE);
							$mail_array['title'] = $btr_data['report_no'].' '.$this->data['btr_status_list'][4];							
							$next = 1;	
							$last = 1;
													
						}
						
					}
				}else if($btr_status == 3){
					
					//SERVICE_MANAGER/SALES_MANAGER/FINANCE edit	
					if($role_id == SERVICE_MANAGER || $role_id == SALES_MANAGER || $role_id == FINANCE){
						
						$array['awaiting_table']  = GENERAL_MANAGER;
						$array['status']  = 3;
						//job
						$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
						$jobArray['status_id'] = 3;
						$jobArray['status_text'] = $this->data['btr_status_list'][3];
						//mail
						$mail_array['related_role'] = array($requestor_role,GENERAL_MANAGER);
						$mail_array['title'] = $btr_data['report_no'].' '.$this->data['btr_status_list'][3];
						
					//GENERAL_MANAGER approved/denied
					}else if($role_id == GENERAL_MANAGER){
						
						//SERVICE_MANAGER denied
						if($confirm == 0){
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	//mail
							$mail_array['related_role'] = array($requestor_role,GENERAL_MANAGER);
							$mail_array['title'] = $btr_data['report_no'].' '.$this->data['btr_status_list'][0];
						  
						  	$next = 1;
							$last = 1;

						//GENERAL_MANAGER approved	
						} else if($correct_check == 'on'){
							
							$array['awaiting_table']  = '';
							$array['status']  = 4;
							$jobArray['awaiting_role_id'] = '';
							$jobArray['status_id'] = 4;
							$jobArray['status_text'] = $this->data['btr_status_list'][4];
							
							//mail
							$mail_array['related_role'] = array($requestor_role,GENERAL_MANAGER);
							$mail_array['title'] = $btr_data['report_no'].' '.$this->data['btr_status_list'][4];
														
							$next = 1;	
							$last = 1;
													
						}
						
					}
				}
				*/
				
				 if($next == 1){
				  $this->Job_model->update($jobArray['parent_id'], array(
				  	'is_completed' => 1,
				  ));
				  //不是最後
				  if($last == 0){
				  	$new_job_id = $this->Job_model->insert($jobArray);				  	
					$array['latest_job_id'] = $new_job_id;
				  }
			    }
				
			  $array['lastupdate_user_id'] = $this->data['userdata']['id'];
			  $array['modified_date'] = $now;
			  
			  $this->Business_trip_model->update($id, $array);
			  
			  //mail
				  if(isset($new_job_id)){
				  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
				  $mail_array['related_role'] = $related_role;		
				  $mail_array['title'] = $workflow['title']." ".$btr_data['report_no']." ".$next_status_text;
				  }
			  
			  
			    
		  }
		  		  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		  
		  
	  }
	  
	  public function del($id){
		  $this->Business_trip_model->delete($id);
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		
	  }  	  
	  

}

?>