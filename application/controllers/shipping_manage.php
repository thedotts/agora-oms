<?php

class Shipping_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();     
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Shipping_model');
			$this->load->model('Job_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');

            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$this->data['userdata'] = $this->session->userdata("userdata");
			}      
			
			$this->data['status_list'] = $this->Shipping_model->status_list(false);			
			$this->data['country_list'] = $this->Shipping_model->country_list();
			$this->data['shipping_type'] = $this->Shipping_model->shipping_type();
			$this->data['currency_list'] = $this->Shipping_model->currency_list();			
			
			$this->data['group_name'] = "administrator";  
			$this->data['model_name'] = "shipping";  
			$this->data['common_name'] = "Shipping"; 
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);  
			
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}  
           
      }
         
	  
	  public function index($q="ALL", $page=1) {  
          			
            $this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/anexus/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/';
			if($q == 'ALL') {
				$q = "";
			}
			$this->data['q'] = $q;
									
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Shipping_model->record_count($filter, $q);
			
			//get particular ranged list
			$this->data['results'] = $this->Shipping_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start);
						
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->session->set_userdata("lastpage", $url.$page);
			
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  	  	 	  	  
	  
	  public function add($id=false) {  
	  
          	if($id !== false) {
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Shipping_model->get($id);	
			} else {
				$this->data['mode'] = 'Add';	
			}
			
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		            
			
      }	  	
	  
	  public function del($id) {
		  
		  $employee_data = $this->Shipping_model->get($id);		  
		  $this->Shipping_model->delete($id);		  
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		
		  
	  }
	  
	  public function sortByWeight($a, $b) {
		return $a['weight'] - $b['weight'];
	  }  
	  
	  public function submit(){
		  
		  $mode 					= $this->input->post("mode", true);
		  $id 						= $this->input->post("id", true);		  		  		  		  
		  $shipper_name 			= $this->input->post("shipper_name", true);		  
		  $shipping_zone 			= $this->input->post("shipping_zone", true);		  
		  $country 					= isset($_POST['country'])?implode(",",$_POST['country']):"";		  
		  $enabled 					= isset($_POST['enabled'])?1:0;
		  $shipping_type 			= $this->input->post("shipping_type", true);		  		  
		  $currency 				= $this->input->post("currency", true);
		  
		  $cost_json = array(); 
		  foreach($_POST['weight'] as $k => $v){
			  $tmp = array(
			  	'weight' => $v,
				'cost' => $_POST['cost'][$k],
			  );
			  $cost_json[] = $tmp;
		  }				 
				
		  usort($cost_json, array($this,'sortByWeight'));		 		  
		  //print_r($cost_json);exit;					  
								  
		  $iu_array = array(		  			  	
		  	'shipper_name'			=> $shipper_name,			
			'shipping_zone'			=> $shipping_zone,
			'country'				=> $country,
			'enabled'				=> $enabled,
			'shipping_type'			=> $shipping_type,			
			'cost_json'				=> json_encode($cost_json),			
			'currency'				=> $currency,			
		  );		 		  
		  		  		  
		  //Add
		  if($mode == 'Add') {			  			  
			  			  
			  $iu_array['created_date'] = date("Y-m-d H:i:s");			  
			  $this->Shipping_model->insert($iu_array);			  			  		  	  			  			  			  			  
			  
		  //Edit	  			  
		  } else {
			  
			  $iu_array['modified_date'] = date("Y-m-d H:i:s");			  
			  $this->Shipping_model->update($id, $iu_array);			  			  			  
			  
		  }
		  		  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		  
		  
	  }
	  
	 
}

?>