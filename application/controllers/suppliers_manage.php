<?php

class Suppliers_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();     
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Suppliers_model');
			$this->load->model('Job_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');

            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$this->data['userdata'] = $this->session->userdata("userdata");
			}      
			
			$this->data['status_list'] = $this->Suppliers_model->status_list(false);			
			
			$this->data['group_name'] = "administrator";  
			$this->data['model_name'] = "suppliers";  
			$this->data['common_name'] = "Suppliers Account";   
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}  
           
      }
         
	  
	  public function index($q="ALL", $page=1) {  
          			
            $this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/anexus/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/';
			if($q == 'ALL') {
				$q = "";
			}
			$this->data['q'] = $q;
									
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Suppliers_model->record_count($filter, $q);
			
			//get particular ranged list
			$this->data['results'] = $this->Suppliers_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start);
						
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->session->set_userdata("lastpage", $url.$page);
			
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  	  	 	  	  
	  
	  public function add($id=false) {  
	  
          	if($id !== false) {
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Suppliers_model->get($id);	
			} else {
				$this->data['mode'] = 'Add';	
			}
			
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		            
			
      }	  	
	  
	  public function del($id) {
		  
		  $employee_data = $this->Suppliers_model->get($id);		  
		  $this->Suppliers_model->delete($id);		  
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		
		  
	  }  
	  
	  public function submit(){
		  
		  $mode 					= $this->input->post("mode", true);
		  $id 						= $this->input->post("id", true);		  		  		  		  
		  $company_name 			= $this->input->post("company_name", true);
		  $status 					= $this->input->post("status", true);		  
		  $contact_person 			= $this->input->post("contact_person", true);		  
		  $contact_info 			= $this->input->post("contact_info", true);		 
		  $email 					= $this->input->post("email", true);	 
		  $address 					= $this->input->post("address", true);
		  $postal 					= $this->input->post("postal", true);		  		  
		  		  		 		  
		  $iu_array = array(		  	
		  	'company_name'			=> $company_name,
			'status'				=> $status,			
			'contact_person'		=> $contact_person,
			'contact_info'			=> $contact_info,
			'email'					=> $email,
			'address'				=> $address,
			'postal'				=> $postal,			
		  );		 		  
		  		  		  
		  //Add
		  if($mode == 'Add') {			  			  
			  			  
			  $iu_array['created_date'] = date("Y-m-d H:i:s");			  
			  $customer_id = $this->Suppliers_model->insert($iu_array);			  			  		  	  			  
			  
			  $custom_code = $this->Suppliers_model->zerofill($customer_id);
			  $update_array = array(
			  	'supplier_no'	=> $custom_code,
			  );
			  $this->Suppliers_model->update($customer_id, $update_array);
			  
			  
		  //Edit	  			  
		  } else {
			  
			  $iu_array['modified_date'] = date("Y-m-d H:i:s");			  
			  $this->Suppliers_model->update($id, $iu_array);			  			  			  
			  
		  }
		  		  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		  
		  
	  }
	  
	 
}

?>