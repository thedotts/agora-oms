<?php

class Report_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();     
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Customers_model');
			$this->load->model('Job_model');
			$this->load->model('Shipping_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Quotation_model');
			$this->load->model('Employee_model');
			$this->load->model('Workflow_model');
			$this->load->model('Reports_model');
			$this->load->model('Order_model');
			$this->load->model('Products_model');
			$this->load->model('Product_category_model');

            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$userdata = $this->session->userdata("userdata");
				
				//print_r($userdata);exit;
				if(strpos($userdata['role_id'],',')){
					$userdata['role_id'] = explode(',',$userdata['role_id']);	
				}else{
					$userdata['role_id'] = array($userdata['role_id']);	
				}
				
				
				$this->data['userdata'] = $userdata;
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}       
			
			$this->data['status_list'] = $this->Customers_model->status_list(false);
			$this->data['country_list'] = $this->Shipping_model->country_list();
			$this->data['currency_list'] = $this->Customers_model->currency_list();
			
			$this->data['group_name'] = "administrator";  
			$this->data['model_name'] = "reports";  
			$this->data['common_name'] = "Reports";  
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			if(in_array(3,$this->data['userdata']['role_id']) ){
				
				
				$a_csutomer = $this->data['staff_info']['assign_customer'];
				
				if($a_csutomer != ''){
				
					if(strpos($a_csutomer,',')){
						$a_customer_array = explode(',',$a_csutomer);	
					}else{
						$a_customer_array = array($a_csutomer);	
					}
				
				}else{
					$a_customer_array = array();	
				}
				
				$this->data['task_display_count'] = $this->Job_model->record_count2(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
				),$this->data['userdata']['role_id'],$a_customer_array);
				
				
				

			}else{
			
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			}
			
			/*
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}
			*/
           
      }
         
	  
	  public function index() {  
          	
			$this->data['order_year'] = $this->Order_model->order_year();
			
			//$this->data['userdata']['role_id'] = 1;
			//$this->data['userdata']['employee_id'] = 3;
			//manager
			if(in_array(2,$this->data['userdata']['role_id'])){
				
				$employee = $this->Employee_model->get_where(array('is_deleted'=>0));
				
				$sales_array = array();
				foreach($employee as $k => $v){
				
					if(strpos($v['role_id'],',')){
						$tmp_role = explode(',',$v['role_id']);	
					}else{
						$tmp_role = array($v['role_id']);	
					}
					
					if(in_array(3,$tmp_role)){
						$sales_array[] = $v;
					}
					
				}
				
				$this->data['sales'] = $sales_array;
				
				$this->data['customer'] = $this->Customers_model->get_where(array('is_deleted'=>0));
				
			//sales
			}else{
				
				$sales = $this->Employee_model->get($this->data['userdata']['employee_id']);
				$this->data['sales'] = array($sales);
				
				//sales assign customer
				$assign_customer = $sales['assign_customer'];
				
				if($assign_customer != ''){
					if(strpos($assign_customer,',')){
						$assign_customer = explode(',',$assign_customer);
					}else{
						$assign_customer = array($assign_customer);
					}
					$this->data['customer'] = $this->Customers_model->customer_whereIn($assign_customer);
				}else{
					$this->data['customer'] = array();
				}
				
			}

			$this->data['workflow_table'] = $this->Workflow_model->get_where();
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/reports/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	
	  //1
	  public function qr_track($startdate='', $enddate='') {  
	  			
          	//to do ...		            
			if($startdate != '' && $enddate != '') {
				
				//export CSV
				$this->data['startdate'] = $startdate;
				$this->data['enddate'] = $enddate;
				
				$table_array = array(
					'qr_serial' => 'Ref No.',
					'request_date'	=> 'QR Date',
					'requestor_name'	=> 'Requestor',
					'request_date'	=> 'Customer Information',
					'request_date'	=> 'Description',
					'request_date'	=> 'Quote Ref. No.',
					'request_date'	=> 'Quote Date',
					'request_date'	=> 'Country',
				);
				
				//load data from quotation
				
				$data = $this->Quotation_model->get_where();
				
				$example_data = array();
				foreach($data as $v) {
					$tmp = array();
					foreach($table_array as $k2=>$v2) {
						$tmp[$v2] = $v[$k2];						
					}
					$example_data[] = $tmp;
				}
										
				$this->export($data);
					
			}
			
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/reports/qr_track', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	 
	  //2
	  public function pr_received() {  
          	//to do ...				            
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/reports/pr_received', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	
	  
	  //3
	  public function aStock() {  
          	//to do ...				            
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/reports/aStock', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	
	  
	  //4
	  public function logistic() {  
          	//to do ...				            
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/reports/logistic', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	
	  
	  //5
	  public function tracking_of_fso() {  
          	//to do ...				            
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/reports/tracking_of_fso', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	
	  
	  //6
	  public function fso_table() {  
          	//to do ...				            
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/reports/fso_table', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	 	
	  
	  public function download_send_headers($filename) {
            // disable caching
            $now = gmdate("D, d M Y H:i:s");
            header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
            header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
            header("Last-Modified: {$now} GMT");

            // force download  
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");

            // disposition / encoding on response body
            header("Content-Disposition: attachment;filename={$filename}");
            header("Content-Transfer-Encoding: binary");
        }
        
        public function array2csv(array &$array)
        {
           if (count($array) == 0) {
             return null;
           }
           ob_start();
           $df = fopen("php://output", 'w');
           fputcsv($df, array_keys(reset($array)));
           foreach ($array as $row) {
              fputcsv($df, $row);
           }
           fclose($df);
           return ob_get_clean();
        }
        
        public function export($data){                        
            
            $this->download_send_headers("data_export_" . date("YmdHis") . ".csv");
            echo $this->array2csv($data);
            die();
        }  
		
		public function ajax_getTableColum(){
			
			$table_name = $_POST['table'];
			$table_colum = $this->Reports_model->getTableColum($_POST['table']);
			
			$json = array(
				'status'=>'OK',
				'data'	=>$table_colum,
			);
			
			echo json_encode($json);
			exit;
			
		}	
		
		public function ajax_order(){
		
			$month = $_POST['month'];
			$year = $_POST['year'];
			$staff = $_POST['staff'];
			
			$tmp = $this->Order_model->filter_order($month, $year, $staff);
			
			$name_list = $this->Employee_model->name_list();
			$order_status = $this->Order_model->status_list();
			
			foreach($tmp as $k => $v){
				$tmp[$k]['status_name'] = $order_status[$v['status']];
				$tmp[$k]['employee_name'] = $name_list[$v['create_user_id']];
			}
			
			$json = array(
				'status' 	=> 'OK',
				'data'		=> $tmp,
			);
				
			echo json_encode($json);exit;
				
		}  
		
		public function ajax_product(){
		
			$customer = $_POST['customer'];
			$start = $_POST['start'];
			$end = $_POST['end'];
			
			$tmp = $this->Order_model->filter_product($customer, $start, $end);
			
			$cat_list = $this->Product_category_model->getIDKeyArray('name');
			
			foreach($tmp as $k => $v){
				$product = $this->Products_model->get($v['product_id']);
				
				$tmp[$k]['product_name'] = $product['product_name'];
				$tmp[$k]['product_no'] = $product['product_no'];
				$tmp[$k]['supplier'] = $product['supplier'];
				$tmp[$k]['product_category'] = $cat_list[$product['product_category']];
				
			}
			
			$json = array(
				'status' 	=> 'OK',
				'data'		=> $tmp,
			);
				
			echo json_encode($json);exit;
				
		} 
	  
	 
}

?>