<?php

class Ajaxgetdata extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();
            $this->load->model('function_model');
            $this->load->model('Product_model'); //使用者MODEL   
			$this->load->model('Warehouse_model'); //使用者MODEL  
            $this->load->model('cart_model');
            $this->load->model('transaction_model');
            $this->load->model('stock_model');
            $this->load->model('stock_product_model');
            
            $this->data['init'] = $this->function_model->page_init();
            
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
            if ($this->data['islogin']) {

                  $this->data['userdata'] = $this->session->userdata("userdata");                  
            } else {
                  redirect('/', 'refresh');
            }
           
      }
   
      public function index() {           
          $action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
          $temp = array();
          switch ($action){
              case 'add_stock':
                  
                  $product_id = isset($_REQUEST['id'])?$_REQUEST['id']:'';
                  $newqty = isset($_REQUEST['qty'])?$_REQUEST['qty']:'';
                  $warehouse_id = isset($_REQUEST['warehouse_id'])?$_REQUEST['warehouse_id']:'';
                  $stock_serial = date('YmdHis');
                  
                  //原本的庫存
                  $original_qty = $this->stock_model->get_products_qty($product_id,$warehouse_id);
                  
                  //減少
                  if($original_qty > $newqty){
                      $qty = $original_qty - $newqty;
                      $array = array(
                              'type'            => 0, // 0 = export , 1 = import
                              'entity_type'     => 'warehouse',
                              'entity_id'       => $warehouse_id,
                              'stock_serial'    => $stock_serial
                      );
                      $stock_id = $this->stock_model->insertStock($array,$this->data['userdata']);

                      $sarray = array();
                      $sarray = array(
                          'stock_id'        => $stock_id,
                          'product_id'      => $product_id,
                          'qty'             => $qty
                      );
                      $this->stock_product_model->insertStock_product($sarray);
                      $temp = array(
                          'status'  => 'OK',
                          'results' => $this->data['init']['lang']['minus_successed']
                      );
                  }
                  //增加
                  else if($original_qty < $newqty){
                      $qty = $newqty - $original_qty;
                      
                      $array = array(
                          'type'            => 1, // 0 = export , 1 = import
                          'entity_type'     => 'warehouse',
                          'entity_id'       => $warehouse_id,
                          'stock_serial'    => $stock_serial
                      );
                      $stock_id = $this->stock_model->insertStock($array,$this->data['userdata']);

                      $sarray = array();
                      $sarray = array(
                          'stock_id'        => $stock_id,
                          'product_id'      => $product_id,
                          'qty'             => $qty
                      );
                      $this->stock_product_model->insertStock_product($sarray);


                      $temp = array(
                          'status'  => 'OK',
                          'results' => $this->data['init']['lang']['add_successed']
                      );
                  }
                  //沒有改變
                  else{
                      $temp = array(
                          'status'  => 'ERROR',
                          'results' => $this->data['init']['lang']['change_stock']
                      );
                  }
                  
                  
                  
                  break;
              case 'transaction_update':
                  $transaction_id = isset($_REQUEST['transaction_id'])?$_REQUEST['transaction_id']:0;
                  $status = isset($_REQUEST['status'])?$_REQUEST['status']:0;
                  $array = array(
                      'status'  => $status,
                  );
                  
                  $this->transaction_model->updateTransaction($transaction_id,$array,$this->data['userdata']);
                  
                  $temp = array(
                          'status'  => 'OK',
                          'results' => $this->data['init']['lang']['status_change']
                      );
                  break;
              default :
                  show_error('Invalid request', '500');
                  break;
          }
          
          echo json_encode($temp);
      }

}

?>