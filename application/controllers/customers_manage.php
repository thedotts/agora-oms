<?php

class Customers_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();     
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Customers_model');
			$this->load->model('Job_model');
			$this->load->model('Shipping_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Products_model');
			$this->load->model('Audit_log_model');
			$this->load->model('Employee_model');
			$this->load->model('Order_model');
			$this->load->model('Packing_list_model');
			$this->load->model('Order_item_model');
			$this->load->model('Packing_list_item_model');
			$this->load->model('Delivery_order_model');

            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$userdata = $this->session->userdata("userdata");
				
				//print_r($userdata);exit;
				if(strpos($userdata['role_id'],',')){
					$userdata['role_id'] = explode(',',$userdata['role_id']);	
				}else{
					$userdata['role_id'] = array($userdata['role_id']);	
				}
				
				
				$this->data['userdata'] = $userdata;
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}        
			
			$this->data['status_list'] = $this->Customers_model->status_list(false);
			$this->data['country_list'] = $this->Shipping_model->country_list();
			$this->data['currency_list'] = $this->Customers_model->currency_list();
			
			$this->data['group_name'] = "administrator";  
			$this->data['model_name'] = "customers";  
			$this->data['common_name'] = "Customer Account";  
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			if(in_array(3,$this->data['userdata']['role_id'])){
				
				
				$a_csutomer = $this->data['staff_info']['assign_customer'];
				
				if($a_csutomer != ''){
				
					if(strpos($a_csutomer,',')){
						$a_customer_array = explode(',',$a_csutomer);	
					}else{
						$a_customer_array = array($a_csutomer);	
					}
				
				}else{
					$a_customer_array = array();	
				}
				
				$this->data['task_display_count'] = $this->Job_model->record_count2(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
				),$this->data['userdata']['role_id'],$a_customer_array);
				
				
				

			}else{
			
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			}
			
			/*
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}
			*/
			
			//print_r($this->data);exit;
           
      }
         
	  
	  public function index($q="ALL", $page=1, $alert=0) {  
          		
			$this->data['alert'] = $alert;  
            $this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			if(in_array(3,$this->data['userdata']['role_id'])){
				$sales_data = $this->Employee_model->get($this->data['userdata']['employee_id']);
				$sales_customer = $sales_data['assign_customer'];
				
				if(strpos($sales_customer,',')){
					
					$assing_cutomer = explode(',',$sales_customer);
					
				}else{
					$assing_cutomer = array($sales_customer);
				}
				
				//print_r($assing_cutomer);exit;
				
			}
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/agora/'.$this->data['group_name'].'/customers/'.$q.'/';
			if($q == 'ALL') {
				$q = "";
			}else{
				$q = urldecode($q);
			}
			$this->data['q'] = urldecode($q);
									
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//当只有sales 一个角色的时候才显示分配的customer
			if(in_array(3,$this->data['userdata']['role_id'])){
				//count total Data
				$this->data["total"] = $this->Customers_model->record_count($filter, $q,$assing_cutomer);
				
				//get particular ranged list
				$this->data['results'] = $this->Customers_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start,$assing_cutomer);
			
			
			//显示所有的customer
			}else{
				//count total Data
				$this->data["total"] = $this->Customers_model->record_count($filter, $q);
				
				//get particular ranged list
				$this->data['results'] = $this->Customers_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start);
			
			}
						
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->session->set_userdata("lastpage", $url.$page);
			
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }
	  
	  public function all_customer($q="ALL", $page=1, $alert=0) {  
          		
			$this->data['alert'] = $alert;  
            $this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/';
			if($q == 'ALL') {
				$q = "";
			}
			$this->data['q'] = $q;
									
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Customers_model->record_count($filter, $q);
			
			//get particular ranged list
			$this->data['results'] = $this->Customers_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start);
						
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->session->set_userdata("lastpage", $url.$page);
			
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/all_customer', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  	  
	  
	  public function view($id=false) {  
	  
          	
			$this->data['result'] = $this->Customers_model->get($id);	
			
			//All jobs related
			$this->data['job'] = $this->Job_model->get_where_groupBy2(array(
				'customer_id'	=> $id,
				'is_deleted'	=> 0,
				//'type'			=> 'Order'
			));
			
			
			
			//抓出job的order id
			foreach($this->data['job'] as $x => $y){
				
				
				
				$this->data['job'][$x]['order_id'] = $this->Order_model->getId_bySerial($y['order_no']);
				
				$order_data_tmp = $this->Order_model->get($this->data['job'][$x]['order_id']);
				$v = $order_data_tmp;
				
				//print_r($v);exit;
				
				//status is completed
				if($v['status'] == 2){
					
					$related_packing = $this->Packing_list_model->get_where(array('is_deleted'=>0,'status'=>1,'order_id'=>$v['id']));
					
					if(!empty($related_packing)){

						//order 的數量
						$order_item = $this->Order_item_model->getRelatedItem($v['id']);
						$order_qty = 0;
						if(!empty($order_item)){
							foreach($order_item as $k2=>$v2){
								$order_qty += $v2['quantity'];
							}
						}
						
						//packing 數量
						$packing_qty = 0;
						foreach($related_packing as $k2 => $v2){
							
							$packing_item = $this->Packing_list_item_model->getRelatedItem($v2['id']);
							
							foreach($packing_item as $k3 => $v3){
								$packing_qty += $v3['quantity_pack'];
							}
							
						}
						
						//packing list 的數量
						$pack_count = count($related_packing);
						
						//判斷item pack完了沒
						if($order_qty == $packing_qty){
							
							//目前有的delivery order
							$related_do = count($this->Delivery_order_model->get_where(array('is_deleted'=>0,'status'=>2,'order_id'=>$v['id'])));
							
							if($related_do == 0){
								$this->data['job'][$x]['status_text'] = 'Pending Delivery Order';
							//代表do已處理完畢
							}else if($related_do == $pack_count){

								$this->data['job'][$x]['status_text'] = 'Completed';
								/*
								//do 數量
								$do_count = $related_do;
								
								//invoice 數量
								$related_invoice = count($this->Invoice_model->get_where(array('is_deleted'=>0,'status'=>1,'order_id'=>$v['id'])));
								
								if($related_invoice == 0){
									$this->data['results'][$k]['status_text'] = 'Pending Invoice';
								}else if($do_count == $related_invoice){
									$this->data['results'][$k]['status_text'] = 'Completed';
								}else{
									$this->data['results'][$k]['status_text'] = 'Pending Generate Invoice ['.$related_invoice.' of '.$do_count.']';
								}
								*/
								
							}else{
								$this->data['job'][$x]['status_text'] = 'Pending Generate Delivery Order ['.($related_do+1).' of '.$pack_count.']';
							}
							
						}else{
							$this->data['job'][$x]['status_text'] = 'Pending Generate Packing List ['.($pack_count+1).' of '.($pack_count+1).']';
						}
						
					}else{
						$this->data['job'][$x]['status_text'] = 'Pending Packing List';
					}
					
				}//endif status
				
				
			}
			//print_r($this->data['job']);exit;
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/view', $this->data);
            $this->load->view('anexus/footer', $this->data);		            
			
      }	
	  	  
	  
	  public function add($id=false) {  
	  
          	if($id !== false) {
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Customers_model->get($id);
				
				//delivery address	
				$da_json = $this->data['result']['delivery_address'];
				$this->data['da_json'] = json_decode($da_json,true);
				
			} else {
				$this->data['mode'] = 'Add';	
			}
			
			//all product
			$this->data['product'] = json_encode($this->Products_model->get());
			
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		            
			
      }	  	
	  
	  public function del($id) {
		  
		  $employee_data = $this->Customers_model->get($id);		  
		  $this->Customers_model->delete($id);
		  $this->User_model->delete($employee_data['user_id']);  
		  $this->Employee_model->delete($employee_data['user_id']);  
		  
		  //audit log
		  $log_array = array(
			'ip_address'	=> $this->input->ip_address(),
			'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
			'table_affect'	=> 'customer',
			'description'	=> 'Delete customer',
			'created_date'	=> date('Y-m-d H:i:s'),
		  );
			  
		  $audit_id = $this->Audit_log_model->insert($log_array);	
		  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
		  $update_array = array(
			 'log_no'	=> $custom_code,
		  );
		  $this->Audit_log_model->update($audit_id, $update_array);
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage.'/3','refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/3'));
		  }		
		  
	  }  
	  
	  public function submit(){
		  
		  
		  $mode 					= $this->input->post("mode", true);
		  $id 						= $this->input->post("id", true);		  		  
		  
		  $company_name 			= $this->input->post("company_name", true);
		  $status 					= $this->input->post("status", true);		  
		  $primary_attention_to 	= $this->input->post("primary_attention_to", true);		  
		  $primary_contact_info 	= $this->input->post("primary_contact_info", true);
		  $primary_contact_email 	= $this->input->post("primary_contact_email", true);
		  $secondary_attention_to 	= $this->input->post("secondary_attention_to", true);		  
		  $secondary_contact_info 	= $this->input->post("secondary_contact_info", true);
		  $secondary_contact_email 	= $this->input->post("secondary_contact_email", true);
		  $country 					= $this->input->post("country", true);  		  
		  $address 					= $this->input->post("address", true);
		  $postal 					= $this->input->post("postal", true);
		  $default_price 			= $this->input->post("default_price", true);
		  
		  $password = $this->input->post("password", true);
		  $copassword = $this->input->post("copassword", true);
		  		  		 		  
		  $iu_array = array(
		  	'company_name'					=> $company_name,
			'status'						=> $status,			
			'primary_attention_to'			=> $primary_attention_to,
			'primary_contact_info'			=> $primary_contact_info,
			'primary_contact_email'			=> $primary_contact_email,
			'secondary_attention_to'		=> $secondary_attention_to,
			'secondary_contact_info'		=> $secondary_contact_info,
			'secondary_contact_email'		=> $secondary_contact_email,
			'country'						=> $country,
			'address'						=> $address,
			'postal'						=> $postal,
			'use_default_price'				=> $default_price=='on'?1:0,
			'create_user_id'				=> $this->data['userdata']['employee_id'],
		  );		 		  
		  
		  
		  //get all delivery address
		  $da_counter = $this->input->post("da_counter", true);
		  $da_json = array();
		  
		  for($i=0;$i<$da_counter;$i++){
			  
			  if($_POST['is_del'.$i] != 1){
				  $tmp_array = array(
					'alias'	=> $_POST['alias'.$i],
					'add'	=> $_POST['add'.$i],
					'postal'	=> $_POST['postal'.$i],
					'country'	=> $_POST['country'.$i],
				  );
				  
				  $da_json [] = $tmp_array;
			  }
				  
		  }
		  
		  $iu_array['delivery_address'] = json_encode($da_json);
		  	
		  //price list
		  $price_json = array();
		  if(isset($_POST['productId'])){
			  foreach($_POST['productId'] as $k => $v){
				  $tmp_array = array(
					'id'	=> $v,
					'price'	=> $_POST['price'][$k],
				  );
				  $price_json [] = $tmp_array;
			  }
		  }
		  
		  $iu_array['price_list'] = json_encode($price_json);
		  
		  //user employee
		  $new_array = array(
			'role_id'						=> 7,
			'status'						=> $status,
			'email'							=> $primary_contact_email,
		  );
		  	  	  
		  //Add
		  if($mode == 'Add') {	
		  
		  	  //insert employee & user
			  
			  $new_array['created_date'] = date("Y-m-d H:i:s");
			  
			  $employee_id = $this->Employee_model->insert($new_array);
			  
			  		  
			    			  
			  $iu_array['created_date'] = date("Y-m-d H:i:s");			  
			  $customer_id = $this->Customers_model->insert($iu_array);		
			  //echo $this->db->last_query();exit;	
			  
			  $usr_array = array(
				'role_id'		=> 7,
				'email'			=> $primary_contact_email,
				'name'			=> $company_name,
				'passwd'		=> md5($password),
				'employee_id'	=> $employee_id,
				'customer_id'	=> $customer_id,
				'created_date'	=> date("Y-m-d H:i:s"),				
			  );
			  			  
			  //when we get user_id, we update employee table			  
			  $user_id = $this->User_model->insert($usr_array);	
			    			  		  	  			  
			  
			  //update employee customer id
			  $u_array = array(
			  	'user_id' 		=> $user_id,
				'employee_no'	=> $this->Employee_model->zerofill($employee_id, 5),
				'customer_id'	=> $customer_id,
			  );
			  $this->Employee_model->update($employee_id, $u_array);
			  
			  $custom_code = $this->Customers_model->zerofill($customer_id);
			  $update_array = array(
			  	'custom_code'	=> $custom_code,
				'user_id'		=> $user_id,
			  );
			  $this->Customers_model->update($customer_id, $update_array);
			  
			  //audit log
			  $log_array = array(
			  	'ip_address'	=> $this->input->ip_address(),
				'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
				'table_affect'	=> 'customer',
				'description'	=> 'Added new customer',
				'created_date'	=> date('Y-m-d H:i:s'),
			  );
			  
			  
			  
			  $audit_id = $this->Audit_log_model->insert($log_array);	
			  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
			  $update_array = array(
			  	'log_no'	=> $custom_code,
			  );
			  $this->Audit_log_model->update($audit_id, $update_array);
			  
			  //auto assign customer
			  if(in_array(3,$this->data['userdata']['role_id'])){
				$sales_data = $this->Employee_model->get($this->data['userdata']['employee_id']);
				$sales_customer = $sales_data['assign_customer'];
				
				if($sales_customer != ''){
					if(strpos($sales_customer,',')){
						
						$assing_cutomer = explode(',',$sales_customer);
						
					}else{
						$assing_cutomer = array($sales_customer);
					}
				}else{
					$assing_cutomer = array();
				}
				
				$assing_cutomer[] = $customer_id;
				
				//echo count($assing_cutomer);
				//print_r($assing_cutomer);exit;
				
			  	$update_employee = array(
			  		'assign_customer' => implode(',',$assing_cutomer),
			  	);
			  
			  	$this->Employee_model->update($this->data['userdata']['employee_id'],$update_employee);
			  
			  }
			  
			  
		  //Edit	  			  
		  } else {
			  
			  $iu_array['modified_date'] = date("Y-m-d H:i:s");			  
			  $this->Customers_model->update($id, $iu_array);
			  
			  //update employee and user
			  $customer = $this->Customers_model->get($id);
			  $this->Employee_model->update($customer['user_id'], $new_array);
			  
			  $usr_array = array(
				'email'			=> $primary_contact_email,
				'name'			=> $company_name,				
				'modified_date'	=> date("Y-m-d H:i:s"),				
			  );
			  
			  if(!empty($password)) {
					$usr_array['passwd'] = md5($password);					  
			  }
			  
			  $employee_data = $this->Employee_model->get($customer['user_id']);
			  
			  			  
			  $this->User_model->update($employee_data['user_id'], $usr_array);
			  
			  
			  //audit log
			  $log_array = array(
			  	'ip_address'	=> $this->input->ip_address(),
				'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
				'table_affect'	=> 'customer',
				'description'	=> 'Edit customer',
				'created_date'	=> date('Y-m-d H:i:s'),
			  );
			  
			  $audit_id = $this->Audit_log_model->insert($log_array);	
			  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
			  $update_array = array(
			  	'log_no'	=> $custom_code,
			  );
			  $this->Audit_log_model->update($audit_id, $update_array);			  			  			  
			  
		  }
		  		  		 		  		  
		  //alert
		  if($mode == 'Add'){
			$alert_type = '/1';
		  }else{
			$alert_type = '/2';
		  }
		  
		  		  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage.$alert_type,'refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].$alert_type));
		  }		  
		  
	  }
	  
	  public function assign_customer(){
			
			//all customer
			$this->data['customer'] = json_encode($this->Customers_model->get2());
			
			//all sales employee
			$sales = $this->Employee_model->get_where(array('is_deleted'=>0));
			
			//抓role id 有3=sales 2=sales manager 的 employee
			foreach($sales as $k => $v){
				
				if($v['role_id'] != ''){
					
					if(strpos($v['role_id'],',')){
						$role_id = explode(',',$v['role_id']);	
					}else{
						$role_id = array($v['role_id']);	
					}
					
					if(!in_array(2,$role_id) && !in_array(3,$role_id)){
						unset($sales[$k]);	
					}
					
				}else{
					
					unset($sales[$k]);	
				
				}
				
			}
			
			//排序从0开始 如果不是会影响前台js
			$sales = array_values($sales);
			
			//print_r($sales);exit;
			
			$this->data['sales'] = $sales;
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/agora/manager/assign_customer';
			$this->session->set_userdata("lastpage", $url);
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/assign_customer/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
		
		  
	  }
	  
	  public function assign_customer_submit(){
		  
		  //print_r($_POST);exit;
		  
		  $employee_id 	= $this->input->post("employee_id", true);
		  $customer 	= $this->input->post("customer", true);
		  
		  if($employee_id !='-'){
			  
			  $array = array();
			  if(!empty($customer)){
				  foreach($customer as $v){
					  $array[] = $v;
				  }
			  }
			  
			  $customer_list = implode(',',array_unique($array));
			  $update = array(
			  	'assign_customer' => $customer_list,
			  );
			  
			  $this->Employee_model->update($employee_id,$update);
		  }
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/agora/manager/assign_customer'));
		  }		
		  
	  }
	  
	  public function check_email(){
			
			$email = $_POST['email'];
			$customer_id = $_POST['id'];
			
			$check_val = $this->User_model->getUserByEmail3($email,$customer_id);
			//echo $this->db->last_query();exit;
			$check_val2 = $this->Customers_model->getCustomerByEmail3($email,$customer_id);
			//echo $this->db->last_query();exit;
			
			$email_isset = 0;
			if($check_val != false || $check_val2 != false){
				$email_isset = 1;
			}
			
			$json_array = array(
				'status' 	=> 'ok',
				'email'		=> $email_isset,
			);
			
			echo json_encode($json_array);exit;
			
		  
	  }
	  
	 
}

?>