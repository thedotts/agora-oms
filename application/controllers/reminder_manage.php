<?php

class Reminder_manage extends CI_Controller {

      public function __construct() {
            parent::__construct();  
			$this->load->model('Settings_model');
            $this->load->model('Job_model');
			$this->load->model('Employee_model');
			$this->load->model('User_model');
			
           
      }
   
      public function index() {
			
			//user list
			$user_list = $this->User_model->getIDKeyArray("name");
			
			//all employee
			$all_employee = $this->Employee_model->getAll();
			if(!empty($all_employee)){
				foreach($all_employee as $k => $v){
					if(strpos($v['role_id'],',')){
						$all_employee[$k]['role_id'] = explode(',',$v['role_id']);
					}else{
						$all_employee[$k]['role_id'] = array($v['role_id']);	
					}
				}
			}
			
			//setting的提醒的時間
			$reminder1 = $this->Settings_model->get_settings(26);
			$reminder1 = $reminder1['value'];
		  	$reminder2 = $this->Settings_model->get_settings(27);
			$reminder2 = $reminder2['value'];
			
			//所有remind的job
			$remind_job = $this->Job_model->job_remind($reminder1,$reminder2);
			$remind_job_urgent = $this->Job_model->job_remind_urgent($reminder1,$reminder2);
			
			//main data
			$remind_role_array = array();
			
			//把role相關的role放在array
			if(!empty($remind_job)){
				foreach($remind_job as $k => $v){
					
					//這個job針對的role
					$awaiting_role = $v['awaiting_role_id'];
					if($awaiting_role != ''){
						
						$tmp_role = str_replace(',', '', $awaiting_role);
						
					}
					
					//幫資料對應的role array	
					$remind_role_array[$tmp_role]['normal'][] = $v;
						
					
				}
			}
			
			//把role相關的role放在array(urgent)
			if(!empty($remind_job_urgent)){
				foreach($remind_job_urgent as $k => $v){
					
					//這個job針對的role
					$awaiting_role = $v['awaiting_role_id'];
					if($awaiting_role != ''){
						
						$tmp_role = str_replace(',', '', $awaiting_role);
						
					}
					
					//幫資料對應的role array	
					$remind_role_array[$tmp_role]['urgent'][] = $v;
						
					
				}
			}
			
			
			
			if(!empty($remind_role_array)){
				
				foreach($remind_role_array as $k => $v){
					
						//如果不是sales
						if($k != 3){
						
							//取等於這個role的email
							$email_array = array();
							if(!empty($all_employee)){
								foreach($all_employee as $k2 => $v2){
									
									if(in_array($k,$v2['role_id'])){
										$email_array[] = $v2['email'];
									}
									
								}	
							}
							
							
							$tmp_normal = '';
							$tmp_urgent = '';
							
							//normal
							if(!empty($v['normal'])){
								
								foreach($v['normal'] as $k2 => $v2){
									
									$tmp_normal .= '<tr>
										<th>'.$v2['order_no'].'</th>
										<th>'.$v2['customer_name'].'</th>
										<th>'.$user_list[$v2['user_id']].'</th>
										<th>'.date('d/m/Y H:i:s',strtotime($v2['created_date'])).'</th>
										<th>'.$v2['status_text'].'</th>
									</tr>';
									
								}
								
							}
							
							//urgent
							if(!empty($v['urgent'])){
								
								foreach($v['urgent'] as $k2 => $v2){
									
									$tmp_urgent .= '<tr>
										<th>'.$v2['order_no'].'</th>
										<th>'.$v2['status_text'].'</th>
										<th>'.$user_list[$v2['user_id']].'</th>
										<th>'.date('d/m/Y H:i:s',strtotime($v2['created_date'])).'</th>
										<th>'.$v2['status_text'].'</th>
									</tr>';
									
								}
								
							}
							
							//generate table
							$content_normal ='
								<table cellpadding="5" border="1">
									<thead>
										<tr>
										<th colspan="5">Task</th>
										</tr>
										<tr>
										<th>Order No.</th>
										<th>Company</th>
										<th>Update by</th>
										<th>Update Date/Time</th>
										<th>Status</th>
										</tr>
									</thead>
									'.$tmp_normal.'
									
								</table>
							';
							
							$content_urgent ='
								<table cellpadding="5" border="1">
									<thead>
										<tr>
										<th colspan="5">Task(Urgent)</th>
										</tr>
										<tr>
										<th>Order No.</th>
										<th>Company</th>
										<th>Update by</th>
										<th>Update Date/Time</th>
										<th>Status</th>
										</tr>
									</thead>
									'.$tmp_urgent.'
									
								</table>
							';
							
							$content = '';
							
							if($tmp_normal != ''){
								$content .= $content_normal;
							}
							
							if($tmp_urgent != ''){
								$content .= $content_urgent;
							}
		
							$this->sent_email($content,$email_array);
						
						//end if role !=3
						//sales需要特別處理 只有assign到customer需要send reminder
						}else{
							
							//抓出所有sale的資料
							if(!empty($all_employee)){
								foreach($all_employee as $k2 => $v2){
									
									if(in_array(3,$v2['role_id'])){
										
										if($v2['assign_customer'] != ''){
											
											//assign customer array
											if(strpos($v2['assign_customer'],',')){
												$assign_customer_tmp = explode(',',$v2['assign_customer']);
											}else{
												$assign_customer_tmp = array(
													'0' => $v2['assign_customer'],
												);	
											}
											
											$tmp_normal = '';
											$tmp_urgent = '';
											
											//normal
											if(!empty($v['normal'])){
												
												foreach($v['normal'] as $k3 => $v3){
													
													//如果這個customer id出現在assign customer 裡面就取資料
													if(in_array($v3['customer_id'],$assign_customer_tmp)){
														$tmp_normal .= '<tr>
															<th>'.$v3['order_no'].'</th>
															<th>'.$v3['status_text'].'</th>
															<th>'.$user_list[$v3['user_id']].'</th>
															<th>'.date('d/m/Y H:i:s',strtotime($v3['created_date'])).'</th>
															<th>'.$v3['status_text'].'</th>
														</tr>';
													}
													
												}
												
											}
											
											//urgent
											if(!empty($v['urgent'])){
												
												foreach($v['urgent'] as $k3 => $v3){
													//如果這個customer id出現在assign customer 裡面就取資料
													if(in_array($v3['customer_id'],$assign_customer_tmp)){
														$tmp_urgent .= '<tr>
															<th>'.$v3['order_no'].'</th>
															<th>'.$v3['status_text'].'</th>
															<th>'.$user_list[$v3['user_id']].'</th>
															<th>'.date('d/m/Y H:i:s',strtotime($v3['created_date'])).'</th>
															<th>'.$v3['status_text'].'</th>
														</tr>';
													}
													
												}
												
											}
											
											//generate table
											$content_normal ='
												<table cellpadding="5" border="1">
													<thead>
														<tr>
														<th colspan="5">Task</th>
														</tr>
														<tr>
														<th>Order No.</th>
														<th>Company</th>
														<th>Update by</th>
														<th>Update Date/Time</th>
														<th>Status</th>
														</tr>
													</thead>
													'.$tmp_normal.'
													
												</table>
											';
											
											$content_urgent ='
												<table cellpadding="5" border="1">
													<thead>
														<tr>
														<th colspan="5">Task(Urgent)</th>
														</tr>
														<tr>
														<th>Order No.</th>
														<th>Company</th>
														<th>Update by</th>
														<th>Update Date/Time</th>
														<th>Status</th>
														</tr>
													</thead>
													'.$tmp_urgent.'
													
												</table>
											';
											
											$content = '';
							
											if($tmp_normal != ''){
												$content .= $content_normal;
											}
											
											if($tmp_urgent != ''){
												$content .= $content_urgent;
											}
						
											$this->sent_email_sales($content,$v2['email']);
											
											
											//foreach reminder 資料
											//如果customer id = assign customer 取資料
											//然後sent email
											
										}
										
									}
									
								}	
							}
							
						}
				
				}//end foreach
				
			}
			
			
			
			
			//print_r($remind_role_array);

      }	 
	  
	  
	  private function sent_email($content,$email_array){
		  
	  		//Send Reminder
			$sitename = $this->Settings_model->get_settings(1);
		  	$default_email = $this->Settings_model->get_settings(6);
			
	  		if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agoral.local") {
				
				if(!empty($email_array)){
				
					$this->load->library('email');
				
					foreach($email_array as $k => $v){
						$this->email->clear();
						$this->email->from($default_email['value'], $sitename['value']);
						$this->email->to($v); 
									
						$this->email->subject('Agora Reminder');
						$this->email->message($content);	
						$this->email->set_newline("\r\n");
									
						$this->email->send();
					}
				
				}
							
			}
	  
	  
	  } 
	  
	  
	  private function sent_email_sales($content,$email){
		  
	  		//Send Reminder
			$sitename = $this->Settings_model->get_settings(1);
		  	$default_email = $this->Settings_model->get_settings(6);
			
	  		if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agoral.local") {
				
				$this->load->library('email');
				$this->email->clear();
				$this->email->from($default_email['value'], $sitename['value']);
				$this->email->to($email); 
							
				$this->email->subject('Agora Reminder');
				$this->email->message($content);	
				$this->email->set_newline("\r\n");
							
				$this->email->send();
							
			}
	  
	  
	  } 
	  
	  

}

?>