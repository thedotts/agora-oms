<?php

class Product_category_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();  
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Employee_model');
			$this->load->model('Role_model');
			$this->load->model('Department_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Product_category_model');
			$this->load->model('Audit_log_model');
			
			
            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$userdata = $this->session->userdata("userdata");
				
				//print_r($userdata);exit;
				if(strpos($userdata['role_id'],',')){
					$userdata['role_id'] = explode(',',$userdata['role_id']);	
				}else{
					$userdata['role_id'] = array($userdata['role_id']);	
				}
				
				
				$this->data['userdata'] = $userdata;
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}  
			
			$this->data['role_list'] = $this->Role_model->getIDKeyArray("name");
			$this->data['status_list'] = $this->Employee_model->status_list(false);
			
			$this->data['group_name'] = "administrator";  
			$this->data['model_name'] = "product_category";  
			$this->data['common_name'] = "";   
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			if(in_array(3,$this->data['userdata']['role_id']) ){
				
				
				$a_csutomer = $this->data['staff_info']['assign_customer'];
				
				if($a_csutomer != ''){
				
					if(strpos($a_csutomer,',')){
						$a_customer_array = explode(',',$a_csutomer);	
					}else{
						$a_customer_array = array($a_csutomer);	
					}
				
				}else{
					$a_customer_array = array();	
				}
				
				$this->data['task_display_count'] = $this->Job_model->record_count2(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
				),$this->data['userdata']['role_id'],$a_customer_array);
				
				
				

			}else{
			
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			}
			
			/*
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}
			*/
           
      }
   
      public function index($q="ALL", $page=1, $alert=0) {  
          		
			$this->data['alert'] = $alert;		
            $this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/';
			if($q == 'ALL') {
				$q = "";
			}else{
				$q = urldecode($q);
			}
			$this->data['q'] = urldecode($q);
									
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Product_category_model->record_count($filter, $q);
			
			//get particular ranged list
			$this->data['results'] = $this->Product_category_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start);
						
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->session->set_userdata("lastpage", $url.$page);
			
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  
	  public function add($id=false) {  
	  
          	if($id !== false) {
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Product_category_model->get($id);
			} else {
				$this->data['mode'] = 'Add';	
			}
			
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		            
			
      }	  	
	  
	  public function del($id) {
		  	  
		  $this->Product_category_model->delete($id);
		  
		  //audit log
		  $log_array = array(
			'ip_address'	=> $this->input->ip_address(),
			'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
			'table_affect'	=> 'product_category',
			'description'	=> 'Delete product category',
			'created_date'	=> date('Y-m-d H:i:s'),
		  );
			  
		  $audit_id = $this->Audit_log_model->insert($log_array);	
		  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
		  $update_array = array(
			 'log_no'	=> $custom_code,
		  );
		  $this->Audit_log_model->update($audit_id, $update_array);
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage.'/3','refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/3'));
		  }		
		  
	  }  
	  
	  public function submit(){
		  
		  $mode = $this->input->post("mode", true);
		  $id = $this->input->post("id", true);
		  $category_name = $this->input->post("category_name", true);
		  $description = $this->input->post("description", true);
		  $now = date("Y-m-d H:i:s");
		  
		  $array = array(
		  	'name'						=> $category_name,
			'description'				=> $description,
		  );

		  //Add
		  if($mode == 'Add') {			  			  
		  	  
			  $array['created_date'] = $now;
			  $this->Product_category_model->insert($array);
			  
			  //audit log
			  $log_array = array(
				'ip_address'	=> $this->input->ip_address(),
				'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
				'table_affect'	=> 'product_category',
				'description'	=> 'Added product category',
				'created_date'	=> date('Y-m-d H:i:s'),
			  );			
			  
			  $audit_id = $this->Audit_log_model->insert($log_array);	
			  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
			  $update_array = array(
				 'log_no'	=> $custom_code,
			  );
			  $this->Audit_log_model->update($audit_id, $update_array);  		  	  			  
			  
		  //Edit	  			  
		  } else {
			  
			  $array['modified_date'] = $now;	  
			  $this->Product_category_model->update($id, $array);
			  
			  //audit log
			  $log_array = array(
				'ip_address'	=> $this->input->ip_address(),
				'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
				'table_affect'	=> 'product_category',
				'description'	=> 'Edit product category',
				'created_date'	=> date('Y-m-d H:i:s'),
			  );
			  
			  $audit_id = $this->Audit_log_model->insert($log_array);	
			  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
			  $update_array = array(
				 'log_no'	=> $custom_code,
			  );
			  $this->Audit_log_model->update($audit_id, $update_array);
			  
		  }
		  		  		 		  		  
		  //alert
		  if($mode == 'Add'){
			$alert_type = '/1';
		  }else{
			$alert_type = '/2';
		  }
		  
		  		  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage.$alert_type,'refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].$alert_type));
		  }		  
		  
	  }
	  
	  public function check_category(){
			
			$category_name = $_POST['category_name'];
			$id = $_POST['id'];
			
			$check_val = $this->Product_category_model->check_category($category_name,$id);
			
			$isset = 0;
			if($check_val != false){
				$isset = 1;
			}
			
			$json_array = array(
				'status' 	=> 'ok',
				'email'		=> $isset,
			);
			
			echo json_encode($json_array);exit;
			
		  
	  }
	  

}

?>