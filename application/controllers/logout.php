<?php
class Logout extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('cookie');
	}
	
	function index()
	{
		$this->session->unset_userdata("userdata");		
		//$this->session->session_destroy();                
        setcookie("chl_token", "", time()-3600); //destroy cookie
		//del cart
		delete_cookie('Cart');
		//setcookie("Cart", "", time()-3600); //destroy cart;

		redirect('/en/login', 'refresh');
	}
}