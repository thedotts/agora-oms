<?php

class Audit_log_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();  
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Employee_model');
			$this->load->model('Role_model');
			$this->load->model('Department_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Audit_log_model');
			
			
            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$userdata = $this->session->userdata("userdata");
				
				//print_r($userdata);exit;
				if(strpos($userdata['role_id'],',')){
					$userdata['role_id'] = explode(',',$userdata['role_id']);	
				}else{
					$userdata['role_id'] = array($userdata['role_id']);	
				}
				
				
				$this->data['userdata'] = $userdata;
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}  
			
			$this->data['role_list'] = $this->Role_model->getIDKeyArray("name");
			$this->data['status_list'] = $this->Employee_model->status_list(false);
			
			$this->data['user_list'] = $this->User_model->name_list();
			
			$this->data['group_name'] = "administrator";  
			$this->data['model_name'] = "audit_log";  
			$this->data['common_name'] = "";   
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			if(in_array(3,$this->data['userdata']['role_id'])){
				
				
				$a_csutomer = $this->data['staff_info']['assign_customer'];
				
				if($a_csutomer != ''){
				
					if(strpos($a_csutomer,',')){
						$a_customer_array = explode(',',$a_csutomer);	
					}else{
						$a_customer_array = array($a_csutomer);	
					}
				
				}else{
					$a_customer_array = array();	
				}
				
				$this->data['task_display_count'] = $this->Job_model->record_count2(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
				),$this->data['userdata']['role_id'],$a_customer_array);
				
				
				

			}else{
			
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			}
			
			/*
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}
			*/
           
      }
   
      public function index($start_date='ALL',$end_date='ALL',$q="ALL", $page=1) {
				
			$this->data['page'] = $page;
			$this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'audit_log.is_deleted'	=> 0,			
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$start_date.'/'.$end_date.'/'.$q.'/';
			if($q == 'ALL') {
				$q = "";
			}else{
				$q = urldecode($q);
			}
			
			if($start_date == 'ALL') {
				$start_date = "";
			}else{
				$start_date = str_replace("-", "/", $start_date);
			}
			
			if($end_date == 'ALL') {
				$end_date = "";
			}else{
				$end_date = str_replace("-", "/", $end_date);
			}
			
			
			$this->data['start_date'] = $start_date;
			$this->data['end_date'] = $end_date;
			$this->data['q'] = urldecode($q);
									
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Audit_log_model->record_count($filter, $q,$start_date,$end_date);
			
			//get particular ranged list
			$this->data['results'] = $this->Audit_log_model->fetch($filter, $q,$start_date,$end_date, $this->data['item_per_page'], $limit_start);
						
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->session->set_userdata("lastpage", $url.$page);
					
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  
	  public function add($id=false) {  
	  
          	if($id !== false) {
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Department_model->get($id);
			} else {
				$this->data['mode'] = 'Add';	
			}
			
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		            
			
      }	  	
	  
	  public function del($id) {
		  	  
		  $this->Department_model->delete($id);
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		
		  
	  }  
	  
	  public function submit(){
		  
		  $mode = $this->input->post("mode", true);
		  $id = $this->input->post("id", true);
		  $department_name = $this->input->post("department_name", true);
		  $now = date("Y-m-d H:i:s");
		  
		  $array = array(
		  	'name'						=> $department_name,
		  );

		  //Add
		  if($mode == 'Add') {			  			  
		  	  
			  $array['created_date'] = $now;
			  $this->Department_model->insert($array);			  		  	  			  
			  
		  //Edit	  			  
		  } else {
			  
			  $array['modified_date'] = $now;	  
			  $this->Department_model->update($id, $array);
			  
		  }
		  		  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		  
		  
	  }
	  
	  
	  public function export_data($start_date='ALL',$end_date='ALL',$q="ALL") {  
          		
			//Filter						
			$filter = array(
			 	'audit_log.is_deleted'	=> 0,			
			);	
			
			if($q == 'ALL') {
				$q = "";
			}
			
			if($start_date == 'ALL') {
				$start_date = "";
			}else{
				$start_date = str_replace("-", "/", $start_date);
			}
			
			if($end_date == 'ALL') {
				$end_date = "";
			}else{
				$end_date = str_replace("-", "/", $end_date);
			}
			
			$data = $this->Audit_log_model->export_data($filter,$q,$start_date,$end_date);
			$export_data = $data;
				
			//print_r($export_data);exit;
									
			$this->export($export_data);
			
      }	
	  
	  
	  public function download_send_headers($filename) {
            // disable caching
            $now = gmdate("D, d M Y H:i:s");
            header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
            header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
            header("Last-Modified: {$now} GMT");

            // force download  
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");

            // disposition / encoding on response body
            header("Content-Disposition: attachment;filename={$filename}");
            header("Content-Transfer-Encoding: binary");
        }
        
      public function array2csv(array &$array) {
           if (count($array) == 0) {
             return null;
           }
           ob_start();
           $df = fopen("php://output", 'w');
           fputcsv($df, array_keys(reset($array)));
           foreach ($array as $row) {
			  $row['log_no'] = '\''.$row['log_no'];
              fputcsv($df, $row);
           }
           fclose($df);
           return ob_get_clean();
      }
	  
	  public function export($data){                        
            
            $this->download_send_headers("Audit_log_export_" . date("YmdHis") . ".csv");
            echo $this->array2csv($data);
            die();
      }  
	  

}

?>