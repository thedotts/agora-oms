<?php

class Service_request_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();  
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Customers_model');
			$this->load->model('Service_request_model');
			$this->load->model('Service_request_item_model');
			$this->load->model('Quotation_model');
			$this->load->model('Employee_model');
			$this->load->model('Department_model');
			$this->load->model('Quotation_item_model');
			$this->load->model('Products_model');
			$this->load->model('Suppliers_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Settings_model');
			$this->load->model('Workflow_model');
			$this->load->model('Workflow_order_model');

            $this->data['init'] = $this->function_model->page_init();
			
			//get status list
			$sr_status_list = $this->Workflow_model->get(4);
			$this->data['sr_status_list'] = json_decode($sr_status_list['status_json']);
			
			
			$this->data['role_list'] = $this->Role_model->getIDKeyArray('name');
			$this->data['status_list'] = $this->Service_request_model->status_list();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$this->data['userdata'] = $this->session->userdata("userdata");
			}   
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
				  
			//echo $this->data['userdata']['role_id'];exit;
			$this->data['group_name'] = "sales";  
			$this->data['model_name'] = "service_request";  
			$this->data['common_name'] = "Service Request";   
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
           
		   //permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			} 
			
			define('SUPERADMIN', 1);
			define('GENERAL_MANAGER', 2);
			define('SERVICE_MANAGER', 3);
		    define('LOGISTIC', 6);
			define('FINANCE', 5);
		    define('SALES_MANAGER', 7);
		    define('SALES_EXECUTIVE', 8);	
		   
      }
   
      public function index($q="ALL", $status="ALL", $page=1) {  
          			
            $this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/anexus/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/'.$status.'/';
			if($q == 'ALL') {
				$q = "";
			}
			$this->data['q'] = $q;
			
			if($status != 'ALL') {
				$filter['status'] = $status;	
			}
			$this->data['status'] = $status;			
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Service_request_model->record_count($filter, $q);
			
			//get particular ranged list
			$this->data['results'] = $this->Service_request_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start);
			
			if(!empty($this->data['results'])){
				foreach($this->data['results'] as $k => $v){
					if(!empty($v['awaiting_table'])){
							$this->data['results'][$k]['awaiting_table']= explode(',',$v['awaiting_table']);
					}
				}
			}
						
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->data['customer_list'] = $this->Customers_model->getIDKeyArray("company_name");
			$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
			
			$this->session->set_userdata("lastpage", $url.$page);
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  
	  public function add($id=false,$job_id=false) {  
          	
			$role_id = $this->data['userdata']['role_id'];
			
			if($id !== false && $job_id===false) {
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Service_request_model->get($id);	
				$this->data['department_list'] = $this->Department_model->getIDKeyArray("name");
				
				$this->data['getRelatedService'] = $this->Service_request_item_model->getRelatedService($id);
				
				if(count($this->data['getRelatedService'])>0){
					$this->data['company_name'] = $this->data['getRelatedService'] [0]['company_name'];
					$this->data['pic'] = $this->data['getRelatedService'] [0]['person_in_charge'];
					$this->data['purchase_order_reg'] = $this->data['getRelatedService'] [0]['purchase_order_reg'];
				}
				
				$this->data['getRelatedService'] = json_encode($this->data['getRelatedService']);
				
				
				$this->data['result']['awaiting_table'] = explode(',',$this->data['result']['awaiting_table']);
				
				$requestor_employee = $this->Employee_model->get($this->data['result']['requestor_id']);
			    $requestor_role = $requestor_employee['role_id'];
				
				$default = 0;
				
				//////////////////////////////////////////////////////////////////////////////////
				
				   		//get related workflow order data
						$workflow_flitter = array(
							'status_id' => $this->data['result']['status'],
							'workflow_id' => 4,
						);
						
						$type='';
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$type = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$type = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
							$type = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
									$type = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(4);
						$status = json_decode($workflow['status_json'],true);
						
						$this->data['workflow_title'] = $workflow['title'];
					
					$this->data['btn_type'] = $type;
					//print_r($workflow_order);exit;

					switch($type){
						case 'edit':
							$this->data['head_title'] = 'Edit '.$workflow['title'].' Request';
						break;
						case 'approval':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
						break;
						case 'approval_update':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'update':
							$this->data['head_title'] = 'Update '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'send_email':
							$this->data['head_title'] = 'Waiting to Send '.$workflow['title'].' Request';
						break;
						case 'sendmail_update':
							$this->data['head_title'] = 'Waiting to Send/Edit '.$workflow['title'].' Request';
						break;
						case 'upload':
							$this->data['head_title'] = 'Upload Confirm File '.$workflow['title'].' Request';
						break;
						case 'complete':
							$this->data['head_title'] = $workflow['title'].' Request';
							$this->data['last_action'] = $workflow_order['action_before_complete'];
						break;
						default:
							$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					foreach(json_decode($workflow['status_json'],true) as $k => $v){
						
						if($v == 'Completed'){
							$this->data['completed_status_id'] = $k;
							break;
						}
					}
				//////////////////////////////////////////////////////////////////////////////////////
				
				/*
				//waiting sm approve
				if($this->data['result']['status'] == 1){
					
					//se edit
					if($role_id == 4){
					$this->data['head_title'] = 'Edit Service Request';
					//sm comfirm
					}else if($role_id == 3){
					$this->data['head_title'] = 'Confirm Service Request';
					}else{
					$default = 1;
					}
				
				//waiting gm approve
				}else if($this->data['result']['status'] == 2){
					
					//sm edit
					if($role_id == 3){
					$this->data['head_title'] = 'Edit Service Request';
					//gm comfirm
					}else if($role_id == 2){
					$this->data['head_title'] = 'Confirm Service Request';
					}else{
					$default = 1;
					}
					
				//finance approve
				}else if($this->data['result']['status'] == 3){
					
					//sr edit
					if($role_id == 5){
					$this->data['head_title'] = 'Confirm Service Request';
					}else{
					$default = 1;
					}
					
				}else{
					$default = 1;
				}
				
				//no related person
				if($default == 1){
					$this->data['head_title'] = 'Service Request';	
				}
				*/						
				
			} else {
				$this->data['mode'] = 'Add';	
				$this->data['head_title'] = 'Create Service Request';	
				
				//get related workflow order data
					$workflow_flitter = array(
						'role_id' => $role_id,
						'status_id' => -1,
						'workflow_id' => 4,
					);
					
					//data form database
					$workflow = $this->Workflow_model->get(4);
					$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
					
					//get view bottom layout
					$type = '';
					if(!empty($workflow_order)){
						$type = $workflow_order['action'];
						if($workflow_order['action'] == 'create'){
							$this->data['head_title'] = 'Create '.$workflow['title'].' Request';
						}else{
							$this->data['head_title'] = $workflow['title'].' Request';
						}
					}else{
						$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					$this->data['btn_type'] = $type;
					
					//auto select quotation id
					$this->data['qr_id'] = 0;
					if($id !== false){
					$this->data['qr_id'] = $id;
					}
					//for complete the job
					$this->data['job_id'] = $job_id;
				
			}
			
			
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		
            
			
      }	  	  
	  
	  public function submit(){
		  
		  $now = date("Y-m-d H:i:s");
		  $mode = $this->input->post("mode", true);
		  $id = $this->input->post("id", true);
		  
		  $role_id = $this->data['userdata']['role_id'];
		  
		  //get value approve or not approve
		  $confirm = $this->input->post("confirm", true);
		  $correct_check = $this->input->post("correct_check", true);
		  
		  $itemCounter = $this->input->post("addCounter", true);
		  		  
		  $dateRequisition = $this->input->post("dateRequisition", true);
			if(!empty($dateRequisition)) {
			  $tmp = explode("/", $dateRequisition);
			  $dateRequisition = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
		  	}
			
		  if($mode == 'Add'){
			  
			  $customer_id = $this->input->post("customer_id", true);
		  	  
		  }else{
		  
			  //sr
			  $sr_data = $this->Service_request_model->get($id);
		  	  $customer_id = $sr_data['customer_id'];
			  
		  }	
			
			
			//job
		  	$jobArray = array(
				'parent_id' =>0,
				'user_id' =>$this->data['userdata']['id'],
				'customer_id' =>$customer_id,
				'type' => 'Service Request',
				'type_path' =>'sales/service_request_edit/',
				'created_date'	=> $now,
				'modified_date'	=> $now,
		 	 );
			 
			//main		
			$array = array(
				'customer_id' => $customer_id,
				'customer_name' => $this->input->post("customer_name", true),
				'requesting_date' => $dateRequisition,
				'purchase_order_ref' => $this->input->post("purchase_order_ref", true),
				'sale_order_ref' => $this->input->post("salesOrderRef", true),
				'requestor_id' => 0,
				'employee_ref' => $this->input->post("employeeRef", true),
				'department_id' => $this->input->post("departmentId", true),
				'service_request_form' => $this->input->post("service-request", true),
				'consignee' => $this->input->post("consignee", true),
				'uncrating_service_required' => $this->input->post("uncrating", true),
				'requestor_id' => $this->input->post("requestor_id", true),
			);	 
			
			//mail
			$mail_array = array(
		  	'creater_name' =>$this->data['userdata']['name'],
		  	);		 
		  	  
		  //print_r($array);exit;
		  	  
		  //Add
		  if($mode == 'Add') {			  			  
			  	
				////////////////////////////////////////////////////////////////////////////////////
				
				//get related workflow order data
				$workflow_flitter = array(
					'role_id' => $role_id,
					'status_id' => -1,
					'workflow_id' => 4,
				);
				
				//data form database
				$workflow = $this->Workflow_model->get(4);
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
				$status = json_decode($workflow['status_json'],true);
				
				if(!empty($workflow_order)){
				
				//ad_hoc
				if($workflow_order['ad_hoc'] == 1){
					$array['ad_hoc'] = 1;
				}
				
				//purchaser
				$is_adhoc = false;
				if($role_id == 10){
					$is_adhoc = true;
				}
				
				//next status
				$next_status = $workflow_order['next_status_id'];
				$next_status_text = $status[$workflow_order['next_status_id']];
				
			  	//if need_det = 1
				if($workflow_order['need_det'] == 1){
					
					$formula = json_decode($workflow_order['formula'],true);
					
					//check if product or service price over value
					foreach($formula as $k => $v){
					
						$price = $this->input->post($v['target_colum'], true);
						$det_temp = $price." ".$v['logic']." ".$v['value'];
						$check = $this->parse_boolean($det_temp);
						
						//check price over value
						if($check){
							$next_status = $v['next_status_id'];
							$next_status_text = $status[$v['next_status_id']];
							break;
						}
						
					}
					
				}
				
				//get next status workflow order data
				$next_workflow_flitter = array(
					'status_id' => $next_status,
					'workflow_id' => 4,
				);
				
				//next status data
				$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
				$awating_person = $next_status_data['role_id'];
				
				
				
				if($awating_person == 'requestor'){
					$awating_person = $this->input->post("requestor_id", true);
				}
				//echo $awating_person;exit;
				
				//main
				$array['awaiting_table']  = $awating_person;
				$array['status'] = $next_status;
				//main
				$jobArray['awaiting_role_id'] = $awating_person.',';
				$jobArray['status_id'] = $next_status;
				$jobArray['status_text'] = $next_status_text; 	
				  
				  //insert main data
					$array['created_date'] = $now;
					$array['modified_date'] = $now;
					$array['create_user_id'] = $this->data['userdata']['id'];
					$array['lastupdate_user_id'] = $this->data['userdata']['id'];

					//新增資料
					$insert_id = $this->Service_request_model->insert($array);					
					
					//更新purchase serial
					$service_serial = $this->Service_request_model->zerofill($insert_id, 5);
					
					//insert job
					$jobArray['serial'] = $service_serial;
					$jobArray['type_id'] = $insert_id;
					
					$jobArray['created_date'] = $now;
					$jobArray['modified_date'] = $now;
					
					$job_id = $this->Job_model->insert($jobArray);
				  
					//after insert job,update job id,qr serial....
					$array =array(
						'job_id'	=> $job_id,
						'service_serial'  => $service_serial,
						'latest_job_id'	=> $job_id,
					);
					$this->Service_request_model->update($insert_id, $array);
					
				  //mail
				  $related_role = $this->Job_model->getRelatedRoleID($job_id);
				  $mail_array['related_role'] = $related_role;		
				  $mail_array['title'] = $workflow['title']." ".$service_serial." ".$next_status_text; 
				  
				  //task of auto create job quotation completed if job id != ''
				  $task_qr_job_id = $this->input->post('job_id',true);
				  if($task_qr_job_id != ''){
				  $this->Job_model->update($task_qr_job_id, array(
				  	'is_completed' => 1,
					'display' => 0));
				  }
				  	
				}
				
				////////////////////////////////////////////////////////////////////////////////////
				
				
				/*
				//create by service executive
			  	if($this->data['userdata']['role_id'] == SALES_EXECUTIVE){
					
			  		//main
					$array['awaiting_table']  = SALES_MANAGER;
					$array['status']  = 1;
					//job
					$jobArray['awaiting_role_id'] = SALES_MANAGER.',';
					$jobArray['status_id'] = 1;
					$jobArray['status_text'] = $this->data['sr_status_list'][1];
					//mail
					$mail_array['related_role'] = array(SALES_EXECUTIVE,SALES_MANAGER);
					
				//create by service manager
				}else if($this->data['userdata']['role_id'] == SALES_MANAGER){
					
					//main
					$array['awaiting_table']  = GENERAL_MANAGER;
					$array['status']  = 2;
					//job
					$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
					$jobArray['status_id'] = 2;
					$jobArray['status_text'] = $this->data['sr_status_list'][2];
					//mail
					$mail_array['related_role'] = array(SALES_MANAGER,GENERAL_MANAGER);
					
				}
			  
			  $mail_array['title'] = 'create service request';
			  
			  $array['created_date'] = $now;
			  $array['create_user_id'] = $this->data['userdata']['id'];
			  $array['modified_date'] = $now;
			  $array['lastupdate_user_id'] = $this->data['userdata']['id'];
			  
			  
			  $insert_id = $this->Service_request_model->insert($array);	
			  $service_serial = $this->Service_request_model->zerofill($insert_id, 5);
				
				//insert job
				$jobArray['serial'] =$service_serial;
			    $jobArray['type_id'] =$insert_id;
			    $job_id = $this->Job_model->insert($jobArray);
			  
			    //after insert job,update job id,qr serial....
			    $array =array(
			  	'job_id'	=> $job_id,
			  	'service_serial'  => $service_serial,
				'latest_job_id'	=> $job_id,
			    );
			    $this->Service_request_model->update($insert_id, $array);	
			  */
			  
		  //Edit	  			  
		  } else {
			  
			    $sr_data = $this->Service_request_model->get($id);
			  	$sr_status = $sr_data['status'];
			  
			  	$requestor_employee = $this->Employee_model->get($sr_data['requestor_id']);
			  	$requestor_role = $requestor_employee['role_id'];
				
			  	$related_roles = $this->Job_model->getRelatedRoleID($sr_data['latest_job_id']);	
			  
			  	$jobArray['serial'] =$sr_data['service_serial'];
			  	$jobArray['type_id'] =$sr_data['id'];
			  	$jobArray['parent_id'] = $sr_data['latest_job_id'];
				
				$next = 0;
			  	$last = 0;
			  	$status_change = 0;
				
				/////////////////////////////////////////////////////////////////////////////////
			  
			  //can edit status
			  $edit_status = $this->Workflow_order_model->get_status_edit(4);
			  
			  //requestor edit
			  if(in_array($sr_status, $edit_status) && $role_id == $requestor_role){
			  	  
						//get related workflow order data
						$workflow_flitter = array(
							'role_id' => $role_id,
							'status_id' => -1,
							'workflow_id' => 4,
						);
						
						//data form database
						$workflow = $this->Workflow_model->get(4);
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						$status = json_decode($workflow['status_json'],true);
						
						if(!empty($workflow_order)){
						
						//next status
						$next_status = $workflow_order['next_status_id'];
						$next_status_text = $status[$workflow_order['next_status_id']];
						
						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];
								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						//get next status workflow order data
						$next_workflow_flitter = array(
							'status_id' => $next_status,
							'workflow_id' => 4,
						);
						
						//next status data
						$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
						$awating_person = $next_status_data['role_id'];
						
						//if awating person equal requestor ,asign requestor for it
						if($awating_person == 'requestor'){
							$awating_person = $requestor_role;
						}
						
						//main
						$array['awaiting_table']  = $awating_person;
						$array['status'] = $next_status;
						
						//job
						$jobArray['awaiting_role_id'] = $awating_person.',';
						$jobArray['status_id'] = $next_status;
						$jobArray['status_text'] = $next_status_text;
						
						
						}
						
				  
			  }else{
				  		
												
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $sr_status,
							'workflow_id' => 4,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(4);
						$status = json_decode($workflow['status_json'],true);
						
						//print_r($workflow_order);exit;
						if(isset($action)){
							
						
						
						//action
						switch ($action) {
							case 'approval':
							
								//clear data infront
								$array = array();
							
								//approved
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];

										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										//clear data infront
										$array = array();
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'approval_sendmail':
							
								//clear data infront
								$array = array();
							
								//approved
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];

										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
										
										//sent mail to service manager
										$role_service_manager = $this->Employee_model->get_related_role(array($workflow_order['who_email']));
										$sitename = $this->Settings_model->get_settings(1);
										$default_email = $this->Settings_model->get_settings(6);
										
										$this->load->library('email');
										
										if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
											foreach($role_service_manager as $k => $v){
												
												$this->email->clear();
												$this->email->from($default_email['value'], $sitename['value']);
												$this->email->to($v['email']); 
												$this->email->subject('Service request completed');
												$this->email->message($sr_data['service_serial'].' Service request had completed');
												$this->email->send();
											  
											}
										}
										
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'update':
								
								//next status
								$next_status = $workflow_order['next_status_id'];
								$next_status_text = $status[$workflow_order['next_status_id']];
								
								$array = array(
								);
								
								//check last job
								if($workflow_order['next_status_text'] == 'Completed'){
									$last = 1;
								}
								
								
								$next = 1;
								
								break;
							case 'approval_update':
								
								//clear data infront
								$array = array();
								
								if($confirm == 1){
									
									if($correct_check == 'on'){
										
										//next status
										$next_status = $workflow_order['next_status_id'];
										$next_status_text = $status[$workflow_order['next_status_id']];
										
										$target_colum = json_decode($workflow_order['target_colum'],true);
										
										//check update colum single or multiple
										$array = array();
										foreach($target_colum as $k => $v){
										
											if($v['type'] == 'single'){
												
												if($v['data_type'] == 'string'){
													
												$array[$v['name']] = $this->input->post($v['name'],true);
												
												}else if($v['data_type'] == 'date'){
													
													$date = $this->input->post($v['name'], true);
													if(!empty($date)) {
													  $tmp = explode("/", $date);
													  $date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
													  $array[$v['name']] = $date;
													}
													
												}
											
											}else{
												
											}
											
										}
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
										$next = 1;
								
									}
									
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
							case 'upload':
							
								//clear data infront
								$array = array();
								
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'comfirm_file'	=> $save_path,
											);	
											
											//next status
											$next_status = $workflow_order['next_status_id'];
											$next_status_text = $status[$workflow_order['next_status_id']];	
											$next = 1;
											//check last job
											if($workflow_order['next_status_text'] == 'Completed'){
												$last = 1;
											}												
									}	
									
								}
								
								break;	
							case 'complete':
							
								//clear data infront
								$array = array();
							
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'comfirm_file'	=> $save_path,
											);	
										
									}	
								}
								
								break;	
								
						}
						
						//print_r($workflow_order);exit;
						//if need_det = 1
						
						if($workflow_order['need_det'] == 1){

							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$value = $this->input->post($v['target_colum'], true);
								//string or number
								if(is_numeric($v['value'])){
									$det_temp = $value." ".$v['logic']." ".$v['value'];
								}else{
									
									if($v['value'] == 'original'){
										$string = $sr_data['term_condition'];
									}else{
										$string = $v['value'];
									}
									
									$det_temp = "'".md5(trim($value))."' ".$v['logic']." '".md5(trim($string))."'";
								}
								
								
								$check = $this->parse_boolean($det_temp);
								
								//check
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
							}
							
						}

						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 4,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
						}
						
						}
						
				  
			  }
			  
			 	if(isset($next_status)){
					//main
					$array['awaiting_table']  = $awating_person;
					$array['status'] = $next_status;
					//job
					$jobArray['awaiting_role_id'] = $awating_person.',';
					$jobArray['status_id'] = $next_status;
					$jobArray['status_text'] = $next_status_text;
				}
				//print_r($array);exit;
			  //////////////////////////////////////////////////////////////////////////////////////
				
				
				/*
				//判断status 给予下个job
				if($sr_status == 1){
					
					//se edit	
					if($role_id == SALES_EXECUTIVE){
						
						$array['awaiting_table']  = SALES_MANAGER;
						$array['status']  = 1;
						//job
						$jobArray['awaiting_role_id'] = SALES_MANAGER.',';
						$jobArray['status_id'] = 1;
						$jobArray['status_text'] = $this->data['sr_status_list'][1];
						//mail
						$mail_array['related_role'] = array(SALES_MANAGER,SALES_EXECUTIVE);
						$mail_array['title'] = $sr_data['service_serial'].' '.$this->data['sr_status_list'][1];
						
					//sm approved/denied
					}else if($role_id == SALES_MANAGER){
						
						//sm manager denied
						if($confirm == 0){
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	//mail
							$mail_array['related_role'] = array(SALES_MANAGER,SALES_EXECUTIVE);
							$mail_array['title'] = $sr_data['service_serial'].' '.$this->data['sr_status_list'][0];
						  
						  	$next = 1;
						  	$last = 1;	
						//sm maneger approved
						}else if($correct_check == 'on'){
							
							$array['awaiting_table']  = FINANCE;
							$array['status']  = 3;
							$jobArray['awaiting_role_id'] = FINANCE.',';
							$jobArray['status_id'] = 3;
							$jobArray['status_text'] = $this->data['sr_status_list'][3];
							
							$related_roles[] = FINANCE;
							//mail
							$mail_array['related_role'] = $related_roles;
							$mail_array['title'] = $sr_data['service_serial'].' '.$this->data['sr_status_list'][3];
							
							$next = 1;
						}
						
					}
				//general approved/denied	
				}else if($sr_status == 2){
					
					//sm edit	
					if($role_id == SALES_MANAGER){
						
						$array['awaiting_table']  = GENERAL_MANAGER;
						$array['status']  = 2;
						//job
						$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
						$jobArray['status_id'] = 2;
						$jobArray['status_text'] = $this->data['sr_status_list'][2];
						//mail
						$mail_array['related_role'] = array(GENERAL_MANAGER,SALES_MANAGER);
						$mail_array['title'] = $sr_data['service_serial'].' '.$this->data['sr_status_list'][2];
						
					//gm approved/denied
					}else if($role_id == 2){
												
						//sm manager denied
						if($confirm == 0){
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	//mail
							$mail_array['related_role'] = array(SALES_MANAGER,GENERAL_MANAGER);
							$mail_array['title'] = $sr_data['service_serial'].' '.$this->data['sr_status_list'][0];
						  
						  	$next = 1;
						  	$last = 1;	
						//sm maneger approved
						}else if($correct_check == 'on'){
							
							$array['awaiting_table']  = FINANCE;
							$array['status']  = 3;
							$jobArray['awaiting_role_id'] = FINANCE.',';
							$jobArray['status_id'] = 3;
							$jobArray['status_text'] = $this->data['sr_status_list'][3];
							
							$related_roles[] = FINANCE;
							//mail
							$mail_array['related_role'] = $related_roles;
							$mail_array['title'] = 'Approved service request';
							
							$next = 1;
						}
						
					}
					
				//finance approved/denied	
				}else if($sr_status == 3 && $role_id == FINANCE){
					
						
						//sm manager denied
						if($confirm == 0){
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	//mail
							$mail_array['related_role'] = $related_roles;
							$mail_array['title'] = $sr_data['service_serial'].' '.$this->data['sr_status_list'][0];
						  
						  	$next = 1;
						  	$last = 1;	
						//sm maneger approved
						}else if($correct_check == 'on'){
							
							$array['awaiting_table']  = '';
							$array['status']  = 4;
							$jobArray['awaiting_role_id'] = '';
							$jobArray['status_id'] = 4;
							$jobArray['status_text'] = $this->data['sr_status_list'][4];
							
							//mail
							$mail_array['related_role'] = $related_roles;
							$mail_array['title'] = $sr_data['service_serial'].' '.$this->data['sr_status_list'][4];
							
							$next = 1;
							$last = 1;
							
							//sent mail to service manager
							$role_service_manager = $this->Employee_model->get_related_role(array(SERVICE_MANAGER));
							$sitename = $this->Settings_model->get_settings(1);
		  					$default_email = $this->Settings_model->get_settings(6);
							
							$this->load->library('email');
							
							if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
								foreach($role_service_manager as $k => $v){
									
									$this->email->clear();
									$this->email->from($default_email['value'], $sitename['value']);
									$this->email->to($v['email']); 
									$this->email->subject('Service request completed');
									$this->email->message($sr_data['service_serial'].' Service request had completed');
									$this->email->send();
								  
								}
							}
						
							
							
						}
							
						
				}
				*/
				
				$array['modified_date'] = $now;
				$array['lastupdate_user_id'] = $this->data['userdata']['id'];
				
				
				if($next == 1){
				  $lastjob_update = array(
				  	'is_completed' => 1,
					'display' => 0,
				  );
				  $this->Job_model->update($jobArray['parent_id'], $lastjob_update);
				  if($last == 0){
				  	$new_job_id = $this->Job_model->insert($jobArray);
				  	$array['latest_job_id'] = $new_job_id;
				  }
			  	}	  
			    
				$this->Service_request_model->update($id, $array);
				$insert_id = $id;

				//mail
				  if(isset($new_job_id)){
				  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
				  $mail_array['related_role'] = $related_role;		
				  $mail_array['title'] = $workflow['title']." ".$sr_data['service_serial']." ".$next_status_text;
				  }
		  }
		  
		  $item_type = 0;
		  if($mode == 'Add'){
				
				$item_type = 1;
			
		  }else if($mode == 'Edit' && $role_id == $requestor_role){
				
				if($sr_status < 3 && $sr_status != 0){
					$item_type = 1;
				}
					
		  }
		  
		  if($item_type == 1){
		  if($itemCounter>0){
				
				for($i=0;$i<$itemCounter;$i++){
					$startDate = '';
					if(!empty($_POST['startDate'.$i])) {
					  $tmp = explode("/", $_POST['startDate'.$i]);
					  $startDate = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
					}
					
					$itemData = array(
						'service_request_id' =>$insert_id ,
						'company_name'=>$_POST['companyName'],
						'person_in_charge'=>$_POST['pic'],
						'purchase_order_reg'=>$_POST['orderReg'],
						'product_id'=>isset($_POST['product_id'.$i])?$_POST['product_id'.$i]:'',
						'quantity'=>$_POST['quantity'.$i],
						'part_no'=>$_POST['partNo'.$i],
						'description'=>$_POST['description'.$i],
						'hours'=>$_POST['hour'.$i],
						'startdate'=>$startDate,
						'hours_included'=>$_POST['hours_included'.$i],
					);
					
					//print_r($itemData);exit;
				
				
				//有deleted 又有old_id 代表要刪除資料
					if(isset($_POST['is_deleted'.$i]) && !empty($_POST['is_deleted'.$i]) && isset($_POST['old_id'.$i]) && !empty($_POST['old_id'.$i])){
						
						$this->Service_request_item_model->delete($_POST['old_id'.$i]);
					//沒有deleted 但有old_id 代表要更新這個資料
					} else if ( isset($_POST['is_deleted'.$i]) && empty($_POST['is_deleted'.$i]) && isset($_POST['old_id'.$i]) && !empty($_POST['old_id'.$i])) {
						$itemData['modified_date'] =$now;
						$this->Service_request_item_model->update($_POST['old_id'.$i], $itemData);
					//沒有deleted 也沒有old_id 代表要新增這個資料	
					} else if ( isset($_POST['is_deleted'.$i]) && empty($_POST['is_deleted'.$i]) && isset($_POST['old_id'.$i]) && empty($_POST['old_id'.$i])){
						//echo '1';exit;
						//至少要有product_id才能insert
						if(!empty($itemData['part_no'])) {
							
							$itemData['created_date'] =$now;
							$this->Service_request_item_model->insert($itemData);
						}
						
					}
				}
			}
		  }
		  
		  
		  //sent mail
			if(isset($mail_array['related_role'])){
			
				$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
				$sitename = $this->Settings_model->get_settings(1);
				$default_email = $this->Settings_model->get_settings(6);
				
				$this->load->library('email');
				
				if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
							
					foreach($related_role_data as $k => $v){
					$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
					
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->send();	
					  
					}
				
				}
			
			}
			
		  
			
			
		  
		  		  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		  
		  
	  }
	  
	  public function del($id){
		  $this->Service_request_model->delete($id);
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		
	  } 
	  
	  public function ajax_getQuotation(){
		  
		  	$keyword = $_POST['keyword'];
			$limit = $_POST['limit'];
			$page = $_POST['page'];
			
			$start = ($page-1) * $limit;
		  	//我們只取已經confirm的quotation
		  	$record_count = $this->Quotation_model->ajax_record_count($keyword, array(
				'status'	=> 6
			));
		  	$data = $this->Quotation_model->ajax_quotation($keyword, $limit, $start, array(
				'status'	=> 6
			));
		  	$paging = $this->get_paging($limit,10,$record_count,$page);
			$quotation_status = $this->Quotation_model->quotation_status_list();
			
			//顯示Status為狀態文字, 不是數字
			foreach($data as $k=>$v) {
				$data[$k]['status'] = $quotation_status[$v['status']];
			}
		  	
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					'paging'	=> $paging,
			);
			
			echo json_encode($temp);	
			exit;
	  }	  
	  
	  public function ajax_getQuotationData(){
		  
		  	$id = $_POST['id'];
		  	$data = $this->Quotation_model->get($id);
			$employee = $this->Employee_model->get($data['create_user_id']);
			$department = $this->Department_model->get($employee['department_id']);
			$data['employee_no'] = $employee['employee_no'];
			$data['department'] = $department['name'];
			
			
			//get quotation related item
			$related_quotation_item = $this->Quotation_item_model->getRelatedService($id);
			if(!empty($related_quotation_item)){
				foreach($related_quotation_item as $k => $v){
					$product_data = $this->Products_model->get($v['product_id']);
					$supplier_data = $this->Suppliers_model->get($product_data['supplier']);
					$related_quotation_item[$k]['model_no'] = $product_data['model_no'];
					$related_quotation_item[$k]['serial'] = $product_data['serial'];
					
					
					$company_data = array(
						'supplier_id' =>$product_data['supplier'],
						'company_name' =>$supplier_data['company_name'],
						'company_pic' =>$supplier_data['contact_person'],
					);
				}
			}
			
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					//'product'   => $related_quotation_item,
					'product' => $related_quotation_item,
					'company_data' => isset($company_data)?$company_data:'',
			);
			
			
			
			echo json_encode($temp);	
			exit;
	  }	 
	  
	  public function get_paging($item_per_page,$pagenum,$total_item,$page)
	  {
	
		$start = (int)(($page-1)/$pagenum)*$pagenum+1;
		$end = $start+$pagenum-1;
		$next = $page+1;
		$pre = $page-1;
		
		$total_page = ceil( $total_item / $item_per_page );
                $paging = '';
		if($total_item > $item_per_page){
                    $paging .= '<ul class="pagination">';

                    if($page > 1){
                            $paging .= '<li><a href="javascript:changePage(1)">&laquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$pre.')">&lsaquo;</li>';
                    }

                    if($total_page <= $pagenum){

                            for($i=$start;$i<=$total_page;$i++){
                                    if($i == $page){

                                            $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                    }else{

                                            $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                    }
                            }
                    }else{
                            if($page > 5){
                                    $end = $page+5;
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    $start = $end - ($pagenum - 1);

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }else{
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }	
                    }

                    if($page < $total_page){
                            $paging .= '<li><a href="javascript:changePage('.$next.')">&rsaquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$total_page.')">&raquo;</a></li>';
                    }

                    $paging .= '</ul>';
                }
		
		return $paging;
	  } 	  
	  

}

?>