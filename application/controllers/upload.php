<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$file_name_with_full_path = realpath('./assets/img/avatar.png');
		$post = array('extra_info' => '123456','u'=>'@'.$file_name_with_full_path);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://aipi2.com/u.php");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post); 
		$response = curl_exec($ch);
		echo $response;
		
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */