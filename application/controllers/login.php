<?php

/* * ***********************
 *
 * When user want to login, they send info into this page
 *
 * *********************** */

class Login extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();
			$this->load->helper('cookie');
			
			$this->load->model('Check_model');
			$this->load->model('Settings_model');
			
			$this->load->library('Mobile_Detect'); //for mobile view
			
			if( !$this->Check_model->is_installed() ) {
				 redirect('/', 'refresh');
				 exit;
			}

            $this->load->model('User_model');
            $this->load->model('function_model');

            $this->data['init'] = $this->function_model->page_init();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			$detect = new Mobile_Detect;
            
			if ($detect->isMobile()) {
				$this->data['mobile'] = 1;
			}else{
				$this->data['mobile'] = 0;
			}
			
            if ($this->data['islogin']) {
                  $this->data['userdata'] = $this->session->userdata("userdata");
				  if($this->data['userdata']['role_id'] == 7){
							
					if($this->data['mobile'] == 1){
						redirect('/en/agora/customer/customer_m', 'refresh');	
					}else{
						redirect('/en/agora/customer/promotion', 'refresh');
					}
							
				}else{
					redirect('/en/agora/tasklist', 'refresh');	
				}
            }
			
			
			
			//$this->data['mobile'] = 1;
			
      }
      

      
      public function index() {

            $this->load->helper('form');
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == FALSE) {
                   
                  //$this->data['title'] = ucfirst("home");
                  $this->load->view("anexus/login", $this->data);
				  
            } else {
                  
                  $email = $this->input->post("email", true);
                  $remember = $this->input->post("remember", true);
                  $password = $this->input->post("password", true);                                    
                  $userdata = $this->User_model->getUserByEmail($email);
				  
				  //print_r($userdata);exit;
				  
                  if($userdata['passwd'] != md5($password)) {
                        
                        $this->data['invalid'] = $this->data['init']['lang']['invalid_login'];
                        
                        $this->data['title'] = ucfirst("home");
                       
                        $this->load->view("anexus/login", $this->data);
						
                  } else {
                        
                        $user_id = $userdata['id'];
                        //Load Sean in Default
                        $this->session->set_userdata("userdata", $this->User_model->get_user($user_id));                                                                       
                        
                        //if user set rememeber their login info to cookie
                        //cookie expired after one year
						//echo $remember;exit;
                        if($remember == "on") {
                              setcookie("chl_token", $email."/".md5($password), time()+365*24*3600);
							  
                        } 
                                                
                        //if = customer,redirect promotion page
						if($userdata['role_id'] == 7){
							
							if($this->data['mobile'] == 1){
								redirect('/en/agora/customer/customer_m', 'refresh');	
							}else{
								redirect('/en/agora/customer/promotion', 'refresh');
							}
							
						}else{
							redirect('/en/agora/tasklist', 'refresh');	
						}
                        
                        
                  }
                  
            }
            
      }
	  
	  public function forgot_pw($status=''){
		  
		  $this->data['status'] = $status;
		  $this->load->view("anexus/forgot_pw", $this->data);
		  
	  }
	  
	  public function reset_email(){
		  
		  $pwd_retrival = md5(date('Y-m-d H:i:s'));
		  $email = $this->input->post('email',true);
		  
		  $userdata = $this->User_model->getUserByEmail($email);
		  
		  if(!empty($userdata)){
			  $this->User_model->updatePwdResetCode($pwd_retrival,$userdata['id']);
			  
			  if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
		
				$sitename = $this->Settings_model->get_settings(1);
				$default_email = $this->Settings_model->get_settings(6);
		
				$this->load->library('email');
				$this->email->clear();	
				$this->email->from($default_email['value'], $sitename['value']);
				$this->email->to($email); 
				$this->email->subject('Agora reset password');
				$this->email->message(base_url('/en/reset_pw/'.$pwd_retrival));	
				$this->email->set_newline("\r\n");				
				$this->email->send();
					
				//echo $this->email->print_debugger();exit;
		  	  }
			   
			  redirect('/en/forgot_pw/1', 'refresh');	
		  }else{
			  redirect('/en/forgot_pw/0', 'refresh');
		  }
		  
		  
	  }
	  
	  public function reset_pw($pwd_retrival){
		
			$check_isset = $this->User_model->getPwdResetCode2($pwd_retrival);
			$isset_data = $this->User_model->getUserBy_retrieval($pwd_retrival);
			if($check_isset != true){
				$this->data['status'] = 0;
			}else{
				$this->data['user_id'] = $isset_data['id'];
			}
		
			$this->load->view("anexus/reset_pw", $this->data);
		  
	  }
	  
	  public function reset_submit(){
		
			$user_id = $this->input->post('user_id',true);
			$pw1 = $this->input->post('pw1',true);
			$pw2 = $this->input->post('pw2',true);
			
			if($pw1 == $pw2 && $user_id !=0){
				$this->User_model->updateUserPassword(md5($pw1),$user_id);
				//echo $this->db->last_query();exit;
				redirect('/en/reset_done/1', 'refresh');
			}else{
				redirect('/en/reset_done/0', 'refresh');
			}
			 
	  }
	  
	  public function reset_done($status){
		
			$this->data['status'] = $status;
			$this->load->view("anexus/reset_done", $this->data);
			 
	  }

}

?>