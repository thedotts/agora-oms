<?php

class Pending_order_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();  
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Customers_model');
			$this->load->model('Tender_model');
			$this->load->model('Tender_item_model');
			$this->load->model('Employee_model');
			$this->load->model('Department_model');
			$this->load->model('Products_model');
			$this->load->model('Suppliers_model');
			$this->load->model('Shipping_model');
			$this->load->model('Products_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Settings_model');
			$this->load->model('Quotation_model');
			$this->load->model('Quotation_item_model');
			$this->load->model('Workflow_model');
			$this->load->model('Workflow_order_model');
			$this->load->model('Order_model');
			$this->load->model('Product_log_model');
			$this->load->model('Order_item_model');
			$this->load->model('Audit_log_model');
			$this->load->model('Packing_list_item_model');
			$this->load->model('Packing_list_model');
			$this->load->model('Delivery_order_model');
			$this->load->model('Invoice_model');
			
            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
			
			$this->data['role_list'] = $this->Role_model->getIDKeyArray('name');
			$status_list = $this->Workflow_model->get(1);
			$this->data['status_list'] = json_decode($status_list['status_json']);
			
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$userdata = $this->session->userdata("userdata");
				
				//print_r($userdata);exit;
				if(strpos($userdata['role_id'],',')){
					$userdata['role_id'] = explode(',',$userdata['role_id']);	
				}else{
					$userdata['role_id'] = array($userdata['role_id']);	
				}
				
				
				$this->data['userdata'] = $userdata;
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}  
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			$this->data['group_name'] = "customer";  
			$this->data['model_name'] = "pending_order";  
			$this->data['common_name'] = "Pending Orders";   
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			/*
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
					
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}  
			*/
            
			//related role
		    define('SUPERADMIN', 1);
		    define('SALES_MANAGER', 7);
		    define('SALES_EXECUTIVE', 8);
		   
      }
   
      public function index($q="ALL", $page=1, $alert=0) {  
          		
			$this->data['alert'] = $alert;
					
			$this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
				'new_status !='	=> 1,
				'customer_id'	=> $this->data['staff_info']['customer_id'],
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/';
			if($q == 'ALL') {
				$q = "";
			}else{
				$q = urldecode($q);
			}
			$this->data['q'] = urldecode($q);
			$this->data['page'] = $page;
					
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Order_model->record_count($filter, $q);
			
			//get particular ranged list
			$this->data['results'] = $this->Order_model->fetch2($q, $this->data['item_per_page'], $limit_start,$filter);
			
			//抓目前status
			if(!empty($this->data['results'])){
			foreach($this->data['results'] as $k => $v){
				
				//status is completed
				if($v['status'] == 2){
					
					$related_packing = $this->Packing_list_model->get_where(array('is_deleted'=>0,'status'=>1,'order_id'=>$v['id']));
					
					if(!empty($related_packing)){

						//order 的數量
						$order_item = $this->Order_item_model->getRelatedItem($v['id']);
						$order_qty = 0;
						if(!empty($order_item)){
							foreach($order_item as $k2=>$v2){
								$order_qty += $v2['quantity'];
							}
						}
						
						//packing 數量
						$packing_qty = 0;
						foreach($related_packing as $k2 => $v2){
							
							$packing_item = $this->Packing_list_item_model->getRelatedItem($v2['id']);
							
							foreach($packing_item as $k3 => $v3){
								$packing_qty += $v3['quantity_pack'];
							}
							
						}
						
						//packing list 的數量
						$pack_count = count($related_packing);
						
						//判斷item pack完了沒
						if($order_qty == $packing_qty){
							
							//目前有的delivery order
							$related_do = count($this->Delivery_order_model->get_where(array('is_deleted'=>0,'status'=>2,'order_id'=>$v['id'])));
							
							if($related_do == 0){
								$this->data['results'][$k]['status_text'] = 'Pending Delivery Order';
							//代表do已處理完畢
							}else if($related_do == $pack_count){
								$this->data['results'][$k]['status_text'] = 'Completed';
								/*
								//do 數量
								$do_count = $related_do;
								
								//invoice 數量
								$related_invoice = count($this->Invoice_model->get_where(array('is_deleted'=>0,'status'=>1,'order_id'=>$v['id'])));
								
								if($related_invoice == 0){
									$this->data['results'][$k]['status_text'] = 'Pending Invoice';
								}else if($do_count == $related_invoice){
									$this->data['results'][$k]['status_text'] = 'Completed';
								}else{
									$this->data['results'][$k]['status_text'] = 'Pending Generate Invoice ['.$related_invoice.' of '.$do_count.']';
								}
								*/
								
							}else{
								$this->data['results'][$k]['status_text'] = 'Pending Generate Delivery Order ['.($related_do+1).' of '.$pack_count.']';
							}
							
						}else{
							$this->data['results'][$k]['status_text'] = 'Pending Generate Packing List ['.($pack_count+1).' of '.($pack_count+1).']';
						}
						
					}else{
						$this->data['results'][$k]['status_text'] = 'Pending Packing List';
					}
					
				}//endif status
				
			}//end foreach
			}
			
			if(!empty($this->data['results'])) {		
				foreach($this->data['results'] as $k => $v){
					if(!empty($v['awaiting_table'])){
							$this->data['results'][$k]['awaiting_table']= explode(',',$v['awaiting_table']);
					}
				}		
			}
				
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->data['customer_list'] = $this->Customers_model->getIDKeyArray("company_name");
			$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
			
			$this->session->set_userdata("lastpage", $url.$page);		
						
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  
	  public function add($id=false) {  
          	
			//customer
			$customer_id = $this->data['userdata']['customer_id'];
			$this->data['customer_json'] = json_encode($this->Customers_model->get($customer_id));
				
			$this->data['gst'] = $this->Settings_model->get_settings(5);
						
			$role_id = $this->data['userdata']['role_id'];
					
			if($id !== false) {
				
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Order_model->get($id);
					
				$order_date = $this->data['result']['order_date'];
				$tmp = explode('-',$order_date);
				$this->data['result']['order_date'] = $tmp[2]."/".$tmp[1]."/".$tmp[0];
				
				$product = $this->Order_item_model->getRelatedItem2($id);
				
				
				
				/*
				//packed item
				foreach($product as $k => $v){
					$packed_tmp = $this->Packing_list_item_model->item_packed($id,$v['product_id']);
					
					if($packed_tmp != ''){
					$product[$k]['packed'] = $packed_tmp;
					}else{
					$product[$k]['packed'] = 0;	
					}
				}
				*/
				
				$this->data['result']['product'] = json_encode($product);
				
				$requestor_employee = $this->Employee_model->get($this->data['result']['requestor_id']);
			    $requestor_role = $requestor_employee['role_id'];
				$this->data['requestor_role'] = $requestor_role;
				if(!empty($this->data['result']['awaiting_table'])){
				$this->data['result']['awaiting_table'] = explode(',',$this->data['result']['awaiting_table']);
				}
				
				$this->data['customer'] = $this->Customers_model->get($this->data['staff_info']['customer_id']);
				
				$default = 0;
				
				//get related workflow order data
				$workflow_flitter = array(
					'status_id' => $this->data['result']['status'],
					'workflow_id' => 1,
				);
						
				$type='';
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						
				//print_r($requestor_role);exit;
				//$workflow_order['role_id'] == 'requestor'
				if($workflow_order['role_id'] == 'requestor'){
							
				//requestor edit
				if(in_array($requestor_role,$role_id) && $workflow_order['requestor_edit']) {
				$type = 'edit';	
								
				//user role = current status role
				}else if(in_array($requestor_role,$role_id)){
					$type = $workflow_order['action'];
				}
							
				//$workflow_order['role_id'] != 'requestor'						
				}else{
							
					//user role = current status role
					if (in_array($workflow_order['role_id'],$role_id)){
						$type = $workflow_order['action'];	
							
					//requestor edit															
					}else if(in_array($requestor_role,$role_id) && $workflow_order['requestor_edit']) {
						$type = 'edit';	
					}
						
				}
						
				//data form database
				$workflow = $this->Workflow_model->get(1);
				$status = json_decode($workflow['status_json'],true);
						
				$this->data['workflow_title'] = $workflow['title'];
					
				$this->data['btn_type'] = $type;
				//print_r($workflow_order);exit;

					switch($type){
						case 'edit':
							$this->data['head_title'] = 'Edit '.$workflow['title'].' Request';
						break;
						case 'approval':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
						break;
						case 'approval_update':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'update':
							$this->data['head_title'] = 'Update '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'send_email':
							$this->data['head_title'] = 'Waiting to Send '.$workflow['title'].' Request';
						break;
						case 'sendmail_update':
							$this->data['head_title'] = 'Waiting to Send/Edit '.$workflow['title'].' Request';
						break;
						case 'upload':
						case 'upload_update':
							$this->data['head_title'] = 'Upload Confirm File '.$workflow['title'].' Request';
						break;
						case 'complete':
							$this->data['head_title'] = $workflow['title'].' Request';
							$this->data['last_action'] = $workflow_order['action_before_complete'];
						break;
						default:
							$this->data['head_title'] = $workflow['title'].' Request';
					}
					
				foreach(json_decode($workflow['status_json'],true) as $k => $v){
						
					if($v == 'Completed'){
						$this->data['completed_status_id'] = $k;
						break;
					}
				}

				
			} else {
				
				$this->data['mode'] = 'Add';
				$this->data['head_title'] = 'New Order';	

				//get related workflow order data
					$workflow_flitter = array(
						//'role_id' => $role_id,
						'status_id' => -1,
						'workflow_id' => 1,
					);
					
					//data form database
					$workflow = $this->Workflow_model->get(1);
					$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter,$role_id);
					
					//get view bottom layout
					$type = '';
					if(!empty($workflow_order)){
						$type = $workflow_order['action'];
						if($workflow_order['action'] == 'create'){
							$this->data['head_title'] = 'Create '.$workflow['title'].' Request';
						}else{
							$this->data['head_title'] = $workflow['title'].' Request';
						}
					}else{
						$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					$this->data['btn_type'] = $type;
					
			}		
			
			//all product 
			$products = $this->Products_model->get();
			//sum product by id
			foreach($products as $k => $v){
				$products[$k]['qty'] = $this->Product_log_model->sum_by_id($v['id']);
				
				//promo限制數量
				$p_start_date = $v['p_start_date'];
				$p_end_date = $v['p_end_date'];
				$p_qty = $v['p_qty'];
						
				$order_item_qty = $this->Order_item_model->count_promo_qty($v['id'],$p_start_date,$p_end_date);
				$promo_val = 0;
				if($order_item_qty >= $p_qty || strtotime($p_end_date)<strtotime(date('Y-m-d'))|| $p_qty==0){
					$promo_val = 1;
				}
				
				$products[$k]['promo_val'] = $promo_val;
				
			}
			
			//print_r($products);exit;
			
			$this->data['product'] = json_encode($products);
			
			//get assign customer
			$company_list = $this->Customers_model->getAll();
			$assign_customer = $this->data['staff_info']['assign_customer'];
			
			if(strpos($assign_customer,',')){
				$assign_customer = explode(",",$assign_customer);
			}else{
				$assign_customer = array($assign_customer);
			}
			
			//只有是sales 的時候才給予指定customer
			if(in_array(3,$this->data['userdata']['role_id'])){
			
			$new_company_list = array();
			foreach($company_list as $k => $v){
				
				foreach($assign_customer as $k2 => $v2){
					if($v2 == $v['id']){
						$new_company_list[] = $v;
					}
				}
				
			}
			
			}else{
				$new_company_list = $company_list;
			}
			
			$this->data['company_list'] = json_encode($new_company_list);
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			//product
			$product_name = $this->Products_model->get_groupBy();
			$this->data['product_name'] = json_encode($product_name);
			
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);	
			
      }
	  
	  public function submit(){
		  
		  $customer = $this->Customers_model->get($this->data['staff_info']['customer_id']);
		  
		  $now = date("Y-m-d H:i:s");
		  $mode = $this->input->post("mode", true);
		  $id = $this->input->post("id", true);
		  
		  $role_id = $this->data['userdata']['role_id'];

		  //form data
		  $requestor_id = $this->input->post("requestor_id", true);
		  $order_date = $this->input->post("order_date", true);
		  $company = $this->input->post("company", true);
		  $remarks = $this->input->post("remarks", true);
		  $gst_amount = $this->input->post("gst_amount", true);
		  $total_price = $this->input->post("total_amount", true);

		  $order_date = $this->input->post("order_date", true);
		  $remarks = $this->input->post("remarks", true);

		  if(!empty($order_date)) {
			  $tmp = explode("/", $order_date);
			  $order_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
		  }
		  
		  //main
		  $array = array(
		  	'order_date'		=> $order_date,
			'remark'			=> $remarks,
			'gst_amount'			=> $gst_amount,
			'total_price'			=> $total_price,
		  );

		  $this->Order_model->update($id, $array);
		  
		  
		  //audit log
			  $log_array = array(
				'ip_address'	=> $this->input->ip_address(),
				'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
				'table_affect'	=> 'order',
				'description'	=> 'Update order',
				'created_date'	=> date('Y-m-d H:i:s'),
			  );
				  
			  $audit_id = $this->Audit_log_model->insert($log_array);	
			  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
			  $update_array = array(
				 'log_no'	=> $custom_code,
			  );
			  $this->Audit_log_model->update($audit_id, $update_array);
			
		  
		  
		  $item_count = $this->input->post("item_count", true);
		  
		  if($item_count > 0){
		  //針對底下的ITEM做處理
		  
		  for($i=1;$i<=$item_count;$i++){
			  
			  $product = $_POST['product'.$i];
			  //$product = isset($_POST['product'.$i])?$_POST['product'.$i]:'';
			  //print_r($product);exit;
			  if($product != '-'){
					
					//$product = explode('|',$product);
					
					//print_r($tmp);exit;
					$data = array(
						'order_id' => $id,
						'product_id' => isset($_POST['product'.$i])?$_POST['product'.$i]:'',
						'model_name' => isset($_POST['model_name'.$i])?$_POST['model_name'.$i]:'',
						'quantity' => isset($_POST['qty_amend'.$i])?$_POST['qty_amend'.$i]:'',
						'quantity_order' => isset($_POST['qty_amend'.$i])?$_POST['qty_amend'.$i]:'',
						'price' => isset($_POST['price'.$i])?$_POST['price'.$i]:'',
						'created_date' => $now,
					);
					
					
					
					//有deleted 又有old_id 代表要刪除資料
					if(isset($_POST['is_del'.$i]) && !empty($_POST['is_del'.$i]) && isset($_POST['old_id'.$i]) && !empty($_POST['old_id'.$i])){
						
						
						$this->Order_item_model->delete($_POST['old_id'.$i]);
						
					//沒有deleted 但有old_id 代表要更新這個資料
					} else if ( isset($_POST['is_del'.$i]) && empty($_POST['is_del'.$i]) && isset($_POST['old_id'.$i]) && !empty($_POST['old_id'.$i])) {
						
						
						
						//先刪除商品加回庫存
						$orderItem = $this->Order_item_model->get($_POST['old_id'.$i]);
						
						//order item 的 product 跟 post product 不一樣代表物品更改
						if($orderItem['product_id'] == $_POST['product'.$i]){
							
							if($data['quantity'] != 0){
								
								
								$this->Order_item_model->update($_POST['old_id'.$i], $data);
								
							}
								
						
						//物品不一樣		
						}else{
							
							$this->Order_item_model->delete($_POST['old_id'.$i]);
							
							
							//至少要有product_id才能insert
							if(!empty($data['product_id']) && !empty($data['model_name'])) {
								
								$this->Order_item_model->insert($data);
								
							}
							
					
						}
						
					//沒有deleted 也沒有old_id 代表要新增這個資料	
					} else if ( isset($_POST['is_del'.$i]) && empty($_POST['is_del'.$i]) && isset($_POST['old_id'.$i]) && empty($_POST['old_id'.$i])){
						
						//至少要有product_id才能insert
						if(!empty($data['product_id']) && !empty($data['model_name'])) {
							
							$this->Order_item_model->insert($data);
							
						}
						
					}

			   }
			}

			
			
		  }
		  		  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage.'/2','refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/2'));
		  }		  
		  
	  }
	  
	  public function del($id){
		  
		  $customer = $this->Customers_model->get($this->data['staff_info']['customer_id']);
		  
		  $data = $this->Order_model->get($id);
		  
		  //job 也要一起del
		  $this->Job_model->update($data['latest_job_id'],array('is_deleted'=>1));
		  
		  $this->Order_model->delete($id);
		  
		  
		  //if status != 0 ,刪除order增加商品庫存
		  if($data['status'] != 0){
		  
		  	  $item = $this->Order_item_model->getRelatedItem($id);
							
			  foreach($item as $k => $v){
				  
				  $log_data = array(
					'product_id' => $v['product_id'],
					'qty' => $v['quantity'],
					'reason' => 'del order',
					'employee_no'	=> $customer['custom_code'],
					'employee_name'	=> $customer['company_name'],
					'created_date' => date('Y-m-d H:i:s'),
				  );
									
				  $this->Product_log_model->insert($log_data);
				  //echo $this->db->last_query();exit;
			  
			  }
		  
		  }
		  
		  
		  
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage.'/3','refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/3'));
		  }		
	  }
	  
	  public function ajax_getQuotation(){
		  
		  	$keyword = $_POST['keyword'];
			$limit = $_POST['limit'];
			$page = $_POST['page'];
			
			$start = ($page-1) * $limit;
			//我們只取已經confirm的quotation
		  	$record_count = $this->Quotation_model->ajax_record_count($keyword, array(
				'status'	=> 6
			));
		  	$data = $this->Quotation_model->ajax_quotation($keyword, $limit, $start, array(
				'status'	=> 6
			));
		  	$paging = $this->get_paging($limit,10,$record_count,$page);
			
			$quotation_status = $this->Quotation_model->quotation_status_list();
			
			//顯示Status為狀態文字, 不是數字
			if(!empty($data)) {
				foreach($data as $k=>$v) {
					$data[$k]['status'] = $quotation_status[$v['status']];
				}
			}
		  	
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					'paging'	=> $paging,
			);
			
			echo json_encode($temp);	
			exit;
	  }	  
	  
	  public function ajax_getQuotationData(){
		  
		  	$id = $_POST['id'];
		  	$data = $this->Quotation_model->get($id);
			$employee = $this->Employee_model->get($data['create_user_id']);
			$department = $this->Department_model->get($employee['department_id']);
			$data['employee_no'] = $employee['employee_no'];
			$data['department'] = $department['name'];
			
			
			//get quotation related item
			$related_quotation_item = $this->Quotation_item_model->getRelatedItem($id);
			
			foreach($related_quotation_item as $k => $v){
				$product_data = $this->Products_model->get($v['product_id']);
				$supplier_data = $this->Suppliers_model->get($product_data['supplier']);
				$related_quotation_item[$k]['model_no'] = $product_data['model_no'];
				$related_quotation_item[$k]['part_no'] = $product_data['part_no'];
				$related_quotation_item[$k]['supplier_id'] = $product_data['supplier'];
				$related_quotation_item[$k]['cost_price'] = $product_data['cost_price'];
				$related_quotation_item[$k]['supplier_name'] = $supplier_data['company_name'];
				$related_quotation_item[$k]['supplier_pic'] = $supplier_data['contact_person'];
			}
			

		

	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					//'product'   => $related_quotation_item,
					'product' => $related_quotation_item ,
			);
			
			
			
			echo json_encode($temp);	
			exit;
	  }	 
	  
	  public function get_paging($item_per_page,$pagenum,$total_item,$page)
	  {
	
		$start = (int)(($page-1)/$pagenum)*$pagenum+1;
		$end = $start+$pagenum-1;
		$next = $page+1;
		$pre = $page-1;
		
		$total_page = ceil( $total_item / $item_per_page );
                $paging = '';
		if($total_item > $item_per_page){
                    $paging .= '<ul class="pagination">';

                    if($page > 1){
                            $paging .= '<li><a href="javascript:changePage(1)">&laquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$pre.')">&lsaquo;</li>';
                    }

                    if($total_page <= $pagenum){

                            for($i=$start;$i<=$total_page;$i++){
                                    if($i == $page){

                                            $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                    }else{

                                            $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                    }
                            }
                    }else{
                            if($page > 5){
                                    $end = $page+5;
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    $start = $end - ($pagenum - 1);

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }else{
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }	
                    }

                    if($page < $total_page){
                            $paging .= '<li><a href="javascript:changePage('.$next.')">&rsaquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$total_page.')">&raquo;</a></li>';
                    }

                    $paging .= '</ul>';
                }
		
		return $paging;
	  }
	  
	  
	  public function ajax_getModel(){
		  
		  	$id = $_POST['id'];
			$type = $this->input->post("type", true);
			
			//Products
			if($type == 1) {

		  		$data = $this->Products_model->ajax_model($id, $type);
			
			//Service
			} else if ($type == 2) {
				
				$data = $this->Products_model->getService();
				
			}
		  	
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					'type'		=> $type,
			);
			
			echo json_encode($temp);	
			exit;
	  }	   
	  
	  public function ajax_estShipping(){
		  
		  	$companyId = $_POST['companyId'];	
		  	$shipperId = $_POST['shipperId'];		
			$groupItem = json_decode($_POST['groupItem'],true);

			$shippingItem = $this->Shipping_model->get($shipperId);

			if( strpos($shippingItem['country'], ",") !== FALSE ) {
			
				$shippingCountry = explode(",",$shippingItem['country']);
			
			} else {
				
				$shippingCountry = array();
				$shippingCountry[] = $shippingItem['country'];
					
			}
			
			$shippingType = $shippingItem['shipping_type'];
			$shippingRate = json_decode($shippingItem['cost_json'],true);
			
			$estShipping = 0;
			
			foreach($groupItem as $k => $v){
				
				if($v['item_type'] == 'product'){
					
				$customer = $this->Customers_model->get($companyId);
				
				$countryCheck = 0;
				foreach($shippingCountry as $x){
					if($customer['country'] == $x){
						$countryCheck = 1;
						break;
					}
				}
				
				if($countryCheck == 1){
					
					$product = $this->Products_model->get($v['modelNo']);
					$actualWeight = $product['weight'];
					$dimensionWeight = ($product['length']*$product['breadth']*$product['height'])/6000; 
					//抓取weight
					if($actualWeight > $dimensionWeight){
						$weight = $actualWeight;
					}else{
						$weight = $dimensionWeight;
					}
					if($v['quantity']=='select'){
						$weight = 0;
					}else{
						$weight = $weight*$v['quantity'];
					}
					
					$min_diff = 999;
					$target = 0;
					$count_shippingRate = count($shippingRate);
					
					//針對每個shippingrate去看最小差距
					foreach($shippingRate as $x => $y){
						$diff_weight = abs($weight-$y['weight']);
						if($diff_weight < $min_diff){
							$min_diff = $diff_weight; //最小的差距
							$target = $x; //抓最小差距的 array(key)
						}
					}
					
					//拿搜尋的weight減掉最小差距的weight
					$weightL = $weight - $shippingRate[$target]['weight'];
					
					//如果是大於零, 代表我們需要看他的下一層是否存在
					if($weightL > 0){

						//他的下一層有存在
						if(isset($shippingRate[($target+1)]['weight'])) {
							$cost = $shippingRate[($target+1)]['cost'];
						//不存在就取當下的這層	
						} else {
							$cost = $shippingRate[$target]['cost'];	
						}
					//如果等於零, 代表剛剛好是這層							
					}else{
						$cost = $shippingRate[$target]['cost'];
					}
					
					if($cost > $estShipping){
						$estShipping = $cost;
					}
				//管理者根本沒有設定這個國家的運費	
				}else{
					$estShipping = 0;
				}
				
				}
				
			}
			
		  	
	  		$temp = array(
					'status'	=> 'OK',
					'estShipping'		=> $estShipping,
			);
			
			echo json_encode($temp);	
			exit;
	  }	   
	  
	  
	  public function ajax_getProductDetail(){
		  	$id = $_POST['id'];
			
			$product = $this->Products_model->get($id);
			
			$temp = array(
					'status'	=> 'OK',
					'product'		=> $product,
			);
			
			echo json_encode($temp);	
			exit;
			
			
	  }
	  
	  private function PDF_generation($id, $mode='I') {
		  
		  
		  		$tender_data = $this->Tender_model->get($id);
					
		  		$receivedDate = $tender_data['received_date'];
				$tmp = explode('-',$receivedDate);
				$receivedDate = $tmp[2]."/".$tmp[1]."/".$tmp[0];
				
				$requestDate = $tender_data['request_date'];
				$tmp = explode('-',$requestDate);
				$requestDate = $tmp[2]."/".$tmp[1]."/".$tmp[0];
				
				$requestor = $this->Employee_model->get($tender_data['requestor_id']);
				$tender_data['requestor_name'] = $requestor['full_name'];
				$requestor_department = $this->Department_model->get($requestor['department_id']);
				$tender_data['requestor_department_name'] = $requestor_department['name'];
				
				$product = $this->Tender_item_model->getRelatedItem($id);
				
				foreach($product as $k => $v){
					$product_data = $this->Products_model->get($v['product_id']);
			  		$supplier_tmp = $this->Suppliers_model->get($product_data['supplier']);
			 		$product[$k]['supplier_name'] = $supplier_tmp['company_name'];
				}

		 		$customer = $this->Customers_model->get($tender_data['customer_id']);
		  		$shipper_data = $this->Shipping_model->get($tender_data['shipper']);
		  	  
		  
		  $item_group = '';
		  foreach($product as $k => $v){
			  $item_group .='<tr><td>'.($k+1).'</td><td>'.$v['supplier_name'].'</td><td>'.$v['model_no'].'</td><td>'.$v['quantity'].'</td><td>'.$v['unit_price'].'</td><td>'.$v['extended_price'].'</td></tr>';
		  }
		  
		  
		  $this->load->library("Pdf");		
          $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		  
		  // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Anexus');
            $pdf->SetTitle("Tender");
            $pdf->SetSubject("");
            $pdf->SetKeywords("");

            // remove default header/footer
            $pdf->setPrintHeader(false);			
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(10, 10, 10);

            // set auto page breaks
            //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
            }

            // ---------------------------------------------------------

            // set font
            $pdf->SetFont('times', '', 10);

            

            // set font
            $pdf->SetFont('times', '', 10);
			
			// add a page
       		$pdf->AddPage();
			
			
			$html = '			
			<h1>Tender</h1>
			
			<table width="100%" cellpadding="5">
				<tr>
					<td>
						<table width="100%"  border="1" cellpadding="5">
			
							<tr>
								<th colspan="2"><h3>Tender Information</h3></th>
							</tr>
							<tr>
								<th colspan="2"><h4>QR Serial No.</h4></th>
							</tr>
							<tr>
								<td colspan="2">'.$tender_data['qr_serial'].'</td>
							</tr>
							<tr>
								<th colspan="2"><h4>Date Received</h4></th>
							</tr>
							<tr>
								<td colspan="2">'.$receivedDate.'</td>
							</tr>
							<tr>
								<th colspan="2"><h4>Payment Terms</h4></th>
							</tr>
							<tr>
								<td>Milestone</td>
								<td>Percentage</td>
							</tr>
							<tr>
								<td>Sign-Off</td>
								<td>'.$tender_data['tender_payment_sign_off'].'</td>
							</tr>
							<tr>
								<td>Before Delivery</td>
								<td>'.$tender_data['tender_payment_before_delivery'].'</td>
							</tr>
							<tr>
								<td>After Delivery</td>
								<td>'.$tender_data['tender_payment_after_delivery'].'</td>
							</tr>
							<tr>
								<td>Completed</td>
								<td>'.$tender_data['tender_payment_completed'].'</td>
							</tr>
							<tr>
								<td>Warranty</td>
								<td>'.$tender_data['tender_payment_warrantly'].'</td>
							</tr>
							<tr>
								<th colspan="2"><h4>Legal Terms</h4></th>
							</tr>
							<tr>
								<td colspan="2">'.$tender_data['tender_legal_term'].'</td>
							</tr>
	
						</table>
					</td>
					
					<td>
						<table width="100%"  border="1" cellpadding="5">
			
							<tr>
								<th><h3>Customer Information</h3></th>
							</tr>
							<tr>
								<th><h4>Company Name</h4></th>
							</tr>
							<tr>
								<td>'.$customer['company_name'].'</td>
							</tr>
							<tr>
								<th><h4>Attention</h4></th>
							</tr>
							<tr>
								<td>'.$customer['primary_attention_to'].'</td>
							</tr>
							<tr>
								<th><h4>Address</h4></th>
							</tr>
							<tr>
								<td>'.$customer['address'].'</td>
							</tr>
							<tr>
								<th><h4>Postal Code</h4></th>
							</tr>
							<tr>
								<td>'.$customer['postal'].'</td>
							</tr>
							<tr>
								<th><h4>Email</h4></th>
							</tr>
							<tr>
								<td>'.$customer['email'].'</td>
							</tr>
							<tr>
								<th><h4>Contact</h4></th>
							</tr>
							<tr>
								<td>'.$customer['contact_info'].'</td>
							</tr>
		
						</table>
						
					</td>
				</tr>
				
				<tr>
					<td>
						
						<table width="100%"  border="1" cellpadding="5">
			
							<tr>
								<th><h3>Shipper Information</h3></th>
							</tr>
							<tr>
								<th><h4>Shipper</h4></th>
							</tr>
							<tr>
								<td>'.$shipper_data['shipper_name'].'</td>
							</tr>
							<tr>
								<th><h4>Est. Shipping</h4></th>
							</tr>
							<tr>
								<td>'.$tender_data['est_shipping'].'</td>
							</tr>
		
						</table>
					</td>
					
					<td>
						<table width="100%"  border="1" cellpadding="5">
			
							<tr>
								<th><h3>Staff Information</h3></th>
							</tr>
							<tr>
								<th><h4>Requestor Name</h4></th>
							</tr>
							<tr>
								<td>'.$tender_data['requestor_name'].'</td>
							</tr>
							<tr>
								<th><h4>Date of Request</h4></th>
							</tr>
							<tr>
								<td>'.$requestDate.'</td>
							</tr>
							<tr>
								<th><h4>Department</h4></th>
							</tr>
							<tr>
								<td>'.$tender_data['requestor_department_name'].'</td>
							</tr>
							<tr>
								<th><h4>Requestor To</h4></th>
							</tr>
							<tr>
								<td>'.$tender_data['requestor_to'].'</td>
							</tr>
	
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						
						<table width="100%" border="1" cellpadding="5">
				
							<tr>
								<th colspan="6"><h3>Items</h3></th>
							</tr>
							<tr>
								<td>#</td>
								<td>Principal/Supplier</td>
								<td>Model No</td>
								<td>Quantity</td>
								<td>Unit Price</td>
								<td>Extended Price</td>
							</tr>'.$item_group.'
						</table>
						
					</td>
				</tr>
				<tr>
					<td colspan="2">
						
						<table width="100%" border="1" cellpadding="5">
				
							<tr>
								<th><h3>Remarks</h3></th>
							</tr>
							<tr>
								<td>'.$tender_data['qr_remarks'].'</td>
							</tr>
						</table>
						
					</td>
				</tr>

			</table>';
			
			$pdf->writeHTML($html, true, false, true, false, '');
									
			
			$file_name = date("YmdHis").rand(1000,9999);
			
			if($mode == 'I') {
				$pdf->Output($file_name.'.pdf', 'I'); 	
				return $file_name.'.pdf';
			} else if ($mode == 'f') {				
				$pdf->Output('./uploads/'.$file_name.'.pdf', 'f'); 
				return './uploads/'.$file_name.'.pdf';
			}
			
		  		  
		  
	  }
	  
	  public function export_pdf($id){		  
		  $this->PDF_generation($id, 'I');			
	  }
	  
	  public function sent_mail($id,$product_total=0,$service_total=0){
		  
		   	$filename = $this->PDF_generation($id, 'f');	 		  
		  
		    $tender_data = $this->Tender_model->get($id);
		  	$customer = $this->Customers_model->get($tender_data['customer_id']);
		  
		  	$send_to = $customer['email'];
			
			$sitename = $this->Settings_model->get_settings(1);
		  	$default_email = $this->Settings_model->get_settings(6);
			
			 //$requestor_role
			$requestor_employee = $this->Employee_model->get($tender_data['requestor_id']);
			$requestor_role = $requestor_employee['role_id'];
		  
		 	$role_id = $this->data['userdata']['role_id'];
			
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $tender_data['status'],
							'workflow_id' => 3,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if(in_array($requestor_role,$role_id) && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if(in_array($requestor_role,$role_id)){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if (in_array($workflow_order['role_id'],$role_id)){
								$action = $workflow_order['action'];
							
							//requestor edit															
							}else if(in_array($requestor_role,$role_id) && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(3);
						$status = json_decode($workflow['status_json'],true);
						
						if(isset($action)){

		
						if($action == 'send_email' || $action == 'sendmail_update'){
							
							//next status
							$next_status = $workflow_order['next_status_id'];
							$next_status_text = $status[$workflow_order['next_status_id']];
							$who_email = $workflow_order['who_email'];
	
						}

						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];

								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 1,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
							
							$jobArray = array(
								'parent_id' 		=>$tender_data['latest_job_id'],
								'user_id' 			=>$this->data['userdata']['id'],
								'customer_id' 		=>$tender_data['customer_id'],
								'type' 				=> 'Tender',
								'type_path' 		=>'sales/tender_edit/',
								'awaiting_role_id' 	=>$awating_person.',',
								'status_id' 		=>$next_status,
								'status_text'		=>$next_status_text,
								'serial' 			=>$tender_data['qr_serial'],
								'type_id' 			=>$tender_data['id'],
								'created_date'		=>date("Y-m-d H:i:s"),
								'modified_date'		=>date("Y-m-d H:i:s"),
							);			
					  
						    $array = array(
							'awaiting_table' => $awating_person.',',
							'status' => $next_status,
							'modified_date' => date('Y-m-d H:i:s'),
							'lastupdate_user_id' => $this->data['userdata']['id'],
						    );
							
							//print_r($jobArray);exit;

						
						  //更新上一個JOB的狀態					  
						  $this->Job_model->update($jobArray['parent_id'], array(
							'is_completed' => 1,
							'display' => 0,
						  ));
						  
						  //新增一個新的JOB
						  $new_job_id = $this->Job_model->insert($jobArray);
						  $array['latest_job_id'] = $new_job_id;
						  
						  $this->Tender_model->update($tender_data['id'], $array);
							
							
						}
						
						}
			
			
			if(isset($who_email)){
			
				$this->load->library('email');
				
				switch($who_email){
					case 'customer':
							
							//Send Notification
							if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
										
								$this->email->from($default_email['value'], $sitename['value']);
								$this->email->to($send_to); 
							
								$this->email->subject('Anexus '.$workflow['title'].': '.$tender_data['qr_serial']);
								$this->email->message($tender_data['qr_serial']);	
								$this->email->attach($filename);
								$this->email->set_newline("\r\n");
								$this->email->send();
							
							}
							
					break;	
					case 'supplier':
						
							  //count supplier
							  $supplier_count = $this->Purchase_request_item_model->get_supplier_groupBy($id);
							  
							  foreach($supplier_count as $k => $v){
									
								  $supplier = $this->Suppliers_model->get($v['supplier_id']);
								  
								  $file_name = $this->PDF_generation($id, 'f',$v['supplier_id']);	
										 
								  if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
									
									$this->load->library('email');
									$this->email->clear();	
									$this->email->from($default_email['value'], $sitename['value']);
									$this->email->to($supplier ['email']); 
									
									$this->email->subject('Anexus Purchase request '.$tender_data['qr_serial']);
									$this->email->message($tender_data['qr_serial']);	
									$this->email->attach($file_name);
									$this->email->set_newline("\r\n");
									$this->email->send();
									
								  }
							  
							  }
							
					break;
					case 'employee':
					break;
				
				}
				
			}
			
			 //mail
			 $mail_array = array(
				'creater_name' =>$this->data['userdata']['name'],
				'title' =>'Purchase '.$tender_data['qr_serial'].' has been send out',
			 );
			 
			 if(isset($new_job_id)){
			  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
			  $mail_array['related_role'] = $related_role;		
			  $mail_array['title'] = $workflow['title']." ".$tender_data['qr_serial']." ".$next_status_text;
			 }
			 echo $_SERVER['HTTP_HOST'];exit;
			 //sent Notification to related role						  			
		  	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local" && isset($mail_array['related_role'])) {
				$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
			  				  	
			  	foreach($related_role_data as $k => $v){
				  
				  	$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
					
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->set_newline("\r\n");
					$this->email->send();	
				  
		 		}
			}
			
			
			$lastpage = $this->session->userdata("lastpage");
			if(!empty($lastpage)) {
			  redirect($lastpage,'refresh');  
			} else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
			}		 			
		  
	  }

}

?>