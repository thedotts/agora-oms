<?php
class Loading extends CI_Controller {
	public $data = array();
    public function __construct() {
            parent::__construct();
	}
	public function index() {
          			
            $this->data['title'] = ucfirst("Loading..."); // Capitalize the first letter		
            $this->load->view('configurator/header', $this->data);
            $this->load->view('configurator/loading', $this->data);
            $this->load->view('configurator/footer', $this->data);
			
    }
}
?>