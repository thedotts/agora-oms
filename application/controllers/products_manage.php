<?php

class Products_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();     
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Products_model');
			$this->load->model('Job_model');
			$this->load->model('Suppliers_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Product_category_model');
			$this->load->model('Product_log_model');
			$this->load->model('Audit_log_model');
			$this->load->model('Employee_model');
			$this->load->model('Customers_model');


            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$userdata = $this->session->userdata("userdata");
				
				//print_r($userdata);exit;
				if(strpos($userdata['role_id'],',')){
					$userdata['role_id'] = explode(',',$userdata['role_id']);	
				}else{
					$userdata['role_id'] = array($userdata['role_id']);	
				}
				
				
				$this->data['userdata'] = $userdata;
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}        
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			//print_r($this->data['staff_info']);exit;
			
			$this->data['status_list'] = $this->Products_model->status_list(false);
			$this->data['uom_list'] = $this->Products_model->uom_list();				
			$this->data['currency_list'] = $this->Products_model->currency_list();
			
			$this->data['group_name'] = "administrator";  
			$this->data['model_name'] = "products";  
			$this->data['common_name'] = "Products";   
			
			if(in_array(3,$this->data['userdata']['role_id']) ){
				
				
				$a_csutomer = $this->data['staff_info']['assign_customer'];
				
				if($a_csutomer != ''){
				
					if(strpos($a_csutomer,',')){
						$a_customer_array = explode(',',$a_csutomer);	
					}else{
						$a_customer_array = array($a_csutomer);	
					}
				
				}else{
					$a_customer_array = array();	
				}
				
				$this->data['task_display_count'] = $this->Job_model->record_count2(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
				),$this->data['userdata']['role_id'],$a_customer_array);
				
				
				

			}else{
			
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			}
			
			//category list
			$this->data['category_list'] = $this->Product_category_model->getList();
			
			/*
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			} 
			*/
           
      }
         
	  
	  public function index($q="ALL", $page=1, $alert=0) {  
          		
			$this->data['alert'] = $alert; 
          			
            $this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/';
			if($q == 'ALL') {
				$q = "";
			}else{
				$q = urldecode($q);
			}
			$this->data['q'] = urldecode($q);
									
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Products_model->record_count($filter, $q);
			
			//get particular ranged list
			$results = $this->Products_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start);
			
			//get product qty
			if(!empty($results)){
				foreach($results as $k => $v){
					$results[$k]['qty'] = $this->Product_log_model->sum_by_id($v['id']);
				}
			}
					
			$this->data['results'] = $results;
						
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->session->set_userdata("lastpage", $url.$page);
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  	  	 	  	  
	  
	  public function add($id=false) {  
	  
	  		$this->data['customer_list'] = $this->Customers_model->getAll();
	  
          	if($id !== false) {
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Products_model->get($id);	
				$this->data['result']['no_promo_id'] = explode(',',$this->data['result']['no_promo_id']);
				
				
				$inventory_log = $this->Product_log_model->get_all_limit($id);
				$qty_total = 0;
				foreach($inventory_log as $k => $v){
					
					$inventory_log[$k]['created_date'] = date("Y/m/d H:i",strtotime($v['created_date']));

					//check type 
					if(strpos($v['qty'],'-') === false){
						$inventory_log[$k]['type'] = 'Add Inventory';
					}else{
						$inventory_log[$k]['type'] = 'Deduct Inventory';
						$inventory_log[$k]['qty'] = substr($v['qty'],1,strlen($v['qty']));
					}
					
					$qty_total += $v['qty'];
					
				}
				
				
				
				$this->data['inventory_log'] = $inventory_log;
				$this->data['current_qty'] = $qty_total;
				
			} else {
				$this->data['mode'] = 'Add';	
			}
			
			//print_r($inventory_log);exit; 
			
			$product_name = $this->Products_model->productName_groupBy();
			
			$this->data['product_name'] = json_encode($product_name);
			 
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		            
			
      }	 
	  
	  public function view($id=false,$customer_id=false) {  
	  
          	if($id !== false) {
				$this->data['mode'] = 'View';
				$this->data['result'] = $this->Products_model->get($id);	
				$inventory_log = $this->Product_log_model->get_all_limit($id);
				$qty_total = 0;
				foreach($inventory_log as $k => $v){
					
					$inventory_log[$k]['created_date'] = date("Y/m/d H:i",strtotime($v['created_date']));

					//check type 
					if(strpos($v['qty'],'-') === false){
						$inventory_log[$k]['type'] = 'Add Inventory';
					}else{
						$inventory_log[$k]['type'] = 'Deduct Inventory';
						$inventory_log[$k]['qty'] = substr($v['qty'],1,strlen($v['qty']));
					}
					
					$qty_total += $v['qty'];
					
				}
				
				$customer_price = $this->Customers_model->get($customer_id);
				
				if(!empty($customer_price)){
					
					if($customer_price['price_list'] != ''){
						
						$price_list = json_decode($customer_price['price_list'],true);
						
						foreach($price_list as $k => $v){
						
							if($v['id'] == $id){
								
								$this->data['s_price'] = $v['price'];
								
							}
						
						}
						
					}
					
					
					
				}
				
				//print_r($customer_price);exit;
				
				
				$this->data['inventory_log'] = $inventory_log;
				$this->data['current_qty'] = $qty_total;
				
			}
			
			$product_name = $this->Products_model->productName_groupBy();
			
			$this->data['product_name'] = json_encode($product_name);
			 
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		            
			
      } 	
	  
	  public function del($id) {
		  
		  $employee_data = $this->Products_model->get($id);		  
		  $this->Products_model->delete($id);		  
		  
		  //audit log
		  $log_array = array(
			'ip_address'	=> $this->input->ip_address(),
			'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
			'table_affect'	=> 'product',
			'description'	=> 'Delete product',
			'created_date'	=> date('Y-m-d H:i:s'),
		  );
			  
		  $audit_id = $this->Audit_log_model->insert($log_array);	
		  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
		  $update_array = array(
			 'log_no'	=> $custom_code,
		  );
		  $this->Audit_log_model->update($audit_id, $update_array);
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage.'/3','refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/3'));
		  }		
		  
	  }  
	  
	  public function submit(){
		  
		  $mode 					= $this->input->post("mode", true);
		  $id 						= $this->input->post("id", true);	
		  
		  $product_category 		= $this->input->post("product_category", true);
		  $product_name 			= $this->input->post("product_name", true);
		  $product_description 		= $this->input->post("product_desc", true);		  		  		  
		  $status 					= $this->input->post("status", true);		  	  
	  
		  $model_name 				= $this->input->post("model_name", true);		  
		  $supplier 				= $this->input->post("supplier", true);		  
		  $selling_price 			= $this->input->post("selling_price", true);
		  $promo_price 				= $this->input->post("promo_price", true);
		  
		  $p_start_date 			= $this->input->post("p_start_date", true);
		  if($p_start_date  != ''){
			  $p_start_date = explode('/',$p_start_date);
			  $p_start_date = $p_start_date[2].'-'.$p_start_date[1].'-'.$p_start_date[0];
		  }
		  
		  $p_end_date 				= $this->input->post("p_end_date", true);
		  if($p_end_date  != ''){
			  $p_end_date = explode('/',$p_end_date);
			  $p_end_date = $p_end_date[2].'-'.$p_end_date[1].'-'.$p_end_date[0];
		  }
		  
		  $p_qty 					= $this->input->post("p_qty", true);
		  $uom 						= $this->input->post("uom", true);
		  $inventory_uom 			= $this->input->post("inventory_uom", true);
		  $old_pic 					= $this->input->post("old_pic", true);
		  		  		 		  
		  $iu_array = array(	
		    'product_category'		=> $product_category,
		  	'product_name'			=> $product_name,	
			'description'			=> $product_description,		  			  	
			'status'				=> $status,			
			'model_name'			=> $model_name,
			'supplier'				=> $supplier,
			'selling_price'			=> $selling_price,
			'promo_price'			=> $promo_price,
			'p_start_date'			=> $p_start_date,
			'p_end_date'			=> $p_end_date,
			'p_qty'					=> $p_qty,
			'uom'					=> $uom,	
			'inventory_uom'			=> $inventory_uom,	
			'pic_path'				=> $old_pic,		
		  );	
		  
		  //no promo list
		  $no_promo = isset($_POST['no_promo'])?$_POST['no_promo']:array();
		  $no_promo = implode(',',$no_promo);
		  $iu_array['no_promo_id'] = $no_promo;
		  
		  //print_r($no_promo);exit;
		  //echo $iu_array['no_promo_id'];exit;
		  
		  //upload pic
		  //print_r($_FILES);exit;
		  
		  if(isset($_FILES['pic'])){
				
				if ( $_FILES['pic']['error'] == 0 && $_FILES['pic']['name'] != ''  ){									
					$pathinfo = pathinfo($_FILES['pic']['name']);
					$ext = $pathinfo['extension'];
					$ext = strtolower($ext);
														
					$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
					$path = "./uploads/".$filename.'.'.$ext;
					$save_path = base_url()."uploads/".$filename.'.'.$ext;
					$result = move_uploaded_file($_FILES['pic']['tmp_name'], $path);
					if(!$result) {
						die("cannot upload file");
					}	
										
					$iu_array['pic_path']= $save_path;									
				}	
									
		  }
		  	 		  
		  		  		  
		  //Add
		  if($mode == 'Add') {			  			  
			  			  
			  $iu_array['created_date'] = date("Y-m-d H:i:s");			  
			  $product_id = $this->Products_model->insert($iu_array);			  			  		  	  			  
			  $custom_code = $this->Products_model->zerofill($product_id);
			  $update_array = array(
			  	'product_no'	=> $custom_code,
			  );
			  $this->Products_model->update($product_id, $update_array);
			  
			  
			  //insert stock qty
			  $qty_json = $this->input->post("qty_json", true);
			  
			  if($qty_json != ''){
				  
				  $qty_json = json_decode($qty_json,true);
				  
				  foreach($qty_json as $v){
					  
					  $data_array = array(
					  	'product_id'	=>$product_id,
						'qty'			=>$v['qty'],
						'reason'		=>$v['reason'],
						'employee_no'	=> $this->data['staff_info']['employee_no'],
						'employee_name'	=> $this->data['staff_info']['full_name'],
						'created_date'	=>$v['time'],
					  );
					  
					  $this->Product_log_model->insert($data_array);
					  
				  }
				  
			  }
			  
			  //audit log
			  $log_array = array(
				'ip_address'	=> $this->input->ip_address(),
				'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
				'table_affect'	=> 'product',
				'description'	=> 'Added product',
				'created_date'	=> date('Y-m-d H:i:s'),
			  );
				  
			  $audit_id = $this->Audit_log_model->insert($log_array);	
			  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
			  $update_array = array(
				 'log_no'	=> $custom_code,
			  );
			  $this->Audit_log_model->update($audit_id, $update_array);

			  
		  //Edit	  			  
		  } else {
			  
			  $iu_array['modified_date'] = date("Y-m-d H:i:s");			  
			  $this->Products_model->update($id, $iu_array);
			  
			  //audit log
			  $log_array = array(
				'ip_address'	=> $this->input->ip_address(),
				'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
				'table_affect'	=> 'product',
				'description'	=> 'Edit product',
				'created_date'	=> date('Y-m-d H:i:s'),
			  );
				  
			  $audit_id = $this->Audit_log_model->insert($log_array);	
			  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
			  $update_array = array(
				 'log_no'	=> $custom_code,
			  );
			  $this->Audit_log_model->update($audit_id, $update_array);			  			  			  
			  
		  }
		  		  		 		  		  
		  //alert
		  if($mode == 'Add'){
			$alert_type = '/1';
		  }else{
			$alert_type = '/2';
		  }
		  
		  		  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage.$alert_type,'refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].$alert_type));
		  }			  
		  
	  }
	  
	  public function update_qty(){
		  
		   $id = $_POST['id'];
		   $qty = $_POST['qty'];
		   $reason = $_POST['reason'];
		   $now = date("Y-m-d H:i:s");
		   
		   $array = array(
		   	'product_id' 	=> $id,
			'qty' 			=> $qty,
			'reason'		=> $reason,
			'employee_no'	=> $this->data['staff_info']['employee_no'],
			'employee_name'	=> $this->data['staff_info']['full_name'],
			'created_date'	=> $now,
		   );
		   
		   $this->Product_log_model->insert($array);
		   
		   $json = array(
		   	'status'  =>'OK',
		   );
		   
		  //audit log
		  $log_array = array(
			'ip_address'	=> $this->input->ip_address(),
			'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
			'table_affect'	=> ' product_log',
			'description'	=> 'Update product quantity',
			'created_date'	=> date('Y-m-d H:i:s'),
		  );
			  
		  $audit_id = $this->Audit_log_model->insert($log_array);	
		  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
		  $update_array = array(
			 'log_no'	=> $custom_code,
		  );
		  $this->Audit_log_model->update($audit_id, $update_array);
		   
		   echo json_encode($json);exit;
		   
	  }
	  
	  public function test(){

          $this->load->view('anexus/'.$this->data['model_name'].'/test', $this->data);

	  }
	  
	  public function update_reason(){
		  
		   $id = $_POST['id'];
		   $reason = $_POST['reason'];
		   
		   $array = array('reason'=>$reason,'modified_date'=>date('Y-m-d H:i:s'));
		   
		   $this->Product_log_model->update($id,$array);
		   
		   $json = array('status'=>'ok');
		   
		   echo json_encode($json);exit;
		   
	  }
	  
	  public function ajax_inventory_log(){
		  
		  $id = $_POST['id'];
		  $start_date = $_POST['start_date'];
		  $end_date = $_POST['end_date'];
		  
		  
		  		$inventory_log = $this->Product_log_model->get_by_date_range($id,$start_date,$end_date);
				foreach($inventory_log as $k => $v){
					
					$inventory_log[$k]['created_date'] = date("Y/m/d H:i",strtotime($v['created_date']));

					//check type 
					if(strpos($v['qty'],'-') === false){
						$inventory_log[$k]['type'] = 'Add Inventory';
					}else{
						$inventory_log[$k]['type'] = 'Deduct Inventory';
						$inventory_log[$k]['qty'] = substr($v['qty'],1,strlen($v['qty']));
					}
					
				}
				
				$json = array(
					'status'=>'ok',
					'data' => $inventory_log,
				);
		   
		   		echo json_encode($json);exit;
		  
		  
	  }
	  
	  function products_check(){
		//print_r($_REQUEST);
	  	$product_name = $_POST['name'];
		
		$check = $this->Products_model->get_where(array(
			'is_deleted' => 0,
			'product_name' => $product_name,
		));
		
		//print_r($check);exit;
		
		if(!empty($check)){
			$isset = 1;
		}else{
			$isset = 0;
		}
		
	  	$json = array(
			'status' => 'ok',
			'isset' => $isset,
		);
	  
	  	echo json_encode($json);exit;
	  
	  }
	  
}

?>