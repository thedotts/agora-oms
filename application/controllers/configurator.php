<?php
require_once(APPPATH.'../install/includes/database_class.php');
require_once(APPPATH.'../install/includes/core_class.php');
	
class Configurator extends CI_Controller {
	  
      public $data = array();
      public function __construct() {
            parent::__construct();
			
			$all_config = $this->_load_data();
			
			$this->load->model('Check_model');
			//如果系統已經安裝了就直接去登入吧
			if( $this->Check_model->is_installed() ) {
				 redirect('/en/login', 'refresh');
				 exit;
			}
      }

   	  //如果還沒有就開始安裝
      public function index() {
            redirect('/en/configurator/dashboard', 'refresh');

      }	  	  

	  public function dashboard() {
          			
            $this->data['title'] = ucfirst("Dashboard"); // Capitalize the first letter		
            $this->load->view('configurator/header', $this->data);
            $this->load->view('configurator/dashboard', $this->data);
            $this->load->view('configurator/footer', $this->data);
			
      }
	  
	  public function access(){
			$current_data = $this->_load_data();
	  		$this->data['roles'] = $current_data['roles'];
			
			//print_r($current_data['roles']);
			//exit;
			$access_list = array();
			/*
			$access_list = array(
				'C,R,U,D' 	=> 'full',
				'C,R,U' 	=> 'create/update/view',
				'C,OR,OU' 	=> 'create/update/view own record only',
				'X'			=> 'no access',
				'R'			=> 'read only',
				'C,OR'		=> 'create/view own record only'
			);
			*/	
			$access_list = array(
				'C,R,U,D' 	=> 'C,R,U,D',
				'C,R,U' 	=> 'C,R,U',
				'C,OR,OU' 	=> 'C,OR,OU',
				'X'			=> 'X',
				'R'			=> 'R',
				'C,OR'		=> 'C,OR'
			);		
			
			$milestone = array(
				'quotation_control'				=> 'Quotation', //1
				'tender_control'				=> 'Tender', //2
				'purchase_control'				=> 'Purchase', //3
			  	'service_control'				=> 'Service', //4
				'delivery_order_control'		=> 'Delivery', //5
			  	'commercial_invoice_control' 	=> 'Commercial Invoice', //6
			  	'service_order_control'			=> 'Field Service', //7
			  	
									  	
			);
			
			$milestone2 = array(		
				'inventory_control'				=> 'Inventory', //8		
			  	'rma_control'					=> 'Return Material Authorization', //9
				'invoice_control'				=> 'Invoice', //10
			  	'credit_note_control'			=> 'Credit Note',	//11
				'trip_control'					=> 'Business Trip Report',	 //12
				'manage_employee_control'		=> 'Manage Employee', //13
			  	'manage_customer_control'		=> 'Manage Customer', //14
			  	'manage_suppliers_control'		=> 'Manage Suppliers', //15
			  	
			);
			
			$milestone3 = array(
				'manage_inventory_control'		=> 'Manage Inventory', //16
				'manage_shipping_control'		=> 'Manage Shipping', //17	  	
				'admin_service_control'			=> 'Manage Service', //18	  	
				'department_control'			=> 'Manage Department', //19
				'settings_control'				=> 'Settings', //20			  	
			  	'all_job_control'				=> 'All Jobs', //21
			  	'all_customer_control'			=> 'All Customers', //22			  	
			  	'reporting_control'				=> 'Reporting', //23
			);
			
			$default_roles_privileges = array(
				'1'	=> array( //SuperAdmin
					'quotation_control'				=> 'C,R,U,D', //1
					'tender_control'				=> 'C,R,U,D', //2
					'purchase_control'				=> 'C,R,U,D', //3
			  		'service_control'				=> 'C,R,U,D', //4
					'delivery_order_control'		=> 'C,R,U,D', //5
			  		'commercial_invoice_control' 	=> 'C,R,U,D', //6
			  		'service_order_control'			=> 'C,R,U,D', //7
			  		'inventory_control'				=> 'C,R,U,D', //8		
					'rma_control'					=> 'C,R,U,D', //9
					'invoice_control'				=> 'C,R,U,D', //10
					'credit_note_control'			=> 'C,R,U,D',	//11
					'trip_control'					=> 'C,R,U,D',	 //12
					'manage_employee_control'		=> 'C,R,U,D', //13
					'manage_customer_control'		=> 'C,R,U,D', //14
					'manage_suppliers_control'		=> 'C,R,U,D', //15
					'manage_inventory_control'		=> 'C,R,U,D', //16
					'manage_shipping_control'		=> 'C,R,U,D', //17
					'admin_service_control'			=> 'C,R,U,D', //18	  	
					'department_control'			=> 'C,R,U,D', //19
					'settings_control'				=> 'C,R,U,D', //20			  	
					'all_job_control'				=> 'R', //21
					'all_customer_control'			=> 'R', //22			  	
					'reporting_control'				=> 'R', //23	
				),
				'2'	=> array( //GM
					'quotation_control'				=> 'R', //1
					'tender_control'				=> 'R', //2
					'purchase_control'				=> 'R', //3
			  		'service_control'				=> 'R', //4
					'delivery_order_control'		=> 'R', //5
			  		'commercial_invoice_control' 	=> 'R', //6
			  		'service_order_control'			=> 'R', //7
			  		'inventory_control'				=> 'R', //8		
					'rma_control'					=> 'R', //9
					'invoice_control'				=> 'R', //10
					'credit_note_control'			=> 'R',	//11
					'trip_control'					=> 'R',	 //12
					'manage_employee_control'		=> 'R', //13
					'manage_customer_control'		=> 'R', //14
					'manage_suppliers_control'		=> 'R', //15
					'manage_inventory_control'		=> 'R', //16
					'manage_shipping_control'		=> 'R', //17
					'admin_service_control'			=> 'R', //18	  	
					'department_control'			=> 'R', //19
					'settings_control'				=> 'R', //20			  	
					'all_job_control'				=> 'R', //21
					'all_customer_control'			=> 'R', //22			  	
					'reporting_control'				=> 'R', //23	
				),
				'3'	=> array( //Service Manager
					'quotation_control'				=> 'X', //1
					'tender_control'				=> 'X', //2
					'purchase_control'				=> 'X', //3
			  		'service_control'				=> 'R', //4
					'delivery_order_control'		=> 'X', //5
			  		'commercial_invoice_control' 	=> 'X', //6
			  		'service_order_control'			=> 'C,R,U', //7
			  		'inventory_control'				=> 'C,R,U', //8		
					'rma_control'					=> 'C,R,U', //9
					'invoice_control'				=> 'X', //10
					'credit_note_control'			=> 'X',	//11
					'trip_control'					=> 'C,R,U',	 //12
					'manage_employee_control'		=> 'X', //13
					'manage_customer_control'		=> 'X', //14
					'manage_suppliers_control'		=> 'X', //15
					'manage_inventory_control'		=> 'X', //16
					'manage_shipping_control'		=> 'X', //17
					'admin_service_control'			=> 'X', //18	  	
					'department_control'			=> 'X', //19
					'settings_control'				=> 'X', //20			  	
					'all_job_control'				=> 'R', //21
					'all_customer_control'			=> 'R', //22			  	
					'reporting_control'				=> 'R', //23	
				),
				'4'	=> array( //Service Executive
					'quotation_control'				=> 'X', //1
					'tender_control'				=> 'X', //2
					'purchase_control'				=> 'X', //3
			  		'service_control'				=> 'R', //4
					'delivery_order_control'		=> 'X', //5
			  		'commercial_invoice_control' 	=> 'X', //6
			  		'service_order_control'			=> 'C,OR', //7
			  		'inventory_control'				=> 'C,OR,OU', //8		
					'rma_control'					=> 'C,OR', //9
					'invoice_control'				=> 'X', //10
					'credit_note_control'			=> 'X',	//11
					'trip_control'					=> 'C,OR,OU',	 //12
					'manage_employee_control'		=> 'X', //13
					'manage_customer_control'		=> 'X', //14
					'manage_suppliers_control'		=> 'X', //15
					'manage_inventory_control'		=> 'X', //16
					'manage_shipping_control'		=> 'X', //17
					'admin_service_control'			=> 'X', //18	  	
					'department_control'			=> 'X', //19
					'settings_control'				=> 'X', //20			  	
					'all_job_control'				=> 'R', //21
					'all_customer_control'			=> 'R', //22			  	
					'reporting_control'				=> 'X', //23	
				),
				'5'	=> array( //Finance
					'quotation_control'				=> 'X', //1
					'tender_control'				=> 'X', //2
					'purchase_control'				=> 'X', //3
			  		'service_control'				=> 'R', //4
					'delivery_order_control'		=> 'X', //5
			  		'commercial_invoice_control' 	=> 'X', //6
			  		'service_order_control'			=> 'X', //7
			  		'inventory_control'				=> 'X', //8		
					'rma_control'					=> 'X', //9
					'invoice_control'				=> 'C,R,U', //10
					'credit_note_control'			=> 'C,R,U',	//11
					'trip_control'					=> 'X',	 //12
					'manage_employee_control'		=> 'X', //13
					'manage_customer_control'		=> 'X', //14
					'manage_suppliers_control'		=> 'C,R,U', //15
					'manage_inventory_control'		=> 'X', //16
					'manage_shipping_control'		=> 'X', //17
					'admin_service_control'			=> 'X', //18	  	
					'department_control'			=> 'X', //19
					'settings_control'				=> 'X', //20			  	
					'all_job_control'				=> 'R', //21
					'all_customer_control'			=> 'R', //22			  	
					'reporting_control'				=> 'X', //23	
				),
				'6'	=> array( //Logistics
					'quotation_control'				=> 'X', //1
					'tender_control'				=> 'X', //2
					'purchase_control'				=> 'C,R,U', //3
			  		'service_control'				=> 'X', //4
					'delivery_order_control'		=> 'C,R,U', //5
			  		'commercial_invoice_control' 	=> 'C,R,U', //6
			  		'service_order_control'			=> 'X', //7
			  		'inventory_control'				=> 'C,R,U', //8		
					'rma_control'					=> 'X', //9
					'invoice_control'				=> 'X', //10
					'credit_note_control'			=> 'X',	//11
					'trip_control'					=> 'X',	 //12
					'manage_employee_control'		=> 'X', //13
					'manage_customer_control'		=> 'X', //14
					'manage_suppliers_control'		=> 'X', //15
					'manage_inventory_control'		=> 'C,R,U', //16
					'manage_shipping_control'		=> 'C,R,U', //17
					'admin_service_control'			=> 'X', //18	  	
					'department_control'			=> 'X', //19
					'settings_control'				=> 'X', //20			  	
					'all_job_control'				=> 'R', //21
					'all_customer_control'			=> 'R', //22			  	
					'reporting_control'				=> 'X', //23	
				),
				'7'	=> array( //Sales Manager
					'quotation_control'				=> 'C,R,U', //1
					'tender_control'				=> 'C,R,U', //2
					'purchase_control'				=> 'C,R,U', //3
			  		'service_control'				=> 'C,R,U', //4
					'delivery_order_control'		=> 'X', //5
			  		'commercial_invoice_control' 	=> 'X', //6
			  		'service_order_control'			=> 'X', //7
			  		'inventory_control'				=> 'X', //8		
					'rma_control'					=> 'X', //9
					'invoice_control'				=> 'X', //10
					'credit_note_control'			=> 'X',	//11
					'trip_control'					=> 'C,R,U',	 //12
					'manage_employee_control'		=> 'X', //13
					'manage_customer_control'		=> 'C,R,U', //14
					'manage_suppliers_control'		=> 'X', //15
					'manage_inventory_control'		=> 'X', //16
					'manage_shipping_control'		=> 'X', //17
					'admin_service_control'			=> 'X', //18	  	
					'department_control'			=> 'X', //19
					'settings_control'				=> 'X', //20			  	
					'all_job_control'				=> 'R', //21
					'all_customer_control'			=> 'R', //22			  	
					'reporting_control'				=> 'R', //23	
				),
				'8'	=> array( //Sales Executive
					'quotation_control'				=> 'C,OR,OU', //1
					'tender_control'				=> 'C,OR,OU', //2
					'purchase_control'				=> 'C,OR', //3
			  		'service_control'				=> 'C,OR', //4
					'delivery_order_control'		=> 'X', //5
			  		'commercial_invoice_control' 	=> 'X', //6
			  		'service_order_control'			=> 'X', //7
			  		'inventory_control'				=> 'X', //8		
					'rma_control'					=> 'X', //9
					'invoice_control'				=> 'X', //10
					'credit_note_control'			=> 'X',	//11
					'trip_control'					=> 'C,OR,OU',	 //12
					'manage_employee_control'		=> 'X', //13
					'manage_customer_control'		=> 'X', //14
					'manage_suppliers_control'		=> 'X', //15
					'manage_inventory_control'		=> 'X', //16
					'manage_shipping_control'		=> 'X', //17
					'admin_service_control'			=> 'X', //18	  	
					'department_control'			=> 'X', //19
					'settings_control'				=> 'X', //20			  	
					'all_job_control'				=> 'R', //21
					'all_customer_control'			=> 'R', //22			  	
					'reporting_control'				=> 'X', //23	
				),
				'9'	=> array( //Human Resource
					'quotation_control'				=> 'X', //1
					'tender_control'				=> 'X', //2
					'purchase_control'				=> 'X', //3
			  		'service_control'				=> 'X', //4
					'delivery_order_control'		=> 'X', //5
			  		'commercial_invoice_control' 	=> 'X', //6
			  		'service_order_control'			=> 'X', //7
			  		'inventory_control'				=> 'X', //8		
					'rma_control'					=> 'X', //9
					'invoice_control'				=> 'X', //10
					'credit_note_control'			=> 'X',	//11
					'trip_control'					=> 'X',	 //12
					'manage_employee_control'		=> 'C,R,U,D', //13
					'manage_customer_control'		=> 'X', //14
					'manage_suppliers_control'		=> 'X', //15
					'manage_inventory_control'		=> 'X', //16
					'manage_shipping_control'		=> 'X', //17
					'admin_service_control'			=> 'X', //18	  	
					'department_control'			=> 'X', //19
					'settings_control'				=> 'X', //20			  	
					'all_job_control'				=> 'X', //21
					'all_customer_control'			=> 'X', //22			  	
					'reporting_control'				=> 'R', //23	
				),
				'10'	=> array( //Purchaser
					'quotation_control'				=> 'X', //1
					'tender_control'				=> 'X', //2
					'purchase_control'				=> 'C,R,U', //3
			  		'service_control'				=> 'X', //4
					'delivery_order_control'		=> 'X', //5
			  		'commercial_invoice_control' 	=> 'X', //6
			  		'service_order_control'			=> 'X', //7
			  		'inventory_control'				=> 'X', //8		
					'rma_control'					=> 'X', //9
					'invoice_control'				=> 'X', //10
					'credit_note_control'			=> 'X',	//11
					'trip_control'					=> 'X',	 //12
					'manage_employee_control'		=> 'X', //13
					'manage_customer_control'		=> 'X', //14
					'manage_suppliers_control'		=> 'X', //15
					'manage_inventory_control'		=> 'X', //16
					'manage_shipping_control'		=> 'X', //17
					'admin_service_control'			=> 'X', //18	  	
					'department_control'			=> 'X', //19
					'settings_control'				=> 'X', //20			  	
					'all_job_control'				=> 'X', //21
					'all_customer_control'			=> 'X', //22			  	
					'reporting_control'				=> 'X', //23	
				),
			);						
			
			$this->data['default_roles_privileges'] = $default_roles_privileges;
			$this->data['milestone'] = $milestone;
			$this->data['milestone2'] = $milestone2;
			$this->data['milestone3'] = $milestone3;
			
			
		  	$this->data['control'] = $access_list;
		  	//$this->data['role'] = array('Superadmin', 'Sales Exec', 'General MGR', 'Service MGR', 'Service Exec', 'Sales Mgr');
		  	$this->data['title'] = ucfirst("Access"); // Capitalize the first letter
            $this->load->view('configurator/header', $this->data);
            $this->load->view('configurator/access', $this->data);
            $this->load->view('configurator/footer', $this->data);
	  }
	  
	  public function complete(){
		  
		  	$this->data['title'] = ucfirst("Complete"); // Capitalize the first letter
            $this->load->view('configurator/header', $this->data);
            $this->load->view('configurator/complete', $this->data);
            $this->load->view('configurator/footer', $this->data);
		  
	  }
	  
	  public function data(){
			$this->data['schema'] = $this->_parse_sql(APPPATH.'../install/anexus.sql');
			//$keys = array_keys($this->data['schema']);
			//print_r($keys);exit;
		  	$this->data['title'] = ucfirst("Data"); // Capitalize the first letter
            $this->load->view('configurator/header', $this->data);
            $this->load->view('configurator/data', $this->data);
            $this->load->view('configurator/footer', $this->data);
	  }
	  
	  public function roles(){
	  		$current_data = $this->_load_data();
	  		if(array_key_exists('roles', $current_data)){
	  			$this->data['roles'] = $current_data['roles'];
	  		}
			
			$default_parent = array();
			$default_parent[] = array(
				'id'	=> 0,
				'value'	=> "Non",
				'children'	=> array(),
			);
			$default_parent[] = array(
				'id'	=> 1,
				'value'	=> "Super Admin",
				'children'	=> array(),
			);
			$default_parent[] = array(
				'id'	=> 2,
				'value'	=> "Manager",
				'children'	=> array(3,4,5,6),
			);
			$this->data['default_parent'] = $default_parent;
			
		  	$this->data['title'] = ucfirst("Roles"); // Capitalize the first letter
            $this->load->view('configurator/header', $this->data);
            $this->load->view('configurator/roles', $this->data);
            $this->load->view('configurator/footer', $this->data);
	  }
	  
	  public function workflow(){
	  		
	  		$this->data['assignee'] = array('technician', 'sales');
		  	$this->data['title'] = ucfirst("Workflow"); // Capitalize the first letter
            $this->load->view('configurator/header', $this->data);
            $this->load->view('configurator/workflow', $this->data);
            $this->load->view('configurator/footer', $this->data);
		  
	  }
	  
	  public function workflow_order(){
	  		
			$current_data = $this->_load_data();
			$this->data['status'] = json_encode($current_data['workflow']);
			$this->data['roles'] = json_encode($current_data['roles']);
			//print_r($this->data['status']);exit;
			
	  		$this->data['assignee'] = array('technician', 'sales');
		  	$this->data['title'] = ucfirst("Workflow Order"); // Capitalize the first letter
            $this->load->view('configurator/header', $this->data);
            $this->load->view('configurator/workflow_order', $this->data);
            $this->load->view('configurator/footer', $this->data);
		  
	  }
	  
	  public function settings(){
   			$this->data['check_db'] = 0;
		  	$this->data['title'] = ucfirst("Settings"); // Capitalize the first letter
			$this->data['error'] = '';
            $this->load->view('configurator/header', $this->data);
            $this->load->view('configurator/settings', $this->data);
            $this->load->view('configurator/footer', $this->data);
		  
	  }
   	  public function db_connect_test(){
   		    $db_name = ($_POST['db_name']!='')?$_POST['db_name']:'';
		    $db_usr = ($_POST['db_usr']!='')?$_POST['db_usr']:'';
		    $db_passwd = ($_POST['db_passwd']!='')?$_POST['db_passwd']:'';
		    $db_host = ($_POST['db_host']!='')?$_POST['db_host']:'';
		  	
			$test['hostname'] = $db_host;
			$test['username'] = $db_usr;
			$test['password'] = $db_passwd;
			$test['database'] = $db_name;
			$test['dbdriver'] = 'mysql';
			//if password is empty, definitely cannot login database
			if($test['password'] == ''){
				echo(0);
			}else{
	  			$CI =& get_instance();
				$rr = $CI->load->database($test, TRUE);
		        $connected = $rr->initialize();
				if($connected){

					$data = array(
						'db_host' => $db_host,
						'db_usr' => $db_usr,
						'db_passwd' => $db_passwd,
						'db_name' => $db_name,
					);
					
					$the_database = new Database();
					if($the_database->create_database($data) == false) {
						echo(0);
					} else {
						echo(1);	
					}
					
					
				}else{
					echo(0);
				}
			}
	  }
	  public function _load_data(){
		$saved_config	= APPPATH.'/config/saved_config.json';
		$config_content_json = file_get_contents($saved_config);
		$current_data = json_decode($config_content_json, true);
		return $current_data;
	  }

	  public function write_data($section, $data){
   	    //write data collected from configuator into a json file
		// Config path
		$saved_config	= APPPATH.'/config/saved_config.json';
		$current_data = $this->_load_data();
		$current_data[$section] = $data;

		file_put_contents($saved_config, json_encode($current_data));
	  }
	  public function _write_log($data){
	  	$log = APPPATH.'../install/log.txt';
	  	file_put_contents($log, 'log:'.$data."\n", FILE_APPEND);
	  }

	  public function saving_config(){
		  $section = (isset($_POST['section']))?$_POST['section']:'';
		  switch ($section){
			case "settings":
   			    $data['db_name'] 				= $this->input->post("db_name", true);
   			    $data['db_host'] 				= $this->input->post("db_host", true);
   			    $data['db_usr'] 				= $this->input->post("db_usr", true);
   			    $data['db_passwd'] 				= $this->input->post("db_passwd", true);
				$data['company_name'] 			= $this->input->post("company_name", true);
				$data['company_address'] 		= $this->input->post("company_address", true);
				$data['company_postal'] 		= $this->input->post("company_postal", true);
   			    $data['site_name'] 				= $this->input->post("site_name", true);
   			    $data['logo_url'] 				= $this->input->post("logo_url", true);				
				$data['quotation_sale_value'] 	= $this->input->post("quotation_sale_value",true);
				$data['quotation_service_value'] 	= $this->input->post("quotation_service_value", true);
				$data['purchase_value'] 		= $this->input->post("purchase_value", true);
				$data['gst_percentage']			= $this->input->post("gst_percentage", true);
				$data['default_email'] 			= $this->input->post("default_email", true);
				$data['main_contact_person'] 	= $this->input->post("main_contact_person", true);
				$data['main_contact_no'] 		= $this->input->post("main_contact_no", true);
				$data['payment_information'] 	= $this->input->post("payment_information", true);
				$data['purchase_order_terms'] 	= $this->input->post("purchase_order_terms", true);
				$data['payment_terms'] 			= $this->input->post("payment_terms", true);
				$data['qty_for_reminder'] 		= $this->input->post("qty_for_reminder", true);
				$data['prefix'] 				= $this->input->post("prefix", true);
				$data['reminder_alert'] 		= $this->input->post("reminder_alert", true);	
				$data['task_reminder'] 			= $this->input->post("task_reminder", true);
				$data['task_reminder2'] 		= $this->input->post("task_reminder2", true);							
				$data['admin_email'] 			= $this->input->post("admin_email", true);
				$data['admin_pwd'] 				= $this->input->post("admin_pwd", true);

				$this->write_data($section, $data);
				$this->data();
				break;
			case "data":
				$data['data'] = json_decode((isset($_POST['data']))?$_POST['data']:'', true);
				$this->_shape_sql($data['data']);
				$this->roles();
				break;
			case "roles":
   			    $data['roles'] = json_decode((isset($_POST['roles']))?$_POST['roles']:'', true);
   			    $this->write_data($section, $data['roles']['roles']);
				break;
			case "workflow":
				$data['workflow'] = json_decode((isset($_POST['workflow']))?$_POST['workflow']:'', true);
				$this->write_data($section, $data['workflow']);
				break;
			case "workflow_order":
				$data['workflow_order'] = json_decode((isset($_POST['workflow_order']))?$_POST['workflow_order']:'', true);
				$this->write_data($section, $data['workflow_order']);
				$this->_generate_data();
				break;
			case "access":
				$data['access'] = json_decode((isset($_POST['access']))?$_POST['access']:'', true);
				$this->write_data($section, $data['access']['access']);
				$this->_generate_data();
				break;
			case "complete":
				$data = $this->_load_data();
				$this->_create_db_tables($data['settings']);
				break;
		  }
	  }
	  public function _parse_sql($sql){
  		  	//$schema = file_get_contents($sql);
  		  	$sql_schema = file($sql);
			$schema;
			$flag = 0;
			$table_header;
			$table_name;
			
			//print_r($sql_schema);exit;
			
			foreach($sql_schema as $s){
				if(preg_match("/CREATE TABLE `(.+)` \(/", $s, $matches)){
					$flag = 1;
					$table_name = $matches[1];
					//print_r($matches);
					$schema[$table_name]['header'] = $matches[0];
				}else if(preg_match("/\) ENGINE=MyISAM.+/", $s, $matches)){
					$flag = 0;
					$schema[$table_name]['footer'] = $matches[0];
				}
				if($flag == 1){
					#capture structures
					if(preg_match("/`(.+)` (.+?) (.+),/", $s, $matches)){
						$schema[$table_name][$matches[1]]['type'] = $matches[2];
						$schema[$table_name][$matches[1]]['other'] = $matches[3];
					}else{
						$s = trim(preg_replace('/\s$/', '', $s));
						$schema[$table_name]['primary_key'] = $s;
					}
				}
			}
			return $schema;
			
			
	  }

	  public function _shape_sql($data){
	  	$schema = $this->_parse_sql(APPPATH.'../install/anexus.sql');
		
		//clear all things in anexus_custom.sql first				
	  	$custom_sql = APPPATH.'../install/anexus_custom.sql';
		
		$fp = fopen($custom_sql, "w");
		fwrite($fp, "");
		fclose($fp);
		
	  	$log = APPPATH.'../install/log.txt';
	  	$columns = array_keys($data);
	  	//file_put_contents($log, count($data));
	  	$foreign;
	  	foreach($columns as $c){
	  			$column_array = explode("@", $c);
	  			$column_name = $column_array[0];
	  			$table_name = $column_array[1];
	  			$foreign[$table_name][$column_name] = $data[$c];
	  	}
	  	$table_name = array_keys($schema );
	  	foreach($table_name as $t){
	  		$this_fields = array_keys($schema[$t]);
	  		file_put_contents($custom_sql, $schema[$t]['header']. "\n", FILE_APPEND);
	  		foreach($this_fields as $f){
	  			if($f == 'header' || $f == 'footer' || $f == 'primary_key'){
	  			}else{
	  				$this_desc = '`'. $f . '` '. $schema[$t][$f]['type'].' '.$schema[$t][$f]['other']. ",\n";
	  				file_put_contents($custom_sql, $this_desc, FILE_APPEND);
	  			}
	  		}
	  		$all_fkeys = array_keys($foreign[$t]);
	  		$pk_comma = (count($all_fkeys)==0)?"\n":",\n";
	  		file_put_contents($custom_sql, $schema[$t]['primary_key'].$pk_comma, FILE_APPEND);
	  		$count_fkeys = count($all_fkeys);
	  		$fk_cnt = 1;
	  		foreach( $all_fkeys as $fk){
	  			$fk_comma = ($count_fkeys==$fk_cnt)?'':',';
	  			$this_fk_desc = 'FOREIGN KEY (`' . $fk . '`) REFERENCES `' . $foreign[$t][$fk] . '`(`id`)'. $fk_comma . "\n";
	  			file_put_contents($custom_sql, $this_fk_desc, FILE_APPEND);
	  			$fk_cnt++;
	  		}//FOREIGN KEY (`xxx_id`) REFERENCES xxx(`id`),
	  		file_put_contents($custom_sql, $schema[$t]['footer'], FILE_APPEND);
	  		file_put_contents($custom_sql, "\n\n", FILE_APPEND);
	  	}
	  }
	  public function _write_config($data) {

		// Config path
		$template_path 	= APPPATH.'/config/database.php.example';
		$output_path 	= APPPATH.'/config/database.php';

		// Open the file
		$database_file = file_get_contents($template_path);
		$new  = str_replace("%HOSTNAME%", $data['db_host'],$database_file);
		$new  = str_replace("%USERNAME%", $data['db_usr'],$new);
		$new  = str_replace("%PASSWORD%", $data['db_passwd'],$new);
		$new  = str_replace("%DATABASE%", $data['db_name'],$new);

		// Write the new database.php file
		#$json_handle = fopen($saved_config,'w+');
		$handle = fopen($output_path,'w+');

		// Chmod the file, in case the user forgot
		$result = chmod($output_path,0777);
		if(!$result){
			return false;
		}

		// Verify file permissions
		if(is_writable($output_path)) {
			// Write the file
			if(fwrite($handle,$new)) {
				fclose($handle);
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	  }
  	  public function _create_db_tables($data){
  	  	$the_database = new Database();
		if($the_database->create_database($data) == false) {
			//$message = $this->_write_log('error',"The database could not be created, please verify your settings.");
			$message = "The database could not be created, please verify your settings";
		} else if ($the_database->create_tables($data) == false) {
			//$message = $this->_write_log('error',"The database tables could not be created, please verify your settings.");
			$message = "The database tables could not be created, please verify your settings.";
		}
		
		//all seems ok, we change the config/database.php
		$result = $this->_write_config($data);
		if(!$result) {
			show_error('application/config/database.php has problem to write in');	
		}

		if(!isset($message)) {
			return true;
		}else{
			$this->_write_log($message);
		}
	  }
	  public function _generate_data(){
		$all_config = $this->_load_data();
	  	$custom_sql = APPPATH.'../install/anexus_custom.sql';
	  	$each_keys = array_keys($all_config);
	  	foreach($each_keys as $c){
	  		switch($c){
	  			case 'settings':
				
					//site name
	  				$this_title = $all_config[$c]['site_name'];
	  				$this_insert = "INSERT INTO `settings` VALUES ('', 'site name', '', '$this_title', '".date("Y-m-d H:i:s")."');\n";
					
					//logo url
					$logo_url = $all_config[$c]['logo_url'];
	  				$this_insert .= "INSERT INTO `settings` VALUES ('', 'logo url', '', '$logo_url', '".date("Y-m-d H:i:s")."');\n";										
					
					//admin user
					$admin_email = $all_config[$c]['admin_email'];
					$admin_pwd = md5($all_config[$c]['admin_pwd']);					
					$this_insert .= "INSERT INTO `user` VALUES ('', '1', 'Admin', '$admin_email', '$admin_pwd', '1', '0', '".date("Y-m-d H:i:s")."', '0000-00-00 00:00:00');\n";	
					
					//admin employee
					$this_insert .= "INSERT INTO `employee` VALUES ('', '1', '00001', '1', '', '', '1', '', '', '', '', 'Admin', '', '', '', '', '', '', '', '', '','','', '".date("Y-m-d H:i:s")."', '0000-00-00 00:00:00','0');\n";					
					
					//Quotation Sales Value
					$quotation_sale_value = $all_config[$c]['quotation_sale_value'];
	  				$this_insert .= "INSERT INTO `settings` VALUES ('', 'quotation_sale_value', 'The amount of sales value that requires approval by manager /management', '$quotation_sale_value', '".date("Y-m-d H:i:s")."');\n";										
					
					//Purchase Order Purchase Value
					$purchase_value = $all_config[$c]['purchase_value'];
	  				$this_insert .= "INSERT INTO `settings` VALUES ('', 'purchase_value', 'The amount of sales value that requires approval by manager /management', '$purchase_value', '".date("Y-m-d H:i:s")."');\n";										
					
					//GST Percentage
					$gst_percentage = $all_config[$c]['gst_percentage'];
	  				$this_insert .= "INSERT INTO `settings` VALUES ('', 'gst_percentage', 'The percentage of GST', '$gst_percentage', '".date("Y-m-d H:i:s")."');\n";										
					
					//Default Email 
					$default_email = $all_config[$c]['default_email'];
	  				$this_insert .= "INSERT INTO `settings` VALUES ('', 'default_email', 'Default Email Address of Company', '$default_email', '".date("Y-m-d H:i:s")."');\n";										
					
					//Main Contact Person
					$main_contact_person = $all_config[$c]['main_contact_person'];
	  				$this_insert .= "INSERT INTO `settings` VALUES ('', 'main_contact_person', 'Main Contact Person of Company', '$main_contact_person', '".date("Y-m-d H:i:s")."');\n";										
					
					//Main Contact No.
					$main_contact_no = $all_config[$c]['main_contact_no'];
	  				$this_insert .= "INSERT INTO `settings` VALUES ('', 'main_contact_no', 'Main Contact No.', '$main_contact_no', '".date("Y-m-d H:i:s")."');\n";										
					
					//Payment Information
					$payment_information = $all_config[$c]['payment_information'];
	  				$this_insert .= "INSERT INTO `settings` VALUES ('', 'payment_information', 'To be used in invoice', '$payment_information', '".date("Y-m-d H:i:s")."');\n";										
					
					//Purchase Order Terms
					$purchase_order_terms = $all_config[$c]['purchase_order_terms'];
	  				$this_insert .= "INSERT INTO `settings` VALUES ('', 'purchase_order_terms', 'To be used in Purchase Order', '$purchase_order_terms', '".date("Y-m-d H:i:s")."');\n";										
					
					//List of Payment Terms
					$payment_terms = $all_config[$c]['payment_terms'];
	  				$this_insert .= "INSERT INTO `settings` VALUES ('', 'payment_terms', '', '$payment_terms', '".date("Y-m-d H:i:s")."');\n";										
					
					//Quantity for reminder
					$qty_for_reminder = $all_config[$c]['qty_for_reminder'];
	  				$this_insert .= "INSERT INTO `settings` VALUES ('', 'qty_for_reminder', 'Default Quantity when drop below this amount to send an email reminder to default company email account', '$qty_for_reminder', '".date("Y-m-d H:i:s")."');\n";										
					
					//Pre-fixed for Document
					$prefix = $all_config[$c]['prefix'];
	  				$this_insert .= "INSERT INTO `settings` VALUES ('', 'prefix', 'Admin to be able to change all pre-fixed alphabet to be displayed in front of all serial numbers. ', '$prefix', '".date("Y-m-d H:i:s")."');\n";										
					
					//Reminder Alert
					$reminder_alert = $all_config[$c]['reminder_alert'];
	  				$this_insert .= "INSERT INTO `settings` VALUES ('', 'reminder_alert', 'Admin to be able to change reminder alert for PO generation (date generated), Quotation Generation (date generated) and Waiting for Good (use date of delivery from Purchase)', '$reminder_alert', '".date("Y-m-d H:i:s")."');\n";										
					
					//company name
	  				$this_company_name = $all_config[$c]['company_name'];
	  				$this_insert .= "INSERT INTO `settings` VALUES ('', 'company_name', '', '$this_company_name', '".date("Y-m-d H:i:s")."');\n";
					
					//company address
					$this_company_address = $all_config[$c]['company_address'];
					$this_insert .= "INSERT INTO `settings` VALUES ('16', 'company address', '', '$this_company_address', '".date("Y-m-d H:i:s")."');\n";
					//company postal
					$this_company_postal = $all_config[$c]['company_postal'];
					$this_insert .= "INSERT INTO `settings` VALUES ('25', 'company postal', '', '$this_company_postal', '".date("Y-m-d H:i:s")."');\n";
					
					//reminded1
					$reminded1 = $all_config[$c]['task_reminder'];
					$this_insert .= "INSERT INTO `settings` VALUES ('26', 'reminded_1', '', '$reminded1', '".date("Y-m-d H:i:s")."');\n";
					
					//reminded2
					$reminded2 = $all_config[$c]['task_reminder2'];
					$this_insert .= "INSERT INTO `settings` VALUES ('27', 'reminded_2', '', '$reminded2', '".date("Y-m-d H:i:s")."');\n";
					
					//promotion
					$this_insert .= "INSERT INTO `promotion` VALUES ('1', '','".date("Y-m-d H:i:s")."','','0');\n";
					
	  				file_put_contents($custom_sql, $this_insert, FILE_APPEND);
	  				break;
	  			case 'roles':
	  				foreach($all_config[$c] as $each_role){
	  					//INSERT INTO `role` VALUES (1, 'Superadmin', 0, 0, '0000-00-00 00:00:00');
	  					$this_insert = "INSERT INTO `role` VALUES ('" . $each_role["id"]. "', '". $each_role["name"] . "', '". $each_role["parent_id"]. "', '', '".date("Y-m-d H:i:s")."', '0');\n";
	  					file_put_contents($custom_sql, $this_insert, FILE_APPEND);
	  				}					
	  				break;
	  			case 'workflow':
	  				foreach($all_config[$c] as $k => $v){
	  					$this_insert = "INSERT INTO `workflow` VALUES ('" . $v["id"]. "', '". $v["title"] . "', '". $v["table_name"]. "', '".json_encode($v["status_json"])."');\n";
	  					file_put_contents($custom_sql, $this_insert, FILE_APPEND);
	  				}	
	  				break;
				case 'workflow_order':
	  				foreach($all_config[$c] as $k => $v){
						
						foreach($v as $k2 => $v2){
						
	  					$this_insert = "INSERT INTO `workflow_order` VALUES ('', '". $v2["status_id"] . "', '". $v2["action"]. "', '". $v2["requestor_edit"]. "', '".$v2["role_id"]."', '".$v2["workflow_id"]."', '".$v2["need_det"]."', ".json_encode($v2["formula"]).", ".json_encode($v2["target_colum"]).", '".$v2["action_before_complete"]."', '".$v2["next_status_id"]."', '".$v2["next_status_text"]."', '".$v2["ad_hoc"]."', '".$v2["who_email"]."','','','');\n";
	  					file_put_contents($custom_sql, $this_insert, FILE_APPEND);
						
						}
						
	  				}	
	  				break;
	  			case 'access':
	  				$this_access = 1;
					/*
					
					`quotation_control` varchar(50) NOT NULL,
				  `tender_control` varchar(50) NOT NULL,
				  `purchase_control` varchar(50) NOT NULL,
				  `service_control` varchar(50) NOT NULL,
				  `commercial_invoice_control` varchar(50) NOT NULL,
				  `delivery_order_control` varchar(50) NOT NULL,
				  `service_order_control` varchar(50) NOT NULL,
				  `inventory_control` varchar(50) NOT NULL,
				  `rma_control` varchar(50) NOT NULL,
				  `credit_note_control` varchar(50) NOT NULL,
				  `manage_employee_control` varchar(50) NOT NULL,
				  `manage_customer_control` varchar(50) NOT NULL,
				  `manage_suppliers_control` varchar(50) NOT NULL,
				  `manage_inventory_control` varchar(50) NOT NULL,
				  `manage_shipping_control` varchar(50) NOT NULL,
				  `settings_control` varchar(50) NOT NULL,
				  `invoice_control` varchar(50) NOT NULL,
				  `all_job_control` varchar(50) NOT NULL,
				  `all_customer_control` varchar(50) NOT NULL,
				  `admin_service_control` varchar(50) NOT NULL,
				  `department_control` varchar(50) NOT NULL,
				  `trip_control` varchar(50) NOT NULL,
				  `reporting_control` varchar(50) NOT NULL,
					
					*/
	  				foreach($all_config[$c] as $a){
	  					$this_insert = "INSERT INTO `permission` VALUES('".$this_access."', '".$a['quotation_control']."', '".$a['tender_control']."','".$a['purchase_control']."','".$a['service_control']."','".$a['commercial_invoice_control']."','".$a['delivery_order_control']."', '".$a['service_order_control']."','".$a['inventory_control']."','".$a['rma_control']."', '".$a['credit_note_control']."', '".$a['manage_employee_control']."', '".$a['manage_customer_control']."', '".$a['manage_suppliers_control']."', '".$a['manage_inventory_control']."', '".$a['manage_shipping_control']."', '".$a['settings_control']."', '".$a['invoice_control']."', '".$a['all_job_control']."', '".$a['all_customer_control']."', '".$a['admin_service_control']."', '".$a['department_control']."', '".$a['trip_control']."', '".$a['reporting_control']."', '".date("Y-m-d H:i:s")."', '','');\n";
	  					$this_update = "UPDATE `role` SET `permission_id` = '".$this_access."' WHERE `id` = '".$a['id']."';\n";
	  					file_put_contents($custom_sql, $this_insert, FILE_APPEND);
	  					file_put_contents($custom_sql, $this_update, FILE_APPEND);
	  					$this_access++;
	 				}
	  				break;
	  		}

	  	}
	  }
}
?>