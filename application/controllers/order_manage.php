<?php

/*************************
 *
 * Order Management
 *
 * *********************** */

class Order_manage extends CI_Controller {

	public $data = array();

	public function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Product_model');
		$this->load->model('Product_image_model');
		$this->load->model('Category_model');
		$this->load->model('function_model');
		$this->load->model('Order_model');
		$this->load->model('Order_log_model');	
		$this->load->model('Marketing_channel_model');
		$this->load->model('Mail_settings_model');						

		$this->data['init'] = $this->function_model->page_init();

		//This section is all about user logged in information
		//we do it in constructor, so every method will call this once
		//and use in every pages
		$this->data['item_per_page'] = $this->function_model->item_per_page();
		$this->data['webpage'] = $this->function_model->get_web_setting();
		$this->data['islogin'] = $this->function_model->isLogin();

		if ($this->data['islogin']) {
			$this->data['userdata'] = $this->session->userdata("userdata");
		}else{
			redirect(base_url('en/login'),'refresh'); 
		}  
	}

	public function sendMail($send_to,$who_send,$subject,$message)
	{

		$sCharset = 'utf-8';
		//@ 誰送的
		$sMailFrom = $who_send;	
		//@ 為了傳送 HTML 格式的 email, 我們需要設定 MIME 版本和 Content-type header 內容.
		$sHeaders = "MIME-Version: 1.0\r\n"."Content-type: text/html; charset=$sCharset\r\n"."From: $sMailFrom\r\n";

		mail($send_to,$subject, $message, $sHeaders);	
	}

	public function add($order_id=false)
	{
		$this->data['marketing_type'] = $this->Marketing_channel_model->getAllMarketing(); // all marketing channels
		$this->data['category_type'] = $this->Category_model->getList("0", 100); //all categories
		$this->data['product_type'] = $this->Product_model->get(); //all products
		$this->data['salesmans'] = $this->user_model->getparent(); //all salesmen
		$this->data['mode'] = "Add";         
		$this->load->view('templates/header', $this->data);
		$this->load->view("order/add", $this->data);
		$this->load->view('templates/footer', $this->data);
	}	

	public function index($page=1)
	{
		$this->data['marketing_type'] = $this->Marketing_channel_model->getType();
		$this->data['category_type'] = $this->Category_model->getList("0", 100); //all categories
		$this->data['product_type'] = $this->Product_model->get(); //all products
		//print_r($this->data['marketing_type'][0]);
		$searchword = isset($_POST['searchword'])?$_POST['searchword']:$this->session->userdata('searchword');
		$searchfield = isset($_POST['keyword_search'])?$_POST['keyword_search']:$this->session->userdata('keyword_search');
		$this->session->set_userdata('searchfield',$searchfield);
		$this->data['searchfield'] = $searchfield;
		//$this->data['searchfield'] = '';
		$this->session->set_userdata('searchword',$searchword);
		$this->data['searchword'] = $searchword;
		//$this->session->set_userdata('searchword','');
		//$this->data['searchword'] = '';

		$userlevel = isset($_POST['userlevel'])?$_POST['userlevel']:$this->session->userdata('userlevel');
		//show order association with this user
		$this->session->set_userdata('userlevel',$userlevel);
		$this->data['userlevel'] = $userlevel;
		$this->data['page'] = $page;
		$limit_start = ($page-1)*$this->data['item_per_page'];
		$userdata = $this->data['userdata'];            
		$this->data["total"] = $this->Order_model->record_count(($userdata['level']==3)?$userdata['id']:false,$searchword,$userlevel, $searchfield);

		$results = array();
		$results = $this->Order_model->fetchOrder($this->data['item_per_page'], $limit_start,$userdata['level'],($userdata['level']==3)?$userdata['id']:false,$searchword,$userlevel, $searchfield);
		//print_r(array_keys($results[0]));
		//exit;
		//$this->data["results"] = $this->format_user_data($results);
		$this->data["results"] = $results;
		$url = base_url().$this->data['init']['langu'].'/order/list/';
		$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);

		$this->load->view('templates/header', $this->data);
		$this->load->view("order/list", $this->data);
		$this->load->view('templates/footer', $this->data);
	}	  	 

	public function show($id) //show single booking record from Backend
	{
		$this->data['mode'] = "Show";
		$this->data['marketing_type'] = $this->Marketing_channel_model->getType(); // all marketing channels
		$this->data['category_type'] = $this->Category_model->getList("0", 100); //all categories
		$this->data['product_type'] = $this->Product_model->get(); //all products		
		$userlevel = isset($_POST['userlevel'])?$_POST['userlevel']:$this->session->userdata('userlevel');
		//show order association with this user
		$this->session->set_userdata('userlevel',$userlevel);
		$this->data['results'] = $this->Order_model->getOrderfromID($id);
		//retrieve logs
		$this->data['order_logs'] = $this->Order_log_model->getLogsfromID($id);
		$this->load->view('templates/header', $this->data);
		$this->load->view("order/add", $this->data);
		$this->load->view('templates/footer', $this->data);		  
	}

	public function deleteOrder($id)
	{
		//$this->transaction_model->delete($id); //unknown model
		$this->Order_model->deleteOrder($id);
		redirect(base_url($this->data['init']['langu'].'/order/list'), 'refresh');
	}

	public function edit ($id)
	{
		$this->data['mode'] = "Edit";
		$this->data['marketing_type'] = $this->Marketing_channel_model->getType(); // all marketing channels
		$this->data['category_type'] = $this->Category_model->getList("0", 100); //all categories
		$this->data['product_type'] = $this->Product_model->get(); //all products
		$this->data['salesmans'] = $this->user_model->getparent(); //all salesmen;
		$this->data['results'] = $this->Order_model->getOrderfromID($id);
		$this->data['order_logs'] = $this->Order_log_model->getLogsfromID($id);		
		$this->load->view('templates/header', $this->data);
		$this->load->view("order/add", $this->data);
		$this->load->view('templates/footer', $this->data);
	}

	public function update ()
	{
		$mode = isset($_POST['mode'])?$_POST['mode']:'';
        $userlevel = isset($_POST['userlevel'])?$_POST['userlevel']:$this->session->userdata('userlevel');
		if($mode == 'Add')
		{

			$id = isset($_POST['id'])?$_POST['id']:'';
			$array = array(
					'status' => isset($_POST['status'])?$_POST['status']:'',
					'marketing_id' => isset($_POST['marketing_id'])?$_POST['marketing_id']:'',
					'marketing_account' => isset($_POST['marketing_account'])?$_POST['marketing_account']:'',
					'name' => isset($_POST['name'])?$_POST['name']:'',
					'tel' => isset($_POST['tel'])?$_POST['tel']:'',
					'email' => isset($_POST['email'])?$_POST['email']:'',
					'category_id' => isset($_POST['category_id'])?$_POST['category_id']:'',
					'product_id' => isset($_POST['product_id'])?$_POST['product_id']:'',																
					'checkin_date' => isset($_POST['checkin_date'])?$_POST['checkin_date']:'',
					'checkout_date' => isset($_POST['checkout_date'])?$_POST['checkout_date']:'',
					'qty' => isset($_POST['qty'])?$_POST['qty']:'',
					'room_detail' => isset($_POST['room_detail'])?$_POST['room_detail']:'',
					'deposit' => isset($_POST['deposit'])?$_POST['deposit']:'',
					'bankin_detail' => isset($_POST['bankin_detail'])?$_POST['bankin_detail']:'',
					'bankin_method' => isset($_POST['bankin_method'])?$_POST['bankin_method']:'',
					'nationality' => isset($_POST['nationality'])?$_POST['nationality']:'',
					'is_replied' => isset($_POST['isrelied'])?$_POST['isrelied']:'0',																																				
					'is_checked' => isset($_POST['ischecked'])?$_POST['ischecked']:'0',																																				
					'checked_user_id' => isset($_POST['checked_user_id'])?$_POST['checked_user_id']:'',																																				
					'checked_remarks' => isset($_POST['checked_remarks'])?$_POST['checked_remarks']:'',
					'order_amount' => isset($_POST['order_amount'])?$_POST['order_amount']:'',
					'balance_amount' => isset($_POST['balance_amount'])?$_POST['balance_amount']:'',																																																																																																																																																																																								
					'is_deleted' => isset($_POST['is_deleted'])?$_POST['is_deleted']:'0',																																																																																																																																																																																																				
					);
					
				$usr_array = array(
						'parent_id'       => isset($_POST['parent_id'])?$_POST['parent_id']:'0',
						'level'           => '3',
						'firstname'       => isset($_POST['name'])?$_POST['name']:'',
						'lastname'        => '',
						'email'           => isset($_POST['email'])?$_POST['email']:'',
						'mobile'          => isset($_POST['tel'])?$_POST['tel']:'',
						'password'        => isset($_POST['tel'])?$_POST['tel']:'',
						'gender'		   => 'M',
						'activated'       => '1',
						'emailcode'       => '0',
						);
				//check if user email is in database
				$tmp = $this->User_model->getUserByEmail($usr_array['email']);
				if($tmp == FALSE){
					//also add user automatically
					$array['user_id'] = $this->User_model->insertUser($usr_array);
				}else{
					$array['user_id'] = $tmp['id'];
				}

				$thisOrderID = $this->Order_model->insertOrder($array); 
				//redirect to translocation notice
				if($userlevel <= 2){
					redirect(base_url($this->data['init']['langu'].'/order/list'), 'refresh');					
				}else{
					redirect(base_url($this->data['init']['langu'].'/query'), 'refresh');
				}          
		}else{
			//edit or edit from show
			$id = isset($_POST['id'])?$_POST['id']:'';
			$userdata = $this->data['userdata'];
			//insert Order log
			if($_POST['is_checked'] == 1){
				$remarks = '將對帳設為已對帳';
			}elseif($_POST['is_replied'] == 1){
				$remarks = '修改成已回覆';
			}else{
				$remarks = '修改訂單';
			}
			$log_array = array(
					'order_id' => $id,
					'user_id' => $userdata['id'],
					'remarks' => $remarks,
					);
			$this->Order_log_model->insertOrderLog($log_array); 

			$array = array(
					'status' => isset($_POST['status'])?$_POST['status']:'',
					'marketing_id' => isset($_POST['marketing_id'])?$_POST['marketing_id']:'',
					'marketing_account' => isset($_POST['marketing_account'])?$_POST['marketing_account']:'',
					'name' => isset($_POST['name'])?$_POST['name']:'',
					'tel' => isset($_POST['tel'])?$_POST['tel']:'',
					'email' => isset($_POST['email'])?$_POST['email']:'',
					'category_id' => isset($_POST['category_id'])?$_POST['category_id']:'',
					'product_id' => isset($_POST['product_id'])?$_POST['product_id']:'',																
					'checkin_date' => isset($_POST['checkin_date'])?$_POST['checkin_date']:'',
					'checkout_date' => isset($_POST['checkout_date'])?$_POST['checkout_date']:'',
					'qty' => isset($_POST['qty'])?$_POST['qty']:'',
					'room_detail' => isset($_POST['room_detail'])?$_POST['room_detail']:'',
					'deposit' => isset($_POST['deposit'])?$_POST['deposit']:'',
					'bankin_detail' => isset($_POST['bankin_detail'])?$_POST['bankin_detail']:'',
					'bankin_method' => isset($_POST['bankin_method'])?$_POST['bankin_method']:'',
					'nationality' => isset($_POST['nationality'])?$_POST['nationality']:'',
					'is_replied' => isset($_POST['is_relied'])?$_POST['is_relied']:'',																																				
					'is_checked' => isset($_POST['is_checked'])?$_POST['is_checked']:'',																																				
					'checked_user_id' => isset($_POST['checked_user_id'])?$_POST['checked_user_id']:'',																																				
					'checked_remarks' => isset($_POST['checked_remarks'])?$_POST['checked_remarks']:'',
					'user_id' => isset($_POST['user_id'])?$_POST['user_id']:'',
					'order_amount' => isset($_POST['order_amount'])?$_POST['order_amount']:'',
					'balance_amount' => isset($_POST['balance_amount'])?$_POST['balance_amount']:'',																																																																																												
					'modified_date' => date("Y-m-d H:i:s"),																																																																																												
					'is_deleted' => isset($_POST['is_deleted'])?$_POST['is_deleted']:'',	
					);
			$this->Order_model->updateOrder($id, $array);
			//redirect to somewehere, maybe backend
			redirect(base_url($this->data['init']['langu'].'/order/list'), 'refresh');
		}
	}
	public function mail ($id)
	{
		$type = "0";
		$userdata = $this->data['userdata'];
		//retrieve user email & mail content
		$this->data['results'] = $this->Order_model->getOrderfromID($id);
		$this->data['mails'] = $this->Mail_settings_model->get_mailContentFromCategory($this->data['results']['category_id'], $type);
		$log_array = array(
				'order_id' => $id,
				'user_id' => $userdata['id'],
				'remarks' => '修改成已回覆',
				);
		$this->Order_log_model->insertOrderLog($log_array); 		

		$this->sendMail('cot.kuo@i-tea.com.tw', 'cot.kuo@i-tea.com.tw', 'Email Test', $this->data['mails']['content']); // test

		redirect(base_url($this->data['init']['langu'].'/order/list'), 'refresh');		
	}
	
	public function cal_display ($category_id=false)
	{
		$userdata = $this->data['userdata'];
		$this->data['category_type'] = $this->Category_model->getList("0", 100); //all categories
		$this->data['product_type'] = $this->Product_model->get(); //all products
		if($userdata['level']<=2){
			if($category_id != ''){
				$this->data['results'] = $this->Order_model->getOrderStatusPaid($category_id);
			}else{
				$this->data['results'] = $this->Order_model->getOrderStatusPaid();
			}
		    //if (!$this->data['results']) show_404();
			$data = array();
			foreach($this->data['results'] as $key => $value){
				//id, title (category + product), start, end, url, category, product
				$data[$key]['id'] = $this->data['results'][$key]['id'];
				$product_id = $this->data['results'][$key]['product_id'];
				$category_id = $this->data['results'][$key]['category_id'];
				$product_name = $this->data['product_type'][$product_id]['title'];
				$data[$key]['backgroundColor'] = '#'.substr(md5($category_id), 0, 6);
				$data[$key]['borderColor'] = '#000';
				$data[$key]['title'] = $this->data['results'][$key]['name'] . ' @ '. $product_name;
				$data[$key]['start'] = $this->data['results'][$key]['checkin_date'];
				$data[$key]['end'] = $this->data['results'][$key]['checkout_date'];
				$data[$key]['url'] = '/'.$this->data['init']['langu'].'/order/show/'.$this->data['results'][$key]['id'];
			}
			$this->output->set_content_type('application/json')
			    ->set_output(json_encode($data));
		}
	}
	
}
