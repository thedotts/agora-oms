<?php

class Settings_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();     
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Settings_model');
			$this->load->model('Job_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Audit_log_model');
			$this->load->model('Employee_model');

            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$userdata = $this->session->userdata("userdata");
				
				//print_r($userdata);exit;
				if(strpos($userdata['role_id'],',')){
					$userdata['role_id'] = explode(',',$userdata['role_id']);	
				}else{
					$userdata['role_id'] = array($userdata['role_id']);	
				}
				
				
				$this->data['userdata'] = $userdata;
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}     						
			
			$this->data['group_name'] = "administrator";  
			$this->data['model_name'] = "settings";  
			$this->data['common_name'] = "Settings"; 
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			if(in_array(3,$this->data['userdata']['role_id'])){
				
				
				$a_csutomer = $this->data['staff_info']['assign_customer'];
				
				if($a_csutomer != ''){
				
					if(strpos($a_csutomer,',')){
						$a_customer_array = explode(',',$a_csutomer);	
					}else{
						$a_customer_array = array($a_csutomer);	
					}
				
				}else{
					$a_customer_array = array();	
				}
				
				$this->data['task_display_count'] = $this->Job_model->record_count2(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
				),$this->data['userdata']['role_id'],$a_customer_array);
				
				
				

			}else{
			
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			}
			
			/*
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			} 
			*/ 
           
      }
         
	  
	  public function index($alert=0) {
          		
			$this->data['alert'] = $alert;
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/agora/'.$this->data['group_name'].'/'.$this->data['model_name'];

			$this->data['setting'] = $this->Settings_model->get_settings_ra();
			
			//prefix
			$prefix = $this->data['setting']['prefix'];
			$prefix = json_decode($prefix,true);
			
			$reArray_tmp = array();
			foreach($prefix as $k => $v){
				$reArray_tmp[$v['table_name']] = $v['prefix'];
			}
			$this->data['prefix'] = $reArray_tmp;
			
			$this->session->set_userdata("lastpage", $url);

            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  	  	 	  	  
	  
	  public function add($id=false) {  
	  
          	if($id !== false) {
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Settings_model->get($id);
			} else {
				$this->data['mode'] = 'Add';	
			}
			
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		            
			
      }	  	
	  
	  public function del($id) {
		  
		  $employee_data = $this->Settings_model->get($id);		  
		  $this->Settings_model->delete($id);		  
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		
		  
	  }  
	  
	  public function submit(){
		  
		  $mode 					= $this->input->post("mode", true);
		  $id 						= $this->input->post("id", true);	
		  	  		  		  		  
		  $remind_1 				= $this->input->post("remind_1", true);
		  $remind_2 				= $this->input->post("remind_2", true);		  
		  $gst 						= $this->input->post("gst", true);		
		  $company_name 			= $this->input->post("company_name", true);
		  $company_address 			= $this->input->post("company_address", true);
		  $postal 					= $this->input->post("postal", true);
		  $email 					= $this->input->post("email", true);
		  $main_contact_person 		= $this->input->post("main_contact_person", true);
		  $main_contact_no 			= $this->input->post("main_contact_no", true);
		  $payment_info 			= $this->input->post("payment_info", true);
		  $term 					= $this->input->post("term", true); 
		  $reg_no 					= $this->input->post("reg_no", true); 
		  $d_web 					= $this->input->post("d_web", true); 
		  
		  //prefix
		  $prefix_array = array();
		  $prefix_array['order'] 			= $this->input->post("prefix_order", true);
		  $prefix_array['packing_list']		= $this->input->post("prefix_pl", true);
		  $prefix_array['delivery_order'] 	= $this->input->post("prefix_do", true);
		  $prefix_array['invoice']			= $this->input->post("prefix_invoice", true);
		  
		  $prefix_tmp = array();
		  foreach($prefix_array as $k => $v){
			  
			  $tmp = array(
			  	'table_name' 	=> $k,
				'prefix'		=> $v,
				'YY'			=> 0,
			  );
			  
			  if($k == 'order'){
				 $tmp['YY'] = 1; 
			  }
			  
			  $prefix_tmp[] = $tmp;
		  }

				 
		  //update data,number = id 		 		  
		  $iu_array = array(		  	
		  	'26'	=> $remind_1 ,
			'27'	=> $remind_2 ,			
			'5'		=> $gst,
			'15'	=> $company_name,
			'16'	=> $company_address,
			'25'	=> $postal,
			'6'		=> $email,
			'7'		=> $main_contact_person,
			'8'		=> $main_contact_no,
			'9'		=> $payment_info,
			'10'	=> $term,
			'13'	=> json_encode($prefix_tmp),
			'28'	=> $reg_no,
			'29'	=> $d_web,
		  );		 		  
		  		  		  
		  foreach($iu_array as $k => $v){
		  		  			  
			$update_array = array('value'=>$v);
		  	$this->Settings_model->update($k, $update_array);
			
		  }
		  
		  //audit log
		  $log_array = array(
			'ip_address'	=> $this->input->ip_address(),
			'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
			'table_affect'	=> 'settings',
			'description'	=> 'Edit settings',
			'created_date'	=> date('Y-m-d H:i:s'),
		  );
			  
		  $audit_id = $this->Audit_log_model->insert($log_array);	
		  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
		  $update_array = array(
			 'log_no'	=> $custom_code,
		  );
		  $this->Audit_log_model->update($audit_id, $update_array);
			    		 		  		  
		  //alert
		  $alert_type = '/2';
	  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage.$alert_type,'refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].$alert_type));
		  }		  
		  
	  }
	  
	 
}

?>