<?php

class Quotation_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();  
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Customers_model');
			$this->load->model('Quotation_model');
			$this->load->model('Quotation_item_model');
			$this->load->model('Shipping_model');
			$this->load->model('Employee_model');
			$this->load->model('Department_model');
			$this->load->model('Products_model');
			$this->load->model('Suppliers_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Department_model');
			$this->load->model('Settings_model');
			$this->load->model('Workflow_model');
			$this->load->model('Workflow_order_model');

			$this->data['role_list'] = $this->Role_model->getIDKeyArray('name');
			$quotation_status_list = $this->Workflow_model->get(1);
			$this->data['quotation_status_list'] = json_decode($quotation_status_list['status_json']);
			$this->data['status_list'] = $this->Quotation_model->status_list();
            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$this->data['userdata'] = $this->session->userdata("userdata");
			}else{
				redirect(base_url('en/login'),'refresh');
			}  
			
			$this->data['group_name'] = "sales";  
			$this->data['model_name'] = "quotation";  
			$this->data['common_name'] = "Quotation Request";   
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			} 
			
			define('SUPERADMIN', 1);
			define('GENERAL_MANAGER', 2);
		    define('LOGISTIC', 6);
		    define('SALES_MANAGER', 7);
		    define('SALES_EXECUTIVE', 8);
			
			/*
			$formula = array();
			
			$product = array(
				'target_colum' => 'product_total',
				'logic' => '>',
				'value' => 150000,
				'next_status_id' => 2,
			);
			$service = array(
				'target_colum' => 'service_total',
				'logic' => '>',
				'value' => 50000,
				'next_status_id' => 2,
			);
			
			$formula[] =$product;
			$formula[] =$service;
			
			print_r(json_encode($formula));exit;
			*/
           
      }
   
      public function index($q="ALL", $status="ALL", $page=1) {  
          			
			//$a = $this->Workflow_order_model->get_workflow_by_id(12);
			//print_r(json_encode($a));exit;
					
            $this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/anexus/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/'.$status.'/';
			if($q == 'ALL') {
				$q = "";
			}
			$this->data['q'] = $q;
			
			if($status != 'ALL') {
				$filter['status'] = $status;	
			}
			$this->data['status'] = $status;			
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Quotation_model->record_count($filter, $q);
			
			//get particular ranged list
			$this->data['results'] = $this->Quotation_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start);
			if(!empty($this->data['results'])) {
				foreach($this->data['results'] as $k => $v){
					if(!empty($v['awaiting_table'])){
							$this->data['results'][$k]['awaiting_table']= explode(',',$v['awaiting_table']);
					}
				}
			}
						
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->data['customer_list'] = $this->Customers_model->getIDKeyArray("company_name");
			$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
			
			$this->session->set_userdata("lastpage", $url.$page);
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  
	  public function add($id=false) {  
	  		
			$role_id = $this->data['userdata']['role_id'];
			
			//Edit
			if($id !== false) {
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Quotation_model->get($id);																
				
				
				$default = 0;
				
				$requestor_employee = $this->Employee_model->get($this->data['result']['requestor_id']);
			    $requestor_role = $requestor_employee['role_id'];
				$this->data['requestor_role'] = $requestor_role;				
				
				$this->data['result']['awaiting_table'] = explode(',',$this->data['result']['awaiting_table']);
				$awating_array =$this->data['result']['awaiting_table'];
				//print_r($this->data['result']);exit;
				
				//if user role id equal to awaiting list match = 1  
				$awaiting_match = 0;
				foreach($awating_array as $v){
					if($this->data['userdata']['role_id'] == $v){
						$this->data['awaiting_match'] = 1;
						break;
					}
				}
				

				/*
				if($this->data['result']['status'] == 1 || $this->data['result']['status'] == 2){
					
					if($requestor_role  == 8 && ($role_id == 8 || $role_id == 7)){
					$this->data['head_title'] = 'Edit Quotation Request';
					}else if($requestor_role  == 7 && $role_id == 7){
					$this->data['head_title'] = 'Edit Quotation Request';
					}else{
					$default = 1;
					}
					
				}else if($this->data['result']['status'] == 3){
					
					if($role_id == 6){
					$this->data['head_title'] = 'Enter Shipping Cost';
					}else{
					$default = 1;
					}
					
				}else if($this->data['result']['status'] == 4){
					if($role_id == 7 || $role_id == 8){
					$this->data['head_title'] = 'Send out Quotation';
					}else{
					$default = 1;
					}
					
				}else if($this->data['result']['status'] == 5){
					if($role_id == 7 || $role_id == 8){
					$this->data['head_title'] = 'Confirm Quotation';
					}else{
					$default = 1;
					}
				}else{
					$default = 1;
				}
				//no related person
				if($default == 1){
					
					$this->data['head_title'] = 'View Quotation';
						
				}
				*/
					
				$receivedDate = $this->data['result']['received_date'];
				$tmp = explode('-',$receivedDate);
				$this->data['result']['received_date'] = $tmp[2]."/".$tmp[1]."/".$tmp[0];
				
				$requestDate = $this->data['result']['request_date'];
				$tmp = explode('-',$requestDate);
				$this->data['result']['request_date'] = $tmp[2]."/".$tmp[1]."/".$tmp[0];
				
				$requestor = $this->Employee_model->get($this->data['result']['requestor_id']);
				$this->data['result']['requestor_name'] = $requestor['full_name'];
				$requestor_department = $this->Department_model->get($requestor['department_id']);
				$this->data['result']['requestor_department_name'] = $requestor_department['name'];
				
				$product = $this->Quotation_item_model->getRelatedItem($id);//print_r($product);exit;
				foreach($product as $k => $v){
					$product_data = $this->Products_model->get($v['product_id']);
					
					$product[$k]['supplier_id'] = $product_data['supplier'];
				}
				$this->data['result']['product'] = json_encode($product);
				
				
					//get related workflow order data
						$workflow_flitter = array(
							'status_id' => $this->data['result']['status'],
							'workflow_id' => 1,
						);
						
						$type='';
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						//print_r($role_id);exit;
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$type = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$type = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
							$type = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
									$type = 'edit';	
							}
						
						}
						
						
						//data form database
						$workflow = $this->Workflow_model->get(1);
						$status = json_decode($workflow['status_json'],true);
						
						$this->data['workflow_title'] = $workflow['title'];
						
					$this->data['btn_type'] = $type;
					
					
					
					switch($type){
						case 'edit':
							$this->data['head_title'] = 'Edit '.$workflow['title'].' Request';
						break;
						case 'approval':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
						break;
						case 'approval_update':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'update':
							$this->data['head_title'] = 'Update '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'send_email':
							$this->data['head_title'] = 'Waiting to Send '.$workflow['title'].' Request';
						break;
						case 'upload':
							$this->data['head_title'] = 'Upload Confirm File '.$workflow['title'].' Request';
						break;
						case 'complete':
							$this->data['head_title'] = $workflow['title'].' Request';
							$this->data['last_action'] = $workflow_order['action_before_complete'];
						break;
						default:
							$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					
					foreach(json_decode($workflow['status_json'],true) as $k => $v){
						
						if($v == 'Completed'){
							$this->data['completed_status_id'] = $k;
							break;
						}
					}
			
			//Add	
			} else {
				
				$this->data['mode'] = 'Add';	
				
				//get the manager of current login
				$manager_data = $this->Role_model->getParentRoleData($this->data['userdata']['role_id']);
				$this->data['manager_data'] = $manager_data;
				
				
				    //get related workflow order data
					$workflow_flitter = array(
						'role_id' => $role_id,
						'status_id' => -1,
						'workflow_id' => 1,
					);
					
					//data form database
					$workflow = $this->Workflow_model->get(1);
					$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
					$this->data['workflow_title'] = $workflow['title'];
					
					//get view bottom layout
					$type = '';
					if(!empty($workflow_order)){
						$type = $workflow_order['action'];
						if($workflow_order['action'] == 'create'){
							$this->data['head_title'] = 'Create '.$workflow['title'].' Request';
						}else{
							$this->data['head_title'] = $workflow['title'].' Request';
						}
					}else{
						$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					$this->data['btn_type'] = $type;
					
				
			}
			
			$group_product_supplier = $this->Products_model->getAll_groupby();
			if(!empty($group_product_supplier)){
			$supplier_list_name = $this->Suppliers_model->getIDKeyArray("company_name");
			//print_r($group_product_supplier);exit;
			foreach($group_product_supplier as $k => $v){
				$this->data['supplier_list'][$k]['name'] = $supplier_list_name[$v['supplier']];
				$this->data['supplier_list'][$k]['id'] = $v['supplier'];
			}
			$this->data['supplier_list']=json_encode($this->data['supplier_list']);
			}
			//print_r($this->data['result']);exit;
			$this->data['service'] = json_encode($this->Products_model->getService());
			
			$this->data['company_list'] = json_encode($this->Customers_model->getAll());
			$this->data['shipping_list'] = $this->Shipping_model->getAll();
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			if(isset($this->data['staff_info']['department_id'])){
			$department_data = $this->Department_model->get($this->data['staff_info']['department_id']);
			$this->data['staff_info']['department_name'] = $department_data['name'];
			}
			
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		
            
			
      }	  	  
	  
	  public function submit(){
		  
		  //print_r($_POST);exit;
		  
		  $now = date("Y-m-d H:i:s");
		  $mode = $this->input->post("mode", true);
		  $id = $this->input->post("id", true);
		  
		  $role_id = $this->data['userdata']['role_id'];
		  
		  	  $received_date = $this->input->post("received_date", true);
			  $requestor_id = $this->input->post("requestor_id", true); //建立者的employee_id
			  $request_date = $this->input->post("request_date", true);
			  $department_id = $this->input->post("department_id", true);
			  $requestor_to = $this->input->post("requestor_to", true);
			  $requestor_to_roleid = $this->input->post("requestor_to_roleid", true);
			  $company = $this->input->post("company", true);
			  $shipper = $this->input->post("shipper", true);
			  $qr_remarks = $this->input->post("qr_remarks", true);
			  $totalPrice = $this->input->post("totalPrice", true);
			  $product_total = $this->input->post("product_total", true);
			  $service_total = $this->input->post("service_total", true);
			  $ship_fee = $this->input->post("est_shipping", true);
		  
		  	  //get value approve or not approve
			  $confirm = $this->input->post("confirm", true);
			  $correct_check = $this->input->post("correct_check", true);
		  
			  if(!empty($received_date)) {
				  $tmp = explode("/", $received_date);
				  $received_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
			  }
			  
			  if(!empty($request_date)) {
				  $tmp = explode("/", $request_date);
				  $request_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
			  }		
			    
		  
		  if($mode == 'Add'){
			  
			  if(!empty($company)) { 
				  $tmp = explode("|", $company);
				  $customer_id = $tmp[0];
				  $customer_name = $tmp[1];
			  }
			  
			  
		  }else{
		  
			  //quota
			  $quotation_data = $this->Quotation_model->get($id);
		  	  $customer_id = $quotation_data['customer_id'];
			  $customer_name = $quotation_data['customer_name'];
			  
		  }
		   
		  //job
		  $jobArray = array(
		  	'parent_id' =>0,
			'user_id' =>$this->data['userdata']['id'],
			'customer_id' =>$customer_id,
			'type' => 'Quotation',
			'type_path' =>'sales/quotation_edit/',
		  );
		  
		  
		  //quotation insert array
		  $array = array(
		  	'received_date'					=> $received_date,
			'requestor_id'					=> $requestor_id,
			'request_date'					=> $request_date,
			'department_id'					=> $department_id,
			'requestor_to'					=> $requestor_to,
			'requestor_to_roleid'			=> $requestor_to_roleid,
			'customer_id'					=> $customer_id,
			'customer_name'					=> $customer_name,
			'shipper'						=> $shipper,
			'qr_remarks'					=> $qr_remarks,
			'total_price'					=> $totalPrice,
			'product_total'					=> $product_total,
			'service_total'					=> $service_total,
			'est_shipping'					=> $ship_fee,
		  );
		  
		  $mail_array = array(
		  	'creater_name' =>$this->data['userdata']['name'],
		  );
		  
		  
		  
		  //新增時判斷创立者 决定status
		  if($mode == 'Add'){
			  
			  
			    //get related workflow order data
				$workflow_flitter = array(
					'role_id' => $role_id,
					'status_id' => -1,
					'workflow_id' => 1,
				);
				
				//data form database
				$workflow = $this->Workflow_model->get(1);
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
				$status = json_decode($workflow['status_json'],true);
				
				if(!empty($workflow_order)){
				
				//ad_hoc
				if($workflow_order['ad_hoc'] == 1){
					$array['ad_hoc'] = 1;
				}
				
				//purchaser
				$is_adhoc = false;
				if($role_id == 10){
					$is_adhoc = true;
				}
				
				//next status
				$next_status = $workflow_order['next_status_id'];
				$next_status_text = $status[$workflow_order['next_status_id']];
				
			  	//if need_det = 1
				if($workflow_order['need_det'] == 1){
					
					$formula = json_decode($workflow_order['formula'],true);
					
					//check if product or service price over value
					foreach($formula as $k => $v){
					
						$price = $this->input->post($v['target_colum'], true);
						$det_temp = $price." ".$v['logic']." ".$v['value'];
						$check = $this->parse_boolean($det_temp);
						
						//check price over value
						if($check){
							$next_status = $v['next_status_id'];
							$next_status_text = $status[$v['next_status_id']];
							break;
						}
						
					}
					
				}
				
				//get next status workflow order data
				$next_workflow_flitter = array(
					'status_id' => $next_status,
					'workflow_id' => 1,
				);
				
				//next status data
				$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
				$awating_person = $next_status_data['role_id'];
				
				//main
				$array['awaiting_table']  = $awating_person;
				$array['status'] = $next_status;
				
				//main
				$jobArray['awaiting_role_id'] = $awating_person.',';
				$jobArray['status_id'] = $next_status;
				$jobArray['status_text'] = $next_status_text;
				
				}

				
			  
			  /*
			  //Sales Executive
			  if($role_id == SALES_EXECUTIVE){
			  	  //role 2=General Manager,3=Service Manager,7=Sales Manager
				  //如果報價是再criteria之間 就交給General Manager 去approve
				  if($product_total >= 150000 || $service_total >= 50000){
					$array['awaiting_table']  = GENERAL_MANAGER;
					$array['status']  = 2;
					$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
					$jobArray['status_id'] = 2;
					$jobArray['status_text'] = $this->data['quotation_status_list'][2];
					
					$mail_array['related_role'] = array(SALES_EXECUTIVE,GENERAL_MANAGER);
				  //不在criteria	就交給Sales Manager 去Approve
				  }else{
					$array['awaiting_table']  = SALES_MANAGER;
					$array['status'] = 1;
					$jobArray['awaiting_role_id'] = SALES_MANAGER.',';
					$jobArray['status_id'] = 1;
					$jobArray['status_text'] = $this->data['quotation_status_list'][1];
					
					$mail_array['related_role'] = array(SALES_EXECUTIVE,SALES_MANAGER);
				  }
			  //Sales Manager	  
			  //如果建立的人是Sales manager, 不管是不是在criteria都是交給GM
			  }else if($role_id == SALES_MANAGER){
				  				  
					$array['awaiting_table']  = GENERAL_MANAGER;
					$array['status']  = 2;
					$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
					$jobArray['status_id'] = 2;
					$jobArray['status_text'] = $this->data['quotation_status_list'][2];					
					$mail_array['related_role'] = array(SALES_MANAGER,GENERAL_MANAGER);					
			  }
			  $mail_array['title'] = 'create quotation';
			  */
			  
		  }
		  
		  if($mode == 'Edit'){
			  
			  //quota
			  $quotation_data = $this->Quotation_model->get($id);
			  $quota_status = $quotation_data['status'];
			  
			  //requestor
			  $requestor_employee = $this->Employee_model->get($quotation_data['requestor_id']);
			  $requestor_role = $requestor_employee['role_id'];
			  
			  //job
			  $jobArray['serial'] =$quotation_data['qr_serial'];
			  $jobArray['type_id'] =$quotation_data['id'];
			  $jobArray['parent_id'] = $quotation_data['latest_job_id'];
			  
			  //status
			  $next = 0; //會把上一個JOB completed 會INSERT新的JOB
			  $last = 0; //會把上一個JOB completed 可是不會INSERT新的JOB
			  $status_change = 0; //擔心QUOTATION的價格有變動, 就要重新來過, 所以要把舊的JOB都刪掉
			 
			  
			  /////////////////////////////////////////////////////////////////////////////////
			  
			  //can edit status
			  $edit_status = $this->Workflow_order_model->get_status_edit(1);
			  
			  //requestor edit
			  if(in_array($quota_status, $edit_status) && $role_id == $requestor_role){
			  	  
						//get related workflow order data
						$workflow_flitter = array(
							'role_id' => $role_id,
							'status_id' => -1,
							'workflow_id' => 1,
						);
						
						//data form database
						$workflow = $this->Workflow_model->get(1);
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						$status = json_decode($workflow['status_json'],true);
						
						if(!empty($workflow_order)){
						
						//next status
						$next_status = $workflow_order['next_status_id'];
						$next_status_text = $status[$workflow_order['next_status_id']];
						
						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];
								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						//get next status workflow order data
						$next_workflow_flitter = array(
							'status_id' => $next_status,
							'workflow_id' => 1,
						);
						
						//next status data
						$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
						$awating_person = $next_status_data['role_id'];
						
						//if awating person equal requestor ,asign requestor for it
						if($awating_person == 'requestor'){
							$awating_person = $requestor_role;
						}
						
						//main
						$array['awaiting_table']  = $awating_person;
						$array['status'] = $next_status;
						
						//job
						$jobArray['awaiting_role_id'] = $awating_person.',';
						$jobArray['status_id'] = $next_status;
						$jobArray['status_text'] = $next_status_text;
						
						if($quota_status != $array['status']){  
							  $this->Job_model->delete($quotation_data['latest_job_id']);
							  $status_change = 1;
						}
						
						}
						
				  
			  }else{
				  	//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $quota_status,
							'workflow_id' => 1,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(1);
						$status = json_decode($workflow['status_json'],true);
						
						if(isset($action)){
						
						//action
						$action = $workflow_order['action'];
						
						switch ($action) {
							case 'approval':
							
								//clear data infront
								$array = array();
							
								//approved
								if($confirm == 1){
									
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];
										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'update':
								
								//next status
								$next_status = $workflow_order['next_status_id'];
								$next_status_text = $status[$workflow_order['next_status_id']];
								
								$target_colum = json_decode($workflow_order['target_colum'],true);
								
										
										$array = array();
										foreach($target_colum as $k => $v){
											//single
											if($v['type'] == 'single'){
												
												if($v['data_type'] == 'string'){
													
												$array[$v['name']] = $this->input->post($v['name'],true);
												
												}else if($v['data_type'] == 'date'){
													
													$date = $this->input->post($v['name'], true);
													if(!empty($date)) {
													  $tmp = explode("/", $date);
													  $date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
													  $array[$v['name']] = $date;
													}
													
												}
												
											//multiple
											}else{
												
											}
											
										}
										$next = 1;
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
								break;
							case 'upload':
								
								//clear data infront
								$array = array();
								
								if($confirm == 1){
								
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'comfirm_file'	=> $save_path,
											);	
											
											//next status
											$next_status = $workflow_order['next_status_id'];
											$next_status_text = $status[$workflow_order['next_status_id']];	
											$next = 1;
											//check last job
											if($workflow_order['next_status_text'] == 'Completed'){
												$last = 1;
											}		
											
											//auto create pr task for sales executive
											$auto_create = 1;																				
									}	
								}
								
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
								
								break;	
							case 'complete':
							
								//clear data infront
								$array = array();
							
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'comfirm_file'	=> $save_path,
											);	
										
									}	
								}
								
								break;	
								
						}
						
						
						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];
								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 1,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
						}
						
						}
				  
			  }
			  
			 	if(isset($next_status)){
					//main
					$array['awaiting_table']  = $awating_person;
					$array['status'] = $next_status;
					//job
					$jobArray['awaiting_role_id'] = $awating_person.',';
					$jobArray['status_id'] = $next_status;
					$jobArray['status_text'] = $next_status_text;
				}
				
			  //////////////////////////////////////////////////////////////////////////////////////
			  
			 
			  
			  
			  /*
			  
			  //如果目前是 等待 Sales Manager 或 等待 General manager approve
			  if($quota_status == 1 || $quota_status == 2){
				  //requestor edit
				  if($role_id == $requestor_role){
					  			
						 //over 150k
						 if($product_total >= 150000 || $service_total >= 50000){
							$array['awaiting_table']  = GENERAL_MANAGER;
							$array['status']  = 2;
							$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
							$jobArray['status_id'] = 2;
							$jobArray['status_text'] = $this->data['quotation_status_list'][2];
							
							if($requestor_role != SALES_EXECUTIVE){
								$mail_array['related_role'] = array(SALES_MANAGER,GENERAL_MANAGER,LOGISTIC);
							}else{
								$mail_array['related_role'] = array(SALES_EXECUTIVE,GENERAL_MANAGER,LOGISTIC);
							}

							
						  }else{
							$array['awaiting_table']  = SALES_MANAGER;
							$array['status'] = 1;
							$jobArray['awaiting_role_id'] = SALES_MANAGER.',';
							$jobArray['status_id'] = 1;
							$jobArray['status_text'] = $this->data['quotation_status_list'][1];
							
							if($requestor_role != SALES_EXECUTIVE){
								$mail_array['related_role'] = array(SALES_MANAGER,LOGISTIC);
							}else{
								$mail_array['related_role'] = array(SALES_EXECUTIVE,SALES_MANAGER,LOGISTIC);
							}
						  }
						  
						  
						  if($quota_status != $array['status']){  
							  $this->Job_model->delete($quotation_data['latest_job_id']);
							  $status_change = 1;
						  }
						  
				  //sale mananger approved
				  }else if($role_id == SALES_MANAGER){
					  
					  if($correct_check == 'on'){
						  
						  if($product_total >= 150000 || $service_total >= 50000){
							$array['awaiting_table']  = GENERAL_MANAGER;
							$array['status']  = 2;
							$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
							$jobArray['status_id'] = 2;
							$jobArray['status_text'] = $this->data['quotation_status_list'][2];
							
							//no equal 8 , mean create by SM
							  if($product_total >= 150000 || $service_total >= 50000){
									if($requestor_role != SALES_EXECUTIVE){
										$mail_array['related_role'] = array(SALES_MANAGER,GENERAL_MANAGER,LOGISTIC);
									}else{
										$mail_array['related_role'] = array(SALES_EXECUTIVE,GENERAL_MANAGER,LOGISTIC);
									}
									
							  }else{
									if($requestor_role != SALES_EXECUTIVE){
										$mail_array['related_role'] = array(SALES_MANAGER,LOGISTIC);
									}else{
										$mail_array['related_role'] = array(SALES_EXECUTIVE,SALES_MANAGER,LOGISTIC);
									}
							  }
							
						  }else{
							$array['awaiting_table']  = LOGISTIC.','; //logistics to wait
						    $array['status']  = 3;
						  
						    $jobArray['awaiting_role_id'] = LOGISTIC.',';
						    $jobArray['status_id'] = 3;
						    $jobArray['status_text'] = $this->data['quotation_status_list'][3];
							
							//no equal 8 , mean create by SM
							  if($product_total >= 150000 || $service_total >= 50000){
									if($requestor_role != SALES_EXECUTIVE){
										$mail_array['related_role'] = array(SALES_MANAGER,GENERAL_MANAGER,LOGISTIC);
									}else{
										$mail_array['related_role'] = array(SALES_EXECUTIVE,GENERAL_MANAGER,LOGISTIC);
									}
									
							  }else{
									if($requestor_role != SALES_EXECUTIVE){
										$mail_array['related_role'] = array(SALES_MANAGER,LOGISTIC);
									}else{
										$mail_array['related_role'] = array(SALES_EXECUTIVE,SALES_MANAGER,LOGISTIC);
									}
							  }
						  }
						  $next = 1;
						  
					  }
					  
					  if($confirm == 0){
						  $array['awaiting_table']  = '';
						  $array['status']  = 0;
						  
						  $next = 1;
						  $last = 1;
					  }
					  
					  $mail_array['title'] = 'Approve quotation';
				  
				  //General Manager	  
				  }else if($role_id == GENERAL_MANAGER){
					  $array = array();
					  if($correct_check == 'on'){
						  
						  $array = array(
							'awaiting_table' =>LOGISTIC.',',
							'status' => 3,
						  );
						  
						  $jobArray['awaiting_role_id'] = LOGISTIC.',';
						  $jobArray['status_id'] = 3;
						  $jobArray['status_text'] = $this->data['quotation_status_list'][3];
						  $next = 1;
						  $mail_array['title'] = 'Approve quotation';
					  }
					  
					  if($confirm == 0){
						  $array['awaiting_table']  = '';
						  $array['status']  = 0;
						  
						  $next = 1;
						  $last = 1;
						  $mail_array['title'] = 'No Approve quotation';
					  }
					  //no equal 8 , mean create by SM
					  if($product_total >= 150000 || $service_total >= 50000){
							if($requestor_role != SALES_EXECUTIVE){
								$mail_array['related_role'] = array(SALES_MANAGER,GENERAL_MANAGER,LOGISTIC);
							}else{
								$mail_array['related_role'] = array(SALES_EXECUTIVE,GENERAL_MANAGER,LOGISTIC);
							}
							
					  }else{
						    if($requestor_role != SALES_EXECUTIVE){
								$mail_array['related_role'] = array(SALES_MANAGER,LOGISTIC);
							}else{
								$mail_array['related_role'] = array(SALES_EXECUTIVE,SALES_MANAGER,LOGISTIC);
							}
					  }
					  
				  }
				  
			  //logistic
			  }else if($quota_status == 3){
				  if($role_id == LOGISTIC){
					  
					  //sales manager & sales executive to wait
					  $array = array(
					  	'awaiting_table' => SALES_MANAGER.','.SALES_EXECUTIVE.',',
						'status' => 4,
						'est_shipping' =>$this->input->post("shippingFee", true),
					  );
					  
					  $jobArray['awaiting_role_id'] = SALES_EXECUTIVE.','.SALES_MANAGER.',';
					  $jobArray['status_id'] = 4;
					  $jobArray['status_text'] = $this->data['quotation_status_list'][4];
					  
					  $next = 1;
					  
					  //no equal 8 , mean create by SM
					  if($product_total >= 150000 || $service_total >= 50000){
							if($requestor_role != SALES_EXECUTIVE){
								$mail_array['related_role'] = array(SALES_MANAGER,GENERAL_MANAGER,LOGISTIC);
							}else{
								$mail_array['related_role'] = array(SALES_EXECUTIVE,GENERAL_MANAGER,LOGISTIC);
							}
							
					  }else{
						    if($requestor_role != SALES_EXECUTIVE){
								$mail_array['related_role'] = array(SALES_MANAGER,LOGISTIC);
							}else{
								$mail_array['related_role'] = array(SALES_EXECUTIVE,SALES_MANAGER,LOGISTIC);
							}
					  }
					  $mail_array['title'] = 'Logistic';
					  
				  }
			  //sale executive upload comfirm file
			  }else if($quota_status == 5){
				  
				  //print_r($_FILES);exit;
				  
				  if(in_array($role_id, array(SALES_MANAGER,SALES_EXECUTIVE,GENERAL_MANAGER))){
						
					  $array = array(
					  	'awaiting_table' =>'',
						'status' => 6,
					  );
					  
					  
					  if(isset($_FILES['comfirm_file'])){
				
						if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
								$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
								$ext = $pathinfo['extension'];
								$ext = strtolower($ext);
													
								$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
								$path = "./uploads/".$filename.'.'.$ext;
								$save_path = base_url()."uploads/".$filename.'.'.$ext;
								$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
								if(!$result) {
									die("cannot upload file");
								}	
							
								$array['comfirm_file']= $save_path;															
						}	
					  }
							    
					  $next = 1;
					  $last = 1;
					  
					  //no equal 8 , mean create by SM
					  if($product_total >= 150000 || $service_total >= 50000){
							if($requestor_role != SALES_EXECUTIVE){
								$mail_array['related_role'] = array(SALES_MANAGER,GENERAL_MANAGER,LOGISTIC);
							}else{
								$mail_array['related_role'] = array(SALES_EXECUTIVE,GENERAL_MANAGER,LOGISTIC);
							}
							
					  }else{
						    if($requestor_role != SALES_EXECUTIVE){
								$mail_array['related_role'] = array(SALES_MANAGER,LOGISTIC);
							}else{
								$mail_array['related_role'] = array(SALES_EXECUTIVE,SALES_MANAGER,LOGISTIC);
							}
					  }
					  
					  $mail_array['title'] = 'Comfirm file';
					  
				  }
			  //雖然是complete了, 但還是允許使用者上傳document	  
			  }else if($quota_status == 6){
				  
				  	  if(isset($_FILES['comfirm_file'])){
				
						if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
								$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
								$ext = $pathinfo['extension'];
								$ext = strtolower($ext);
													
								$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
								$path = "./uploads/".$filename.'.'.$ext;
								$save_path = base_url()."uploads/".$filename.'.'.$ext;
								$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
								if(!$result) {
									die("cannot upload file");
								}	
							
								$file= $save_path;															
						}	
					  }
					  if(isset($file)){
						  $array = array(
							'comfirm_file' =>$file,
						  );
					  }
					  
			  }*/
		  
		  }
		  		  
		  $quotation_id = "";
		  //如果是SuperAdmin就可以直接控制status
		  if($role_id == SUPERADMIN){
			  $array['status'] = $this->input->post("status", true);
		  }
		  
		  //Add
		  if($mode == 'Add') {			  			  
			  
			  $array['lastupdate_user_id'] = $this->data['userdata']['id'];
			  $array['create_user_id'] = $this->data['userdata']['id'];
			  $array['created_date'] = $now;
			  $array['modified_date'] = $now;
			  
			  
			  $quotation_id = $this->Quotation_model->insert($array);
			  //echo $this->db->last_query();exit;
		  	  $qr_serial = $this->Quotation_model->zerofill($quotation_id, 5);
			  
			  $jobArray['created_date'] = date("Y-m-d H:i:s");
			  $jobArray['modified_date'] = date("Y-m-d H:i:s");
			  $jobArray['serial'] =$qr_serial;
			  $jobArray['type_id'] =$quotation_id;
			  
			  $job_id = $this->Job_model->insert($jobArray);
			  
			  //print_r($array);exit;
			  $array =array(
			  	'job_id'	=> $job_id,
			  	'qr_serial'  => $qr_serial,
				'latest_job_id'	=> $job_id,
			  );
			  $this->Quotation_model->update($quotation_id, $array);	
			  
			  //mail
			  $related_role = $this->Job_model->getRelatedRoleID($job_id);
			  $mail_array['related_role'] = $related_role;		
			  $mail_array['title'] = $workflow['title']." ".$qr_serial." ".$next_status_text;  			  
			  
		  //Edit	  			  
		  } else {
			  //print_r($jobArray['parent_id']);exit;
			  $array['lastupdate_user_id'] = $this->data['userdata']['id'];
			  $array['modified_date'] = $now;	
			  
			  $quotation_id = $id;
			  
			  $jobArray['created_date'] = date("Y-m-d H:i:s");
			  $jobArray['modified_date'] = date("Y-m-d H:i:s");
			  
			  if($next == 1){
				  $lastjob_update = array(
				  	'is_completed' => 1,
					'display' => 0,
				  );
				  $this->Job_model->update($jobArray['parent_id'], $lastjob_update);
				  if($last == 0){
				  $new_job_id = $this->Job_model->insert($jobArray);
				  $array['latest_job_id'] = $new_job_id;
				  }
			  }else if($status_change == 1){
				  //edit price over 150k
				  $new_job_id = $this->Job_model->insert($jobArray);
				  $array['latest_job_id'] = $new_job_id;
				  $array['job_id'] = $new_job_id;
				  
			  }
			  
			  $this->Quotation_model->update($quotation_id, $array);
			  
			  //mail
			  if(isset($new_job_id)){
			  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
			  $mail_array['related_role'] = $related_role;		
			  $mail_array['title'] = $workflow['title']." ".$quotation_data['qr_serial']." ".$next_status_text;
			  }
			   
			     
		  }
		  
		  //when item allow be update
		  $item_update = 0;
		  if($mode == 'Add'){
			  
			  $item_update = 1;
			  
		  }else{
			  
			  if(in_array($quota_status, $edit_status) && $requestor_role == $role_id){
				  $item_update = 1;
			  }
			  
		  }

		  if($item_update == 1){
		  //針對底下的ITEM做處理
		  $groupCount = $this->input->post("groupCount", true);
		  
		  if($groupCount > 0){
				for($i=0;$i<$groupCount;$i++){
					
					
					//print_r($tmp);exit;
					$data = array(
						'product_id' => isset($_POST['modelNo'.$i])?$_POST['modelNo'.$i]:'',
						'model_no' => isset($_POST['modelNo2'.$i])?$_POST['modelNo2'.$i]:'',
						'quantity' => isset($_POST['quantity'.$i])?$_POST['quantity'.$i]:'',
						'unit_price' => isset($_POST['unitPrice'.$i])?$_POST['unitPrice'.$i]:'',
						'extended_price' => isset($_POST['extendedPrice'.$i])?$_POST['extendedPrice'.$i]:'',
						'created_date' => $now,
						'type' => isset($_POST['type'.$i])?$_POST['type'.$i]:'',
						'quotation_id' => $quotation_id,
					);
					
					//有deleted 又有old_id 代表要刪除資料
					if(isset($_POST['is_deleted'.$i]) && !empty($_POST['is_deleted'.$i]) && isset($_POST['old_id'.$i]) && !empty($_POST['old_id'.$i])){
						
						$this->Quotation_item_model->delete($_POST['old_id'.$i]);
					//沒有deleted 但有old_id 代表要更新這個資料
					} else if ( isset($_POST['is_deleted'.$i]) && empty($_POST['is_deleted'.$i]) && isset($_POST['old_id'.$i]) && !empty($_POST['old_id'.$i])) {
						$data['modified_date'] =$now;
						$this->Quotation_item_model->update($_POST['old_id'.$i], $data);
					//沒有deleted 也沒有old_id 代表要新增這個資料	
					} else if ( isset($_POST['is_deleted'.$i]) && empty($_POST['is_deleted'.$i]) && isset($_POST['old_id'.$i]) && empty($_POST['old_id'.$i])){
						
						//至少要有product_id才能insert
						if(!empty($data['product_id']) && !empty($data['model_no'])) {
							$data['created_date'] =$now;
							$this->Quotation_item_model->insert($data);
						}
						
					}
																														
				}
		  }
		  }
		  
		  //Send Notification
		  if(isset($mail_array['related_role'])){
		  if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
		  
			  $sitename = $this->Settings_model->get_settings(1);
			  $default_email = $this->Settings_model->get_settings(6);
			 
			  $related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
			  $this->load->library('email');
			  foreach($related_role_data as $k => $v){
				  
				$this->email->from($default_email['value'], $sitename['value']);
				$this->email->to($v['email']); 
				
				$this->email->subject($mail_array['title']);
				$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
				$this->email->send();	
				  
			  }
		  
		  }		 
		  }
		  
		  
		  //auto create pr/sr task
		  if(isset($auto_create)){
			  
			//related item type
			$types = $this->Quotation_item_model->getRelatedItem_groupBy($id);
			  
			$product_type = 0;
			$service_type = 0;
			
			//check how many type of product
			foreach($types as $k => $v){
				  
				if($v['type'] == 1){
					$product_type = 1;
				}else if($v['type'] == 2){
					$service_type = 1;
				}
				  
			}
			
			//create pr task
			if($product_type == 1){
					
				  //job
				  $newJob = array(
					'parent_id' 		=>$quotation_data['latest_job_id'],
					'user_id' 			=>$this->data['userdata']['id'],
					'customer_id'		=>$quotation_data['customer_id'],
					'type'		 		=>'Purchase',
					'status_id' 			=>-1,
					'status_text' 		=>'Pending Create Purchase Request',
					'created_date'		=>date('Y-m-d H:i:s'),
					'modified_date'		=>date('Y-m-d H:i:s'),
					'awaiting_role_id'	=>'8,',
					'serial'			=>$quotation_data['qr_serial'],
				  );
				  
				  //insert job
				  $new_job_id = $this->Job_model->insert($newJob);
				  
				  //job
				  $newJob = array(
					'type_path' 	=>'sales/purchase_add/'.$id.'/'.$new_job_id,
				  );
				  
				  //update job
				  $this->Job_model->update($new_job_id,$newJob);
				  
			}
			
			//create sr task
			if($service_type == 1){
				  
				  //job
				  $newJob = array(
					'parent_id' 		=>$quotation_data['latest_job_id'],
					'user_id' 			=>$this->data['userdata']['id'],
					'customer_id'		=>$quotation_data['customer_id'],
					'type' 				=>'Service',
					'status_id' 			=>-1,
					'status_text' 		=>'Pending Create Service Request',
					'created_date'		=>date('Y-m-d H:i:s'),
					'modified_date'		=>date('Y-m-d H:i:s'),
					'awaiting_role_id'	=>'8,',
					'serial'			=>$quotation_data['qr_serial'],
				  );
				  
				  //insert job
				  $new_job_id = $this->Job_model->insert($newJob);

				  //job
				  $newJob = array(
					'type_path'		=>'sales/service_request_add/'.$id.'/'.$new_job_id,
				  );
				  
				  //update job
				  $this->Job_model->update($new_job_id,$newJob);
				  
			}
			
			//update quotation data
			$qr_array = array(
			'latest_job_id' => $new_job_id,
			);  
			$this->Quotation_model->update($id, $qr_array);
			  
			  
		  }
		  
		  		  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		  
		  
	  }
	  
	  public function del($id){
		  $this->Quotation_model->delete($id);
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		
	  }
	  
	  private function PDF_generation($id, $mode='I') {
		  
		  $quotation_data = $this->Quotation_model->get($id);
		  $product = $this->Quotation_item_model->getRelatedItem($id);
		  $customer = $this->Customers_model->get($quotation_data['customer_id']);
		  
		  //take supplier name
		  foreach($product as $k => $v){
			  
			  $product_tmp = $this->Products_model->get($v['product_id']);
			  $supplier_tmp = $this->Suppliers_model->get($product_tmp['supplier']);
			  $product[$k]['supplier_name'] = $supplier_tmp['company_name'];
		  }
		  
		  
		  $quotation_data['received_date'] = date('d/m/Y',strtotime($quotation_data['received_date']));
		  
		  $staff_info = $this->Employee_model->get($quotation_data['requestor_id']);
		  $staff_department = $this->Department_model->get($quotation_data['department_id']);
		  $shipper_data = $this->Shipping_model->get($quotation_data['shipper']);
		  
		  
		  //print_r($product);exit;
		  
		  $item_group = '';
		  foreach($product as $k => $v){
			  $item_group .='<tr><td>'.($k+1).'</td><td>'.$v['supplier_name'].'</td><td>'.$v['model_no'].'</td><td>'.$v['quantity'].'</td><td>'.$v['unit_price'].'</td><td>'.$v['extended_price'].'</td></tr>';
		  }
		  
		  $this->load->library("Pdf");		
          $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		  
		  // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Anexus');
            $pdf->SetTitle("Return Material Authorization");
            $pdf->SetSubject("");
            $pdf->SetKeywords("");

            // remove default header/footer
            $pdf->setPrintHeader(false);			
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(10, 10, 10);

            // set auto page breaks
            //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
            }

            // ---------------------------------------------------------

            // set font
            $pdf->SetFont('times', '', 10);

            

            // set font
            $pdf->SetFont('times', '', 10);
			
			// add a page
       		$pdf->AddPage();
			
			
			$html = '			
			<h1>Quotation</h1>
			
			<table width="100%" cellpadding="5">
				<tr>
					<td>
						<table  width="100%" cellpadding="5" border="1">
			
							<tr>
								<th><h3>Quotation Request Information</h3></th>
							</tr>
							<tr>
								<th><h4>QR Serial No.</h4></th>
							</tr>
							<tr>
								<td>'.$quotation_data['qr_serial'].'</td>
							</tr>
							<tr>
								<th><h4>Date Received</h4></th>
							</tr>
							<tr>
								<td>'.$quotation_data['received_date'].'</td>
							</tr>
	
						</table>
						<table  width="100%"  cellpadding="5" border="1">
			
							<tr>
								<th><h3>Staff Information</h3></th>
							</tr>
							<tr>
								<th><h4>Requestor Name</h4></th>
							</tr>
							<tr>
								<td>'.$staff_info['full_name'].'</td>
							</tr>
							<tr>
								<th><h4>Date of Request</h4></th>
							</tr>
							<tr>
								<td>'.date('d/m/Y',strtotime($quotation_data['request_date'])).'</td>
							</tr>
							<tr>
								<th><h4>Department</h4></th>
							</tr>
							<tr>
								<td>'.$staff_department['name'].'</td>
							</tr>
							<tr>
								<th><h4>Requestor To</h4></th>
							</tr>
							<tr>
								<td>'.$quotation_data['requestor_to'].'</td>
							</tr>
	
						</table>
					</td>
					
					<td>
						<table  width="100%"  cellpadding="5" border="1">
			
							<tr>
								<th><h3>Customer Information</h3></th>
							</tr>
							<tr>
								<th><h4>Company Name</h4></th>
							</tr>
							<tr>
								<td>'.$customer['company_name'].'</td>
							</tr>
							<tr>
								<th><h4>Attention</h4></th>
							</tr>
							<tr>
								<td>'.$customer['primary_attention_to'].'</td>
							</tr>
							<tr>
								<th><h4>Address</h4></th>
							</tr>
							<tr>
								<td>'.$customer['address'].'</td>
							</tr>
							<tr>
								<th><h4>Postal Code</h4></th>
							</tr>
							<tr>
								<td>'.$customer['postal'].'</td>
							</tr>
							<tr>
								<th><h4>Email</h4></th>
							</tr>
							<tr>
								<td>'.$customer['email'].'</td>
							</tr>
							<tr>
								<th><h4>Contact</h4></th>
							</tr>
							<tr>
								<td>'.$customer['contact_info'].'</td>
							</tr>
		
						</table>
						
						<table width="100%"  cellpadding="5" border="1">
			
							<tr>
								<th><h3>Shipper Information</h3></th>
							</tr>
							<tr>
								<th><h4>Shipper</h4></th>
							</tr>
							<tr>
								<td>'.$shipper_data['shipper_name'].'</td>
							</tr>
							<tr>
								<th><h4>Est. Shipping</h4></th>
							</tr>
							<tr>
								<td>'.$quotation_data['est_shipping'].'</td>
							</tr>
		
						</table>
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
						<table cellpadding="5" width="100%" border="1">				
							<tr>
								<th colspan="6"><h3>Items</h3></th>
							</tr>
							<tr>
								<td>#</td>
								<td>Principal/Supplier</td>
								<td>Model No</td>
								<td>Quantity</td>
								<td>Unit Price</td>
								<td>Extended Price</td>
							</tr>'.$item_group.'
						</table>
						
						<table cellpadding="5" border="1" width="100%">
							<tr><td>'.$quotation_data['qr_remarks'].'</td></tr>
						</table>
					</td>
				</tr>

			</table>
			
			';						
			
			$pdf->writeHTML($html, true, false, true, false, '');
			
			$file_name = date("YmdHis").rand(1000,9999);
			
			if($mode == 'I') {
				$pdf->Output($file_name.'.pdf', 'I'); 	
				return $file_name.'.pdf';
			} else if ($mode == 'f') {				
				$pdf->Output('./uploads/'.$file_name.'.pdf', 'f'); 
				return './uploads/'.$file_name.'.pdf';
			}
			
		  		  
		  
	  }
	  
	  public function export_pdf($id){		  
		  $this->PDF_generation($id, 'I');			
	  }
	  
	  public function sent_mail($id,$product_total=0,$service_total=0){
		  		  
		   	$filename = $this->PDF_generation($id, 'f');	 		  
			
			$quotation_data = $this->Quotation_model->get($id);
		  	$product = $this->Quotation_item_model->getRelatedItem($id);
		  	$customer = $this->Customers_model->get($quotation_data['customer_id']);
		  
		  	$send_to = $customer['email'];
			
			$sitename = $this->Settings_model->get_settings(1);
		  	$default_email = $this->Settings_model->get_settings(6);
			
			//$requestor_role
			$requestor_employee = $this->Employee_model->get($quotation_data['requestor_id']);
			$requestor_role = $requestor_employee['role_id'];
			
			$role_id = $this->data['userdata']['role_id'];
			
		  	
			
			/*
			define('GENERAL_MANAGER', 2);
		    define('LOGISTIC', 6);
		    define('SALES_MANAGER', 7);
		    define('SALES_EXECUTIVE', 8);	
			*/
			
			///////////////////////////////////////////////////////////
			
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $quotation_data['status'],
							'workflow_id' => 1,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(1);
						$status = json_decode($workflow['status_json'],true);
						
						if(isset($action)){
		
						if($action == 'send_email'){
							
							//next status
							$next_status = $workflow_order['next_status_id'];
							$next_status_text = $status[$workflow_order['next_status_id']];
							$who_email = $workflow_order['who_email'];
	
						}
					
						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];
								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 1,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
							
							$jobArray = array(
								'parent_id' 		=>$quotation_data['latest_job_id'],
								'user_id' 			=>$this->data['userdata']['id'],
								'customer_id' 		=>$quotation_data['customer_id'],
								'type' 				=>'Quotation',
								'type_path' 		=>'sales/quotation_edit/',
								'awaiting_role_id' 	=>$awating_person.',',
								'status_id' 		=>$next_status,
								'status_text'		=>$next_status_text,
								'serial' 			=>$quotation_data['qr_serial'],
								'type_id' 			=>$quotation_data['id'],
								'created_date'		=>date("Y-m-d H:i:s"),
								'modified_date'		=>date("Y-m-d H:i:s"),
							);			
					  
						    $array = array(
							'awaiting_table' => $awating_person.',',
							'status' => $next_status,
							'modified_date' => date('Y-m-d H:i:s'),
							'lastupdate_user_id' => $this->data['userdata']['id'],
						    );
					  
						  //更新上一個JOB的狀態					  
						  $this->Job_model->update($jobArray['parent_id'], array(
							'is_completed' => 1,
							'display' => 0,
						  ));
						  
						  //新增一個新的JOB
						  $new_job_id = $this->Job_model->insert($jobArray);
						  $array['latest_job_id'] = $new_job_id;
						  
						  $this->Quotation_model->update($quotation_data['id'], $array);
							
							
						}
						
						}
			
			
			if(isset($who_email)){
			
				$this->load->library('email');
				
				switch($who_email){
					case 'customer':
							
							//Send Notification
							if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
										
								$this->email->from($default_email['value'], $sitename['value']);
								$this->email->to($send_to); 
							
								$this->email->subject('Anexus '.$workflow['title'].': '.$quotation_data['qr_serial']);
								$this->email->message($quotation_data['qr_serial']);	
								$this->email->attach($filename);
							
								$this->email->send();
							
							}
							
					break;	
					case 'supplier':
						
							  //count supplier
							  $supplier_count = $this->Purchase_request_item_model->get_supplier_groupBy($id);
							  
							  foreach($supplier_count as $k => $v){
									
								  $supplier = $this->Suppliers_model->get($v['supplier_id']);
								  
								  $file_name = $this->PDF_generation($id, 'f',$v['supplier_id']);	
										 
								  if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
									
									$this->load->library('email');
									$this->email->clear();	
									$this->email->from($default_email['value'], $sitename['value']);
									$this->email->to($supplier ['email']); 
									
									$this->email->subject('Anexus Purchase request '.$pr_data['purchase_serial']);
									$this->email->message($pr_data['purchase_serial']);	
									$this->email->attach($file_name);
									
									$this->email->send();
									
								  }
							  
							  }
							
					break;
					case 'employee':
					break;
				
				}
				
			}
			
			
			
			
			
			///////////////////////////////////////////////////////////
			
			
			
			/*
			//sale service executive send_out
			if($quotation_data['status'] == 4){
				  if(in_array($this->data['userdata']['role_id'], array(SALES_MANAGER,SALES_EXECUTIVE,GENERAL_MANAGER))){
					  			 
					  $jobArray = array(
						'parent_id' =>$quotation_data['latest_job_id'],
						'user_id' =>$this->data['userdata']['id'],
						'customer_id' =>$quotation_data['customer_id'],
						'type' => 'Quotation',
						'type_path' =>'sales/quotation_edit/',
						'awaiting_role_id' => SALES_MANAGER.','.SALES_EXECUTIVE.',',
						'status_id' => 5,
						'status_text'=>$this->data['quotation_status_list'][5],
						'serial' =>$quotation_data['qr_serial'],
			  			'type_id' =>$quotation_data['id'],
						'created_date'	=> date("Y-m-d H:i:s"),
						'modified_date'	=> date("Y-m-d H:i:s"),
					  );			
					  
					  $array = array(
					  	'awaiting_table' => SALES_MANAGER.','.SALES_EXECUTIVE.',',
						'status' => 5,
					  );
					  

					  //更新上一個JOB的狀態					  
					  $this->Job_model->update($jobArray['parent_id'], array(
					  	'is_completed' => 1,
					  ));
					  
					  //新增一個新的JOB
					  $new_job_id = $this->Job_model->insert($jobArray);
					  $array['latest_job_id'] = $new_job_id;
					  
					  $this->Quotation_model->update($quotation_data['id'], $array);
				  }
				  
			}
			*/
			
			 //mail
			 $mail_array = array(
				'creater_name' =>$this->data['userdata']['name'],
				'title' =>'Quotation '.$quotation_data['qr_serial'].' has been send out',
			 );
			 
			 if(isset($new_job_id)){
			  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
			  $mail_array['related_role'] = $related_role;		
			  $mail_array['title'] = $workflow['title']." ".$quotation_data['qr_serial']." ".$next_status_text;
			 }
					
			/*		
			//no equal 8 , mean create by SM
			 if($product_total >= 150000 || $service_total >= 50000){
				if($requestor_role != SALES_EXECUTIVE){
					$mail_array['related_role'] = array(SALES_MANAGER,GENERAL_MANAGER,LOGISTIC);
				}else{
					$mail_array['related_role'] = array(SALES_EXECUTIVE,GENERAL_MANAGER,LOGISTIC);
				}
							
			 }else{
			    if($requestor_role != SALES_EXECUTIVE){
					$mail_array['related_role'] = array(SALES_MANAGER,LOGISTIC);
				}else{
					$mail_array['related_role'] = array(SALES_EXECUTIVE,SALES_MANAGER,LOGISTIC);
				}
			}		
			*/			  
					  
			//sent Notification to related role						  			
		  	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local" && isset($mail_array['related_role'])) {
				$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
			  				  	
			  	foreach($related_role_data as $k => $v){
				  
				  	$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
					
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->send();	
				  
		 		}
			}
			
			$lastpage = $this->session->userdata("lastpage");
			if(!empty($lastpage)) {
			  redirect($lastpage,'refresh');  
			} else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
			}		 			
		  
	  }
	  
	  private function parse_boolean($string){
		  return eval("return (".$string.");");
	  }
	  

}

?>