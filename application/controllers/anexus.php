<?php

class Anexus extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();       
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Customers_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Employee_model');

            $this->data['init'] = $this->function_model->page_init();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$userdata = $this->session->userdata("userdata");
				
				//print_r($userdata);exit;
				if(strpos($userdata['role_id'],',')){
					$userdata['role_id'] = explode(',',$userdata['role_id']);	
				}else{
					$userdata['role_id'] = array($userdata['role_id']);	
				}
				
				
				$this->data['userdata'] = $userdata;
				
			}else{
				redirect(base_url('en/login'),'refresh'); 
			} 
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			//role list
			$this->data['role_list'] = $this->Role_model->getIDKeyArray('name');
			
			
			if(in_array(3,$this->data['userdata']['role_id'])){
				
				
				$a_csutomer = $this->data['staff_info']['assign_customer'];
				
				if($a_csutomer != ''){
				
					if(strpos($a_csutomer,',')){
						$a_customer_array = explode(',',$a_csutomer);	
					}else{
						$a_customer_array = array($a_csutomer);	
					}
				
				}else{
					$a_customer_array = array(0);	
				}
				
				$this->data['task_display_count'] = $this->Job_model->record_count2(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
				),$this->data['userdata']['role_id'],$a_customer_array);
				
				
				

			}else{
			
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			}
			
			/*
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}
			*/
            
           
      }
   
   	  //判斷是否已經安裝完成, 如果還沒有就跳去安裝的頁面
      public function index() {  
          	
			//未登入去登入頁面
			if (!$this->data['islogin']) {
				 redirect('/en/login', 'refresh');		
			//已經登入去tasklist
			} else {
				
				//if = customer,redirect promotion page
				if($this->data['userdata']['role_id'] == 7){
					redirect('/en/agora/customer/promotion', 'refresh');	
				}else{
					redirect('/en/agora/tasklist', 'refresh');	
				}
				
			}
           
			
      }
	  
	  public function task_update(){
		  $id = $this->input->post("id", true);
		  $display = $this->input->post("display", true);
		  
		  $update_array = array(
		  	'display'	=> $display,
		  );
		  
		  $this->Job_model->update($id, $update_array);
		  
		  echo json_encode(array(
		  	'status'	=> "OK",
			'result'	=> $display==0?'Show':'Hide',
		  ));
		  
	  }
	  
	  public function tasklist($display = 'ALL',$alert=0) { 
	   
          	$this->data['alert'] = $alert;
					
            $this->data['title'] = ucfirst("Task List"); // Capitalize the first letter		
			
			$filter = array(
				//'user_id'		=> $this->data['userdata']['id'],
				'is_deleted'	=> 0,
				'is_completed'	=> 0,		
			);
			
			if($display != 'ALL') {
				$filter['display'] = $display;
			}
			
			$this->data['display'] = $display;
			
			//sales only see own customer task
			if(in_array(3,$this->data['userdata']['role_id'])){
				
				
				$a_csutomer = $this->data['staff_info']['assign_customer'];
				
				if($a_csutomer != ''){
				
					if(strpos($a_csutomer,',')){
						$a_customer_array = explode(',',$a_csutomer);	
					}else{
						$a_customer_array = array($a_csutomer);	
					}
				
				}else{
					$a_customer_array = array(0);	
				}
				
				$this->data['task_list'] = $this->Job_model->get_where_sales($filter,$this->data['userdata']['role_id'],$a_customer_array);
				
				
				

			}else{
				
				$this->data['task_list'] = $this->Job_model->get_where($filter,$this->data['userdata']['role_id']);
				
			}
			
			
			
			
			foreach($this->data['task_list'] as $k => $v){
				if(!empty($v['awaiting_role_id'])){
						$this->data['task_list'][$k]['awaiting_role_id']= explode(',',$v['awaiting_role_id']);
				}
			}
			
			$url = base_url().$this->data['init']['langu'].'/agora/tasklist/';
			$this->session->set_userdata("lastpage", $url);						
			
			$this->data['customer_list'] = $this->Customers_model->getIDKeyArray("company_name");
			
			$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
			
			$this->session->set_userdata("lastpage", $url.$display);	
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }
	  

}

?>