<?php

class Jobs_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Customers_model');
			$this->load->model('Order_model');
			$this->load->model('Packing_list_model');
			$this->load->model('Order_item_model');
			$this->load->model('Packing_list_item_model');
			$this->load->model('Delivery_order_model');
			$this->load->model('Invoice_model');
			$this->load->model('Employee_model');
			$this->load->model('Workflow_model');

			$this->load->library('Mobile_Detect'); //for mobile view

            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
			$this->data['role_list'] = $this->Role_model->getIDKeyArray('name');
			$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
			$this->data['customer_list'] = $this->Customers_model->getIDKeyArray("company_name");
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$userdata = $this->session->userdata("userdata");
				
				
				if(strpos($userdata['role_id'],',')){
					$userdata['role_id'] = explode(',',$userdata['role_id']);	
				}else{
					$userdata['role_id'] = array($userdata['role_id']);	
				}
				
				
				$this->data['userdata'] = $userdata;
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}  	
			
			$detect = new Mobile_Detect;
             /*
			if ($detect->isMobile()) {
				$this->data['mobile'] = 1;
			}else{
				$this->data['mobile'] = 0;
			}
			*/
			
			/*
			$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
			print_r($deviceType);exit;
			*/
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			/*
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}
			*/ 
			
			$this->data['status_list'] = $this->Job_model->status_list();
			
			$this->data['model_name'] = 'jobs';
           
      }
   
     public function index($q="ALL", $page=1) {  
          		
          	$this->data['page'] = $page;
			$this->data['title'] = ucfirst($this->data['model_name']);
			
			//assign customer
			if($this->data['userdata']['role_id'] == 3){
				$assign_customer = explode(',',$this->data['staff_info']['assign_customer']);
			}else{
				$assign_customer = '';
			}
			
			//print_r($assign_customer);exit;
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/agora/jobs/'.$q.'/';
			if($q == 'ALL') {
				$q = "";
			}
			$this->data['q'] = $q;
					
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Order_model->record_count($filter, $q,$assign_customer);
			
			//get particular ranged list
			$this->data['results'] = $this->Order_model->fetch($q, $this->data['item_per_page'], $limit_start,$assign_customer);
			
			
			//抓目前status
			if(!empty($this->data['results'])){
			foreach($this->data['results'] as $k => $v){
				
				//status is completed
				if($v['status'] == 2){
					
					$related_packing = $this->Packing_list_model->get_where(array('is_deleted'=>0,'status'=>1,'order_id'=>$v['id']));
					
					if(!empty($related_packing)){

						//order 的數量
						$order_item = $this->Order_item_model->getRelatedItem($v['id']);
						$order_qty = 0;
						if(!empty($order_item)){
							foreach($order_item as $k2=>$v2){
								$order_qty += $v2['quantity'];
							}
						}
						
						//packing 數量
						$packing_qty = 0;
						foreach($related_packing as $k2 => $v2){
							
							$packing_item = $this->Packing_list_item_model->getRelatedItem($v2['id']);
							
							foreach($packing_item as $k3 => $v3){
								$packing_qty += $v3['quantity_pack'];
							}
							
						}
						
						//packing list 的數量
						$pack_count = count($related_packing);
						
						//判斷item pack完了沒
						if($order_qty == $packing_qty){
							
							//目前有的delivery order
							$related_do = count($this->Delivery_order_model->get_where(array('is_deleted'=>0,'status'=>2,'order_id'=>$v['id'])));
							
							if($related_do == 0){
								$this->data['results'][$k]['status_text'] = 'Pending Delivery Order';
							//代表do已處理完畢
							}else if($related_do == $pack_count){
								$this->data['results'][$k]['status_text'] = 'Completed';
								/*
								//do 數量
								$do_count = $related_do;
								
								//invoice 數量
								$related_invoice = count($this->Invoice_model->get_where(array('is_deleted'=>0,'status'=>1,'order_id'=>$v['id'])));
								
								if($related_invoice == 0){
									$this->data['results'][$k]['status_text'] = 'Pending Invoice';
								}else if($do_count == $related_invoice){
									$this->data['results'][$k]['status_text'] = 'Completed';
								}else{
									$this->data['results'][$k]['status_text'] = 'Pending Generate Invoice ['.$related_invoice.' of '.$do_count.']';
								}
								*/
								
							}else{
								$this->data['results'][$k]['status_text'] = 'Pending Generate Delivery Order ['.$related_do.' of '.$pack_count.']';
							}
							
						}else{
							$this->data['results'][$k]['status_text'] = 'Pending Generate Packing List ['.($pack_count+1).' of '.($pack_count+1).']';
						}
						
					}else{
						$this->data['results'][$k]['status_text'] = 'Pending Packing List';
					}
					
				}//endif status
				
			}//end foreach
			}
			
			
			
			if(!empty($this->data['results'])) {		
				foreach($this->data['results'] as $k => $v){
					if(!empty($v['awaiting_table'])){
							$this->data['results'][$k]['awaiting_table']= explode(',',$v['awaiting_table']);
					}
				}		
			}
				
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->data['customer_list'] = $this->Customers_model->getIDKeyArray("company_name");
			$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
			
			$this->session->set_userdata("lastpage", $url.$page);		
						
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }
	  
	  public function job_detail($id) {
		  
		  
		  	$this->data['result'] = $this->Order_model->get($id);
			$this->data['customer'] = $this->Customers_model->get($this->data['result']['customer_id']);
			$this->data['employee_list'] = $this->Employee_model->getIDKeyArray("full_name");
			
		  	
			//抓目前status
			if(!empty($this->data['result'])){
				
				//status is completed
				if($this->data['result']['status'] == 2){
					
					$related_packing = $this->Packing_list_model->get_where(array('is_deleted'=>0,'status'=>1,'order_id'=>$this->data['result']['id']));
					
					if(!empty($related_packing)){

						//order 的數量
						$order_item = $this->Order_item_model->getRelatedItem($this->data['result']['id']);
						$order_qty = 0;
						if(!empty($order_item)){
							foreach($order_item as $k2=>$v2){
								$order_qty += $v2['quantity'];
							}
						}
						
						//packing 數量
						$packing_qty = 0;
						foreach($related_packing as $k2 => $v2){
							
							$packing_item = $this->Packing_list_item_model->getRelatedItem($v2['id']);
							
							foreach($packing_item as $k3 => $v3){
								$packing_qty += $v3['quantity_pack'];
							}
							
						}
						
						//packing list 的數量
						$pack_count = count($related_packing);
						
						//判斷item pack完了沒
						if($order_qty == $packing_qty){
							
							//目前有的delivery order
							$related_do = count($this->Delivery_order_model->get_where(array('is_deleted'=>0,'status'=>2,'order_id'=>$this->data['result']['id'])));
							
							if($related_do == 0){
								
								$this->data['result']['status_text'] = 'Pending Delivery Order';
							//代表do已處理完畢
							}else if($related_do == $pack_count){
								
								$this->data['result']['status_text'] = 'Completed';
								/*
								//do 數量
								$do_count = $related_do;
								
								//invoice 數量
								$related_invoice = count($this->Invoice_model->get_where(array('is_deleted'=>0,'status'=>1,'order_id'=>$this->data['result']['id'])));
								
								if($related_invoice == 0){
									$this->data['result']['status_text'] = 'Pending Invoice';
								}else if($do_count == $related_invoice){
									$this->data['result']['status_text'] = 'Completed';
								}else{
									$this->data['result']['status_text'] = 'Pending Generate Invoice ['.$related_invoice.' of '.$do_count.']';
								}
								*/
								
							}else{
								$this->data['result']['status_text'] = 'Pending Generate Delivery Order ['.$related_do.' of '.$pack_count.']';
							}
							
						}else{
							$this->data['result']['status_text'] = 'Pending Generate Packing List ['.($pack_count+1).' of '.($pack_count+1).']';
						}
						
					}else{
						$this->data['result']['status_text'] = 'Pending Packing List';
					}
					
				//endif status
				}
			}
			
			
			$this->data['packing_result'] = $this->Packing_list_model->get_where(array('is_deleted'=>0,'order_id'=>$id));
			
			$this->data['do_result'] = $this->Delivery_order_model->get_where(array('is_deleted'=>0,'order_id'=>$id));
			
			$this->data['invoice_result'] = $this->Invoice_model->get_where(array('is_deleted'=>0,'order_id'=>$id));
			
			//print_r($this->data['results']);exit;
			
			$status_list = $this->Workflow_model->get(1);
			$this->data['status_list'] = json_decode($status_list['status_json']);
			
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/job_detail', $this->data);
            $this->load->view('anexus/footer', $this->data);
		  
		  
	  }
	  
	 
}

?>