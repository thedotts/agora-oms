<?php

class Reports_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();           
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');

            $this->data['init'] = $this->function_model->page_init();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$userdata = $this->session->userdata("userdata");
				
				//print_r($userdata);exit;
				if(strpos($userdata['role_id'],',')){
					$userdata['role_id'] = explode(',',$userdata['role_id']);	
				}else{
					$userdata['role_id'] = array($userdata['role_id']);	
				}
				
				
				$this->data['userdata'] = $userdata;
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}  
			
			/*
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			} 
			*/
           
      }
   
      public function index() {  
	  
	  		
          			
            $this->data['title'] = ucfirst("Reports"); // Capitalize the first letter		
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/reports/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }
	  
	 
}

?>