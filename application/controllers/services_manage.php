<?php

class Services_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();     
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Products_model');
			$this->load->model('Job_model');
			$this->load->model('Suppliers_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Settings_model');

            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$this->data['userdata'] = $this->session->userdata("userdata");
			}      
			
			$this->data['status_list'] = $this->Products_model->status_list(false);
			$this->data['uom_list'] = $this->Products_model->uom_list();			
			$this->data['supplier_list'] = $this->Suppliers_model->getIDKeyArray("company_name");			
			$this->data['currency_list'] = $this->Products_model->currency_list();
			
			$this->data['group_name'] = "administrator";  
			$this->data['model_name'] = "services";  
			$this->data['common_name'] = "Service";   
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			} 
           
		   
      }
         
	  
	  public function index($q="ALL", $page=1) {  
          			
            $this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,	
				'type'			=> 2,		 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/anexus/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/';
			if($q == 'ALL') {
				$q = "";
			}
			$this->data['q'] = $q;
									
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Products_model->record_count($filter, $q);
			
			//get particular ranged list
			$this->data['results'] = $this->Products_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start);
						
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->session->set_userdata("lastpage", $url.$page);
			
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  	  	 	  	  
	  
	  public function add($id=false) {  
	  
          	if($id !== false) {
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Products_model->get($id);	
			} else {
				$this->data['mode'] = 'Add';	
			}
			$name = $this->Settings_model->get_settings(15);
			$name = $name['value'];
			$this->data['anexus'] = $this->Suppliers_model->getaNexus($name);	
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		            
			
      }	  	
	  
	  public function del($id) {
		  
		  $employee_data = $this->Products_model->get($id);		  
		  $this->Products_model->delete($id);		  
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		
		  
	  }  
	  
	  public function submit(){
		  
		  $mode 					= $this->input->post("mode", true);
		  $id 						= $this->input->post("id", true);		  		  		  		  
		  $status 					= $this->input->post("status", true);		  
		  $model_no 				= $this->input->post("model_no", true);				    
		  $serial 					= $this->input->post("serial", true);		  		  
		  $supplier 				= $this->input->post("supplier", true);		  		  
		  $description 				= $this->input->post("description", true);
		  $uom 						= $this->input->post("uom", true);
		  $currency 				= $this->input->post("currency", true);
		  
		  $unit_price 				= $this->input->post("unit_price", true);
		  $additional_fee 			= $this->input->post("additional_fee", true);		  
		  
		  		  		 		  
		  $iu_array = array(		  			  	
			'status'				=> $status,						
			'model_no'				=> $model_no,
			'serial'				=> $serial,
			'supplier'				=> $supplier,
			'description'			=> $description,
			'uom'					=> $uom,			
			'unit_price'			=> $unit_price,			
			'additional_fee'		=> $additional_fee,			
			'currency'				=> $currency,						
			'unit_price'			=> $unit_price,			
			'additional_fee'		=> $additional_fee,		
			'type'					=> 2,	
		  );		 		  
		  		  		  
		  //Add
		  if($mode == 'Add') {			  			  
			  			  
			  $iu_array['created_date'] = date("Y-m-d H:i:s");			  
			  $this->Products_model->insert($iu_array);			  			  		  	  			  			  
			  
		  //Edit	  			  
		  } else {
			  
			  $iu_array['modified_date'] = date("Y-m-d H:i:s");			  
			  $this->Products_model->update($id, $iu_array);			  			  			  
			  
		  }
		  		  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		  
		  
	  }
	  
	 
}

?>