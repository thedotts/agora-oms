<?php

class Packing_list_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();  
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Customers_model');
			$this->load->model('Tender_model');
			$this->load->model('Tender_item_model');
			$this->load->model('Employee_model');
			$this->load->model('Department_model');
			$this->load->model('Products_model');
			$this->load->model('Suppliers_model');
			$this->load->model('Shipping_model');
			$this->load->model('Products_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Settings_model');
			$this->load->model('Quotation_model');
			$this->load->model('Quotation_item_model');
			$this->load->model('Workflow_model');
			$this->load->model('Workflow_order_model');
			$this->load->model('Order_model');
			$this->load->model('Product_log_model');
			$this->load->model('Order_item_model');
			$this->load->model('Packing_list_model');
			$this->load->model('Packing_list_item_model');
			$this->load->model('Audit_log_model');
			$this->load->model('Delivery_order_model');
			$this->load->model('Invoice_model');
			
            $this->data['init'] = $this->function_model->page_init();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
			
			$this->data['role_list'] = $this->Role_model->getIDKeyArray('name');
			$status_list = $this->Workflow_model->get(2);
			$this->data['status_list'] = json_decode($status_list['status_json']);
			
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$userdata = $this->session->userdata("userdata");
				
				
				if(strpos($userdata['role_id'],',')){
					$userdata['role_id'] = explode(',',$userdata['role_id']);	
				}else{
					$userdata['role_id'] = array($userdata['role_id']);	
				}
				
				
				$this->data['userdata'] = $userdata;
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}       
			
			//employee data
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			//print_r($this->data['staff_info']);exit;
			$this->data['role_list'] = $this->Role_model->getIDKeyArray('name');
			
			$this->data['group_name'] = "ground_staff";  
			$this->data['model_name'] = "packing_list";  
			$this->data['common_name'] = "Packing List";   
			
			if(in_array(3,$this->data['userdata']['role_id']) ){
				
				
				$a_csutomer = $this->data['staff_info']['assign_customer'];
				
				if($a_csutomer != ''){
				
					if(strpos($a_csutomer,',')){
						$a_customer_array = explode(',',$a_csutomer);	
					}else{
						$a_customer_array = array($a_csutomer);	
					}
				
				}else{
					$a_customer_array = array();	
				}
				
				$this->data['task_display_count'] = $this->Job_model->record_count2(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
				),$this->data['userdata']['role_id'],$a_customer_array);
				
				
				

			}else{
			
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			}
			
			/*
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
					
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}  
			*/
            
			//related role
		    define('SUPERADMIN', 1);
		    define('SALES_MANAGER', 7);
		    define('SALES_EXECUTIVE', 8);
		   
      }
   
      public function index($q="ALL", $page=1, $alert=0) {  
          		
			$this->data['alert'] = $alert; 
          			
			$this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/';
			if($q == 'ALL') {
				$q = "";
			}else{
				$q = urldecode($q);
			}
			$this->data['q'] = urldecode($q);
					
			$limit_start = ($page-1)*$this->data['item_per_page'];

			//count total Data
			$this->data["total"] = $this->Packing_list_model->record_count($filter, $q);
			
			//get particular ranged list
			$this->data['results'] = $this->Packing_list_model->fetch($q, $this->data['item_per_page'], $limit_start);
			
			//抓目前status
			if(!empty($this->data['results'])){
			foreach($this->data['results'] as $k => $v){
				
				//status is completed
				if($v['status'] == 1){
					
					$related_packing = $this->Packing_list_model->get_where(array('is_deleted'=>0,'status'=>1,'order_id'=>$v['order_id']));
					
					if(!empty($related_packing)){

						//order 的數量
						$order_item = $this->Order_item_model->getRelatedItem($v['order_id']);
						$order_qty = 0;
						if(!empty($order_item)){
							foreach($order_item as $k2=>$v2){
								$order_qty += $v2['quantity'];
							}
						}
						
						//packing 數量
						$packing_qty = 0;
						foreach($related_packing as $k2 => $v2){
							
							$packing_item = $this->Packing_list_item_model->getRelatedItem($v2['id']);
							
							foreach($packing_item as $k3 => $v3){
								$packing_qty += $v3['quantity_pack'];
							}
							
						}
						
						//packing list 的數量
						$pack_count = count($related_packing);
						
						//判斷item pack完了沒
						if($order_qty == $packing_qty){
							
							
							
							//目前有的delivery order
							$related_do = count($this->Delivery_order_model->get_where(array('is_deleted'=>0,'status'=>2,'order_id'=>$v['order_id'])));
							
							if($related_do == 0){
								$this->data['results'][$k]['status_text'] = 'Pending Delivery Order';
							//代表do已處理完畢
							}else if($related_do == $pack_count){
								
								$this->data['results'][$k]['status_text'] = 'Completed';
								/*
								//do 數量
								$do_count = $related_do;
								
								//invoice 數量
								$related_invoice = count($this->Invoice_model->get_where(array('is_deleted'=>0,'status'=>1,'order_id'=>$v['order_id'])));
								
								if($related_invoice == 0){
									$this->data['results'][$k]['status_text'] = 'Pending Invoice';
								}else if($do_count == $related_invoice){
									$this->data['results'][$k]['status_text'] = 'Completed';
								}else{
									$this->data['results'][$k]['status_text'] = 'Pending Generate Invoice ['.$related_invoice.' of '.$do_count.']';
								}
								*/
								
							}else{
								$this->data['results'][$k]['status_text'] = 'Pending Generate Delivery Order ['.($related_do+1).' of '.$pack_count.']';
							}
							
						}else{
							$this->data['results'][$k]['status_text'] = 'Pending Generate Packing List ['.($pack_count+1).' of '.($pack_count+1).']';
						}
						
					}else{
						$this->data['results'][$k]['status_text'] = 'Pending Packing List';
					}
					
				}//endif status
				
			}//end foreach
			}
			
			
			if(!empty($this->data['results'])) {		
				foreach($this->data['results'] as $k => $v){
					if(!empty($v['awaiting_table'])){
							$this->data['results'][$k]['awaiting_table']= explode(',',$v['awaiting_table']);
					}
				}		
			}
				
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->data['customer_list'] = $this->Customers_model->getIDKeyArray("company_name");
			$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
			
			$this->session->set_userdata("lastpage", $url.$page);		
						
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  
	  public function add($id=false,$job_id=false) {  
          			
			$role_id = $this->data['userdata']['role_id'];
					
			if($id !== false && $job_id === false) {
				
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Packing_list_model->get($id);
					
				$packing_date = $this->data['result']['packing_date'];
				$tmp = explode('-',$packing_date);
				$this->data['result']['packing_date'] = $tmp[2]."/".$tmp[1]."/".$tmp[0];
				
				//customer infomation
				$this->data['customer_info'] = $this->Customers_model->get($this->data['result']['customer_id']);
				
				//product
				$product = $this->Packing_list_item_model->getRelatedItem($id);
				foreach($product as $k => $v){
					$tmp_product = $this->Products_model->get($v['product_id']);
					$product[$k]['uom'] = $tmp_product['uom'];
					$product[$k]['inventory_uom'] = $tmp_product['inventory_uom'];
				}
				$this->data['result']['product'] = $product;
				$this->data['product_list'] = $this->Products_model->getIDKeyArray('product_name');
				
				$requestor_employee = $this->Employee_model->get($this->data['result']['requestor_id']);
			    $requestor_role = $requestor_employee['role_id'];
				$this->data['requestor_role'] = $requestor_role;
				if(!empty($this->data['result']['awaiting_table'])){
				$this->data['result']['awaiting_table'] = explode(',',$this->data['result']['awaiting_table']);
				}
				
				$default = 0;
				
				//get related workflow order data
				$workflow_flitter = array(
					'status_id' => $this->data['result']['status'],
					'workflow_id' => 2,
				);
						
				$type='';
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						
				//print_r($requestor_role);exit;
				//$workflow_order['role_id'] == 'requestor'
				if($workflow_order['role_id'] == 'requestor'){
							
					//requestor edit
					if(in_array($requestor_role,$role_id)&& $workflow_order['requestor_edit']) {
					$type = 'edit';	
									
					//user role = current status role
					}else if(in_array($requestor_role,$role_id)){
						$type = $workflow_order['action'];
					}
							
				//$workflow_order['role_id'] != 'requestor'						
				}else{
							
					//user role = current status role
					if (in_array($workflow_order['role_id'],$role_id) || in_array(1,$this->data['userdata']['role_id']) || in_array(8,$this->data['userdata']['role_id'])){
						$type = $workflow_order['action'];	
							
					//requestor edit															
					}else if(in_array($requestor_role,$role_id) && $workflow_order['requestor_edit']) {
						$type = 'edit';	
					}
						
				}
				

				//data form database
				$workflow = $this->Workflow_model->get(2);
				$status = json_decode($workflow['status_json'],true);
						
				$this->data['workflow_title'] = $workflow['title'];
					
				$this->data['btn_type'] = $type;
				//print_r($workflow_order);exit;
				
					switch($type){
						case 'edit':
							$this->data['head_title'] = 'Edit '.$workflow['title'].' Request';
						break;
						case 'approval':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
						break;
						case 'approval_update':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'update':
							$this->data['head_title'] = 'Update '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'send_email':
							$this->data['head_title'] = 'Waiting to Send '.$workflow['title'].' Request';
						break;
						case 'sendmail_update':
							$this->data['head_title'] = 'Waiting to Send/Edit '.$workflow['title'].' Request';
						break;
						case 'upload':
							$this->data['head_title'] = 'Upload Confirm File '.$workflow['title'].' Request';
						break;
						case 'complete':
							$this->data['head_title'] = $workflow['title'].' Request';
							$this->data['last_action'] = $workflow_order['action_before_complete'];
						break;
						default:
							$this->data['head_title'] = $workflow['title'].' Request';
					}
					//echo $this->data['head_title'];exit;
				foreach(json_decode($workflow['status_json'],true) as $k => $v){
						
					if($v == 'Completed'){
						$this->data['completed_status_id'] = $k;
						break;
					}
				}

				
			} else {
				
				$this->data['mode'] = 'Add';
				$this->data['head_title'] = 'New Packing List';	

				//get related workflow order data
					$workflow_flitter = array(
						//'role_id' => $role_id,
						'status_id' => -1,
						'workflow_id' => 2,
					);
					
					//data form database
					$workflow = $this->Workflow_model->get(2);
					$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter,$role_id);
					
					//print_r($workflow_order);exit;
					
					//get view bottom layout
					$type = '';
					if(!empty($workflow_order)){
						$type = $workflow_order['action'];
						if($workflow_order['action'] == 'create'){
							$this->data['head_title'] = 'Create '.$workflow['title'].' Request';
						}else{
							$this->data['head_title'] = $workflow['title'].' Request';
						}
					}else{
						$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					$this->data['btn_type'] = $type;
					
					//auto select quotation id
					$this->data['order_id'] = 0;
					if($id !== false){
					$this->data['order_id'] = $id;
					}
					//for complete the job
					$this->data['job_id'] = $job_id;
					
			}		
			
			//all product 
			$products = $this->Products_model->get();
			//sum product by id
			foreach($products as $k => $v){
				$products[$k]['qty'] = $this->Product_log_model->sum_by_id($v['id']);
			}
			
			$this->data['product'] = json_encode($products);
			
			$this->data['company_list'] = json_encode($this->Customers_model->getAll());
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);
			
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);	
			
      }
	  
	  public function submit(){

		  $now = date("Y-m-d H:i:s");
		  $mode = $this->input->post("mode", true);
		  $id = $this->input->post("id", true);
		  
		  $role_id = $this->data['userdata']['role_id'];

		  //form data
		  $requestor_id 	= $this->input->post("requestor_id", true);
		  $packing_date 	= $this->input->post("packing_date", true);
		  $customer_id 		= $this->input->post("customer_id", true);
		  $customer_name 	= $this->input->post("customer_name", true);
		  $order_id 		= $this->input->post("order_id", true);
		  $order_no 		= $this->input->post("order_no", true);
		  $staff_name 		= $this->input->post("staff_name", true);
		  $staff_role 		= $this->input->post("staff_role", true);
		  $remarks 			= $this->input->post("remarks", true);

		  //get value approve or not approve
		  $confirm = $this->input->post("confirm", true);
		  $correct_check = $this->input->post("check_correct", true);
		  
		  $next = 0;
		  $last = 0;
		  $status_change = 0;
		  
		  if(!empty($packing_date)) {
			  $tmp = explode("/", $packing_date);
			  $packing_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
		  }
		  
 		  //job
		  $jobArray = array(
			'parent_id' =>$this->input->post('job_id',true),
			'user_id' =>$this->data['userdata']['id'],
			'customer_id' =>isset($customer_id)?$customer_id:'',
			'customer_name' =>isset($customer_name)?$customer_name:'',
			'type' => 'Packing List',
			'type_path' =>'ground_staff/packing_list_edit/',
	 	  );
 
 		  //main
		  $array = array(
		  	'order_no'			=> $order_no,
		  	'order_id'			=> $order_id,
		  	'requestor_id'		=> $requestor_id,
		  	'packing_date'		=> $packing_date,
			'customer_id'		=> isset($customer_id)?$customer_id:'',
			'customer_name'		=> isset($customer_name)?$customer_name:'',
			'staff_name'		=> $staff_name,
			'staff_role'		=> $staff_role,
			'remark'			=> $remarks,
		  );
		  
		  //mail
		  $mail_array = array(
		  'creater_name' =>$this->data['userdata']['name'],
		  );
		  
		  
		  $tender_id = "";
		  //Add
		  if($mode == 'Add'){
				
				//get related workflow order data
				$workflow_flitter = array(
					//'role_id' => $role_id,
					'status_id' => -1,
					'workflow_id' => 2,
				);
				
				//data form database
				$workflow = $this->Workflow_model->get(2);
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter,$role_id);
				$status = json_decode($workflow['status_json'],true);
				
				if(!empty($workflow_order)){
				
				//ad_hoc
				if($workflow_order['ad_hoc'] == 1){
					$array['ad_hoc'] = 1;
				}
				
				//purchaser
				$is_adhoc = false;
				if(in_array(10,$role_id)){
					$is_adhoc = true;
				}
				
				//next status
				$next_status = $workflow_order['next_status_id'];
				$next_status_text = $status[$workflow_order['next_status_id']];
				
			  	//if need_det = 1
				if($workflow_order['need_det'] == 1){
					
					$formula = json_decode($workflow_order['formula'],true);
					
					//check if product or service price over value
					foreach($formula as $k => $v){
					
						$price = $this->input->post($v['target_colum'], true);
						$det_temp = $price." ".$v['logic']." ".$v['value'];
						$check = $this->parse_boolean($det_temp);
						
						//check price over value
						if($check){
							$next_status = $v['next_status_id'];
							$next_status_text = $status[$v['next_status_id']];
							break;
						}
						
					}
					
				}
				
				//get next status workflow order data
				$next_workflow_flitter = array(
					'status_id' => $next_status,
					'workflow_id' => 2,
				);
				
				//next status data
				$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
				$awating_person = $next_status_data['role_id'];
				
				if($awating_person == 'requestor'){
					$awating_person = $this->input->post("requestor_id", true);
				}
				
				//main
				$array['awaiting_table']  = $awating_person;
				$array['status'] = $next_status;
				//main
				$jobArray['awaiting_role_id'] = $awating_person.',';
				$jobArray['status_id'] = $next_status;
				$jobArray['status_text'] = $next_status_text; 	
				  
				
				//if order_id != 0 ,get the latest job id of order
				if($array['order_id'] != ''){
					$data_previous_flow = $this->Order_model->get($array['order_id']);
					$jobArray['parent_id'] = $data_previous_flow['latest_job_id'];
					
					/*
					//update order job completed
					$last_job_array = array('is_completed'=>1);
					$this->Job_model->update($data_previous_flow['latest_job_id'],$last_job_array);
					*/
				}
				

				//insert main data
				$array['created_date'] = $now;
				$array['modified_date'] = $now;
				$array['create_user_id'] = $this->data['userdata']['id'];
				$array['lastupdate_user_id'] = $this->data['userdata']['id'];

				//新增資料
				$data_id = $this->Packing_list_model->insert($array);				
				$p_id = $data_id;
					
				//更新serial
				$data_serial = $this->Packing_list_model->zerofill($data_id, 5, $is_adhoc);
					
				//insert job
				$jobArray['serial'] = $data_serial;
				$jobArray['order_no'] = $array['order_no'];
				$jobArray['type_id'] = $data_id;
					
				$jobArray['created_date'] = $now;
				$jobArray['modified_date'] = $now;
				
				/*
				//if next action == complete,create job auto complete
				if($next_status_data['action'] == 'complete'){
					$jobArray['is_completed'] = 1;
				}
				*/
				
				if($workflow_order['next_status_text'] == 'Completed'){
					
					   //job
					  $newJob = array(
						'parent_id' 		=>$this->input->post('job_id',true),
						'user_id' 			=>$this->data['userdata']['id'],
						'customer_id'		=> $customer_id,
						'customer_name'		=> $customer_name,
						'type'		 		=> 'Delivery Order',
						'status_id' 		=> -1,
						'status_text' 		=>'Pending Create Delivery Order',
						'created_date'		=>date('Y-m-d H:i:s'),
						'modified_date'		=>date('Y-m-d H:i:s'),
		 				'awaiting_role_id'	=>'5,',
						'serial'			=>$data_serial,
						'order_no'			=>$array['order_no'],
					  );
					  
					  //insert job
					  $new_job_id = $this->Job_model->insert($newJob);
					  
					  //job
					  $newJob = array(
						'type_path' 	=>'admin/delivery_order_add/'.$data_id.'/'.$new_job_id,
					  );
					  
					  //update job
					  $this->Job_model->update($new_job_id,$newJob);
					  
					  $job_id = $new_job_id;
					  
					
				}else{
					$job_id = $this->Job_model->insert($jobArray);
				}
					
				  
				//after insert job,update job id,qr serial....
				$array =array(
					'job_id'	=> $job_id,
					'packing_no'  => $data_serial,
					'latest_job_id'	=> $job_id,
				);
				$this->Packing_list_model->update($data_id, $array);
					
				//mail
				$related_role = $this->Job_model->getRelatedRoleID($job_id);
				$mail_array['related_role'] = $related_role;		
				$mail_array['title'] = $workflow['title']." ".$data_serial." ".$next_status_text; 
				  	
				}
				
				  
					  //audit log
					  $log_array = array(
						'ip_address'	=> $this->input->ip_address(),
						'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
						'table_affect'	=> 'packing_list',
						'description'	=> 'Create Packing List',
						'created_date'	=> date('Y-m-d H:i:s'),
					  );
						  
					  $audit_id = $this->Audit_log_model->insert($log_array);	
					  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
					  $update_array = array(
						 'log_no'	=> $custom_code,
					  );
					  $this->Audit_log_model->update($audit_id, $update_array);
				
		  //Edit	  			  
		  } else {
			  
			  	$pre_data = $this->Packing_list_model->get($id);
			 	$requestor_employee = $this->Employee_model->get($pre_data['requestor_id']);
			  	$requestor_role = $requestor_employee['role_id'];
			  	$pre_status = $pre_data['status'];
			  
			  	$jobArray['serial'] =$pre_data['packing_no'];
				$jobArray['order_no'] = $pre_data['order_no'];
			  	$jobArray['type_id'] =$pre_data['id'];
			  	$jobArray['parent_id'] = $pre_data['latest_job_id'];
				
				//item parent id
				$p_id = $id;
				
			    //can edit status
			    $edit_status = $this->Workflow_order_model->get_status_edit(2);
			  
			    //requestor edit
			    if(in_array($pre_status, $edit_status) && in_array($requestor_role,$role_id)){
			  	  
				//get related workflow order data
				$workflow_flitter = array(
					//'role_id' => $role_id,
					'status_id' => -1,
					'workflow_id' => 2,
				);
						
				//data form database
				$workflow = $this->Workflow_model->get(2);
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter,$role_id);
				$status = json_decode($workflow['status_json'],true);
						
				if(!empty($workflow_order)){
						
					//next status
					$next_status = $workflow_order['next_status_id'];
					$next_status_text = $status[$workflow_order['next_status_id']];
						
					//if need_det = 1
					if($workflow_order['need_det'] == 1){
							
						$formula = json_decode($workflow_order['formula'],true);
							
						//check if product or service price over value
						foreach($formula as $k => $v){
							
							$price = $this->input->post($v['target_colum'], true);
							$det_temp = $price." ".$v['logic']." ".$v['value'];
							$check = $this->parse_boolean($det_temp);
								
							//check price over value
							if($check){
								$next_status = $v['next_status_id'];
								$next_status_text = $status[$v['next_status_id']];
								break;
							}
								
						}
							
					}
						
					//get next status workflow order data
					$next_workflow_flitter = array(
						'status_id' => $next_status,
						'workflow_id' => 2,
					);
						
					//next status data
					$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
					$awating_person = $next_status_data['role_id'];
						
					//if awating person equal requestor ,asign requestor for it
					if($awating_person == 'requestor'){
						$awating_person = $requestor_role;
					}
						
					//main
					$array['awaiting_table']  = $awating_person;
					$array['status'] = $next_status;
						
					//job
					$jobArray['awaiting_role_id'] = $awating_person.',';
					$jobArray['status_id'] = $next_status;
					$jobArray['status_text'] = $next_status_text;
						
						
				}
						
				  
			  }else{
				  		
												
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $pre_status,
							'workflow_id' => 2,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if(in_array($requestor_role,$role_id)){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if (in_array($workflow_order['role_id'],$role_id)|| in_array(1,$role_id) || in_array(8,$role_id)){
								$action = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(2);
						$status = json_decode($workflow['status_json'],true);
						
						//print_r($workflow_order);exit;
						if(isset($action)){
						
						//action
						switch ($action) {
							case 'approval':
							
								//clear data infront
								$array = array();
							
								//approved
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];

										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;

							case 'update':
								
								//next status
								$next_status = $workflow_order['next_status_id'];
								$next_status_text = $status[$workflow_order['next_status_id']];
								
								$array = array(
								);
								
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
								$next = 1;
								
								break;
							case 'approval_update':
								
								//clear data infront
								$array = array();
								
								if($confirm == 1){
									
									if($correct_check == 'on'){
										
										//next status
										$next_status = $workflow_order['next_status_id'];
										$next_status_text = $status[$workflow_order['next_status_id']];
										
										$target_colum = json_decode($workflow_order['target_colum'],true);
										
										//check update colum single or multiple
										$array = array();
										foreach($target_colum as $k => $v){
										
											if($v['type'] == 'single'){
												
												if($v['data_type'] == 'string'){
													
												$array[$v['name']] = $this->input->post($v['name'],true);
												
												}else if($v['data_type'] == 'date'){
													
													$date = $this->input->post($v['name'], true);
													if(!empty($date)) {
													  $tmp = explode("/", $date);
													  $date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
													  $array[$v['name']] = $date;
													}
													
												}
											
											}else{
												
											}
											
										}
										$next = 1;
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
									}
									
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
							case 'upload':
							
								//clear data infront
								$array = array();
								
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'comfirm_file'	=> $save_path,
											);	
											
											//next status
											$next_status = $workflow_order['next_status_id'];
											$next_status_text = $status[$workflow_order['next_status_id']];	
											$next = 1;
											//check last job
											if($workflow_order['next_status_text'] == 'Completed'){
												$last = 1;
											}		
											//echo $workflow_order['next_status_text'];exit;										
									}	
									
								}
								
								break;	
							case 'complete':
							
								//clear data infront
								$array = array();
							
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'comfirm_file'	=> $save_path,
											);	
										
									}	
								}
								
								break;	
								
						}
						
						//print_r($workflow_order);exit;
						//if need_det = 1
						
						if($workflow_order['need_det'] == 1){

							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$value = $this->input->post($v['target_colum'], true);
								//string or number
								if(is_numeric($v['value'])){
									$det_temp = $value." ".$v['logic']." ".$v['value'];
								}else{
									
									if($v['value'] == 'original'){
										$string = $tender_data['term_condition'];
									}else{
										$string = $v['value'];
									}
									
									$det_temp = "'".md5(trim($value))."' ".$v['logic']." '".md5(trim($string))."'";
								}
								
								
								$check = $this->parse_boolean($det_temp);
								
								//check
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
							}
							
						}

						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 2,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
						}
						
						}
						
					  //audit log
					  $log_array = array(
						'ip_address'	=> $this->input->ip_address(),
						'user_trigger'	=> $this->data['userdata']['employee_id'],//employee id
						'table_affect'	=> 'packing_list',
						'description'	=> 'Update Packing List',
						'created_date'	=> date('Y-m-d H:i:s'),
					  );
						  
					  $audit_id = $this->Audit_log_model->insert($log_array);	
					  $custom_code = $this->Audit_log_model->zerofill($audit_id);	
					  $update_array = array(
						 'log_no'	=> $custom_code,
					  );
					  $this->Audit_log_model->update($audit_id, $update_array);
						
				  
			  }
			  
			  if(isset($next_status)){
				//main
				$array['awaiting_table']  = $awating_person;
				$array['status'] = $next_status;
				//job
				$jobArray['awaiting_role_id'] = $awating_person.',';
				$jobArray['status_id'] = $next_status;
				$jobArray['status_text'] = $next_status_text;
			  }

			  
			  $array['lastupdate_user_id'] = $this->data['userdata']['id'];
			  $array['modified_date'] = $now;
			  
			  
			    if($next == 1){
				  $lastjob_update = array(
				  	'is_completed' => 1,
					'display' => 0,
				  );
				  $this->Job_model->update($jobArray['parent_id'], $lastjob_update);
				  if($last == 0){
				  $new_job_id = $this->Job_model->insert($jobArray);
				  $array['latest_job_id'] = $new_job_id;
				  }
			  	}
				
			  $this->Packing_list_model->update($id, $array);
			 
			 //mail
			 if(isset($new_job_id)){
				$related_role = $this->Job_model->getRelatedRoleID($new_job_id);
				$mail_array['related_role'] = array($related_role);		
				$mail_array['title'] = $workflow['title']." ".$pre_data['order_no']." ".$next_status_text;
			 }
			  
			    
		  }
		  
		  $item_type = 0;
		  if($mode == 'Add'){
				
				$item_type = 1;
			
		  }else if($mode == 'Edit'){
				if($pre_status < 1 && $pre_status != 0){
					$item_type = 1;
				}	
		  }
		  
		  //print_r($_POST);exit;
		  
		  if($item_type == 1){
		  //針對底下的ITEM做處理
		  
		  $item_counter = $this->input->post("item_counter", true);
		  
		  //print_r($_POST);exit;
		  
		  for($j=0;$j<$item_counter;$j++){
			  
			  if(isset($_POST['is_checked'.$j]) && $_POST['is_checked'.$j] == 'on'){
					
					//print_r($tmp);exit;
					$data = array(
						'order_id' 			=> $order_id,
						'packing_list_id' 	=> $p_id,
						'product_id' 		=> $_POST['product_id'.$j],
						'model_name' 		=> $_POST['model_name'.$j],
						'quantity_order' 	=> $_POST['quantity'.$j],
						'quantity_stock' 	=> $_POST['quantityStock'.$j],
						'quantity_pack' 	=> $_POST['quantityPack'.$j],
						'inventory_deduct'  => $_POST['inventory_deduct'.$j],
						'price' 			=> $_POST['price'.$j],
						'created_date' 		=> $now,
					);
					
					$this->Packing_list_item_model->insert($data);
					
					//減庫存
					$log_data = array(
						'product_id' => $data['product_id'],
						'qty' => '-'.$data['inventory_deduct'],
						'reason' => 'Order product packed',
						'employee_no'	=> $this->data['staff_info']['employee_no'],
						'employee_name'	=> $this->data['staff_info']['full_name'],
						'created_date' => date('Y-m-d H:i:s'),
					);
							
					$this->Product_log_model->insert($log_data);
					
					
			   }
		  }
		  
		  if($mode == 'Add'){
			//task of auto create job packing completed if job id != ''
				  $task_job_id = $this->input->post('job_id',true);
				  if($task_job_id != ''){
					  
					  //all item of this order id
					  $order_item = $this->Order_item_model->getRelatedItem($order_id);
					  
					  //get all related packing list with this order id
					  $packing_list = $this->Packing_list_model->get_where(array('is_deleted'=>0,'order_id'=>$order_id));
					  $packed_item_array = array();
					  foreach($packing_list as $k => $v){
						 $tmp_packing_item = $this->Packing_list_item_model->getRelatedItem($v['id']);
						 
						 foreach($tmp_packing_item as $k2 => $v2){
							 if(isset($packed_item_array[$v2['product_id']])){
								 $packed_item_array[$v2['product_id']] += $v2['quantity_pack'];
						     }else{
								 $packed_item_array[$v2['product_id']] = $v2['quantity_pack'];
							 }
					     }
						 
					  }
					  
					  $no_packed = 0;
					  foreach($order_item as $k => $v){
						  
						  if(isset($packed_item_array[$v['product_id']])){

							  if($packed_item_array[$v['product_id']] < $v['quantity']){
								  $no_packed = 1;
								  break;
							  }
						  
						  }else{
							  
							  $no_packed = 1;
							  break;
						  
						  }
						  
					  }
					  
					  //echo $no_packed;
					  //print_r($packing_list);echo"<br>";
					  //print_r($order_item);echo"<br>";
					  //print_r($packed_item_array);
					  
					  
					  if($no_packed == 0){
					  	 //echo $no_packed;exit;
						  $this->Job_model->update($task_job_id, array(
							'is_completed' => 1,
							'display' => 0));
						
					  }
					  
				  }
			  
		  }
		  
		  	/*
		    //sent mail
			if(isset($mail_array['related_role'])){
			
				$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
				$sitename = $this->Settings_model->get_settings(1);
				$default_email = $this->Settings_model->get_settings(6);
				
				$this->load->library('email');
				
				if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
							
					foreach($related_role_data as $k => $v){
					$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
					
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->send();	
					  
					}
				
				}
			
			}
			*/
			
		  }
		  
		  //sent mail agora
		  if(isset($workflow_order)){
				
				$this->load->library('email');
				
				//send mail to admin and customer
				if($workflow_order['action'] == 'create'){
					
					$role = array(5);
					
					$all_employee = $this->Employee_model->get_where(array('is_deleted'=>0));
					
					$related_role_data = array();
					foreach($all_employee as $k => $v){
						if(strpos($v['role_id'],',')){
							$tmp_role = explode(',',$v['role_id']);	
						}else{
							$tmp_role = array($v['role_id']);	
						}
						
						if(in_array(5,$tmp_role) && !in_array(1,$tmp_role)){
							$related_role_data[] = $v;
						}
					}
					
					//$related_role_data = $this->Employee_model->get_related_role($role);
					$sitename = $this->Settings_model->get_settings(1);
					$default_email = $this->Settings_model->get_settings(6);
					//print_r($related_role_data);exit;
					
					if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
								
						//Send Notification sales			
						foreach($related_role_data as $k => $v){
							
							$this->email->clear();
									
							$this->email->from($default_email['value'], $sitename['value']);
							$this->email->to($v['email']); 
									
							$this->email->subject('Agora '.$workflow['title'].': '.$jobArray['serial']);
							$this->email->message('Order packed '.$jobArray['serial'].'<br>'.$customer_name);
							$this->email->set_newline("\r\n");
							$this->email->send();
						
						}
						
						/*
						//customer
						$customer = $this->Customers_model->get($customer_id);
						$customer_email = $customer['primary_contact_email'];
						
						$this->email->clear();
									
						$this->email->from($default_email['value'], $sitename['value']);
						$this->email->to($customer_email); 
									
						$this->email->subject('Agora '.$workflow['title'].': '.$jobArray['serial']);
						$this->email->message('Order '.$jobArray['serial'].' processed'.'<br>'.$customer_name);
						$this->email->set_newline("\r\n");
						$this->email->send();
						*/
						
					}
					
				}
		  }
		  
		  //alert
		  if($mode == 'Add'){
			$alert_type = '/1';
		  }else{
			$alert_type = '/2';
		  }
		  
		  //echo 123;exit;
		  		  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage.$alert_type,'refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].$alert_type));
		  }		  
		  
	  }
	  
	  public function del($id){
		  
		  $data = $this->Packing_list_model->get($id);
		  //job 也要一起del
		  $this->Job_model->update($data['latest_job_id'],array('is_deleted'=>1));
		  
		  $this->Packing_list_model->delete($id);
		  
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage.'/3','refresh');  
		  } else {
			  redirect(base_url('en/agora/'.$this->data['group_name'].'/'.$this->data['model_name'].'/3'));
		  }		
	  }
	  
	  public function ajax_getSearch(){
		  
		  	$keyword = $_POST['keyword'];
			$limit = $_POST['limit'];
			$page = $_POST['page'];
			
			$start = ($page-1) * $limit;
			//我們只取已經confirm的quotation
		  	$record_count = $this->Order_model->ajax_record_count($keyword, array(
				'status'	=> 2
			));
		  	$data = $this->Order_model->ajax_search($keyword, $limit, $start, array(
				'status'	=> 2
			));
		  	$paging = $this->get_paging($limit,10,$record_count,$page);
			
			$status = $this->Order_model->status_list();
			
			//顯示Status為狀態文字, 不是數字
			if(!empty($data)) {
				foreach($data as $k=>$v) {
					$data[$k]['status'] = $status[$v['status']];
				}
			}
		  	
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					'paging'	=> $paging,
			);
			
			echo json_encode($temp);	
			exit;
	  }	  
	  
	  public function ajax_getSearchData(){
		  
		  	$id = $_POST['id'];
		  	$data = $this->Order_model->get($id);
			
			$customer = $this->Customers_model->get($data['customer_id']);
			$data['attention'] = $customer['primary_attention_to'];
			$data['address'] = $customer['address'];
			$data['postal'] = $customer['postal'];
			$data['country'] = $customer['country'];
			$data['email'] = $customer['primary_contact_email'];
			$data['contact_no'] = $customer['primary_contact_info'];
			
			//get quotation related item
			$related_item = $this->Order_item_model->getRelatedItem($id);
			$related_packing = $this->Packing_list_model->get_where(array('is_deleted'=>0,'order_id'=>$id));
			
			//sum product by id
			foreach($related_item as $k => $v){
				$tmp_s_qty = $this->Product_log_model->sum_by_id($v['product_id']);
				$related_item[$k]['stock_qty'] = $tmp_s_qty!=''?$tmp_s_qty:0;

				$packing_qty = 0;
				
				foreach($related_packing as $k2 => $v2){
							
					$packing_item = $this->Packing_list_item_model->getRelatedItem($v2['id'],$v['product_id']);
					//print_r($packing_item);exit;
					foreach($packing_item as $k3 => $v3){
						$packing_qty += $v3['quantity_pack'];
					}
							
				}
				$related_item[$k]['packed'] = $packing_qty;
				
				/*
				//get total packed
				$related_item[$k]['packed'] = $this->Packing_list_item_model->sumRelated_packing($id,$v['product_id']);
				*/
			}
			
			//get product name
			foreach($related_item as $k => $v){
				$product_data = $this->Products_model->get($v['product_id']);
				$related_item[$k]['product_name'] = $product_data['product_name'];
				$related_item[$k]['uom'] = $product_data['uom'];
				$related_item[$k]['inventory_uom'] = $product_data['inventory_uom'];
		
			}
			
			

	  		$temp = array(
				'status'	=> 'OK',
				'data'		=> $data,
				'product' => $related_item ,
			);
			
			
			echo json_encode($temp);	
			exit;
	  }	 
	  
	  public function get_paging($item_per_page,$pagenum,$total_item,$page)
	  {
	
		$start = (int)(($page-1)/$pagenum)*$pagenum+1;
		$end = $start+$pagenum-1;
		$next = $page+1;
		$pre = $page-1;
		
		$total_page = ceil( $total_item / $item_per_page );
                $paging = '';
		if($total_item > $item_per_page){
                    $paging .= '<ul class="pagination">';

                    if($page > 1){
                            $paging .= '<li><a href="javascript:changePage(1)">&laquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$pre.')">&lsaquo;</li>';
                    }

                    if($total_page <= $pagenum){

                            for($i=$start;$i<=$total_page;$i++){
                                    if($i == $page){

                                            $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                    }else{

                                            $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                    }
                            }
                    }else{
                            if($page > 5){
                                    $end = $page+5;
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    $start = $end - ($pagenum - 1);

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }else{
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }	
                    }

                    if($page < $total_page){
                            $paging .= '<li><a href="javascript:changePage('.$next.')">&rsaquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$total_page.')">&raquo;</a></li>';
                    }

                    $paging .= '</ul>';
                }
		
		return $paging;
	  }
	  	  
	  private function PDF_generation($id, $mode='I') {
		  
		    //letter head
		  	$company_name = $this->Settings_model->get_settings(15);
			$d_email = $this->Settings_model->get_settings(6);
			$reg_no = $this->Settings_model->get_settings(29);
			$d_web = $this->Settings_model->get_settings(28);
			$postal = $this->Settings_model->get_settings(25);
			$address = $this->Settings_model->get_settings(16);
			$contact_no = $this->Settings_model->get_settings(8);
		  
			$data = $this->Packing_list_model->get($id);
			
			//product list
			$product_list = $this->Products_model->getIDKeyArray('product_name');
			
			//date		
		 	$date = $data['packing_date'];
			$tmp = explode('-',$date);
			$date = $tmp[2]."/".$tmp[1]."/".$tmp[0];
			
			
			$requestor = $this->Employee_model->get($data['requestor_id']);
			$data['requestor_name'] = $requestor['full_name'];
				
			$product = $this->Packing_list_item_model->getRelatedItem_join($id);
				
		 	$customer = $this->Customers_model->get($data['customer_id']);
		  	  
		  
		  	$item_group = '';
		  	foreach($product as $k => $v){
			  	$item_group .='<tr><td>'.($k+1).'</td><td>'.$product_list[$v['product_id']].'</td><td>'.$v['model_name'].'</td><td>'.$v['quantity_order'].' '.$v['uom'].'(s)'.'</td><td>'.$v['quantity_pack'].' '.$v['uom'].'(s)'.'</td><td>'.$v['quantity_stock'].' '.$v['inventory_uom'].'(s)'.'</td><td>'.$v['inventory_deduct'].' '.$v['inventory_uom'].'(s)'.'</td></tr>';
		  	}
			
			if(strpos($data['staff_role'],',')){
				$roleId = explode(',',$data['staff_role']);	
			}else{
				$roleId = array($data['staff_role']);	
			}
			
			$role_name = '';
			foreach($roleId as $v){
				$role_name .= $this->data['role_list'][$v].',';
			}
									
			$role_name = substr($role_name,0,strlen($role_name)-1);
			
		  	$logo = $this->Settings_model->get_settings(2);
		  
		  $this->load->library("Pdf");		
          $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		  
		  // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Anexus');
            $pdf->SetTitle("Packing List");
            $pdf->SetSubject("");
            $pdf->SetKeywords("");

            // remove default header/footer
            $pdf->setPrintHeader(false);			
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(10, 10, 10);

            // set auto page breaks
            //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
            }

            // ---------------------------------------------------------

            // set font
            $pdf->SetFont('times', '', 10);

            

            // set font
            $pdf->SetFont('times', '', 10);
			
			// add a page
       		$pdf->AddPage();
			
			
			$html = '
			<table width="100%" cellpadding="5">
			
				<tr>
					<td width="60%">
						<table  width="100%">
							<tr>
								<td>
									<b>'.$company_name['value'].'</b><br>
									'.$d_email['value'].'<br>
									'.$d_web['value'].'<br>
									Company Registration No. '.$reg_no['value'].'<br>
									'.$address['value'].'<br>
									'.$postal['value'].'<br>
									Contact No. '.$contact_no['value'].'<br>
								</td>
							</tr>
						</table>
					</td>
					
					<td>
						<img height="80px" width="" src="'.$logo['value'].'">
					</td>
				</tr>
			</table>
			
			<table width="100%" cellpadding="5">
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="">
				
							<tr>
								<th colspan="3"><h1 style="color:#428bca">Packing List</h1></th>
							</tr>
							<tr>
								<td colspan="3"></td>
							</tr>
							<tr>
								<td colspan="3"></td>
							</tr>
							<tr>
								<td colspan="1" width="70%"><h4>Packing List To</h4></td>
								<td colspan="1" width="15%">Packing List No.</td>
								<td colspan="1">'.$data['packing_no'].'</td>
							</tr>
							<tr>
								<td colspan="1" width="70%">'.$customer['primary_attention_to'].'</td>
								<td colspan="1" width="15%">Date</td>
								<td colspan="1">'.$date.'</td>
							</tr>
							<tr>
								<td>'.$customer['company_name'].'</td>
							</tr>
							<tr>
								<td>'.$customer['address'].'</td>
							</tr>
							<tr>
								<td>'.$customer['country'].' '.$customer['postal'].'</td>
							</tr>
							
						</table>

					</td>

				</tr>
				
				<tr>
					<td colspan="2">
						
						<table width="100%" cellpadding="5">
				
							<tr style="background-color:#B6DDFD;color:#62A7DE">
								<td>#</td>
								<td>Product Name</td>
								<td>Country</td>
								<td>Order Balance</td>
								<td>Qty Packed</td>
								<td>Inventory Balance</td>
								<td>Inventory Deduction</td>
								
							</tr>'.$item_group.'
						</table>
						
					</td>
				</tr>
				<tr>
					<td colspan="2">
						
						<table width="100%" cellpadding="5">
				
							<tr>
								<th><h4>Remarks</h4></th>
							</tr>
							<tr>
								<td>'.$data['remark'].'</td>
							</tr>
						</table>
						
					</td>
				</tr>

			</table>';
			
			$pdf->writeHTML($html, true, false, true, false, '');
									
			
			$file_name = date("YmdHis").rand(1000,9999);
			
			if($mode == 'I') {
				$pdf->Output($file_name.'.pdf', 'I'); 	
				return $file_name.'.pdf';
			} else if ($mode == 'f') {				
				$pdf->Output('./uploads/'.$file_name.'.pdf', 'f'); 
				return './uploads/'.$file_name.'.pdf';
			}
			
		  		  
		  
	  }
	  
	  public function export_pdf($id){		  
		  $this->PDF_generation($id, 'I');			
	  }
	  
	  public function sent_mail($id,$product_total=0,$service_total=0){
		  
		   	$filename = $this->PDF_generation($id, 'f');	 		  
		  
		    $tender_data = $this->Tender_model->get($id);
		  	$customer = $this->Customers_model->get($tender_data['customer_id']);
		  
		  	$send_to = $customer['email'];
			
			$sitename = $this->Settings_model->get_settings(1);
		  	$default_email = $this->Settings_model->get_settings(6);
			
			 //$requestor_role
			$requestor_employee = $this->Employee_model->get($tender_data['requestor_id']);
			$requestor_role = $requestor_employee['role_id'];
		  
		 	$role_id = $this->data['userdata']['role_id'];
			
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $tender_data['status'],
							'workflow_id' => 3,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(3);
						$status = json_decode($workflow['status_json'],true);
						
						if(isset($action)){

		
						if($action == 'send_email' || $action == 'sendmail_update'){
							
							//next status
							$next_status = $workflow_order['next_status_id'];
							$next_status_text = $status[$workflow_order['next_status_id']];
							$who_email = $workflow_order['who_email'];
	
						}

						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];

								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 1,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
							
							$jobArray = array(
								'parent_id' 		=>$tender_data['latest_job_id'],
								'user_id' 			=>$this->data['userdata']['id'],
								'customer_id' 		=>$tender_data['customer_id'],
								'type' 				=> 'Packing List',
								'type_path' 		=>'ground_staff/packing_list/',
								'awaiting_role_id' 	=>$awating_person.',',
								'status_id' 		=>$next_status,
								'status_text'		=>$next_status_text,
								'serial' 			=>$tender_data['packing_no'],
								'type_id' 			=>$tender_data['id'],
								'created_date'		=>date("Y-m-d H:i:s"),
								'modified_date'		=>date("Y-m-d H:i:s"),
							);			
					  
						    $array = array(
							'awaiting_table' => $awating_person.',',
							'status' => $next_status,
							'modified_date' => date('Y-m-d H:i:s'),
							'lastupdate_user_id' => $this->data['userdata']['id'],
						    );
							
							//print_r($jobArray);exit;

						
						  //更新上一個JOB的狀態					  
						  $this->Job_model->update($jobArray['parent_id'], array(
							'is_completed' => 1,
							'display' => 0,
						  ));
						  
						  //新增一個新的JOB
						  $new_job_id = $this->Job_model->insert($jobArray);
						  $array['latest_job_id'] = $new_job_id;
						  
						  $this->Tender_model->update($tender_data['id'], $array);
							
							
						}
						
						}
			
			
			if(isset($who_email)){
			
				$this->load->library('email');
				
				switch($who_email){
					case 'customer':
							
							//Send Notification
							if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
										
								$this->email->from($default_email['value'], $sitename['value']);
								$this->email->to($send_to); 
							
								$this->email->subject('Anexus '.$workflow['title'].': '.$tender_data['qr_serial']);
								$this->email->message($tender_data['qr_serial']);	
								$this->email->attach($filename);
								$this->email->set_newline("\r\n");
								$this->email->send();
							
							}
							
					break;	
					case 'supplier':
						
							  //count supplier
							  $supplier_count = $this->Purchase_request_item_model->get_supplier_groupBy($id);
							  
							  foreach($supplier_count as $k => $v){
									
								  $supplier = $this->Suppliers_model->get($v['supplier_id']);
								  
								  $file_name = $this->PDF_generation($id, 'f',$v['supplier_id']);	
										 
								  if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local") {
									
									$this->load->library('email');
									$this->email->clear();	
									$this->email->from($default_email['value'], $sitename['value']);
									$this->email->to($supplier ['email']); 
									
									$this->email->subject('Anexus Purchase request '.$tender_data['qr_serial']);
									$this->email->message($tender_data['qr_serial']);	
									$this->email->attach($file_name);
									$this->email->set_newline("\r\n");
									$this->email->send();
									
								  }
							  
							  }
							
					break;
					case 'employee':
					break;
				
				}
				
			}
			
			 //mail
			 $mail_array = array(
				'creater_name' =>$this->data['userdata']['name'],
				'title' =>'Purchase '.$tender_data['qr_serial'].' has been send out',
			 );
			 
			 if(isset($new_job_id)){
			  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
			  $mail_array['related_role'] = $related_role;		
			  $mail_array['title'] = $workflow['title']." ".$tender_data['qr_serial']." ".$next_status_text;
			 }
			 echo $_SERVER['HTTP_HOST'];exit;
			 //sent Notification to related role						  			
		  	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="agora.local" && isset($mail_array['related_role'])) {
				$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
			  				  	
			  	foreach($related_role_data as $k => $v){
				  
				  	$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
					
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->set_newline("\r\n");
					$this->email->send();	
				  
		 		}
			}
			
			
			$lastpage = $this->session->userdata("lastpage");
			if(!empty($lastpage)) {
			  redirect($lastpage,'refresh');  
			} else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
			}		 			
		  
	  }
	  
	  public function unpack_list(){
		  
		  	//get all order cutomer
		  	$o_customer = $this->Order_model->get_order_customer();
		  
		  
		  	$cus_array = array();
		  	foreach($o_customer as $k => $v){
				
				//customer
				$cus_array[$v['customer_id']] = array(
					'name' => $v['customer_name'],
				);
				
				$related_order = $this->Order_model->get_where(array(
					'is_deleted' => 0,
					'customer_id'=> $v['customer_id'],
					'status'	=> 2,//confirmed by sales
				));
				
				//get realted item
				$item_array = array();
				$item_uom_array = array();
				$item_stock_qty = array();
				$pack_item_array = array();
				foreach($related_order as $k2 => $v2){
				
					//pack item
					$related_packing = $this->Packing_list_model->get_where(array('is_deleted'=>0,'status'=>1,'order_id'=>$v2['id']));
					//echo $this->db->last_query();exit;
					//print_r($related_packing);exit;
					
					if(!empty($related_packing)){
						
						foreach($related_packing as $k3 => $v3){
							
							$packing_item = $this->Packing_list_item_model->getRelatedItem($v3['id']);
							
							foreach($packing_item as $k4 => $v4){
								if(isset($pack_item_array[$v4['product_id']])){
		
									$pack_item_array[$v4['product_id']] = $pack_item_array[$v4['product_id']]+$v4['quantity_pack'];
									
								}else{
								
									$pack_item_array[$v4['product_id']] = $v4['quantity_pack'];
										
								}
							}
							
						}
					}
					$cus_array[$v['customer_id']]['item_pack'] = $pack_item_array;
					
					//order item
					$related_item = $this->Order_item_model->getRelatedItem2($v2['id']);
					
					foreach($related_item as $k3 => $v3){
						//print_r($item_array);exit;
						if(isset($item_array[$v3['product_id']])){

							$item_array[$v3['product_id']] = $item_array[$v3['product_id']]+$v3['quantity'];
							
						}else{
						
							$item_array[$v3['product_id']] = $v3['quantity'];
								
						}
						
						$item_uom_array[$v3['product_id']] = $v3['uom'];
						$item_uom_inventory_array[$v3['product_id']] = $v3['inventory_uom'];
						$item_stock_qty[$v3['product_id']] = $this->Product_log_model->sum_by_id($v3['product_id']);
					}
					
					$cus_array[$v['customer_id']]['item'] = $item_array;
					$cus_array[$v['customer_id']]['item_uom'] = $item_uom_array;
					$cus_array[$v['customer_id']]['item_inventory_uom'] = $item_uom_inventory_array;
					$cus_array[$v['customer_id']]['stock_qty'] = $item_stock_qty;
				
				}
				
				
			}
			
			
			//print_r($cus_array);exit;
			//查看物品pack完了没
			$check_array = array();
			
			foreach($cus_array as $k =>$v){
				
				//针对每个item处理
				$check_id = array();
				if(!empty($v['item'])){
					
				foreach($v['item'] as $k2 => $v2){
					
					//针对每个pack的item
					foreach($v['item_pack'] as $k3 => $v3){
						if($k2 == $k3){
							
							//echo $v['name']."<br>";
							
							if(($v2-$v3)<=0){
								$check_id[] = $k2;
							}else{
								$cus_array[$k]['item'][$k2] = $v2-$v3;
							}
						
						}
					}
					
				}
				
				}
				
				//把相减<=0的记录起来
				if(!empty($check_id)){
					
					$check_array[$k] = $check_id;
					
				}
				
				
			}
			
			//print_r($check_array);exit;
			
			//把已经pack完的物品删除掉
			foreach($check_array as $k => $v){
				
				foreach($v as $k2=>$v2){
					
					unset($cus_array[$k]['item'][$v2]);
					
				}
				
			}
			
			//print_r($cus_array);exit;
			
			$this->data['product_list'] = $this->Products_model->getIDKeyArray('product_name');
			$this->data['unpack_data'] = $cus_array;
			
			//print_r($check_array);exit;
			//print_r($cus_array);exit;
		  
		  	$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/unpack', $this->data);
            $this->load->view('anexus/footer', $this->data);
		  
	  }

	  public function unpack_pdf(){
		  
		  	//get all order cutomer
		  	$o_customer = $this->Order_model->get_order_customer();
		  
		  	$cus_array = array();
		  	foreach($o_customer as $k => $v){
				
				//customer
				$cus_array[$v['customer_id']] = array(
					'name' => $v['customer_name'],
				);
				
				$related_order = $this->Order_model->get_where(array(
					'is_deleted' => 0,
					'customer_id'=> $v['customer_id'],
				));
				
				//get realted item
				$item_array = array();
				$item_uom_array = array();
				$item_stock_qty = array();
				$pack_item_array = array();
				foreach($related_order as $k2 => $v2){
				
					//pack item
					$related_packing = $this->Packing_list_model->get_where(array('is_deleted'=>0,'status'=>1,'order_id'=>$v2['id']));
					//echo $this->db->last_query();exit;
					//print_r($related_packing);exit;
					
					if(!empty($related_packing)){
						
						foreach($related_packing as $k3 => $v3){
							
							$packing_item = $this->Packing_list_item_model->getRelatedItem($v3['id']);
							
							foreach($packing_item as $k4 => $v4){
								if(isset($pack_item_array[$v4['product_id']])){
		
									$pack_item_array[$v4['product_id']] = $pack_item_array[$v4['product_id']]+$v4['quantity_pack'];
									
								}else{
								
									$pack_item_array[$v4['product_id']] = $v4['quantity_pack'];
										
								}
							}
							
						}
					}
					$cus_array[$v['customer_id']]['item_pack'] = $pack_item_array;
					
					//order item
					$related_item = $this->Order_item_model->getRelatedItem2($v2['id']);
					
					foreach($related_item as $k3 => $v3){
						//print_r($item_array);exit;
						if(isset($item_array[$v3['product_id']])){

							$item_array[$v3['product_id']] = $item_array[$v3['product_id']]+$v3['quantity'];
							
						}else{
						
							$item_array[$v3['product_id']] = $v3['quantity'];
								
						}
					$item_uom_array[$v3['product_id']] = $v3['uom'];
					$item_uom_inventory_array[$v3['product_id']] = $v3['inventory_uom'];
					$item_stock_qty[$v3['product_id']] = $this->Product_log_model->sum_by_id($v3['product_id']);
					}
					
					$cus_array[$v['customer_id']]['item'] = $item_array;
					$cus_array[$v['customer_id']]['item_uom'] = $item_uom_array;
					$cus_array[$v['customer_id']]['item_inventory_uom'] = $item_uom_inventory_array;
					$cus_array[$v['customer_id']]['stock_qty'] = $item_stock_qty;
				
				}
				
				
			}
			
			//查看物品pack完了没
			$check_array = array();
			
			foreach($cus_array as $k =>$v){
				
				//针对每个item处理
				$check_id = array();
				if(!empty($v['item'])){
					foreach($v['item'] as $k2 => $v2){
						
						//针对每个pack的item
						foreach($v['item_pack'] as $k3 => $v3){
							if($k2 == $k3){
								
								//echo $v['name']."<br>";
								
								if(($v2-$v3)<=0){
									$check_id[] = $k2;
								}else{
									$cus_array[$k]['item'][$k2] = $v2-$v3;
								}
							
							}
						}
						
					}
				}
				
				//把相减<=0的记录起来
				if(!empty($check_id)){
					
					$check_array[$k] = $check_id;
					
				}
				
				
			}
			
			//把已经pack完的物品删除掉
			foreach($check_array as $k => $v){
				
				foreach($v as $k2=>$v2){
					
					unset($cus_array[$k]['item'][$v2]);
					
				}
				
			}
			
			$product_list = $this->Products_model->getIDKeyArray('product_name');
			$unpack_data = $cus_array;
				
			$data = '';
			//print_r($unpack_data);exit;
			if(!empty($unpack_data)) {
			
				foreach($unpack_data as $k => $v){
					if(!empty($v['item'])){
						
						//标题
						$data .= '<tr><th colspan="3">'.$v['name'].'</th></tr><tr><th>Product Name</th><th>Qty</th><th>Qty-in-stock</th></tr>';
						
						//item
						foreach($v['item'] as $k2 => $v2){
						
							$data .= '<tr><td>'.$product_list[$k2].'</td><td>'.$v2.' '.$v['item_uom'][$k2].'(s)'.'</td><td>'.$v['stock_qty'][$k2].' '.$v['item_inventory_uom'][$k2].'(s)'.'</td></tr>';
							
						}
						
						//间隔
						$data .= '<tr><th colspan="3"></th></tr>';
						
					}
				}
				
			}
				
						
		  	$logo = $this->Settings_model->get_settings(2);
		  
		  $this->load->library("Pdf");		
          $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		  
		  // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Anexus');
            $pdf->SetTitle("Packing List");
            $pdf->SetSubject("");
            $pdf->SetKeywords("");

            // remove default header/footer
            $pdf->setPrintHeader(false);			
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(10, 10, 10);

            // set auto page breaks
            //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
            }

            // ---------------------------------------------------------

            // set font
            $pdf->SetFont('times', '', 10);

            

            // set font
            $pdf->SetFont('times', '', 10);
			
			// add a page
       		$pdf->AddPage();
			
			
			$html = '
			<table width="100%" cellpadding="5">
			
				<tr>
					<td width="60%">
						<table  width="100%">
							<tr>
								<td>
									<b>PAC-FUNG AGORA PTE LTD</b><br>
									sales@agora-foods.com<br>
									www.agora-foods.com<br>
									Company Registration No. 201510316C<br>
								</td>
							</tr>
						</table>
					</td>
					
					<td>
						<img height="80px" width="" src="'.$logo['value'].'">
					</td>
				</tr>
			</table>
			
			<table width="100%" cellpadding="5" border="1">
				'.$data.'
			</table>';
			
			$pdf->writeHTML($html, true, false, true, false, '');
									
			
			$file_name = date("YmdHis").rand(1000,9999);
			
			$pdf->Output($file_name.'.pdf', 'I'); 	
			return $file_name.'.pdf';
			
			
	  }

}

?>