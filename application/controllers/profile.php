<?php

class Profile extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Employee_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
            			            
            $this->data['init'] = $this->function_model->page_init();

            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
            if ($this->data['islogin']) {

                  $userdata = $this->session->userdata("userdata");
				
				//print_r($userdata);exit;
				if(strpos($userdata['role_id'],',')){
					$userdata['role_id'] = explode(',',$userdata['role_id']);	
				}else{
					$userdata['role_id'] = array($userdata['role_id']);	
				}
				
				
				$this->data['userdata'] = $userdata;                 
            }else{
				redirect(base_url('en/login'),'refresh'); 
			}  
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']); 
			
			/*
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			} 
			*/
			
           
      }
   
      public function index($status="") {  
          			
            $this->data['title'] = ucfirst("Profile"); // Capitalize the first letter		
			$this->data['mode'] = 'Edit';
			$result = $this->Employee_model->get_where(array('user_id'=>$this->data['userdata']['id']));
			$this->data['result'] = $result[0];
			$this->data['status'] = $status;
			
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/profile', $this->data);
            $this->load->view('anexus/footer', $this->data);
      }
	  
	  public function submit(){
		   
		  $id = $this->input->post("id", true);		  		  
		  $email = $this->input->post("email", true);		  
		  $password = $this->input->post("password", true);
		  $copassword = $this->input->post("copassword", true);		  
		  
		  $bankname = $this->input->post("bankname", true);
		  $bank_code = $this->input->post("bank_code", true);
		  $bank_branch = $this->input->post("bank_branch", true);
		  $account_no = $this->input->post("account_no", true);
		  
		  $full_name = $this->input->post("full_name", true);
		  $ic_no = $this->input->post("ic_no", true);
		  $date_of_birth = $this->input->post("date_of_birth", true);
		  $dob = "";
		  if(!empty($date_of_birth)) {
			  $tmp = explode("/", $date_of_birth);
			  $dob = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
		  }
		  
		  
		  $home_address = $this->input->post("home_address", true);
		  $contact_info = $this->input->post("contact_info", true);
		  $passport_no = $this->input->post("passport_no", true);
		  
		  $expired_date = $this->input->post("expired_date", true);
		  
		  $exp = "";
		  if(!empty($expired_date)) {
			  $tmp = explode("/", $expired_date);
			  $exp = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
		  }
		  
		  $emergency_contact_name = $this->input->post("emergency_contact_name", true);
		  $emergency_contact_relationship = $this->input->post("emergency_contact_relationship", true);
		  $emergency_contact_no = $this->input->post("emergency_contact_no", true);
		  
		  $iu_array = array(		  				
			//'email'							=> $email,
			//'bankname'						=> $bankname,
			//'bank_code'						=> $bank_code,
			//'bank_branch'					=> $bank_branch,
			//'account_no'					=> $account_no,
			//'full_name'						=> $full_name,
			//'ic_no'							=> $ic_no,
			//'date_of_birth'					=> $dob,
			//'home_address'					=> $home_address,
			//'contact_info'					=> $contact_info,
			//'passport_no'					=> $passport_no,			
			//'expired_date'					=> $exp,
			//'emergency_contact_name'		=> $emergency_contact_name,
			//'emergency_contact_relationship'=> $emergency_contact_relationship,
			//'emergency_contact_no'			=> $emergency_contact_no,
		  );
		  
		  
		  $iu_array['modified_date'] = date("Y-m-d H:i:s");			  
		  $this->Employee_model->update($id, $iu_array);
		  	  
	      $usr_array = array(				
				'email'			=> $email,
			//	'name'			=> $full_name,				
			//	'employee_id'	=> $id,
				'modified_date'	=> date("Y-m-d H:i:s"),				
		  );
			  
		  if(!empty($password)) {
		  		$usr_array['passwd'] = md5($password);					  
		  }
			  
		  $employee_data = $this->Employee_model->get($id);
		  $this->User_model->update($employee_data['user_id'], $usr_array);
		  
		  redirect(base_url($this->data['init']['langu'].'/agora/profile/success'),'refresh');
		  
	  }

}

?>