<?php

class Credit_note_manage extends CI_Controller {

      public $data = array();

      public function __construct() {
            parent::__construct();  
			$this->load->model('User_model');
            $this->load->model('function_model');
			$this->load->model('Job_model');
			$this->load->model('Customers_model');
			$this->load->model('Credit_note_model');
			$this->load->model('Credit_note_item_model');
			$this->load->model('Quotation_model');
			$this->load->model('Quotation_item_model');
			$this->load->model('Delivery_order_model');
			$this->load->model('Delivery_order_item_model');
			$this->load->model('Settings_model');
			$this->load->model('Purchase_request_model');
			$this->load->model('Products_model');
			$this->load->model('Permission_model');
			$this->load->model('Role_model');
			$this->load->model('Employee_model');
			$this->load->model('Workflow_model');
			$this->load->model('Workflow_order_model');

			$this->data['role_list'] = $this->Role_model->getIDKeyArray('name');
			
			//get status list
			$cn_status_list = $this->Workflow_model->get(11);
			$this->data['cn_status_list'] = json_decode($cn_status_list['status_json']);

            $this->data['init'] = $this->function_model->page_init();
			$this->data['status_list'] = $this->Credit_note_model->status_list();
			$this->data['item_per_page'] = $this->function_model->item_per_page();
            //This section is all about user logged in information
            //we do it in constructor, so every method will call this once
            //and use in every pages
            $this->data['webpage'] = $this->function_model->get_web_setting();
            $this->data['islogin'] = $this->function_model->isLogin();
			
			//已登入就會有userdata的變數
			if($this->data['islogin']){
				$this->data['userdata'] = $this->session->userdata("userdata");
			}else{
				redirect(base_url('en/login'),'refresh'); 
			}     
			
			$this->data['staff_info'] = $this->Employee_model->getByUser($this->data['userdata']['id']);      
			
			$this->data['group_name'] = "finance";  
			$this->data['model_name'] = "credit_note";  
			$this->data['common_name'] = "Credit Note";   
			
			$this->data['task_display_count'] = $this->Job_model->record_count(array(
				//'user_id'		=>	$this->data['userdata']['id'],
				'display'		=> 1,
				'is_deleted'	=> 0,
				'is_completed'	=> 0,
			),$this->data['userdata']['role_id']);
			
			//permission
			$role = $this->Role_model->get($this->data['userdata']['role_id']);
			$permission = $this->Permission_model->get($role['permission_id']);
			
			foreach($permission as $k => $v){
				if(strpos($v,',') == true){
					$type = explode(',',$v);
				}else{
					$type = array(
						'0' => $v,
					);	
				}
				foreach($type as $x){
					if($x == 'R' || $x == 'OR'){
						$this->data[$k] = 1;
					}
				}
			}
			
			define('SUPERADMIN', 1);
			define('GENERAL_MANAGER', 2);
		    define('FINANCE', 5);
           
      }
   
      public function index($q="ALL",$page=1) {  
          			
            $this->data['title'] = ucfirst($this->data['model_name']);
			
			//Filter						
			$filter = array(
			 	'is_deleted'	=> 0,			 
			);
			
			//Grouping URL			
			$url = base_url().$this->data['init']['langu'].'/anexus/'.$this->data['group_name'].'/'.$this->data['model_name'].'/'.$q.'/';
			if($q == 'ALL') {
				$q = "";
			}
			$this->data['q'] = $q;
				
			$limit_start = ($page-1)*$this->data['item_per_page'];
			
			//count total Data
			$this->data["total"] = $this->Credit_note_model->record_count($filter, $q);
			
			//get particular ranged list
			$this->data['results'] = $this->Credit_note_model->fetch($filter, $q, $this->data['item_per_page'], $limit_start);
					
			//awaiting
			if(!empty($this->data['results'])){
				foreach($this->data['results'] as $k => $v){
					if(!empty($v['awaiting_table'])){
							$this->data['results'][$k]['awaiting_table']= explode(',',$v['awaiting_table']);
					}
				}
			}
						
			//pagination
			$this->data['paging'] = $this->function_model->get_paging($this->data['item_per_page'],10,$this->data['total'],$page,$url);
			
			$this->data['customer_list'] = $this->Customers_model->getIDKeyArray("company_name");
			$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
			
			$this->session->set_userdata("lastpage", $url.$page);
			
            $this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/index', $this->data);
            $this->load->view('anexus/footer', $this->data);
			
      }	  
	  
	  public function add($id=false) {  
	  
          	$role_id = $this->data['userdata']['role_id'];
			
			if($id !== false) {
				$this->data['mode'] = 'Edit';
				$this->data['result'] = $this->Credit_note_model->get($id);									
				$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
				$this->data['invoice_item'] = json_encode($this->Credit_note_item_model->get_related($id));	
				
				$this->data['result']['awaiting_table'] = explode(',',$this->data['result']['awaiting_table']);
				
				$requestor_employee = $this->Employee_model->get($this->data['result']['requestor_id']);
			    $requestor_role = $requestor_employee['role_id'];
				
				$default = 0;
				
				/*
				if($this->data['result']['status'] == 1){
					
					if($role_id == FINANCE){
					$this->data['head_title'] = 'Edit Credit Note';
					}else{
					$default = 1;
					}
					
				}else if($this->data['result']['status'] == 2){
					
					if($role_id == GENERAL_MANAGER){
					$this->data['head_title'] = 'Confirm Credit Note';
					}else{
					$default = 1;
					}
	
				}else{
					$default = 1;
				}
				
				//no related person
				if($default == 1){
					$this->data['head_title'] = 'View Credit Note';	
				}
				*/
				
				//////////////////////////////////////////////////////////////////////////////////
				
				   		//get related workflow order data
						$workflow_flitter = array(
							'status_id' => $this->data['result']['status'],
							'workflow_id' => 11,
						);
						
						$type='';
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$type = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$type = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
							$type = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
									$type = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(11);
						$status = json_decode($workflow['status_json'],true);
						
						$this->data['workflow_title'] = $workflow['title'];
					
					$this->data['btn_type'] = $type;
					//print_r($workflow_order);exit;

					switch($type){
						case 'edit':
							$this->data['head_title'] = 'Edit '.$workflow['title'].' Request';
						break;
						case 'approval':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
						break;
						case 'approval_update':
							$this->data['head_title'] = 'Confirm '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'update':
							$this->data['head_title'] = 'Update '.$workflow['title'].' Request';
							$this->data['target_colum'] = $workflow_order['target_colum'];
						break;
						case 'send_email':
							$this->data['head_title'] = 'Waiting to Send '.$workflow['title'].' Request';
						break;
						case 'sendmail_update':
							$this->data['head_title'] = 'Waiting to Send/Edit '.$workflow['title'].' Request';
						break;
						case 'upload':
							$this->data['head_title'] = 'Upload Confirm File '.$workflow['title'].' Request';
						break;
						case 'complete':
							$this->data['head_title'] = $workflow['title'].' Request';
							$this->data['last_action'] = $workflow_order['action_before_complete'];
						break;
						default:
							$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					foreach(json_decode($workflow['status_json'],true) as $k => $v){
						
						if($v == 'Completed'){
							$this->data['completed_status_id'] = $k;
							break;
						}
					}
				//////////////////////////////////////////////////////////////////////////////////////
					
			} else {
				$this->data['mode'] = 'Add';	
				$this->data['head_title'] = 'Create Credit Note';
				
				//get related workflow order data
					$workflow_flitter = array(
						'role_id' => $role_id,
						'status_id' => -1,
						'workflow_id' => 11,
					);
					
					//data form database
					$workflow = $this->Workflow_model->get(11);
					$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
					
					//get view bottom layout
					$type = '';
					if(!empty($workflow_order)){
						$type = $workflow_order['action'];
						if($workflow_order['action'] == 'create'){
							$this->data['head_title'] = 'Create '.$workflow['title'].' Request';
						}else{
							$this->data['head_title'] = $workflow['title'].' Request';
						}
					}else{
						$this->data['head_title'] = $workflow['title'].' Request';
					}
					
					$this->data['btn_type'] = $type;
				
			}
			
			$this->data['gst'] = $this->Settings_model->get_settings(5);
			
			
			$this->load->view('anexus/header', $this->data);
            $this->load->view('anexus/'.$this->data['model_name'].'/add', $this->data);
            $this->load->view('anexus/footer', $this->data);		
            
			
      }	  	  
	  
	  public function submit(){
		  $now = date("Y-m-d H:i:s");
		  $mode = $this->input->post("mode", true);
		  $id = $this->input->post("id", true);
		  
		  $role_id = $this->data['userdata']['role_id'];
		  
		  //get value approve or not approve
		  $confirm = $this->input->post("confirm", true);
		  $correct_check = $this->input->post("correct_check", true);
		  
		  		
		  $issue_date = $this->input->post("issue_date", true);
		  if(!empty($issue_date)) {
			  $tmp = explode("/", $issue_date);
			  $issue_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
		  }
		  
		  $payment_date = $this->input->post("payment_date", true);
		  if(!empty($payment_date)) {
			  $tmp = explode("/", $payment_date);
			  $payment_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
		  }		
		  
		  if($mode == 'Add'){
			  
			  $customer_id = $this->input->post("customer_id", true);
		  	  
		  }else{
		  
			  //cn
			  $cn_data = $this->Credit_note_model->get($id);
		  	  $customer_id = $cn_data['customer_id'];
			  
		  }	
		  
		  //job
		  	$jobArray = array(
				'parent_id' =>0,
				'user_id' =>$this->data['userdata']['id'],
				'customer_id' =>$customer_id,
				'type' => 'Credit Note',
				'type_path' =>'finance/credit_note_edit/',
		 	 );
		  
		  //main
 		  $array = array(		
		  	  	'issue_date' =>$issue_date,
				'customer_id' =>$this->input->post("customer_id", true),
				'purchase_order_ref' =>$this->input->post("CP_Order_Ref", true),
				'purchase_order_no' =>$this->input->post("PurchaseOrderNo", true),
				'job_id' =>0,
				'sales_person_id' =>$this->input->post("sale_person_id", true),
				'quotation_id' =>$this->input->post("quotation_id", true),
				'delivery_order_invoice_relationship_id' =>0,
				'payment_terms' =>$this->input->post("TermsofPayment", true),
				'payment_due_day' =>$payment_date,
				'subtotal' =>$this->input->post("sub_total", true),
				'gst' =>$this->input->post("gst", true),
				'grand_total' =>$this->input->post("grand_total", true),
				'remarks' =>$this->input->post("remarks", true),
				'sales_order_ref' =>$this->input->post("S_Order_Ref", true),
				'sales_order_no' =>$this->input->post("S_Order_No", true),
				'delivery_order_no' =>$this->input->post("DeliveryOrderNo", true),
				'currency' =>$this->input->post("Currency", true),
				'term_of_payment' =>$this->input->post("TermsofPayment", true),
				'invoice_to' =>$this->input->post("invoiceTo", true),
				'account_no' =>$this->input->post("accountNo", true),
				'delivered_to' =>$this->input->post("DeliveredTo", true),
				'primary_attention_to' =>$this->input->post("AttentionTo", true),
				'primary_contact' =>$this->input->post("Contact", true),
				'secoundary_attention_to' =>$this->input->post("AttentionTo2", true),
				'secoundary_contact' =>$this->input->post("Contact2", true),
				'requestor_id' => $this->input->post("requestor_id", true),
		  );
		  
		  //mail
			$mail_array = array(
		  	'creater_name' =>$this->data['userdata']['name'],
		  	);
		  
		  //print_r($_POST);exit;
		  
		  //Add
		  if($mode == 'Add') {			  			  
			  /*
			  //main
			  $array['awaiting_table']  = GENERAL_MANAGER;
			  $array['status']  = 1;
			  $array['create_user_id'] = $this->data['userdata']['id'];
			  $array['lastupdate_user_id'] = $this->data['userdata']['id'];
			  $array['created_date'] = $now;
			  $array['modified_date'] = $now;
			  			  
			  $insert_id = $this->Credit_note_model->insert($array);	
			  $invoice_no = $this->Credit_note_model->zerofill($insert_id, 5);
			  
			  //insert job
			  $jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
			  $jobArray['status_id'] = 1;
			  $jobArray['status_text'] = $this->data['cn_status_list'][1];
			  $jobArray['serial'] =$invoice_no;
			  $jobArray['type_id'] =$insert_id;
			  $jobArray['created_date'] = $now;
			  $jobArray['modified_date'] = $now;
			  
			  $job_id = $this->Job_model->insert($jobArray);
			  
			  
			  //after insert job,update job id,qr serial....
			  $array =array(
			  		'job_id'	=> $job_id,
			  		'invoice_no'  => $invoice_no,
					'latest_job_id'	=> $job_id,
			  );

			  $this->Credit_note_model->update($insert_id, $array);	 
			  */
			  
			   ////////////////////////////////////////////////////////////////////////////////////
				
				//get related workflow order data
				$workflow_flitter = array(
					'role_id' => $role_id,
					'status_id' => -1,
					'workflow_id' => 11,
				);
				
				//data form database
				$workflow = $this->Workflow_model->get(11);
				$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
				$status = json_decode($workflow['status_json'],true);
				
				if(!empty($workflow_order)){
				
				//ad_hoc
				if($workflow_order['ad_hoc'] == 1){
					$array['ad_hoc'] = 1;
				}
				
				//purchaser
				$is_adhoc = false;
				if($role_id == 10){
					$is_adhoc = true;
				}
				
				//next status
				$next_status = $workflow_order['next_status_id'];
				$next_status_text = $status[$workflow_order['next_status_id']];
				
			  	//if need_det = 1
				if($workflow_order['need_det'] == 1){
					
					$formula = json_decode($workflow_order['formula'],true);
					
					//check if product or service price over value
					foreach($formula as $k => $v){
					
						$price = $this->input->post($v['target_colum'], true);
						$det_temp = $price." ".$v['logic']." ".$v['value'];
						$check = $this->parse_boolean($det_temp);
						
						//check price over value
						if($check){
							$next_status = $v['next_status_id'];
							$next_status_text = $status[$v['next_status_id']];
							break;
						}
						
					}
					
				}
				
				//get next status workflow order data
				$next_workflow_flitter = array(
					'status_id' => $next_status,
					'workflow_id' => 10,
				);
				
				//next status data
				$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
				$awating_person = $next_status_data['role_id'];
				
				if($awating_person == 'requestor'){
					$awating_person = $this->input->post("requestor_id", true);
				}
				//echo $awating_person;exit;
				
				//main
				$array['awaiting_table']  = $awating_person;
				$array['status'] = $next_status;
				//main
				$jobArray['awaiting_role_id'] = $awating_person.',';
				$jobArray['status_id'] = $next_status;
				$jobArray['status_text'] = $next_status_text; 	
				  
				  //insert main data
					$array['created_date'] = $now;
					$array['modified_date'] = $now;
					$array['create_user_id'] = $this->data['userdata']['id'];
					$array['lastupdate_user_id'] = $this->data['userdata']['id'];

					//新增資料
					$insert_id = $this->Credit_note_model->insert($array);					
					//echo $this->db->last_query();exit;
					//更新 serial
					$invoice_no = $this->Credit_note_model->zerofill($insert_id, 5);
					
					//insert job
					$jobArray['serial'] = $invoice_no;
					$jobArray['type_id'] = $insert_id;
					
					$jobArray['created_date'] = $now;
					$jobArray['modified_date'] = $now;
					
					$job_id = $this->Job_model->insert($jobArray);
				  	
					//after insert job,update job id,qr serial....
					$array =array(
						'job_id'	=> $job_id,
						'invoice_no'  => $invoice_no,
						'latest_job_id'	=> $job_id,
					);
					$this->Credit_note_model->update($insert_id, $array);
					
				  //mail
				  $related_role = $this->Job_model->getRelatedRoleID($job_id);
				  $mail_array['related_role'] = $related_role;		
				  $mail_array['title'] = $workflow['title']." ".$invoice_no." ".$next_status_text; 
				  	
				}
				
				//////////////////////////////////////////////////////////////////////////////////// 	  
			  
		  //Edit	  			  
		  } else {
			  
			    $cn_data = $this->Credit_note_model->get($id);
			  	$cn_status = $cn_data['status'];
			  
			  	$jobArray['serial'] =$cn_data['invoice_no'];
			  	$jobArray['type_id'] =$cn_data['id'];
			  	$jobArray['parent_id'] = $cn_data['latest_job_id'];
				$jobArray['created_date'] = $now;
				$jobArray['modified_date'] = $now;
				
				$requestor_employee = $this->Employee_model->get($cn_data['requestor_id']);
			 	$requestor_role = $requestor_employee['role_id'];
				
				$next = 0;
			  	$last = 0;
			  	$status_change = 0;
			  
			  	/*
			   
			    //判断status 给予下个job
				if($cn_status == 1){
					
					//finance edit	
					if($role_id == FINANCE){
						
						$array['awaiting_table']  = GENERAL_MANAGER;
						$array['status']  = 1;
						//job
						$jobArray['awaiting_role_id'] = GENERAL_MANAGER.',';
						$jobArray['status_id'] = 1;
						$jobArray['status_text'] = $this->data['cn_status_list'][1];
						//mail
						$mail_array['related_role'] = array(FINANCE,GENERAL_MANAGER);
						$mail_array['title'] = $cn_data['invoice_no'].' '.$this->data['cn_status_list'][1];
						
					//general manager approved/denied
					}else if($role_id == GENERAL_MANAGER){
						
						//general manager denied
						if($confirm == 0){
							$array['awaiting_table']  = '';
						  	$array['status']  = 0;
						  
						  	//mail
							$mail_array['related_role'] = array(FINANCE,GENERAL_MANAGER);
							$mail_array['title'] = $cn_data['invoice_no'].' '.$this->data['cn_status_list'][0];
						  
						  	$next = 1;
							$last = 1;

						//general maneger approved	
						} else if($correct_check == 'on'){
							
							$array['awaiting_table']  = FINANCE;
							$array['status']  = 2;
							$jobArray['awaiting_role_id'] = FINANCE.',';
							$jobArray['status_id'] = 2;
							$jobArray['status_text'] = $this->data['cn_status_list'][2];
							
							//mail
							$mail_array['related_role'] = array(GENERAL_MANAGER,FINANCE);
							$mail_array['title'] = $cn_data['invoice_no'].' '.$this->data['cn_status_list'][2];							
							$next = 1;	
													
						}
						
					}
				}
				
				
			  */
			  
			  /////////////////////////////////////////////////////////////////////////////////
			  
			  //can edit status
			  $edit_status = $this->Workflow_order_model->get_status_edit(11);
			  
			  //requestor edit
			  if(in_array($cn_data['status'], $edit_status) && $role_id == $requestor_role){
			  	  
						//get related workflow order data
						$workflow_flitter = array(
							'role_id' => $role_id,
							'status_id' => -1,
							'workflow_id' => 10,
						);
						
						//data form database
						$workflow = $this->Workflow_model->get(10);
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						$status = json_decode($workflow['status_json'],true);
						
						if(!empty($workflow_order)){
						
						//next status
						$next_status = $workflow_order['next_status_id'];
						$next_status_text = $status[$workflow_order['next_status_id']];
						
						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];
								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						//get next status workflow order data
						$next_workflow_flitter = array(
							'status_id' => $next_status,
							'workflow_id' => 10,
						);
						
						//next status data
						$next_status_data = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
						$awating_person = $next_status_data['role_id'];
						
						//if awating person equal requestor ,asign requestor for it
						if($awating_person == 'requestor'){
							$awating_person = $requestor_role;
						}
						
						//main
						$array['awaiting_table']  = $awating_person;
						$array['status'] = $next_status;
						
						//job
						$jobArray['awaiting_role_id'] = $awating_person.',';
						$jobArray['status_id'] = $next_status;
						$jobArray['status_text'] = $next_status_text;
						
						
						}
						
				  
			  }else{
				  		
												
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $cn_data['status'],
							'workflow_id' => 10,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];	
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(10);
						$status = json_decode($workflow['status_json'],true);
						
						//print_r($workflow_order);exit;
						if(isset($action)){
						
						//action
						switch ($action) {
							case 'approval':
							
								//approved
								
								//clear data infront
								$array = array();
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];

										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
										
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'approval_sendmail':
							
								//approved
								
								//clear data infront
								$array = array();
								
								if($confirm == 1){
								
									if($correct_check == 'on'){
										//next status
										$next_status = $workflow_order['next_status_id'];

										$next_status_text = $status[$workflow_order['next_status_id']];
										$next = 1;
										
										
										
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
										
										//sent mail to service manager
										$role_service_manager = $this->Employee_model->get_related_role(array($workflow_order['who_email']));
										$sitename = $this->Settings_model->get_settings(1);
										$default_email = $this->Settings_model->get_settings(6);
										
										$this->load->library('email');
										
										if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
											foreach($role_service_manager as $k => $v){
												
												$this->email->clear();
												$this->email->from($default_email['value'], $sitename['value']);
												$this->email->to($v['email']); 
												$this->email->subject('Service request completed');
												$this->email->message($sr_data['service_serial'].' Service request had completed');
												$this->email->send();
											  
											}
										}
										
									}
									
								//not approved
								}else{
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
								}
					  
								break;
							case 'update':
								
								//next status
								$next_status = $workflow_order['next_status_id'];
								$next_status_text = $status[$workflow_order['next_status_id']];
								
								$array = array(
								);
								
								//check last job
								if($workflow_order['next_status_text'] == 'Completed'){
									$last = 1;
								}
								
								$next = 1;
								
								break;
							case 'approval_update':
								
								if($confirm == 1){
									
									if($correct_check == 'on'){
										
										//next status
										$next_status = $workflow_order['next_status_id'];
										$next_status_text = $status[$workflow_order['next_status_id']];
										
										$target_colum = json_decode($workflow_order['target_colum'],true);
										
										//check update colum single or multiple
										$array = array();
										foreach($target_colum as $k => $v){
										
											if($v['type'] == 'single'){
												
												if($v['data_type'] == 'string'){
													
												$array[$v['name']] = $this->input->post($v['name'],true);
												
												}else if($v['data_type'] == 'date'){
													
													$date = $this->input->post($v['name'], true);
													if(!empty($date)) {
													  $tmp = explode("/", $date);
													  $date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
													  $array[$v['name']] = $date;
													}
													
												}
											
											}else{
												
											}
											
										}
										$next = 1;
										//check last job
										if($workflow_order['next_status_text'] == 'Completed'){
											$last = 1;
										}
								
									}
									
								}else{
									
									$array = array(
										'awaiting_table' 	=> '',
										'status' 			=> 0,
									);
									$next = 1;
									$last = 1;
									
								}	
								
								break;
							case 'upload':
							
								//clear data infront
								$array = array();
								
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'comfirm_file'	=> $save_path,
											);	
											
											//check last job
											if($workflow_order['next_status_text'] == 'Completed'){
												$last = 1;
											}
											
											//next status
											$next_status = $workflow_order['next_status_id'];
											$next_status_text = $status[$workflow_order['next_status_id']];	
											$next = 1;
																							
									}	
									
								}
								
								break;	
							case 'complete':
							
								//clear data infront
								$array = array();
							
								if(isset($_FILES['comfirm_file'])){
				
									if ( $_FILES['comfirm_file']['error'] == 0 && $_FILES['comfirm_file']['name'] != ''  ){									
											$pathinfo = pathinfo($_FILES['comfirm_file']['name']);
											$ext = $pathinfo['extension'];
											$ext = strtolower($ext);
																
											$filename = 'file_'.date("YmdHis")."_".rand(1000,9999);
											$path = "./uploads/".$filename.'.'.$ext;
											$save_path = base_url()."uploads/".$filename.'.'.$ext;
											$result = move_uploaded_file($_FILES['comfirm_file']['tmp_name'], $path);
											if(!$result) {
												die("cannot upload file");
											}	
										
											$array= array(
												'comfirm_file'	=> $save_path,
											);	
										
									}	
								}
								
								break;	
								
						}
						
						//print_r($workflow_order);exit;
						//if need_det = 1
						
						if($workflow_order['need_det'] == 1){

							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$value = $this->input->post($v['target_colum'], true);
								//string or number
								if(is_numeric($v['value'])){
									$det_temp = $value." ".$v['logic']." ".$v['value'];
								}else{
									
									if($v['value'] == 'original'){
										$string = $sr_data['term_condition'];
									}else{
										$string = $v['value'];
									}
									
									$det_temp = "'".md5(trim($value))."' ".$v['logic']." '".md5(trim($string))."'";
								}
								
								
								$check = $this->parse_boolean($det_temp);
								
								//check
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
							}
							
						}

						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 10,
							);
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
						}
						
						}
						
				  
			  }
			  
			 	if(isset($next_status)){
					//main
					$array['awaiting_table']  = $awating_person;
					$array['status'] = $next_status;
					//job
					$jobArray['awaiting_role_id'] = $awating_person.',';
					$jobArray['status_id'] = $next_status;
					$jobArray['status_text'] = $next_status_text;
				}
				//print_r($array);exit;
			  //////////////////////////////////////////////////////////////////////////////////////
				
			  $array['lastupdate_user_id'] = $this->data['userdata']['id'];
			  $array['modified_date'] = $now;
			  $insert_id = $id;
			  
			  if($next == 1){
				  $this->Job_model->update($jobArray['parent_id'], array(
				  	'is_completed' => 1,
				  ));
				  //不是最後
				  if($last == 0){
				  	$new_job_id = $this->Job_model->insert($jobArray);				  	
					$array['latest_job_id'] = $new_job_id;
				  }
			  }
			  
			  $this->Credit_note_model->update($id, $array);
			  
			  	//mail
				  if(isset($new_job_id)){
				  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
				  $mail_array['related_role'] = $related_role;		
				  $mail_array['title'] = $workflow['title']." ".$cn_data['invoice_no']." ".$next_status_text;
				  }
			    
		  }
		  
		  $item_type = 0;
		  if($mode == 'Add'){
				
				$item_type = 1;
			
		  }else if($mode == 'Edit' && $role_id == $requestor_role){
				
				if($cn_data['status'] < 2 && $cn_data['status'] != 0){
					$item_type = 1;
				}
					
		  }
		  
		  if($item_type == 1){
			  
		  $invoiceCount = $this->input->post("invoiceCount", true);
		  
		  for($i=0;$i<=$invoiceCount;$i++){
			  $invoiceArray = array(
			    'invoice_id' => $insert_id,
			  	'product_id' => $_POST['product_id'.$i],
			  	'uom' => $_POST['uom'.$i],
				'quantity' => $_POST['quantity'.$i],
				'Item_description' => $_POST['description'.$i],
				'unit_price' => $_POST['unitPrice'.$i],
				'extended_price' => $_POST['extendPrice'.$i],
				'total_price' => $_POST['total_price'.$i],
			  );
			  //echo $mode;exit;
			  if($mode=='Add'){
				  $invoiceArray['created_date'] = $now;
				  $this->Credit_note_item_model->insert($invoiceArray);
				  
			  }
		  }
		  }
		  		  
		  //sent mail
			if(isset($mail_array['related_role'])){
		  	$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
			$sitename = $this->Settings_model->get_settings(1);
		  	$default_email = $this->Settings_model->get_settings(6);
			
		  	$this->load->library('email');
			
				if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
							
					foreach($related_role_data as $k => $v){
					$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
					
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->send();	
					  
					}
				
				}
			
			}
				  		 		  		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		  
		  
	  }
	  
	  public function del($id){
		  $this->Credit_note_model->delete($id);
		  
		  $lastpage = $this->session->userdata("lastpage");
		  if(!empty($lastpage)) {
		  	  redirect($lastpage,'refresh');  
		  } else {
			  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
		  }		
	  }  	
	  
	  public function ajax_getQuotation(){
		  
		  	$keyword = $_POST['keyword'];
			$limit = $_POST['limit'];
			$page = $_POST['page'];
			
			$start = ($page-1) * $limit;
		  	//我們只取已經confirm的quotation
		  	$record_count = $this->Quotation_model->ajax_record_count($keyword, array(
				'status'	=> 6
			));
		  	$data = $this->Quotation_model->ajax_quotation($keyword, $limit, $start, array(
				'status'	=> 6
			));
			
			$quotation_status = $this->Quotation_model->quotation_status_list();
			
			//顯示Status為狀態文字, 不是數字
			foreach($data as $k=>$v) {
				$data[$k]['status'] = $quotation_status[$v['status']];
			}
			
		  	$paging = $this->get_paging($limit,10,$record_count,$page);
		  	
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					'paging'	=> $paging,
			);
			
			echo json_encode($temp);	
			exit;
	  }	  
	  
	  public function ajax_getQuotationData(){
		  
		  	$id = $_POST['id'];
		  	$data = $this->Quotation_model->get($id);
			$customer_id = $data['customer_id'];
			$customer_data = $this->Customers_model->get($customer_id);
			$this->data['user_list'] = $this->User_model->getIDKeyArray("name");
			
			$invoiceInfo = array();
			$invoiceInfo['SalesOrderNo']=$data['qr_serial'];
			$invoiceInfo['Originator']=$this->data['user_list'][$data['create_user_id']];
			
			//DeliveryOrderNo
			$DeliveryOrderNo = $this->Delivery_order_model->getRelatedDelivery($id);
			$deliveryArray = array();
			foreach($DeliveryOrderNo as $k => $v){
				$deliveryArray[] =$v['delivery_order_no'];
			}
			if(count($deliveryArray)>1){
			$invoiceInfo['DeliveryOrderNo'] = implode(',',$deliveryArray);
			}else if(!empty($deliveryArray)){
			$invoiceInfo['DeliveryOrderNo'] = $deliveryArray[0];
			}else{
			$invoiceInfo['DeliveryOrderNo'] = '';
			}
			
			//TermsofPayment
			$TermsofPayment = $this->Settings_model->get_settings(11);
			$invoiceInfo['TermsofPayment'] = $TermsofPayment['value'];
			
			//Purchase Order No
			$PurchaseOrderNo = $this->Purchase_request_model->getRelatedPurchase($id);
			$PurchaseArray = array();
			foreach($PurchaseOrderNo as $k => $v){
				$PurchaseArray[] =$v['purchase_serial'];
			}
			if(count($PurchaseArray)>1){
			$invoiceInfo['PurchaseOrderNo'] = implode(',',$PurchaseArray);
			}else if(!empty($PurchaseArray)){
			$invoiceInfo['PurchaseOrderNo'] = $PurchaseArray[0];
			}else{
			$invoiceInfo['PurchaseOrderNo'] = '';
			}
			
			//qutation_related_item
			$qutation_related_item = $this->Quotation_item_model->getRelatedItem($id);
			foreach($qutation_related_item as $k => $v){
				$product_data = $this->Products_model->get($v['product_id']);
				$qutation_related_item [$k]['description'] = $product_data['description'];
				$qutation_related_item [$k]['uom'] = $product_data['uom'];
			}
			
			//Packing list Items
			$qutation_related_product = $this->Quotation_item_model->getRelatedProduct($id);
			$delivery_order_related = $this->Delivery_order_model->getRelatedDelivery($id);
			
			//get qty delivered
			if(!empty($delivery_order_related)){
				$product_id_quantity = array();
				foreach($delivery_order_related as $x => $y){
						$do_related_item = $this->Delivery_order_item_model->get($y['id']);
						
						foreach($do_related_item as $k=> $y){
							if(isset($product_id_quantity[$y['product_id']])){
								$product_id_quantity[$y['product_id']] += $y['quantity'];
							}else{
								$product_id_quantity[$y['product_id']] = $y['quantity'];
							}
						}
						
				}
				foreach($qutation_related_product as $k => $v){
					$qutation_related_product[$k]['delivered_qty'] = isset($product_id_quantity[$v['product_id']])?$product_id_quantity[$v['product_id']]:0;
				}
			}else{
				foreach($qutation_related_product as $k => $v){
					$qutation_related_product[$k]['delivered_qty'] = 0;
				}
			}			
			
			
			
			/*if(!empty($delivery_order_related)){
				foreach($qutation_related_product as $k => $v){
					
					foreach($delivery_order_related as $x => $y){
						$do_related_item = $this->Delivery_order_item_model->get($y['id']);
					}
					
				}
			}*/
			
			
		
	  		$temp = array(
					'status'	=> 'OK',
					'data'		=> $data,
					'invoiceInfo' => $invoiceInfo,
					'customer_data' => $customer_data,
					'qutation_related_item' => $qutation_related_item,
					'packing_list_item' => $qutation_related_product,
			);
			
			
			
			echo json_encode($temp);	
			exit;
	  }	 
	  
	  public function get_paging($item_per_page,$pagenum,$total_item,$page)
	  {
	
		$start = (int)(($page-1)/$pagenum)*$pagenum+1;
		$end = $start+$pagenum-1;
		$next = $page+1;
		$pre = $page-1;
		
		$total_page = ceil( $total_item / $item_per_page );
                $paging = '';
		if($total_item > $item_per_page){
                    $paging .= '<ul class="pagination">';

                    if($page > 1){
                            $paging .= '<li><a href="javascript:changePage(1)">&laquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$pre.')">&lsaquo;</li>';
                    }

                    if($total_page <= $pagenum){

                            for($i=$start;$i<=$total_page;$i++){
                                    if($i == $page){

                                            $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                    }else{

                                            $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                    }
                            }
                    }else{
                            if($page > 5){
                                    $end = $page+5;
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    $start = $end - ($pagenum - 1);

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }else{
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="javascript:changePage('.$i.')">'.$i.'</a></li>';
                                            }
                                    }
                            }	
                    }

                    if($page < $total_page){
                            $paging .= '<li><a href="javascript:changePage('.$next.')">&rsaquo;</a></li>';
                            $paging .= '<li><a href="javascript:changePage('.$total_page.')">&raquo;</a></li>';
                    }

                    $paging .= '</ul>';
                }
		
		return $paging;
	  }	     
	  
	  public function PDF_generation($id, $mode='I') {
		  
		  $cn_data = $this->Credit_note_model->get($id);	
		  $this->data['user_list'] = $this->User_model->getIDKeyArray("name");
		  $customer_id = $cn_data['customer_id'];
		  $customer_data = $this->Customers_model->get($customer_id);
		  $related_item = $this->Credit_note_item_model->get_related($id);
		  $gst = $this->Settings_model->get_settings(5);	
			
		  foreach($related_item as $k => $v){
				$product = $this->Products_model->get($v['product_id']);
				$related_item[$k]['model_no'] = $product['model_no'];
				$related_item[$k]['uom'] = $product['uom'];
				$related_item[$k]['description'] = $product['description'];
		  }
		  
		  $issue_date = $cn_data['issue_date']!='0000-00-00 00:00:00'?date('d/m/Y',strtotime($cn_data['issue_date'])):'';
		  $payment_date = $cn_data['payment_due_day']!='0000-00-00 00:00:00'?date('d/m/Y',strtotime($cn_data['payment_due_day'])):'';
		  
		  //print_r($product);exit;
		  $item_group = '';
		  foreach($related_item as $k => $v){
			  $item_group .='<tr><td>'.($k+1).'</td><td>'.$v['uom'].'</td><td>'.$v['quantity'].'</td><td>'.$v['Item_description'].'</td><td>'.$v['unit_price'].'</td><td>'.$v['extended_price'].'</td><td>'.$v['total_price'].'</td></tr>';
		  }
		  
		  $this->load->library("Pdf");		
          $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		  
		  // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Anexus');
            $pdf->SetTitle($cn_data['delivery_order_no']);
            $pdf->SetSubject("");
            $pdf->SetKeywords("");

            // remove default header/footer
            $pdf->setPrintHeader(false);			
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(10, 10, 10);

            // set auto page breaks
            //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
            }

            // ---------------------------------------------------------

            // set font
            $pdf->SetFont('times', '', 10);

            

            // set font
            $pdf->SetFont('times', '', 10);
			
			// add a page
       		$pdf->AddPage();
			
			$html = '			
			<h1>Credit Note</h1>
			
			<table width="100%" cellpadding="5">
				<tr>
					<td>
						<table width="100%" cellpadding="5" border="1">
			
							<tr>
								<th><h3>Invoice Information</h3></th>
							</tr>
							<tr>
								<th><h4>Invoice No.</h4></th>
							</tr>
							<tr>
								<td>'.$cn_data['invoice_no'].'</td>
							</tr>
							<tr>
								<th><h4>Date of Issue</h4></th>
							</tr>
							<tr>
								<td>'.$issue_date.'</td>
							</tr>
							<tr>
								<th><h4>Customer Purchase Order Ref</h4></th>
							</tr>
							<tr>
								<td>'.$cn_data['purchase_order_ref'].'</td>
							</tr>
							<tr>
								<th><h4>Originator</h4></th>
							</tr>
							<tr>
								<td>'.$this->data['user_list'][$cn_data['sales_person_id']].'</td>
							</tr>
							<tr>
								<th><h4>Sales Order No.</h4></th>
							</tr>
							<tr>
								<td>'.$cn_data['sales_order_no'].'</td>
							</tr>
							<tr>
								<th><h4>Delivery Order No.</h4></th>
							</tr>
							<tr>
								<td>'.$cn_data['delivery_order_no'].'</td>
							</tr>
							<tr>
								<th><h4>Currency</h4></th>
							</tr>
							<tr>
								<td>'.$cn_data['currency'].'</td>
							</tr>
							<tr>
								<th><h4>Terms of Payment</h4></th>
							</tr>
							<tr>
								<td>'.$cn_data['term_of_payment'].'</td>
							</tr>
							<tr>
								<th><h4>Payment Due Date</h4></th>
							</tr>
							<tr>
								<td>'.$payment_date.'</td>
							</tr>
							<tr>
								<th><h4>Purchase Order No.</h4></th>
							</tr>
							<tr>
								<td>'.$cn_data['purchase_order_no'].'</td>
							</tr>
	
						</table>
					</td>
					
					<td>
						<table width="100%" cellpadding="5" border="1">
			
							<tr>
								<th><h3>Customer Information</h3></th>
							</tr>
							<tr>
								<th><h4>Invoice To</h4></th>
							</tr>
							<tr>
								<td>'.$customer_data['company_name'].'</td>
							</tr>
							<tr>
								<th><h4>Account No.</h4></th>
							</tr>
							<tr>
								<td>'.$customer_data['account_no'].'</td>
							</tr>
							<tr>
								<th><h4>Delivered To</h4></th>
							</tr>
							<tr>
								<td>'.$cn_data['delivered_to'].'</td>
							</tr>
							<tr>
								<th><h4>Attention To</h4></th>
							</tr>
							<tr>
								<td>'.$customer_data['primary_attention_to'].'</td>
							</tr>
							<tr>
								<th><h4>Contact</h4></th>
							</tr>
							<tr>
								<td>'.$customer_data['primary_contact_info'].'</td>
							</tr>
							<tr>
								<th><h4>Attention To</h4></th>
							</tr>
							<tr>
								<td>'.$customer_data['secondary_attention_to'].'</td>
							</tr>
							<tr>
								<th><h4>Contact</h4></th>
							</tr>
							<tr>
								<td>'.$customer_data['secondary_contact_info'].'</td>
							</tr>
							<tr>
								<th><h3>Total Amount</h3></th>
							</tr>
							<tr>
								<th><h4>Sub-Total</h4></th>
							</tr>
							<tr>
								<td>'.$cn_data['subtotal'].'</td>
							</tr>
							<tr>
								<th><h4>GST @ '.$gst['value'].'%</h4></th>
							</tr>
							<tr>
								<td>'.$cn_data['gst'].'</td>
							</tr>
							<tr>
								<th><h4>Grand Total</h4></th>
							</tr>
							<tr>
								<td>'.$cn_data['grand_total'].'</td>
							</tr>
		
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="5" border="1">
				
							<tr>
								<th colspan="7"><h3>Commercial Invoice Items</h3></th>
							</tr>
							<tr>
								<td>#</td>
								<td>UOM</td>
								<td>Quantity</td>
								<td>Item Description</td>
								<td>Unit Price</td>
								<td>Extended Price</td>
								<td>Total Price</td>
							</tr>'.$item_group.'
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						
						<table width="100%" cellpadding="5" border="1">				
							<tr>
								<td><h4>Remarks</h4></td>
							</tr>
							<tr>	
								<td>'.$cn_data['remarks'].'</td>
							</tr>
						</table>
						
					</td>
				</tr>

			</table>
			
			';									
			$pdf->writeHTML($html, true, false, true, false, '');
			
			$file_name = date("YmdHis").rand(1000,9999);
			
			if($mode == 'I') {
				$pdf->Output($file_name.'.pdf', 'I'); 	
				return $file_name.'.pdf';
			} else if ($mode == 'f') {				
				$pdf->Output('./uploads/'.$file_name.'.pdf', 'f'); 
				return './uploads/'.$file_name.'.pdf';
			}
		  
		  
	  }
	  
	  public function export_pdf($id){		  		    			
			$this->PDF_generation($id, 'I');		  		  
	  }
	  
	  public function sent_mail($id){
		  
		  	$cn_data = $this->Credit_note_model->get($id);		  		  	
			$customer_id = $cn_data['customer_id'];
			$customer = $this->Customers_model->get($customer_id);
			
			$send_to = $customer['email'];
			
			$sitename = $this->Settings_model->get_settings(1);
		  	$default_email = $this->Settings_model->get_settings(6);			
			
			$role_id = $this->data['userdata']['role_id'];
		    $cn_status = $cn_data['status'];
			
			//$requestor_role
			$requestor_employee = $this->Employee_model->get($cn_data['requestor_id']);
			$requestor_role = $requestor_employee['role_id'];	
		  
		  	$file_name = $this->PDF_generation($id, 'f');	
		  		 
			///////////////////////////////////////////////////////////
			
						//get related workflow order data
						$workflow_flitter = array(								
							'status_id' => $cn_status,
							'workflow_id' => 11,
						);
						
						$workflow_order = $this->Workflow_order_model->get_related_workflow($workflow_flitter);
						
						//$workflow_order['role_id'] == 'requestor'
						if($workflow_order['role_id'] == 'requestor'){
							
							//requestor edit
							if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							
							//user role = current status role
							}else if($role_id == $requestor_role){
								$action = $workflow_order['action'];
							}
							
						//$workflow_order['role_id'] != 'requestor'						
						}else{
							
							//user role = current status role
							if ($workflow_order['role_id'] == $role_id){
								$action = $workflow_order['action'];
							
							//requestor edit															
							}else if($role_id == $requestor_role && $workflow_order['requestor_edit']) {
								$action = 'edit';	
							}
						
						}
						
						//data form database
						$workflow = $this->Workflow_model->get(11);
						$status = json_decode($workflow['status_json'],true);
						
						if(isset($action)){

		
						if($action == 'send_email'){
							
							//next status
							$next_status = $workflow_order['next_status_id'];
							$next_status_text = $status[$workflow_order['next_status_id']];
							$who_email = $workflow_order['who_email'];
	
						}

						//if need_det = 1
						if($workflow_order['need_det'] == 1){
							
							$formula = json_decode($workflow_order['formula'],true);
							
							//check if product or service price over value
							foreach($formula as $k => $v){
							
								$price = $this->input->post($v['target_colum'], true);
								$det_temp = $price." ".$v['logic']." ".$v['value'];
								$check = $this->parse_boolean($det_temp);
								
								//check price over value
								if($check){
									$next_status = $v['next_status_id'];
									$next_status_text = $status[$v['next_status_id']];
									break;
								}
								
							}
							
						}
						
						if(isset($next_status)){
							
							//get next status workflow order data
							$next_workflow_flitter = array(
								'status_id' => $next_status,
								'workflow_id' => 11,
							);
							
							
							//next status data
							$next_status_data = $workflow_order = $this->Workflow_order_model->get_related_workflow($next_workflow_flitter);
							
							//if next requestor = 'requestor' then asign requestor role id for it
							$awating_person = $next_status_data['role_id'];
							if($awating_person == 'requestor'){
								$awating_person = $requestor_role;
							}
							
							
							$jobArray = array(
								'parent_id' 		=>$cn_data['latest_job_id'],
								'user_id' 			=>$this->data['userdata']['id'],
								'customer_id' 		=>$cn_data['customer_id'],
								'type' => 'Commercial Invoice',
								'type_path' =>'logistics/commercial_invoice_edit/',
								'awaiting_role_id' 	=>$awating_person.',',
								'status_id' 		=>$next_status,
								'status_text'		=>$next_status_text,
								'serial' 			=>$cn_data['delivery_order_no'],
								'type_id' 			=>$cn_data['id'],
								'created_date'		=>date("Y-m-d H:i:s"),
								'modified_date'		=>date("Y-m-d H:i:s"),
							);			
					  
						    $array = array(
							'awaiting_table' => $awating_person.',',
							'status' => $next_status,
							'modified_date' => date('Y-m-d H:i:s'),
							'lastupdate_user_id' => $this->data['userdata']['id'],
						    );
							
							
						  //print_r($array);exit;
							
						
						  //更新上一個JOB的狀態					  
						  $this->Job_model->update($jobArray['parent_id'], array(
							'is_completed' => 1,
							'display' => 0,
						  ));
						  
						  //新增一個新的JOB
						  $new_job_id = $this->Job_model->insert($jobArray);
						  $array['latest_job_id'] = $new_job_id;
						  
						  $this->Credit_note_model->update($cn_data['id'], $array);
							
							
						}
						
						}
			
			
			if(isset($who_email)){
			
				$this->load->library('email');
				
				switch($who_email){
					case 'customer':
							
							//Send Notification
							if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
										
								$this->email->from($default_email['value'], $sitename['value']);
								$this->email->to($send_to); 
							
								$this->email->subject('Anexus '.$workflow['title'].': '.$ci_data['invoice_no']);
								$this->email->message($ci_data['invoice_no']);	
								$this->email->attach($filename);
							
								$this->email->send();
							
							}
							
					break;	
					case 'supplier':
						
							  //count supplier
							  $supplier_count = $this->Delivery_order_item_model->get_supplier_groupBy($id);
							  
							  foreach($supplier_count as $k => $v){
									
								  $supplier = $this->Suppliers_model->get($v['supplier_id']);
								  
								  $file_name = $this->PDF_generation($id, 'f',$v['supplier_id']);	
										 
								  if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
									
									$this->load->library('email');
									$this->email->clear();	
									$this->email->from($default_email['value'], $sitename['value']);
									$this->email->to($supplier ['email']); 
									
									$this->email->subject('Anexus Purchase request '.$ci_data['invoice_no']);
									$this->email->message($ci_data['invoice_no']);	
									$this->email->attach($file_name);
									
									$this->email->send();
									
								  }
							  
							  }
							
					break;
					case 'employee':
					break;
				
				}
				
			}
			
			 //mail
			 $mail_array = array(
				'creater_name' =>$this->data['userdata']['name'],
			 );
			 
			 if(isset($new_job_id)){
			  $related_role = $this->Job_model->getRelatedRoleID($new_job_id);
			  $mail_array['related_role'] = $related_role;		
			  $mail_array['title'] = $workflow['title']." ".$cn_data['invoice_no']." ".$next_status_text;
			 }
			 
			 //sent Notification to related role						  			
		  	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
				$related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
			  				  	
			  	foreach($related_role_data as $k => $v){
				  
				  	$this->email->clear();
					$this->email->from($default_email['value'], $sitename['value']);
					$this->email->to($v['email']); 
					
					$this->email->subject($mail_array['title']);
					$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
					$this->email->send();	
				  
		 		}
			}
			
			///////////////////////////////////////////////////////////	 
				 
			
			/*	 
		  	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {		  		  			
				
				$this->load->library('email');
	
				$this->email->from($default_email['value'], $sitename['value']);
				$this->email->to($customer['email']); 				
	
				$this->email->subject('Anexus Delivery Order: '.$do_data['delivery_order_no']);
				$this->email->message($do_data['delivery_order_no']);	
				$this->email->attach($file_name);
				
				$this->email->send();
			
			}
									
			//logistic send mail	
			if($cn_status == 2 && $role_id == FINANCE){
					
			 
					  //set last job completed
					  $this->Job_model->update($cn_data['latest_job_id'], array(
					  	'is_completed' => 1,
					  ));
					  
					  //update pr data
					  $array = array(
			  				'awaiting_table' => '',
							'status' => 3,
			  		  );
				
					  $this->Credit_note_model->update($cn_data['id'], $array);	
					  
					  $mail_array = array(
					    'creater_name' =>$this->data['userdata']['name'],
					    'title' => $cn_data['invoice_no'].' '.$this->data['cn_status_list'][3],
						'related_role' =>array(FINANCE,GENERAL_MANAGER),
					  );
					  
					  //sent mail
					  $related_role_data = $this->Employee_model->get_related_role($mail_array['related_role']);
					  $sitename = $this->Settings_model->get_settings(1);
		  			  $default_email = $this->Settings_model->get_settings(6);
					  
					  $this->load->library('email');
					  
					  if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!="anexus.local") {
						  foreach($related_role_data as $k => $v){
							  
							$this->email->clear();
							$this->email->from($default_email['value'], $sitename['value']);
							$this->email->to($v['email']); 
							$this->email->subject($mail_array['title']);
							$this->email->message($mail_array['creater_name'].' '.$mail_array['title']);
							$this->email->send();	
							  
						  }
					  }
					    
			}
			*/
			
			$lastpage = $this->session->userdata("lastpage");
			if(!empty($lastpage)) {
				  redirect($lastpage,'refresh');  
			}else {
				  redirect(base_url('en/anexus/'.$this->data['group_name'].'/'.$this->data['model_name']));
			}		 
			
		  
	  }
	  

}

?>