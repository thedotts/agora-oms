<div id="pageoptions">
		</div>

			<header>
		<div id="logo">
			<a href="dashboard.html">ERP Configurator</a>
		</div>

				<nav>
			<ul id="nav">
				<li class="i_house"><a href="<?=base_url('en/configurator/dashboard')?>"><span>Start</span></a></li>
				<li class="i_cog"><a href="<?=base_url('en/configurator/settings')?>"><span>Setting</span></a></li>
				<li class="i_table"><a href="<?=base_url('en/configurator/data')?>"><span>Data Setup</span></a></li>
				<li class="i_users"><a href="<?=base_url('en/configurator/roles')?>"><span>Roles Setup</span></a></li>
				<li class="i_breadcrumb"><a href="<?=base_url('en/configurator/workflow')?>"><span>Workflow Setup</span></a></li>
                <li class="i_breadcrumb"><a href="<?=base_url('en/configurator/workflow_order')?>"><span>Workflow Order Setup</span></a></li>
				<!--<li class="i_key"><a href="<?=base_url('en/configurator/access')?>"><span>Access Control Setup</span></a></li>-->
				<li class="i_facebook_like"><a href="<?=base_url('en/configurator/complete')?>"><span>Complete</span></a></li>
			</ul>
		</nav>
		
			
		
		<section id="content">
		
		<div class="g12 nodrop">
			<h1>ROLES SETUP</h1>
            <p>This step allows you to define all roles in the system. Access Control will be in the Access Tab.</p>
            <p>If you are removing a role, make sure that no active accounts are tagged to the role.</p>
            
            <button href="#" id="add_role" class="btn small">Add New Role</button>
            
            <form id="role_form" action="submit.php" method="post" autocomplete="off">
                <fieldset>
                    <label>Role Table</label>
                    <table>
                    <thead>
                        <tr>
                            <th width="40%">Role Name</th><th width="40%">Manager</th><th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($roles)&&!empty($roles)){
                            foreach($roles as $r){
                        ?>
                        <tr>
                            <td><input class="g10" type="text" value="<?= $r['name']?>" name="roles[<?= $r['id']?>].name"></td>
                            <input type="hidden" value="<?= $r['id']?>" name="roles[<?= $r['id']?>].id"></input>
                            <td><select  name="roles[<?= $r['id']?>].parent_id">
                            	<?php
								foreach($default_parent as $v) {
								?>
                                <option value="<?=$v['id']?>" <?=in_array($r['id'], $v['children'])?'selected="selected"':''?>><?=$v['value']?></option>
                                <?php	
								}
								?>
                            </select></td>
                            <td><a href="#" class="removeRowBtn">Remove</a></td>
                        </tr>
                        <?php } //roles
                         }else{
                        ?>
                        <tr>
                            <td><input class="g10" type="text" value="Super Admin" name="roles[1].name"></td>
                            <input type="hidden" value="1" name="roles[1].id"></input>
                            <td><select name="roles[1].parent_id"><option selected value='0'>Non</option></select></td>
                            <td><a href="#" class="removeRowBtn">Remove</a></td>
                        </tr>
                        <tr>
                            <td><input class="g10" type="text" value="General Manager" name="roles[2].name"></td>
                            <input type="hidden" value="2" name="roles[2].id"></input>
                            <td><select name="roles[2].parent_id"><?php
								foreach($default_parent as $v) {
								?>
                                <option value="<?=$v['id']?>" <?=in_array(2, $v['children'])?'selected="selected"':''?>><?=$v['value']?></option>
                                <?php	
								}
								?></select></td>
                            <td><a href="#" class="removeRowBtn">Remove</a></td>
                        </tr>
                        <tr>
                            <td><input class="g10" type="text" value="Service Manager" name="roles[3].name"></td>
                            <input type="hidden" value="3" name="roles[3].id"></input>
                            <td><select name="roles[3].parent_id"><?php
								foreach($default_parent as $v) {
								?>
                                <option value="<?=$v['id']?>" <?=in_array(3, $v['children'])?'selected="selected"':''?>><?=$v['value']?></option>
                                <?php	
								}
								?></select></td>
                            <td><a href="#" class="removeRowBtn">Remove</a></td>
                        </tr>
                        <tr>
                            <td><input class="g10" type="text" value="Service Executive" name="roles[4].name"></td>
                            <input type="hidden" value="4" name="roles[4].id"></input>
                            <td><select name="roles[4].parent_id"><?php
								foreach($default_parent as $v) {
								?>
                                <option value="<?=$v['id']?>" <?=in_array(4, $v['children'])?'selected="selected"':''?>><?=$v['value']?></option>
                                <?php	
								}
								?></select></td>
                            <td><a href="#" class="removeRowBtn">Remove</a></td>
                        </tr>
                        <tr>
                            <td><input class="g10" type="text" value="Finance" name="roles[5].name"></td>
                            <input type="hidden" value="5" name="roles[5].id"></input>
                            <td><select name="roles[5].parent_id"><?php
								foreach($default_parent as $v) {
								?>
                                <option value="<?=$v['id']?>" <?=in_array(5, $v['children'])?'selected="selected"':''?>><?=$v['value']?></option>
                                <?php	
								}
								?></select></td>
                            <td><a href="#" class="removeRowBtn">Remove</a></td>
                        </tr>
                        <tr>
                            <td><input class="g10" type="text" value="Logistic" name="roles[6].name"></td>
                            <input type="hidden" value="6" name="roles[6].id"></input>
                            <td><select name="roles[6].parent_id"><?php
								foreach($default_parent as $v) {
								?>
                                <option value="<?=$v['id']?>" <?=in_array(6, $v['children'])?'selected="selected"':''?>><?=$v['value']?></option>
                                <?php	
								}
								?></select></td>
                            <td><a href="#" class="removeRowBtn">Remove</a></td>
                        </tr>
                        <tr>
                            <td><input class="g10" type="text" value="Sales Manager" name="roles[7].name"></td>
                            <input type="hidden" value="7" name="roles[7].id"></input>
                            <td><select name="roles[7].parent_id"><?php
								foreach($default_parent as $v) {
								?>
                                <option value="<?=$v['id']?>" <?=in_array(7, $v['children'])?'selected="selected"':''?>><?=$v['value']?></option>
                                <?php	
								}
								?></select></td>
                            <td><a href="#" class="removeRowBtn">Remove</a></td>
                        </tr>
                        <tr>
                            <td><input class="g10" type="text" value="Sales Executive" name="roles[8].name"></td>
                            <input type="hidden" value="8" name="roles[8].id"></input>
                            <td><select name="roles[8].parent_id"><?php
								foreach($default_parent as $v) {
								?>
                                <option value="<?=$v['id']?>" <?=in_array(8, $v['children'])?'selected="selected"':''?>><?=$v['value']?></option>
                                <?php	
								}
								?></select></td>
                            <td><a href="#" class="removeRowBtn">Remove</a></td>
                        </tr>
                        <tr>
                            <td><input class="g10" type="text" value="Human Resource" name="roles[9].name"></td>
                            <input type="hidden" value="9" name="roles[9].id"></input>
                            <td><select name="roles[9].parent_id"><?php
								foreach($default_parent as $v) {
								?>
                                <option value="<?=$v['id']?>" <?=in_array(9, $v['children'])?'selected="selected"':''?>><?=$v['value']?></option>
                                <?php	
								}
								?></select></td>
                            <td><a href="#" class="removeRowBtn">Remove</a></td>
                        </tr>
                        <tr>
                            <td><input class="g10" type="text" value="Purchaser" name="roles[10].name"></td>
                            <input type="hidden" value="10" name="roles[10].id"></input>
                            <td><select name="roles[10].parent_id"><?php
								foreach($default_parent as $v) {
								?>
                                <option value="<?=$v['id']?>" <?=in_array(10, $v['children'])?'selected="selected"':''?>><?=$v['value']?></option>
                                <?php	
								}
								?></select></td>
                            <td><a href="#" class="removeRowBtn">Remove</a></td>
                        </tr>
                        <?php } ?>
                    </tbody>
					</table>
                </fieldset>
            	<button id="save_roles" class="btn i_triangle_double_right green icon next">NEXT</button>
            </form>

		</div></section>        
<script>
$(document).ready(function(){
	<!--add/remove roles-->
	var cnt = 8;
	$('#add_role').click(function(){
		
		var option_array = <?=isset($default_parent)?json_encode($default_parent):'[]'?>;
		
		var select_options = '';
		for(v in option_array) {
			select_options += '<option value="'+option_array[v].id+'">'+option_array[v].value+'</option>';	
		}
		
		$('#role_form').find('tbody')
		.append($('<tr><td><input class="g10" type="text" value="" placeholder="Place new role here" name="roles[' + cnt + '].name"></td><input type="hidden" value="'+ cnt +'" name="roles[' + cnt + '].id"></input><td><div class="selector" id="uniform-undefined"><span>Sales Manager</span><select name="roles[' + cnt + '].parent_id" style="opacity: 0;">'+select_options+'</select></div></td><td><a href="#" class="removeRowBtn">Remove</a></td></tr>'));
		cnt += 1;
	});
	$('.removeRowBtn').live('click', function(){
		$(this).parent().parent().remove();
	});
	<!--get form data-->
	$('#save_roles').click(function(){
		var roles = form2js('role_form', '.', true);
		//console.log(roles);
		$.ajax({
			url: '<?=base_url("en/configurator/save_config")?>',
			type: 'POST',
			data: {
				'section': 'roles',
				'roles': JSON.stringify(roles)
			}
		}).done(function(){
			window.location.assign("/en/configurator/workflow");
		});
	});
		
	
});
</script>