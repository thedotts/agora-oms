				<div id="pageoptions">
		</div>

			<header>
		<div id="logo">
			<a href="dashboard.html">ERP Configurator</a>
		</div>

				<nav>
			<ul id="nav">
				<li class="i_house"><a href="<?=base_url('en/configurator/dashboard')?>"><span>Start</span></a></li>
				<li class="i_cog"><a href="<?=base_url('en/configurator/settings')?>"><span>Setting</span></a></li>
				<li class="i_table"><a href="<?=base_url('en/configurator/data')?>"><span>Data Setup</span></a></li>
				<li class="i_users"><a href="<?=base_url('en/configurator/roles')?>"><span>Roles Setup</span></a></li>
				<li class="i_breadcrumb"><a href="<?=base_url('en/configurator/workflow')?>"><span>Workflow Setup</span></a></li>
                <li class="i_breadcrumb"><a href="<?=base_url('en/configurator/workflow_order')?>"><span>Workflow Order Setup</span></a></li>
				<!--<li class="i_key"><a href="<?=base_url('en/configurator/access')?>"><span>Access Control Setup</span></a></li>-->
				<li class="i_facebook_like"><a href="<?=base_url('en/configurator/complete')?>"><span>Complete</span></a></li>
			</ul>
		</nav>
		
			
		
		<section id="content">
		
		<div class="g12 nodrop">
			<h1>COMPLETED</h1>
			<p>You have completed the setup process. Click the complete button to create database and import your settings.</p>
            <button id="complete" class="btn i_triangle_double_right green icon next">Complete</button>
			<p>You can access the portal login from <a href="/en/login">here</a>. Please remember to delete the install folder.</p>
		</div>


			</section>
<script>

function sleep(millis, callback) {
    setTimeout(function()
            { callback(); }
    , millis);
}

$(document).ready(function(){
    <!--get form data-->
    $('#complete').click(function(){
        $.ajax({
            url: '<?=base_url('en/configurator/save_config')?>',
            type: 'POST',
            data: {
                'section': 'complete'
            },
			complete: function(xhr, textStatus){
				//console.log(xhr);
				if(xhr.status != 200) {					
					alert(xhr.responseText);	
				} else {
					
					sleep(3000, window.location.assign("/en/loading"));
					 
				}	
			}
        });
    });
});
</script>