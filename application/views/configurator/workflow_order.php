<div id="pageoptions">
		</div>

			<header>
		<div id="logo">
			<a href="dashboard.html">ERP Configurator</a>
		</div>

				<nav>
			<ul id="nav">
				<li class="i_house"><a href="<?=base_url('en/configurator/dashboard')?>"><span>Start</span></a></li>
				<li class="i_cog"><a href="<?=base_url('en/configurator/settings')?>"><span>Setting</span></a></li>
				<li class="i_table"><a href="<?=base_url('en/configurator/data')?>"><span>Data Setup</span></a></li>
				<li class="i_users"><a href="<?=base_url('en/configurator/roles')?>"><span>Roles Setup</span></a></li>
				<li class="i_breadcrumb"><a href="<?=base_url('en/configurator/workflow')?>"><span>Workflow Setup</span></a></li>
                <li class="i_breadcrumb"><a href="<?=base_url('en/configurator/workflow_order')?>"><span>Workflow Order Setup</span></a></li>
				<!--<li class="i_key"><a href="<?=base_url('en/configurator/access')?>"><span>Access Control Setup</span></a></li>-->
				<li class="i_facebook_like"><a href="<?=base_url('en/configurator/complete')?>"><span>Complete</span></a></li>
			</ul>
		</nav>

		<section id="content">

        <section>

		<div class="g12 nodrop">
			<h1>WORKFLOW ORDER SETUP</h1>
            <p>This step allows you to configure the workflow order.</p>
            <select id="workflow_status">
				<option value="order">Order</option>
                <option value="packing_list">Packing List</option>
                <option value="delivery_order">Delivery Order</option>
                <option value="invoice">Invoice</option>
            </select>
            <input type="hidden" name="section" value="workflow">
         <div style="overflow:auto">
         <table>
        	<thead>
            	<th>Status</th>
                <th>Action</th>
                <th>Requestor Edit</th>
                <th>Role In Charge</th>
                <th>Need Determine</th>
                <th>Formula</th>
                <th>Target Colum</th>
                <th>Last Action</th>
                <th>Next status</th>
                <th>Ad Hoc</th>
                <th>Sendmail Target</th>
            </thead>
        	<tbody id="table_content">
            </tbody>
        </table>
        </div>
        
        </div>
        
        <div class="g12">
                <button id="save_workflow" class="btn i_triangle_double_right green icon next">NEXT</button>
		</div>

		</section>
<script>
var workflow_status = <?=$status?>;

var workflow = {'order':{}, 'packing_list':{}, 'delivery_order':{}, 'invoice':{}};

workflow['order'] = [{"id":"16","status_id":"-1","action":"create","requestor_edit":"0","role_id":"1","workflow_id":"1","need_det":"0","formula":"","target_colum":"","action_before_complete":"0","next_status_id":"2","next_status_text":workflow_status.order.status_json[2],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"},{"id":"1","status_id":"-1","action":"create","requestor_edit":"0","role_id":"7","workflow_id":"1","need_det":"0","formula":"","target_colum":"","action_before_complete":"0","next_status_id":"1","next_status_text":workflow_status.order.status_json[1],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"},{"id":"2","status_id":"-1","action":"create","requestor_edit":"0","role_id":"3","workflow_id":"1","need_det":"0","formula":"","target_colum":"","action_before_complete":"0","next_status_id":"2","next_status_text":workflow_status.order.status_json[2],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"},{"id":"3","status_id":"1","action":"approval_update_s","requestor_edit":"0","role_id":"3","workflow_id":"1","need_det":"0","formula":"","target_colum":"","action_before_complete":"0","next_status_id":"2","next_status_text":workflow_status.order.status_json[2],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"},{"id":"4","status_id":"2","action":"complete","requestor_edit":"0","role_id":"3","workflow_id":"1","need_det":"0","formula":"","target_colum":"","action_before_complete":"0","next_status_id":"2","next_status_text":workflow_status.order.status_json[2],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"},{"id":"5","status_id":"0","action":"no_approve","requestor_edit":"0","role_id":"0","workflow_id":"1","need_det":"0","formula":"","target_colum":"","action_before_complete":"0","next_status_id":"0","next_status_text":workflow_status.order.status_json[0],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"}];

workflow['packing_list'] = [{"id":"17","status_id":"-1","action":"create","requestor_edit":"0","role_id":"1","workflow_id":"2","need_det":"0","formula":"","target_colum":"","action_before_complete":"0","next_status_id":"1","next_status_text":workflow_status.packing_list.status_json[1],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"},{"id":"19","status_id":"-1","action":"create","requestor_edit":"0","role_id":"8","workflow_id":"2","need_det":"0","formula":"","target_colum":"","action_before_complete":"0","next_status_id":"1","next_status_text":workflow_status.packing_list.status_json[1],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"},{"id":"6","status_id":"-1","action":"create","requestor_edit":"0","role_id":"4","workflow_id":"2","need_det":"0","formula":"","target_colum":"","action_before_complete":"0","next_status_id":"1","next_status_text":workflow_status.packing_list.status_json[1],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"},{"id":"7","status_id":"1","action":"complete","requestor_edit":"0","role_id":"4","workflow_id":"2","need_det":"0","formula":"","target_colum":"","action_before_complete":"pdf","next_status_id":"1","next_status_text":workflow_status.packing_list.status_json[1],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"},{"id":"8","status_id":"0","action":"no_approve","requestor_edit":"0","role_id":"0","workflow_id":"2","need_det":"0","formula":"","target_colum":"","action_before_complete":"0","next_status_id":"0","next_status_text":workflow_status.packing_list.status_json[0],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"}];

workflow['delivery_order'] = [{"id":"18","status_id":"-1","action":"create","requestor_edit":"0","role_id":"1","workflow_id":"3","need_det":"0","formula":"","target_colum":"","action_before_complete":"0","next_status_id":"2","next_status_text":workflow_status.delivery_order.status_json[2],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"},{"id":"20","status_id":"-1","action":"create","requestor_edit":"0","role_id":"8","workflow_id":"3","need_det":"0","formula":"","target_colum":"","action_before_complete":"0","next_status_id":"2","next_status_text":workflow_status.delivery_order.status_json[2],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"},{"id":"9","status_id":"-1","action":"create","requestor_edit":"0","role_id":"5","workflow_id":"3","need_det":"0","formula":"","target_colum":"","action_before_complete":"0","next_status_id":"2","next_status_text":workflow_status.delivery_order.status_json[2],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"},{"id":"10","status_id":"1","action":"upload_update","requestor_edit":"0","role_id":"5","workflow_id":"3","need_det":"0","formula":"","target_colum":"","action_before_complete":"","next_status_id":"2","next_status_text":workflow_status.delivery_order.status_json[2],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"},{"id":"11","status_id":"2","action":"complete","requestor_edit":"0","role_id":"5","workflow_id":"3","need_det":"0","formula":"","target_colum":"","action_before_complete":"upload","next_status_id":"2","next_status_text":workflow_status.delivery_order.status_json[2],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"},{"id":"12","status_id":"0","action":"no_approve","requestor_edit":"0","role_id":"0","workflow_id":"3","need_det":"0","formula":"","target_colum":"","action_before_complete":"0","next_status_id":"0","next_status_text":workflow_status.delivery_order.status_json[0],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"}];

workflow['invoice'] = [{"id":"13","status_id":"-1","action":"create","requestor_edit":"0","role_id":"6","workflow_id":"4","need_det":"0","formula":"","target_colum":"","action_before_complete":"0","next_status_id":"1","next_status_text":workflow_status.invoice.status_json[1],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"},{"id":"7","status_id":"1","action":"complete","requestor_edit":"0","role_id":"6","workflow_id":"4","need_det":"0","formula":"","target_colum":"","action_before_complete":"pdf","next_status_id":"1","next_status_text":workflow_status.invoice.status_json[1],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"},{"id":"8","status_id":"0","action":"no_approve","requestor_edit":"0","role_id":"0","workflow_id":"4","need_det":"0","formula":"","target_colum":"","action_before_complete":"0","next_status_id":"0","next_status_text":workflow_status.invoice.status_json[0],"ad_hoc":"0","who_email":"","created_date":"0000-00-00 00:00:00","modified_date":"0000-00-00 00:00:00","is_deleted":"0"}];



var roles = <?=$roles?>;

var action = ['approval','approval_sendmail','approval_update','approval_update_s','complete','create','no_approve','send_email','sendmail_update','update','upload','upload_collection','upload_delivery','upload_update','pdf','upload_pdf'];

var sendmail_target = ['customer','employee'];

var last_flow = '';

$('#workflow_status').on('change', function() {

	if(last_flow != ''){
		
		var status_text = new Array();
		$('.status').each(function(){
			status_text.push($(this).val());
		});
		
		var action_text = new Array();
		$('.action').each(function(){
			action_text.push($(this).val());
		});
		
		var edit_text = new Array();
		$('.edit').each(function(){
			if($(this).is(':checked')){
				edit_text.push(1);
			}else{
				edit_text.push(0);
			}
		});
		
		var role_text = new Array();
		$('.role').each(function(){
			role_text.push($(this).val());
		});
		
		var det_text = new Array();
		$('.det').each(function(){
			if($(this).is(':checked')){
				det_text.push(1);
			}else{
				det_text.push(0);
			}
		});
		
		var formula_text = new Array();
		$('.formula').each(function(){
			formula_text.push($(this).val());
		});
		
		var target_col_text = new Array();
		$('.target_col').each(function(){
			target_col_text.push($(this).val());
		});
		
		var last_action_text = new Array();
		$('.last_action').each(function(){
			last_action_text.push($(this).val());
		});
		
		var next_status_text = new Array();
		$('.next_status').each(function(){
			next_status_text.push($(this).val());
		});
		
		var ad_hoc_text = new Array();
		$('.ad_hoc').each(function(){
			if($(this).is(':checked')){
				ad_hoc_text.push(1);
			}else{
				ad_hoc_text.push(0);
			}
		});
		
		var mail_target_text = new Array();
		$('.mail_target').each(function(){
			mail_target_text.push($(this).val());
		});
		
		var replace_data = new Array();
		for(var i=0;i<status_text.length;i++){
			
			var tmp = {"status_id":status_text[i],"action":action_text[i],"requestor_edit":edit_text[i],"role_id":role_text[i],"workflow_id":workflow_status[last_flow].id,"need_det":det_text[i],"formula":formula_text[i],"target_colum":target_col_text[i],"action_before_complete":"","next_status_id":last_action_text[i],"next_status_text":workflow_status[last_flow].status_json[next_status_text[i]],"ad_hoc":ad_hoc_text[i],"who_email":mail_target_text[i]};
			
			replace_data.push(tmp);
		}
		
		workflow[last_flow] = replace_data;
		console.log(replace_data);
		
	}
	
	last_flow = this.value;
	var selected_workflow = this.value;
	
	var status_option = '';
	for(var i=0;i<workflow_status[selected_workflow].status_json.length;i++){
		status_option += '<option value ="'+i+'">'+workflow_status[selected_workflow].status_json[i]+'</option>';
	}
	
	var tmp_html = '';
	for(var i=0;i<workflow[selected_workflow].length;i++){
		
		var edit_check = '';
		if(workflow[selected_workflow][i].requestor_edit == 1){
			edit_check = 'checked';
		}
		
		var det_check = '';
		if(workflow[selected_workflow][i].need_det == 1){
			det_check = 'checked';
		}
		
		var adhoc_check = '';
		if(workflow[selected_workflow][i].ad_hoc == 1){
			adhoc_check = 'checked';
		}
		

		var roles_option = '';
		for(var j=0;j<roles.length;j++){
			var selected = '';
			if(roles[j].id == workflow[selected_workflow][i].role_id){
				selected = 'selected';
			}
			roles_option += '<option value ="'+roles[j].id+'" '+selected+'>'+roles[j].name+'</option>';
		}
		
		var status_option = '';
		for(var x=0;x<workflow_status[selected_workflow].status_json.length;x++){
			var selected = '';
			if(x == workflow[selected_workflow][i].next_status_id){
				selected = 'selected';
			}
			status_option += '<option value ="'+x+'" '+selected+'>'+workflow_status[selected_workflow].status_json[x]+'</option>';
		}
		
		var action_option = '';
		for(var y=0;y<action.length;y++){
			var selected = '';
			if(action[y] == workflow[selected_workflow][i].action){
				selected = 'selected';
			}
			action_option += '<option value ="'+action[y]+'" '+selected+'>'+action[y]+'</option>';
		}
		
		var last_action_option = '<option value="0">Select</option>';
		for(var z=0;z<action.length;z++){
			var selected = '';
			if(action[z] == workflow[selected_workflow][i].action_before_complete){
				selected = 'selected';
			}
			last_action_option += '<option value ="'+action[z]+'" '+selected+'>'+action[z]+'</option>';
		}
		
		var sendmail_target_option = '<option value="0">Select</option>';
		for(var a=0;a<sendmail_target.length;a++){
			var selected = '';
			if(sendmail_target[a] == workflow[selected_workflow][i].who_email){
				selected = 'selected';
			}
			sendmail_target_option += '<option value ="'+sendmail_target[a]+'" '+selected+'>'+sendmail_target[a]+'</option>';
		}
		
		tmp_html += '<tr><>';
		tmp_html += '<td><input class="status" type="text" value="'+workflow[selected_workflow][i].status_id+'" readonly /></td>';
		tmp_html += '<td><select class="action">'+action_option+'</select></td>';
		tmp_html += '<td><input class="edit" type="checkbox" '+edit_check+'/></td>';
		tmp_html += '<td><select class="role">'+roles_option+'</select></td>';
		tmp_html += '<td><input class="det" type="checkbox" '+det_check+'/></td>';
		tmp_html += '<td><textarea class="formula">'+workflow[selected_workflow][i].formula+'</textarea></td>';
		tmp_html += '<td><textarea class="target_col">'+workflow[selected_workflow][i].target_colum+'</textarea></td>';
		tmp_html += '<td><select class="last_action">'+last_action_option+'</select></td>';
		tmp_html += '<td><select class="next_status">'+status_option+'</select></td>';
		tmp_html += '<td><input class="ad_hoc" type="checkbox" '+adhoc_check+'/></td>';
		tmp_html += '<td><select class="mail_target">'+sendmail_target_option+'</select></td>';
		tmp_html += '</tr>';
		
	}

	$('#table_content').html('');
	$('#table_content').append(tmp_html);
});


$(document).ready(function(){
    
    $('#save_workflow').click(function(){
		
		if(last_flow != ''){
		
		var status_text = new Array();
		$('.status').each(function(){
			status_text.push($(this).val());
		});
		
		var action_text = new Array();
		$('.action').each(function(){
			action_text.push($(this).val());
		});
		
		var edit_text = new Array();
		$('.edit').each(function(){
			if($(this).is(':checked')){
				edit_text.push(1);
			}else{
				edit_text.push(0);
			}
		});
		
		var role_text = new Array();
		$('.role').each(function(){
			role_text.push($(this).val());
		});
		
		var det_text = new Array();
		$('.det').each(function(){
			if($(this).is(':checked')){
				det_text.push(1);
			}else{
				det_text.push(0);
			}
		});
		
		var formula_text = new Array();
		$('.formula').each(function(){
			formula_text.push($(this).val());
		});
		
		var target_col_text = new Array();
		$('.target_col').each(function(){
			target_col_text.push($(this).val());
		});
		
		var last_action_text = new Array();
		$('.last_action').each(function(){
			last_action_text.push($(this).val());
		});
		
		var next_status_text = new Array();
		$('.next_status').each(function(){
			next_status_text.push($(this).val());
		});
		
		var ad_hoc_text = new Array();
		$('.ad_hoc').each(function(){
			if($(this).is(':checked')){
				ad_hoc_text.push(1);
			}else{
				ad_hoc_text.push(0);
			}
		});
		
		var mail_target_text = new Array();
		$('.mail_target').each(function(){
			mail_target_text.push($(this).val());
		});
		
		var replace_data = new Array();
		for(var i=0;i<status_text.length;i++){
			
			var tmp = {"status_id":status_text[i],"action":action_text[i],"requestor_edit":edit_text[i],"role_id":role_text[i],"workflow_id":workflow_status[last_flow].id,"need_det":det_text[i],"formula":formula_text[i],"target_colum":target_col_text[i],"action_before_complete":"","next_status_id":last_action_text[i],"next_status_text":workflow_status[last_flow].status_json[next_status_text[i]],"ad_hoc":ad_hoc_text[i],"who_email":mail_target_text[i]};
			
			replace_data.push(tmp);
		}
		
		workflow[last_flow] = replace_data;
		
		}
		
        $.ajax({
            url: '<?=base_url("en/configurator/save_config")?>',
            type: 'POST',
            data: {
                'section': 'workflow_order',
                'workflow_order': JSON.stringify(workflow)
            }
        }).done(function() {
            window.location.assign("/en/configurator/complete");
        });
    });
	
});
</script>