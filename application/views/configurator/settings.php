				<div id="pageoptions">
		</div>

			<header>
		<div id="logo">
			<a href="<?=base_url('/en/configurator/dashboard')?>">ERP Configurator</a>
		</div>

				<nav>
			<ul id="nav">
				<li class="i_house"><a href="<?=base_url('en/configurator/dashboard')?>"><span>Start</span></a></li>
				<li class="i_cog"><a href="<?=base_url('en/configurator/settings')?>"><span>Setting</span></a></li>
				<li class="i_table"><a href="<?=base_url('en/configurator/data')?>"><span>Data Setup</span></a></li>
				<li class="i_users"><a href="<?=base_url('en/configurator/roles')?>"><span>Roles Setup</span></a></li>
				<li class="i_breadcrumb"><a href="<?=base_url('en/configurator/workflow')?>"><span>Workflow Setup</span></a></li>
                <li class="i_breadcrumb"><a href="<?=base_url('en/configurator/workflow_order')?>"><span>Workflow Order Setup</span></a></li>
				<!--<li class="i_key"><a href="<?=base_url('en/configurator/access')?>"><span>Access Control Setup</span></a></li>-->
				<li class="i_facebook_like"><a href="<?=base_url('en/configurator/complete')?>"><span>Complete</span></a></li>
			</ul>
		</nav>

		<section id="content">

		<div class="g12 nodrop">
			<h1>SETTINGS</h1>
            <p>This step allows you to setup database connection and basic site settings</p>
			<form id="setting_form" action="<?=base_url('en/configurator/save_config')?>" method="post" autocomplete="off"  data-ajax="false" enctype="multipart/form-data">
	            <input type="hidden" name="section" value="settings">
                <fieldset>
                    <label>DATABASE SETTINGS</label>
                    <section><label for="text_field">Database Name</label>
                        <div><input type="text" name='db_name' id="db_name" required="required" value="agora"></div>
                    </section>
                    <section><label for="text_field">User</label>
                        <div><input type="text" name='db_usr' id="db_usr" required="required" value="agora"></div>
                    </section>
                    <section><label for="text_field">Password</label>
                        <div><input type="text" name='db_passwd' id="db_passwd" required="required" value="6cBa84VaN5NqKpr6"></div>
                    </section>
                    <section><label for="text_field">Database Host</label>
                        <div><input type="text" name='db_host' id="db_host" required="required" value="localhost"></div>
                    </section>
                    <section>
                    <button id="test_db_connect">Check Connection</button>
                    </section>
                </fieldset>
                		<div id="connect_success"></div>
                		<div id="connect_failed"></div>
                <fieldset>
                    <label>SITE SETTINGS</label>
                    <section><label for="text_field">Company Name</label>
                        <div><input type="text" name="company_name" id="company_name" required="required" value="Agora"></div>
                    </section>
                    <section><label for="text_field">Company Address</label>
                        <div><input type="text" name="company_address" id="company_address" required="required" value="18 Mandai Estate"></div>
                    </section>
                    <section><label for="text_field">Company Postal</label>
                        <div><input type="text" name="company_postal" id="company_postal" required="required" value="729910"></div>
                    </section>
                    <section><label for="text_field">Site Name</label>
                        <div><input type="text" name="site_name" id="site_name" required="required" value="Pac-fung Agora"></div>
                    </section>
                    <section><label for="logo_upload">Logo</span></label>
                        <div>
                        	<input type="file" id="logo_upload" name="logo_upload">
                            <input type="hidden" name="logo_url" id="logo_url" />
                        </div>
                    </section>
                    <section style="display:none"><label for="text_field">Quotation Sales Value *</label>
                        <div><input type="text" name="quotation_sale_value" id="quotation_sale_value" value="150000" placeholder="The amount of sales value that requires approval by manager /management"></div>
                    </section>
                    <section style="display:none"><label for="text_field">Quotation Service Value *</label>
                        <div><input type="text" name="quotation_service_value" id="quotation_service_value" value="50000" placeholder="The amount of service value that requires approval by manager /management"></div>
                    </section>
                    <section style="display:none"><label for="text_field">Purchase Order Purchase Value *</label>
                        <div><input type="text" name="purchase_value" id="purchase_value" value="0" placeholder="The amount of sales value that requires approval by manager /management"></div>
                    </section>
                    <section><label for="text_field">GST Percentage *</label>
                        <div><input type="text" name="gst_percentage" id="gst_percentage" value="7" placeholder="The percentage of GST"></div>
                    </section>
                    <section><label for="text_field">Default Email *</label>
                        <div><input type="text" name="default_email" id="default_email" value="test@gmail.com" placeholder="Default Email Address of Company"></div>
                    </section>
                    <section><label for="text_field">Main Contact Person *</label>
                        <div><input type="text" name="main_contact_person" id="main_contact_person" value="Jason Tian" placeholder="Main Contact Person of Company"></div>
                    </section>
                    <section><label for="text_field">Main Contact No. *</label>
                        <div><input type="text" name="main_contact_no" id="main_contact_no" value="0722334455" placeholder="Main Contact No."></div>
                    </section>
                    <section><label for="text_field">Payment Information</label>
                        <div><input type="text" name="payment_information" id="payment_information" value="" placeholder="To be used in invoice"></div>
                    </section>
                    <section style="display:none"><label for="text_field">Purchase Order Terms</label>
                        <div><input type="text" name="purchase_order_terms" id="purchase_order_terms" value="" placeholder="To be used in Purchase Order"></div>
                    </section>
                    <section style="display:none"><label for="text_field">List of Payment Terms</label>
                        <div><textarea name="payment_terms" id="payment_terms" placeholder="Name, value"></textarea></div>
                    </section>
                    <section style="display:none"><label for="text_field">Quantity for reminder</label>
                        <div><input type="text" name="qty_for_reminder" id="qty_for_reminder" value="" placeholder="Default Quantity when drop below this amount to send an email reminder to default company email account"></div>
                    </section>
                    <section><label for="text_field">Pre-fixed for Document</label>
                        <div><textarea name="prefix" id="prefix" placeholder="Admin to be able to change all pre-fixed alphabet to be displayed in front of all serial numbers. ">[{"table_name":"order","prefix":"O","YY":1},{"table_name":"packing_list","prefix":"PL","YY":0},{"table_name":"delivery_order","prefix":"DO","YY":0},{"table_name":"invoice","prefix":"I","YY":0}]</textarea></div>
                    </section>
                    <section style="display:none"><label for="text_field">Reminder Alert</label>
                        <div><textarea name="reminder_alert" id="reminder_alert" placeholder="Admin to be able to change reminder alert for PO generation (date generated), Quotation Generation (date generated) and Waiting for Good (use date of delivery from Purchase) ">[{"action":"PO generation","date":""},{"action":"Quotation Generation","date":""},{"action":"Waiting for Good ","date":""}]</textarea></div>
                    </section>
                    <section><label for="text_field">Hour of 1st Task List Reminder</label>
                        <div><input type="text" name="task_reminder" id="task_reminder" value="" placeholder=""></div>
                    </section>
                    <section><label for="text_field">Hour of 2nd Task List Reminder</label>
                        <div><input type="text" name="task_reminder2" id="task_reminder2" value="" placeholder=""></div>
                    </section>
                    
                    
                </fieldset>
                
                <fieldset>
                	<label>ADMIN ACCOUNT</label>
                	<section><label for="text_field">Admin Email</label>
                        <div><input type="text" name="admin_email" id="admin_email" placeholder="Please input a Superadmin Email. e.g.: abc@example.com" required="required" value="elvinong@thedottsolutions.com"></div>
                    </section>
                    <section><label for="text_field">Admin Password</label>
                        <div><input type="text" name="admin_pwd" id="admin_pwd" placeholder="Please input a Superadmin Password" required="required" value="58030008"></div  >
                    </section>
                </fieldset>
                
				<button id="save_setting" type="submit" class="btn i_triangle_double_right green icon next submit">NEXT</button>
            </form>
		</div>
			</section>

<script>

 $("#logo_upload").wl_File({
	 url: "<?=base_url('en/configurator/upload')?>",
	 onFinish: function(e, data){		
	 	 // console.log(data);
		 //console.log(data.result);
		 
		 var obj = eval("(" + data.result + ')'); 
		 
	 	 if(data.textStatus == "success") {						
			 
			//console.log(obj[0].thumbnail_url); 
		 	$("#logo_url").val(obj[0].url);
		 } else {
			 alert("logo upload failure");
		 }
	 }
 });

  $(document).ready(function() {
		 $('#test_db_connect').click(function(){
 			 var db_name = $('#db_name').val();
			 var db_usr = $('#db_usr').val();
			 var db_passwd = $('#db_passwd').val();
			 var db_host = $('#db_host').val();
			$.ajax({
				type: "POST",
				url: "<?=base_url('en/configurator/db_connect_test')?>",
				data: {
					"db_name": db_name,
					"db_usr": db_usr,
					"db_passwd": db_passwd,
					"db_host": db_host
				}
			}).done(function(result){
				if(result == 1){
					$.wl_Alert('Connection Established', 'alert success', '#connect_success');
				}else{
					$.wl_Alert('Connection cannot be Established', 'alert i_access_denied red', '#connect_failed');
				}
			});
		 });
		 // $('#save_setting').click(function(){
		 // 	var setting_data = form2js('setting_form', '.', true);
		 // 	console.log(setting_data);
			// $.ajax({
			// 	url: '<?=base_url("en/configurator/save_config")?>',
			// 	type: 'POST',
			// 	data: {
			// 		'section': 'setting',
			// 		'setting': JSON.stringify(setting_data)
			// 	}
			// }).done(function(result){
			// 	console.log(result);
			// });
		 // });
  });
  
</script>