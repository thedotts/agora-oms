				<div id="pageoptions">
		</div>

			<header>
		<div id="logo">
			<a href="dashboard.html">ERP Configurator</a>
		</div>

				<nav>
			<ul id="nav">
				<li class="i_house"><a href="<?=base_url('en/configurator/dashboard')?>"><span>Start</span></a></li>
				<li class="i_cog"><a href="<?=base_url('en/configurator/settings')?>"><span>Setting</span></a></li>
				<li class="i_table"><a href="<?=base_url('en/configurator/data')?>"><span>Data Setup</span></a></li>
				<li class="i_users"><a href="<?=base_url('en/configurator/roles')?>"><span>Roles Setup</span></a></li>
				<li class="i_breadcrumb"><a href="<?=base_url('en/configurator/workflow')?>"><span>Workflow Setup</span></a></li>
                <li class="i_breadcrumb"><a href="<?=base_url('en/configurator/workflow_order')?>"><span>Workflow Order Setup</span></a></li>
				<li class="i_key"><a href="<?=base_url('en/configurator/access')?>"><span>Access Control Setup</span></a></li>
				<li class="i_facebook_like"><a href="<?=base_url('en/configurator/complete')?>"><span>Complete</span></a></li>
			</ul>
		</nav>
		
			
		
		<section id="content">
		
		<div class="g12 nodrop">
			<h1>START</h1>
			<p>Follow the Steps to Setup your Operations Management System.</p>
			<p>Please ensure that your data schema has been loaded to the database before proceeding.</p>
            <a href="<?=base_url('en/configurator/settings')?>"><button class="btn i_triangle_double_right green icon next">NEXT</button></a>
		</div>	

			
			</section>