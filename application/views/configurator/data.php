<div id="pageoptions">
		</div>

			<header>
		<div id="logo">
			<a href="dashboard.html">ERP Configurator</a>
		</div>

				<nav>
			<ul id="nav">
				<li class="i_house"><a href="<?=base_url('en/configurator/dashboard')?>"><span>Start</span></a></li>
				<li class="i_cog"><a href="<?=base_url('en/configurator/settings')?>"><span>Setting</span></a></li>
				<li class="i_table"><a href="<?=base_url('en/configurator/data')?>"><span>Data Setup</span></a></li>
				<li class="i_users"><a href="<?=base_url('en/configurator/roles')?>"><span>Roles Setup</span></a></li>
				<li class="i_breadcrumb"><a href="<?=base_url('en/configurator/workflow')?>"><span>Workflow Setup</span></a></li>
                <li class="i_breadcrumb"><a href="<?=base_url('en/configurator/workflow_order')?>"><span>Workflow Order Setup</span></a></li>
				<!--<li class="i_key"><a href="<?=base_url('en/configurator/access')?>"><span>Access Control Setup</span></a></li>-->
				<li class="i_facebook_like"><a href="<?=base_url('en/configurator/complete')?>"><span>Complete</span></a></li>
			</ul>
		</nav>

		<section id="content">

		<div class="g12 nodrop">
			<h1>DATA RELATION SETUP</h1>
            <p>This step allows you to classify your table, fields and data lists (ie Checkbox, List, Radio).</p>
            <p>Field Type: Normal - Char, Int; Extended - List, Radio, Checkbox</p>
            <!--<form id="form">-->
			<fieldset>
            <section>
            <label> Select Table </label>
            <div><select id="getTable">
            <?php
            $linked_tabs = array();
			$keys = array_keys($schema);
			foreach($keys as $s){ ?>
				<option value="<?= $s ?>"><?= $s ?></option>
			<?php } ?>
			</select></div>
            </section>
            </fieldset>
            <button id="view_table" class="submit" name="submitbuttonname">View Table</button>
            <!--</form>-->
            <hr>
            <form id="data_form" action="<?=base_url('en/configurator/save_config')?>" method="post" autocomplete="off">
                 <input type="hidden" name="section" value="data">
            <?php foreach($keys as $s){
				$table_keys = array_keys($schema[$s]);
			?>
                <fieldset id="tab_<?= $s ?>" class="hidden">
                    <label><?= ucfirst($s) ?> Table</label>
                    <table>
                    <thead>
                        <tr>
                            <th>Field Name</th><th>Field Type</th><th>Link To (Table)</th>
                        </tr>
                    </thead>
                    <tbody>
						<?php foreach($table_keys as $t){
                        	if($t == 'header' || $t == "footer" || $t == "primary_key"){
								continue;
							}
                            if(preg_match("'_date'", $t)){
                                continue;
                            }
                            $this_type;
                            $this_length;
                            $t_match;
                            $match_result;
                            if(preg_match("/(.*)_id/", $t, $t_match)){
                                $this_type = "Foreign Keys";
                            }else if(preg_match("/.*\((.*)\)/", $schema[$s][$t]['type'], $match_result)){
                                if($match_result[1] == "1"){
                                    $this_type = "Extended";
                                }else{
                                    $this_type = "Normal";
                                }
                            }else if(preg_match("/datetime/", $schema[$s][$t]['type'])){
                                $this_type = "Disable";
                            }
						?>
                        <tr>
                            <td><?= $t ?></td>
                            <td><select class='field_type'>
                            <!--<option selected><?= $schema[$s][$t]['type'] ?></option>-->
                            <option <?= ($this_type == "Normal")?"selected":""?> >Normal</option>
                            <option <?= ($this_type == "Extended")?"selected":""?> >Extended</option>
                            <option <?= ($this_type == "Foreign Keys")?"selected":""?> >Foreign Keys</option>
                            <option <?= ($this_type == "Disable")?"selected":""?> >Disable</option></select>
                            </td>
                            <?php
                                $special_column = 0;
                                if($t == 'sales_person_id'){
                                    $link_to = 'employee';
                                    $special_column = 1;
                                }else if($t == 'requestor_id'){
                                    $link_to = 'employee';
                                    $special_column = 1;
                                }
                            ?>
                            <td>
                                <select class="link_to" name="<?= $t . '@'. $s ?>" id="<?= $t . '@'. $s ?>">
                                    <?php if($this_type == "Foreign Keys"){ ?>
                                        <option value=""> --Please select-- </option>
                                        <?php
                                            if($special_column == 1){
                                                $linked_tabs[$link_to][$s]++;
                                            echo "<option value=\"$link_to\" selected>$link_to</option>";
                                            }
                                        ?>
                                        <?php foreach($keys as $s2){ ?>
                                            <option value="<?= $s2?>" <?= ($t_match[1] == $s2)?"selected":""?>> <?= $s2 ?></option>
                                        <?php
                                                if($t_match[1] == $s2){
                                                    $linked_tabs[$s2][$s]++;
                                                }
                                            }
                                        ?>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
						<?php }?>
                    </tbody>
					</table>
                </fieldset>
			<?php } ?>
                <!--linked tables-->
                <?php
                $linked_table_name = array_keys($linked_tabs);
                foreach($linked_table_name as $linked_l){
                        $linked_table_classes = implode(' ',array_keys($linked_tabs[$linked_l]));
                        $linked_table_keys = array_keys($schema[$linked_l]);
                ?>
                <fieldset id="linked_<?= $linked_l ?>" class="hidden <?= $linked_table_classes ?>">
                    <label>Link to: <?= ucfirst($linked_l) ?> Table</label>
                    <table>
                    <thead>
                        <tr>
                            <th>Field Name</th><th>Field Type</th><th>Link To (Table)</th>
                        </tr>
                    </thead>
                   <tbody>
                        <?php foreach($linked_table_keys as $linked_t){
                            if($linked_t == 'header' || $linked_t == "footer" || $linked_t == "primary_key"){
                                continue;
                            }
                            if(preg_match("'_date'", $linked_t)){
                                continue;
                            }
                            $linked_this_type;
                            $linked_this_length;
                            $linked_t_match;
                            $linkedmatch_result;
                            if(preg_match("'(.*)_id'", $linked_t, $linked_t_match)){
                                $linked_this_type = "Foreign Keys";
                            }else if(preg_match("'.*\((.*)\)'", $schema[$linked_l][$linked_t]['type'], $linked_match_result)){
                                if($linked_match_result[1] == "1"){
                                    $linked_this_type = "Extended";
                                }else{
                                    $linked_this_type = "Normal";
                                }
                            }else if(preg_match("'datetime'", $schema[$linked_l][$linked_t]['type'])){
                                $linked_this_type = "Disable";
                            }
                        ?>
                        <tr>
                            <td><?= $linked_t ?></td>
                            <td><select>
                            <option selected><?= $schema[$linked_l][$linked_t]['type'] ?></option>
                            <option <?= ($linked_this_type == "Normal")?"selected":""?> >Normal</option>
                            <option <?= ($linked_this_type == "Extended")?"selected":""?> >Extended</option>
                            <option <?= ($linked_this_type == "Foreign Keys")?"selected":""?> >Foreign Keys</option>
                            <option <?= ($linked_this_type == "Disable")?"selected":""?> >Disable</option></select>
                            <?php
                                $linked_special_column = 0;
                                if($linked_t == 'sales_person_id'){
                                    $linked_link_to = 'employee';
                                    $linked_special_column = 1;
                                }else if($linked_t == 'requestor_id'){
                                    $linked_link_to = 'employee';
                                    $linked_special_column = 1;
                                }
                            ?>
                            <td>
                                <select disabled>
                                    <?php if($linked_this_type == "Foreign Keys"){ ?>
                                        <option value=""> --Please select-- </option>
                                        <?php
                                            if($linked_special_column == 1){
                                            echo "<option value=\"$linked_link_to\" selected>$linked_link_to</option>";
                                            }
                                        ?>
                                        <?php foreach($keys as $linked_s2){ ?>
                                            <option value="<?= $linked_s2?>" <?= ($linked_t_match[1] == $linked_s2)?"selected":""?>> <?= $linked_s2 ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                    </table>
                </fieldset>
                <?php
                } //linked_tabs
                ?>
                <button id="save_data" class="btn i_triangle_double_right green icon next">NEXT</button>
            </form>
		</div>

			</section>
<script>
$(document).ready(function(){
	//view table structure
	$('#view_table').click(function(){
        var tab_list = $('fieldset[id^="tab"]');
        tab_list.addClass("hidden");
		var this_tab = $('#getTable').val();
		$('#tab_'+ this_tab).removeClass("hidden");
        //show linked table
        var linked_tab = $('fieldset[id^="linked"]');
        var linked_tab_hasclass = $('fieldset.' + this_tab);
        linked_tab.addClass("hidden");
        linked_tab_hasclass.removeClass('hidden');
	});
    $('.field_type').change(function(){
        var this_val = $(this).val();
        var this_select = $(this).closest('td').next('td').find('select');
        $(this_select).parent().find('span').text('--Please select--');
        if(this_val == 'Foreign Keys'){
            $(this_select).append('<option value=""> --Please select-- </option>')
            <?php foreach($keys as $s2){ ?>
             $(this_select).append('<option value="<?= $s2?>"> <?= $s2 ?></option>');
            <?php } ?>
        }else{
            //var that_id = this_select.attr('id');
            //var that_name = this_select.attr('name');
            $(this_select).parent().find('span').text('');
            //$(this_select).find('option').removeAttr("selected");
            //console.log(this_select.find('option :selected'));
            //$(this_select)[0].selectedIndex = -1;
            //console.log($(this_select)[0].selectedIndex);
            $(this_select).find('option').remove();
            //this_select.html('<select class="link_to" name="'+ that_name +'" id="' + that_id +'"></select>');
        }

    });
    $('#save_data').click(function(){
        var data = form2js('data_form', '.', true);
        $.ajax({
            url: '<?=base_url("en/configurator/save_config")?>',
            type: 'POST',
            data: {
                'section': 'data',
                'data': JSON.stringify(data)
            },
			complete: function(xhr, textStatus){
				//console.log(xhr);
				if(xhr.status != 200) {					
					alert(xhr.responseText);	
				} else {					
					window.location.assign("/en/configurator/roles");					 
				}	
			}
        });

    });
});
</script>