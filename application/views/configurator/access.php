<div id="pageoptions">
		</div>

			<header>
		<div id="logo">
			<a href="<?=base_url('')?>">ERP Configurator</a>
		</div>

				<nav>
			<ul id="nav">
				<li class="i_house"><a href="<?=base_url('en/configurator/dashboard')?>"><span>Start</span></a></li>
				<li class="i_cog"><a href="<?=base_url('en/configurator/settings')?>"><span>Setting</span></a></li>
				<li class="i_table"><a href="<?=base_url('en/configurator/data')?>"><span>Data Setup</span></a></li>
				<li class="i_users"><a href="<?=base_url('en/configurator/roles')?>"><span>Roles Setup</span></a></li>
				<li class="i_breadcrumb"><a href="<?=base_url('en/configurator/workflow')?>"><span>Workflow Setup</span></a></li>
                <li class="i_breadcrumb"><a href="<?=base_url('en/configurator/workflow_order')?>"><span>Workflow Order Setup</span></a></li>
				<li class="i_key"><a href="<?=base_url('en/configurator/access')?>"><span>Access Control Setup</span></a></li>
				<li class="i_facebook_like"><a href="<?=base_url('en/configurator/complete')?>"><span>Complete</span></a></li>
			</ul>
		</nav>
		<section id="content">
		<div class="g12 nodrop">
			<h1>ACCESS CONTROL SETUP</h1>
            <p>This step allows you to define all access control in the system.</p>
            <p>C,R,U,D: full, <br/>
               C,R,U: create/update/view, <br/> 
               C,OR,OU: create/update/view own record only, <br/> 
               R: read only<br/>
               C,OR: create/view own record only<br/>
               X: no access
		    </p>		
            <form id="access_form" action="<?=base_url('en/configurator/save_config')?>" method="post" autocomplete="off">
                <fieldset>
                    <label>Role Table</label>
                    <table>
                    <thead>
                        <tr>
                            <th>Role</th><?php                            
                            foreach($milestone as $km=>$m){
                            ?>
                            <th><?=$m?></th>
							<?php
							}
							?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $r_cnt = 0;
                    foreach($roles as $r){ ?>
                        <tr>
                            <td><?= ucfirst($r['name']) ?></td>
                            <input type="hidden" value="<?= $r['id'] ?>" name="access[<?= $r_cnt ?>].id">
                            <?php                            
                            foreach($milestone as $km=>$m){
                            ?>
                                <td><select name="access[<?= $r_cnt ?>].<?= $km ?>">
                                <?php
                                //$c_cnt = 1;
                                foreach($control as $k=>$c){
                                ?>
                                <option value="<?=$k?>" <?=isset($default_roles_privileges[$r['id']][$km])&&$default_roles_privileges[$r['id']][$km]==$k?'selected="selected"':''?>><?=$c?></option>
                                <?php 
                                 } //end of control?>
                            </select></td>
                            <?php } // end of milestone?>

                        </tr>
                    <?php $r_cnt ++; } //end of roles?>
                    </tbody>
					</table>
                    
                    <table>
                    <thead>
                        <tr>
                            <th>Role</th><?php                            
                            foreach($milestone2 as $km=>$m){
                            ?>
                            <th><?=$m?></th>
							<?php
							}
							?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $r_cnt = 0;
                    foreach($roles as $r){ ?>
                        <tr>
                            <td><?= ucfirst($r['name']) ?></td>
                            <input type="hidden" value="<?= $r['id'] ?>" name="access[<?= $r_cnt ?>].id">
                            <?php                            
                            foreach($milestone2 as $km=>$m){
                            ?>
                                <td><select name="access[<?= $r_cnt ?>].<?= $km ?>">
                                <?php
                                //$c_cnt = 1;
                                foreach($control as $k=>$c){
                                ?>
                                <option value="<?=$k?>" <?=isset($default_roles_privileges[$r['id']][$km])&&$default_roles_privileges[$r['id']][$km]==$k?'selected="selected"':''?>><?=$c?></option>
                                <?php 
                                 } //end of control?>
                            </select></td>
                            <?php } // end of milestone?>

                        </tr>
                    <?php $r_cnt ++; } //end of roles?>
                    </tbody>
					</table>
                    
                    <table>
                    <thead>
                        <tr>
                            <th>Role</th><?php                            
                            foreach($milestone3 as $km=>$m){
                            ?>
                            <th><?=$m?></th>
							<?php
							}
							?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $r_cnt = 0;
                    foreach($roles as $r){ ?>
                        <tr>
                            <td><?= ucfirst($r['name']) ?></td>
                            <input type="hidden" value="<?= $r['id'] ?>" name="access[<?= $r_cnt ?>].id">
                            <?php                            
                            foreach($milestone3 as $km=>$m){
                            ?>
                                <td><select name="access[<?= $r_cnt ?>].<?= $km ?>">
                                <?php
                                //$c_cnt = 1;
                                foreach($control as $k=>$c){
                                ?>
                                <option value="<?=$k?>" <?=isset($default_roles_privileges[$r['id']][$km])&&$default_roles_privileges[$r['id']][$km]==$k?'selected="selected"':''?>><?=$c?></option>
                                <?php 
                                 } //end of control?>
                            </select></td>
                            <?php } // end of milestone?>

                        </tr>
                    <?php $r_cnt ++; } //end of roles?>
                    </tbody>
					</table>
                    
                </fieldset>
            </form>
            <button type="submit" id="save_access" class="btn i_triangle_double_right green icon next">NEXT</button>
		</div>
</section>
<script>
$(document).ready(function(){
    <!--get form data-->
    $('#save_access').click(function(){
        var access = form2js('access_form', '.', true);
        $.ajax({
            url: '<?=base_url('en/configurator/save_config')?>',
            type: 'POST',
            data: {
                'section': 'access',
                'access': JSON.stringify(access)
            }
        }).done(function(){
            window.location.assign("/en/configurator/complete");
        });
    });
});
</script>