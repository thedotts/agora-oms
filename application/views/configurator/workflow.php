<div id="pageoptions">
		</div>

			<header>
		<div id="logo">
			<a href="dashboard.html">ERP Configurator</a>
		</div>

				<nav>
			<ul id="nav">
				<li class="i_house"><a href="<?=base_url('en/configurator/dashboard')?>"><span>Start</span></a></li>
				<li class="i_cog"><a href="<?=base_url('en/configurator/settings')?>"><span>Setting</span></a></li>
				<li class="i_table"><a href="<?=base_url('en/configurator/data')?>"><span>Data Setup</span></a></li>
				<li class="i_users"><a href="<?=base_url('en/configurator/roles')?>"><span>Roles Setup</span></a></li>
				<li class="i_breadcrumb"><a href="<?=base_url('en/configurator/workflow')?>"><span>Workflow Setup</span></a></li>
                <li class="i_breadcrumb"><a href="<?=base_url('en/configurator/workflow_order')?>"><span>Workflow Order Setup</span></a></li>
				<!--<li class="i_key"><a href="<?=base_url('en/configurator/access')?>"><span>Access Control Setup</span></a></li>-->
				<li class="i_facebook_like"><a href="<?=base_url('en/configurator/complete')?>"><span>Complete</span></a></li>
			</ul>
		</nav>

		<section id="content">
        
		<div class="g12 nodrop">
			<h1>WORKFLOW SETUP</h1>
            <p>This step allows you to configure the workflow.</p>

            <select id="workflow_status">
				<option value="order">Order</option>
                <option value="packing_list">Packing List</option>
                <option value="delivery_order">Delivery Order</option>
                <option value="invoice">Invoice</option>
            </select>
        
        <table>
        	<thead>
            	<th>status</th>
                <th>status text</th>
            </thead>
        	<tbody id="table_content">
            </tbody>
        </table>
        
        </div>
        
        <div class="g12">
                <button id="save_workflow" class="btn i_triangle_double_right green icon next">NEXT</button>
		</div>

		</section>
        
<script>
var workflow = {'order':{}, 'packing_list':{}, 'delivery_order':{}, 'invoice':{}};

//order
workflow['order'] = {'id':1, 'title':'Order', 'table_name':'order', 'status_json':[]};
workflow['order'].status_json = ["No Approved","Pending Approval By Sales","Completed"];

//packing_list
workflow['packing_list'] = {'id':2, 'title':'Packing List', 'table_name':'packing_list', 'status_json':[]};
workflow['packing_list'].status_json = ["No Approved","Completed"];

//delivery_order
workflow['delivery_order'] = {'id':3, 'title':'Delivery Order', 'table_name':'delivery_order', 'status_json':[]};
workflow['delivery_order'].status_json = ["No Approved","Pending Upload Confirm File By Admin","Completed"];

//invoice
workflow['invoice'] = {'id':4, 'title':'Invoice', 'table_name':'invoice', 'status_json':[]};
workflow['invoice'].status_json = ["No Approved","Completed"];

var last_flow = '';

$('#workflow_status').on('change', function() {

	if(last_flow != ''){
		
		var complete = 0;
		var status_text = new Array();
		$('.status_text').each(function(){
			status_text.push($(this).val());
			if($(this).val() == 'Completed'){
				complete = 1;
			}
		})
		
		if(complete != 1){
			alert('Status Completed is Necessary!');
			$('#workflow_status').val(last_flow);
			return false;
		}
		
		workflow[last_flow].status_json = status_text;
		
	}

	last_flow = this.value;
	var flow = this.value;
	var tmp_html = '';
	
	for(var i=0;i<workflow[flow].status_json.length;i++){
		
		tmp_html += '<tr>';
		tmp_html += '<td><input type="text" value="'+i+'" readonly></td>';
		tmp_html += '<td><input class="status_text" type="text" value="'+workflow[flow].status_json[i]+'"></td>';
		tmp_html += '</tr>';
		
	}
	$('#table_content').html('');
	$('#table_content').append(tmp_html);
	
	console.log(JSON.stringify(workflow));

});

$(document).ready(function(){
	
	console.log(JSON.stringify(workflow));
	
    $('#save_workflow').click(function(){
		
		if(last_flow != ''){
		
		var complete = 0;
		var status_text = new Array();
		$('.status_text').each(function(){
			status_text.push($(this).val());
			if($(this).val() == 'Completed'){
				complete = 1;
			}
		})
		
		if(complete != 1){
			alert('Status Completed is Necessary!');
			$('#workflow_status').val(last_flow);
			return false;
		}
		
		workflow[last_flow].status_json = status_text;
		
		}
		
        $.ajax({
            url: '<?=base_url("en/configurator/save_config")?>',
            type: 'POST',
            data: {
                'section': 'workflow',
                'workflow': JSON.stringify(workflow)
            }
        }).done(function() {
            window.location.assign("/en/configurator/workflow_order");
        });
    });
	
});
</script>