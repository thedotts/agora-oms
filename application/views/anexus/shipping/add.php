<div id="page-wrapper">
        	<form method="post" action="<?=base_url('en/anexus/'.$group_name.'/'.$model_name.'_submit')?>" onsubmit="return validate();">
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-pencil-square-o fa-fw"></i> Create Shipper
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Shipper Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Shipper Name *</label>
                                    <input class="form-control" name="shipper_name" value="<?=$mode=='Edit'?$result['shipper_name']:''?>">                                    
                                </div>
                                <div class="form-group">
                                    <label>Shipping Zone Name*</label>
                                    <input class="form-control" name="shipping_zone" value="<?=$mode=='Edit'?$result['shipping_zone']:''?>">                                    
                                </div>
                                <div class="form-group">
                                    <label>Country *</label>
                                    <select name="country[]" class="form-control" multiple>
                                        <?php
										foreach($country_list as $k=>$v) {
										?>
                                        <option value="<?=$k?>" <?=$mode=='Edit'&&in_array($k, explode(",",$result['country']))?'selected="selected"':''?>><?=$v?></option>
                                        <?php
										}
										?>  
                                    </select>
                                </div>                                
                                <div class="form-group">
                                    <label>Enabled</label>
                                    <input type="checkbox" name="enabled" value="1" <?=$mode=='Edit'&&$result['enabled']?'checked="checked"':''?>>
                                </div>
                                <div class="form-group">
                                    <label>Shipping Type *</label>
                                    <select name="shipping_type" class="form-control">
                                        <?php
										foreach($shipping_type as $k=>$v) {
										?>
                                        <option value="<?=$k?>" <?=$mode=='Edit'&&$k==$result['shipping_type']?'selected="selected"':''?>><?=$v?></option>
                                        <?php
										}
										?>  
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Currency *</label>
                                    <select name="currency" class="form-control">
                                        <?php
										foreach($currency_list as $k=>$v) {
										?>
                                        <option value="<?=$k?>" <?=$mode=='Edit'&&$k==$result['currency']?'selected="selected"':''?>><?=$v?></option>
                                        <?php
										}
										?>  
                                    </select>
                                </div>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th width="50%">Weight</th>
                                            <th>Cost</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody">
                                    </tbody>
                                </table>
                                <a class="btn btn-primary pull-right"href="javascript:addRow();">+</a>
                         </div>

                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            
            
            <div class="row">
                <div class="col-lg-12">
                    
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->

<script>
var mode ='<?=$mode?>';
var cost_json = <?=$mode=='Edit'?$result['cost_json']:'[]'?>;

$(document).ready(function(e) {
	//$("#dob").datepicker({"dateFormat":"dd/mm/yy"}); 
	//$("#expired_date").datepicker({"dateFormat":"dd/mm/yy"});  
	if(mode != 'Edit'){
		addRow();  
	}else{
		for(i=0;i<cost_json.length;i++){
			addRow(); 
		}	
	}
});
 
var rowCounter = 0;
function addRow(){
	
	if(mode == 'Edit' && rowCounter < cost_json.length){
	
		html = '<tr id="row'+rowCounter+'"><td><input id="weight" name="weight[]" type="text" class="form-control" value="'+cost_json[rowCounter].weight+'"></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input id="cost" name="cost[]" type="text" class="form-control" value="'+cost_json[rowCounter].cost+'"></div></td><td><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="delRow('+rowCounter+')"><i class="fa fa-times"></i></button></td></tr>';
	
	}else{
	
		html = '<tr id="row'+rowCounter+'"><td><input id="weight" name="weight[]" type="text" class="form-control" value=""></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input id="cost" name="cost[]" type="text" class="form-control" value=""></div></td><td><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="delRow('+rowCounter+')"><i class="fa fa-times"></i></button></td></tr>';
	
	}
	rowCounter++;
	$('#tbody').append(html);
}

function delRow(id){
	$('#row'+id).remove();
}

function validate(){
		
	
	
}
</script>