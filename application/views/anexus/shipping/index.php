

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-user fa-fw"></i> Manage Shipping
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input id="q" class="form-control" placeholder="Search" onblur="filter()" value="<?=$q?>">
                                </div>
                                <!-- /.form-group -->
                            </form>
                            <script>
							function filter(){								
								var q = $("#q").val();
								if(q == '') {
									q = 'ALL';	
								}								
								location.href='<?=base_url('en/anexus/'.$group_name.'/'.$model_name)?>/'+q;
							}
							</script>
                        </div>
                		<!-- /.col-lg-3 -->
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <a href="<?=base_url('en/anexus/'.$group_name.'/'.$model_name.'_add')?>"><button class="btn btn-default btn-sm" type="button">Create Shipper </button></a><p></p>
                    <div class="panel panel-default">
                    	<?php
								if(!empty($results)) {
								?>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Shipper Name</th>
                                            <th>Shipping Zone</th>
                                            <th>Shipping Rate</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
										foreach($results as $v) {
										?>
                                        <tr>
                                            <td><?=$v['id']?></td>
                                            <td><?=$v['shipper_name']?></td>
                                            <td><?=$v['shipping_zone']?></td>
                                            <td><?=$v['shipping_type']?></td>                                            
                                            <td>
                                                                                        	                             
                                                
                                                <a class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button" href="<?=base_url('en/anexus/'.$group_name.'/'.$model_name.'_edit/'.$v['id'])?>"><i class="fa fa-edit"></i></a>
                                                
                                                <a class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" href="javascript: deleteData('<?=base_url('en/anexus/'.$group_name.'/'.$model_name.'_del/'.$v['id'])?>')"><i class="fa fa-times"></i></a>
                                                
                                        </tr>
                                        <?php
										}
										?>                                        
                                    </tbody>
                                </table>
                                <?php
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
								?>
          </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                    <?=$paging?>
                    
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
             <script>
		function deleteData(url){
			var c = confirm("Are you sure you want to delete?");
			if(c){
				location.href=url;	
			}
		}
		</script>

        