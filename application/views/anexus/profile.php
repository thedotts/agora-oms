<div id="page-wrapper">
        	<form method="post" action="<?=base_url('en/agora/profile_submit')?>" onsubmit="return validate();">
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>            
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-pencil-square-o fa-fw"></i> Update Profile
                	</h1>
                </div>
                <!-- /.col-lg-12 -->                
            </div>
            
            <?php
				if($status=='success') {
				?>
                <div class="alert alert-success">Profile updated successfully</div>
                <?php	
				}
				?>
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Account Information
                        </div>
                        <div class="panel-body">
                        		<?php if(!in_array(7,$userdata['role_id'])){?>
                                <div class="form-group">
                                	
                                    <label>Employee No.</label>
                                    <input class="form-control" disabled value="<?=$mode=='Edit'?$result['employee_no']:''?>">
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
								<?php }?>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input name="email" class="form-control" value="<?=$mode=='Edit'?$result['email']:''?>" readonly="readonly">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" id="pwd" name="password" class="form-control">
                                    <p class="help-block"><i>Leave Blank if no change</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" id="copwd" name="copassword" class="form-control">
                                    <p class="help-block"><i>Leave Blank if no change</i></p>
                                </div>                                
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    
                    <div class="panel panel-primary" style="display:none">
                    	<div class="panel-heading">
                            Bank Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Bank Name</label>
                                    <input name="bankname" class="form-control" value="<?=$mode=='Edit'?$result['bankname']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Bank Code (4 digits)</label>
                                    <input name="bank_code" class="form-control" value="<?=$mode=='Edit'?$result['bank_code']:''?>" maxlength="4">
                                </div>
                                <div class="form-group">
                                    <label>Branch Code (3 digits)</label>
                                    <input name="bank_branch" class="form-control" value="<?=$mode=='Edit'?$result['bank_branch']:''?>" maxlength="3">
                                </div>
                                <div class="form-group">
                                    <label>Account No.</label>
                                    <input name="account_no" class="form-control" value="<?=$mode=='Edit'?$result['account_no']:''?>">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
                <div class="col-lg-12" style="display:none">
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Personal Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Full Name *</label>
                                    <input name="full_name" class="form-control" value="<?=$mode=='Edit'?$result['full_name']:''?>" required="required">
                                </div>
                                <div class="form-group">
                                    <label>IC No. *</label>
                                    <input name="ic_no" class="form-control" value="<?=$mode=='Edit'?$result['ic_no']:''?>" required="required">
                                </div>
                                <div class="form-group">
                                    <label>Date of Birth *</label>
                                    <input id="dob" name="date_of_birth" class="form-control" placeholder="Calendar Input" value="<?=$mode=='Edit'?date("d/m/Y", strtotime($result['date_of_birth'])):''?>" required="required">
                                </div>
                                <div class="form-group">
                                    <label>Home Address</label>
                                    <input name="home_address" class="form-control" value="<?=$mode=='Edit'?$result['home_address']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Contact No. *</label>
                                    <input name="contact_info" class="form-control" value="<?=$mode=='Edit'?$result['contact_info']:''?>" required="required">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    <div class="panel panel-primary" style="display:none">
                    	<div class="panel-heading">
                            Travel Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Passport No.</label>
                                    <input name="passport_no" class="form-control" value="<?=$mode=='Edit'?$result['contact_info']:''?>">
                                </div> 
                                <div class="form-group">
                                    <label>Expiry Date</label>
                                    <input id="expired_date" name="expired_date" class="form-control" value="<?=$mode=='Edit'?date("d/m/Y", strtotime($result['expired_date'])):''?>">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    <div class="panel panel-primary" style="display:none">
                    	<div class="panel-heading">
                            Emergency Contact
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Contact Name *</label>
                                    <input name="emergency_contact_name" class="form-control" value="<?=$mode=='Edit'?$result['emergency_contact_name']:''?>" required="required">
                                </div>
                                <div class="form-group">
                                    <label>Relationship *</label>
                                    <input name="emergency_contact_relationship" class="form-control" value="<?=$mode=='Edit'?$result['emergency_contact_relationship']:''?>" required="required">
                                </div>
                                <div class="form-group">
                                    <label>Contact No. *</label>
                                    <input name="emergency_contact_no" class="form-control" value="<?=$mode=='Edit'?$result['emergency_contact_no']:''?>" required="required">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            
            
            <div class="row">
                <div class="col-lg-12">
                    
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->

<script>

$(document).ready(function(e) {
	$("#dob").datepicker({"dateFormat":"dd/mm/yy"}); 
	$("#expired_date").datepicker({"dateFormat":"dd/mm/yy"});    
});



function validate(){
	
	if($("#pwd").val()!=$("#copwd").val()) {
		alert("Password is not same with confirm password");
		return false;
	}
	
	
}
</script>