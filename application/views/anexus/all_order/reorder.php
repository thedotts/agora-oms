<div id="page-wrapper">
        	<form action="<?=base_url($init['langu'].'/agora/customer/all_order_submit2');?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="mode" value="Add"/>
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" id="item_count" name="item_count" value=""/>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-briefcase fa-fw"></i> Re-Order
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Order Request Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Order No.</label>
                                    <input class="form-control" value="" disabled>
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Order Date *</label>
                                    <input class="form-control" placeholder="Calendar Input" id="order_date" name="order_date" value="">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Customer Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input name="company_name" class="form-control" value="<?=$customer['company_name']?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Attention</label>
                                    <input class="form-control" value="<?=$customer['primary_attention_to']?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control" value="<?=$customer['address']?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Postal Code</label>
                                    <input class="form-control" value="<?=$customer['postal']?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Country</label>
                                    <input class="form-control" value="<?=$customer['country']?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" value="<?=$customer['email']?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Contact No.</label>
                                    <input class="form-control" value="<?=$customer['primary_contact_info']?>" disabled>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
            </div>
            <!-- /.row -->
            
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Items 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                            	<button class="btn btn-default btn-sm" type="button" onclick="add_item()">Add New Product Item</button>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Product Name & Model Name</th>
                                            <th width="200px">Qty Ordered</th>
                                            <th width="200px">Qty Fulfillable</th>
                                            <th width="200px">Qty to Amend *</th>
                                            <th width="80px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody">
                                        
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                        
                        
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Remarks
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <textarea id="remarks" name="remarks" class="form-control"><?=$mode=='Edit'?$result['remark']:''?></textarea>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->
<script>
var product = <?=$product?>;
var item_count = 0;
var product_edit = <?=$result['product']?>;
var mode = '<?=$mode?>';

$( "#order_date" ).datepicker({
	dateFormat:'dd/mm/yy',
});

$('#order_date').datepicker('setDate', 'today');

var option = '<option>-</option>';
for(var i=0;i<product.length;i++){
	option += '<option value="'+product[i].id+'|'+product[i].model_name+'">'+product[i].product_name+' - '+product[i].model_name+'</option>';
}


for(var i=0;i<product_edit.length;i++){
	add_item();
}

function add_item(){
	
	item_count++;
	
	if(mode == 'Edit' && item_count <= product_edit.length){
	
		html = '<tr id="item_'+item_count+'"><td>'+item_count+'</td><td><select id="product'+item_count+'" name="product'+item_count+'" onchange="product_detail('+item_count+')" class="form-control">'+option+'</select></td><td><input id="qty_ord'+item_count+'" name="qty_ord'+item_count+'" type="text" class="form-control" value="0" readonly></td><td><input id="qty_fill'+item_count+'" name="qty_fill'+item_count+'" type="text" class="form-control" value="0" readonly></td><td><input  id="qty_amend'+item_count+'" name="qty_amend'+item_count+'" type="text" class="form-control" value="'+product_edit[item_count-1].quantity+'"></td><td><a id="link'+item_count+'" href="'+'<?=base_url($init['langu'].'/agora/administrator/products_edit/')?>'+'/'+product_edit[item_count-1].product_id+'"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></a><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" onclick="del_item('+item_count+')" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button><input type="hidden" id="old_id'+item_count+'" name="old_id'+item_count+'" value="'+product_edit[item_count-1].id+'"/><input type="hidden" id="is_del'+item_count+'" name="is_del'+item_count+'" value="0"/></td></tr>';
	
	}else{
		
		html = '<tr id="item_'+item_count+'"><td>'+item_count+'</td><td><select id="product'+item_count+'" name="product'+item_count+'" onchange="product_detail('+item_count+')" class="form-control">'+option+'</select></td><td><input id="qty_ord'+item_count+'" name="qty_ord'+item_count+'" type="text" class="form-control" value="0" readonly></td><td><input id="qty_fill'+item_count+'" name="qty_fill'+item_count+'" type="text" class="form-control" value="0" readonly></td><td><input  id="qty_amend'+item_count+'" name="qty_amend'+item_count+'" type="text" class="form-control" value="0"></td><td><a id="link'+item_count+'" href="#"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></a><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" onclick="del_item('+item_count+')" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button><input type="hidden" id="old_id'+item_count+'" name="old_id'+item_count+'" value="0"/><input type="hidden" id="is_del'+item_count+'" name="is_del'+item_count+'" value="0"/></td></tr>';
		
	}
	
	$('#item_count').val(item_count);
	$('#tbody').append(html);
	
	
	//preselect
	if(mode == 'Edit' && item_count <= product_edit.length){  
		$('#product'+item_count).val(product_edit[item_count-1].product_id+'|'+product_edit[item_count-1].model_name);                       
		//product_detail(item_count);
	}
	
}

function del_item(i){
	$('#item_'+i).hide();
	$('#is_del'+i).val(1);
}

function product_detail(i){
	product1 = $('#product'+i).val();
	product2 = product1.split('|');
	product_id = product2[0];
	console.log(product_id);
	if(product1 != '-'){
		for(var j=0;j<product.length;j++){
			if(product[j].id == product_id){
				document.getElementById('link'+i).href='<?=base_url($init['langu'].'/agora/administrator/products_edit/')?>'+'/'+product_id; 
				break;
			}
		}
	}else{
		document.getElementById('link'+i).href='#'; 
	}
	
}

</script>