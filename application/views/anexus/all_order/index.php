<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-briefcase fa-fw"></i> All Orders
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input id="q" class="form-control" placeholder="Search" value="<?=$q?>" onblur="filter();">
                                </div>
                                <!-- /.form-group -->
                            </form>
                        </div>
                		<!-- /.col-lg-3 -->
                        <script>
							function filter(){
								var q = $("#q").val();
								if(q == '') {
									q = 'ALL';	
								}								
								location.href='<?=base_url('en/agora/'.$group_name.'/'.$model_name)?>/'+q;
							}
							</script>
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default hidden-xs">
                            <div class="table-responsive">
                            	<?php
								if(!empty($results)) {
								?>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Order No.</th>
                                            <th>Order Date/Time</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
										foreach($results as $k => $v) {
										?>
                                        <tr>
                                            <td><?=($k+1+(12*($page-1)))?></td>
                                            <td><?=$v['order_no']?></td>
                                            <td><?=date('d/m/Y',strtotime($v['order_date']))?></td>
                                            <td><?=$v['status']<2?(isset($status_list[$v['status']])?$status_list[$v['status']]:''):$v['status_text']?></td>
                                            <td>
                                                <button onclick="reOrder(<?=$v['id']?>)" class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Re-order Items" type="button"><i class="fa fa-share-square-o"></i></button>
                                                <a href="<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_edit/'.$v['id'])?>"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="<?=$v['status']==1?'Edit':'View'?>"" type="button"><i class="fa fa-<?=$v['status']==1?'edit':'folder-open-o'?>""></i></button></a>
                                                <a href="javascript: deleteData('<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_del/'.$v['id'])?>')"><button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button></a>
                                             </td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                                 <?php
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
								?>
                      </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                    <!-- Data list (Mobile) -->
                        <div class="mobile-list visible-xs">
                        	<?php
								if(!empty($results)) {
								?>
                        	<table class="table table-striped table-bordered table-hover">
                            	<?php										
										foreach($results as $k=>$v) {
									?>
                            	<tr>
                                	<td>
                                    	<div class="row mobile-list-header">
                                            <div class="col-xs-12"><strong>#<?=($k+1)?></strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Order No:</strong></div>
                                            <div class="col-xs-8"><?=$v['order_no']?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Status:</strong></div>
                                            <div class="col-xs-8"><?=$v['status']<2?(isset($status_list[$v['status']])?$status_list[$v['status']]:''):$v['status_text']?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Action:</strong></div>
                                            <div class="col-xs-8"><button onclick="reOrder(<?=$v['id']?>)" class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Re-order Items" type="button"><i class="fa fa-share-square-o"></i></button>
                                                <a href="<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_edit/'.$v['id'])?>"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="<?=$v['status']==1?'Edit':'View'?>"" type="button"><i class="fa fa-<?=$v['status']==1?'edit':'folder-open-o'?>""></i></button></a>
                                                <a href="javascript: deleteData('<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_del/'.$v['id'])?>')"><button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button></a></div>
                                        </div>
                                    </td>
                                </tr>
                                <?php											
										}
									?>
                            </table>
                            <?php
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
								?>
                                                       
                        </div>
                    
                    <?=$paging?>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
        
        
        <script>
		var alert_v = <?=$alert?>;
		
		switch(alert_v){
			case 1:
				alert('Record has been created successfully');
				break;
			case 2:
				alert('Record has been updated successfully');
				break;
			case 3:
				alert('Record has been deleted successfully');
				break;
		}
		
		function deleteData(url){
			var c = confirm("Are you sure you want to delete?");
			if(c){
				location.href=url;	
			}
		}
		
		function reOrder(orderId){
			
			$.ajax({
			  method: "POST",
			  url: "<?=base_url($init['langu'].'/agora/customer/cart_reorder')?>",
			  data: { "orderId": orderId },
			  dataType: "JSON"
			})
			.done(function(r) {
				if(r.status == 'OK'){
					$('#cart').html('');
			
					var html = '';
					for(var x=0;x<r.data.length;x++){
						html +='<li><a href="#"><div><strong>'+r.data[x].name+'</strong><span class="pull-right text-muted"><em>Qty: '+r.data[x].qty+'</em></span><div>'+r.data[x].model_name+'</div></div></a></li>';
					}
					url ='';
					
					html+='<li class="divider"></li><li><a class="text-center" href="'+'<?=base_url($init['langu'].'/agora/customer/cart')?>'+'"><strong>Shopping Cart</strong><i class="fa fa-angle-right"></i></a></li>';
					
					$('#cart').append(html);
					alert('Order has been added to cart');
					
				}
				
			});

		}
		
		
		//enter search
		$(document).ready(function() {
		  $(window).keydown(function(event){
			if(event.keyCode == 13) {
			  event.preventDefault();
			  q.blur();
			  return false;
			}
		  });
		});
		</script>