<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-briefcase fa-fw"></i> <?=$head_title?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                       		<!-- Button trigger modal -->
                            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal"<?=$mode=='Edit'?'disabled':''?>>
                                Search from Quotation
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Search Pending Service Request</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Search</label>
                                                <input class="form-control" placeholder="By Company Name or Reference No." id="search" name="search">
                                            </div>
                                            <table id="quotation_table" class="table table-striped table-bordered table-hover" id="dataTables">
                                            </table>
                                            <div id="pagination"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <form action="<?=base_url($init['langu'].'/anexus/sales/service_request_submit');?>" method="post">
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" id="customer_id" name="customer_id" value="<?=$mode=='Edit'?$result['customer_id']:''?>"/>
            <input type="hidden" id="customer_name" name="customer_name" value="<?=$mode=='Edit'?$result['customer_name']:''?>"/>
            <input type="hidden" id="requestor_id" name="requestor_id" value="<?=$mode=='Edit'?$result['requestor_id']:$staff_info['id']?>"/>
            <input type="hidden" id="job_id" name="job_id" value="<?=isset($job_id)?$job_id:0?>"/>
            <div class="row">
                
        	
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Service Request Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>SR Serial No.</label>
                                    <input class="form-control" value="<?=$mode=='Edit'?$result['service_serial']:''?>" readonly>
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Date of Requisition *</label>
                                    <input id="dateRequisition" name="dateRequisition" class="form-control" placeholder="Calendar Input" value="<?=$mode=='Edit'?date("d/m/y", strtotime($result['requesting_date'])):''?>">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                       
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Sales Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Customer Purchase Order Ref</label>
                                    <input id="purchase_order_ref" name="purchase_order_ref" class="form-control" value="<?=$mode=='Edit'?$result['purchase_order_ref']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Sales Order Ref</label>
                                    <input id="salesOrderRef" name="salesOrderRef" class="form-control" readonly value="<?=$mode=='Edit'?$result['sale_order_ref']:''?>">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6"> 
                
                
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Requestor Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Requestor Name</label>
                                    <input id="requestorName" name="requestorName" class="form-control" value="" disabled>
                                    <input type="hidden" id="requestorId" name="requestorId" class="form-control" value="<?=$mode=='Edit'?$result['requestor_id']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Employee Ref</label>
                                    <input id="employeeRef" name="employeeRef" class="form-control" value="<?=$mode=='Edit'?$result['employee_ref']:''?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Department</label>
                                    <input id="department" name="department" class="form-control" value="<?=$mode=='Edit'?$department_list[$result['department_id']]:''?>" disabled>
                                    <input type="hidden" id="departmentId" name="departmentId" class="form-control" value="<?=$mode=='Edit'?$result['department_id']:''?>">
                                </div>
                                <div class="form-group">
                                   	<label>Service Request Form</label>
                                    <div class="radio">
                                        <label> 
                                            <input type="radio" name="service-request" id="optionsRadios1" value="1" <?=$mode=='Edit'?($result['service_request_form']==1?'checked':''):''?>>Yes
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="service-request" id="optionsRadios2" value="0" <?=$mode=='Edit'?($result['service_request_form']==0?'checked':''):''?>>No
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                   	<label>Consignee</label>
                                    <div class="radio">
                                        <label> 
                                            <input type="radio" name="consignee" id="optionsRadios1" value="anexus" <?=$mode=='Edit'?($result['consignee']=='anexus'?'checked':''):''?>>Anexus
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="consignee" id="optionsRadios2" value="direct" <?=$mode=='Edit'?($result['consignee']=='direct'?'checked':''):''?>>Direct
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                   	<label>Uncrating Service Required</label>
                                    <div class="radio">
                                        <label> 
                                            <input type="radio" name="uncrating" id="optionsRadios1" value="1" <?=$mode=='Edit'?($result['uncrating_service_required']==1?'checked':''):''?>>Yes
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="uncrating" id="optionsRadios2" value="0"  <?=$mode=='Edit'?($result['uncrating_service_required']==0?'checked':''):''?>>No
                                        </label>
                                    </div>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                
                </div>
                <!-- /.col-lg-6 -->
                
            </div>
            <!-- /.row -->
            <hr />
            
            <div class="row">
                <div class="col-lg-12">
                
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Service Items
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>Company Name</td>
                                            <td>
                                    			<input id="companyName" name="companyName" class="form-control" value="<?=$mode=='Edit'?$company_name:''?>" readonly>
                                             </td>
                                         </tr>
                                         <tr>
                                            <td>
                                    			Person In Charge
                                            </td>
                                            <td>
                                                <input id="pic" name="pic" class="form-control" value="<?=$mode=='Edit'?$pic:''?>" readonly>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td>
                                    			Anexus Purchase Order Reg
                                            </td>
                                            <td>
                                                <input id="orderReg" name="orderReg" class="form-control" value="<?=$mode=='Edit'?$purchase_order_reg:''?>">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                    
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Items 
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                            			<button id="addService" class="btn btn-default btn-sm" type="button" onclick="addNewItem()">Add New Item</button>
                                        <input type="hidden" id="addCounter" name="addCounter" value="0" />
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Quantity</th>
                                                    <th>Part Number</th>
                                                    <th>Description</th>
                                                    <th>Hours</th>
                                                    <th>Start Date</th>
                                                    <th>Hours include in Quote?</th>
                                                    <th width="80px">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="table_body">

                                            </tbody>
                                        </table>
                                        
                                    </div>
                                    <!-- /.table-responsive -->
                                    
                                </div>
                                <!-- /.panel-body -->
                                
                            </div>
                            <!-- /.panel -->
                    
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                    <?php if($mode=='Add'){?>
                    
                    	<?php if($btn_type == 'create'){?>
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                        </div>
                    	<?php }?>
                        
                    <?php }else if($mode=='Edit'){?>
                    	<?php switch($btn_type){
							case 'edit':
							?>
                        		<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Submit</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            <?php 
							break;
							case 'approval':
							case 'approval_sendmail':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Update</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'approval_update':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'sendmail_update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-default">Save</button>
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/sales/tender_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/tender_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'send_email':
							?>
                            	<div class="form-group">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/sales/tender_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/tender_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                </div>
                            
                            <?php 
							break;
							case 'upload':
							?>
                            
                            	<div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/sales/tender_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/tender_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file">
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                		<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            
                            <?php 
							break;
							case 'complete':
							?>
                            	<?php if($last_action == 'upload'){?>
                            	<div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/sales/tender_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/tender_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file"> 
                                            <a target="_blank" href="<?=$result['comfirm_file']?>"><?=$result['comfirm_file']?></a>
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Reupload</button>
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            	<?php }?>
                            <?php 
							break;
                        }?>

                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Status
                        </div>
                        <div class="panel-body">
                            <div class="form-group input-group">
							<?php 
                                //如果不是not approved, 也不是 completed 就顯示目前正在等誰
                                if($result['status'] != 0 && $result['status'] != $completed_status_id){
                            ?>
                            <label>
                                                    <?php
                                                    if(!empty($result['awaiting_table'])){
                                                        echo 'Awaiting ';
                                                        foreach($result['awaiting_table'] as $x){
                                                            if(isset($role_list[$x])){
                                                            echo '<br>'.$role_list[$x];
                                                            }
                                                        }
                                                    }
                                                    ?>
                            </label><br>
                            <?php 
                            } 
                            ?>
                            <label><?=$sr_status_list[$result['status']]?></label>
                            </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                    
                    <?php } //Edit mode end ?>
                    
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->
		<script>
		var mode = '<?=$mode?>';
		var btn_type = '<?=$btn_type?>';
		
					
		
        $( "#search" ).blur(function(){
			
			ajax_quotation(1);
			
		});
		
		$('#myModal').on('show.bs.modal', function (event) {
			ajax_quotation(1);
		})
		
		function changePage(i){
			ajax_quotation(i);
		}
		
		function ajax_quotation(i){
			keyword = $("#search").val();
			term = {keyword:keyword,limit:10,page:i};
			url = '<?=base_url($init['langu'].'/anexus/sales/getQuotationService')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					//console.log(r);
					tmp = '';
					
					if(r.data != false){
					for(var i=0;i<r.data.length;i++){
						
						tmp+='<tr><td><a href="javascript:getQuotation('+r.data[i].id+',1)">'+r.data[i].customer_name+'</a></td><td>'+r.data[i].qr_serial+'</td><td>'+r.data[i].modified_date+'</td><td>'+r.data[i].status+'</td></tr>';
					
					}
					
					html = '<thead><tr><th>Company Name</th><th>Reference No.</th><th>Date Last Updated</th><th>Status</th></tr></thead><tbody>'+tmp+'</tbody>';
					
					pagination = r.paging;
					
					$('#quotation_table').html('');
					$('#pagination').html('');
					$('#quotation_table').append(html);
					$('#pagination').append(pagination);
					}else{
						$('#quotation_table').html('No search result');
						$('#pagination').html('');
					}
					
				}
			});	
		}
		var product ='';
		function getQuotation(id,mode){
			var dateRequisition = '';
			var html = '';
			var temp_item = '';
			var company_name = '';
			var pic = '';
			
			if(mode==1){
			$('#myModal').modal('hide');
			}
			
			term = {id:id};
			url = '<?=base_url($init['langu'].'/anexus/sales/getQuotationDataService')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					console.log(r);
					dateRequisition = r.data.request_date;
					dateRequisition = dateRequisition.split('-');
					
					$('#customer_id').val(r.data.customer_id);
					$('#customer_name').val(r.data.customer_name);
					$('#salesOrderRef').val(r.data.qr_serial);
					$('#employeeRef').val(r.data.employee_no);
					$('#department').val(r.data.department);
					$('#departmentId').val(r.data.department_id);
					$('#requestorName').val(r.data.requestor_to);
					$('#dateRequisition').val(dateRequisition[2]+"/"+dateRequisition[1]+"/"+dateRequisition[0]);
					
					$('#companyName').val(r.company_data.company_name);
					$('#pic').val(r.company_data.company_pic);
					
					$('#table_body').html('');
					counter =0;
					product = r.product;
					if(r.product.length>0){
						for(var i=0;i<r.product.length;i++){
							addNewItem();
							//alert(1);
						}
					}
					
					
					
					//console.log(product);
					
					
				}
			});	
			
		}
		var tmp_html='';
		var counter=0;
		
		var related_service_json = <?=$mode=='Edit'?$getRelatedService:'[]'?>;
		
		$(document).ready(function(){
			
			if(mode == 'Edit' && related_service_json.length > 0){
				for(var i=0;i<related_service_json.length;i++){
					addNewItem();
				}	 
			}	 
			//change input to read only
					if(btn_type != 'create' && btn_type != 'edit' && btn_type != 'sendmail_update'){
						
						$(".form-control").each(function() {
						  $(this).prop('disabled', 'disabled');
						});
						
						$(".btn-circle").each(function() {
						  $(this).hide();
						});
						$('#addProduct').hide();
						$('#addService').hide();
						if(btn_type == 'update' || btn_type == 'approval_update' ){
							
							var target_colum = <?=isset($target_colum)?$target_colum:'[]'?>;
							
							for(var i=0;i<target_colum.length;i++){
								var colum = target_colum[i].name;
								if(target_colum[i].type == 'single'){
									$("input[name='"+colum+"']").prop('disabled', false);
									
								}else if(target_colum[i].type == 'multiple'){
									$("."+colum).prop('disabled', false);
								}
							}
							
							
						}
					}
		});
		
		function addNewItem(){
			
			var temp_option='';
			var tmp_date ='';
			
			if(mode =='Edit' && counter < related_service_json.length){
				
				for(i=1;i<=50;i++){
					if(related_service_json[counter].quantity == i){
						temp_option+='<option value="'+i+'" selected>'+i+'</option>';
					}else{
						temp_option+='<option value="'+i+'">'+i+'</option>';
					}
				}
				tmp_date = related_service_json[counter].startdate;
				tmp_date = tmp_date.split('-');
				tmp_date = tmp_date[2]+'/'+tmp_date[1]+'/'+tmp_date[0];
				
				yes ='';
				no ='';
				
				if(related_service_json[counter].hours_included == '1'){
					yes ='selected';
				}else{
					no ='selected';
				}
				
				tmp_html='<tr id="group'+counter+'"><td>'+(counter+1)+'<input id="product_id'+counter+'" name="product_id'+counter+'" type="hidden" value="'+related_service_json[counter].product_id+'"></td><td><select name="quantity'+counter+'" class="form-control">'+temp_option+'</select></td><td><input name="partNo'+counter+'" type="text" class="form-control" value="'+related_service_json[counter].part_no+'"></td><td><textarea name="description'+counter+'" class="form-control">'+related_service_json[counter].description+'</textarea></td><td><input name="hour'+counter+'" type="text" class="form-control" value="'+related_service_json[counter].hours+'"></td><td><input id="startDate'+counter+'" name="startDate'+counter+'" type="text" class="form-control" placeholder="Calendar Input" value="'+tmp_date+'"></td><td><select name="hours_included'+counter+'" class="form-control"><option value="1"'+yes+'>Yes</option><option value="0"'+no+'>No</option></select></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="delGroup('+counter+')"><i class="fa fa-times"></i></button><input id="is_deleted'+counter+'" name="is_deleted'+counter+'" type="hidden" value="0"><input id="old_id'+counter+'" name="old_id'+counter+'" type="hidden" value="'+related_service_json[counter].id+'"></td></tr>';
				
				
			}else{
			
			if(counter < product.length && product != ''){
				for(i=1;i<=50;i++){
					if(product[counter].quantity == i){
						temp_option+='<option value="'+i+'" selected>'+i+'</option>';
					}else{
						temp_option+='<option value="'+i+'">'+i+'</option>';
					}
				}
			}else{
				for(i=1;i<=50;i++){
					temp_option+='<option value="'+i+'">'+i+'</option>';
				}
			}
			
			if(counter < product.length && product != ''){
				tmp_html='<tr id="group'+counter+'"><td>'+(counter+1)+'<input id="product_id'+counter+'" name="product_id'+counter+'" type="hidden" value="'+product[counter].product_id+'"></td><td><select name="quantity'+counter+'" class="form-control">'+temp_option+'</select></td><td><input name="partNo'+counter+'" type="text" class="form-control" value="'+product[counter].serial+'"></td><td><textarea name="description'+counter+'" class="form-control"></textarea></td><td><input name="hour'+counter+'" type="text" class="form-control" value=""></td><td><input id="startDate'+counter+'" name="startDate'+counter+'" type="text" class="form-control" placeholder="Calendar Input"></td><td><select name="hours_included'+counter+'" class="form-control"><option value="1">Yes</option><option value="0">No</option></select></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="delGroup('+counter+')"><i class="fa fa-times"></i></button><input id="is_deleted'+counter+'" name="is_deleted'+counter+'" type="hidden" value="0"><input id="old_id'+counter+'" name="old_id'+counter+'" type="hidden" value=""></td></tr>';
			}else{
				tmp_html='<tr id="group'+counter+'"><td>'+(counter+1)+'</td><td><select name="quantity'+counter+'" class="form-control">'+temp_option+'</select></td><td><input name="partNo'+counter+'" type="text" class="form-control" value=""></td><td><textarea name="description'+counter+'" class="form-control"></textarea></td><td><input name="hour'+counter+'" type="text" class="form-control" value=""></td><td><input id="startDate'+counter+'" name="startDate'+counter+'" type="text" class="form-control" placeholder="Calendar Input"></td><td><select name="hours_included'+counter+'" class="form-control"><option value="1">Yes</option><option value="0">No</option></select></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="delGroup('+counter+')"><i class="fa fa-times"></i></button><input id="is_deleted'+counter+'" name="is_deleted'+counter+'" type="hidden" value="0"><input id="old_id'+counter+'" name="old_id'+counter+'" type="hidden" value=""></td></tr>';
			}
			
			}
			
			
			
			$('#table_body').append(tmp_html);
			
			$( "#startDate"+counter ).datepicker({
			dateFormat:'dd/mm/yy',
			});
			
			//set datepicker get current date
			if(mode == 'Add'){
				 $("#startDate"+counter).datepicker('setDate', 'today');
			}
			
			counter++;
			$('#addCounter').val(counter);
			//counter++;
			
		}
		
		$( "#dateRequisition" ).datepicker({
		dateFormat:'dd/mm/yy',
		});
		
		//set datepicker get current date
		if(mode == 'Add'){
			 $('#dateRequisition').datepicker('setDate', 'today');
		}
		
		function delGroup(id){
			$("#is_deleted"+id).val(1);
			$('#group'+id).hide();
			
		}
		
		var qr_id =<?=isset($qr_id)?$qr_id:0?>;
		
		if(qr_id != 0){
			getQuotation(qr_id,2);
		}
        </script>
        
        
        