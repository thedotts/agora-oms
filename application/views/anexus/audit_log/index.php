 <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-file-text fa-fw"></i> Audit Log
                    	<div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input class="form-control" id="q" placeholder="Search" onblur="filter();" value="<?=$q?>">
                                </div>
                                
                                <!-- /.form-group -->
                            </form>
                        </div>
                		<!-- /.col-lg-3 -->
                        
                        <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input class="form-control" id="end_date" placeholder="end date" value="<?=$end_date?>">
                                </div>
                                
                                <!-- /.form-group -->
                            </form>
                        </div>
                		<!-- /.col-lg-3 -->
                        
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input class="form-control" id="start_date" placeholder="start date" value="<?=$start_date?>">
                                </div>
                                
                                <!-- /.form-group -->
                            </form>
                        </div>
                		<!-- /.col-lg-3 -->
                        
                         <script>
							function filter(){
								var start_date = $("#start_date").val();
								if(start_date == '') {
									start_date = 'ALL';	
								}else{
									start_date = start_date.replace(/\//g,"-");
								}
								
								var end_date = $("#end_date").val();
								if(end_date == '') {
									end_date = 'ALL';	
								}else{
									end_date = end_date.replace(/\//g,"-");
								}
								
								var q = $("#q").val();
								
								if(q == '') {
									q = 'ALL';	
									
								}
								//alert(q);		
								//return false;						
								location.href='<?=base_url('en/agora/'.$group_name.'/'.$model_name)?>/'+start_date+'/'+end_date+'/'+q;
							}
						</script>
                        
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default hidden-xs">
                    	<?php
								if(!empty($results)) {
								?>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Log No.</th>
                                            <th>Timestamp</th>
                                            <th>IP Address</th>
                                            <th>User Trigger</th>
                                            <th>Table Affected</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
										foreach($results as $k => $v) {
										?>
                                        <tr>
                                            <td><?=($k+1+(12*($page-1)))?></td>
                                            <td><?=$v['log_no']?></td>
                                            <td><?=date('d/m/Y h:iA',strtotime($v['audit_date']))?></td>
                                            <td><?=$v['ip_address']?></td>
                                            <td><?=$user_list[$v['user_trigger']]?></td>
                                            <td><?=$v['table_affect']?></td>
                                            <td><?=$v['description']?></td>
                                        </tr>
                                        <?php
										}
										?>                                        
                                    </tbody>
                                </table>
                                
                                <?php									
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
								?>
          </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                    <!-- Data list (Mobile) -->
                        <div class="mobile-list visible-xs">
                        	<?php
								if(!empty($results)) {
								?>
                        	<table class="table table-striped table-bordered table-hover">
                            	<?php										
										foreach($results as $k=>$v) {
									?>
                            	<tr>
                                	<td>
                                    	<div class="row mobile-list-header">
                                            <div class="col-xs-12"><strong>#<?=($k+1+(12*($page-1)))?></strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Log No.</strong></div>
                                            <div class="col-xs-8"><?=$v['log_no']?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>User Trigger:</strong></div>
                                            <div class="col-xs-8"><?=$employee_list[$v['user_trigger']]?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Timestamp:</strong></div>
                                            <div class="col-xs-8"><?=date('d/m/Y h:iA',strtotime($v['created_date']))?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Table Affected:</strong></div>
                                            <div class="col-xs-8"><?=$v['table_affect']?></div>
                                        </div>
                                    </td>
                                </tr>
                                <?php											
										}
									?>
                            </table>
                            <?php
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
								?>
                                                       
                        </div>
                    
                    <?=$paging?>
                    <div style="float:right">
                    <a href="<?=base_url('en/agora/administrator/audit_log_export'.'/'.str_replace('/','-',$start_date).'/'.str_replace('/','-',$end_date).'/'.($q==''?'ALL':$q))?>"><button class="btn btn-success">Export</button></a>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
        
<script>
$( "#start_date" ).datepicker({
	dateFormat:'dd/mm/yy',
	onSelect: function() {
        filter();
    }
});

$( "#end_date" ).datepicker({
	dateFormat:'dd/mm/yy',
	onSelect: function() {
        filter();
    }
});

		//enter search
		$(document).ready(function() {
		  $(window).keydown(function(event){
			if(event.keyCode == 13) {
			  event.preventDefault();
			  q.blur();
			  return false;
			}
		  });
		});
</script>