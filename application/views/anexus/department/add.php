<div id="page-wrapper">
        	<form method="post" action="<?=base_url('en/anexus/'.$group_name.'/'.$model_name.'_submit')?>" onsubmit="return validate();">
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-pencil-square-o fa-fw"></i> Create Department
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Department Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Department Name</label>
                                    <input class="form-control" name="department_name" id="department_name" value="<?=$mode=='Edit'?$result['name']:''?>">
                                    <p class="help-block"><i></i></p>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                </div>
                <!-- /.col-lg-12 -->

            </div>
            <!-- /.row -->
            
            
            <div class="row">
                <div class="col-lg-12">
                    
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->

<script>

$(document).ready(function(e) {
	$("#dob").datepicker({"dateFormat":"dd/mm/yy"}); 
	$("#expired_date").datepicker({"dateFormat":"dd/mm/yy"});    
});

</script>