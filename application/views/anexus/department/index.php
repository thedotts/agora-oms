
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-pencil-square-o fa-fw"></i> Manage Departments
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input id="q" class="form-control" placeholder="Search" onBlur="filter()" value="<?=$q?>">                                    
                                </div>
                                <!-- /.form-group -->
                            </form>
                            <script>
							function filter(){								
								var q = $("#q").val();
								if(q == '') {
									q = 'ALL';	
								}								
								location.href='<?=base_url('en/anexus/'.$group_name.'/'.$model_name)?>/'+q;
							}
							</script>
                        </div>
                		<!-- /.col-lg-3 -->
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                            	<a href="<?=base_url('en/anexus/'.$group_name.'/'.$model_name.'_add')?>"><button class="btn btn-default btn-sm" type="button">Create Department </button></a><p></p>
                    <div class="panel panel-default">
                            <div class="table-responsive">
                            	<?php
								if(!empty($results)) {
								?>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
										foreach($results as $v) {
										?>
                                        <tr>
                                            <td><?=$v['id']?></td>
                                            <td><?=$v['name']?></td>
                                            <td>
                                                <a class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button" href="<?=base_url('en/anexus/'.$group_name.'/'.$model_name.'_edit/'.$v['id'])?>"><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" href="javascript: deleteData('<?=base_url('en/anexus/'.$group_name.'/'.$model_name.'_del/'.$v['id'])?>')"><i class="fa fa-times"></i></a>
                                             </td>
                                        </tr>  
                                        <?php
										}
										?>
                                    </tbody>
                                </table>
                                <?php
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
								?>
          </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                    <?=$paging?>
                    
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
        
         <script>
		function deleteData(url){
			alert(url);
			var c = confirm("Are you sure you want to delete?");
			if(c){
				location.href=url;	
			}
		}
		</script>

   