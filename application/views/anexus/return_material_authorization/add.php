<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-wrench fa-fw"></i> <?=$head_title?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                       		<!-- Button trigger modal -->
                            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" <?=$mode=='Edit'?'disabled':''?>>
                                Seach from Quotation
                            </button>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Search Quotation</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Search</label>
                                                <input class="form-control" placeholder="By Company Name or Reference No." id="search" name="search">
                                            </div>
                                            <table id="quotation_table" class="table table-striped table-bordered table-hover" id="dataTables">
                                            </table>
                                            <div id="pagination"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            
                
        	<form action="<?=base_url($init['langu'].'/anexus/service/return_material_authorization_submit');?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" id="customer_id" name="customer_id" value="<?=$mode=='Edit'?$result['customer_id']:''?>"/>
            <input type="hidden" id="requestor_id" name="requestor_id" value="<?=$mode=='Edit'?$result['requestor_id']:$staff_info['id']?>"/>
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Return Material Authorization Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Serial No.</label>
                                    <input class="form-control" value="<?=$mode=='Edit'?$result['return_material_serial_no']:''?>" readonly>
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Type of Return</label>
                                    <select id="type_return" name="type_return" class="form-control">
                                    <option value="No Charge" <?=$mode=='Edit'?($result['type_of_return']=='No Charge'?'selected':''):''?>>No Charge</option>
                                    <option value="Manufacturer Warranty" <?=$mode=='Edit'?($result['type_of_return']=='Manufacturer Warranty'?'selected':''):''?>>Manufacturer Warranty</option>
                                    <option value="Service Warranty" <?=$mode=='Edit'?($result['type_of_return']=='Service Warranty'?'selected':''):''?>>Service Warranty</option>
                                    <option value="Upgrade" <?=$mode=='Edit'?($result['type_of_return']=='Upgrade'?'selected':''):''?>>Upgrade</option>
                                    <option value="Paid" <?=$mode=='Edit'?($result['type_of_return']=='Paid'?'selected':''):''?>>Paid</option>
                                    <option value="Others" <?=$mode=='Edit'?($result['type_of_return']=='Others'?'selected':''):''?>>Others</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Reason for Return</label>
                                    <textarea id="reason_return" name="reason_return" class="form-control"><?=$mode=='Edit'?$result['reason_of_return']:''?></textarea>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6"> 
                
                
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Customer Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Customer</label>
                                    <input id="customer_name" name="customer_name" class="form-control" value="<?=$mode=='Edit'?$customer['company_name']:''?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="contact_person" name="contact_person" class="form-control" value="<?=$mode=='Edit'?$customer['contact_person']:''?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Contact No.</label>
                                    <input id="contact_no" name="contact_no" class="form-control" value="<?=$mode=='Edit'?$customer['contact_info']:''?>" readonly>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                
                </div>
                <!-- /.col-lg-6 -->
                
            </div>
            <!-- /.row -->
            <hr />
            
            <div class="row">
                <div class="col-lg-12">
                
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Collection Information
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                         <tr>
                                            <td>
                                    			Date of Collection
                                            </td>
                                            <td>
                                                <input id="date_collection" name="date_collection" class="form-control" value="<?=$mode=='Edit'?($result['collection_date'] != '0000-00-00 00:00:00'?date('d/m/Y',strtotime($result['collection_date'])):''):''?>">
                                            </td>
                                        </tr>
                                         <tr>
                                            <td>
                                    			Special Instructions
                                            </td>
                                            <td>
                                                <textarea id="special_instruct_collect" name="special_instruct_collect" class="form-control"><?=$mode=='Edit'?$result['collection_special_instruction']:''?></textarea>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                    
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Items 
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Item</th>
                                                    <th>Quantity</th>
                                                    <th>Part Number</th>
                                                    <th width="80px">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="collect_body">
                                                
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                    <!-- /.table-responsive -->
                                    
                                </div>
                                <!-- /.panel-body -->
                                
                            </div>
                            <!-- /.panel -->
                    
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    
                    <div class="panel">
                        
                    <?php if($mode=='Add'){?>
                    
                    	<?php if($btn_type == 'create'){?>
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary" name="type" value=1>Submit</button>
                        <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                        </div>
                    	<?php }?>
                        
                    <?php }else if($mode=='Edit'){?>
                    	<?php switch($btn_type){
							case 'edit':
							?>
                        		<div class="form-group">
                                	<button type="submit" name="type" value=1 class="btn btn-primary">Submit</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            <?php 
							break;
							
							case 'update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-primary" name="type" value=1>Update</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'approval_update':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'sendmail_update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-default">Save</button>
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/service/return_material_authorization_pdf/'.$result['id'].'/1')?>" target="_blank">Download PDF</a>
                       			<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/service/return_material_authorization_send_mail/'.$result['id'].'/1')?>')">Send <?=$workflow_title?> in email</button>
                                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'send_email':
							?>
                            	<div class="form-group">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/service/return_material_authorization_pdf/'.$result['id'].'/1')?>" target="_blank">Download PDF</a>
                       			<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/service/return_material_authorization_send_mail/'.$result['id'].'/1')?>')">Send <?=$workflow_title?> in email</button>
                                </div>
                            
                            <?php 
							break;
							case 'upload':
							?>
                            
                            	<div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/service/return_material_authorization_pdf/'.$result['id'].'/1')?>" target="_blank">Download PDF</a>
                       			<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/service/return_material_authorization_send_mail/'.$result['id'].'/1')?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file">
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                		<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            
                            <?php 
							break;
							case 'upload_collection':
							?>
                            	
                        		<a class="btn btn-default" href="<?=base_url('en/anexus/service/return_material_authorization_pdf/'.$result['id'].'/1')?>" target="_blank">Download PDF</a>
                       			<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/service/return_material_authorization_send_mail/'.$result['id'].'/1')?>')">Send <?=$workflow_title?> in email</button>
                        		<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                            	
                            <?php 
							break;
							case 'upload_delivery':
							?>
                            	
                        		<a class="btn btn-default" href="<?=base_url('en/anexus/service/return_material_authorization_pdf/'.$result['id'].'/1')?>" target="_blank">Download PDF</a>
                       			<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/service/return_material_authorization_send_mail/'.$result['id'].'/1')?>')">Send <?=$workflow_title?> in email</button>
                        		<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                            	
                            <?php 
							break;
							case 'complete':
							?>
                            	<?php if($last_action == 'upload'){?>
                            	<div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/logistics/do_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/logistics/do_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file"> 
                                            <a target="_blank" href="<?=$result['comfirm_file']?>"><?=$result['comfirm_file']?></a>
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Reupload</button>
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            	<?php }?>
                            <?php 
							break;
                        }?>

                    <?php } //Edit mode end ?>
                        
                        
                    
                    </div>
                    <!-- /.panel -->
                    <?php if($mode=='Edit'){?>
                    <?php if($result['status'] == 5 && $userdata['role_id'] == $requestor_role){?>
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Confirmation by Customer
                        </div>
                        <div class="panel-body">
                            <div class="form-group input-group">
                                <label>Upload Confirmation Document</label>
                                <input id="collect_file" name="collect_file" type="file">
                            </div>
                            <button type="submit" class="btn btn-success" name="type_value" value="Complete Collection">Complete Collection</button>
                            <button type="submit" class="btn btn-danger" name="type_value" value="Not Approved">Not Approved</button>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <?php }else if($result['status'] > 5){ ?>
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Confirmation by Customer
                        </div>
                        <div class="panel-body">
                            <div class="form-group input-group">
                                <label>Upload Confirmation Document</label>
                                <input id="collect_file" name="collect_file" type="file">
                                <?php if($result['collection_confirm_ref'] != ''){?>
                                	<a href="<?=$result['collection_confirm_ref']?>" target="_blank"><?=$result['collection_confirm_ref']?></a>
                                <?php } ?>
                            </div>
                            <button type="submit" class="btn btn-default" name="type" value="1">Submit</button>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <?php } ?>
                    <?php } ?>
                
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Delivery and Return Information
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                         <tr>
                                            <td>
                                    			Date of Return
                                            </td>
                                            <td>
                                                <input id="date_return" name="date_return" class="form-control" value="<?=$mode=='Edit'?($result['return_date'] != '0000-00-00 00:00:00'?date('d/m/Y',strtotime($result['return_date'])):''):''?>">
                                            </td>
                                        </tr>
                                         <tr>
                                            <td>
                                    			Special Instructions
                                            </td>
                                            <td>
                                                <textarea id="special_instruct_return" name="special_instruct_return" class="form-control"><?=$mode=='Edit'?$result['return_special_instruction']:''?></textarea>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                    
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Items 
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Item</th>
                                                    <th>Quantity</th>
                                                    <th>Part Number</th>
                                                    <th width="80px">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="return_body">
                                                
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                    <!-- /.table-responsive -->
                                    
                                </div>
                                <!-- /.panel-body -->
                                
                            </div>
                            <!-- /.panel -->
                    
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                    
                    <div class="panel">
                    
                    
                    <?php if($mode=='Add'){?>
                    
                    	<?php if($btn_type == 'create'){?>
                        <div class="form-group">
                        <button type="submit" name="type" value=2 class="btn btn-primary">Submit</button>
                        <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                        </div>
                    	<?php }?>
                        
                    <?php }else if($mode=='Edit'){?>
                    	<?php switch($btn_type){
							case 'edit':
							?>
                        		<div class="form-group">
                                	<button type="submit" name="type" value=2 class="btn btn-primary">Submit</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            <?php 
							break;
							case 'approval':
							case 'approval_sendmail':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-primary" name="type" value=2>Update</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'approval_update':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'sendmail_update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-default">Save</button>
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/service/return_material_authorization_pdf/'.$result['id'].'/2')?>" target="_blank">Download PDF</a>
                       			<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/service/return_material_authorization_send_mail/'.$result['id'].'/2')?>')">Send <?=$workflow_title?> in email</button>
                                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'send_email':
							?>
                            	<div class="form-group">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/service/return_material_authorization_pdf/'.$result['id'].'/2')?>" target="_blank">Download PDF</a>
                       			<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/service/return_material_authorization_send_mail/'.$result['id'].'/2')?>')">Send <?=$workflow_title?> in email</button>
                                </div>
                            
                            <?php 
							break;
							case 'upload':
							?>
                            
                            	<div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/service/return_material_authorization_pdf/'.$result['id'].'/2')?>" target="_blank">Download PDF</a>
                       			<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/service/return_material_authorization_send_mail/'.$result['id'].'/2')?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file">
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                		<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            
                            <?php 
							break;
							case 'upload_collection':
							?>
                            	
                        		<a class="btn btn-default" href="<?=base_url('en/anexus/service/return_material_authorization_pdf/'.$result['id'].'/2')?>" target="_blank">Download PDF</a>
                       			<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/service/return_material_authorization_send_mail/'.$result['id'].'/2')?>')">Send <?=$workflow_title?> in email</button>
                        		<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                            	
                            <?php 
							break;
							case 'upload_delivery':
							?>
                            	
                        		<a class="btn btn-default" href="<?=base_url('en/anexus/service/return_material_authorization_pdf/'.$result['id'].'/2')?>" target="_blank">Download PDF</a>
                       			<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/service/return_material_authorization_send_mail/'.$result['id'].'/2')?>')">Send <?=$workflow_title?> in email</button>
                        		<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                            	
                            <?php 
							break;
							case 'complete':
							?>
                            	<?php if($last_action == 'upload'){?>
                            	<div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/logistics/do_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/logistics/do_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file"> 
                                            <a target="_blank" href="<?=$result['comfirm_file']?>"><?=$result['comfirm_file']?></a>
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Reupload</button>
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            	<?php }?>
                            <?php 
							break;
                        }?>

                    <?php } //Edit mode end ?>
                        
                    
                    </div>
                    
                    <?php if($mode=='Edit'){?>
                    <?php if($result['status'] == 6 && $userdata['role_id'] == $requestor_role){?>
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Confirmation by Customer
                        </div>
                        <div class="panel-body">
                            <div class="form-group input-group">
                                <label>Upload Confirmation Document</label>
                                <input id="return_file" name="return_file" type="file">
                            </div>
                            <button type="submit" class="btn btn-success" name="type_value" value="Complete Delivery">Complete Delivery</button>
                            <button type="submit" class="btn btn-danger" name="type_value" value="Not Approved">Not Approved</button>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <?php }else if($result['status'] > 6){ ?>
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Confirmation by Customer
                        </div>
                        <div class="panel-body">
                            <div class="form-group input-group">
                                <label>Upload Confirmation Document</label>
                                <input id="return_file" name="return_file" type="file">
                                <?php if($result['return_confirm_ref'] != ''){?>
                                	<a href="<?=$result['return_confirm_ref']?>" target="_blank"><?=$result['return_confirm_ref']?></a>
                                <?php } ?>
                            </div>
                            <button type="submit" class="btn btn-default" name="type" value="2">Submit</button>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <?php } ?>
                    
                    
                    <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Status
                                </div>
                                <div class="panel-body">
                                    <div class="form-group input-group">
                                        <?php if($result['status'] > 0 && $result['status'] < 5){?>
                                        <label>
                                                                <?php
                                                                if(!empty($result['awaiting_table'])){
                                                                    echo 'Awaiting ';
                                                                    foreach($result['awaiting_table'] as $x){
                                                                        if(isset($role_list[$x])){
                                                                        echo '<br>'.$role_list[$x];
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                        </label><br>
                                        <?php } ?>
                                        <label><?=$rma_status_list[$result['status']]?></label>
                                    </div>
                                 </div>
                                 <!-- /.panel-body -->
                             </div>
                             
                             <?php } ?>
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->
		<script>
		var mode ='<?=$mode?>';
		var related_item = <?=$mode=='Edit'?$related_item:'[]'?>;
		var btn_type = '<?=$btn_type?>';
		
		if(mode=='Edit'){
				
			for(var i=0;i<related_item.length;i++){
				collect_html='<tr class="item_'+i+'"><td>'+(i+1)+'<input name="item_id[]" type="hidden" value="'+related_item[i].id+'"><input id="is_del'+i+'" name="is_del[]" type="hidden" value="0"><input name="product_id[]" type="hidden" value="'+related_item[i].product_id+'"></td><td><input class="form-control" name="model_no[]" value="'+related_item[i].item_name+'" readonly></td><td><input name="quantity[]" class="form-control" value="'+related_item[i].quantity+'" readonly></td><td><input type="text" class="form-control" name="part_no[]" value="'+related_item[i].part_number+'" readonly></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button><button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="delItem('+i+')"><i class="fa fa-times"></i></button></td></tr>';
						
				return_html ='<tr class="item_'+i+'"><td>'+(i+1)+'</td><td><input class="form-control" value="'+related_item[i].item_name+'" readonly></td><td><input class="form-control" value="'+related_item[i].quantity+'" readonly></td><td><input type="text" class="form-control" value="'+related_item[i].part_number+'" readonly></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></td></tr>';
					
				$('#collect_body').append(collect_html);
				$('#return_body').append(return_html);
			 }
			 //change input to read only
					if(btn_type != 'create' && btn_type != 'edit' && btn_type != 'sendmail_update'){
						
						$(".form-control").each(function() {
						  $(this).prop('disabled', 'disabled');
						});
						
						$(".btn-circle").each(function() {
						  $(this).hide();
						});
						$('#addProduct').hide();
						$('#addService').hide();
						if(btn_type == 'update' || btn_type == 'approval_update' ){
							
							var target_colum = <?=isset($target_colum)?$target_colum:'[]'?>;
							
							for(var i=0;i<target_colum.length;i++){
								var colum = target_colum[i].name;
								if(target_colum[i].type == 'single'){
									$("input[name='"+colum+"']").prop('disabled', false);
									
								}else if(target_colum[i].type == 'multiple'){
									$("."+colum).prop('disabled', false);
								}
							}
							
							
						}
					}
		}
		
        $( "#search" ).blur(function(){
			
			ajax_quotation(1);
			
		});
		
		$('#myModal').on('show.bs.modal', function (event) {
			ajax_quotation(1);
		})
		
		function changePage(i){
			ajax_quotation(i);
		}
		
		function ajax_quotation(i){
			keyword = $("#search").val();
			term = {keyword:keyword,limit:10,page:i};
			url = '<?=base_url($init['langu'].'/anexus/service/getQuotation')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					//console.log(r);
					tmp = '';
					
					if(r.data != false){
					for(var i=0;i<r.data.length;i++){
						
						tmp+='<tr><td><a href="javascript:getQuotation('+r.data[i].id+')">'+r.data[i].customer_name+'</a></td><td>'+r.data[i].qr_serial+'</td><td>'+r.data[i].modified_date+'</td><td>'+r.data[i].status+'</td></tr>';
					
					}
					
					html = '<thead><tr><th>Company Name</th><th>Reference No.</th><th>Date Last Updated</th><th>Status</th></tr></thead><tbody>'+tmp+'</tbody>';
					
					pagination = r.paging;
					
					$('#quotation_table').html('');
					$('#pagination').html('');
					$('#quotation_table').append(html);
					$('#pagination').append(pagination);
					}else{
						$('#quotation_table').html('No search result');
						$('#pagination').html('');
					}
					
				}
			});	
		}
		
		function getQuotation(id){
			var dateRequisition = '';
			var html = '';
			var temp_item = '';
			var company_name = '';
			var pic = '';
			
			$('#myModal').modal('hide');
			
			term = {id:id};
			url = '<?=base_url($init['langu'].'/anexus/service/getQuotationData')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					console.log(r);
					
					//$('#quotation_id').val(r.data.id);
					$('#customer_id').val(r.customer_data.id);
					$('#customer_name').val(r.customer_data.company_name);
					$('#contact_person').val(r.customer_data.contact_person);
					$('#contact_no').val(r.customer_data.contact_info);
					
					//collect item
					$('#collect_body').append('');
					$('#return_body').append('');
						
					for(var i=0;i<r.product.length;i++){
						collect_html='<tr class="item_'+i+'"><td>'+(i+1)+'<input name="item_id[]" type="hidden" value="0"><input id="is_del'+i+'" name="is_del[]" type="hidden" value="0"><input name="product_id[]" type="hidden" value="'+r.product[i].product_id+'"></td><td><input class="form-control" name="model_no[]" value="'+r.product[i].model_no+'" readonly></td><td><input name="quantity[]" class="form-control" value="'+r.product[i].quantity+'" readonly></td><td><input type="text" class="form-control" name="part_no[]" value="'+r.product[i].part_no+'" readonly></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button><button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="delItem('+i+')"><i class="fa fa-times"></i></button></td></tr>';
						
						return_html ='<tr class="item_'+i+'"><td>'+(i+1)+'</td><td><input class="form-control" value="'+r.product[i].model_no+'" readonly></td><td><input class="form-control" value="'+r.product[i].quantity+'" readonly></td><td><input type="text" class="form-control" value="'+r.product[i].part_no+'" readonly></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></td></tr>';
					
						$('#collect_body').append(collect_html);
						$('#return_body').append(return_html);
					}

					
					
				}
			});	
			
		}
		
		function delItem(id){
			$('.item_'+id).hide();
			$('#is_del'+id).val(1);
		}
		
		$( "#date_collection" ).datepicker({
		dateFormat:'dd/mm/yy',
		});
		
		$( "#date_return" ).datepicker({
		dateFormat:'dd/mm/yy',
		});
		
		//set datepicker get current date
		if(mode == 'Add'){
			 $('#date_collection').datepicker('setDate', 'today');
			 $('#date_return').datepicker('setDate', 'today');
		}
		
		function sentMail(url){
			location.href=url;
		}
		
        </script>