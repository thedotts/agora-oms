<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-wrench fa-fw"></i> Return Material Authorization
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input id="q" class="form-control" placeholder="Search" value="<?=$q?>" onblur="filter();">
                                </div>
                                <script>
							function filter(){
								var q = $("#q").val();
								if(q == '') {
									q = 'ALL';	
								}								
								location.href='<?=base_url('en/anexus/'.$group_name.'/'.$model_name)?>/'+q;
							}
							</script>
                                <!-- /.form-group -->
                            </form>
                        </div>
                		<!-- /.col-lg-3 -->
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                            	<a href="<?=base_url('en/anexus/'.$group_name.'/'.$model_name.'_add')?>"><button class="btn btn-default btn-sm" type="button">Create Return Material Authorization</button></a><p></p>
                    <div class="panel panel-default">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>S/R No.</th>
                                            <th>Customer</th>
                                            <th>Date of Collection</th>
                                            <th>Date of Delivery</th>
                                            <th>Status</th>
                                            <th width="80px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(!empty($results)){
												foreach($results as $v)
												{
													?>
													<tr>
                                                        <td><?=$v['id']?></td>
                                                        <td><?=$v['return_material_serial_no']?></td>
                                                        <td><?=$customer_list[$v['customer_id']]?></td>
                                                        <td><?=$v['collection_date'] != '0000-00-00 00:00:00'?date('d/m/Y',strtotime($v['collection_date'])):''?></td>
                                                        <td><?=$v['return_date'] != '0000-00-00 00:00:00'?date('d/m/Y',strtotime($v['return_date'])):''?></td>
                                                        <td><?=isset($rma_status_list[$v['status']])?$rma_status_list[$v['status']]:''?></td>
                                                        <td>
                                                        	<!--<button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View" type="button"><i class="fa fa-folder-open"></i></button>-->
                                                            <button  class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button" onclick="location.href='<?=base_url('en/anexus/service/return_material_authorization_edit/'.$v['id'])?>'"><i class="fa fa-edit"></i></button>
                                                            <button  class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="deleteData('<?=base_url('en/anexus/service/return_material_authorization_del/'.$v['id'])?>')"><i class="fa fa-times"></i></button>
                                                            
                                                         </td>
                                                    </tr>
                                            		<?php
												}
											}
											else 
											{
												echo "<td colspan='8'><p><b>No records found.</b></p>";
											}
										?>
                                    </tbody>
                                </table>
                      </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                    <?=$paging?>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
        <script>
		function deleteData(url){
			var c = confirm("Are you sure you want to delete?");
			if(c){
				location.href=url;	
			}
		}
		</script>
