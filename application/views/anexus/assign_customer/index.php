<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-bar-chart-o fa-fw"></i> Assign Customers
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <div class="row">
            
        	<form method="post" action="<?=base_url('en/agora/manager/assign_customer_submit')?>" onsubmit="return validate();">
                <div class="col-lg-4">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Employee
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Employee</label>
                                    <select class="form-control" id="employee_id" name="employee_id" onChange="sel_employee(this.value)">
                                        <option>-</option>
                                        <?php foreach($sales as $k => $v){?>
                                        	<option value="<?=$v['id']?>"><?=$v['full_name']?></option>
                                        <?php }?>
                                    </select>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                    
                </div>
                <!-- /.col-lg-4 -->
                
                <div class="col-lg-8">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Customers 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                            	<button class="btn btn-default btn-sm" type="button" onClick="add_customer(0)">Add New Customer</button>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Customer Name</th>
                                            <th width="80px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="add_body">
                                    
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-8 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->
        
<script>

var counter = 0;
var customer = <?=$customer?>;
var sales = <?=json_encode($sales)?>;

var option_tmp = '';
for(var i=0;i<customer.length;i++){
	option_tmp+='<option value="'+customer[i].id+'">'+customer[i].company_name+'</option>';	 
}

//if id != 0 type = edit
function add_customer(id){
	
	if(id != 0){
		option_tmp = '';
		for(var i=0;i<customer.length;i++){
			if(id == customer[i].id){
				option_tmp+='<option value="'+customer[i].id+'"selected>'+customer[i].company_name+'</option>';	
			}else{
				option_tmp+='<option value="'+customer[i].id+'">'+customer[i].company_name+'</option>';	
			}
		}
	}
	
	counter++;	
	tmp='<tr id="row_'+counter+'">';
    tmp+='<td>'+counter+'</td>';
    tmp+='<td><select class="form-control" name="customer[]">'+option_tmp+'</select></td>';
    tmp+='<td><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="del_row('+counter+')"><i class="fa fa-times"></i></button></td>';
    tmp+='</tr>';
	
	$('#add_body').append(tmp);
}

function del_row(i){
	$('#row_'+i).remove();
}


function sel_employee(sel){
	
	var customer_list='';
	
	for(var i=0;i<sales.length;i++){
		if(sales[i].id == sel){
			if(sales[i].assign_customer != '' && sales[i].assign_customer != null){
				customer_list = sales[i].assign_customer.split(',');
				break;
			}
		}
	}
	
	console.log('id'.sel);
	console.log(customer_list);
	
	$('#add_body').html('');
	counter = 0;
	
	for(var x=0;x<customer_list.length;x++){
		add_customer(customer_list[x]);
	}
	
}
</script>