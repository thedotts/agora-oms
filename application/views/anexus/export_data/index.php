 <div id="page-wrapper">
        	<form action="<?=base_url('en/agora/administrator/export_data')?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-file-o fa-fw"></i> Export Data
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Export Options
                        </div>
                        <div class="panel-body">
                        	<div class="form-group">
                                    <label>Start date *</label>
                                    <input id="start_date" name="start_date" class="form-control" placeholder="Calendar Input" value="<?=date("01/m/Y")?>">
                            </div>
                            <div class="form-group">
                                    <label>End date *</label>
                                    <input id="end_date" name="end_date" class="form-control" placeholder="Calendar Input" value="<?=date("t/m/Y")?>">
                            </div>
                            <div class="form-group">
                                    <label>Table *</label>
                                    <select name="table_name" class="form-control">
                                        <?php foreach($table as $v){?>
                                        <option value="<?=$v?>"><?=$v?></option>
                                        <?php }?>
                                    </select>
                            </div>
                            <button type="submit" class="btn btn-success">Export</button>
                         </div>
                         <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            
            </form>
            
        </div>
        <!-- /#page-wrapper -->
        
<script>
$( "#start_date" ).datepicker({
	dateFormat:'dd/mm/yy',
});
$( "#end_date" ).datepicker({
	dateFormat:'dd/mm/yy',
});
</script>