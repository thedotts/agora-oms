<link rel="stylesheet" href="<?=base_url('assets/css/jquery-ui.css')?>">
<script src="<?=base_url('assets/js/jquery-ui.js')?>"></script>
<div id="page-wrapper">
        	<form action="<?=base_url($init['langu'].'/agora/customer/pending_order_submit');?>" method="post" enctype="multipart/form-data" onsubmit="return validate()">
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" id="item_count" name="item_count" value=""/>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-briefcase fa-fw"></i> Edit Order
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Order Request Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Order No.</label>
                                    <input class="form-control" value="<?=$mode=='Edit'?$result['order_no']:''?>" disabled>
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Order Date *</label>
                                    <input class="form-control" placeholder="Calendar Input" id="order_date" name="order_date" value="<?=$mode=='Edit'?$result['order_date']:''?>">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Customer Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input class="form-control" value="<?=$customer['company_name']?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Attention</label>
                                    <input class="form-control" value="<?=$customer['primary_attention_to']?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control" value="<?=$customer['address']?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Postal Code</label>
                                    <input class="form-control" value="<?=$customer['postal']?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Country</label>
                                    <input class="form-control" value="<?=$customer['country']?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" value="<?=$customer['email']?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Contact No.</label>
                                    <input class="form-control" value="<?=$customer['primary_contact_info']?>" disabled>
                                </div>
                                
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
            </div>
            <!-- /.row -->
            
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Items 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                            	<button class="btn btn-default btn-sm" type="button" onclick="add_item()">Add New Product Item</button>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Product Name</th>
                                            <th style="display:none">Description</th>
                                            <th width="200px">Qty Ordered</th>
                                            <th width="200px">Qty to Amend *</th>
                                            <th width="100px">Unit Price</th>
                                            <th width="80px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody">
                                        
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Total Amount
                        </div>
                        <div class="panel-body">
                        		<div class="form-group">
                                	<label>Total Amount</label>
                                    <input id="total_amount" name="total_amount" class="form-control" value="<?=$mode=='Edit'?$result['total_price']:'0'?>" readonly="readonly"/>
                                </div>
                                <div class="form-group">
                                	<label>GST Amount</label>
                                    <input id="gst_amount" name="gst_amount" class="form-control" value="<?=$mode=='Edit'?$result['gst_amount']:'0'?>" readonly="readonly"/>
                                </div>
                                <div class="form-group">
                                	<label>Nett Amount</label>
                                    <input id="nett_amount" name="nett_amount" class="form-control" value="<?=$mode=='Edit'?$result['total_price']+$result['gst_amount']:'0'?>" readonly="readonly"/>
                                </div>
                                
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                        
                        
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Remarks
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <textarea id="remarks" name="remarks" class="form-control"><?=$mode=='Edit'?$result['remark']:''?></textarea>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <?php if($result['status']==1){?>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                    <?php }?>
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->
<script>
var product = <?=$product?>;
var item_count = 0;
var product_edit = <?=$result['product']?>;
var mode = '<?=$mode?>';
var customer = <?=$customer_json?>;
var gst = <?=$gst['value']?>;

var product_name = <?=$product_name?>;

//product select
var product_sel = '<option>-</option>';
for(var i=0;i<product_name.length;i++){
	product_sel +='<option value="'+product_name[i].product_name+'">'+product_name[i].product_name+'</option>';
}


$( "#order_date" ).datepicker({
	dateFormat:'dd/mm/yy',
});

var option = '<option>-</option>';
for(var i=0;i<product.length;i++){
	option += '<option value="'+product[i].id+'|'+product[i].model_name+'">'+product[i].product_name+' - '+product[i].model_name+'</option>';
}


for(var i=0;i<product_edit.length;i++){
	add_item();
}

function add_item(){
	
	item_count++;
	
	if(mode == 'Edit' && item_count <= product_edit.length){
	
		html = '<tr id="item_'+item_count+'"><td>'+item_count+'</td><td><input data-count="'+item_count+'" class="form-control item" data-name="Product Name" id="product'+item_count+'"></td><td style="display:none"><select class="form-control item" data-name="Description" id="model_name'+item_count+'" name="product'+item_count+'" onchange="product_detail('+item_count+',0)" ><option>-</option></select><input type="hidden" id="model_name2_'+item_count+'" name="model_name'+item_count+'"></td><td><div class="form-group input-group"><input id="qty_ord'+item_count+'" name="qty_ord'+item_count+'" type="text" class="form-control" value="'+product_edit[item_count-1].quantity_order+'" readonly><span class="input-group-addon" id="uom1'+item_count+'">'+product_edit[item_count-1].uom+'(s)</span></div></td><td><div class="form-group input-group"><input  id="qty_amend'+item_count+'" name="qty_amend'+item_count+'" type="text" class="form-control item" value="0" onblur="total_amount_f()" data-name="Quantity"><span class="input-group-addon" id="uom2'+item_count+'">'+product_edit[item_count-1].uom+'(s)</span></div></td><td><input id="price'+item_count+'" name="price'+item_count+'" type="text" class="form-control" value="'+product_edit[item_count-1].price+'" readonly></td><td><a id="link'+item_count+'" href="'+'<?=base_url($init['langu'].'/agora/administrator/products_view/')?>'+'/'+product_edit[item_count-1].product_id+'/<?=$userdata['customer_id']?>" target="_blank"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></a><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" onclick="del_item('+item_count+')" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button><input type="hidden" id="old_id'+item_count+'" name="old_id'+item_count+'" value="'+product_edit[item_count-1].order_item_id+'"/><input class="is_del" type="hidden" id="is_del'+item_count+'" name="is_del'+item_count+'" value="0"/><input type="hidden" id="promo'+item_count+'" name="promo[]" value="0"/><input type="hidden" class="uom" id="uom_val'+item_count+'" value="'+product_edit[item_count-1].uom+'"/></td></tr>';
	
	}else{
		
		html = '<tr id="item_'+item_count+'"><td>'+item_count+'</td><td><input data-count="'+item_count+'" class="form-control item" data-name="Product Name" id="product'+item_count+'"></td><td style="display:none"><select class="form-control item" data-name="Description" id="model_name'+item_count+'" name="product'+item_count+'" onchange="product_detail('+item_count+',0)"><option>-</option></select><input type="hidden" id="model_name2_'+item_count+'" name="model_name'+item_count+'"></td><td><div class="form-group input-group"><input id="qty_ord'+item_count+'" name="qty_ord'+item_count+'" type="text" class="form-control" value="0" readonly><span class="input-group-addon" id="uom1'+item_count+'"></span></div></td><td><div class="form-group input-group"><input  id="qty_amend'+item_count+'" name="qty_amend'+item_count+'" type="text" class="form-control item" data-name="Quantity" value="0" onblur="total_amount_f()"><span class="input-group-addon" id="uom2'+item_count+'"></span></div></td><td><input  id="price'+item_count+'" name="price'+item_count+'" type="text" class="form-control" value="0" readonly></td><td><a id="link'+item_count+'" href="#" target="_blank"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></a><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" onclick="del_item('+item_count+')" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button><input type="hidden" id="old_id'+item_count+'" name="old_id'+item_count+'" value="0"/><input type="hidden" class="is_del" id="is_del'+item_count+'" name="is_del'+item_count+'" value="0"/><input type="hidden" id="promo'+item_count+'" name="promo[]" value="0"/><input type="hidden" class="uom" id="uom_val'+item_count+'" value=""/></td></tr>';
		
	}
	
	$('#item_count').val(item_count);
	$('#tbody').append(html);
	
	
	//preselect
	if(mode == 'Edit' && item_count <= product_edit.length){  
		
		for(var j=0;j<product.length;j++){
			if(product[j].id == product_edit[item_count-1].product_id){
				var product_name = product[j].product_name;
			}
		}
		//console.log(product_name);
		$('#product'+item_count).val(product_name); 
		product_model(item_count); 
		$('#model_name'+item_count).val(product_edit[item_count-1].product_id);                    
		product_detail(item_count,1);
		
	}
	
	var tmp_auto_id = '';
	for(var i=1;i<=item_count;i++){
		if(tmp_auto_id==''){
			tmp_auto_id = '#product'+i;
		}else{
			tmp_auto_id += ',#product'+i;
		}
	}
	
	if(tmp_auto_id != ''){
		//alert(tmp_auto_id);
	$( tmp_auto_id ).autocomplete({
      source: function( request, response ) {
		  
		  
        $.ajax({
          url: "<?=base_url($init['langu'].'/agora/sales/searchProduct/')?>/"+request.term,
          dataType: "json",
          //data: {"search_type":search_type,"order_type":order_type},
          success: function( data ) {
            response( data );
          }
        });
      },
      select: function (event, ui) {
        event.preventDefault();
        $(this).val(ui.item.label); // display the selected text
        //$("#po_id").val(ui.item.value); // save selected id to hidden input
        //toSearch();
		
		var tmp_count = $(this).data('count');
		product_model(tmp_count);
		product_detail(tmp_count,0);
		
        }
    });
	
	}
	
}

function del_item(i){
	$('#item_'+i).hide();
	$('#is_del'+i).val(1);
	total_amount_f();
}

function product_detail(i,product_c_id){
	//product1 = $('#product'+i).val();
	//product2 = product1.split('|');
	product_id = $('#model_name'+i).val();
	console.log(product_id);
	
	
	var default_price = customer.use_default_price;
	
	if(product_id != '-'){
		for(var j=0;j<product.length;j++){
			if(product[j].id == product_id){
				document.getElementById('link'+i).href='<?=base_url($init['langu'].'/agora/administrator/products_view/')?>'+'/'+product_id+'/'+'<?=$userdata['customer_id']?>'; 
				
				$('#model_name2_'+i).val(product[j].model_name);
				$('#uom1'+i).text(product[j].uom);
				$('#uom2'+i).text(product[j].uom);
				$('#uom_val'+i).text(product[j].uom);
			
			
						//原本價格
						var promo_val = 0;
						if(default_price == 1){
							
							var no_promo_id = product[j].no_promo_id.split(',');
							
							var price = product[j].selling_price;
							if(product[j].promo_price != '0.00' && price > product[j].promo_price && !inArray('<?=$staff_info['customer_id']?>',no_promo_id) && product[j].promo_val != 1){
								price = product[j].promo_price;
								promo_val = 1;
							}
							$('#price'+i).val(price);
							
							//可能promo price edit 取原本order item 價格
							if(product_c_id != 0){
								$('#price'+i).val(product_edit[i-1].price);
							}
							
							$('#promo'+i).val(promo_val);
							//alert(price);
						
						//特定價格	
						}else{
							
							var tmp_data = JSON.parse(customer.price_list);
							
							for(var x=0;x<tmp_data.length;x++){
								if(tmp_data[x].id == product_id){
									
									var no_promo_id = product[x].no_promo_id.split(',');
									
									var price = tmp_data[x].price;
									if(product[x].promo_price != '0.00' && price > product[x].promo_price && !inArray('<?=$staff_info['customer_id']?>',no_promo_id) && product[x].promo_val != 1){
										price = product[x].promo_price;
										promo_val = 1;
									}
									
									$('#price'+i).val(price);
									
									//可能promo price edit 取原本order item 價格
									if(product_c_id != 0){
										$('#price'+i).val(product_edit[i-1].price);
									}
									
									$('#promo'+i).val(promo_val);
									console.log(tmp_data[x].price);
									break;
								}
							}
							
						}
						
				}
			
			
			
		}
	}else{
		document.getElementById('link'+i).href='#'; 
		$('#price'+i).val(0);
		$('#uom1'+i).text('');
		$('#uom2'+i).text('');
		
	}
	
	total_amount_f();
	
}



function total_amount_f(){

	var total_amount = 0;
	
	for(var i=1;i<=item_count;i++){
		
		var is_del = $('#is_del'+i).val();
		
		if(is_del != 1){
		
			var qty_ord = $('#qty_ord'+i).val();
			var qty_amend = $('#qty_amend'+i).val();
			
			var qty = qty_ord;
			if(qty_amend != '' && qty_amend != 0 && !isNaN(qty_amend)){
				qty = qty_amend;
			}
			
			var price = $('#price'+i).val();
			
			if(qty != '' && !isNaN(qty)){
				total_amount += parseFloat(price)*parseFloat(qty);
			}
		
		}
		
	}
	
	var gst_amount = (total_amount*gst)/100;
	
	$('#gst_amount').val(gst_amount.toFixed(2));
	$('#total_amount').val(total_amount.toFixed(2));
	$('#nett_amount').val((total_amount+gst_amount).toFixed(2));
	
}

function product_model(i){
	
	product_name = $('#product'+i).val();
	var model_sel = '';
	
	
		if(product_name != '-'){
			for(var j=0;j<product.length;j++){
				if(product[j].product_name == product_name){
					model_sel +='<option value="'+product[j].id+'">'+product[j].model_name+'</option>';
					
				}
			}
			
			$('#model_name'+i).html('');
			$('#model_name'+i).append(model_sel);
			$('#price'+i).val('');
			if(mode=='Add'){
			$('#qty'+i).val('');
			}
			
		}else{
			$('#model_name'+i).html('<option>-</option>');
			$('#model_name2_'+i).val('');
			$('#qty_balance'+i).val('');	
			$('#price'+i).val('');
			if(mode=='Add'){
			$('#qty'+i).val('');
			}
		}
		
		$('#uom1'+i).text('');
		$('#uom2'+i).text('');
	
	
}

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

function validate(){
	var error = 0;
	
	
	//item
	$('.item').each(function() {

		//is del
		var is_del = $(this).closest('tr').find('.is_del').val();
		var uom = $(this).closest('tr').find('.uom').val();
		
		if(is_del != 1){
		
			var name = $(this).data('name');
			if($(this).val() == '' || $(this).val() == '-'){
				alert(name+' can\'t be blank');
				$(this).focus();
				error = 1;
				return false;
				
			}else{
				
				if(name == 'Quantity' && $(this).val() %1!=0 && uom != 'kg'){
					
					alert(name+' wrong format');
					$(this).focus();
					error = 1;
					return false;
					
				}
				
			}
		
		}
		
    });
	
	if(error == 1){
		return false;
	}
		
}

</script>