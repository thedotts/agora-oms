

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-usd fa-fw"></i> Invoice
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input class="form-control" placeholder="Search">
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </form>
                        </div>
                		<!-- /.col-lg-3 -->
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <a href="create-invoice.html"><button class="btn btn-default btn-sm" type="button">Create Invoice</button></a><p></p>
                    <div class="panel panel-default">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Invoice No.</th>
                                            <th>Job ID</th>
                                            <th>Customer</th>
                                            <th>Last Updated</th>
                                            <th>Last Updated By</th>
                                            <th>Awaiting</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td><a href="payment.html">AI-140001</a></td>
                                            <td>JN-00008</td>
                                            <td>VWX Company</td>
                                            <td>09/07/14</td>
                                            <td>Ms Finance</td>
                                            <td>Finance</td>
                                            <td>Awaiting Payment</td>
                                            <td>
                                                <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button"><i class="fa fa-edit"></i></button>
                                                <button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button></td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>AI-140001</td>
                                            <td>JN-00009</td>
                                            <td>YX Company</td>
                                            <td>01/07/14</td>
                                            <td>Ms Finance</td>
                                            <td>-</td>
                                            <td>Completed</td>
                                            <td>
                                                <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View" type="button"><i class="fa fa-folder-open"></i></button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
