<div id="page-wrapper">
        	<form method="post" action="<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_submit')?>" onsubmit="return validate();">
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <input type="hidden" name="id" value="<?=$id?>"/>
            
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-pencil-square-o fa-fw"></i> Manage Promotions
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Promotions Content
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <textarea class="details" id="details" name="details"><?=$result['json_content']?></textarea>
                                    <?php echo $this->ckeditor->replace("details"); ?>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            
            
            <div class="row">
                <div class="col-lg-12">
                    
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->
        
<script>
var alert_v = <?=$alert?>;
		
		switch(alert_v){
			case 1:
				alert('Record has been created successfully');
				break;
			case 2:
				alert('Record has been updated successfully');
				break;
			case 3:
				alert('Record has been deleted successfully');
				break;
		}
</script>