</div>
    <!-- /#wrapper -->
    </div>
    <div class="site-footer">
      <span>Pac-Fung Agora Pte Ltd &copy; 2015 </span>
    </div>

    <!-- Core Scripts - Include with every page -->    
    <script src="<?=base_url('assets/anexus/js/bootstrap.min.js')?>"></script>
    <script src="<?=base_url('assets/anexus/js/plugins/metisMenu/jquery.metisMenu.js')?>"></script>

    <!-- Page-Level Plugin Scripts - Dashboard -->
    <script src="<?=base_url('assets/anexus/js/plugins/morris/raphael-2.1.0.min.js')?>"></script>
    <script src="<?=base_url('assets/anexus/js/plugins/morris/morris.js')?>"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="<?=base_url('assets/anexus/js/sb-admin.js')?>"></script>

    <!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
    <script src="<?=base_url('assets/anexus/js/demo/dashboard-demo.js')?>"></script>
    
    <!--<script src="<?=base_url('assets/js/bootstrap3-typeahead.js')?>"></script>
    <script src="<?=base_url('assets/js/Gruntfile.js')?>"></script>-->
    
    <!-- Page-Level Demo Scripts - Notifications - Use for reference -->
    <script>
    // tooltip demo
    $('.table').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    })

    // popover demo
    $("[data-toggle=popover]")
        .popover()
	
    </script>

</body>

</html>