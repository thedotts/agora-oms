<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-clipboard fa-fw"></i> Reports     
                        
                       <div class="col-lg-3 pull-right">
                            <form role="form">
                                <div class="form-group">
                                    <select id="type" onchange="changeType(this.value)" class="form-control">
                                        <option value="1">Customer Orders</option>
                                        <option value="2">Product Summary</option>                                   
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </form>
                        </div>
                        <!-- /.col-lg-3 -->  

                    </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                                
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Filter Options
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body" id="option">                                
                                                        
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->

                    <div class="panel panel-default">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="table">
                                    
                                    
                                </table>
                      </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

<script>

var type=1;
var role = <?=json_encode($userdata['role_id'])?>;
var year = <?=json_encode($order_year)?>; 
var staff = <?=json_encode($sales)?>;
var customer = <?=json_encode($customer)?>;

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

var year_opt ='';
for(i=0;i<year.length;i++){
	year_opt +='<option value="'+year[i].year+'">'+year[i].year+'</option>';
}

//if not manager
if(inArray(2,role)){
	var staff_opt='<option value="all">All</option>';
}else{
	var staff_opt='';
}

for(i=0;i<staff.length;i++){
	staff_opt +='<option value="'+staff[i].id+'">'+staff[i].full_name+'</option>';
}

if(inArray(2,role)){
	var customer_opt='<option value="all">All</option>';
}else{
	var customer_opt='';
}

for(i=0;i<customer.length;i++){
	customer_opt +='<option value="'+customer[i].id+'">'+customer[i].company_name+'</option>';
}

changeType(type);
getOrder();

function changeType(sel){
	
	if(sel == 1){
		html ='<div class="form-group col-lg-3"><label>Month / Quarter</label><select id="month" class="form-control" onchange="getOrder()"><option value="all">All</option><optgroup label="Month"><option value="1">January</option><option value="2">February</option><option value="3">March</option><option value="4">April</option><option value="5">May</option><option value="6">June</option><option value="7">July</option><option value="8">August</option><option value="9">September</option><option value="10">October</option><option value="11">November</option><option value="12">December</option></optgroup><optgroup label="Quarter"><option value="1q">1st Quarter</option><option value="2q">2nd Quarter</option><option value="3q">3rd Quarter</option><option value="4q">4th Quarter</option></optgroup></select></div><div class="form-group col-lg-3"><label>Year</label><select id="year" class="form-control" onchange="getOrder()"><option value="all">All</option>'+year_opt+'</select></div><div class="form-group col-lg-3" id="staff_div"><label>Sales Staff</label><select id="staff" class="form-control" onchange="getOrder()">'+staff_opt+'</select></div>';	
	}else{
		html ='<div class="form-group col-lg-3"><label>Customer</label><select onchange="getProduct()" id="customer" class="form-control">'+customer_opt+'</select></div><div class="form-group col-lg-3"><label>Start Date</label><input id="startdate" type="text" class="form-control" value=""></div><div class="form-group col-lg-3"><label>End Date</label><input id="enddate" type="text" class="form-control" value=""></div>';
	}
	
	$('#option').html('');
	$('#option').append(html);
	
	//set date
	var d = new Date();
	var currMonth = d.getMonth();
	var currYear = d.getFullYear();
	var startDate = new Date(currYear,currMonth,1);
	var endDate = new Date(currYear,currMonth+1,0);
	
	$( "#startdate" ).datepicker({
		dateFormat:'dd/mm/yy',
		onSelect: function(dateText) {
			getProduct()
		}
	});
	
	$( "#startdate" ).datepicker( "setDate", startDate );
	
	$( "#enddate" ).datepicker({
		dateFormat:'dd/mm/yy',
		onSelect: function(dateText) {
			getProduct()
		}
	});
	
	$( "#enddate" ).datepicker( "setDate", endDate );
	
	//ajax
	if(sel == 1){
		getOrder();	
	}else{
		getProduct();
	}
	
}

function getOrder(){
	
	var month = $('#month').val();
	var year = $('#year').val();
	var staff = $('#staff').val();
	
	$.ajax({
	  method: "POST",
	  url: "<?=base_url($init['langu'].'/agora/manager/order_report')?>",
	  data: {"month": month, "year":year, "staff":staff},
	  dataType: "JSON"
	})
	.done(function(r) {
		
		if(r.status == 'OK'){
			
				if(r.data.length > 0){
				
				html_tmp ='<thead><tr><th>#</th><th>Order No.</th><th>Customer</th><th>Order Date</th><th>Sales Staff</th><th>Status</th><th>Action</th></tr></thead><tbody>';
				
				for(var i=0;i<r.data.length;i++){
					
					html_tmp +='<tr><td>'+(i+1)+'</td><td>'+r.data[i].order_no+'</td><td>'+r.data[i].customer_name+'</td><td>'+r.data[i].created_date+'</td><td>'+r.data[i].employee_name+'</td><td>'+r.data[i].status_name+'</td><td><a href="'+'<?=base_url('en/agora/sales/order_edit/')?>'+'/'+r.data[i].id+'"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View" type="button"><i class="fa fa-folder-open"></i></button></a></td></tr>';
					
				}
				
				html_tmp += '</tbody>';
				
				
				}else{
				html_tmp = '<td>No Data</td>';	
				}
				
				$('#table').html('');
				$('#table').append(html_tmp);
						
		}
		
	});
	
	//alert(month+year+staff);	
}

function getProduct(){
	var customer = $('#customer').val();
	var start = $('#startdate').val();
	var end = $('#enddate').val();
	
	$.ajax({
	  method: "POST",
	  url: "<?=base_url($init['langu'].'/agora/manager/product_report')?>",
	  data: {"customer": customer, "start":start, "end":end},
	  dataType: "JSON"
	})
	.done(function(r) {
		
		if(r.status == 'OK'){
			
			if(r.data.length > 0){
				
				html_tmp ='<thead><tr><th>#</th><th>Product No.</th><th>Product Name</th><th>Product Model</th><th>Product Category</th><th>Qty</th><th>Supplier</th><th>Customer</th><th width="100px">Action</th></tr></thead><tbody>';
				console.log(r);
				for(var i=0;i<r.data.length;i++){
					
					html_tmp +='<tr><td>'+(i+1)+'</td><td>'+r.data[i].product_no+'</td><td>'+r.data[i].product_name+'</td><td>'+r.data[i].model_name+'</td><td>'+r.data[i].product_category+'</td><td>'+r.data[i].total_qty+'</td><td>'+r.data[i].supplier+'</td><td>'+r.data[i].customer_name+'</td><td><a href="'+'<?=base_url('en/agora/administrator/products_edit/')?>'+'/'+r.data[i].product_id+'"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View" type="button"><i class="fa fa-folder-open"></i></button></a></td></tr>';
					
				}
				
				html_tmp += '</tbody>';
				
				
				}else{
				html_tmp = '<td>No Data</td>';	
				}
				
				$('#table').html('');
				$('#table').append(html_tmp);
				
		}
		
	});
	
	
	//alert(customer+start+end);
	
}

if(!inArray(2,role)){
	$('#staff_div').hide();	
}

</script>