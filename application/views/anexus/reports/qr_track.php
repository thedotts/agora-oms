

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-user fa-fw"></i> All Reports</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">                    
                    <div class="panel panel-default">
                    	
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">                                    
                                    <tbody>
                                    	<tr>
                                        	<th>Report Name</th>
                                            <td>QR Track</td>
                                        </tr>
                                    	<tr>
                                            <th>Start Date</th>
                                            <td><input type="text" class="form-control" id="startdate" value="<?=isset($startdate)?$startdate:date("01-m-Y")?>"/></td>                                            
                                        </tr>
                                        <tr>
                                            <th>End Date</th>
                                            <td><input type="text" class="form-control" id="enddate" value="<?=isset($enddate)?$enddate:date("t-m-Y")?>"/></td>                                            
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <td><input type="button" class="btn btn-primary" value="Export" onClick="toExport()"/></td>                                            
                                        </tr>
                                                                            
                                    </tbody>
                                </table>
                                
                                
          </div>
                            <!-- /.table-responsive -->
                    </div>
                    
                    
                </div>
                <!-- /.col-lg-12 -->
            </div>
<script>
$(document).ready(function(){
	$( "#startdate" ).datepicker({
		dateFormat:'dd-mm-yy',
	});
	
	$( "#enddate" ).datepicker({
		dateFormat:'dd-mm-yy',
	});
});

function toExport(){
	var startdate = $("#startdate").val();
	var enddate = $("#enddate").val();
	
	if(startdate!='' && enddate != '') {
		location.href='<?=base_url('en/anexus/'.$model_name.'/qr_track')?>/'+encodeURI(startdate)+'/'+encodeURI(enddate);
	}
	
}
</script>            
            
        