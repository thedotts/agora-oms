<div id="page-wrapper">
        	<form method="post" action="<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_submit')?>" onsubmit="return validate();">
            <input type="hidden" id="id" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-pencil-square-o fa-fw"></i> <?=$mode=='Edit'?'Edit':'Create'?> Employee Account
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Account Information
                        </div>
                        <div class="panel-body">
                        		<div class="form-group">
                                    <label>Email *</label>
                                    <input onblur="check_email()" id="email" name="email" class="form-control" value="<?=$mode=='Edit'?$result['email']:''?>" <?=$mode=='Edit'?'readonly':''?>>
                                    <input id="email_check" type="hidden" value="0">
                                    <span style="color:red" id="email_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Employee No.</label>
                                    <input class="form-control" disabled value="<?=$mode=='Edit'?$result['employee_no']:''?>">
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Role *</label>
                                    	<?php
										foreach($role_list as $k=>$v) {
											if($k != 7 && $k != 6){
										?>
                                        <div class="checkbox">
                                        <label>
                                            <input name="role[]" type="checkbox" value="<?=$k?>" <?=$mode=='Edit'&&in_array($k,$result['role_id'])?'checked':''?>><?=$v?>
                                        </label>
                                    	</div>
                                       
                                        <?php
										}}
										?> 
                                    <span style="color:red" id="role_err"></span>                                       
                                </div>
                                <div class="form-group">
                                    <label>Status *</label>
                                    <select name="status" class="form-control">
                                    	<?php
										foreach($status_list as $k=>$v) {
										?>
                                        <option value="<?=$k?>" <?=$mode=='Edit'&&$k==$result['status']?'selected="selected"':''?>><?=$v?></option>
                                        <?php
										}
										?>                                         
                                    </select>
                                    <span style="color:red" id="status_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Password *</label>
                                    <input type="password" id="pwd" name="password" class="form-control">
                                    <span style="color:red" id="pwd_err"></span>
                                    <?=$mode=='Edit'?'<p class="help-block"><i>Leave Blank if no change</i></p>':''?>
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password *</label>
                                    <input type="password" id="copwd" name="copassword" class="form-control">
                                    <span style="color:red" id="copwd_err"></span>
                                    <?=$mode=='Edit'?'<p class="help-block"><i>Leave Blank if no change</i></p>':''?>
                                    
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                </div>
                <!-- /.col-lg-12 -->
                
                <div class="col-lg-12">
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Personal Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Full Name *</label>
                                    <input id="full_name" name="full_name" class="form-control" value="<?=$mode=='Edit'?$result['full_name']:''?>">
                                    <span style="color:red" id="full_name_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>IC No.</label>
                                    <input name="ic_no" class="form-control" value="<?=$mode=='Edit'?$result['ic_no']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Date of Birth</label>
                                    <input id="dob" name="date_of_birth" class="form-control" placeholder="Calendar Input" value="<?=$mode=='Edit'?date("d/m/Y", strtotime($result['date_of_birth'])):''?>">
                                </div>
                                <div class="form-group">
                                    <label>Home Address</label>
                                    <input name="home_address" class="form-control" value="<?=$mode=='Edit'?$result['home_address']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Contact No.</label>
                                    <input name="contact_info" class="form-control" value="<?=$mode=='Edit'?$result['contact_info']:''?>">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            
            
            <div class="row">
                <div class="col-lg-12">
                    
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->

<script>
var mode = '<?=$mode?>';
var role = <?=json_encode($userdata['role_id'])?>;

$(document).ready(function(e) {
	$("#dob").datepicker({"dateFormat":"dd/mm/yy"}); 
	$("#expired_date").datepicker({"dateFormat":"dd/mm/yy"});    
});

if(mode=='Edit'){
	check_email();
}


function validate(){
	
	var error = 0;
	
	//email
	var email_check = $('#email_check').val();
	if(email_check == 0){
		check_email();
		error = 1;
	}
	
	
	//role
	var role_isset = 0;
	$('input[name="role[]"]').each(function() {
        if($(this).prop('checked')){
			role_isset = 1;
		} 
    });
	
	if(role_isset == 0){
		$('#role_err').text('Select at least one role');
		error = 1;
	}else{
		$('#role_err').text('');
	}
	
	
	//pwd
	if(mode == 'Add'){
		if($('#pwd').val() == ''){
			$('#pwd_err').text('Password can\'t be blank');
			error = 1;
		}else{
			$('#pwd_err').text('');
		}
	}
	
	//co password
	if($("#pwd").val()!=$("#copwd").val()) {
		$('#copwd_err').text('Password is not same with confirm password');
		error = 1;
	}else{
		$('#copwd_err').text('');
	}
	
	//full name
	if($("#full_name").val() == '') {
		$('#full_name_err').text('Full name can\'t be blank');
		error = 1;
	}else{
		$('#full_name_err').text('');
	}
	
	if(error == 1){
		return false;
	}
	
}

if(mode != 'Add' && !inArray(1, role)){

$(".form-control").each(function() {
	$(this).prop('disabled', 'disabled');
});

//$('#pwd').prop('disabled', false);
//$('#copwd').prop('disabled', false);

}

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

function check_email(){
	
	email = $("#email").val();
	id = $("#id").val();
	term = {"email":email,"id":id};
	url = '<?=base_url($init['langu'].'/agora/administrator/employee_check_email')?>';
	
	if(validateEmail(email)){
	
		$.ajax({
			type:"POST",
			url: url,
			data: term,
			dataType : "json"
			}).done(function(r) {
				if(r.status == "ok") {
					
					if(r.email == 1){
						$('#email_err').text('Email not available');
						$('#email_check').val(0);
					}else{
						$('#email_err').text('');	
						$('#email_check').val(1);
					}
					
				}
		});	
	
	}else{
		$('#email_err').text('Wrong email format');
		$('#email_check').val(0);
	}
}

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

</script>