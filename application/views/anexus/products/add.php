<link rel="stylesheet" href="<?=base_url('assets/css/jquery-ui.css')?>">
<script src="<?=base_url('assets/js/jquery-ui.js')?>"></script>
<style>
.ui-datepicker-year{
	color:#000;	
}
</style>

<div id="page-wrapper">
        	<form method="post" action="<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_submit')?>" onsubmit="return validate();" enctype="multipart/form-data">
            <input type="hidden" id="id" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-truck fa-fw"></i> <?=$mode=='Edit'?'Edit':($mode=="View"?'View':'New')?> Product
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
             <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Product Information
                        </div>
                        <div class="panel-body">
                        		
                                <div class="form-group">
                                    <label>Product No.</label>
                                    <input class="form-control" value="<?=$mode!='Add'?$result['product_no']:''?>" disabled>
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Product Category</label>
                                    <select class="form-control" id="product_category" name="product_category">
                                    	<option value="0">-</option>
                                        <?php foreach($category_list as $k => $v){ ?>
                                        	<option value="<?=$k?>" <?=$mode!='Add'?($k==$result['product_category']?'selected':''):''?>><?=$v?></option>
                                        <? }?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Product Name *</label>
                                    <input class="form-control" id="product_name" name="product_name" onblur="check_product(this.value)" value="<?=$mode!='Add'?$result['product_name']:''?>">
                                    <span style="color:red" id="product_name_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Product Description</label>
                                    <textarea class="form-control" id="product_desc" name="product_desc"><?=$mode!='Add'?$result['description']:''?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Status *</label>
                                    <select class="form-control" id="status" name="status">
                                        <option value="1" <?=$mode!='Add'&&$result['status']==1?'selected':''?>>Active</option>
                                        <option value="0" <?=$mode!='Add'&&$result['status']==0?'selected':''?>>Suspended</option>
                                    </select>
                                </div>
                                <div class="form-group" <?=$mode=='View'?'style="display:none"':''?>>
                                    <label>Product Image</label>
                                    <input id="pic" name="pic" type="file"/>
                                    <input id="old_pic" name="old_pic" type="hidden" value="<?=$mode=='Edit'?$result['pic_path']:''?>"/>
                                    <?php if($mode=='Edit'){?>
                                    <a href="<?=$result['pic_path']?>" target="_blank"><?=$result['pic_path']?></a>
                                    <?php }?>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Product Details
                        </div>
                        <div class="panel-body">
                                <div class="form-group" style="display:none">
                                    <label>Country *</label>
                                    <input class="form-control" id="model_name" name="model_name" value="Singapore">
                                    <span style="color:red" id="model_name_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Supplier</label>
                                    <input class="form-control" id="supplier" name="supplier" value="<?=$mode!='Add'?$result['supplier']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Quantity</label>
                                    <input class="form-control" id="qty" name="qty" value="<?=$mode!='Add'?$current_qty:'0'?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Selling Price *</label>
                                    <input class="form-control" id="selling_price" name="selling_price" value="<?=$mode!='Add'?(isset($s_price)?number_format($s_price,2):$result['selling_price']):''?>">
                                    <span style="color:red" id="selling_price_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Promo Start Date</label>
                                    <input class="form-control" id="p_start_date" name="p_start_date" value="<?=$mode!='Add'?date('d/m/Y',strtotime($result['p_start_date'])):''?>">
                                </div>
                                <div class="form-group">
                                    <label>Promo End Date</label>
                                    <input class="form-control" id="p_end_date" name="p_end_date" value="<?=$mode!='Add'?date('d/m/Y',strtotime($result['p_end_date'])):''?>">
                                </div>
                                <div class="form-group">
                                    <label>Promo Qty</label>
                                    <input class="form-control" id="p_qty" name="p_qty" value="<?=$mode!='Add'?$result['p_qty']:''?>">
                                </div>
                                
                                <div class="form-group">
                                    <label>Promo Price</label>
                                    <input class="form-control" id="promo_price" name="promo_price" value="<?=$mode!='Add'?($result['promo_price']!=0.00?$result['promo_price']:''):''?>">
                                    <p class="help-block"><i>Fill in to activate promotion</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Customer UOM *</label>
                                    <select class="form-control" id="uom" name="uom">
                                        <option value="each" <?=$mode!='Add'&&$result['uom']=='each'?'selected':''?>>Each</option>
                                        <option value="pcs" <?=$mode!='Add'&&$result['uom']=='pcs'?'selected':''?>>Pcs</option>
                                        <option value="ctn" <?=$mode!='Add'&&$result['uom']=='ctn'?'selected':''?>>Ctn</option>
                                        <option value="g" <?=$mode!='Add'&&$result['uom']=='g'?'selected':''?>>G</option>
                                        <option value="kg" <?=$mode!='Add'&&$result['uom']=='kg'?'selected':''?>>Kg</option>
                                        <option value="pkt" <?=$mode!='Add'&&$result['uom']=='pkt'?'selected':''?>>Pkt</option>
                                        <option value="pun" <?=$mode!='Add'&&$result['uom']=='pun'?'selected':''?>>Pun</option>
                                        <option value="comb" <?=$mode!='Add'&&$result['uom']=='comb'?'selected':''?>>Comb</option>
                                        <option value="bot" <?=$mode!='Add'&&$result['uom']=='bot'?'selected':''?>>Bot</option>
                                        <option value="tray" <?=$mode!='Add'&&$result['uom']=='tray'?'selected':''?>>Tray</option>
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                    <label>Inventory UOM *</label>
                                    <select class="form-control" id="inventory_uom" name="inventory_uom">
                                        <option value="each" <?=$mode!='Add'&&$result['inventory_uom']=='each'?'selected':''?>>Each</option>
                                        <option value="pcs" <?=$mode!='Add'&&$result['inventory_uom']=='pcs'?'selected':''?>>Pcs</option>
                                        <option value="ctn" <?=$mode!='Add'&&$result['inventory_uom']=='ctn'?'selected':''?>>Ctn</option>
                                        <option value="g" <?=$mode!='Add'&&$result['inventory_uom']=='g'?'selected':''?>>G</option>
                                        <option value="kg" <?=$mode!='Add'&&$result['inventory_uom']=='kg'?'selected':''?>>Kg</option>
                                        <option value="pkt" <?=$mode!='Add'&&$result['inventory_uom']=='pkt'?'selected':''?>>Pkt</option>
                                        <option value="pun" <?=$mode!='Add'&&$result['inventory_uom']=='pun'?'selected':''?>>Pun</option>
                                        <option value="comb" <?=$mode!='Add'&&$result['inventory_uom']=='comb'?'selected':''?>>Comb</option>
                                        <option value="bot" <?=$mode!='Add'&&$result['inventory_uom']=='bot'?'selected':''?>>Bot</option>
                                        <option value="tray" <?=$mode!='Add'&&$result['inventory_uom']=='tray'?'selected':''?>>Tray</option>
                                    </select>
                                </div>
                                
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    
                    
                </div>
                <!-- /.col-lg-6 -->
                
            </div>
            <!-- /.row -->
            
            <div class="panel panel-primary">
                        <div class="panel-heading">
                             Don’t Show Promo to (Exception) list of customers
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Customer</th>
                                            <th width="80px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="item_product">
                                        
                                        <?php foreach($customer_list as $k => $v){?>
                                        
                                        	<tr>
                                            	<td><?=$v['company_name']?></td>
                                                <td><input name="no_promo[]" type="checkbox" value="<?=$v['id']?>" <?=$mode=="Edit"?(in_array($v['id'],$result['no_promo_id'])?'checked':''):''?>/></td>
                                            </tr>
                                        
                                        <?php }?>
                                        
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
            
            <div class="col-lg-3">
            <div class="form-group">
            <label>Start Date</label>
            <input class="form-control" placeholder="Calendar Input" id="start_date" name="start_date" value="<?=date('01/m/Y')?>"/>
            </div>
            </div>
            
            <div class="col-lg-3">
            <div class="form-group">
            <label>End Date</label>
            <input class="form-control" placeholder="Calendar Input" id="end_date" name="end_date" value="<?=date('t/m/Y')?>" />
            </div>
            </div>
            
            
            <div class="row" <?=$mode=='View'?'style="display:none"':''?>>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Inventory Log 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                            	<button class="btn btn-default btn-sm" type="button" data-toggle="modal" data-target="#myModal">Update Inventory</button>

                                
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Update Type</th>
                                            <th>User Name</th>
                                            <th>Quantity</th>
                                            <th>Date/Time</th>
                                            <th>Reason (if any)</th>
                                        </tr>
                                    </thead>
                                    <tbody id="update_log">
                                    
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                    
                    <textarea style="display:none" id="qty_json" name="qty_json"></textarea>
                    <?php if(in_array(8,$userdata['role_id']) || in_array(1,$userdata['role_id'])){?>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                    <?php } ?>

                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            
            </form>
            
        </div>
        <!-- /#page-wrapper -->
        
        
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
           <div class="modal-dialog">
           <div class="modal-content">
           <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
           <h4 class="modal-title" id="myModalLabel">Update Inventory to <span id="product_name2"></span></h4>
           </div>
           <div class="modal-body">
           <table class="table table-striped table-bordered table-hover" id="dataTables">
           <thead>
           <tr>
           <th>Update Type *</th>
           <th width="200px">Quantity *</th>
           <th>Reason</th>
           </tr>
           </thead>
           <tbody>
           <tr>
           <td>
           <div class="radio">
           <label>
           <input type="radio" name="updatetype" id="optionsRadios1" value="option1" checked>Add Inventory
           </label>
           </div>
           <div class="radio">
           <label>
           <input type="radio" name="updatetype" id="optionsRadios2" value="option2">Deduct Inventory
           </label>
           </div>
           </td>
           <td>
           <div class="form-group input-group">
           <input id="qty2" type="text" class="form-control" value="0">
           <span id="uom_val" class="input-group-addon"> Carton(s)
           </span>
           </div>
           </td>
           <td><textarea id="reason" class="form-control"></textarea>
           </td>
           </tr>
           </tbody>
           </table>
           </div>
           <div class="modal-footer">
           <button onclick="updateQty()" type="button" class="btn btn-primary">Update</button>
           <button type="cancel" class="btn btn-default" data-dismiss="modal">Close</button>
           </div>
           </div>
           <!-- /.modal-content -->
           </div>
           <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script>
var role = <?=json_encode($userdata['role_id'])?>;

$(function(){
	if(mode=='Edit'){
	ajax_inventoty_log();
	}
});

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

if(!inArray(1,role) && !inArray(8,role)){
	$(".form-control").each(function() {
		$(this).prop('disabled', 'disabled');
	});
}



	$( "#p_start_date" ).datepicker({
	dateFormat:'dd/mm/yy',
	changeYear: true,
	yearRange: "-50:+50", // last hundred years
	});
	
	$( "#p_end_date" ).datepicker({
	dateFormat:'dd/mm/yy',
	changeYear: true,
	yearRange: "-50:+50", // last hundred years
	});




$( "#start_date" ).datepicker({
	dateFormat:'dd/mm/yy',
	onSelect: function(){
		ajax_inventoty_log();	
    }
});

$( "#end_date" ).datepicker({
	dateFormat:'dd/mm/yy',
	onSelect: function(){
		ajax_inventoty_log();	
    }
});

var counter = 0;
function ajax_inventoty_log(){
	var id = <?=$mode=='Edit'?$result['id']:0?>;
	var start_date = $('#start_date').val();
	var end_date = $('#end_date').val();
	
	if(start_date != '' && end_date != ''){
	
		if(start_date != ''){
			start_date = start_date.split('/');
			start_date = start_date[2]+'-'+start_date[1]+'-'+start_date[0];
		}
		
		if(end_date != ''){
			end_date = end_date.split('/');
			end_date = end_date[2]+'-'+end_date[1]+'-'+end_date[0];
		}
		
			//ajax
			term = {'id':id, 'start_date':start_date, 'end_date':end_date};
			url = '<?=base_url($init['langu'].'/agora/administrator/ajax_inventory_log')?>';
			
			$.ajax({
				type:"POST",
				url: url,
				data: term,
				dataType : "json"
			}).done(function(r) {
				if(r.status == 'ok'){
					$('#update_log').html('');
					if(r.data.length > 0){
						
						for(var i=0;i<r.data.length;i++){
						
						//only admin and sales can edit reason
						var disabled_val = '';
						if(!inArray(1,role) && !inArray(8,role)) {
							disabled_val = 'disabled';
						}
						
						tmp ='<tr>';
						tmp +='<td>'+(i+1)+'</td>';
						tmp +='<td>'+r.data[i].type+'</td>';
						tmp +='<td>'+r.data[i].employee_no+' - '+r.data[i].employee_name+'</td>';
						tmp +='<td>'+r.data[i].qty+'</td>';
						tmp +='<td>'+r.data[i].created_date+'</td>';
						tmp +='<td><textarea class="form-control" id="reason'+r.data[i].id+'" onblur="updateReason('+r.data[i].id+')" '+disabled_val+'>'+r.data[i].reason+'</textarea></td></tr>';
						
						$('#update_log').append(tmp);
						
						counter = r.data.length;

						}
						
					}
					
					
					
				}
			});	
		
	
	}
	
}

function firstToUpperCase( str ) {
    return str.substr(0, 1).toUpperCase() + str.substr(1);
}


//$('#start_date').datepicker('setDate', 'today');
//$('#end_date').datepicker('setDate', 'today');

$('#myModal').on('show.bs.modal', function (event) {
	var name = $('#product_name').val();
	
	$('#product_name2').text(name);
	
	//clear input text
	$('#qty2').val(0);
	$('#reason').val('');
	
	var uom = $('#inventory_uom').val();
	uom = firstToUpperCase( uom );
	$('#uom_val').text(uom+'(s)');

});

var mode = '<?=$mode?>';
var qty_tmp = [];
var now = '<?=date("Y/m/d H:i")?>';//show to user
var now2 = '<?=date("Y-m-d H:i:s")?>';//for database

var qty_total = <?=$mode=='Edit'?$current_qty:0?>;

function updateQty(){
	
	if(mode == 'Add'){
		
		var qty = $('#qty2').val();
		var reason = $('#reason').val();
		if($('#optionsRadios1').is(':checked')){ 
			var type_qty = '+'+qty;
		}else{
			var type_qty = '-'+qty;
		}
		
		//sum total qty
		qty_total += type_qty;
		qty_total = eval(qty_total);
		$('#qty').val(qty_total);
		
		if(qty != 0 && qty != ''){
			
			var data = {qty:qty,reason:reason,time:now2};
			qty_tmp.push(data);
			counter++;
			
			//check type -/+
			if(type_qty.indexOf('+') != -1 ){
				type = 'Add Inventory';	
			}else{
				type = 'Deduct Inventory';	
			}
			
			tmp ='<tr>';
			tmp +='<td>'+counter+'</td>';
			tmp +='<td>'+type+'</td>';
			tmp +='<td>'+'<?=$staff_info['employee_no'].' - '.$staff_info['full_name']?>'+'</td>';
			tmp +='<td>'+qty+'</td>';
			tmp +='<td>'+now+'</td>';
			tmp +='<td><textarea class="form-control" disabled>'+reason+'</textarea></td></tr>';
			
			$('#update_log').append(tmp);
			$('#myModal').modal('hide');
			
			$('#qty_json').text(JSON.stringify(qty_tmp));
			
		}else{
		alert('Quantity cant be blank or zero');
		}
		
		console.log(qty_tmp);
	
	}else{
		
		var id = $('#id').val();
		var qty = $('#qty2').val();
		var reason = $('#reason').val();
		
		if($('#optionsRadios1').is(':checked')){ 
			var type_qty = '+'+qty;
		}else{
			var type_qty = '-'+qty;
		}
		
		//sum total qty
		qty_total += type_qty;
		qty_total = eval(qty_total);
		$('#qty').val(qty_total);
		
		if(qty != 0 && qty != ''){
			
			term = {id:id, qty:type_qty, reason:reason};
			url = '<?=base_url($init['langu'].'/agora/administrator/update_qty')?>';
			
			$.ajax({
				type:"POST",
				url: url,
				data: term,
				dataType : "json"
			}).done(function(r) {
				if(r.status == 'OK'){
					
					counter++;
				
					//check type -/+
					if(type_qty.indexOf('+') != -1 ){
						type = 'Add Inventory';	
					}else{
						type = 'Deduct Inventory';	
					}
					
					tmp ='<tr>';
					tmp +='<td>'+counter+'</td>';
					tmp +='<td>'+type+'</td>';
					tmp +='<td>'+'<?=$staff_info['employee_no'].' - '.$staff_info['full_name']?>'+'</td>';
					tmp +='<td>'+qty+'</td>';
					tmp +='<td>'+now+'</td>';
					tmp +='<td><textarea class="form-control" disabled>'+reason+'</textarea></td></tr>';
					
					$('#update_log').append(tmp);

					$('#myModal').modal('hide');
					alert('Update Inventory Successful');
				}
			});	
		
		}else{
		alert('Quantity cant be blank or zero');
		}
		
	}
	
}

$(function() {
    var availableTags = <?=isset($product_name)?$product_name:'[]'?>;
    $( "#product_name" ).autocomplete({
      source: availableTags
    });
  });
  
function updateReason(id){
	
			var reason = $("#reason"+id).val();
			
			$.ajax({
				type:"POST",
				url: "<?=base_url('en/agora/administrator/update_reason')?>",
				data: {"id":id,"reason":reason},
				dataType : "json"
			}).done(function(r) {
				
				if(r.status == 'ok'){
					alert("Reason has been updated successfully");
				}
				
			});	
}  


function validate(){
	
	var error = 0;
	
	//product name
	if($("#product_name").val() == '') {
		$('#product_name_err').text('Product name can\'t be blank');
		error = 1;
	}else{
		$('#company_name_err').text('');
	}
	//model_name
	if($("#model_name").val() == '') {
		$('#model_name_err').text('Description  can\'t be blank');
		error = 1;
	}else{
		$('#company_name_err').text('');
	}
	//selling_price
	if($("#selling_price").val() == '') {
		$('#selling_price_err').text('Selling price can\'t be blank');
		error = 1;
	}else{
		
		if(isNaN($("#selling_price").val())) {
			$('#selling_price_err').text('Selling price needs to be integer only');
			error = 1;
		}else{
			$('#selling_price_err').text('');
		}
	}
	
	
	if(error == 1){
		return false;
	}
	
}


function check_product(val){
	
	if(val != ''){
		
		$.ajax({
		  method: "POST",
          url: "<?=base_url($init['langu'].'//agora/administrator/products_check/')?>",
          dataType: "json",
          data: {"name":val},
          success: function( r ) {
            console.log(r);
			if(r.status == 'ok'){
				
				if(r.isset == 1){
					$('#product_name').val('');
					alert('Product name not available');
				}
				
			}
			
			
          }
        });
		
	}
		
}

</script>