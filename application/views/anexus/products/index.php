

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-truck fa-fw"></i> Products / Inventory
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input id="q" class="form-control" placeholder="Search" onblur="filter()" value="<?=$q?>">
                                </div>
                                <!-- /.form-group -->
                            </form>
                            <script>
							function filter(){								
								var q = $("#q").val();
								if(q == '') {
									q = 'ALL';	
								}								
								location.href='<?=base_url('en/agora/'.$group_name.'/'.$model_name)?>/'+q;
							}
							</script>
                        </div>
                		<!-- /.col-lg-3 -->
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <a href="<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_add')?>"><button class="btn btn-default btn-sm" type="button">Create Product </button></a><p></p>
                    <div class="panel panel-default hidden-xs">
                    	<?php
								if(!empty($results)) {
								?>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Product No.</th>
                                            <th>Product Name</th>
                                            
                                            <th>Product Category</th>
                                            <th>Qty</th>
                                            <th>UOM</th>
                                            <th>Supplier</th>
                                            <th>Status</th>
                                            <th>Inventory</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
										foreach($results as $v) {
										?>
                                        <tr>
                                            <td><?=$v['id']?></td>
                                            <td><?=$v['product_no']?></td>
                                            <td><?=$v['product_name']?></td>
                                            
                                            <td><?=isset($category_list[$v['product_category']])?$category_list[$v['product_category']]:'-'?></td>
                                            <td><?=$v['qty']==''?0:$v['qty']?></td>
                                            <td><?=$v['uom']?></td>
                                            <td><?=$v['supplier']?></td>
                                            <td><?=$v['status']==1?'Active':'Suspended'?></td>
                                            <td>
                                            	<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" data-product_id="<?=$v['id']?>" data-product_name="<?=$v['product_name']?>" data-uom="<?=$v['uom']?>">
                                                    <i class="fa fa-archive"></i> Update
                                                </button>
                                            </td>
                                            <td>
                                                                                        	                             
                                                
                                                <a class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button" href="<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_edit/'.$v['id'])?>"><i class="fa fa-edit"></i></a>
                                                
                                                <a class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" href="javascript: deleteData('<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_del/'.$v['id'])?>')"><i class="fa fa-times"></i></a>
                                                
                                        </tr>
                                        <?php
										}
										?>                                        
                                    </tbody>
                                </table>
                                <?php
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
								?>
          </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                    
                    <!-- Data list (Mobile) -->
                        <div class="mobile-list visible-xs">
                        	<?php
								if(!empty($results)) {
								?>
                        	<table class="table table-striped table-bordered table-hover">
                            	<?php										
										foreach($results as $k=>$v) {
									?>
                            	<tr>
                                	<td>
                                    	<div class="row mobile-list-header">
                                            <div class="col-xs-12"><strong>#<?=($k+1)?></strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Product Name:</strong></div>
                                            <div class="col-xs-8"><?=$v['product_name']?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Status:</strong></div>
                                            <div class="col-xs-8"><?=$v['status']==1?'Active':'Suspended'?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Action:</strong></div>
                                            <div class="col-xs-8"><a class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button" href="<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_edit/'.$v['id'])?>"><i class="fa fa-edit"></i></a>
                                                
                                                <a class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" href="javascript: deleteData('<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_del/'.$v['id'])?>')"><i class="fa fa-times"></i></a></div>
                                        </div>
                                    </td>
                                </tr>
                                <?php											
										}
									?>
                            </table>
                            <?php
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
								?>
                                                       
                        </div>
                    <?=$paging?>
                    
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="myModalLabel">Update Inventory to <span id="product_name"></span></h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <table class="table table-striped table-bordered table-hover" id="dataTables">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Update Type *</th>
                                                                            <th width="200px">Quantity *</th>
                                                                            <th>Reason</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="radio">
                                                                                    <label>
                                                                                        <input type="radio" name="updatetype" id="optionsRadios1" value="option1" checked>Add Inventory
                                                                                    </label>
                                                                                </div>
                                                                                <div class="radio">
                                                                                    <label>
                                                                                        <input type="radio" name="updatetype" id="optionsRadios2" value="option2">Deduct Inventory
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="form-group input-group">
                                                                                    <input id="qty" type="text" class="form-control" value="0">
                                                                                    <span id="uom_val" class="input-group-addon"> Carton(s)
                                                                                    </span>
                                                                                </div>
                                                                            </td>
                                                                            <td><textarea id="reason" class="form-control"></textarea>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <input type="hidden" id="id" value=""/>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button onclick="updateQty()" type="button" class="btn btn-primary">Update</button>
                                            					<button type="cancel" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                <!-- /.modal -->
            
<script>
var alert_v = <?=$alert?>;
var current_url = window.location.href;
	
		switch(alert_v){
			case 1:
				alert('Record has been created successfully');
				break;
			case 2:
				alert('Record has been updated successfully');
				break;
			case 3:
				alert('Record has been deleted successfully');
				break;
		}

function deleteData(url){
	var c = confirm("Are you sure you want to delete?");
	if(c){
		location.href=url;	
	}
}
	
function firstToUpperCase( str ) {
    return str.substr(0, 1).toUpperCase() + str.substr(1);
}
		
$('#myModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget)
	var id = button.data('product_id')
	var name = button.data('product_name')
	var uom = button.data('uom')
	
	$('#product_name').text(name);
	$('#id').val(id);
	
	//clear input text
	$('#qty').val(0);
	$('#reason').val('');
	
	uom = firstToUpperCase(uom);
	$('#uom_val').text(uom+'(s)');

});

function updateQty(){
	
	var id = $('#id').val();
	var qty = $('#qty').val();
	var reason = $('#reason').val();
	
  	if($('#optionsRadios1').is(':checked')){ 
		var type_qty = '+'+qty;
	}else{
		var type_qty = '-'+qty;
	}
	
	if(qty != 0 && qty != ''){
		
		term = {id:id, qty:type_qty, reason:reason};
		url = '<?=base_url($init['langu'].'/agora/administrator/update_qty')?>';
		
		$.ajax({
			type:"POST",
			url: url,
			data: term,
			dataType : "json"
		}).done(function(r) {
			if(r.status == 'OK'){
				$('#myModal').modal('hide');
				alert('Update Inventory Successful');
				
				if(alert_v != 0){
					var url = current_url.substring(0,current_url.length-2);
					window.location.replace(url);
				}else{
					location.reload();	
				}

			}
		});	
		
	}else{
		alert('Quantity cant be blank or zero');
	}

}
//enter search
		$(document).ready(function() {
		  $(window).keydown(function(event){
			if(event.keyCode == 13) {
			  event.preventDefault();
			  q.blur();
			  return false;
			}
		  });
		});
</script>
