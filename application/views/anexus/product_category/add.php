<div id="page-wrapper">
            <form method="post" action="<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_submit')?>" onsubmit="return validate();">
            <input type="hidden" id="id" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-truck fa-fw"></i> <?=$mode=='Add'?'New':($mode=='View'?'View':'Edit')?> Product Category
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Product Category Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Category Name *</label>
                                    <input onblur="check_name()" class="form-control" id="category_name" name="category_name" value="<?=$mode=='Edit'?$result['name']:''?>">
                                    <input id="cat_check" type="hidden" value="0">
                                    <span style="color:red" id="category_name_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description"><?=$mode=='Edit'?$result['description']:''?></textarea>
                                </div>                                
                         </div>
                         <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-12">
                    
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
                                    
            </form>
            
        </div>
        <!-- /#page-wrapper -->
        
<script>
var mode = '<?=$mode?>';

if(mode=='Edit'){
	check_name();	
}

function check_name(){
	
	category_name = $("#category_name").val();
	id = $("#id").val();
	term = {"category_name":category_name,"id":id};
	url = '<?=base_url($init['langu'].'/agora/administrator/product_category_check')?>';
	
	if(category_name != ''){
		$.ajax({
			type:"POST",
			url: url,
			data: term,
			dataType : "json"
			}).done(function(r) {
				if(r.status == "ok") {
					
					if(r.email == 1){
						$('#category_name_err').text('Category name not available');
						$('#cat_check').val(0);
					}else{
						$('#category_name_err').text('');	
						$('#cat_check').val(1);
					}
					
				}
		});	
		
	}else{
		
		$('#category_name_err').text('Category name can\'t be blank');	
		$('#cat_check').val(0);
		
	}
	
}

function validate(){
	
	var error = 0;
	
	//email
	var cat_check = $('#cat_check').val();
	if(cat_check == 0){
		check_name();
		error = 1;
	}
	
	if(error == 1){
		return false;
	}
	
}

</script>