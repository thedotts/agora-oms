
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-pencil-square-o fa-fw"></i> Manage Employees
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input class="form-control" placeholder="Search">
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </form>
                        </div>
                		<!-- /.col-lg-3 -->
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                            	<a href="create-employee.html"><button class="btn btn-default btn-sm" type="button">Create Employee Account </button></a><p></p>
                    <div class="panel panel-default">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Role</th>
                                            <th>Email</th>
                                            <th>Contact No.</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>0001</td>
                                            <td>Sales</td>
                                            <td>Sales</td>
                                            <td>abc@anexuscorp.com</td>
                                            <td>1234567</td>
                                            <td>Active</td>
                                            <td>
                                                <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button"><i class="fa fa-edit"></i></button>
                                                <button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button>
                                             </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>0002</td>
                                            <td>Sales Manager</td>
                                            <td>Sales Manager</td>
                                            <td>abc@anexuscorp.com</td>
                                            <td>1234567</td>
                                            <td>Suspended</td>
                                            <td>
                                                <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button"><i class="fa fa-edit"></i></button>
                                                <button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button>
                                             </td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>0003</td>
                                            <td>Super Admin</td>
                                            <td>Super Admin</td>
                                            <td>abc@anexuscorp.com</td>
                                            <td>1234567</td>
                                            <td>Active</td>
                                            <td>
                                                <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button"><i class="fa fa-edit"></i></button>
                                                <button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button>
                                             </td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>0004</td>
                                            <td>General Manager</td>
                                            <td>General Manager</td>
                                            <td>abc@anexuscorp.com</td>
                                            <td>1234567</td>
                                            <td>Active</td>
                                            <td>
                                                <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button"><i class="fa fa-edit"></i></button>
                                                <button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button>
                                             </td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>0005</td>
                                            <td>Logistics</td>
                                            <td>Logistics</td>
                                            <td>abc@anexuscorp.com</td>
                                            <td>1234567</td>
                                            <td>Active</td>
                                            <td>
                                                <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button"><i class="fa fa-edit"></i></button>
                                                <button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button>
                                             </td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>0006</td>
                                            <td>Finance</td>
                                            <td>Finance</td>
                                            <td>abc@anexuscorp.com</td>
                                            <td>1234567</td>
                                            <td>Active</td>
                                            <td>
                                                <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button"><i class="fa fa-edit"></i></button>
                                                <button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button>
                                             </td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>0007</td>
                                            <td>Service</td>
                                            <td>Service</td>
                                            <td>abc@anexuscorp.com</td>
                                            <td>1234567</td>
                                            <td>Active</td>
                                            <td>
                                                <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button"><i class="fa fa-edit"></i></button>
                                                <button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button>
                                             </td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>0008</td>
                                            <td>Service Manager</td>
                                            <td>Service Manager</td>
                                            <td>abc@anexuscorp.com</td>
                                            <td>1234567</td>
                                            <td>Active</td>
                                            <td>
                                                <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button"><i class="fa fa-edit"></i></button>
                                                <button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button>
                                          </td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>0009</td>
                                            <td style="color:#F00">Purchaser</td>
                                            <td>Purchaser</td>
                                            <td>abc@anexuscorp.com</td>
                                            <td>1234567</td>
                                            <td>Active</td>
                                            <td>
                                                <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button"><i class="fa fa-edit"></i></button>
                                                <button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button>
                                          </td>
                                        </tr>
                                    </tbody>
                                </table>
          </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

   