<link href="<?=base_url('assets/css/multi-select.css')?>" rel="stylesheet">
<script src="<?=base_url('assets/js/jquery.multi-select.js')?>"></script>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-briefcase fa-fw"></i> Create Purchase Request</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                       		<!-- Button trigger modal -->
                            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" <?=$mode=='Edit'?'disabled':''?>>
                                Seach from Quotation
                            </button>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Search Quotation</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Search</label>
                                                <input class="form-control" placeholder="By Company Name or Reference No." id="search" name="search">
                                            </div>
                                            <table id="quotation_table" class="table table-striped table-bordered table-hover" id="dataTables">
                                            </table>
                                            <div id="pagination"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <form action="<?=base_url($init['langu'].'/anexus/sales/purchase_submit');?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" id="quotation_id" name="quotation_id" value="<?=$mode=='Edit'?$result['quotation_id']:''?>"/>
            <div class="row">
                
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Purchase Request Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>PR Serial No.</label>
                                    <input class="form-control" value="<?=$mode=='Edit'?$result['purchase_serial']:''?>"readonly>
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Date of Requisition</label>
                                    <input id="dateRequisition" name="dateRequisition" class="form-control" placeholder="Calendar Input" value="<?=$mode=='Edit'?date("d/m/y", strtotime($result['requesting_date'])):''?>" readonly>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                       
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Sales Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Customer Purchase Order Ref</label>
                                    <input id="purchase_order_ref" name="purchase_order_ref" class="form-control" value="<?=$mode=='Edit'?$result['purchase_order_ref']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Sales Order Ref</label>
                                    <input id="salesOrderRef" name="salesOrderRef" class="form-control" readonly value="<?=$mode=='Edit'?$result['sale_order_ref']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Reference Document</label>
                                    <input id="uploadFile" name="uploadFile" type="file">
                                    <?php if($mode=='Edit'){?>
                                    <a href="<?=$result['reference_document']?>"><?=$result['reference_document']?></a>
									<?php } ?>
                                    <input id="originalFile" name="originalFile" type="hidden" value="<?=$mode=='Edit'?$result['reference_document']:''?>">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Customer Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Company Name *</label>
                                    <select id="customer_id" name="customer_id" class="form-control" onchange="getCompanyDetail()">
                                    <option value="none">Select</option>
                                    </select>
                                    <input type="hidden" id="customer_name" name="customer_name" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Attention</label>
                                    <input id="attention" class="form-control" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input id="address" class="form-control" value="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Postal Code</label>
                                    <input id="postal" class="form-control" value="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input id="email" class="form-control" value="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Contact</label>
                                    <input id="contact" class="form-control" value="" disabled>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6"> 
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Requestor Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Requestor Name</label>
                                    <input id="requestorName" name="requestorName" class="form-control" value="" disabled>
                                    <input type="hidden" id="requestorId" name="requestorId" class="form-control" value="<?=$mode=='Edit'?$result['requestor_id']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Employee Ref</label>
                                    <input id="employeeRef" name="employeeRef" class="form-control" value="<?=$mode=='Edit'?$result['employee_ref']:''?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Department</label>
                                    <input id="department" name="department" class="form-control" value="<?=$mode=='Edit'?$department_list[$result['department_id']]:''?>" disabled>
                                    <input type="hidden" id="departmentId" name="departmentId" class="form-control" value="<?=$mode=='Edit'?$result['department_id']:''?>">
                                </div>
                                <div class="form-group">
                                   	<label>Service Request Form</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="service-request" id="optionsRadios1" value="1" <?=$mode=='Edit'?($result['service_request_form']==1?'checked':''):''?>>Yes
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="service-request" id="optionsRadios2" value="0" <?=$mode=='Edit'?($result['service_request_form']==0?'checked':''):''?>>No
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                   	<label>Consignee</label>
                                    <div class="radio">
                                        <label> 
                                            <input type="radio" name="consignee" id="optionsRadios1" value="anexus" <?=$mode=='Edit'?($result['consignee']=='anexus'?'checked':''):''?>>Anexus
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="consignee" id="optionsRadios2" value="direct" <?=$mode=='Edit'?($result['consignee']=='direct'?'checked':''):''?>>Direct
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                   	<label>Uncrating Service Required</label>
                                    <div class="radio">
                                        <label> 
                                            <input type="radio" name="uncrating" id="optionsRadios1" value="1" <?=$mode=='Edit'?($result['uncrating_service_required']==1?'checked':''):''?>>Yes
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="uncrating" id="optionsRadios2" value="0"  <?=$mode=='Edit'?($result['uncrating_service_required']==0?'checked':''):''?>>No
                                        </label>
                                    </div>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                
                </div>
                <!-- /.col-lg-6 -->
                
            </div>
            <!-- /.row -->
            
            <hr />
            <div id="multi_select">
            <label>Supplier</label>
            <select id='callbacks' multiple='multiple'>
            	<?php foreach($supplier_list as $k => $v){?>
              <option value='<?=$v['id']?>'><?=$v['company_name']?></option>
              	<?php } ?>
            </select>
            <textarea style="display:none;" id="temp_supplier"></textarea>
            <button type="button" class="btn btn-primary" onclick="getSupplier()">Submit</button>
            <hr />
            </div>
            
            
            <div class="row">
                <div class="col-lg-12">
                
                    <div id="productItem">
                    <?php if(!empty($related_product) && $mode=='Edit'){?>
                    <?php
                    $max_counter = count($related_product);
					$counter = 0;
					?>
                    
                    <?php foreach($related_product as $k => $v){?>
                    	<?php 
							$counter++;
							$item_counter = 0;
							$product_item ='';
						?>
                        	<?php foreach($v as $x =>$y){
								$item_counter++;
								$supplier_id = $y['supplier_id'];
								$company_name = $y['company_name'];
								$person_in_charge = $y['person_in_charge'];
								$purchase_order_reg = $y['purchase_order_reg'];
								$remarks = $y['remarks'];
								
								$usd_selected='';
								$sgd_selected='';
								
								
								
								$link_view_product = base_url('en/anexus/'.$group_name.'/product_view/'.$y['id']);
								$currency_tmp='';
								foreach($currency_list as $m => $n){
									$selected ='';
									if($y['currency']==$m){
										$selected ='selected';
									}
									$currency_tmp.='<option value="'.$m.'"'.$selected.'>'.$n.'</option>';
								}
								
								$product_item .='<tr><td>'.$item_counter.'<input name="itemId'.$counter.'[]"type="hidden" value="'.$y['id'].'"></td><td><input class="form-control" value="'.$product_list[$y['product_id']].'" readonly><input name="productId'.$counter.'[]" type="hidden" value="'.$y['product_id'].'"></td><td><input name="quantity'.$counter.'[]" class="form-control" value="'.$y['quantity'].'" readonly></td><td><input name="partNo'.$counter.'[]" type="text" class="form-control" value="'.$y['part_no'].'" readonly></td><td width="15%"><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input name="unitPrice'.$counter.'[]" type="text" class="form-control" value="'.$y['unit_cost'].'" readonly></div></td><td width="15%"><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input name="totalPrice'.$counter.'[]" type="text" class="form-control" value="'.$y['total_cost'].'" readonly></div></td><td><div class="form-group input-group"><select name="currency'.$counter.'[]" class="form-control">'.$currency_tmp.'</select></div></td><td><a href="'.$link_view_product.'"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></a></td></tr>';
							}?>
                        
                    	<div class="panel panel-primary">
                        <div class="panel-heading">Supplier <?=$counter?> of <?=$max_counter?></div>
                        <div class="panel-body">
                        <div class="table-responsive">
                        <table class="table">
                        <tbody>
                        <tr>
                        <td>Company Name</td>
                        <td>
                        <input class="form-control" name="company_name[]" value="<?=$company_name?>" readonly>	
                        <input type="hidden" name="company_id[]" value="<?=$supplier_id?>" ></td>
                        </tr>
                        <tr>
                        <td>Person In Charge</td>
                        <td><input class="form-control" name="pic[]" value="<?=$person_in_charge?>" readonly></td>
                        </tr>
                        <tr>
                        <td>Anexus Purchase Order Reg</td>
                        <td><input name="order Reg[]" class="form-control" value="<?=$purchase_order_reg?>"></td>
                        </tr>
                        <tr>
                        <td>Remarks</td>
                        <td><textarea name="remark[]" class="form-control"><?=$remarks?></textarea></td>
                        </tr>
                        </tbody>
                        </table></div><div class="panel panel-default"><div class="panel-heading">Items</div><div class="panel-body"><div class="table-responsive">
                        <table class="table table-hover">
                        <thead>
                        <tr>
                        <th>#</th>
                        <th>Item</th
                        ><th>Quantity</th>
                        <th>Part Number</th>
                        <th>Unit Cost</th>
                        <th>Total Cost</th>
                        <th width="80px">Action</th>
                        </tr>
                        </thead>
                        <tbody><?=$product_item?></tbody>
                        </table>
                        </div>
                        </div>
                        </div>
                        </div>
                        </div>
                    	
                    <?php }?>
                    <?php }?>
                    </div>
                    
                    <hr/>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Additional Information
                                </div>
                                <div class="panel-body">
                                        <div class="form-group">
                                            <label>Remarks</label>
                                            <textarea name="remark" class="form-control"><?=$mode=='Edit'?$result['remark']:''?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>General Terms & Conditions</label>
                                            <textarea name="term_condition" class="form-control" rows="20"><?=$mode=='Edit'?$result['term_condition']:$term['value']?></textarea>
                                        </div>
                                 </div>
                                 <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                       </div>
                   </div>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->
        
        <script>
		var currency_list = <?=json_encode($currency_list)?>;
		var staff_json = <?=isset($staff_json)?$staff_json:'[]'?>;
		var company_json = <?=$company_list?>;
		var mode = '<?=$mode?>';
		
		for(i=0;i<company_json.length;i++){
			var temp = '';
			temp = $('<option>').attr('value',company_json[i].id).html(company_json[i].company_name);
			$('#customer_id').append(temp);
		}
		
		if(mode == 'Edit'){
			$('#customer_id').val(<?=isset($result['customer_id'])?$result['customer_id']:''?>);
			getCompanyDetail();
		}
		
		function getCompanyDetail(){
			id = $("#customer_id").val();
			term = {id:id};
			url = '<?=base_url($init['langu'].'/anexus/sales/getCompanyDetail')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					console.log(r);
					$('#customer_name').val(r.company_detail.company_name);
					$('#attention').val(r.company_detail.primary_attention_to);
					$('#address').val(r.company_detail.delivery_address);
					$('#postal').val(r.company_detail.delivery_postal);
					$('#email').val(r.company_detail.email);
					$('#contact').val(r.company_detail.contact_info);
				}
			});	
		}
		
		var currency_tmp=''; 
		
		for(z in currency_list){
			currency_tmp+='<option value="'+z+'">'+currency_list[z]+'</option>';				
		}
		
		$( "#search" ).blur(function(){
			
			ajax_quotation(1);
			
		});
		
		$('#myModal').on('show.bs.modal', function (event) {
			ajax_quotation(1);
		})
		
		function changePage(i){
			ajax_quotation(i);
		}
		
		function ajax_quotation(i){
			keyword = $("#search").val();
			term = {keyword:keyword,limit:10,page:i};
			url = '<?=base_url($init['langu'].'/anexus/sales/getQuotation')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					//console.log(r);
					tmp = '';
					
					if(r.data != false){
					for(var i=0;i<r.data.length;i++){
						
						tmp+='<tr><td><a href="javascript:getQuotation('+r.data[i].id+')">'+r.data[i].customer_name+'</a></td><td>'+r.data[i].qr_serial+'</td><td>'+r.data[i].modified_date+'</td><td>'+r.data[i].status+'</td></tr>';
					
					}
					
					html = '<thead><tr><th>Company Name</th><th>Reference No.</th><th>Date Last Updated</th><th>Status</th></tr></thead><tbody>'+tmp+'</tbody>';
					
					pagination = r.paging;
					
					$('#quotation_table').html('');
					$('#pagination').html('');
					$('#quotation_table').append(html);
					$('#pagination').append(pagination);
					}else{
						$('#quotation_table').html('No search result');
						$('#pagination').html('');
					}
					
				}
			});	
		}
		
		function getQuotation(id){
			var dateRequisition = '';
			var html = '';
			var temp_item = '';
			var company_name = '';
			var pic = '';
			
			$('#myModal').modal('hide');
			
			term = {id:id};
			url = '<?=base_url($init['langu'].'/anexus/sales/getQuotationData')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					console.log(r);
					dateRequisition = r.data.request_date;
					dateRequisition = dateRequisition.split('-');
					
					$('#quotation_id').val(r.data.id);
					$('#customer_id').val(r.data.customer_id);
					$('#customer_name').val(r.data.customer_name);
					$('#salesOrderRef').val(r.data.qr_serial);
					$('#employeeRef').val(r.data.employee_no);
					$('#department').val(r.data.department);
					$('#departmentId').val(r.data.department_id);
					$('#requestorName').val(r.data.employee_name);
					$('#requestorId').val(r.data.employee_id);
					$('#dateRequisition').val(dateRequisition[2]+"/"+dateRequisition[1]+"/"+dateRequisition[0]);
					
					console.log(r.product);
					
					getCompanyDetail();
					
					$('#productItem').html('');
					
					var max_counter = 0;
					var counter = 0;
					var item_counter = 0;
					
					for(x in r.product){
						max_counter++;
					}
					
					for(x in r.product){
						counter++;
						temp_item = '';
						item_counter = 0;
						
						for(y in r.product[x]){
							
							item_counter++;
							
							
							temp_item+='<tr><td>'+item_counter+'</td><td><input class="form-control" value="'+r.product[x][y].model_no+'" readonly><input name="productId'+counter+'[]" type="hidden" value="'+r.product[x][y].product_id+'"></td><td><input name="quantity'+counter+'[]" class="form-control" value="'+r.product[x][y].quantity+'" readonly></td><td><input name="partNo'+counter+'[]" type="text" class="form-control" value="'+r.product[x][y].part_no+'" readonly></td><td width="15%"><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input name="unitPrice'+counter+'[]" type="text" class="form-control" value="'+r.product[x][y].unit_price+'" readonly></div></td><td width="15%"><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input name="totalPrice'+counter+'[]" type="text" class="form-control" value="'+r.product[x][y].unit_price*r.product[x][y].quantity+'" readonly></div></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></td></tr>';
							
							company_name = r.product[x][y].supplier_name;
							company_id = r.product[x][y].supplier_id;
							pic = r.product[x][y].supplier_pic;
							
							//console.log(r.product[x][y].model_no);
						}
						
						html='<div class="panel panel-primary"><div class="panel-heading">Supplier '+counter+' of '+max_counter+'</div><div class="panel-body"><div class="table-responsive"><table class="table"><tbody><tr><td>Company Name</td><td><input class="form-control" name="company_name[]" value="'+company_name+'" readonly><input type="hidden" name="company_id[]" value="'+company_id+'" ></td></tr><tr><td>Person In Charge</td><td><input class="form-control" name="pic[]" value="'+pic+'" readonly></td></tr><tr><td>Anexus Purchase Order Reg</td><td><input name="order Reg[]" class="form-control"></td></tr><tr><td>Remarks</td><td><textarea name="supplier_remark[]" class="form-control"></textarea></td></tr></tbody></table></div><div class="panel panel-default"><div class="panel-heading">Items</div><div class="panel-body"><div class="table-responsive"><table class="table table-hover"><thead><tr><th>#</th><th>Item</th><th>Quantity</th><th>Part Number</th><th>Unit Cost</th><th>Total Cost</th><th width="80px">Action</th></tr></thead><tbody>'+temp_item+'</tbody></table></div></div></div></div></div>';
						$('#productItem').append(html);
						
					}
					//console.log(a);
					
				}
			});	
			
		}
		
		var supplier_array = new Array();
		
		//multi select
		$('#callbacks').multiSelect({
		  afterSelect: function(id){
			
			//var selected_count = $("#callbacks :selected").length;
			//getSupplier(String(id),selected_count);
			
			if(supplier_array.length > 0){
				var check = 0;
				for(var i=0;i<supplier_array.length;i++){
					
					if(supplier_array[i] == String(id)){
						check = 1;
					}
				}
				if(check != 1){
					supplier_array.push(String(id));
				}
			}else{
				supplier_array.push(String(id));
			}
			
			$('#temp_supplier').val(JSON.stringify(supplier_array));
			
		  },
		  afterDeselect: function(id){
			
			//$('#supplier'+id).remove();
				var temp_push = new Array();
				for(var i=0;i<supplier_array.length;i++){
					
					if(supplier_array[i] != String(id)){
						temp_push.push(supplier_array[i]);
					}
				}
				supplier_array = temp_push;
				
				$('#temp_supplier').val(JSON.stringify(supplier_array));
			
		  }
		});
		
		function getToday(){
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			
			if(dd<10) {
				dd='0'+dd
			} 
			
			if(mm<10) {
				mm='0'+mm
			} 
			
			return today = dd+'/'+mm+'/'+yyyy;	
		}
		
		function adhoc_requestor_data(){
			
			$('#requestorName').val(staff_json.full_name);
			$('#requestorId').val(staff_json.id);
			$('#employeeRef').val(staff_json.employee_no);
			$('#department').val(staff_json.department_name);
			$('#departmentId').val(staff_json.department_id);
			$('#dateRequisition').val(getToday());
			
		}
		
		function getSupplier(){
		
		adhoc_requestor_data();
		
		$('#multi_select').hide();
		
		var supplier_idArray = $('#temp_supplier').val();
		
		term = {id:supplier_idArray};
		url = '<?=base_url($init['langu'].'/anexus/sales/getSupplier')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
										
					console.log(r);
					
					var total_no = r.supplier.length;
					html="";
					
					for(var i=0;i<r.supplier.length;i++){
						
					html+='<input type="hidden" id="count'+r.supplier[i].id+'" value="0"/><div id="supplier'+r.supplier[i].id+'"class="panel panel-primary"><div class="panel-heading">Supplier '+(i+1)+' of '+total_no+'</div><div class="panel-body"><div class="table-responsive"><table class="table"><tbody><tr><td>Company Name</td><td><input class="form-control" name="company_name[]" value="'+r.supplier[i].company_name+'" readonly><input type="hidden" name="company_id[]" value="'+r.supplier[i].id+'" ></td></tr><tr><td>Person In Charge</td><td><input class="form-control" name="pic[]" value="'+r.supplier[i].contact_person+'" readonly></td></tr><tr><td>Anexus Purchase Order Reg</td><td><input name="order Reg[]" class="form-control"></td></tr><tr><td>Remarks</td><td><textarea name="supplier_remark[]" class="form-control"></textarea></td></tr></tbody></table></div><div class="form-group"><button class="btn btn-default btn-sm" type="button" onclick="addItem('+r.supplier[i].id+','+(i+1)+')">Add New Product Item</button></div><div class="panel panel-default"><div class="panel-heading">Items</div><div class="panel-body"><div class="table-responsive"><table class="table table-hover"><thead><tr><th>#</th><th>Item</th><th>Quantity</th><th>Part Number</th><th>Unit Cost</th><th>Total Cost</th><th width="80px">Action</th></tr></thead><tbody id="tbody'+r.supplier[i].id+'"></tbody></table></div></div></div></div></div>';
					
					}
					
					$('#productItem').append(html);
					
				}
			});	

		}
		
		var sItem_counter = 0;
		
		function addItem(id,no_id){
		
		term = {id:id};
		url = '<?=base_url($init['langu'].'/anexus/sales/getItem')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
										
					console.log(r);
					
					var no = $('#count'+id).val();
					no++;
					$('#count'+id).val(no);
					
					var temp_option = '<option value="none">Select</option>';
					for(var i=0;i<r.item.length;i++){
						temp_option += '<option value='+r.item[i].id+'>'+r.item[i].model_no+'</option>';
					}
					
					temp_item ='<tr id="sItem'+sItem_counter+'"><td>'+no+'</td><td><select onchange="getItemDetail('+sItem_counter+')" class="form-control" id="productId'+sItem_counter+'" name="productId'+no_id+'[]">'+temp_option+'</select></td><td><select onchange="getTotalCost('+sItem_counter+')" id="quantity'+sItem_counter+'" name="quantity'+no_id+'[]" class="form-control" readonly></select></td><td><input id="partNo'+sItem_counter+'" name="partNo'+no_id+'[]" type="text" class="form-control" value="" readonly></td><td width="15%"><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input id="unitPrice'+sItem_counter+'" name="unitPrice'+no_id+'[]" type="text" class="form-control" value="" readonly></div></td><td width="15%"><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input id="totalPrice'+sItem_counter+'" name="totalPrice'+no_id+'[]" type="text" class="form-control" value="" readonly></div></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button><button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="delItem('+sItem_counter+')"><i class="fa fa-times"></i></button></td></tr>';
					
					sItem_counter++;
					$('#tbody'+id).append(temp_item);
					
				}
			});	

		}
		
		function getItemDetail(itemCount){
			
		id = $('#productId'+itemCount).val();
		
		term = {id:id};
		url = '<?=base_url($init['langu'].'/anexus/sales/getItemDetail')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
										
					console.log(r);
					var qty_temp ='<option value="none">Select</option>';
					
					for(var i=1;i<=r.item_detail.current_stock_qty;i++){
						qty_temp += '<option value="'+i+'">'+i+'</option>';
					}
					
					$('#quantity'+itemCount).append(qty_temp);
					$('#quantity'+itemCount).attr('readonly', false);
					$('#partNo'+itemCount).val(r.item_detail.part_no);
					$('#unitPrice'+itemCount).val(r.item_detail.cost_price);
					
					
					
				}
			});	

		}
		
		function getTotalCost(itemCount){
			var qty = $('#quantity'+itemCount).val();
			var unitPrice = $('#unitPrice'+itemCount).val();
			var total_cost = parseInt(qty)*parseInt(unitPrice);
			$('#totalPrice'+itemCount).val(total_cost);
		}
		
		function delItem(id){
			$('#sItem'+id).remove();
		}
		
        </script>