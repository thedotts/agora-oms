<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-briefcase fa-fw"></i> <?=$head_title?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                       		<!-- Button trigger modal -->
                            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" <?=$mode=='Edit'?'disabled':''?>>
                                Seach from Quotation
                            </button>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Search Quotation</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Search</label>
                                                <input class="form-control" placeholder="By Company Name or Reference No." id="search" name="search">
                                            </div>
                                            <table id="quotation_table" class="table table-striped table-bordered table-hover" id="dataTables">
                                            </table>
                                            <div id="pagination"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <form action="<?=base_url($init['langu'].'/anexus/sales/purchase_submit');?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" id="quotation_id" name="quotation_id" value="<?=$mode=='Edit'?$result['quotation_id']:''?>"/>
            <input type="hidden" id="customer_id" name="customer_id" value="<?=$mode=='Edit'?$result['customer_id']:''?>"/>
            <input type="hidden" id="customer_name" name="customer_name" value="<?=$mode=='Edit'?$result['customer_name']:''?>"/>
            <input type="hidden" id="job_id" name="job_id" value="<?=isset($job_id)?$job_id:0?>"/>
            <div class="row">
                
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Purchase Request Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>PR Serial No.</label>
                                    <input class="form-control" value="<?=$mode=='Edit'?$result['purchase_serial']:''?>"readonly>
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Date of Requisition</label>
                                    <input id="dateRequisition" name="dateRequisition" class="form-control" placeholder="Calendar Input" value="<?=$mode=='Edit'?date("d/m/y", strtotime($result['requesting_date'])):''?>" readonly>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                       
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Sales Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Customer Purchase Order Ref</label>
                                    <input id="purchase_order_ref" name="purchase_order_ref" class="form-control" value="<?=$mode=='Edit'?$result['purchase_order_ref']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Sales Order Ref</label>
                                    <input id="salesOrderRef" name="salesOrderRef" class="form-control" readonly value="<?=$mode=='Edit'?$result['sale_order_ref']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Reference Document</label>
                                    <input id="uploadFile" name="uploadFile" type="file">
                                    <?php if($mode=='Edit'){?>
                                    <a href="<?=$result['reference_document']?>"><?=$result['reference_document']?></a>
									<?php } ?>
                                    <input id="originalFile" name="originalFile" type="hidden" value="<?=$mode=='Edit'?$result['reference_document']:''?>">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6"> 
                
                
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Requestor Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Requestor Name</label>
                                    <input id="requestorName" name="requestorName" class="form-control" value="<?=$staff_info['full_name']?>" disabled>
                                    <input type="hidden" id="requestorId" name="requestorId" class="form-control" value="<?=$mode=='Edit'?$result['requestor_id']:$staff_info['id']?>">
                                </div>
                                <div class="form-group">
                                    <label>Employee Ref</label>
                                    <input id="employeeRef" name="employeeRef" class="form-control" value="<?=$mode=='Edit'?$result['employee_ref']:''?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Department</label>
                                    <input id="department" name="department" class="form-control" value="<?=$mode=='Edit'?$department_list[$result['department_id']]:''?>" disabled>
                                    <input type="hidden" id="departmentId" name="departmentId" class="form-control" value="<?=$mode=='Edit'?$result['department_id']:''?>">
                                </div>
                                <div class="form-group">
                                   	<label>Service Request Form</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="service-request" id="optionsRadios1" value="1" <?=$mode=='Edit'?($result['service_request_form']==1?'checked':''):''?>>Yes
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="service-request" id="optionsRadios2" value="0" <?=$mode=='Edit'?($result['service_request_form']==0?'checked':''):''?>>No
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                   	<label>Consignee</label>
                                    <div class="radio">
                                        <label> 
                                            <input type="radio" name="consignee" id="optionsRadios1" value="anexus" <?=$mode=='Edit'?($result['consignee']=='anexus'?'checked':''):''?>>Anexus
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="consignee" id="optionsRadios2" value="direct" <?=$mode=='Edit'?($result['consignee']=='direct'?'checked':''):''?>>Direct
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                   	<label>Uncrating Service Required</label>
                                    <div class="radio">
                                        <label> 
                                            <input type="radio" name="uncrating" id="optionsRadios1" value="1" <?=$mode=='Edit'?($result['uncrating_service_required']==1?'checked':''):''?>>Yes
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="uncrating" id="optionsRadios2" value="0"  <?=$mode=='Edit'?($result['uncrating_service_required']==0?'checked':''):''?>>No
                                        </label>
                                    </div>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                
                </div>
                <!-- /.col-lg-6 -->
                
            </div>
            <!-- /.row -->
            <hr />
            
            <div class="row">
                <div class="col-lg-12">
                    
                    <div id="productItem">
                    <?php if(!empty($related_product) && $mode=='Edit'){?>
                    <?php
                    $max_counter = count($related_product);
					$counter = 0;
					?>
                    
                    <?php foreach($related_product as $k => $v){?>
                    	<?php 
							$counter++;
							$item_counter = 0;
							$product_item ='';
						?>
                        	<?php foreach($v as $x =>$y){
								$item_counter++;
								$supplier_id = $y['supplier_id'];
								$company_name = $y['company_name'];
								$person_in_charge = $y['person_in_charge'];
								$purchase_order_reg = $y['purchase_order_reg'];
								$remarks = $y['remarks'];
								
								$usd_selected='';
								$sgd_selected='';
								
								
								
								$link_view_product = base_url('en/anexus/'.$group_name.'/product_view/'.$y['id']);
								$currency_tmp='';
								foreach($currency_list as $m => $n){
									$selected ='';
									if($y['currency']==$m){
										$selected ='selected';
									}
									$currency_tmp.='<option value="'.$m.'"'.$selected.'>'.$n.'</option>';
								}
								
								$product_item .='<tr><td>'.$item_counter.'<input name="itemId'.$counter.'[]"type="hidden" value="'.$y['id'].'"></td><td><input class="form-control" value="'.$product_list[$y['product_id']].'" readonly><input name="productId'.$counter.'[]" type="hidden" value="'.$y['product_id'].'"></td><td><input name="quantity'.$counter.'[]" class="form-control" value="'.$y['quantity'].'" readonly></td><td><input name="partNo'.$counter.'[]" type="text" class="form-control" value="'.$y['part_no'].'" readonly></td><td width="15%"><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input name="unitPrice'.$counter.'[]" type="text" class="form-control" value="'.$y['unit_cost'].'" readonly></div></td><td width="15%"><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input name="totalPrice'.$counter.'[]" type="text" class="form-control" value="'.$y['total_cost'].'" readonly></div></td><td><div class="form-group input-group"></div></td><td><a href="'.$link_view_product.'"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></a></td></tr>';
							}?>
                        
                    	<div class="panel panel-primary">
                        <div class="panel-heading">Supplier <?=$counter?> of <?=$max_counter?></div>
                        <div class="panel-body">
                        <div class="table-responsive">
                        <table class="table">
                        <tbody>
                        <tr>
                        <td>Company Name</td>
                        <td>
                        <input class="form-control" name="company_name[]" value="<?=$company_name?>" readonly>	
                        <input type="hidden" name="company_id[]" value="<?=$supplier_id?>" ></td>
                        </tr>
                        <tr>
                        <td>Person In Charge</td>
                        <td><input class="form-control" name="pic[]" value="<?=$person_in_charge?>" readonly></td>
                        </tr>
                        <tr>
                        <td>Anexus Purchase Order Reg</td>
                        <td><input name="order Reg[]" class="form-control" value="<?=$purchase_order_reg?>"></td>
                        </tr>
                        <tr>
                        <td>Remarks</td>
                        <td><textarea name="supplier_remark[]" class="form-control"><?=$remarks?></textarea></td>
                        </tr>
                        </tbody>
                        </table></div><div class="panel panel-default"><div class="panel-heading">Items</div><div class="panel-body"><div class="table-responsive">
                        <table class="table table-hover">
                        <thead>
                        <tr>
                        <th>#</th>
                        <th>Item</th
                        ><th>Quantity</th>
                        <th>Part Number</th>
                        <th>Unit Cost</th>
                        <th>Total Cost</th>
                        <th width="80px">Action</th>
                        </tr>
                        </thead>
                        <tbody><?=$product_item?></tbody>
                        </table>
                        </div>
                        </div>
                        </div>
                        </div>
                        </div>
                    	
                    <?php }?>
                    <?php }?>
                    </div>
                    
                    <hr/>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Additional Information
                                </div>
                                <div class="panel-body">
                                        <div class="form-group">
                                            <label>Remarks</label>
                                            <textarea name="remark" class="form-control"><?=$mode=='Edit'?$result['remark']:''?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>General Terms & Conditions</label>
                                            <textarea name="term_condition" class="form-control" rows="20"><?=$mode=='Edit'?$result['term_condition']:$term['value']?></textarea>
                                        </div>
                                 </div>
                                 <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                       </div>
                   </div>
                    
                    <?php if($mode=='Add'){?>
                    
                    	<?php if($btn_type == 'create'){?>
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                        </div>
                    	<?php }?>
                        
                    <?php }else if($mode=='Edit'){?>
                    	<?php switch($btn_type){
							case 'edit':
							?>
                        		<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Submit</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            <?php 
							break;
							case 'approval':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Update</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'approval_update':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'send_email':
							?>
                            	<div class="form-group">
                                	<?php foreach($supplier_list as $k => $v){?>
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/sales/pr_pdf/'.$result['id'].'/'.$v['id'])?>" target="_blank">Download PDF - <?=$v['company_name']?></a>
                                    <?php }?>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/pr_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                </div>
                            
                            <?php 
							break;
							case 'upload':
							?>
                            
                            	<div class="panel">
                                	<?php foreach($supplier_list as $k => $v){?>
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/sales/pr_pdf/'.$result['id'].'/'.$v['id'])?>" target="_blank">Download PDF - <?=$v['company_name']?></a>
                                    <?php }?>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/pr_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file">
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                		<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            
                            <?php 
							break;
							case 'complete':
							?>
                            	<?php if($last_action == 'upload'){?>
                            	<div class="panel">
                                	<?php foreach($supplier_list as $k => $v){?>
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/sales/pr_pdf/'.$result['id'].'/'.$v['id'])?>" target="_blank">Download PDF - <?=$v['company_name']?></a>
                                    <?php }?>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/quotation_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file"> 
                                            <a target="_blank" href="<?=$result['confirm_file']?>"><?=$result['confirm_file']?></a>
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Reupload</button>
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                                <?php }?>
                            
                            <?php 
							break;
                        }?>

                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Status
                        </div>
                        <div class="panel-body">
                            <div class="form-group input-group">
							<?php 
                                //如果不是not approved, 也不是 completed 就顯示目前正在等誰
                                if($result['status'] != 0 && $result['status'] != $completed_status_id){
                            ?>
                            <label>
                                                    <?php
                                                    if(!empty($result['awaiting_table'])){
                                                        echo 'Awaiting ';
                                                        foreach($result['awaiting_table'] as $x){
                                                            if(isset($role_list[$x])){
                                                            echo '<br>'.$role_list[$x];
                                                            }
                                                        }
                                                    }
                                                    ?>
                            </label><br>
                            <?php 
                            } 
                            ?>
                            <label><?=$pr_status_list[$result['status']]?></label>
                            </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                    
                    <?php } //Edit mode end ?>
                    
                </div>
                <!-- /.col-lg-12 -->
                                
            </div>
            <!-- /.row -->
            
            
            
            </form>
            
        </div>
        <!-- /#page-wrapper -->
        
        <script>
		var currency_list = <?=json_encode($currency_list)?>;
		var currency_tmp=''; 
		var mode = '<?=$mode?>';
		var btn_type = '<?=$btn_type?>';
		
					//change input to read only
					if(btn_type != 'create' && btn_type != 'edit'){
						
						$(".form-control").each(function() {
						  $(this).prop('disabled', 'disabled');
						});
						
						$(".btn-circle").each(function() {
						  $(this).hide();
						});
						$('#addProduct').hide();
						$('#addService').hide();
						if(btn_type == 'update' || btn_type == 'approval_update'){
							
							var target_colum = <?=isset($target_colum)?$target_colum:'[]'?>;
							
							for(var i=0;i<target_colum.length;i++){
								var colum = target_colum[i].name;
								if(target_colum[i].type == 'single'){
									$("input[name='"+colum+"']").prop('disabled', false);
									
								}else if(target_colum[i].type == 'multiple'){
									$("."+colum).prop('disabled', false);
								}
							}
							
							
							
						}
					}
		
		for(z in currency_list){
			currency_tmp+='<option value="'+z+'">'+currency_list[z]+'</option>';				
		}
		
		$( "#search" ).blur(function(){
			
			ajax_quotation(1);
			
		});
		
		$('#myModal').on('show.bs.modal', function (event) {
			ajax_quotation(1);
		})
		
		function changePage(i){
			ajax_quotation(i);
		}
		
		function ajax_quotation(i){
			keyword = $("#search").val();
			term = {keyword:keyword,limit:10,page:i};
			url = '<?=base_url($init['langu'].'/anexus/sales/getQuotation')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					//console.log(r);
					tmp = '';
					
					if(r.data != false){
					for(var i=0;i<r.data.length;i++){
						
						tmp+='<tr><td><a href="javascript:getQuotation('+r.data[i].id+',1)">'+r.data[i].customer_name+'</a></td><td>'+r.data[i].qr_serial+'</td><td>'+r.data[i].modified_date+'</td><td>'+r.data[i].status+'</td></tr>';
					
					}
					
					html = '<thead><tr><th>Company Name</th><th>Reference No.</th><th>Date Last Updated</th><th>Status</th></tr></thead><tbody>'+tmp+'</tbody>';
					
					pagination = r.paging;
					
					$('#quotation_table').html('');
					$('#pagination').html('');
					$('#quotation_table').append(html);
					$('#pagination').append(pagination);
					}else{
						$('#quotation_table').html('No search result');
						$('#pagination').html('');
					}
					
				}
			});	
		}
		
		function getQuotation(id,mode){
			var dateRequisition = '';
			var html = '';
			var temp_item = '';
			var company_name = '';
			var pic = '';
			
			//mode = 2  auto select
			if(mode == 1){
			$('#myModal').modal('hide');
			}
			
			term = {id:id};
			url = '<?=base_url($init['langu'].'/anexus/sales/getQuotationData')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					console.log(r);
					dateRequisition = r.data.request_date;
					dateRequisition = dateRequisition.split('-');
					
					$('#quotation_id').val(r.data.id);
					$('#customer_id').val(r.data.customer_id);
					$('#customer_name').val(r.data.customer_name);
					$('#salesOrderRef').val(r.data.qr_serial);
					$('#employeeRef').val(r.data.employee_no);
					$('#department').val(r.data.department);
					$('#departmentId').val(r.data.department_id);
					$('#requestorName').val(r.data.requestor_to);
					$('#dateRequisition').val(dateRequisition[2]+"/"+dateRequisition[1]+"/"+dateRequisition[0]);
					
					console.log(r.product);
					
					
					$('#productItem').html('');
					
					var max_counter = 0;
					var counter = 0;
					var item_counter = 0;
					
					for(x in r.product){
						max_counter++;
					}
					
					for(x in r.product){
						counter++;
						temp_item = '';
						item_counter = 0;
						
						for(y in r.product[x]){
							
							item_counter++;
							
							
							temp_item+='<tr><td>'+item_counter+'</td><td><input class="form-control" value="'+r.product[x][y].model_no+'" readonly><input name="productId'+counter+'[]" type="hidden" value="'+r.product[x][y].product_id+'"></td><td><input name="quantity'+counter+'[]" class="form-control" value="'+r.product[x][y].quantity+'" readonly></td><td><input name="partNo'+counter+'[]" type="text" class="form-control" value="'+r.product[x][y].part_no+'" readonly></td><td width="15%"><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input name="unitPrice'+counter+'[]" type="text" class="form-control" value="'+r.product[x][y].cost_price+'" readonly></div></td><td width="15%"><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input name="totalPrice'+counter+'[]" type="text" class="form-control" value="'+r.product[x][y].cost_price*r.product[x][y].quantity+'" readonly></div></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></td></tr>';
							
							company_name = r.product[x][y].supplier_name;
							company_id = r.product[x][y].supplier_id;
							pic = r.product[x][y].supplier_pic;
							
							//console.log(r.product[x][y].model_no);
						}
						
						html='<div class="panel panel-primary"><div class="panel-heading">Supplier '+counter+' of '+max_counter+'</div><div class="panel-body"><div class="table-responsive"><table class="table"><tbody><tr><td>Company Name</td><td><input class="form-control" name="company_name[]" value="'+company_name+'" readonly><input type="hidden" name="company_id[]" value="'+company_id+'" ></td></tr><tr><td>Person In Charge</td><td><input class="form-control" name="pic[]" value="'+pic+'" readonly></td></tr><tr><td>Anexus Purchase Order Reg</td><td><input name="order Reg[]" class="form-control"></td></tr><tr><td>Remarks</td><td><textarea name="supplier_remark[]" class="form-control"></textarea></td></tr></tbody></table></div><div class="panel panel-default"><div class="panel-heading">Items</div><div class="panel-body"><div class="table-responsive"><table class="table table-hover"><thead><tr><th>#</th><th>Item</th><th>Quantity</th><th>Part Number</th><th>Unit Cost</th><th>Total Cost</th><th width="80px">Action</th></tr></thead><tbody>'+temp_item+'</tbody></table></div></div></div></div></div>';
						$('#productItem').append(html);
						
					}
					//console.log(a);
					
				}
			});	
			
		}
		
		var qr_id =<?=isset($qr_id)?$qr_id:0?>;
		
		if(qr_id != 0){
			getQuotation(qr_id,2);
		}
		
        </script>