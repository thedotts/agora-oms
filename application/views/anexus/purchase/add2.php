<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-briefcase fa-fw"></i> <?=$head_title?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                
        	<form action="<?=base_url($init['langu'].'/anexus/sales/purchase_submit');?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" id="quotation_id" name="quotation_id" value="<?=$mode=='Edit'?$result['quotation_id']:''?>"/>
            <input type="hidden" id="customer_id" name="customer_id" value="<?=$mode=='Edit'?$result['customer_id']:''?>"/>
            <input type="hidden" id="customer_name" name="customer_name" value="<?=$mode=='Edit'?$result['customer_name']:''?>"/>
            
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Purchase Order Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Purchase Order No.</label>
                                    <input class="form-control" value="<?=$result['purchase_serial']?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Date of Creation *</label>
                                    <input class="form-control" name="created_date" id="created_date" placeholder="Calendar Input" value="<?=date('d/m/Y',strtotime($result['created_date']))?>">
                                </div>
                                <div class="form-group">
                                    <label>Originator</label>
                                    <input class="form-control" value="<?=isset($employee_list[$pr_data['requestor_id']])?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Contact No.</label>
                                    <input class="form-control" value="<?=isset($originator['contact_info'])?>" readonly>
                                </div>
                                <!--
                                <div class="form-group">
                                    <label>Incoterms</label>
                                    <select class="form-control">
                                        <option>Ex-works USA</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Forwarder</label>
                                    <input class="form-control" value="Fedex Acc:379649904 (Economy Only)">
                                </div>
                                <div class="form-group">
                                    <label>Payment Terms</label>
                                    <select class="form-control">
                                        <option>Cash on Delivery</option>
                                        <option>15 days</option>
                                        <option>30 days</option>
                                        <option>45 days</option>
                                        <option>60 days</option>
                                    </select>
                                </div>
                                -->
                                <div class="form-group">
                                    <label>Quote Reference</label>
                                    <input class="form-control" value="<?=$pr_data['sale_order_ref']!=0?$pr_data['sale_order_ref']:''?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Currency</label>
                                    <input class="form-control" value="<?=$customer['currency']?>" readonly>
                                </div>
                                <!--
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <textarea class="form-control"></textarea>
                                </div>
                                -->
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Order To Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input class="form-control" name="ot_company_name" value="<?=$result['status']>3 || $result['status'] == 0?$result['ot_company_name']:$customer['company_name']?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control" name="ot_address" value="<?=$result['status']>3 || $result['status'] == 0?$result['ot_address']:$customer['address']?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Postal Code</label>
                                    <input class="form-control" name="ot_postal" value="<?=$result['status']>3 || $result['status'] == 0?$result['ot_postal']:$customer['postal']?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Attention</label>
                                    <input class="form-control" name="ot_attention" value="<?=$result['status']>3 || $result['status'] == 0?$result['ot_attention']:$customer['primary_attention_to']?>">
                                </div>
                                <div class="form-group">
                                    <label>CC</label>
                                    <input class="form-control" name="ot_cc" value="<?=$result['status']>3 || $result['status'] == 0?$result['ot_cc']:$customer['custom_code']?>">
                                </div>
                                <div class="form-group">
                                    <label>Contact</label>
                                    <input class="form-control" name="ot_contact" value="<?=$result['status']>3 || $result['status'] == 0?$result['ot_contact']:$customer['contact_info']?>" readonly>
                                </div>
                                <!--
                                <div class="form-group">
                                    <label>Fax</label>
                                    <input class="form-control" value="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Vendor No.</label>
                                    <input class="form-control" value="JN" disabled>
                                </div>
                                -->
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6">
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Bill to
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input class="form-control" name="bt_company_name" value="<?=$result['status']>3 || $result['status'] == 0?$result['bt_company_name']:$customer['company_name']?>">
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control" name="bt_address" value="<?=$result['status']>3 || $result['status'] == 0?$result['bt_address']:$customer['address']?>">
                                </div>
                                <div class="form-group">
                                    <label>Postal Code</label>
                                    <input class="form-control" name="bt_postal" value="<?=$result['status']>3 || $result['status'] == 0?$result['bt_postal']:$customer['postal']?>">
                                </div>
                                <div class="form-group">
                                    <label>Attention</label>
                                    <input class="form-control" name="bt_attention" value="<?=$result['status']>3 || $result['status'] == 0?$result['bt_attention']:$customer['primary_attention_to']?>">
                                </div>
                                <div class="form-group">
                                    <label>CC</label>
                                    <input class="form-control" name="bt_cc" value="<?=$result['status']>3 || $result['status'] == 0?$result['bt_cc']:$customer['custom_code']?>">
                                </div>
                                <div class="form-group">
                                    <label>Contact</label>
                                    <input class="form-control" name="bt_contact" value="<?=$result['status']>2 || $result['status'] == 0?$result['bt_contact']:$customer['contact_info']?>">
                                </div>
                                <!--
                                <div class="form-group">
                                    <label>Fax</label>
                                    <input class="form-control" value="61234567">
                                </div>
                                -->
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" name="bt_email" value="<?=$result['status']>3 || $result['status'] == 0?$result['bt_email']:$customer['email']?>">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Ship to
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input class="form-control" name="st_company_name" value="<?=$result['status']>3 || $result['status'] == 0?$result['st_company_name']:$customer['company_name']?>">
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control" name="st_address" value="<?=$result['status']>3 || $result['status'] == 0?$result['st_address']:$customer['address']?>">
                                </div>
                                <div class="form-group">
                                    <label>Postal Code</label>
                                    <input class="form-control" name="st_postal" value="<?=$result['status']>3 || $result['status'] == 0?$result['st_postal']:$customer['postal']?>">
                                </div>
                                <div class="form-group">
                                    <label>Attention</label>
                                    <input class="form-control" name="st_attention" value="<?=$result['status']>3 || $result['status'] == 0?$result['st_attention']:$customer['primary_attention_to']?>">
                                </div>
                                <div class="form-group">
                                    <label>CC</label>
                                    <input class="form-control" name="st_cc" value="<?=$result['status']>3 || $result['status'] == 0?$result['st_cc']:$customer['custom_code']?>">
                                </div>
                                <div class="form-group">
                                    <label>Contact</label>
                                    <input class="form-control" name="st_contact" value="<?=$result['status']>3 || $result['status'] == 0?$result['st_contact']:$customer['contact_info']?>">
                                </div>
                                <!--
                                <div class="form-group">
                                    <label>Fax</label>
                                    <input class="form-control" value="61234567">
                                </div>
                                -->
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" name="st_email" value="<?=$result['status']>3 || $result['status'] == 0?$result['st_email']:$customer['email']?>">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    
                </div>
                <!-- /.col-lg-6 -->
                
            </div>
            <!-- /.row -->
            
            
        	<form>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Items 
                        </div>
                        <input type="hidden" name="item_count" value="<?=($result['status']>2 && $result['status']!=0)?count($related_product2):'0'?>"/>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Quantity</th>
                                            <th width="20%">Item Description</th>
                                            <th>Unit Price</th>
                                            <th>Total Price</th>
                                            <th width="15%">Delivery Date</th>
                                            <th width="20%">Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($related_product2 as $k => $v){ ?>
                                        <tr>
                                            <td><?=$k+1?></td>
                                            <td>
                                            	<input type="hidden" name="item_id<?=$k?>" value="<?=$v['id']?>">
                                    			<input class="form-control quantity" name="item_quantity<?=$k?>" value="<?=$v['quantity']?>">
                                             </td>
                                            <td>
                                    			<textarea class="form-control" readonly><?=$v['item_desc']?></textarea>
                                            </td>
                                            <td>
                                            	<div class="form-group input-group">
                                                    <span class="input-group-addon"><i class="fa fa-usd"></i>
                                                    </span>
                                                    <input type="text" class="form-control" value="<?=$v['unit_cost']?>" disabled>
                                                </div>
                                            </td>
                                            <td>
                                    			<div class="form-group input-group">
                                                    <span class="input-group-addon"><i class="fa fa-usd"></i>
                                                    </span>
                                                    <input type="text" class="form-control" value="<?=$v['total_cost']?>" disabled>
                                                </div>
                                            <td>
                                    			<input class="form-control delivery_date" id="delivery_date<?=$k?>" name="delivery_date<?=$k?>" placeholder="calendar input" value="<?=$v['delivery_date']!="0000-00-00"?date('d/m/Y',strtotime($v['delivery_date'])):''?>">
                                             </td>
                                             <td>
                                    			<textarea class="form-control remarks" name="item_remark<?=$k?>"><?=$v['remarks']?></textarea>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                    
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Additional Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <textarea name="remark" class="form-control"><?=$result['remark']?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>General Terms & Conditions</label>
                                    <textarea name="term_condition" class="form-control" rows="20"><?=$result['term_condition']?></textarea>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    
					
                    <?php if($mode=='Add'){?>
                    
                    	<?php if($btn_type == 'create'){?>
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                        </div>
                    	<?php }?>
                        
                    <?php }else if($mode=='Edit'){?>
                    	<?php switch($btn_type){
							case 'edit':
							?>
                        		<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Submit</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            <?php 
							break;
							case 'approval':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Update</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'approval_update':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'send_email':
							?>
                            	<div class="form-group">
                                	<?php foreach($supplier_list as $k => $v){?>
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/sales/pr_pdf/'.$result['id'].'/'.$v['id'])?>" target="_blank">Download PDF - <?=$v['company_name']?></a>
                                    <?php }?>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/pr_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                </div>
                            
                            <?php 
							break;
							case 'upload':
							?>
                            
                            	<div class="panel">
                                	<?php foreach($supplier_list as $k => $v){?>
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/sales/pr_pdf/'.$result['id'].'/'.$v['id'])?>" target="_blank">Download PDF - <?=$v['company_name']?></a>
                                    <?php }?>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/pr_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file">
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                		<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            
                            <?php 
							break;
							case 'complete':
							?>
                            	<?php if($last_action == 'upload'){?>
                            	<div class="panel">
                                	<?php foreach($supplier_list as $k => $v){?>
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/sales/pr_pdf/'.$result['id'].'/'.$v['id'])?>" target="_blank">Download PDF - <?=$v['company_name']?></a>
                                    <?php }?>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/pr_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file"> 
                                            <a target="_blank" href="<?=$result['confirm_file']?>"><?=$result['confirm_file']?></a>
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Reupload</button>
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                                <?php }?>
                            
                            <?php 
							break;
                        }?>

                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Status
                        </div>
                        <div class="panel-body">
                            <div class="form-group input-group">
							<?php 
                                //如果不是not approved, 也不是 completed 就顯示目前正在等誰
                                if($result['status'] != 0 && $result['status'] != $completed_status_id){
                            ?>
                            <label>
                                                    <?php
                                                    if(!empty($result['awaiting_table'])){
                                                        echo 'Awaiting ';
                                                        foreach($result['awaiting_table'] as $x){
                                                            if(isset($role_list[$x])){
                                                            echo '<br>'.$role_list[$x];
                                                            }
                                                        }
                                                    }
                                                    ?>
                            </label><br>
                            <?php 
                            } 
                            ?>
                            <label><?=$pr_status_list[$result['status']]?></label>
                            </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                    
                    <?php } //Edit mode end ?>
                    
                                        	
                    
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->

<script>

var delivery_dateCounter = <?=($result['status']>1 && $result['status']!=0)?count($related_product2):'0'?>;

var btn_type = '<?=$btn_type?>';
		
					//change input to read only
					if(btn_type != 'create' && btn_type != 'edit' && btn_type != 'sendmail_update'){
						
						$(".form-control").each(function() {
						  $(this).prop('disabled', 'disabled');
						});
						
						$(".btn-circle").each(function() {
						  $(this).hide();
						});
						$('#addProduct').hide();
						$('#addService').hide();
						if(btn_type == 'update' || btn_type == 'approval_update'){
							
							var target_colum = <?=isset($target_colum)?$target_colum:'[]'?>;
							
							for(var i=0;i<target_colum.length;i++){
								var colum = target_colum[i].name;
								if(target_colum[i].type == 'single'){
									$("input[name='"+colum+"']").prop('disabled', false);
									
								}else if(target_colum[i].type == 'multiple'){
									$("."+colum).prop('disabled', false);
								}
							}
							
							
						}
					}

if(delivery_dateCounter > 0){
	
	for(var i=0;i<delivery_dateCounter;i++){
		$( "#delivery_date"+i ).datepicker({
		dateFormat:'dd/mm/yy',
		});
	}
}


function downloadPDF(url){
	location.href=url;
}

function sentMail(url){
	location.href=url;
}

$( "#created_date" ).datepicker({
	dateFormat:'dd/mm/yy',
});

</script>