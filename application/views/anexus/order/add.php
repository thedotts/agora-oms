<link rel="stylesheet" href="<?=base_url('assets/css/jquery-ui.css')?>">
<script src="<?=base_url('assets/js/jquery-ui.js')?>"></script>
<div id="page-wrapper">
        	<form action="<?=base_url($init['langu'].'/agora/sales/order_submit');?>" method="post" enctype="multipart/form-data" onsubmit="return validate();">
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" id="requestor_id" name="requestor_id" value="<?=$mode=='Edit'?$result['requestor_id']:$staff_info['id']?>"/>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-briefcase fa-fw"></i> <?=$head_title?>
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Order Request Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Order No.</label>
                                    <input class="form-control" value="<?=$mode=='Edit'?$result['order_no']:''?>" disabled>
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Order Date *</label>
                                    <input class="form-control" placeholder="Calendar Input" id="order_date" name="order_date" value="<?=$mode=='Edit'?$result['order_date']:''?>">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Customer Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Company Name *</label>
                                    <select id="company" name="company" class="form-control" onchange="changeItem_price()">
                                        <option>-</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Attention</label>
                                    <input id="attention" class="form-control" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input id="address" class="form-control" value="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Postal Code</label>
                                    <input id="postal" class="form-control" value="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Country</label>
                                    <input id="country" class="form-control" value="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input id="email" class="form-control" value="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Contact No.</label>
                                    <input id="contact" class="form-control" value="" disabled>
                                </div>
                                
                                <div class="form-group">
                                    <label>Delivery Address *</label>
                                    <select class="form-control" id="delivery_address" name="delivery_address" onchange="show_da()">
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input id="address2" name="address2" class="form-control" value="" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Postal Code</label>
                                    <input id="postal2" name="postal2" class="form-control" value="" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Country</label>
                                    <input id="country2" name="country2" class="form-control" value="" readonly>
                                </div>
                                
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
            </div>
            <!-- /.row -->
            
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Items 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                            	<button class="btn btn-default btn-sm" type="button" onclick="add_item()" <?=$mode=='Edit'?($result['status']==2?'disabled':''):''?>>Add New Product Item</button>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Product Name *</td>
                                            <th style="display:none">Country *</th>
                                            <th width="200px">Quantity *</th>
                                            <th width="200px">Inventory Balance</th>
                                            <th width="100px">Unit Price</th>
                                            <th width="80px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="item_body">
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                        
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Total Amount
                        </div>
                        <div class="panel-body">
                        		<div class="form-group">
                                	<label>Total Amount</label>
                                    <input id="total_amount" name="total_amount" class="form-control" value="<?=$mode=='Edit'?$result['total_price']:'0'?>" readonly="readonly"/>
                                </div>
                                <div class="form-group">
                                	<label>GST Amount</label>
                                    <input id="gst_amount" name="gst_amount" class="form-control" value="<?=$mode=='Edit'?$result['gst_amount']:'0'?>" readonly="readonly"/>
                                </div>
                                <div class="form-group">
                                	<label>Nett Amount</label>
                                    <input id="nett_amount" name="nett_amount" class="form-control" value="<?=$mode=='Edit'?$result['total_price']+$result['gst_amount']:'0'?>" readonly="readonly"/>
                                </div>
                                
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                        
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Remarks
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <textarea id="remarks" name="remarks" class="form-control"><?=$mode=='Edit'?$result['remark']:''?></textarea>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    <?php if($mode=='Add'){?>
                    
                    	<?php if($btn_type == 'create'){?>
                        <div class="form-group">
                        <button onclick="check_customer()" type="submit" class="btn btn-primary">Submit</button>
                        <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                        </div>
                    	<?php }?>
                        
                    <?php }else if($mode=='Edit'){?>
                    	<div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/agora/sales/order_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                            	</div>
                    	<?php switch($btn_type){
							case 'edit':
							?>
                        		<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Submit</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            <?php 
							break;
							case 'approval':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Update</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'approval_update':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'approval_update_s':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="check_correct" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<!--<button type="submit" id="save_btn" class="btn btn-primary" name="confirm" value="2">Save</button>-->
                                	<button type="submit" id="next_btn" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                </div>
                            <?php 
							break;
							case 'sendmail_update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-default">Save</button>
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/sales/tender_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/tender_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'send_email':
							?>
                            	<div class="form-group">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/sales/tender_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/tender_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                </div>
                            
                            <?php 
							break;
							case 'upload':
							case 'upload_update':
							?>

                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="confirm_file" name="confirm_file" type="file">
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                		<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            
                            <?php 
							break;
							case 'complete':
							?>
                            	<?php switch($last_action){
								case 'upload':
								?>

                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="confirm_file" name="confirm_file" type="file"> 
                                            <a target="_blank" href="<?=$result['confirm_file']?>"><?=$result['confirm_file']?></a>
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Reupload</button>
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
								
                                <?php 
								break;
								case 'pdf':
								?>
                                <div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/agora/ground_staff/packing_list_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                            	</div>
                                <?php 
								break;
								}?>
                                
                            <?php 
							break;
                        }?>

                    <div class="panel panel-primary" style="display:none">
                    	<div class="panel-heading">
                            Status
                        </div>
                        <div class="panel-body">
                            <div class="form-group input-group">
							<?php 
                                //如果不是not approved, 也不是 completed 就顯示目前正在等誰
                                if($result['status'] != 0 && $result['status'] != $completed_status_id){
                            ?>
                            <label>
                                                    <?php
                                                    if(!empty($result['awaiting_table'])){
                                                        echo 'Awaiting ';
                                                        foreach($result['awaiting_table'] as $x){
                                                            if(isset($role_list[$x])){
                                                            echo '<br>'.$role_list[$x];
                                                            }
                                                        }
                                                    }
                                                    ?>
                            </label><br>
                            <?php 
                            } 
                            ?>
                            <label><?=$status_list[$result['status']]?></label>
                            </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                    
                    <?php } //Edit mode end ?>

                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->
        
<script>
var mode ='<?=$mode?>';
var company_json = <?=$company_list?>;

var product_name = <?=$product_name?>;
var product = <?=$product?>;


var item_counter = 0;
var order_product = <?=$mode=='Edit'?$result['product']:'[]'?>;
var status = '<?=isset($result['status'])?$status_list[$result['status']]:''?>';
var requestor_id = <?=isset($result['requestor_id'])?$result['requestor_id']:0?>;
var user_id = <?=isset($userdata['id'])?$userdata['id']:0?>;
var role = <?=json_encode($userdata['role_id'])?>;



//product select
var product_sel = '<option>-</option>';
for(var i=0;i<product_name.length;i++){
	product_sel +='<option value="'+product_name[i].product_name+'">'+product_name[i].product_name+'</option>';
}

$( "#order_date" ).datepicker({
	dateFormat:'dd/mm/yy',
});

//get all company list
for(i=0;i<company_json.length;i++){
	var temp = '';
	temp = $('<option>').attr('value',company_json[i].id+'|'+company_json[i].company_name).html(company_json[i].company_name);
	$('#company').append(temp);
}

//set datepicker get current date
if(mode == 'Add'){
	$('#order_date').datepicker('setDate', 'today');
	add_item();
}else{
	$('#company').val('<?=$mode=='Edit'?$result['customer_id'].'|'.$result['customer_name']:''?>');
	get_companyItem('<?=$mode=='Edit'?$result['customer_id'].'|'.$result['customer_name']:''?>');
	$('#delivery_address').val('<?=$mode=='Edit'?$result['alias']:''?>');
	show_da();
	
	for(var i=0;i<order_product.length;i++){
		add_item();
	}
	
	if(status == 'Completed' || !inArray(3, role)){
	$(".form-control").each(function() {
		$(this).prop('disabled', 'disabled');
	});
	}
	
}

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

$('#company').change(function() {
	var id = $(this).val();
	get_companyItem(id);	
});

function get_companyItem(id){
	
	value = id.split("|");
	id = value[0];
	
	if(id == '-'){
		$('#attention').val('');
		$('#address').val('');
		$('#postal').val('');
		$('#country').val('');
		$('#email').val('');
		$('#contact').val('');
		$('#delivery_address').html('');
		
		$('#address2').val('');
		$('#postal2').val('');
		$('#country2').val('');
		
	}else{
		for(i=0;i<company_json.length;i++){
		
			if(company_json[i].id == id){
					
				$('#attention').val(company_json[i].primary_attention_to);
				$('#address').val(company_json[i].address);
				$('#postal').val(company_json[i].postal);
				$('#country').val(company_json[i].country);
				$('#email').val(company_json[i].primary_contact_email);
				$('#contact').val(company_json[i].primary_contact_info);
				
				$('#delivery_address').html('');
				var da = JSON.parse(company_json[i].delivery_address);
				html ='<option>-</option>';
	
				for(var i=0;i<da.length;i++){
					html +='<option value="'+da[i].alias+'">'+da[i].alias+'</option>';
				}
				
				$('#delivery_address').html(html);
				
				$('#address2').val('');
				$('#postal2').val('');
				$('#country2').val('');
				
				break;
			}
		
		}
	}
}

function add_item(){
	
	item_counter++;
	
	if(mode == 'Edit' && item_counter <= order_product.length){
	
	html = '<tr id="item_'+item_counter+'">';
    html += '<td>'+item_counter+'</td>';
	html += '<td><input class="form-control item product_name" data-name="Product Name" data-count="'+item_counter+'" id="product'+item_counter+'"></td>';
	//html += '<td><select class="form-control item" data-name="Product Name" id="product'+item_counter+'" onchange="product_model('+item_counter+')">'+product_sel+'</select></td>';
	html += '<td style="display:none"><select class="form-control item" data-name="Description" id="model_name'+item_counter+'" name="product[]" onchange="product_detail('+item_counter+',0)"><option>-</option></select>';
	html += '<input type="hidden" id="model_name2_'+item_counter+'" name="model_name[]"></td>';
	html += '<td><div class="form-group input-group"><input onblur="check_qty('+item_counter+')" id="qty'+item_counter+'" name="qty[]" type="text" class="form-control item" data-name="Quantity" value="'+order_product[item_counter-1].quantity+'"><span class="input-group-addon" id="uom2'+item_counter+'"></span></div>';
	html += '<input id="uom'+item_counter+'" name="uom[]" type="hidden" class="form-control" value="" readonly></td>';
	html += '<td><div class="form-group input-group"><input id="qty_balance'+item_counter+'" name="qty_balance[]" type="text" class="form-control" value="" readonly><span class="input-group-addon" id="uom3'+item_counter+'"></span></div></td>';
	html += '<td><input id="price'+item_counter+'" name="price[]" type="text" class="form-control"  value="'+order_product[item_counter-1].price+'" readonly></td>';        
	html += '<td><a id="link'+item_counter+'" href="'+'<?=base_url($init['langu'].'/agora/administrator/products_edit/')?>'+'/'+order_product[item_counter-1].product_id+'" target="_blank"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></a>';                                 
    html += '<button onclick="del_item('+item_counter+')" class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button></td>';
	html += '<input type="hidden" id="id'+item_counter+'" name="old_id[]" value="'+order_product[item_counter-1].id+'"/>'; 
	html += '<input type="hidden" id="is_del'+item_counter+'" name="is_del[]" value="0"/>'; 
	html += '<input type="hidden" id="promo'+item_counter+'" name="promo[]" value="0"/>';
	html += '</tr>';     
	
	}else{
		
	html = '<tr id="item_'+item_counter+'">';
    html += '<td>'+item_counter+'</td>';
	html += '<td><input class="form-control item product_name" data-name="Product Name" id="product'+item_counter+'" data-count="'+item_counter+'"></td>';
	//html += '<td><select class="form-control item" data-name="Product Name" id="product'+item_counter+'" onchange="product_model('+item_counter+')">'+product_sel+'</select></td>';
	html += '<td style="display:none"><select class="form-control item" data-name="Description" id="model_name'+item_counter+'" name="product[]" onchange="product_detail('+item_counter+',0)"><option>-</option></select>';
	html += '<input type="hidden" id="model_name2_'+item_counter+'" name="model_name[]"></td>';
	html += '<td><div class="form-group input-group"><input onblur="check_qty('+item_counter+')" id="qty'+item_counter+'" name="qty[]" type="text" class="form-control item" data-name="Quantity" value=""><span class="input-group-addon" id="uom2'+item_counter+'"></span></div>';
	html += '<input id="uom'+item_counter+'" name="uom[]" type="hidden" class="form-control" value="" readonly></td>';
	html += '<td><div class="form-group input-group"><input id="qty_balance'+item_counter+'" name="qty_balance[]" type="text" class="form-control" value="" readonly><span class="input-group-addon" id="uom3'+item_counter+'"></span></div></td>';  
	html += '<td><input id="price'+item_counter+'" name="price[]" type="text" class="form-control"  value="0" readonly></td>';     
	html += '<td><a id="link'+item_counter+'" target="_blank"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></a>';                                 
    html += '<button onclick="del_item('+item_counter+')" class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button></td>';
	html += '<input type="hidden" id="id'+item_counter+'" name="old_id[]" value="0"/>'; 
	html += '<input type="hidden" id="is_del'+item_counter+'" name="is_del[]" value="0"/>'; 
	html += '<input type="hidden" id="promo'+item_counter+'" name="promo[]" value="0"/>'; 
	html += '</tr>';   
	
	}
         
	$('#item_body').append(html);
	
	//preselect
	if(mode == 'Edit' && item_counter <= order_product.length){  
	
		for(var j=0;j<product.length;j++){
			if(product[j].id == order_product[item_counter-1].product_id){
				var product_name = product[j].product_name; 
				var product_c_id = j;
			}
		}
	
		$('#product'+item_counter).val(product_name); 
		product_model(item_counter); 
		$('#model_name'+item_counter).val(order_product[item_counter-1].product_id);                    
		product_detail(item_counter,1);
	}
	
	var tmp_auto_id = '';
	for(var i=1;i<=item_counter;i++){
		if(tmp_auto_id==''){
			tmp_auto_id = '#product'+i;
		}else{
			tmp_auto_id += ',#product'+i;
		}
	}
	
	if(tmp_auto_id != ''){
		//alert(tmp_auto_id);
	$( tmp_auto_id ).autocomplete({
      source: function( request, response ) {
		  
		  
        $.ajax({
          url: "<?=base_url($init['langu'].'/agora/sales/searchProduct/')?>/"+request.term,
          dataType: "json",
          //data: {"search_type":search_type,"order_type":order_type},
          success: function( data ) {
            response( data );
          }
        });
      },
      select: function (event, ui) {
        event.preventDefault();
        $(this).val(ui.item.label); // display the selected text
        //$("#po_id").val(ui.item.value); // save selected id to hidden input
        //toSearch();
		
		var tmp_count = $(this).data('count');
		product_model(tmp_count);
		product_detail(tmp_count,0);
		
        }
    });
	
	}
	
}

function del_item(i){
	$('#item_'+i).hide();
	$('#is_del'+i).val(1);
}

function product_model(i){
	
	product_name = $('#product'+i).val();
	var model_sel = '';
	
		
		if(product_name != '-'){
			for(var j=0;j<product.length;j++){
				if(product[j].product_name == product_name){
					model_sel +='<option value="'+product[j].id+'">'+product[j].model_name+'</option>';
					
				}
			}
			
			$('#model_name'+i).html('');
			console.log('#model_name'+i,model_sel);
			$('#model_name'+i).append(model_sel);
			$('#price'+i).val('');
			if(mode=='Add'){
			$('#qty'+i).val('');
			}
			
			
			
		}else{
			$('#model_name'+i).html('<option>-</option>');
			$('#model_name2_'+i).val('');
			$('#qty_balance'+i).val('');	
			$('#price'+i).val('');
			if(mode=='Add'){
			$('#qty'+i).val('');
			}
		}
		
		$('#uom2'+i).text('');
		$('#uom3'+i).text('');
	
	
}


function product_detail(i,product_c_id){
	product_id = $('#model_name'+i).val();
	company = $('#company').val();
	
	
	//如果company id吻合取 使用特定價格還是原本價格
	var default_price = 1;
	var company_json_id = 0;
	var company = company.split("|");
	for(var j=0;j<company_json.length;j++){
		
		if(company_json[j].id == company[0]){
			default_price = company_json[j].use_default_price;
			company_json_id = j;
			break;
		}
	}
	
	
	
		if(product_id != '-'){
			
			
				for(var j=0;j<product.length;j++){
					if(product[j].id == product_id){
						$('#qty_balance'+i).val(product[j].qty != null?product[j].qty:0);
						document.getElementById('link'+i).href='<?=base_url($init['langu'].'/agora/administrator/products_edit/')?>'+'/'+product_id; 
						$('#model_name2_'+i).val(product[j].model_name);
						$('#uom'+i).val(product[j].uom);
						$('#uom2'+i).text(product[j].uom+'(s)');
						$('#uom3'+i).text(product[j].inventory_uom+'(s)');
						
						
						//原本價格
						var promo_val = 0;
						if(default_price == 1){
							
							var no_promo_id = product[j].no_promo_id.split(',');
							
							var price = product[j].selling_price;
							if(product[j].promo_price != '0.00' && price > product[j].promo_price && !inArray(company[0],no_promo_id)&&product[j].promo_val!=1){
								price = product[j].promo_price;
								promo_val = 1;
							}
							$('#price'+i).val(price);
							$('#promo'+i).val(promo_val);
							
							//可能promo price edit 取原本order item 價格
							if(product_c_id != 0){
								$('#price'+i).val(order_product[i-1].price);
							}
							
						
						//特定價格	
						}else{
							
							var tmp_data = JSON.parse(company_json[company_json_id].price_list);
							
							
							for(var x=0;x<tmp_data.length;x++){
								if(tmp_data[x].id == product_id){
									
									var no_promo_id = product[x].no_promo_id.split(',');
									
									var price = tmp_data[x].price;
									if(product[x].promo_price != '0.00' && price > product[x].promo_price && !inArray(company[0],no_promo_id)&&product[x].promo_val!=1){
										price = product[x].promo_price;
										promo_val = 1;
									}
									
									$('#price'+i).val(price);
									
									//可能promo price edit 取原本order item 價格
									if(product_c_id != 0){
										$('#price'+i).val(order_product[i-1].price);
									}
									
									$('#promo'+i).val(promo_val);
									console.log(tmp_data[x].price);
									break;
								}
							}
							
						}
						
						break;
						
					}
				}
				
				if(mode=='Add'){
					$('#qty'+i).val('');
				}
			
		}else{
			$('#model_name2_'+i).val('');
			$('#qty_balance'+i).val('');
			$('#price'+i).val('');	
			$('#uom2'+i).text('');
			$('#uom3'+i).text('');
		}
	
	
}


function check_qty(i){

	var qty_balance = parseInt($('#qty_balance'+i).val());
	var qty = parseInt($('#qty'+i).val());
	
	if(qty_balance < qty){
		$('#qty_balance'+i).css("border","1px solid #F00");
	}else{
		$('#qty_balance'+i).css("border","1px solid #ccc");
	}
	
	total_amount();
	
	//console.log(qty_balance < qty);
	
}

var gst = <?=$gst['value']?>;

function total_amount(){

	var total_amount = 0;
	
	for(var i=1;i<=item_counter;i++){
		
		var qty = $('#qty'+i).val();
		var price = $('#price'+i).val();
		
		if(qty != '' && !isNaN(qty)){
			total_amount += parseFloat(price)*parseFloat(qty);
		}
		
	}
	
	var gst_amount = (total_amount*gst)/100;
	
	$('#gst_amount').val(gst_amount.toFixed(2));
	$('#total_amount').val(total_amount.toFixed(2));
	$('#nett_amount').val((total_amount+gst_amount).toFixed(2));
	
}

$('#next_btn').click(function(event){
	
	if(!($('input[name="check_correct"]').prop('checked'))){
		event.preventDefault();
		alert('Please checked the checkbox before proceeding');
	}
	
});

function validate(){
	var error = 0;
	
	var company = $('#company').val();
	if(company == '-' || company == null){
		error = 1;
		alert('Company Name cant be blank');
	}
	
	var da = $('#delivery_address').val();
	if(da == '-' || da == null){
		error = 1;
		alert('Delivery address cant be blank');
	}
	
	
	//item
	$('.item').each(function() {
		
		//is del
		var is_del = $(this).closest('tr').find('input[name="is_del[]"]').val();
		var uom = $(this).closest('tr').find('input[name="uom[]"]').val();
		
		if(is_del != 1){
		
			var name = $(this).data('name');
			if($(this).val() == '' || $(this).val() == '-'){
				alert(name+' can\'t be blank');
				$(this).focus();
				error = 1;
				return false;
				
			}else{
				
				if(name == 'Quantity' && isNaN($(this).val())){
					alert(name+' needs to be integer only');
					$(this).focus();
					error = 1;
					return false;
				}else if(name == 'Quantity' && $(this).val() == 0){
					alert(name+' can\'t be zero');
					$(this).focus();
					error = 1;
					return false;
				}else if(name == 'Quantity' && $(this).val() %1!=0 && uom != 'kg'){
					
					alert(name+' wrong format');
					$(this).focus();
					error = 1;
					return false;
					
				}
				
			}
		
		}
		
    });
	
	if(error == 1){
		return false;
	}
		
}


function changeItem_price(){
	
	var company = $('#company').val();
	company = company.split("|");
	var default_price = 1;
	var company_json_id = 0;
	for(var j=0;j<company_json.length;j++){
		if(company_json[j].id == company[0]){
			default_price = company_json[j].use_default_price;
			company_json_id = j;
			break;
		}
	}
	
	
	for(var i = 1;i <= item_counter;i++){
	
		var product_id = $('#model_name'+i).val();
		
		if(product_id !='' && product_id !='-'){
				for(var j=0;j<product.length;j++){
					var promo_val = 0;
					if(product[j].id == product_id){
						
						
						//原本價格
						if(default_price == 1){
							
							var no_promo_id = product[j].no_promo_id.split(',');
							
							var price = product[j].selling_price;
							if(product[j].promo_price != '0.00' && price > product[j].promo_price && !inArray(company[0],no_promo_id)&&product[j].promo_val!=1){
								price = product[j].promo_price;
								promo_val = 1;
							}
							$('#price'+i).val(price);
							$('#promo'+i).val(promo_val);

						//特定價格	
						}else{
							
							var tmp_data = JSON.parse(company_json[company_json_id].price_list);
							
							for(var x=0;x<tmp_data.length;x++){
								if(tmp_data[x].id == product_id){
									
									var no_promo_id = product[x].no_promo_id.split(',');
									
									var price = tmp_data[x].price;
									if(product[x].promo_price != '0.00' && price > product[x].promo_price && !inArray(company[0],no_promo_id)&&product[x].promo_val){
										price = product[x].promo_price;
										promo_val = 1;
									}
									
									$('#price'+i).val(price);
									$('#promo'+i).val(promo_val);
									console.log(tmp_data[x].price);
									break;
								}
							}
							
						}
						
						break;
						
					}
				}
		}
				
	}//for
	
	total_amount();
	
}


function show_da(){
	
	id = $('#company').val();
	id = id.split('|');
	id = id[0];
	
	for(i=0;i<company_json.length;i++){
		
			if(company_json[i].id == id){

				var da = JSON.parse(company_json[i].delivery_address);

				
				break;
			}
		
	}
	
	var sel = $('#delivery_address').val();
	
	if(sel != '-'){
		
		for(var i=0;i<da.length;i++){
			if(da[i].alias == sel){
				
				$('#address2').val(da[i].add);
				$('#postal2').val(da[i].postal);
				$('#country2').val(da[i].country);
				break;				
			}
		}
		
	}else{
		$('#address2').val('');
		$('#postal2').val('');
		$('#country2').val('');
	}
}


$(function() {
    
    
	
});

</script>