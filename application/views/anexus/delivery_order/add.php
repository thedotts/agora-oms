<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-calendar fa-fw"></i> <?=$head_title?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                       		<!-- Button trigger modal -->
                            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" <?=$mode=='Edit' || $job_id!=0?'disabled':''?>>
                                Seach from Packing List
                            </button>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Search Packing List</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Search</label>
                                                <input class="form-control" placeholder="By Company Name or Packing List No." id="search" name="search">
                                            </div>
                                            <table id="table" class="table table-striped table-bordered table-hover" id="dataTables">
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            
            <form action="<?=base_url($init['langu'].'/agora/admin/delivery_order_submit');?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" id="order_id" name="order_id" value="<?=$mode=='Edit'?$result['order_id']:0?>"/>
            <input type="hidden" id="packing_id" name="packing_id" value="<?=$mode=='Edit'?$result['packing_id']:0?>"/>
            <input type="hidden" id="order_no" name="order_no" value="<?=$mode=='Edit'?$result['order_no']:0?>"/>
            <input type="hidden" id="requestor_id" name="requestor_id" value="<?=$mode=='Edit'?$result['requestor_id']:$staff_info['id']?>"/>
            <input type="hidden" id="item_counter" name="item_counter" value="0"/>
            <input type="hidden" id="job_id" name="job_id" value="<?=isset($job_id)?$job_id:0?>"/>
            <div class="row">

                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Delivery Order Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Delivery Order No.</label>
                                    <input class="form-control" value="<?=$mode=='Edit'?$result['do_no']:''?>" disabled>
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Date of Delivery Order *</label>
                                    <input id="do_date" name="do_date" class="form-control" placeholder="Calendar Input" value="<?=$mode=='Edit'?$result['do_date']:''?>">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Staff Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Staff Name</label>
                                    <input id="staff_name" name="staff_name" class="form-control" value="<?=$mode=='Edit'?$result['staff_name']:$staff_info['full_name']?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Role</label>
                                    <?php 
									 if($mode=='Edit'){
										 
                                    	if(strpos($result['staff_role'],',')){
											$roleId = explode(',',$result['staff_role']);	
										}else{
											$roleId = array($result['staff_role']);	
										}
											
                                    }else{
										$roleId = $userdata['role_id'];
									}
									$role_name = '';
									foreach($roleId as $v){
										$role_name .= $role_list[$v].',';
									}
									
									$role_name = substr($role_name,0,strlen($role_name)-1);
									
									?>
                                    <input class="form-control" value="<?=$role_name?>" disabled>
                                    <input id="staff_role" name="staff_role" type="hidden" value="<?=$mode=='Edit'?$result['staff_role']:$staff_info['role_id']?>" readonly>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Customer Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input id="customer_id" name="customer_id" type="hidden" value="<?=$mode=='Edit'?$customer_info['id']:''?>">
                                    <input id="customer_name" name="customer_name" class="form-control" value="<?=$mode=='Edit'?$customer_info['company_name']:''?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Attention</label>
                                    <input id="attention" name="attention" class="form-control" value="<?=$mode=='Edit'?$customer_info['primary_attention_to']:''?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input id="email" name="email" class="form-control" value="<?=$mode=='Edit'?$customer_info['primary_contact_email']:''?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Contact No.</label>
                                    <input id="contact_no" name="contact_no" class="form-control" value="<?=$mode=='Edit'?$customer_info['primary_contact_info']:''?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Delivery Address *</label>
                                    <select class="form-control" id="delivery_address" name="delivery_address" onchange="show_da()">
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input id="address" name="address" class="form-control" value="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Postal Code</label>
                                    <input id="postal" name="postal" class="form-control" value="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Country</label>
                                    <input id="country" name="country" class="form-control" value="" disabled>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
            </div>
            <!-- /.row -->
            <hr />
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Items 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Product Name</th>
                                            <th style="display:none">Country</th>
                                            <th width="200px">Qty to Deliver</th>
                                            
                                            <th width="80px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody">
                                    <?php if($mode =='Edit'){?>
                                    <?php foreach($result['product'] as $k => $v){?>
                                    	<tr>
                                        <td><?=($k+1)?></td>
                                        <td>
                                        <input type="text" class="form-control" value="<?=$product_list[$v['product_id']]?>" id="product_name<?=$k?>" name="product_name<?=$k?>" readonly>
                                        <input type="hidden" value="<?=$v['product_id']?>" id="product_id<?=$k?>" name="product_id<?=$k?>" ><input type="hidden" value="<?=$v['price']?>" id="price<?=$k?>" name="price<?=$k?>" >
                                        </td>
                                        <td>
                                        <input type="text" class="form-control" value="<?=$v['model_name']?>" id="model_name<?=$k?>" name="model_name<?=$k?>" readonly>
                                        </td>
                                        <td>
                                        <div class="form-group input-group">
                                        <input type="text" class="form-control" value="<?=$v['quantity_del']?>" id="quantityDel<?=$k?>" name="quantityDel<?=$k?>" readonly>
                                        <span class="input-group-addon" id="uom3'+item_counter+'"><?=$v['uom']?>(s)</span>
                                        </div>
                                        </td>
                                        
                                        <td>
                                        <a href="<?=base_url($init['langu'].'/agora/administrator/products_edit/'.$v['product_id'])?>" target="_blank"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></a>
                                        </td>
                                        </tr>
                                    <?php }?>
                                    <?php }?>
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                        
                        
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Special Instructions
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <textarea class="form-control" id="instructions" name="instructions"><?=$mode=='Edit'?$result['instructions']:''?></textarea>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    <div class="panel">
                                
                    <?php if($mode=='Add'){?>
                    
                    	<?php if($btn_type == 'create'){?>
                        <div class="form-group">
                        <button onclick="checkDA()" type="submit" class="btn btn-primary">Submit</button>
                        <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                        </div>
                    	<?php }?>
                        
                    <?php }else if($mode=='Edit'){?>
                    	<div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/agora/admin/do_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                            	</div>
                    	<?php switch($btn_type){
							case 'edit':
							?>
                        		<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Submit</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            <?php 
							break;
							case 'approval':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Update</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'approval_update':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'sendmail_update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-default">Save</button>
                                	<a class="btn btn-default" href="<?=base_url('en/agora/sales/tender_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/tender_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'send_email':
							?>
                            	<div class="form-group">
                                	<a class="btn btn-default" href="<?=base_url('en/agora/sales/tender_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/tender_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                </div>
                            
                            <?php 
							break;
							case 'upload':
							case 'upload_update':
							?>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="confirm_file" name="confirm_file" type="file">
                                        </div>
                                        <button onclick="checkUpload()" type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                		<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            <?php 
							break;
							case 'complete':
							?>
                            	<?php switch($last_action){
								case 'upload':
								?>

                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="confirm_file" name="confirm_file" type="file"> 
                                            <a target="_blank" href="<?=$result['confirm_file']?>"><?=$result['confirm_file']?></a>
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Reupload</button>
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
								
                                <?php 
								break;
								case 'pdf':
								?>
                                <div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/agora/admin/do_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                            	</div>
                                <?php 
								break;
								case 'upload_pdf':
								?>
                                <div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/agora/admin/do_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                            	</div>
                                
                                <div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="confirm_file" name="confirm_file" type="file"> 
                                            <a target="_blank" href="<?=$result['confirm_file']?>"><?=$result['confirm_file']?></a>
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Reupload</button>
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                                <?php 
								break;
								}?>
                                
                            <?php 
							break;
                        }?>

                    <div class="panel panel-primary" style="display:none">
                    	<div class="panel-heading">
                            Status
                        </div>
                        <div class="panel-body">
                            <div class="form-group input-group">
							<?php 
                                //如果不是not approved, 也不是 completed 就顯示目前正在等誰
                                if($result['status'] != 0 && $result['status'] != $completed_status_id){
                            ?>
                            <label>
                                    <?php
                                    if(!empty($result['awaiting_table'])){
                                    echo 'Awaiting ';
                                    foreach($result['awaiting_table'] as $x){
                                          if(isset($role_list[$x])){
                                              echo '<br>'.$role_list[$x];
                                          }
                                    }
                                    }
                                    ?>
                            </label><br>
                            <?php 
                            } 
                            ?>
                            <label><?=$status_list[$result['status']]?></label>
                            </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                    
                    <?php } //Edit mode end ?>
                    </div>
                    <!-- /.panel -->
                    
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->
        
<script>
var mode ='<?=$mode?>';
var da = <?=$mode=='Edit'?$customer_info['delivery_address']:'[]'?>;
var status = '<?=isset($result['status'])?$status_list[$result['status']]:''?>';
var requestor_id = <?=isset($result['requestor_id'])?$result['requestor_id']:0?>;
var user_id = <?=isset($userdata['id'])?$userdata['id']:0?>;

$( "#do_date" ).datepicker({
	dateFormat:'dd/mm/yy',
});

//set datepicker get current date
if(mode == 'Add'){
	$('#do_date').datepicker('setDate', 'today');
}else{
	delivery_address();
	$('#delivery_address').val('<?=$mode=='Edit'?$result['alias']:''?>');
	show_da();
	
	if(status == 'Completed' || requestor_id != user_id){
	$(".form-control").each(function() {
		$(this).prop('disabled', 'disabled');
	});
	}
}

$( "#search" ).keyup(function(){			
	ajax_quotation(1);
});
		
$('#myModal').on('show.bs.modal', function (event) {
	ajax_quotation(1);
})
		
function changePage(i){
	ajax_quotation(i);
}

function ajax_quotation(i){
			
	keyword = $("#search").val();
	term = {keyword:keyword,limit:10,page:i};
	url = '<?=base_url($init['langu'].'/agora/admin/getSearch')?>';
			
	$.ajax({
		type:"POST",
		url: url,
		data: term,
		dataType : "json"
		}).done(function(r) {
			if(r.status == "OK") {
				tmp = '';
					
				if(r.data != false){
					for(var i=0;i<r.data.length;i++){
						
						tmp+='<tr><td>'+r.data[i].customer_name+'</td><td>'+r.data[i].order_no+'</td><td><a href="javascript:getSearchData('+r.data[i].id+',1)">'+r.data[i].packing_no+'</a></td><td>'+r.data[i].modified_date+'</td><td>'+r.data[i].status+'</td></tr>';
					
					}
					
					html = '<thead><tr><th>Company Name</th><th>Order No.</th><th>Packing List No.</th><th>Date Last Updated</th><th>Status</th></tr></thead><tbody>'+tmp+'</tbody>';
					
					pagination = r.paging;
					
					$('#table').html('');
					$('#pagination').html('');
					$('#table').append(html);
					$('#pagination').append(pagination);
					
				}else{
					
					$('#table').html('No search result');
						$('#pagination').html('');
					}
					
				}
	});	
	
}

function getSearchData(id,mode){
			
	if(mode == 1){
		$('#myModal').modal('hide');
	}
			
	term = {id:id};
	url = '<?=base_url($init['langu'].'/agora/admin/getSearchData')?>';
			
		$.ajax({
		  type:"POST",
		  url: url,
		  data: term,
		  dataType : "json"
		}).done(function(r) {
			if(r.status == "OK") {
				console.log(r);

				$('#packing_id').val(r.data.id);
				$('#order_id').val(r.data.order_id);
				$('#order_no').val(r.data.order_no);
				$('#customer_id').val(r.data.customer_id);
				$('#customer_name').val(r.data.customer_name);
				$('#attention').val(r.data.attention);
				$('#email').val(r.data.email);
				$('#contact_no').val(r.data.contact_no);
				$('#job_id').val(r.data.latest_job_id);
					
				da = JSON.parse(r.data.da);
				
				delivery_address();
					
				
					$('#delivery_address').val(r.data.alias);
					show_da();
				
					
				$('#tbody').html('');
				
				var html = '';
				for(var x=0;x<r.product.length;x++){
					
					var qty_balance = parseInt(r.product[x].quantity_order) - parseInt(r.product[x].quantity_pack);
					

					html+='<tr><td>'+(x+1)+'</td><td><input type="text" class="form-control" value="'+r.product[x].product_name+'" id="product_name'+x+'" name="product_name'+x+'" readonly><input type="hidden" value="'+r.product[x].product_id+'" id="product_id'+x+'" name="product_id'+x+'" ><input type="hidden" value="'+r.product[x].price+'" id="price'+x+'" name="price'+x+'" ></td><td style="display:none"><input type="text" class="form-control" value="'+r.product[x].model_name+'" id="model_name'+x+'" name="model_name'+x+'" readonly></td><td><div class="form-group input-group"><input type="text" class="form-control" value="'+r.product[x].quantity_pack+'" id="quantityDel'+x+'" name="quantityDel'+x+'" readonly><span class="input-group-addon" id="uom1'+x+'">'+r.product[x].uom+'(s)</span></div></td><td><a href="'+'<?=base_url($init['langu'].'/agora/administrator/products_edit/')?>'+'/'+r.product[x].product_id+'" target="_blank"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></a></td></tr>';
					
				}
				
				$('#item_counter').val(r.product.length);
				
				$('#tbody').append(html);
			}
		});	
			
}

function delivery_address(){
	
	html ='<option>-</option>';
	
	for(var i=0;i<da.length;i++){
		html +='<option value="'+da[i].alias+'">'+da[i].alias+'</option>';
	}
	
	$('#delivery_address').html(html);
	
}

function show_da(){
	var sel = $('#delivery_address').val();
	
	if(sel != '-'){
		
		for(var i=0;i<da.length;i++){
			if(da[i].alias == sel){
				
				$('#address').val(da[i].add);
				$('#postal').val(da[i].postal);
				$('#country').val(da[i].country);
				break;				
			}
		}
		
	}else{
		$('#address').val('');
		$('#postal').val('');
		$('#country').val('');
	}
}

var packing_id =<?=isset($packing_id)?$packing_id:0?>;
		
if(packing_id != 0){
	getSearchData(packing_id,2);
}

function checkUpload(){
	
	if(document.getElementById("confirm_file").files.length == 0){
		event.preventDefault();	
		alert('Please upload confirm document before approve');
	}
	
}

function checkDA(){
	
	var da = $('#delivery_address').val();

	if(da == '-' || da == null){
		event.preventDefault();	
		alert('Delivery address cant be blank');
	}	
}

</script>