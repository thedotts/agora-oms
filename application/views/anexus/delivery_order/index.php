
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-calendar fa-fw"></i> Delivery Orders
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input id="q" class="form-control" placeholder="Search" value="<?=$q?>" onblur="filter();">
                                </div>
                                <!-- /.form-group -->
                            </form>
                        </div>
                		<!-- /.col-lg-3 -->
                        <script>
							function filter(){
								var q = $("#q").val();
								if(q == '') {
									q = 'ALL';	
								}								
								location.href='<?=base_url('en/agora/'.$group_name.'/'.$model_name)?>/'+q;
							}
							</script>
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                				<?php if(in_array(8,$userdata['role_id']) || in_array(1,$userdata['role_id']) ){?>
                            	<a href="<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_add')?>"><button class="btn btn-default btn-sm" type="button">New <?=$common_name?></button></a><p></p>
                                <?php }?>
                    <div class="panel panel-default hidden-xs">
                            <div class="table-responsive">
                            	<?php
								if(!empty($results)) {
								?>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Delivery Order No.</th>                                            
                                            <th>Customer</th>
                                            <th>Last Updated</th>
                                            <th>Last Updated By</th>
                                            <th>Awaiting</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                   <tbody>
                                    	<?php
										foreach($results as $v) {
										?>
                                        <tr>
                                        	<td><?=$v['id']?></td>
                                            <td><?=$v['do_no']?></td>
                                            <td><a href="<?=base_url($init['langu'].'/agora/administrator/customers_view/'.$v['customer_id'])?>" target="_blank"><?=isset($customer_list[$v['customer_id']])?$customer_list[$v['customer_id']]:''?></a></td>
                                            <td><?=$v['modified_date'] != '0000-00-00 00:00:00'?date('d/m/Y',strtotime($v['modified_date'])):''?></td>
                                            <td><?=isset($user_list[$v['lastupdate_user_id']])?$user_list[$v['lastupdate_user_id']]:''?></td>
                                            <td>
                                            <?php
											if(!empty($v['awaiting_table'])){
                                            	foreach($v['awaiting_table'] as $x){
													if(isset($role_list[$x])){
													echo $role_list[$x].'<br>';
													}
												}
											}
											?>
                                            </td>
                                            <td><?=$v['status']<2?(isset($status_list[$v['status']])?$status_list[$v['status']]:''):$v['status_text']?></td>
                                            <td>                                            	
                                                <button  class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button" onclick="location.href='<?=base_url('en/agora/admin/delivery_order_edit/'.$v['id'])?>'"><i class="fa fa-edit"></i></button>
												<?php if(in_array(8,$userdata['role_id']) || in_array(1,$userdata['role_id'])){?>
                                                <button  class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="deleteData('<?=base_url('en/agora/admin/delivery_order_del/'.$v['id'])?>')"><i class="fa fa-times"></i></button>
                                                <?php }?>
                                             </td>
                                        </tr>
                                        <?php
										}
										?>
                                        
                                    </tbody>
                                </table>
                                 <?php
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
								?>
                      </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                    <!-- Data list (Mobile) -->
                        <div class="mobile-list visible-xs">
                        	<?php
								if(!empty($results)) {
								?>
                        	<table class="table table-striped table-bordered table-hover">
                            	<?php										
										foreach($results as $k=>$v) {
									?>
                            	<tr>
                                	<td>
                                    	<div class="row mobile-list-header">
                                            <div class="col-xs-12"><strong>#<?=($k+1)?></strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Customer:</strong></div>
                                            <div class="col-xs-8"><?=isset($customer_list[$v['customer_id']])?$customer_list[$v['customer_id']]:''?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Status:</strong></div>
                                            <div class="col-xs-8"><?=$v['status']<2?(isset($status_list[$v['status']])?$status_list[$v['status']]:''):$v['status_text']?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Action:</strong></div>
                                            <div class="col-xs-8"><button  class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button" onclick="location.href='<?=base_url('en/agora/admin/delivery_order_edit/'.$v['id'])?>'"><i class="fa fa-edit"></i></button>

                                                <button  class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="deleteData('<?=base_url('en/agora/admin/delivery_order_del/'.$v['id'])?>')"><i class="fa fa-times"></i></button></div>
                                        </div>
                                    </td>
                                </tr>
                                <?php											
										}
									?>
                            </table>
                            <?php
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
								?>
                                                       
                        </div>
                    
                    <?=$paging?>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
        <script>
		var alert_v = <?=$alert?>;
		
		switch(alert_v){
			case 1:
				alert('Record has been created successfully');
				break;
			case 2:
				alert('Record has been updated successfully');
				break;
			case 3:
				alert('Record has been deleted successfully');
				break;
		}
		
		function deleteData(url){
			var c = confirm("Are you sure you want to delete?");
			if(c){
				location.href=url;	
			}
		}
		
		//enter search
		$(document).ready(function() {
		  $(window).keydown(function(event){
			if(event.keyCode == 13) {
			  event.preventDefault();
			  q.blur();
			  return false;
			}
		  });
		});
		</script>