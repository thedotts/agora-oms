<div id="page-wrapper">
        	<form method="post" action="<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_submit')?>" onsubmit="return validate();">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-cog fa-fw"></i> System Settings
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Reminders Settings
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Hour of 1st Task List Reminder *</label>
                                    <input class="form-control" id="remind_1" name="remind_1" value="<?=$setting['reminded_1']?>">
                                    <span style="color:red" id="remind_1_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Hour of 2nd Task List Reminder *</label>
                                    <input class="form-control" id="remind_2" name="remind_2" value="<?=$setting['reminded_2']?>">
                                    <span style="color:red" id="remind_2_err"></span>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            General Settings
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>GST Percentage *</label>
                                    <input class="form-control" id="gst" name="gst" value="<?=$setting['gst_percentage']?>">
                                    <span style="color:red" id="gst_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Company Name *</label>
                                    <input class="form-control" id="company_name" name="company_name" value="<?=$setting['company_name']?>">
                                    <span style="color:red" id="company_name_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Company Address *</label>
                                    <input class="form-control" id="company_address" name="company_address" value="<?=$setting['company address']?>">
                                    <span style="color:red" id="company_address_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Company Postal Code *</label>
                                    <input class="form-control" id="postal" name="postal" value="<?=$setting['company postal']?>">
                                    <span style="color:red" id="postal_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Default Email *</label>
                                    <input class="form-control" id="email" name="email" value="<?=$setting['default_email']?>">
                                    <span style="color:red" id="email_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Main Contact Person *</label>
                                    <input class="form-control" id="main_contact_person" name="main_contact_person" value="<?=$setting['main_contact_person']?>">
                                    <span style="color:red" id="main_contact_person_err"></span>
                                </div>

                                <div class="form-group">
                                    <label>Main Contact No.</label>
                                    <input class="form-control" id="main_contact_no" name="main_contact_no" value="<?=$setting['main_contact_no']?>">
                                </div>
                                <div class="form-group">
                                    <label>Payment Information</label>
                                    <input class="form-control" id="payment_info" name="payment_info" value="<?=$setting['payment_information']?>">
                                </div>
                                <div class="form-group">
                                    <label>Order Terms</label>
                                    <input class="form-control" id="term" name="term" value="<?=$setting['purchase_order_terms']?>">
                                </div>
                                <div class="form-group">
                                    <label>Company Registration No.</label>
                                    <input class="form-control" id="reg_no" name="reg_no" value="<?=$setting['reg_no']?>">
                                </div>
                                <div class="form-group">
                                    <label>Default Website</label>
                                    <input class="form-control" id="d_web" name="d_web" value="<?=$setting['d_web']?>">
                                </div>


                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                    
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Pre-fixed for Document
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Document</th>
                                            <th>Pre-fixed Alphabet</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                Order
                                            </td>
                                            <td>
                                                <input class="form-control" value="<?=$prefix['order']?>" id="prefix_order" name="prefix_order">
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>
                                                Packing List
                                            </td>
                                            <td>
                                                <input class="form-control" value="<?=$prefix['packing_list']?>" id="prefix_pl" name="prefix_pl">
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>
                                                Delivery Order
                                            </td>
                                            <td>
                                                <input class="form-control" value="<?=$prefix['delivery_order']?>" id="prefix_do" name="prefix_do">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Invoice
                                            </td>
                                            <td>
                                                <input class="form-control" value="<?=$prefix['invoice']?>" id="prefix_invoice" name="prefix_invoice">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            
            
            <div class="row">
                <div class="col-lg-12">
                    
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->
        
        <script>
       function validate(){
			var error = 0;
			
			//remind_1
			if($("#remind_1").val() == '') {
				$('#remind_1_err').text('Hour of 1st Task List Reminder can\'t be blank');
				error = 1;
			}else{
				
				if(isNaN($("#remind_1").val())){
					$('#remind_1_err').text('Hour of 1st Task List Reminder needs to be integer only');
					error = 1;
				}else{
					$('#remind_1_err').text('');
				}
				
			}
			
			//remind_2
			if($("#remind_2").val() == '') {
				$('#remind_2_err').text('Hour of 2nd Task List Reminder can\'t be blank');
				error = 1;
			}else{
				
				if(isNaN($("#remind_2").val())){
					$('#remind_2_err').text('Hour of 2nd Task List Reminder needs to be integer only');
					error = 1;
				}else{
					$('#remind_2_err').text('');
				}
			}
			
			//gst
			if($("#gst").val() == '') {
				$('#gst_err').text('Gst Percentage can\'t be blank');
				error = 1;
			}else{
				
				if(isNaN($("#gst").val())){
					$('#gst_err').text('Gst Percentage needs to be integer only');
					error = 1;
				}else{
					$('#gst_err').text('');
				}
			}
			
			//company_name
			if($("#company_name").val() == '') {
				$('#company_name_err').text('Company Name can\'t be blank');
				error = 1;
			}else{
				$('#company_name_err').text('');
			}
			
			//company_address
			if($("#company_address").val() == '') {
				$('#company_address_err').text('Company Address can\'t be blank');
				error = 1;
			}else{
				$('#company_address_err').text('');
			}
			
			//postal
			if($("#postal").val() == '') {
				$('#postal_err').text('Company Postal Code can\'t be blank');
				error = 1;
			}else{
				
				if(isNaN($("#postal").val())){
					$('#postal_err').text('Company Postal Code needs to be integer only');
					error = 1;
				}else{
					$('#postal_err').text('');
				}
			}
			
			//email
			if($("#email").val() == '') {
				$('#email_err').text('Default Email can\'t be blank');
				error = 1;
			}else{
				$('#email_err').text('');
			}
			
			//main_contact_person
			if($("#main_contact_person").val() == '') {
				$('#main_contact_person_err').text('Main Contact Person can\'t be blank');
				error = 1;
			}else{
				$('#main_contact_person_err').text('');
			}
				
			
			if(error == 1){
				return false;
			}
		
		}
        </script>
        
        <script>
		$(function() {
		var alert_v = <?=$alert?>;
		
		switch(alert_v){
			case 1:
				alert('Record has been created successfully');
				break;
			case 2:
				alert('Record has been updated successfully');
				break;
			case 3:
				alert('Record has been deleted successfully');
				break;
		}
		});
		</script>