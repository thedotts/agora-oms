<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?=isset($webpage['web_title'])?$webpage['web_title']:"Anexus ERP System"?></title>

    <!-- Core CSS - Include with every page -->
    <link href="<?=base_url('assets/anexus/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/anexus/font-awesome/css/font-awesome.css')?>" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="<?=base_url('assets/anexus/css/plugins/morris/morris-0.4.3.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/anexus/css/plugins/timeline/timeline.css')?>" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="<?=base_url('assets/anexus/css/sb-admin.css')?>" rel="stylesheet">

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Forgot Password</h3>
                    </div>
                    <div class="panel-body">
                    	
                        <?php
                        	if($status != ''){
								
								if($status == 0){		
									echo '<div class="alert alert-danger alert-dismissable">Invalid email</div>';
								}else if($status == 1){
									echo '<div class="alert alert-success alert-dismissable">Email sent</div>';
								}
								
							}
						?>
                        
                        <form role="form" action="<?=base_url($init['langu'].'/reset_email')?>" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Email" name="email" type="email" value="">
                                </div>
                                
                                <!-- Change this to a button or input when using this as a form -->
                                <button class="btn btn-lg btn-success btn-block">Send Email</button>
                                <button class="btn btn-lg btn-primary btn-block" onClick="history.go(-1); return false;">Back</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Core Scripts - Include with every page -->
    <script src="<?=base_url('assets/anexus/js/jquery-1.10.2.js')?>"></script>
    <script src="<?=base_url('assets/anexus/js/bootstrap.min.js')?>"></script>
    <script src="<?=base_url('assets/anexus/js/plugins/metisMenu/jquery.metisMenu.js')?>"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="<?=base_url('assets/anexus/js/sb-admin.js')?>"></script>

</body>

</html>
