<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Pac-Fung Agora Portal</title>

    <!-- Core CSS - Include with every page -->
    <link href="<?=base_url('assets/anexus/css/bootstrap-2.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/anexus/font-awesome/css/font-awesome.css')?>" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="<?=base_url('assets/anexus/css/plugins/morris/morris-0.4.3.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/anexus/css/plugins/timeline/timeline.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/anexus/css/simple-sidebar.css')?>" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="<?=base_url('assets/anexus/css/sb-admin.css')?>" rel="stylesheet">
    
    <script src="<?=base_url('assets/anexus/js/jquery-1.10.2.js')?>"></script>
    <script src="<?=base_url('assets/js/libs/jquery-ui-1.10.0.custom.min.js')?>"></script>
    
    <link href="<?=base_url('assets/css/ui-lightness/jquery-ui-1.10.0.custom.min.css')?>" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href=""><img src="<?=base_url('assets/img/agora-logo.png')?>" style="width: 135px;" /> </a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
            	<?php if(in_array(7,$this->data['userdata']['role_id'])){?>
            	<li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-shopping-cart fa-fw"></i> Cart <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages" id="cart">
                    <?php
						$data = $this->input->cookie('Cart', TRUE);
						if(!empty($data)){
							$data = json_decode($data,true);
						}else{
							$data = array();
						}
					?>
                    <?php 
					if(!empty($data)){
						foreach($data as $k =>$v){
					?>
                        <li>
                            <a href="#">
                                <div>
                                    <strong><?=$v['name']?></strong>
                                    <span class="pull-right text-muted">
                                        <em>Qty: <?=$v['qty']?></em>
                                    </span>
                                    <div><?=$v['model_name']?></div>
                                </div>
                            </a>
                        </li>
                    <?php 
						}
					}else{
					?>
                    	<li>
                            <a class="text-center" href="">
                                <strong>Empty</strong>
                            </a>
                        </li>
                    <?php }?>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="<?=base_url($init['langu'].'/agora/customer/cart')?>">
                                <strong>Shopping Cart</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <?php }?>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <?=$userdata['name']?>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?=base_url($init['langu'].'/agora/profile')?>"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?=base_url($init['langu'].'/logout')?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default navbar-static-side" role="navigation">
              <div id="sidebar-wrapper" >
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                    	<?php if(!in_array(7,$this->data['userdata']['role_id'])){?>
                        <li>
                            <a href="<?=base_url($init['langu'].'/agora/tasklist/ALL')?>"><i class="fa fa-tasks fa-fw"></i> Task List (<?=$task_display_count?>)</a>
                        </li>
                        <?php }?>
                        
                        <?php if(in_array(100,$this->data['userdata']['role_id'])){?>
                        <li>
                            <a href="<?=base_url($init['langu'].'/agora/customer/promotion')?>"><i class="fa fa-shopping-cart fa-fw"></i> Customer</a>
                        </li>
                        <?php }?>
                        
                        <?php if(in_array(3,$this->data['userdata']['role_id'])){?>
                        <li>
                            <a href="#"><i class="fa fa-briefcase fa-fw"></i> Sales<span class="fa arrow"></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/sales/order_add')?>">New Order</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/sales/order/ALL/1')?>">Orders</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/customer/promotion')?>">Promotions</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/administrator/customers/ALL/1')?>">Customers</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/manager/reports')?>">Reports</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php }?>
                        
                        <?php if(in_array(4,$this->data['userdata']['role_id'])){?>
                        <li>
                            <a href="#"><i class="fa fa-truck fa-fw"></i> Ground Staff<span class="fa arrow"></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/sales/order')?>">Orders</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/ground_staff/packing_list/ALL/1')?>">Packing List</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/administrator/unpack_list')?>">Missing Packing List</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php }?>
                        
                        <?php if(in_array(5,$this->data['userdata']['role_id'])){?>
                        <li>
                            <a href="#"><i class="fa fa-calendar fa-fw"></i> Admin<span class="fa arrow"></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/sales/order')?>">Orders</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/ground_staff/packing_list')?>">Packing List</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/admin/delivery_order/ALL/1')?>">Delivery Orders</a>
                                </li>
                                <li>
                                	<a href="<?=base_url($init['langu'].'/agora/administrator/unpack_list')?>">Missing Packing List</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php }?>
                        
                        <?php if(in_array(6,$this->data['userdata']['role_id'])){?>
                        <li>
                            <a href="#"><i class="fa fa-dollar fa-fw"></i> Finance<span class="fa arrow"></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/finance/invoice/ALL/1')?>">Invoices</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php }?>
                        
                        <?php if(in_array(2,$this->data['userdata']['role_id'])){?>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Sales Manager<span class="fa arrow"></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/manager/reports')?>">Reports</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/manager/assign_customer')?>">Assign Customers</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php }?>
                        
                        
                        <?php if(in_array(8,$this->data['userdata']['role_id'])){?>
                        <li>
                            <a href="#"><i class="fa fa-calendar fa-fw"></i> OPS Manager<span class="fa arrow"></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/sales/order')?>">Orders</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/ground_staff/packing_list')?>">Packing List</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/admin/delivery_order/ALL/1')?>">Delivery Orders</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/administrator/products')?>">Products / Inventory</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/administrator/unpack_list')?>">Missing Packing List</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php }?>
                        
                        <?php if(in_array(1,$this->data['userdata']['role_id'])){?>
                        <li>
                            <a href="#"><i class="fa fa-pencil-square-o fa-fw"></i> Super Admin<span class="fa arrow"></a>
                            <ul class="nav nav-second-level">
                            	<li>
                                    <a href="<?=base_url($init['langu'].'/agora/sales/order')?>">Orders</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/ground_staff/packing_list')?>">Packing List</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/admin/delivery_order/ALL/1')?>">Delivery Orders</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/administrator/employee/ALL/1')?>">Employees</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/administrator/customers/ALL/1')?>">Customers</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/administrator/product_category/ALL/1')?>">Product Category</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/administrator/products/ALL/1')?>">Products / Inventory</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/administrator/promotion')?>">Promotions</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/administrator/audit_log')?>">Audit Log</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/administrator/export_data')?>">Export Data</a>
                                </li>
                                <li>
                                    <a href="<?=base_url($init['langu'].'/agora/administrator/settings')?>">System Settings</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php }?>
                        
                        <?php if((!in_array(3,$this->data['userdata']['role_id']) && !in_array(7,$this->data['userdata']['role_id'])) || in_array(1,$this->data['userdata']['role_id']) || in_array(2,$this->data['userdata']['role_id'])){?>
                        <li>
                            <a href="<?=base_url($init['langu'].'/agora/jobs')?>"><i class="fa fa-folder-open fa-fw"></i> All Orders</a>
                        </li>
                        <?php }?>
                        
                        <?php if((!in_array(3,$this->data['userdata']['role_id']) && !in_array(7,$this->data['userdata']['role_id'])) || in_array(1,$this->data['userdata']['role_id']) || in_array(2,$this->data['userdata']['role_id']) ){?>
                        <li>
                            <a href="<?=base_url($init['langu'].'/agora/administrator/customers_all')?>"><i class="fa fa-user fa-fw"></i> Customers</a>
                        </li>
                        <?php }?>
                        
                        <?php if(in_array(7,$this->data['userdata']['role_id'])){?>
                        <li>
                            <a href="<?=base_url($init['langu'].'/agora/customer/new_order')?>"><i class="fa fa-tasks fa-fw"></i> New Order</a>
                        </li>
                        <li>
                            <a href="<?=base_url($init['langu'].'/agora/customer/pending_order/ALL/1')?>"><i class="fa fa-tasks fa-fw"></i> Pending Orders</a>
                        </li>
                        <li>
                            <a href="<?=base_url($init['langu'].'/agora/customer/all_order/ALL/1')?>"><i class="fa fa-briefcase fa-fw"></i> All Orders</a>
                        </li>
                        <li>
                            <a href="<?=base_url($init['langu'].'/agora/customer/catalog')?>"><i class="fa fa-shopping-cart fa-fw"></i> Catalog</a>
                        </li>
                        <li>
                            <a href="<?=base_url($init['langu'].'/agora/customer/promotion')?>"><i class="fa fa-star fa-fw"></i> Promotions</a>
                        </li>
                        <?php }?>
                        
                    </ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
          </span>
            <!-- /.navbar-static-side -->
        </nav>
