

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-wrench fa-fw"></i> Field Service Order
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>All</option>
                                        <option>Show Only</option>
                                        <option>Hide Only</option>
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </form>
                        </div>
                		<!-- /.col-lg-3 -->
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input class="form-control" placeholder="Search">
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </form>
                        </div>
                		<!-- /.col-lg-3 -->
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                            	<a href="create-field-service-order.html"><button class="btn btn-default btn-sm" type="button">Create Field Service Order</button></a><p></p>
                    <div class="panel panel-default">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>SR No.</th>
                                            <th>Job ID</th>
                                            <th>Customer</th>
                                            <th>Last Updated</th>
                                            <th>Last Updated By</th>
                                            <th>Awaiting</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>SR-00002</td>
                                            <td>JN-00007</td>
                                            <td>STU Company</td>
                                            <td>09/07/14</td>
                                            <td>Ms Log</td>
                                            <td>Logistics</td>
                                            <td>Pending Invoice (& D/O)</td>
                                            <td>
                                                <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View" type="button"><i class="fa fa-folder-open"></i></button></td>
                                        </tr>
                                    </tbody>
                                </table>
                      </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    
