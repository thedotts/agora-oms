

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-wrench fa-fw"></i> Inventory Request
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input class="form-control" placeholder="Search">
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </form>
                        </div>
                		<!-- /.col-lg-3 -->
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                            	<a href="create-inventory-request.html"><button class="btn btn-default btn-sm" type="button">Create Inventory Request</button></a><p></p>
                    <div class="panel panel-default">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Serial No.</th>
                                            <th>Customer</th>
                                            <th>No. of Items</th>
                                            <th>Requested By</th>
                                            <th>Requested Date</th>
                                            <th>Status</th>
                                            <th width="80px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td><a href="#">IR-00001</a></td>
                                            <td>JKL Company</td>
                                            <td>4</td>
                                            <td>Mr Service</td>
                                            <td>20/07/14</td>
                                            <td>Completed</td>
                                            <td>
                                                <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button"><i class="fa fa-edit"></i></button>
                                                <button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button"><i class="fa fa-times"></i></button>
                                             </td>
                                        </tr>
                                    </tbody>
                                </table>
                      </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    
