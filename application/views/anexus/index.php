

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-tasks fa-fw"></i> Task List
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                    <select class="form-control" onchange="filter(this.value)">
                                        <option value="ALL" <?=$display=='ALL'?'selected="selected"':''?>>All</option>
                                        <option value="1" <?=$display=='1'?'selected="selected"':''?>>Show Only</option>
                                        <option value="0" <?=$display=='0'?'selected="selected"':''?>>Hide Only</option>
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </form>
                            <script>
							function filter(val){
								location.href='<?=base_url('en/agora/tasklist')?>/'+val;
							}
							</script>
                        </div>
                		<!-- /.col-lg-3 -->
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default hidden-xs">
                            <div class="table-responsive">
                            	<?php
								if(!empty($task_list)) {
								?>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Order No.</th>
                                            <th>Customer</th>
                                            <th>Update by</th>
                                            <th>Update Date/Time</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php										
										foreach($task_list as $k=>$v) {
									?>
                                        <tr>
                                        <td><?=($k+1)?></td>
                                            <td><a href="<?=base_url($init['langu'].'/agora/'.$v['type_path'].$v['type_id'])?>"><?=$v['serial']?></a></td>
                                            <td><a href="<?=base_url($init['langu'].'/agora/administrator/customers_view/'.$v['customer_id'])?>"><?=isset($customer_list[$v['customer_id']])?$customer_list[$v['customer_id']]:''?></a></td>
                                            <td><?=$user_list[$v['user_id']]?></td>
                                            <td><?=date('d/m/y H:i',substr(strtotime($v['created_date']),0,10))?></td>
                                            
                                            <td><?=$v['status_text']?></td>
                                            <td><a href="javascript:;" data-id="<?=$v['id']?>" class="showHidebtn"><?=$v['display']==0?'Show':'Hide'?></a></td>
                                        </tr>
                                    <?php											
										}
									?>                                        
                                    </tbody>
                                </table>
                                <?php
								} else {
								?>
                                <p>No Task has been assigned to you</p>
                                <?php	
								}
								?>
                      </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                    
                    <!-- Data list (Mobile) -->
                        <div class="mobile-list visible-xs">
                        	<?php
								if(!empty($task_list)) {
								?>
                        	<table class="table table-striped table-bordered table-hover">
                            	<?php										
										foreach($task_list as $k=>$v) {
									?>
                            	<tr>
                                	<td>
                                    	<div class="row mobile-list-header">
                                            <div class="col-xs-12"><strong>#<?=($k+1)?></strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Customer:</strong></div>
                                            <div class="col-xs-8"><a href="<?=base_url($init['langu'].'/agora/'.$v['type_path'].$v['type_id'])?>"><?=$v['serial']?></a></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Status:</strong></div>
                                            <div class="col-xs-8"><?=$v['status_text']?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Action:</strong></div>
                                            <div class="col-xs-8"><a href="javascript:;" data-id="<?=$v['id']?>" class="showHidebtn"><?=$v['display']==0?'Show':'Hide'?></a></div>
                                        </div>
                                    </td>
                                </tr>
                                <?php											
										}
									?>
                            </table>
                            <?php
								} else {
								?>
                                <p>No Task has been assigned to you</p>
                                <?php	
								}
								?>
                                                       
                        </div>
                    
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

<script>
var alert_v = <?=$alert?>;
		
		switch(alert_v){
			case 1:
				alert('Record has been created successfully');
				break;
			case 2:
				alert('Record has been updated successfully');
				break;
			case 3:
				alert('Record has been deleted successfully');
				break;
		}

var display_mode = '<?=$display?>';

$(".showHidebtn").click(function(e) {
	var task_id = $(this).data("id");
	var btn = $(this);
	var display = $(this).html()=='Hide'?0:1;
    $.ajax({
	  method: "POST",
	  url: "<?=base_url('en/agora/task_update')?>",
	  dataType: "json",
	  data: {id: task_id, display: display}
	}).done(function(r) {
	  	btn.html(r.result);
		if(display_mode != 'ALL'){
			location.reload();
		}
	});
});
</script>
    
