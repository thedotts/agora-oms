<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-shopping-cart fa-fw"></i> Shopping Cart
                       <div class="col-lg-3 pull-right">
                            
                        </div>
                        <!-- /.col-lg-3 -->
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                <form action="<?=base_url($init['langu'].'/agora/customer/cart_submit');?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="mode" value="Add"/>
                
                	 <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Delivery Address :
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
							<select class="form-control" id="alias" name="alias" onchange="alias_detail(this.value)">
                                </select>
                                <input type="hidden" id="alias_name" name="alias_name" />
                                <input type="hidden" id="address" name="address" />
                                <input type="hidden" id="postal" name="postal" />
                                <input type="hidden" id="country" name="country" />
                            </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                	
                    <div class="panel panel-default">
                    	
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="table">
                                	<?php
										$count = 1;
                                    	if(!empty($cart)){
											$total = 0;
											$gst_amount = 0;
											$nett = 0;
									?>
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Product Name</th>
                                            <th>Model Name</th>
                                            <th width="200px">Quantity</th>
                                            <th width="200px">Unit Price</th>   
                                            <th width="200px">Price</th>   
                                            <th width="80px">Action</th>
                                        </tr>
                                    </thead>
                                    <?php foreach($cart as $k => $v){?>
                                    <tbody>
                                        <tr id="row<?=$count?>">
                                            <td><?=$count?></td>
                                            <td><?=$v['name']?></td>
                                            <td><?=$v['model_name']?></td>   
                                            <td>
                                            	<input id="p_id<?=$count?>" name="p_id[]" type="hidden" class="form-control" value="<?=$k?>">
                                            	<input id="p_name" name="p_name[]" type="hidden" class="form-control" value="<?=$v['name']?>">
                                                <input id="model_name" name="model_name[]" type="hidden" class="form-control" value="<?=$v['model_name']?>">
                                                <input onblur="alert_qty()" id="qty<?=$count?>" name="qty[]" type="text" class="form-control" value="<?=$v['qty']?>">
                                            </td>
                                            <td>
                                                <div class="form-group input-group">
                                                    <span class="input-group-addon"><i class="fa fa-usd"></i>
                                                    </span>
                                                    <input id="price" name="price[]" type="text" class="form-control" value="<?=$v['price']?>" readonly>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group input-group">
                                                    <span class="input-group-addon"><i class="fa fa-usd"></i>
                                                    </span>
                                                    <input id="total_price" name="total_price[]" type="text" class="form-control" value="<?=number_format($v['qty']*$v['price'],2)?>" readonly>
                                                </div>
                                            </td>
                                            <td>
                                                <button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Remove" type="button" onclick="delRow(<?=$count?>)"><i class="fa fa-times"></i></button>
                                             </td>
                                        </tr>
                                        <?php 
										$total+=$v['qty']*$v['price'];
										$count++;
										}
										$gst_amount = ($total*$gst['value'])/100;
										$nett = $total + $gst_amount;
										?>
                                        <tr>
                                            <td colspan="5" style="text-align: right;">Total Amount</td>
                                            <td colspan="2">$<span id="total"><?=number_format($total,2)?></span><input id="total" name="total" type="hidden" value="<?=$total?>"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" style="text-align: right;">GST Amount</td>
                                            <td colspan="2">$<span id="gst"><?=number_format($gst_amount,2)?></span><input id="gst_amount" name="gst_amount" type="hidden" value="<?=$gst_amount?>"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" style="text-align: right;">Nett Amount</td>
                                            <td colspan="2">$<span id="nett"><?=number_format($nett,2)?></span><input id="nett" name="nett" type="hidden" value="<?=$nett?>"></td>
                                        </tr>
                                        <?php }else{?>
                                        <tr>
                                            <td>Empty</td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                      </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                    
					<?php if(!empty($cart)){?>
                    <button type="submit" class="btn btn-primary" id="submit_btn">Checkout</button>
                    <button type="button" class="btn btn-default" onclick="updateCart()">Update Cart</button>
                    <?php } ?>
                    
                    </form>                    
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

<script>
var gst = <?=$gst['value']?>;
var item_count = <?=$count?>;
var check_qty = 0;

function delRow(id){
	console.log(check_qty);
	var status = 1;
	
	if(check_qty == 1){
	
		var c = confirm("You will lost your key in quantity,if you not update cart before delete item!Are you sure?");
		if(!c){
			status = 0;
		}
	
	}
	
	if(status == 1){
	check_qty = 0;
	$('#row'+id).hide();
	
	var p_id = $('#p_id'+id).val();
	
	$.ajax({
	  method: "POST",
	  url: "<?=base_url($init['langu'].'/agora/customer/cart_del')?>",
	  data: {"p_id": p_id },
	  dataType: "JSON"
	})
	.done(function(r) {
		
		if(r.length != 0){
			
		html='<thead><tr><th>#</th><th>Product Name</th><th>Model Name</th><th width="200px">Quantity</th><th width="200px">Unit Price</th><th width="200px">Price</th><th width="80px">Action</th></tr></thead><tbody>';
		
		var total= 0;
		var gst_amount = 0;
		var nett = 0;
		
		for(var i=0;i<r.length;i++){
			total += r[i].price*r[i].qty;
			
			html+='<tr id="row'+(i+1)+'"><td>'+(i+1)+'</td><td>'+r[i].name+'</td><td>'+r[i].model_name+'</td><td><input id="p_id'+(i+1)+'" name="p_id[]" type="hidden" class="form-control" value="'+r[i].id+'"><input id="p_name" name="p_name[]" type="hidden" class="form-control" value="'+r[i].name+'"><input id="model_name" name="model_name[]" type="hidden" class="form-control" value="'+r[i].model_name+'"><input id="qty'+(i+1)+'" name="qty[]" type="text" class="form-control" onblur="alert_qty()" value="'+r[i].qty+'"></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input id="price" name="price[]" type="text" class="form-control" value="'+r[i].price+'" readonly></div></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input id="total_price" name="total_price[]" type="text" class="form-control" value="'+formatNumber(r[i].price*r[i].qty)+'" readonly></div></td><td><button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Remove" type="button" onclick="delRow('+(i+1)+')"><i class="fa fa-times"></i></button></td></tr>';
		}
		gst_amount = (total*gst)/100;
		nett = total+gst_amount;
		
		html+='</tbody>';
		
		html+='<tr><td colspan="5" style="text-align: right;">Total Amount</td><td colspan="2">$<span id="total">'+formatNumber(total)+'</span><input id="total" name="total" type="hidden" value="'+formatNumber(total)+'"></td></tr><tr><td colspan="5" style="text-align: right;">GST Amount</td><td colspan="2">$<span id="gst">'+formatNumber(gst_amount)+'</span><input id="total" name="gst_amount" type="hidden" value="'+formatNumber(gst_amount)+'"></td></tr><tr><td colspan="5" style="text-align: right;">Nett Amount</td><td colspan="2">$<span id="nett">'+formatNumber(nett)+'</span><input id="total" name="nett" type="hidden" value="'+formatNumber(nett)+'"></td></tr>';
		
		$('#table').html('');
		$('#table').append(html);
		
		//cart
		$('#cart').html('');
			
		var html = '';
		for(var x=0;x<r.length;x++){
			html +='<li><a href="#"><div><strong>'+r[x].name+'</strong><span class="pull-right text-muted"><em>Qty: '+r[x].qty+'</em></span><div>'+r[x].model_name+'</div></div></a></li>';
		}
			
		html+='<li class="divider"></li><li><a class="text-center" href="'+'<?=base_url($init['langu'].'/agora/customer/cart')?>'+'"><strong>Shopping Cart</strong><i class="fa fa-angle-right"></i></a></li>';
			
		$('#cart').append(html);
		
		}else{
		
		html ='<tr><td>Empty</td></tr>';
		
		$('#table').html('');
		$('#table').append(html);
		
		//cart
		//cart
		$('#cart').html('');
		var html ='<li><a class="text-center" href="cart.html"><strong>Empty</strong></a></li><li class="divider"></li><li><a class="text-center" href="'+'<?=base_url($init['langu'].'/agora/customer/cart')?>'+'"><strong>Shopping Cart</strong><i class="fa fa-angle-right"></i></a></li>';
		$('#cart').append(html);
		
		}
		
	});
	
	}
	
}

function formatNumber(number)
{	
	var number = parseFloat(number);
    number = number.toFixed(2) + '';
    var x = number.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function updateCart(){
	var tmp = [];
	
	for(var i=1;i<item_count;i++){
		id = $('#p_id'+i).val();
		qty = $('#qty'+i).val();
		data = {"id":id,"qty":qty};
		tmp.push(data);
	}
	
	$.ajax({
	  method: "POST",
	  url: "<?=base_url($init['langu'].'/agora/customer/cart_update')?>",
	  data: {"data": JSON.stringify(tmp)},
	  dataType: "JSON"
	})
	.done(function(r) {
		
		if(r.length != 0){
			
		html='<thead><tr><th>#</th><th>Product Name</th><th>Model Name</th><th width="200px">Quantity</th><th width="200px">Unit Price</th><th width="200px">Price</th><th width="80px">Action</th></tr></thead><tbody>';
		
		var total= 0;
		var gst_amount = 0;
		var nett = 0;
		console.log(r);
		for(var i=0;i<r.length;i++){
			total += r[i].price*r[i].qty;
			
			html+='<tr id="row'+(i+1)+'"><td>'+(i+1)+'</td><td>'+r[i].name+'</td><td>'+r[i].model_name+'</td><td><input id="p_id'+(i+1)+'" name="p_id[]" type="hidden" class="form-control" value="'+r[i].id+'"><input id="p_name" name="p_name[]" type="hidden" class="form-control" value="'+r[i].name+'"><input id="model_name" name="model_name[]" type="hidden" class="form-control" value="'+r[i].model_name+'"><input id="qty'+(i+1)+'" name="qty[]" type="text" class="form-control" onblur="alert_qty()" value="'+r[i].qty+'"></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input id="price" name="price[]" type="text" class="form-control" value="'+r[i].price+'" readonly></div></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input id="total_price" name="total_price[]" type="text" class="form-control" value="'+formatNumber(r[i].price*r[i].qty)+'" readonly></div></td><td><button class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Remove" type="button" onclick="delRow('+(i+1)+')"><i class="fa fa-times"></i></button></td></tr>';
		}
		gst_amount = (total*gst)/100;
		nett = total+gst_amount;
		
		html+='</tbody>';
		
		html+='<tr><td colspan="5" style="text-align: right;">Total Amount</td><td colspan="2">$<span id="total">'+formatNumber(total)+'</span><input id="total" name="total" type="hidden" value="'+formatNumber(total)+'"></td></tr><tr><td colspan="5" style="text-align: right;">GST Amount</td><td colspan="2">$<span id="gst">'+formatNumber(gst_amount)+'</span><input id="total" name="gst_amount" type="hidden" value="'+formatNumber(gst_amount)+'"></td></tr><tr><td colspan="5" style="text-align: right;">Nett Amount</td><td colspan="2">$<span id="nett">'+formatNumber(nett)+'</span><input id="total" name="nett" type="hidden" value="'+formatNumber(nett)+'"></td></tr>';
		
		$('#table').html('');
		$('#table').append(html);
		
		$('#cart').html('');
			
		var html = '';
		for(var x=0;x<r.length;x++){
			html +='<li><a href="#"><div><strong>'+r[x].name+'</strong><span class="pull-right text-muted"><em>Qty: '+r[x].qty+'</em></span><div>'+r[x].model_name+'</div></div></a></li>';
		}
			
		html+='<li class="divider"></li><li><a class="text-center" href="'+'<?=base_url($init['langu'].'/agora/customer/cart')?>'+'"><strong>Shopping Cart</strong><i class="fa fa-angle-right"></i></a></li>';
			
		$('#cart').append(html);
		
		
		}else{
		
		html ='<tr><td>Empty</td></tr>';
		
		$('#table').html('');
		$('#table').append(html);
		
		}
		check_qty = 0;
	});
	
}

function alert_qty(){
	check_qty = 1;
}

var alias_json = <?=$alias?>;
var opt_alias = '<option>-</option>';
if(alias_json.length > 0){
	for(i=0;i<alias_json.length;i++){
		opt_alias += '<option value="'+i+'">'+alias_json[i].alias+'</option>';
	}
	
	$('#alias').append(opt_alias);
}

function alias_detail(sel){
	if(sel != '-'){
		$('#alias_name').val(alias_json[sel].alias);
		$('#address').val(alias_json[sel].add);
		$('#postal').val(alias_json[sel].postal);
		$('#country').val(alias_json[sel].country);	
	}else{
		$('#alias_name').val('');
		$('#address').val('');
		$('#postal').val('');
		$('#country').val('');	
	}
}

$('#submit_btn').click(function(event){

	var alias_val = $('#alias').val();
	if(alias_val == '-'){
		event.preventDefault();
		alert('Please select delivery address before submit');
	}

});

</script>