<div id="page-wrapper">
        	<form method="post" action="<?=base_url('en/anexus/'.$group_name.'/'.$model_name.'_submit')?>" onsubmit="return validate();">
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-pencil-square-o fa-fw"></i> Create Service
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Service Information
                        </div>
                        <div class="panel-body">
                        		<div class="form-group">
                                    <label>Service Name</label>
                                    <input class="form-control" name="model_no" value="<?=$mode=='Edit'?$result['model_no']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Service Item No.</label>
                                    <input class="form-control" name="serial" value="<?=$mode=='Edit'?$result['serial']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Supplier</label>
                                    <select name="supplier" class="form-control">
                                    	<option value="<?=$anexus['id']?>"><?=$anexus['company_name']?></option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Status *</label>
                                    <select name="status" class="form-control">
                                        <?php
										foreach($status_list as $k=>$v) {
										?>
                                        <option value="<?=$k?>" <?=$mode=='Edit'&&$k==$result['status']?'selected="selected"':''?>><?=$v?></option>
                                        <?php
										}
										?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description"><?=$mode=='Edit'?$result['description']:''?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>UOM</label>
                                    <select name="uom" class="form-control">
                                        <?php
										foreach($uom_list as $k=>$v) {
										?>
                                        <option value="<?=$k?>" <?=$mode=='Edit'&&$k==$result['uom']?'selected="selected"':''?>><?=$v?></option>
                                        <?php
										}
										?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Unit Price</label>
                                    <input class="form-control" name="unit_price" value="<?=$mode=='Edit'?$result['unit_price']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Additional Fee</label>
                                    <input class="form-control" name="additional_fee" value="<?=$mode=='Edit'?$result['additional_fee']:''?>">
                                    <p class="help-block"><i>Cost added as a one-time fee</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Currency *</label>
                                    <select name="currency" class="form-control">
                                    	<?php
										foreach($currency_list as $k=>$v) {
										?>
                                        <option value="<?=$k?>" <?=$mode=='Edit'&&$k==$result['currency']?'selected="selected"':''?>><?=$v?></option>
                                        <?php	
										}
										?>                                                                                
                                    </select>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
            </div>
            <!-- /.row -->


            
            
            <div class="row">
                <div class="col-lg-12">
                    
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->

<script>

$(document).ready(function(e) {
	//$("#dob").datepicker({"dateFormat":"dd/mm/yy"}); 
	//$("#expired_date").datepicker({"dateFormat":"dd/mm/yy"});    
});



function validate(){
		
	
	
}
</script>