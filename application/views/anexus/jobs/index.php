

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-folder-open fa-fw"></i> All Orders
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input id="q" class="form-control" placeholder="Search" value="<?=$q?>" onblur="filter();">
                                </div>
                                <!-- /.form-group -->
                            </form>
                        </div>
                		<!-- /.col-lg-3 -->
                        <script>
							function filter(){
								var q = $("#q").val();
								if(q == '') {
									q = 'ALL';	
								}								
								location.href='<?=base_url('en/agora/jobs')?>/'+q;
							}
							</script>
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    
                    <div class="panel panel-default hidden-xs">
                            <div class="table-responsive">
                            	<?php
								if(!empty($results)) {
								?>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Order No.</th>
                                            <th>Customer</th>
                                            <th>Status</th>
                                            
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
										$i=1;
										foreach($results as $v) {
										?>
                                        <tr>
                                            <td><?=$i?></td>
                                            <td><?=$v['order_no']?></td>
                                            <td><?=isset($customer_list[$v['customer_id']])?$customer_list[$v['customer_id']]:''?></td>
                                            <td><?=$v['status']<2?(isset($status_list[$v['status']])?$status_list[$v['status']]:''):$v['status_text']?></td>
                                            
                                            <td>
                                                <!--<a href="<?=base_url($init['langu'].'/agora/'.$v['type_path'].$v['type_id'])?>"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View" type="button"><i class="fa fa-folder-open"></i></button></a>-->
                                                
                                                <a href="<?=base_url($init['langu'].'/agora/job_detail/'.$v['id'])?>"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View" type="button"><i class="fa fa-folder-open"></i></button></a>
                                                </td>
                                        </tr>
                                        <?php
											$i++;
										}
										?>                                        
                                    </tbody>
                                </table>
                                <?php
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
								?>
                      </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                    
                    <!-- Data list (Mobile) -->
                        <div class="mobile-list visible-xs">
                        	<?php
								if(!empty($task_list)) {
								?>
                        	<table class="table table-striped table-bordered table-hover">
                            	<?php										
										foreach($task_list as $k=>$v) {
									?>
                            	<tr>
                                	<td>
                                    	<div class="row mobile-list-header">
                                            <div class="col-xs-12"><strong>#<?=($k+1)?></strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Customer:</strong></div>
                                            <div class="col-xs-8"><?=isset($customer_list[$v['customer_id']])?$customer_list[$v['customer_id']]:''?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Status:</strong></div>
                                            <div class="col-xs-8"><?=$v['status_text']?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Action:</strong></div>
                                            <div class="col-xs-8"><a href="<?=base_url($init['langu'].'/agora/job_detail/'.$v['id'])?>"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View" type="button"><i class="fa fa-folder-open"></i></button></a></div>
                                        </div>
                                    </td>
                                </tr>
                                <?php											
										}
									?>
                            </table>
                            <?php
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
								?>
                                                       
                        </div>
                    
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <script>
            //enter search
			$(document).ready(function() {
			  $(window).keydown(function(event){
				if(event.keyCode == 13) {
				  event.preventDefault();
				  q.blur();
				  return false;
				}
			  });
			});
            </script>
        
