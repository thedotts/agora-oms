<div id="page-wrapper">

            <div class="row">

                <div class="col-lg-12">

                    <h1 class="page-header"><i class="fa fa-folder-open fa-fw"></i> Order Details</h1>

                    <ul class="breadcrumb">

                    	<li><a href="<?=base_url('en/agora/customers')?>">Customers</a></li>

                    	<li><a href="<?=base_url('en/agora/administrator/customers_view/'.$customer['id'])?>"><?=$customer['company_name']?></a></li>

                    	<li><?=$result['order_no']?></li>

                    </ul>

                </div>

                <!-- /.col-lg-12 -->

            </div>

            <!-- /.row -->

            <div class="row">

                <div class="col-lg-12">

                

                    <div class="panel panel-default">

                            <div class="table-responsive">

                                <table class="table table-striped table-bordered table-hover">

                                    <tbody>

                                        <tr>

                                            <th width="20%">Company Name</th>

                                            <td width="30%"><?=$customer['company_name']?></td>

                                            <td></td>

                                            <th width="20%">Customer Code</th>

                                            <td width="30%"><?=$customer['custom_code']?></td>

                                        </tr>

                                        <tr>

                                            <th>Address</th>

                                            <td><?=$customer['address']?></td>

                                            <td></td>

                                            <th>Postal</th>

                                            <td><?=$customer['postal']?></td>

                                        </tr>

                                        <tr>

                                            <th>Country</th>

                                            <td><?=$customer['country']?></td>

                                            <td></td>

                                            <th>Email</th>

                                            <td><?=$customer['primary_contact_email']?></td>

                                        </tr>

                                        <tr>

                                            <th>Contact Person</th>

                                            <td><?=$customer['primary_attention_to']?></td>

                                            <td></td>

                                            <th>Contact</th>

                                            <td><?=$customer['primary_contact_info']?></td>

                                        </tr>

                                    </tbody>

                                </table>

                            </div>

                            <!-- /.table-responsive -->

                    </div>

                    <!-- /.panel -->

                    

                    <div class="panel panel-primary">

                    	<div class="panel-heading">

                            Order Information

                        </div>

                        <div class="panel-body">

                                <div class="table-responsive">

                                <table class="table table-striped table-bordered table-hover">

                                    <tbody>

                                        <tr>

                                            <th width="20%">Order No.</th>

                                            <td width="30%"><?=$result['order_no']?></td>

                                            <td></td>

                                            <th width="20%">Status</th>

                                            <td width="30%"><?=$result['status']<2?(isset($status_list[$result['status']])?$status_list[$result['status']]:''):$result['status_text']?></td>

                                        </tr>

                                        <tr>

                                            <th>Sales Person</th>

                                            <td><?=$user_list[$result['requestor_id']]?></td>

                                            <td></td>

                                            <th>Date of Order</th>

                                            <td><?=date("d/m/Y", strtotime(substr($result['created_date'],0,10)))?></td>

                                        </tr>

                                    </tbody>

                                </table>

                            </div>

                            <!-- /.table-responsive -->

                         </div>

                		 <!-- /.panel-body -->

                    </div>

                    <!-- /.panel -->

                

                    <div class="panel panel-primary">

                        <div class="panel-heading">

                            Job Information

                        </div>

                        <!-- /.panel-heading -->

                        <div class="panel-body">

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#order" data-toggle="tab">Orders</a>
                                </li>
                                <li><a href="#packinglist" data-toggle="tab">Packing List</a>
                                </li>
                                <li><a href="#delivery-order" data-toggle="tab">Delivery Orders</a>
                                </li>
                                <li style="display:none"><a href="#invoices" data-toggle="tab">Invoices</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="order">
                                    <h4>Orders</h4>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover">
                                            <tbody>
                                                <tr>
                                                    <th>Order No.</th>
                                                    <th>Date Created</th>
                                                    <th width="100px">Action</th>
                                                </tr>
                                                <tr>
                                                    <th><?=$result['order_no']?></th>
                                                    <td><?=date("d/m/Y", strtotime(substr($result['created_date'],0,10)))?></td>
                                                    <td>
                                                        <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button" onclick="location.href='<?=base_url('en/agora/sales/order_edit/'.$result['id'])?>'"><i class="fa fa-folder-open-o"></i></button>
                                                </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                                                
                                <div class="tab-pane fade" id="packinglist">
                                    <h4>Packing List</h4>
                                    <div class="table-responsive">
                                    	<?php if(!empty($packing_result)){?>
                                        <table class="table table-striped table-bordered table-hover">
                                            <tbody>
                                                <tr>
                                                    <th>Packing List No.</th>
                                                    <th>Date Created</th>
                                                    <th width="100px">Action</th>
                                                </tr>
                                                <?php foreach($packing_result as $k => $v){?>
                                                <tr>
                                                    <th><?=$v['packing_no']?></th>
                                                    <td><?=date("d/m/Y", strtotime(substr($v['created_date'],0,10)))?></td>
                                                    <td>
                                                        <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button" onclick="location.href='<?=base_url('en/agora/ground_staff/packing_list_edit/'.$v['id'])?>'"><i class="fa fa-folder-open-o"></i></button>
                                                        <a target="_blank" href="<?=base_url('en/agora/ground_staff/packing_list_pdf/'.$v['id'])?>"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Download" type="button"><i class="fa fa-download"></i></button></a>
                                                </td>
                                                </tr>
                                                <?php }?>
                                            </tbody>
                                        </table>
                                        <?php }else{ ?>
											<p>No result</p>
										<?php }?>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                
                                <div class="tab-pane fade" id="delivery-order">
                                    <h4>Delivery Order</h4>
                                    <div class="table-responsive">
                                    	<?php if(!empty($do_result)){?>
                                        <table class="table table-striped table-bordered table-hover">
                                            <tbody>
                                                <tr>
                                                    <th>Delivery Order No.</th>
                                                    <th>Date Issued</th>
                                                    <th>Delivered?</th>
                                                    <th width="100px">Action</th>
                                                </tr>
                                                <?php foreach($do_result as $k => $v){?>
                                                <tr>
                                                    <th><?=$v['do_no']?></th>
                                                    <td><?=date("d/m/Y", strtotime(substr($v['created_date'],0,10)))?></td>
                                                    <td><?=$v['status']!=0?($v['status']==1?'No':'Yes'):'Cancelled'?></td>
                                                    <td>
                                                        <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button" onclick="location.href='<?=base_url('en/agora/admin/delivery_order_edit/'.$v['id'])?>'"><i class="fa fa-folder-open-o"></i></button>
                                                        <a target="_blank" href="<?=base_url('en/agora/admin/do_pdf/'.$v['id'])?>"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Download" type="button"><i class="fa fa-download"></i></button>
                                                </td>
                                                </tr>
                                                <?php }?>
                                            </tbody>
                                        </table>
                                        <?php }else{ ?>
											<p>No result</p>
										<?php }?>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
								
                                <div class="tab-pane fade" id="invoices" style="display:none">
                                    <h4>Invoices</h4>
                                    <div class="table-responsive">
                                    	<?php if(!empty($invoice_result)){?>
                                        <table class="table table-striped table-bordered table-hover">
                                            <tbody>
                                                <tr>
                                                    <th>Invoice No.</th>
                                                    <th>Date Created</th>
                                                    <th width="100px">Action</th>
                                                </tr>
                                                <?php foreach($invoice_result as $k => $v){?>
                                                <tr>
                                                    <th><?=$v['invoice_no']?></th>
                                                    <td><?=date("d/m/Y", strtotime(substr($v['created_date'],0,10)))?></td>
                                                    <td>
                                                        <button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button" onclick="location.href='<?=base_url('en/agora/finance/invoice_edit/'.$v['id'])?>'"><i class="fa fa-folder-open-o"></i></button>
                                                        <a target="_blank" href="<?=base_url('en/agora/finance/invoice_pdf/'.$v['id'])?>"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Download" type="button"><i class="fa fa-download"></i></button>
                                                </td>
                                                </tr>
                                                <?php }?>
                                            </tbody>
                                        </table>
                                        <?php }else{ ?>
											<p>No result</p>
										<?php }?>
                                        
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                
                            </div>
                            <!-- /.tab-content -->

                        </div>

                        <!-- /.panel-body -->

                    </div>

                    <!-- /.panel -->

                </div>

                <!-- /.col-lg-12 -->

            </div>

            <!-- /.row -->

            

        </div>

        <!-- /#page-wrapper -->
