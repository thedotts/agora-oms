

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-user fa-fw"></i> All Customers
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input id="q" class="form-control" placeholder="Search" onblur="filter()" value="<?=$q?>">
                                </div>
                                <!-- /.form-group -->
                            </form>
                            <script>
							function filter(){								
								var q = $("#q").val();
								if(q == '') {
									q = 'ALL';	
								}								
								location.href='<?=base_url('en/agora/'.$group_name.'/customers_all')?>/'+q;
							}
							</script>
                        </div>
                		<!-- /.col-lg-3 -->
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default hidden-xs">
                    	<?php
								if(!empty($results)) {
								?>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Customer ID</th>
                                            <th>Customer</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
										foreach($results as $k => $v) {
										?>
                                        <tr>
                                            <td><?=($k+1)?></td>
                                            <td><?=$v['custom_code']?></td>
                                            <td><?=$v['company_name']?></td>
                                            <td>
                                            	<a class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View" type="button" href="<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_view/'.$v['id'])?>"><i class="fa fa-briefcase"></i></a>
                                                
                                        </tr>
                                        <?php
										}
										?>                                        
                                    </tbody>
                                </table>
                                
                                <?php									
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
								?>
          </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                    <!-- Data list (Mobile) -->
                        <div class="mobile-list visible-xs">
                        	<?php
								if(!empty($results)) {
								?>
                        	<table class="table table-striped table-bordered table-hover">
                            	<?php										
										foreach($results as $k=>$v) {
									?>
                            	<tr>
                                	<td>
                                    	<div class="row mobile-list-header">
                                            <div class="col-xs-12"><strong>#<?=($k+1)?></strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Customer ID:</strong></div>
                                            <div class="col-xs-8"><?=$v['custom_code']?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Customer:</strong></div>
                                            <div class="col-xs-8"><?=$v['company_name']?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Action:</strong></div>
                                            <div class="col-xs-8"><a class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View" type="button" href="<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_view/'.$v['id'])?>"><i class="fa fa-briefcase"></i></a></div>
                                        </div>
                                    </td>
                                </tr>
                                <?php											
										}
									?>
                            </table>
                            <?php
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
								?>
                                                       
                        </div>
                    
                    <?=$paging?>
                    
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
             <script>
			 
		var alert_v = <?=$alert?>;
		
		switch(alert_v){
			case 1:
				alert('Create successful');
				break;
			case 2:
				alert('Edit successful');
				break;
			case 3:
				alert('Delete successful');
				break;
		}
			 
		function deleteData(url){
			var c = confirm("Are you sure you want to delete?");
			if(c){
				location.href=url;	
			}
		}
		
		//enter search
		$(document).ready(function() {
		  $(window).keydown(function(event){
			if(event.keyCode == 13) {
			  event.preventDefault();
			  q.blur();
			  return false;
			}
		  });
		});
		</script>

        