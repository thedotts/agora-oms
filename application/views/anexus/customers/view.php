<div id="page-wrapper">
        	<form>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-user fa-fw"></i> Customer Detail</h1>
                    <ul class="breadcrumb">
                    	<li><a href="<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_all')?>">Customers</a></li>
                    	<li><?=$result['company_name']?></li>
                    </ul>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                
                
                    <div class="panel panel-default">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <tbody>
                                        <tr>
                                            <th width="20%">Company Name</th>
                                            <td width="30%"><?=$result['company_name']?></td>
                                            <td></td>
                                            <th width="20%">Customer Code</th>
                                            <td width="30%"><?=$result['custom_code']?></td>
                                        </tr>
                                        <tr>
                                            <th>Address</th>
                                            <td><?=$result['address']?></td>
                                            <td></td>
                                            <th>Postal</th>
                                            <td><?=$result['postal']?></td>
                                        </tr>
                                        <tr>
                                            <th>Country</th>
                                            <td><?=$result['country']?></td>
                                            <td></td>
                                            <th>Email</th>
                                            <td><?=$result['primary_contact_email']?></td>
                                        </tr>
                                        <tr>
                                            <th>Contact Person</th>
                                            <td><?=$result['primary_attention_to']?></td>
                                            <td></td>
                                            <th>Contact</th>
                                            <td><?=$result['primary_contact_info']?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                
                    
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Orders
                        </div>
                        <div class="table-responsive">
                        	<?php
							if(!empty($job)){
							?>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>      
                                        <th>Order No.</th>
                                        <th>Requested Date</th>
                                        <th>Completed Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
								foreach($job as $k => $v) {
								?>
                                    <tr>
                                    	<td><?=($k+1)?></td>
                                        <td><?=$v['serial']?></td>
                                        <td><?=date('d/m/y',strtotime(substr($v['created_date'],0,10)))?></td>
                                        <td><?=$v['is_completed']?date('d/m/y',strtotime(substr($v['modified_date'],0,10))):'-'?></td>
                                        <td><?=$v['status_text']?></td>                                        
                                            <td>
                                                <a href="<?=base_url($init['langu'].'/agora/job_detail/'.$v['order_id'])?>"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View" type="button"><i class="fa fa-folder-open"></i></button></a></td>
                                    </tr>
                                 <?php
								}
								?>                                    
                                </tbody>
                            </table>
                            <?php
							} else {
							?>
                            <p>No data yet</p>
                            <?php	
							}
							?>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->