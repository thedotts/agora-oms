<div id="page-wrapper">
        	<form method="post" action="<?=base_url('en/agora/'.$group_name.'/'.$model_name.'_submit')?>" onsubmit="return validate();">
            <input type="hidden" id="id" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-pencil-square-o fa-fw"></i> <?=$mode=='Add'?'Create':'Edit'?> Customer Account
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Customer Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Customer No.</label>
                                    <input name="custom_code" class="form-control" disabled value="<?=$mode=='Edit'?$result['custom_code']:''?>">
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Status *</label>
                                    <select name="status" class="form-control">
                                        <?php
										foreach($status_list as $k=>$v) {
										?>
                                        <option value="<?=$k?>" <?=$mode=='Edit'&&$k==$result['status']?'selected="selected"':''?>><?=$v?></option>
                                        <?php
										}
										?>  
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Company Name *</label>
                                    <input class="form-control" id="company_name" name="company_name" value="<?=$mode=='Edit'?$result['company_name']:''?>">
                                    <span style="color:red" id="company_name_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Primary Attention To *</label>
                                    <input class="form-control" id="primary_attention_to" name="primary_attention_to" value="<?=$mode=='Edit'?$result['primary_attention_to']:''?>">
                                    <span style="color:red" id="primary_attention_to_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Primary Contact No. *</label>
                                    <input class="form-control" id="primary_contact_info" name="primary_contact_info" value="<?=$mode=='Edit'?$result['primary_contact_info']:''?>">
                                    <span style="color:red" id="primary_contact_info_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Primary Contact Email *</label>
                                    <input onblur="check_email()" class="form-control" id="primary_contact_email" name="primary_contact_email" value="<?=$mode=='Edit'?$result['primary_contact_email']:''?>">
                                    <input id="email_check" type="hidden" value="0">
                                    <span style="color:red" id="primary_contact_email_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Secondary Attention To</label>
                                    <input class="form-control" name="secondary_attention_to" value="<?=$mode=='Edit'?$result['secondary_attention_to']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Secondary Contact No</label>
                                    <input class="form-control" name="secondary_contact_info" value="<?=$mode=='Edit'?$result['secondary_contact_info']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Secondary Contact Email</label>
                                    <input class="form-control" name="secondary_contact_email" value="<?=$mode=='Edit'?$result['secondary_contact_email']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Address *</label>
                                    <input class="form-control" id="address" name="address" value="<?=$mode=='Edit'?$result['address']:''?>">
                                    <span style="color:red" id="address_err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Postal Code *</label>
                                    <input class="form-control" id="postal" name="postal" value="<?=$mode=='Edit'?$result['postal']:''?>">
                                    <span style="color:red" id="postal_err"></span>
                                </div>
                                
                                <div class="form-group">
                                    <label>Country *</label>
                                    <select name="country" class="form-control">
                                        
                                    <option value="Singapore" <?=$mode=='Edit'&&$result['country']=='Singapore'?'selected="selected"':''?>>Singapore</option>
                                    <option value="Malaysia" <?=$mode=='Edit'&&$result['country']=='Malaysia'?'selected="selected"':''?>>Malaysia</option>
                                        
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                    <label>Password *</label>
                                    <input type="password" id="pwd" name="password" class="form-control">
                                    <span style="color:red" id="pwd_err"></span>
                                    <?=$mode=='Edit'?'<p class="help-block"><i>Leave Blank if no change</i></p>':''?>
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password *</label>
                                    <input type="password" id="copwd" name="copassword" class="form-control">
                                    <span style="color:red" id="copwd_err"></span>
                                    <?=$mode=='Edit'?'<p class="help-block"><i>Leave Blank if no change</i></p>':''?>
                                </div>
                                
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    
                     <div class="panel panel-primary">
                        <div class="panel-heading">
                            Delivery Address
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <button class="btn btn-default btn-sm" type="button" onclick="add_DA()">Add Delivery Address</button>
                                <input type="hidden" id="da_counter" name="da_counter" value="<?=$mode=='Edit'?count($da_json):0?>"/>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Alias *</th>
                                            <th>Address *</th>
                                            <th>Postal Code *</th>
                                            <th>Country *</th>
                                            <th width="80px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="item_da">
                                    <?php if($mode == 'Edit'){?>
                                    <?php foreach($da_json as $k => $v){?>
                                    	<tr id="da_<?=$k?>">
    									<td>
                                        <input id="alias<?=$k?>" name="alias<?=$k?>" type="text" data-name="Alias" class="form-control da" value="<?=$v['alias']?>">
                                        </td>
    									<td>
                                        <input id="add<?=$k?>" name="add<?=$k?>" type="text" data-name="Address" class="form-control da" value="<?=$v['add']?>">
                                        </td>
    									<td>
                                        <input id="postal<?=$k?>" name="postal<?=$k?>"  data-name="Postal Code" type="text" class="form-control da" value="<?=$v['postal']?>">
                                        </td>
    									<td>
                                        <select id="country<?=$k?>" name="country<?=$k?>" class="form-control">
                                        <option <?=$v['country']=='Singapore'?'selected':''?>>Singapore</option>
                                        <option <?=$v['country']=='Malaysia'?'selected':''?>>Malaysia</option>
                                        </select>
                                        </td>
										<td>
                                        <button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="del_da(<?=$k?>)"><i class="fa fa-times"></i></button>
                                        </td>
										<input class="is_del" type="hidden" id="is_del<?=$k?>" name="is_del<?=$k?>" value="0"/>
    									</tr>
                                    <?php }?>
                                    <?php }?>
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                    
                    
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Price List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <button onclick="reset_all_price()" class="btn btn-default btn-sm" type="button"><i class="fa fa-refresh"></i> Reset Default Prices</button>
                                <input type="checkbox" id="default_price" name="default_price" <?=$mode=='Edit'?($result['use_default_price']==1?'checked':''):''?>> Use Default Price
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Product</th>
                                            <th>Description</th>
                                            <th width="500px">Price *</th>
                                            <th width="80px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="item_product">
                                        
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                    
                    
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            
            
            <div class="row">
                <div class="col-lg-12">
                    
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->

<script>
var mode = '<?=$mode?>';

$(document).ready(function(e) {
	//$("#dob").datepicker({"dateFormat":"dd/mm/yy"}); 
	//$("#expired_date").datepicker({"dateFormat":"dd/mm/yy"});
	
	if(mode == 'Add'){  
		add_DA();  
	}
});


$(function(){
	if(mode == 'Add'){
		$('#default_price').prop('checked',true);	
	}
});

var da_counter = <?=$mode=='Edit'?count($da_json):0?>;

function add_DA(){
	
	tmp ='<tr id="da_'+da_counter+'">';
    tmp +='<td><input id="alias'+da_counter+'" name="alias'+da_counter+'" data-name="Alias" type="text" class="form-control da" value=""></td>';
    tmp +='<td><input id="add'+da_counter+'" name="add'+da_counter+'" data-name="Address" type="text" class="form-control da" value=""></td>';
    tmp +='<td><input id="postal'+da_counter+'" name="postal'+da_counter+'" data-name="Postal Code" type="text" class="form-control da" value=""></td>';
    tmp +='<td><select id="country'+da_counter+'" name="country'+da_counter+'" class="form-control"><option>Singapore</option><option>Malaysia</option></select></td>';
	tmp +='<td><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="del_da('+da_counter+')"><i class="fa fa-times"></i></button></td>';
	tmp +='<input class="is_del" type="hidden" id="is_del'+da_counter+'" name="is_del'+da_counter+'" value="0"/>';
    tmp +='</tr>';
	
	da_counter++;
	$('#da_counter').val(da_counter);
	$('#item_da').append(tmp);
	
}

function del_da(id){
	$('#da_'+id).hide();
	$('#is_del'+id).val(1);
}

var product = <?=$product?>;
var price_list = <?=$mode=='Edit'?$result['price_list']:'[]'?>;

function reset_all_price(){
	for(var i=0;i<product.length;i++){
		$('#price'+i).val(product[i].selling_price);
	}
}

function reset_price(i){
	$('#price'+i).val(product[i].selling_price);
}

if(mode == 'Add' || price_list.length == 0){

	for(var i=0;i<product.length;i++){
		
		tmp ='<tr>';
		tmp +='<td>'+product[i].product_name+'<input name="productId[]" type="hidden" value="'+product[i].id+'"></td>';
		tmp +='<td>'+product[i].model_name+'</td>';
		tmp +='<td><div class="form-group input-group"><span class="input-group-addon">$</span><input id="price'+i+'" name="price[]" type="text" class="form-control" value="'+product[i].selling_price+'"><span class="input-group-addon"> Carton(s)</span></div></td>';
		tmp +='<td><button onclick="reset_price('+i+')" class="btn btn-danger btn-circle reset-btn" data-placement="top" data-toggle="tooltip" data-original-title="Reset to Default" type="button"><i class="fa fa-refresh"></i></button></td>';
		tmp +='</tr>';
		
		$('#item_product').append(tmp);
		
	}

}else{
	
	for(var i=0;i<product.length;i++){
		
		//if json product id match get price,else take orginal price
		for(var j=0;j<price_list.length;j++){
			if(price_list[j].id == product[i].id){
				price = price_list[j].price;
				break;
			}else{
				price = product[i].selling_price;
			}
		}
		
		tmp ='<tr>';
		tmp +='<td>'+product[i].product_name+'<input name="productId[]" type="hidden" value="'+product[i].id+'"></td>';
		tmp +='<td>'+product[i].model_name+'</td>';
		tmp +='<td><div class="form-group input-group"><span class="input-group-addon">$</span><input id="price'+i+'" name="price[]" type="text" class="form-control" value="'+price+'"><span class="input-group-addon"> Carton(s)</span></div></td>';
		tmp +='<td><button onclick="reset_price('+i+')" class="btn btn-danger btn-circle reset-btn" data-placement="top" data-toggle="tooltip" data-original-title="Reset to Default" type="button"><i class="fa fa-refresh"></i></button></td>';
		tmp +='</tr>';
		
		$('#item_product').append(tmp);
		
	}
	
}

var use_default_price = <?=$mode=='Edit'?$result['use_default_price']:0?>;

if(use_default_price == 1){
	$("input[name='price[]']").attr('readonly', true);
	$(".reset-btn").attr('disabled', true);
}

$("#default_price").change(function() {
	
    if(this.checked) {
        $("input[name='price[]']").attr('readonly', true);
		$(".reset-btn").attr('disabled', true);
    }else{
		$("input[name='price[]']").attr('readonly', false);
		$(".reset-btn").attr('disabled', false);
	}
	
});

function check_email(){
	
	email = $("#primary_contact_email").val();
	id = $("#id").val();
	term = {"email":email,"id":id};
	url = '<?=base_url($init['langu'].'/agora/administrator/customer_check_email')?>';
	
	if(validateEmail(email)){
	
		$.ajax({
			type:"POST",
			url: url,
			data: term,
			dataType : "json"
			}).done(function(r) {
				if(r.status == "ok") {
					
					if(r.email == 1){
						$('#primary_contact_email_err').text('Email not available');
						$('#email_check').val(0);
					}else{
						$('#primary_contact_email_err').text('');	
						$('#email_check').val(1);
					}
					
				}
		});	
	
	}else{
		$('#primary_contact_email_err').text('Wrong email format');
		$('#email_check').val(0);
	}
}

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

if(mode=='Edit'){
	check_email();
}
function validate(){
	
	var error = 0;
	
	//email
	var email_check = $('#email_check').val();
	if(email_check == 0){
		check_email();
		error = 1;
	}
	
	//pwd
	if(mode == 'Add'){
		if($('#pwd').val() == ''){
			$('#pwd_err').text('Password can\'t be blank');
			error = 1;
		}else{
			$('#pwd_err').text('');
		}
	}
	
	//co password
	if($("#pwd").val()!=$("#copwd").val()) {
		$('#copwd_err').text('Password is not same with confirm password');
		error = 1;
	}else{
		$('#copwd_err').text('');
	}
	
	//company name
	if($("#company_name").val() == '') {
		$('#company_name_err').text('Company name can\'t be blank');
		error = 1;
	}else{
		$('#company_name_err').text('');
	}
	
	//primary_attention_to
	if($("#primary_attention_to").val() == '') {
		$('#primary_attention_to_err').text('Primary Attention To can\'t be blank');
		error = 1;
	}else{
		$('#primary_attention_to_err').text('');
	}
	
	//primary_contact_info
	if($("#primary_contact_info").val() == '') {
		$('#primary_contact_info_err').text('Primary Contact No can\'t be blank');
		error = 1;
	}else{
		$('#primary_contact_info_err').text('');
	}
	
	//address
	if($("#address").val() == '') {
		$('#address_err').text('Address can\'t be blank');
		error = 1;
	}else{
		$('#address_err').text('');
	}
	
	//address
	if($("#postal").val() == '') {
		$('#postal_err').text('Postal Code can\'t be blank');
		error = 1;
	}else{
		
		if(isNaN($("#postal").val())){
			$('#postal_err').text('Postal code needs to be integer only');
			error = 1;
		}else{
			$('#postal_err').text('');
		}
		
	}
	
	//delivery address
	
	$('.da').each(function() {
		//is del
		var is_del = $(this).closest('tr').find('.is_del').val();
		if(is_del != 1){
        var name = $(this).data('name');
		if($(this).val() == ''){
			alert(name+' can\'t be blank');
			$(this).focus();
			error = 1;
			return false;
		}else{
			
			if(name == 'Postal Code' && isNaN($(this).val())){
				alert(name+' needs to be integer only');
				$(this).focus();
				error = 1;
				return false;
			}
			
		}
		}
    });
	
	if(error == 1){
		return false;
	}
	
}

</script>