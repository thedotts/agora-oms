<div id="page-wrapper">
        	<form method="post" action="<?=base_url('en/anexus/'.$group_name.'/'.$model_name.'_submit')?>" onsubmit="return validate();">
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-pencil-square-o fa-fw"></i> Create Supplier Account
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Supplier Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Supplier No.</label>
                                    <input class="form-control" disabled value="<?=$mode=='Edit'?$result['supplier_no']:''?>">
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Status *</label>
                                    <select name="status" class="form-control">
                                        <?php
										foreach($status_list as $k=>$v) {
										?>
                                        <option value="<?=$k?>" <?=$mode=='Edit'&&$k==$result['status']?'selected="selected"':''?>><?=$v?></option>
                                        <?php
										}
										?>  
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input class="form-control" name="company_name" value="<?=$mode=='Edit'?$result['company_name']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Contact Name</label>
                                    <input class="form-control" name="contact_person" value="<?=$mode=='Edit'?$result['contact_person']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Contact No</label>
                                    <input class="form-control" name="contact_info" value="<?=$mode=='Edit'?$result['contact_info']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" name="email" value="<?=$mode=='Edit'?$result['email']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control" name="address" value="<?=$mode=='Edit'?$result['address']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Postal</label>
                                    <input class="form-control" name="postal" value="<?=$mode=='Edit'?$result['postal']:''?>">
                                </div>
                         </div>

                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            
            
            <div class="row">
                <div class="col-lg-12">
                    
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->

<script>

$(document).ready(function(e) {
	//$("#dob").datepicker({"dateFormat":"dd/mm/yy"}); 
	//$("#expired_date").datepicker({"dateFormat":"dd/mm/yy"});    
});



function validate(){
		
	
	
}
</script>