<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-usd fa-fw"></i> Business Trip Report
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input id="q" class="form-control" placeholder="Search" value="<?=$q?>" onblur="filter();">
                                    </select>
                                </div>
                                <script>
							function filter(){
								var q = $("#q").val();
								if(q == '') {
									q = 'ALL';	
								}								
								location.href='<?=base_url('en/anexus/'.$group_name.'/'.$model_name)?>/'+q;
							}
							</script>
                                <!-- /.form-group -->
                            </form>
                        </div>
                		<!-- /.col-lg-3 -->
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <a href="<?=base_url('en/anexus/'.$group_name.'/'.$model_name.'_add')?>"><button class="btn btn-default btn-sm" type="button">Create Business Trip Report</button></a><p></p>
                    <div class="panel panel-default">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Report No.</th>
                                            <th>Job ID</th>
                                            <th>Customer</th>
                                            <th>Last Updated</th>
                                            <th>Last Updated By</th>
                                            <th>Awaiting</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php if(!empty($results)){
												foreach($results as $v)
												{
													?>
													<tr>
                                                        <td><?=$v['id']?></td>
                                                        <td><?=$v['report_no']?></td>
                                                        <td><?=$v['job_id']?></td>
                                                        <td><?=$v['company_name']?></td>
                                                        <td><?=$v['modified_date'] != '0000-00-00 00:00:00'?date('d/m/Y',strtotime($v['modified_date'])):''?></td>
                                                        <td><?=isset($user_list[$v['lastupdate_user_id']])?$user_list[$v['lastupdate_user_id']]:''?></td>
                                                        <td><?php
											if(!empty($v['awaiting_table'])){
                                            	foreach($v['awaiting_table'] as $x){
													if(isset($role_list[$x])){
													echo $role_list[$x].'<br>';
													}
												}
											}
											?></td>
                                                        <td><?=$btr_status_list[$v['status']]?></td>
                                                        <td>
                                                        	<button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View" type="button"><i class="fa fa-folder-open"></i></button>
                                                            <button  class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button" onclick="location.href='<?=base_url('en/anexus/finance/business_trip_edit/'.$v['id'])?>'"><i class="fa fa-edit"></i></button>
                                                            <button  class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="deleteData('<?=base_url('en/anexus/finance/business_trip_del/'.$v['id'])?>')"><i class="fa fa-times"></i></button>
                                                            
                                                         </td>
                                                    </tr>
                                            		<?php
												}
											}
											else 
											{
												echo "<td colspan='8'><p><b>No records found.</b></p>";
											}
										?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                    <?=$paging?>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
        <script>
		function deleteData(url){
			var c = confirm("Are you sure you want to delete?");
			if(c){
				location.href=url;	
			}
		}
		</script>
