<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-usd fa-fw"></i> <?=$head_title?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                       		<!-- Button trigger modal -->
                            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" <?=$mode=='Edit'?'disabled':''?>>
                                Seach from Quotation
                            </button>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Search Quotation</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Search</label>
                                                <input class="form-control" placeholder="By Company Name or Reference No." id="search" name="search">
                                            </div>
                                            <table id="quotation_table" class="table table-striped table-bordered table-hover" id="dataTables">
                                            </table>
                                            <div id="pagination"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            
            <form action="<?=base_url($init['langu'].'/anexus/logistics/commercial_invoice_submit');?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" id="quotation_id" name="quotation_id" value="<?=$mode=='Edit'?$result['quotation_id']:''?>"/>
            <input type="hidden" id="sale_person_id" name="sale_person_id" value="<?=$mode=='Edit'?$result['sales_person_id']:$this->data['userdata']['id']?>"/>
            <input type="hidden" id="ad_hoc" name="ad_hoc" value="<?=$mode=='Edit'?$result['ad_hoc']:1?>"/>
            <input type="hidden" id="requestor_id" name="requestor_id" value="<?=$mode=='Edit'?$result['requestor_id']:$staff_info['id']?>"/>
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Invoice Information
                        </div>
                        <div class="panel-body">
                        		<?php
								if($userdata['role_id']==1) {
								?>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status" class="form-control">
                                    	<?php
										foreach($ci_status_list as $k=>$v) {
										?>
                                        <option value="<?=$k?>" <?=$mode=='Edit'&&$k==$result['status']?'selected="selected"':''?>><?=$v?></option>
                                        <?php	
										}
										?>
                                    </select>
                                </div>
                                <?php	
								}
								?>
                                
                                <div class="form-group">
                                    <label>Invoice No.</label>
                                    <input class="form-control" value="<?=$mode=='Edit'?$result['invoice_no']:''?>" readonly>
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Date of Issue</label>
                                    <input id="issue_date" name="issue_date" class="form-control" placeholder="Calendar Input" value="<?=$mode=='Edit'?date('d/m/Y',strtotime($result['issue_date'])):''?>">
                                </div>
                                <div class="form-group">
                                    <label>Customer Purchase Order Ref</label>
                                    <input id="CP_Order_Ref" name="CP_Order_Ref" class="form-control" value="<?=$mode=='Edit'?$result['purchase_order_ref']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Sales Order Ref</label>
                                    <input id="S_Order_Ref" name="S_Order_Ref" class="form-control"   value="<?=$mode=='Edit'?$result['sales_order_ref']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Originator</label>
                                    <input id="Originator" name="Originator" class="form-control" value="<?=$mode=='Edit'?$user_list[$result['sales_person_id']]:$this->data['user_list'][$this->data['userdata']['id']]?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Sales Order No.</label>
                                    <input id="S_Order_No" name="S_Order_No" class="form-control" value="<?=$mode=='Edit'?$result['sales_order_no']:''?>" >
                                </div>
                                <div class="form-group">
                                    <label>Delivery Order No.</label>
                                    <input id="DeliveryOrderNo" name="DeliveryOrderNo" class="form-control" value="<?=$mode=='Edit'?$result['delivery_order_no']:''?>" >
                                </div>
                                <div class="form-group">
                                    <label>Currency</label>
                                    <input id="Currency" name="Currency" class="form-control" value="<?=$mode=='Edit'?$result['currency']:''?>" >
                                </div>
                                <div class="form-group">
                                    <label>Terms of Payment</label>
                                    <input id="TermsofPayment" name="TermsofPayment" class="form-control"  value="<?=$mode=='Edit'?$result['term_of_payment']:''?>" >
                                </div>
                                <div class="form-group">
                                    <label>Payment Due Date</label>
                                    <input id="payment_date" name="payment_date" class="form-control" placeholder="Calendar Input" value="<?=$mode=='Edit'?date('d/m/Y',strtotime($result['payment_due_day'])):''?>">
                                </div>
                                
                                <div class="form-group">
                                    <label>Purchase Order No.</label>
                                    <input id="PurchaseOrderNo" name="PurchaseOrderNo" class="form-control" value="<?=$mode=='Edit'?$result['purchase_order_no']:''?>" >
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6"> 
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Customer Information
                        </div>
                        <div class="panel-body">
                        		<input type="hidden" id="customer_id" name="customer_id" value="<?=$mode=='Edit'?$result['customer_id']:''?>" />
                                <input type="hidden" id="customer_name" name="customer_name" value="<?=$mode=='Edit'?$result['customer_name']:''?>" />
                                <?php if($mode == 'Add'){?>
                                <div class="form-group" id="customer_sGroup">
                                    <label>Customer</label>
                                    <select onchange="getCompany()" id="customer_select" name="customer_select" class="form-control"><option value="none">select</option></select>
                                </div>
                                <?php } ?>
                                <div class="form-group">
                                    <label>Invoice To</label>
                                    <input id="invoiceTo" name="invoiceTo" class="form-control" value="<?=$mode=='Edit'?$result['invoice_to']:''?>" >
                                </div>
                                <div class="form-group">
                                    <label>Account No.</label>
                                    <input id="accountNo" name="accountNo" class="form-control" value="<?=$mode=='Edit'?$result['account_no']:''?>" >
                                </div>
                                <div class="form-group">
                                    <label>Delivered To</label>
                                    <input id="DeliveredTo" name="DeliveredTo" class="form-control" value="<?=$mode=='Edit'?$result['delivered_to']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Attention To</label>
                                    <input id="AttentionTo" name="AttentionTo" class="form-control" value="<?=$mode=='Edit'?$result['primary_attention_to']:''?>">
                                </div>
                                <div class="form-group">
                                   	<label>Contact</label>
                                    <input id="Contact" name="Contact" class="form-control" value="<?=$mode=='Edit'?$result['primary_contact']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Attention To</label>
                                    <input id="AttentionTo2" name="AttentionTo2" class="form-control" value="<?=$mode=='Edit'?$result['secoundary_attention_to']:''?>">
                                </div>
                                <div class="form-group">
                                   	<label>Contact</label>
                                    <input id="Contact2" name="Contact2" class="form-control" value="<?=$mode=='Edit'?$result['secoundary_contact']:''?>">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                
                
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Total Amount
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Sub-Total</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-usd"></i>
                                        </span>
                                        <input id="sub_total" name="sub_total" type="text" class="form-control" value="<?=$mode=='Edit'?$result['subtotal']:''?>" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>GST @ <?=$gst['value']?>%</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-usd"></i>
                                        </span>
                                        <input id="gst" name="gst" type="text" class="form-control" value="<?=$mode=='Edit'?$result['gst']:''?>" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Grand Total</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-usd"></i>
                                        </span>
                                        <input id="grand_total" name="grand_total" type="text" class="form-control" value="<?=$mode=='Edit'?$result['grand_total']:''?>" >
                                    </div>
                                </div>
                                <?php if($mode=='Edit'){?>
                                    <div class="form-group">
                                    <label class="checkbox-inline">
                                    <input id="auto_caculate" name="auto_caculate" type="checkbox"> Automatic Calculate Amount
                                    </label>
                                	</div>
                                <?php }?>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
            </div>
            <!-- /.row -->
            <hr />
            
            <div class="row">
                <div class="col-lg-12">
                
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Commercial Invoice Items
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                            <?php if($mode == 'Add' || $result['ad_hoc'] == 1){?>
                            <button class="btn btn-default btn-sm add-btn" type="button" onclick="addGroup('product')">Add New Product Item</button>
                            <button class="btn btn-default btn-sm add-btn" type="button" onclick="addGroup('service')">Add New Service Item</button>
                            <?php }?>
                            <input id="invoiceCount" name="invoiceCount" value="0" type="hidden" onclick="addGroup('service')"/>
                                <table class="table table-hover">
                                    <thead>
                                        <tr id="invoice_head">
                                        <?php if($mode == 'Add' || $result['ad_hoc'] == 1){ ?>
                                            <th>#</th>
                                            <th width="15%">Model No.</th>
                                            <th width="5%">UOM</th>
                                            <th width="8%">Quantity</th>
                                            <th width="20%">Item Description</th>
                                            <th>Unit Price</th>
                                            <th>Extended Price</th>
                                            <th>Total Price</th>
                                            <th width="80px">Action</th>
                                        <?php }else{?>
                                        	<th>#</th>
                                            <th width="5%">UOM</th>
                                            <th width="8%">Quantity</th>
                                            <th width="20%">Item Description</th>
                                            <th>Unit Price</th>
                                            <th>Extended Price</th>
                                            <th>Total Price</th>
                                            <th width="80px">Action</th>
                                        <?php }?>

                                        </tr>
                                    </thead>
                                    <tbody id="invoice_item">
                                        
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                    
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->    
                        
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Remarks
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <textarea id="remarks" name="remarks" class="form-control"><?=$mode=='Edit'?$result['remarks']:''?></textarea>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                	<hr>
                </div>
                <!-- /.col-lg-12 -->
                   
                <div class="col-lg-6"> 
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Packing List
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Packing List No:</label>
                                    <input id="PackingListNo" name="PackingListNo" class="form-control" value="<?=$mode=='Edit'?$result['packing_list_no']:''?>" readonly>
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Partial Delivery</label>
                                    <select id="PartialDelivery" name="PartialDelivery" class="form-control">
                                    	<option value="1"<?=$mode=='Edit'?($result['partial_delivery']==1?'selected':''):''?>>Yes</option>
                                        <option value="0"<?=$mode=='Edit'?($result['partial_delivery']==0?'selected':''):''?>>No</option>
                                    </select>
                                </div>
                         </div>
                		 <!-- /.panel-body -->

                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Shipping Instructions
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <textarea id="ShippingInstructions" name="ShippingInstructions" class="form-control"><?=$mode=='Edit'?$result['shipping_instructions']:''?></textarea>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-12"> 
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Packing list Items
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                            <input id="packingCount" name="packingCount" value="0" type="hidden"/>
                                <table class="table table-hover">
                                    <thead>
                                        <tr id="packing_head">
                                        <?php if($mode == 'Add' || $result['ad_hoc'] == 1){ ?>
                                        	<th>#</th>
                                            <th width="15%">Model No.</th>
                                            <th>Quantity</th>
                                            <th>WH</th>
                                            <th>Qty B/O</th>
                                            <th>Qty Delivered</th>
                                            <th>Parts Description</th>
                                            <th width="80px">Action</th>
                                        <?php }else{?>
                                        	<th>#</th>
                                            <th>Quantity</th>
                                            <th>WH</th>
                                            <th>Qty B/O</th>
                                            <th>Qty Delivered</th>
                                            <th>Parts Description</th>
                                            <th width="80px">Action</th>
                                        <?php }?>
                                        </tr>
                                    </thead>
                                    <tbody id="packing_list_item">
                                        
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                    
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                    <?php if($mode=='Add'){?>
                    
                    	<?php if($btn_type == 'create'){?>
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                        </div>
                    	<?php }?>
                        
                    <?php }else if($mode=='Edit'){?>
                    	<?php switch($btn_type){
							case 'edit':
							?>
                        		<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Submit</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            <?php 
							break;
							case 'approval':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Update</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'approval_update':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'sendmail_update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-default">Save</button>
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/logistics/ci_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/logistics/ci_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'send_email':
							?>
                            	<div class="form-group">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/logistics/ci_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/logistics/ci_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                </div>
                            
                            <?php 
							break;
							case 'upload':
							?>
                            
                            	<div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/logistics/ci_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/logistics/ci_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file">
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                		<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            
                            <?php 
							break;
							case 'complete':
							?>
                            	<?php if($last_action == 'upload'){?>
                            	<div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/logistics/ci_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/logistics/ci_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file"> 
                                            <a target="_blank" href="<?=$result['comfirm_file']?>"><?=$result['comfirm_file']?></a>
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Reupload</button>
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            	<?php }?>
                            <?php 
							break;
                        }?>

                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Status
                        </div>
                        <div class="panel-body">
                            <div class="form-group input-group">
							<?php 
                                //如果不是not approved, 也不是 completed 就顯示目前正在等誰
                                if($result['status'] != 0 && $result['status'] != $completed_status_id){
                            ?>
                            <label>
                                                    <?php
                                                    if(!empty($result['awaiting_table'])){
                                                        echo 'Awaiting ';
                                                        foreach($result['awaiting_table'] as $x){
                                                            if(isset($role_list[$x])){
                                                            echo '<br>'.$role_list[$x];
                                                            }
                                                        }
                                                    }
                                                    ?>
                            </label><br>
                            <?php 
                            } 
                            ?>
                            <label><?=$ci_status_list[$result['status']]?></label>
                            </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                    
                    <?php } //Edit mode end ?>
                </div>
                <!-- /.col-lg-12 -->
                
                
            </div>
            <!-- /.row -->
            
            
            </form>
            
        </div>
        <!-- /#page-wrapper -->
		<script>
		var mode = '<?=$mode?>';
		var ad_hoc = <?=$mode=='Edit'?$result['ad_hoc']:0?>;
		var invoice_item = <?=$mode=='Edit'?$invoice_item:'[]'?>;
		var packing_item = <?=$mode=='Edit'?$packing_item:'[]'?>;
		
		var groupCounter = 0;
		var packingCounter = 0;
		
		var model_tmp = '';
		var del_tmp = '';
		var btn_type = '<?=$btn_type?>';
		
		if(mode == 'Edit'){
			for(var i=0;i<invoice_item.length;i++){
				var no = i+1;
				
				if(ad_hoc==1){
					model_tmp ='<td><input class="form-control" value="'+invoice_item[i].model_no+'" readonly/></td>';
					del_tmp = '<button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="delInvoice('+i+')"><i class="fa fa-times"></i></button>';
				}
				
				html='<tr id="invoice'+i+'"><input type="hidden" id="item_id_in'+i+'" name="item_id_in'+i+'" value="'+invoice_item[i].id+'"><input type="hidden" id="is_del_in'+i+'" name="is_del_in'+i+'" value="0"><td>'+no+'<input name="product_id'+i+'" type="hidden" value="'+invoice_item[i].product_id+'"></td>'+model_tmp+'<td><input name="uom'+i+'" class="form-control" value="'+invoice_item[i].uom+'" readonly></td><td><input name="quantity'+i+'" class="form-control" value="'+invoice_item[i].quantity+'" readonly></td><td><textarea name="description'+i+'" class="form-control" readonly>'+invoice_item[i].Item_description+'</textarea></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" name="unitPrice'+i+'" class="form-control" value="'+invoice_item[i].unit_price+'" readonly></div></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" class="form-control" name="extendPrice'+i+'" value="'+invoice_item[i].extended_price+'" readonly></div></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" class="form-control" id="total_price'+i+'" name="total_price'+i+'" value="'+invoice_item[i].total_price+'" readonly></div></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button>'+del_tmp+'</td></tr>';	
						
				$('#invoice_item').append(html);
				$('#invoiceCount').val(i);
				groupCounter = i+1;
				
				
			}
			
			for(var i=0;i<packing_item.length;i++){
				var no = i+1;	
				
				if(ad_hoc==1){
					model_tmp ='<td><input class="form-control" value="'+invoice_item[i].model_no+'" readonly/></td>';
					del_tmp = '<button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="delPacking('+i+')"><i class="fa fa-times"></i></button>';
				}
				
				html='<tr id="packing'+i+'"><input type="hidden" id="item_id_pack'+i+'" name="item_id_pack'+i+'" value="'+packing_item[i].id+'"><input type="hidden" id="is_del_pack'+i+'" name="is_del_pack'+i+'" value="0"><td>'+no+'<input name="packing_item_id'+i+'" type="hidden" value="'+packing_item[i].id+'"><input name="product_id_packing'+i+'" type="hidden" value="'+packing_item[i].product_id+'"></td>'+model_tmp+'<td><input name="quantity_packing'+i+'" class="form-control" value="'+packing_item[i].quantity+'" readonly></td><td><input name="warehouse'+i+'" class="form-control" value="Singapore" readonly></td><td><input class="form-control" value="'+packing_item[i].quantity_bo+'" name="quantity_bo'+i+'" readonly></td><td><input class="form-control" value="'+packing_item[i].quantity_delivered+'" name="quantity_delivered'+i+'" readonly></td><td><textarea class="form-control" name="part_description'+i+'">'+packing_item[i].parts_description+'</textarea></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button>'+del_tmp+'</td></tr>';	
						
				$('#packing_list_item').append(html);
				$('#packingCount').val(i);
				packingCounter = i+1;
			}
			
			//change input to read only
					if(btn_type != 'create' && btn_type != 'edit' && btn_type != 'sendmail_update'){
						
						$(".form-control").each(function() {
						  $(this).prop('disabled', 'disabled');
						});
						
						$(".btn-circle").each(function() {
						  $(this).hide();
						});
						$('#addProduct').hide();
						$('#addService').hide();
						if(btn_type == 'update' || btn_type == 'approval_update' ){
							
							var target_colum = <?=isset($target_colum)?$target_colum:'[]'?>;
							
							for(var i=0;i<target_colum.length;i++){
								var colum = target_colum[i].name;
								if(target_colum[i].type == 'single'){
									$("input[name='"+colum+"']").prop('disabled', false);
									
								}else if(target_colum[i].type == 'multiple'){
									$("."+colum).prop('disabled', false);
								}
							}
							
							
						}
					}
		}
		
		$( "#search" ).blur(function(){
			ajax_quotation(1);
		});
		
		$('#myModal').on('show.bs.modal', function (event) {
			ajax_quotation(1);
		})
		
		function changePage(i){
			ajax_quotation(i);
		}
		
        function ajax_quotation(i){
			keyword = $("#search").val();
			term = {keyword:keyword,limit:10,page:i};
			url = '<?=base_url($init['langu'].'/anexus/logistics/getQuotation')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					//console.log(r);
					tmp = '';
					
					if(r.data != false){
					for(var i=0;i<r.data.length;i++){
						
						tmp+='<tr><td><a href="javascript:getQuotation('+r.data[i].id+')">'+r.data[i].customer_name+'</a></td><td>'+r.data[i].qr_serial+'</td><td>'+r.data[i].modified_date+'</td><td>'+r.data[i].status+'</td></tr>';
					
					}
					
					html = '<thead><tr><th>Company Name</th><th>Reference No.</th><th>Date Last Updated</th><th>Status</th></tr></thead><tbody>'+tmp+'</tbody>';
					
					pagination = r.paging;
					
					$('#quotation_table').html('');
					$('#pagination').html('');
					$('#quotation_table').append(html);
					$('#pagination').append(pagination);
					}else{
						$('#quotation_table').html('No search result');
						$('#pagination').html('');
					}
					
				}
			});	
		}
		
		function getQuotation(id){
			var dateRequisition = '';
			var html = '';
			var temp_item = '';
			var company_name = '';
			var pic = '';
			
			$('#myModal').modal('hide');
			
			term = {id:id};
			url = '<?=base_url($init['langu'].'/anexus/logistics/getQuotationData')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					console.log(r);
					
					$('#quotation_id').val(r.data.id);
					$('#sale_person_id').val(r.data.create_user_id);
					
					//invoice info
					$('#S_Order_Ref').val();
					$('#Originator').val(r.invoiceInfo.Originator);
					$('#S_Order_No').val(r.invoiceInfo.SalesOrderNo);
					$('#DeliveryOrderNo').val(r.invoiceInfo.DeliveryOrderNo);
					$('#Currency').val(r.customer_data.currency);
					$('#TermsofPayment').val(r.invoiceInfo.TermsofPayment);
					$('#PurchaseOrderNo').val(r.invoiceInfo.PurchaseOrderNo);
					
					//customer info
					$('#invoiceTo').val(r.customer_data.company_name);
					$('#accountNo').val(r.customer_data.account_no);
					$('#DeliveredTo').val(r.customer_data.delivery_address);
					$('#AttentionTo').val(r.customer_data.primary_attention_to);
					$('#Contact').val(r.customer_data.primary_contact_info);
					$('#AttentionTo2').val(r.customer_data.secondary_attention_to);
					$('#Contact2').val(r.customer_data.secondary_contact_info);
					$('#customer_id').val(r.data.customer_id);
					$('#customer_name').val(r.data.customer_name);
					
					//become read only
					$('#S_Order_Ref').attr('readonly', true);
					$('#Originator').attr('readonly', true);
					$('#S_Order_No').attr('readonly', true);
					$('#DeliveryOrderNo').attr('readonly', true);
					$('#Currency').attr('readonly', true);
					$('#TermsofPayment').attr('readonly', true);
					$('#PurchaseOrderNo').attr('readonly', true);
					$('#invoiceTo').attr('readonly', true);
					$('#accountNo').attr('readonly', true);
					$('#sub_total').attr('readonly', true);
					$('#gst').attr('readonly', true);
					$('#grand_total').attr('readonly', true);
					//hide add btn
					$('.add-btn').hide();
					$('#customer_sGroup').hide();
					$('#ad_hoc').val(0);
					
					//change thead
					var invoice_head = '<th>#</th><th width="15%">UOM</th><th>Quantity</th><th width="20%">Item Description</th><th>Unit Price</th><th>Extended Price</th><th>Total Price</th><th width="80px">Action</th>';
					
					var packing_head = '<th>#</th><th>Quantity</th><th>WH</th><th>Qty B/O</th><th>Qty Delivered</th><th>Parts Description</th><th width="80px">Action</th>';
					
					$('#invoice_head').html('');
					$('#invoice_head').append(invoice_head);
					$('#packing_head').html('');
					$('#packing_head').append(packing_head);
					
					//total amount
					var sub_total = parseInt(r.data.total_price);
					var gst_value =<?=$gst['value']?>;
					var gst = Math.round((r.data.total_price*(gst_value/1000))*100)/100;
					var grand_total = (sub_total+gst);
					
					$('#sub_total').val(sub_total);
					$('#gst').val(gst);
					$('#grand_total').val(grand_total);
					
					//invoice item
					$('#invoice_item').html('');
					for(var i=0;i<r.qutation_related_item.length;i++){
						unitPrice = r.qutation_related_item[i].unit_price;
						quantity = r.qutation_related_item[i].quantity;
						extendPrice = r.qutation_related_item[i].extended_price;
						totalPrice = (unitPrice*quantity)+parseInt(extendPrice);
						
						if(extendPrice == 0){
							extendPrice='-';	
						}
						var no = i+1;
						html='<tr><input type="hidden" id="item_id_in'+i+'" name="item_id_in'+i+'" value="0"><input type="hidden" id="is_del_in'+i+'" name="is_del_in'+i+'" value="0"><td>'+no+'<input name="product_id'+i+'" type="hidden" value="'+r.qutation_related_item[i].product_id+'"></td><td><input name="uom'+i+'" class="form-control" value="'+r.qutation_related_item[i].uom+'" readonly></td><td><input name="quantity'+i+'" class="form-control" value="'+quantity+'" readonly></td><td><textarea name="description'+i+'" class="form-control" readonly>'+r.qutation_related_item[i].description+'</textarea></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" name="unitPrice'+i+'" class="form-control" value="'+unitPrice+'" readonly></div></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" class="form-control" name="extendPrice'+i+'" value="'+extendPrice+'" readonly></div></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" class="form-control" id="total_price'+i+'" name="total_price'+i+'" value="'+totalPrice+'" readonly></div></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></td></tr>';	
						
						$('#invoice_item').append(html);
						$('#invoiceCount').val(i);
					}
					
					//packing list item
					$('#packing_list_item').html('');
					for(var i=0;i<r.packing_list_item.length;i++){
						var no = i+1;
						html='<tr><input type="hidden" id="item_id_pack'+i+'" name="item_id_pack'+i+'" value="0"><input type="hidden" id="is_del_pack'+i+'" name="is_del_pack'+i+'" value="0"><td>'+no+'<input name="packing_item_id'+i+'" type="hidden" value="0"><input name="product_id_packing'+i+'" type="hidden" value="'+r.packing_list_item[i].product_id+'"></td><td><input name="quantity_packing'+i+'" class="form-control" value="'+r.packing_list_item[i].quantity+'" readonly></td><td><input name="warehouse'+i+'" class="form-control" value="Singapore" readonly></td><td><input class="form-control" value="'+(r.packing_list_item[i].quantity-r.packing_list_item[i].delivered_qty)+'" name="quantity_bo'+i+'" readonly></td><td><input class="form-control" value="'+r.packing_list_item[i].delivered_qty+'" name="quantity_delivered'+i+'" readonly></td><td><textarea class="form-control" name="part_description'+i+'"></textarea></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></td></tr>';	
						
						$('#packing_list_item').append(html);
						$('#packingCount').val(i);
					}
					
					
				}
			});	
			
		}
		
		$( "#issue_date" ).datepicker({
		dateFormat:'dd/mm/yy',
		});
		
		$( "#payment_date" ).datepicker({
		dateFormat:'dd/mm/yy',
		});
		
		//set datepicker get current date
		if(mode == 'Add'){
			 $('#issue_date').datepicker('setDate', 'today');
			 $('#payment_date').datepicker('setDate', 'today');
		}
		
		
		//add new product or service
		var product =<?=isset($product)?$product:'[]'?>;
		var service =<?=isset($service)?$service:'[]'?>;
		
		
		function addGroup(type){
		
		var html='';
		var packing_html='';
		
		var no = groupCounter+1;
		var packing_no = packingCounter+1;
		
		if(type == 'product'){
			
			tmp_product='<option value="none">select</option>';
			for(i=0;i<product.length;i++){
				tmp_product+='<option value='+product[i].id+'>'+product[i].model_no+'</option>';
			}
			
			
			html='<tr id="invoice'+groupCounter+'"><input type="hidden" id="item_id_in'+groupCounter+'" name="item_id_in'+groupCounter+'" value="0"><input type="hidden" id="is_del_in'+groupCounter+'" name="is_del_in'+groupCounter+'" value="0"><td>'+no+'</td><td><select onchange="getDetail('+groupCounter+',1)" id="product_id'+groupCounter+'" name="product_id'+groupCounter+'" class="form-control">'+tmp_product+'</select></td><td><input id="uom'+groupCounter+'" name="uom'+groupCounter+'" class="form-control" value="" readonly></td><td><select onchange="getTotalPrice('+groupCounter+',1)" id="quantity'+groupCounter+'" name="quantity'+groupCounter+'" class="form-control" value="" readonly></select></td><td><textarea id="description'+groupCounter+'" name="description'+groupCounter+'" class="form-control" readonly></textarea></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" id="unitPrice'+groupCounter+'" name="unitPrice'+groupCounter+'" class="form-control" value="" readonly></div></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" class="form-control" id="extendPrice'+groupCounter+'" name="extendPrice'+groupCounter+'" value="" readonly></div></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" class="form-control" id="total_price'+groupCounter+'" name="total_price'+groupCounter+'" value="" readonly></div></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="delInvoice('+groupCounter+')"><i class="fa fa-times"></i></button></td></tr>';	
			
			packing_html='<tr id="packing'+packingCounter+'"><input type="hidden" id="item_id_pack'+packingCounter+'" name="item_id_pack'+packingCounter+'" value="0"><input type="hidden" id="is_del_pack'+packingCounter+'" name="is_del_pack'+packingCounter+'" value="0"><td>'+packing_no+'<input name="packing_item_id'+packingCounter+'" type="hidden" value="0"><input id="product_id_packing'+groupCounter+'" name="product_id_packing'+packingCounter+'" type="hidden" value="0"></td><td><input class="form-control" id="product_model_packing'+groupCounter+'" readonly></td><td><input id="quantity_packing'+groupCounter+'" name="quantity_packing'+packingCounter+'" class="form-control" value="" readonly></td><td><input name="warehouse'+packingCounter+'" class="form-control" value="Singapore" readonly></td><td><input class="form-control" value="" name="quantity_bo'+packingCounter+'" ></td><td><input class="form-control" value="" name="quantity_delivered'+packingCounter+'" ></td><td><textarea class="form-control" name="part_description'+packingCounter+'"></textarea></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="delPacking('+packingCounter+')"><i class="fa fa-times"></i></button></td></tr>';
			
		}else{
			
			tmp_service='<option>select</option>';
			for(i=0;i<service.length;i++){
				tmp_service+='<option value="'+service[i].id+'">'+service[i].model_no+'</option>';
			}
			
			html='<tr id="invoice'+groupCounter+'"><input type="hidden" id="item_id_in'+groupCounter+'" name="item_id_in'+groupCounter+'" value="0"><input type="hidden" id="is_del_in'+groupCounter+'" name="is_del_in'+groupCounter+'" value="0"><td>'+no+'</td><td><select onchange="getDetail('+groupCounter+',2)" id="product_id'+groupCounter+'" name="product_id'+groupCounter+'" class="form-control">'+tmp_service+'</select></td><td><input id="uom'+groupCounter+'" name="uom'+groupCounter+'" class="form-control" value="" readonly></td><td><select onchange="getTotalPrice('+groupCounter+',2)" id="quantity'+groupCounter+'" name="quantity'+groupCounter+'" class="form-control" value="" readonly></select></td><td><textarea id="description'+groupCounter+'" name="description'+groupCounter+'" class="form-control" readonly></textarea></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" id="unitPrice'+groupCounter+'" name="unitPrice'+groupCounter+'" class="form-control" value="" readonly></div></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" class="form-control" id="extendPrice'+groupCounter+'" name="extendPrice'+groupCounter+'" value="" readonly></div></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" class="form-control" id="total_price'+groupCounter+'" name="total_price'+groupCounter+'" value="" readonly></div></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="delInvoice('+groupCounter+')"><i class="fa fa-times"></i></button></td></tr>';
			
		}
		
		if(type == 'product'){
			$('#packing_list_item').append(packing_html);
			$('#packingCount').val(packingCounter);
			packingCounter++;
		}
		
		$('#invoice_item').append(html);
		$('#invoiceCount').val(groupCounter);
		groupCounter++;
		
		
		}
		
		function getDetail(groupCounter,type){
			
			id = $('#product_id'+groupCounter).val();
			
			if(id != 'none'){
			
			term = {id:id};
			url = '<?=base_url($init['langu'].'/anexus/logistics/getItemDetail')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					console.log(r);
					
					var qty_value = 0;
					var qty_temp = '<option value="none">select</option>';
					var uPrice = '';
					var exPrice = '-';
					
					if(type == 1){
						qty_value = r.data.current_stock_qty;
						uPrice = r.data.selling_price;
					}else{
						qty_value = 50;
						uPrice = r.data.unit_price;
						exPrice = r.data.additional_fee;
					}
					
					for(var i=1;i<=qty_value;i++){
						qty_temp += '<option value="'+i+'">'+i+'</option>';
					}
					console.log(qty_temp);
					
					//clear data b4
					$('#total_price'+groupCounter).val('');
					
					//load data
					$('#quantity'+groupCounter).append(qty_temp);
					$('#quantity'+groupCounter).attr('readonly', false);
					$('#quantity'+groupCounter).val('none');
					$('#uom'+groupCounter).val(r.data.uom);
					$('#description'+groupCounter).val(r.data.description);
					$('#unitPrice'+groupCounter).val(uPrice);
					$('#extendPrice'+groupCounter).val(exPrice);
					getSubTotal();
					
				}
			});	
			
			}else{
				//clear data b4
				$('#quantity'+groupCounter).html('');
				$('#quantity'+groupCounter).attr('readonly', true);
				$('#description'+groupCounter).val('');
				$('#unitPrice'+groupCounter).val('');
				$('#extendPrice'+groupCounter).val('');
				$('#total_price'+groupCounter).val('');
				getSubTotal();
				
			}
			

		}
		
		function getTotalPrice(groupCounter,type){
			
			var qty = $('#quantity'+groupCounter).val();
			var unitPrice = $('#unitPrice'+groupCounter).val();
			var extendPrice = $('#extendPrice'+groupCounter).val();
			var total_price = 0;
			
			if(type == 1){
				total_price = parseInt(qty)*parseInt(unitPrice);
			}else{
				total_price = parseInt(qty)*parseInt(unitPrice)+parseInt(extendPrice);
			}
			
			$('#total_price'+groupCounter).val(total_price);
			
			
			var id = $('#product_id'+groupCounter).val();
			var model = $('#product_id'+groupCounter+' option:selected').text();
			
			$('#product_id_packing'+groupCounter).val(id);
			$('#quantity_packing'+groupCounter).val(qty);
			$('#product_model_packing'+groupCounter).val(model);
			
			getSubTotal();
		}
		
		var gst_value = <?=$gst['value']?>;
		
		function getSubTotal(){
			
			var cal_check = $('#auto_caculate').prop('checked');
			
			if(mode != 'Edit' || cal_check != false){
			
			var sub_total = 0;
			var grand_total = 0;
			var gst = 0;
			
			count = $('#invoiceCount').val();
			for(var i=0;i<=count;i++){
				total_tmp = $('#total_price'+i).val();
				if(total_tmp != undefined && total_tmp != ''){
					sub_total += parseInt(total_tmp);
				}
			}
			gst = (sub_total*gst_value)/100;
			grand_total = Math.round((sub_total+gst) * 100) / 100;
			
			$('#sub_total').val(sub_total);
			$('#gst').val(gst);
			$('#grand_total').val(grand_total);
			
			}
		}
		
		function delInvoice(groupCounter){
			$('#invoice'+groupCounter).hide();
			$('#is_del_in'+groupCounter).val(1);
			getSubTotal();
		}
		
		function delPacking(groupCounter){
			$('#packing'+groupCounter).hide();
			$('#is_del_pack'+groupCounter).val(1);
		}
		
		var company_json = <?=isset($company_list)?$company_list:'[]'?>;
		
		for(i=0;i<company_json.length;i++){
			var temp = '';
			temp = $('<option>').attr('value',company_json[i].id+'|'+company_json[i].company_name).html(company_json[i].company_name);
			$('#customer_select').append(temp);
		}
		
		function getCompany(){
			var company = $('#customer_select').val();
			if(company != 'none'){
				var company_tmp = company.split('|');
				$('#customer_id').val(company_tmp[0]);
				$('#customer_name').val(company_tmp[1]);
			}
		}
		
		$("#auto_caculate").click(function() {
 
		   if($("#auto_caculate").prop("checked"))
		   {
			 getSubTotal();
		   }
		});
		
		function sentMail(url){
			location.href=url;
		}
		
        </script>