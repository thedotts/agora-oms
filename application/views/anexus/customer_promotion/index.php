<div id="page-wrapper">               	
        
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><i class="fa fa-star fa-fw"></i> Promotions
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <?=$promotion['json_content']?>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <!-- Nav tabs -->
                                <ul class="nav nav-pills">
                            <?php $count = 0;?>
                            <?php foreach($result as $k => $v){ ?>
                            	<?php if($v['promo_cat'] == 1){?>
                            	<?php if(isset($category_list[$k])){?>
                                	<?php if($count == 0){?>
                                	<li class="active"><a href="#<?=$k?>" data-toggle="tab"><?=$category_list[$k]?></a>
                                	</li>
                                	<?php }else{ ?>
                                    <li><a href="#<?=$k?>" data-toggle="tab"><?=$category_list[$k]?></a>
                                	</li>
                                    <?php } ?>
                                <?php } ?>
                                <?php $count++;?>
                                <?php } ?>
                            <?php } ?>
                            </ul>
    
                                <!-- Tab panes -->
                                <div class="tab-content">
                                	<?php $count_item = 0;?>
                            		<?php foreach($result as $k => $v){ ?>
                                    <?php if($v['promo_cat'] == 1){?>
                                	<?php if($count_item == 0){?>
                                	<div class="tab-pane fade in active" id="<?=$k?>">
                                	<?php }else{ ?>
                                	<div class="tab-pane fade" id="<?=$k?>">
                                	<?php } ?>
                                        <div class="row">
                                           <div class="col-lg-3 pull-right">
                                                <form role="form">
                                                    <div class="form-group">
                                                        <input class="form-control" id="searchWord<?=$k?>" onblur="searchTab(<?=$k?>)" placeholder="Search">
                                                    </div>
                                                    <!-- /.form-group -->
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.col-lg-3 -->
                                        <div class="panel panel-default hidden-xs">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Product</th>
                                                            <th>Country</th>
                                                            <th>R. Price</th>
                                                            <th>Promo</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tabContent<?=$k?>">
                                                    <?php $no=0;?>
                                                    <?php foreach($v['data'] as $k2 => $v2){ ?>
                                                    <?php if($v2['promo'] == 1){?>
                                                        <tr>
                                                            <td><?=$no+1?></td>
                                                            <td><a href="
                                                            <?php if(!in_array(7,$userdata['role_id'])){
																echo base_url('en/agora/administrator/products_edit').'/'.$v2['id'];
															}?>
                                                            "><?=$v2['product_name']?></a></td>
                                                            <td><?=$v2['model_name']?></td>
                                                            <td><h6><strike>$<?=number_format($v2['selling_price'],2)?></strike></h6></td>
                                                            <td><h4 class="text-danger">$<?=number_format($v2['promo_price'],2)?></h4><span> per <?=$v2['uom']?></span></td>
                                                            <td>
                                                            <?php if(in_array(7,$userdata['role_id'])){?>
                                                                <!-- Button trigger modal -->
                                                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" data-productId="<?=$v2['id']?>" data-productName="<?=$v2['product_name']?>" data-modelName="<?=$v2['model_name']?>" data-price="<?=$v2['promo_price']?>" data-uom="<?=$v2['uom']?>">
                                                                    <i class="fa fa-shopping-cart"></i> Add To Cart
                                                                </button>
                                                                <?php }?>
                                                             </td>
                                                        </tr>
                                                        <?php $no++;?>
                                                        <?php $count_item++;?>
                                                        <?php } ?>
                                    					<?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.table-responsive -->
                                    </div>
                                    
                                    <!-- Data list (Mobile) -->
                        	<div class="mobile-list visible-xs">
                        	
                        		<table class="table table-striped table-bordered table-hover">
                                <tbody id="tabContent_m<?=$k?>">
								<?php $no=0;?>
                                <?php foreach($v['data'] as $k2 => $v2){ ?>
                                <?php if($v2['promo'] == 1){?>
                            	<tr>
                                	<td>
                                    	<div class="row mobile-list-header">
                                            <div class="col-xs-12"><strong>#<?=$no+1?></strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Product:</strong></div>
                                            <div class="col-xs-8"><?=$v2['product_name']?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Country:</strong></div>
                                            <div class="col-xs-8"><?=$v2['model_name']?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>R. Price:</strong></div>
                                            <div class="col-xs-8"><h6><strike>$<?=number_format($v2['selling_price'],2)?></strike></h6></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Promo:</strong></div>
                                            <div class="col-xs-8"><h4 class="text-danger">$<?=number_format($v2['promo_price'],2)?></h4></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Action:</strong></div>
                                            <div class="col-xs-8">
                                            	<?php if(in_array(7,$userdata['role_id'])){?>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" data-productId="<?=$v2['id']?>" data-productName="<?=$v2['product_name']?>" data-modelName="<?=$v2['model_name']?>" data-price="<?=$v2['promo_price']?>" data-uom="<?=$v2['uom']?>">
                                                                    <i class="fa fa-shopping-cart"></i> Add To Cart
                                                                </button>
                                                             <?php }?>
                                                </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php $no++;?>
                                <?php $count_item++;?>
                                <?php } ?>
                                <?php } ?>
                                </tbody>
                            </table>

                                                       
                        </div>
                                    
                                    <!-- /.panel -->
                                     <p>* Valid While Stocks Last</p>
                                    
                                    </div>
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                        
                    </div>
                    <!-- /.col-lg-12 -->
                    
                </div>
                <!-- /.row -->
                
        	</div>            
            <!-- /#page-wrapper -->


 <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<h4 class="modal-title" id="myModalLabel">Add to Cart</h4>
</div>
<div class="modal-body">
<table class="table table-striped table-bordered table-hover" id="dataTables">
<thead>
<tr>
<th>Quantity</th>
</tr>
</thead>
<tbody>
<tr>
<td>
<div class="form-group input-group">
<input id="p_id" type="hidden" class="form-control" value=""> 
<input id="p_name" type="hidden" class="form-control" value=""> 
<input id="model_name" type="hidden" class="form-control" value=""> 
<input id="price" type="hidden" class="form-control" value=""> 
<input id="qty" class="form-control" value=""> 
<span class="input-group-addon" id="uom"> Carton(s)</span>
<input type="hidden" id="uom_val" class="form-control" value=""> 
</div>
</td>
</tr>
</tbody>
</table>
</div>
<div class="modal-footer">
<button type="submit" class="btn btn-primary" onclick="modal_addItem()">Add</button>
<button type="cancel" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script>
function formatNumber(number)
{	
	var number = parseFloat(number);
    number = number.toFixed(2) + '';
    var x = number.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

$('#myModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var id = button.data('productid')
  var name = button.data('productname')
  var model_name = button.data('modelname')
  var price = button.data('price')
  var uom = button.data('uom')
  
  var modal = $(this)
  modal.find('.modal-title').text('Add to Cart ' + name)
  modal.find('#p_id').val(id)
  modal.find('#p_name').val(name)
  modal.find('#model_name').val(model_name)
  modal.find('#price').val(price)
  modal.find('#uom').text(capitalizeFirstLetter(uom)+'(s)')
  modal.find('#uom_val').val(uom)
})

function searchTab(i){
	
	keyword = $('#searchWord'+i).val();
	
	$.ajax({
	  method: "POST",
	  url: "<?=base_url($init['langu'].'/agora/customer/promotion_search')?>",
	  data: { "category": i, "keyword": keyword },
	  dataType: "JSON"
	})
	.done(function(r) {
		
		if(r.status == 'OK'){

			$('#tabContent'+i).html('');
			$('#tabContent_m'+i).html('');
			
			var html ='';
			var html_m = '';
			
			for(var x=0;x<r.result.length;x++){
				
				if(r.result[x].promo == 1){
					
				html+='<tr><td>'+(x+1)+'</td><td><a href="#">'+r.result[x].product_name+'</a></td><td>'+r.result[x].model_name+'</td><td><h6><strike>$'+formatNumber(r.result[x].selling_price)+'</strike></h6></td><td><h4 class="text-danger">$'+formatNumber(r.result[x].promo_price)+'</h4></td><td><!-- Button trigger modal --><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" data-productId="'+r.result[x].id+'" data-productName="'+r.result[x].product_name+'" data-modelName="'+r.result[x].model_name+'" data-price="'+r.result[x].promo_price+'" data-uom="'+r.result[x].uom+'"><i class="fa fa-shopping-cart"></i> Add To Cart</button></td></tr>';
				
				html_m +='<tr><td><div class="row mobile-list-header"><div class="col-xs-12"><strong>#'+(x+1)+'</strong></div></div><div class="row"><div class="col-xs-4"><strong>Product:</strong></div><div class="col-xs-8">'+r.result[x].product_name+'</div></div><div class="row"><div class="col-xs-4"><strong>Model:</strong></div><div class="col-xs-8">'+r.result[x].model_name+'</div></div><div class="row"><div class="col-xs-4"><strong>R. Price:</strong></div><div class="col-xs-8"><h6><strike>$'+formatNumber(r.result[x].selling_price)+'</strike></h6></div></div><div class="row"><div class="col-xs-4"><strong>Promo:</strong></div><div class="col-xs-8"><h4 class="text-danger">$'+formatNumber(r.result[x].promo_price)+'</h4></div></div><div class="row"><div class="col-xs-4"><strong>Action:</strong></div><div class="col-xs-8"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" data-productId="'+r.result[x].id+'" data-productName="'+r.result[x].product_name+'" data-modelName="'+r.result[x].model_name+'" data-price="'+r.result[x].promo_price+'" data-uom="'+r.result[x].uom+'"><i class="fa fa-shopping-cart"></i> Add To Cart</button></div></div></td></tr>';
				
				}
				
			}
			
			$('#tabContent'+i).append(html);
			$('#tabContent_m'+i).append(html_m);
		}
		
	});
	
}

function modal_addItem(){
	
	var p_id = $('#p_id').val();
	var p_name = $('#p_name').val();
	var qty = $('#qty').val();
	var price = $('#price').val();
	var model_name = $('#model_name').val();
	
	var uom = $('#uom_val').val();

	var check = 1;
	if(uom != 'kg' && qty%1!=0){
		check = 0;
	}
	
	if(check == 1){
	
	$.ajax({
	  method: "POST",
	  url: "<?=base_url($init['langu'].'/agora/customer/promotion_addCart')?>",
	  data: { "p_name": p_name, "p_id": p_id, "qty": qty, "price": price, "model_name": model_name },
	  dataType: "JSON"
	})
	.done(function(r) {
		
		if(r.status == 'OK'){
			$('#cart').html('');
			
			var html = '';
			for(var x=0;x<r.data.length;x++){
				html +='<li><a href="#"><div><strong>'+r.data[x].name+'</strong><span class="pull-right text-muted"><em>Qty: '+r.data[x].qty+'</em></span><div>'+r.data[x].model_name+'</div></div></a></li>';
			}
			url ='';
			
			html+='<li class="divider"></li><li><a class="text-center" href="'+'<?=base_url($init['langu'].'/agora/customer/cart')?>'+'"><strong>Shopping Cart</strong><i class="fa fa-angle-right"></i></a></li>';
			
			$('#cart').append(html);

			$('#myModal').modal('hide');
			alert("Items have been added to Cart");
			$('#qty').val('');
		}
		
	});
	
	}else{
		
		alert('Quantity wrong format');
			
	}

}
var cat_count = <?=count($result)?>;
//enter search
		$(document).ready(function() {
		  $(window).keydown(function(event){
			if(event.keyCode == 13) {
			  event.preventDefault();
			  
			  for(var i=1;i<=3;i++){
				  $('#searchWord'+i).blur();
			  }
			  

			  return false;
			}
		  });
		});

</script>