
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-truck fa-fw"></i>Missing Packing List
                       <div class="col-lg-3 pull-right">
                       		<!--
                       		<form role="form">
                                <div class="form-group">
                                	<input id="q" class="form-control" placeholder="Search" value="" onblur="filter();">
                                </div>
                               
                            </form>
                            -->
                        </div>
                		<!-- /.col-lg-3 -->
                	</h1>
                    
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default hidden-xs">
                            
                            <?php
								if(!empty($unpack_data)) {
								?>
                                <table class="table table-striped table-bordered table-hover">
                                    <tbody>
                                    <?php foreach($unpack_data as $k => $v){
										if(!empty($v['item'])){
										?>
                                    	<tr>
                                    	<th colspan="4"><?=$v['name']?></th>
                                        </tr>
                                        <tr>
                                    	<th>Product Name</th>
                                        <th>Qty</th>
                                        <th>Qty-in-stock</th>
                                        
                                        <?php foreach($v['item'] as $k2 => $v2){?>
                                        	<tr>
                                            <td><?=$product_list[$k2]?></td>
                                            <td><?=$v2?> <?=$v['item_uom'][$k2]?>(s)</td>
                                            <td><?=$v['stock_qty'][$k2]?>  <?=$v['item_inventory_uom'][$k2]?>(s)</td>
                                            </tr>
                                        <?php }?>
                                        <tr>
                                        <th colspan="4"></th>
                                        <tr>
                                        
                                    <?php }} ?>
                                    </tbody>
                                </table>
                                
                                <div>
                                <a class="btn btn-default" href="<?=base_url('en/agora/administrator/unpack_pdf')?>" target="_blank">Download PDF</a>
                                </div>
                                 <?php
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
							?>
                            	
                      </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                    
                    
                    </div>
                    <!-- /.panel -->
                    
                    <!-- Data list (Mobile) -->
                        <div class="mobile-list visible-xs">
                        	<?php
								if(!empty($unpack_data)) {
								?>
                        	<table class="table table-striped table-bordered table-hover">
                            	<?php										
										foreach($unpack_data as $k => $v){
										if(!empty($v['item'])){
									?>
                            	<tr>
                                	<td>
                                    	<div class="row mobile-list-header">
                                            <div class="col-xs-12"><strong></strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Customer:</strong></div>
                                            <div class="col-xs-8"><?=$v['name']?></div>
                                        </div>
                                        <?php foreach($v['item'] as $k2 => $v2){?>
                                        <div class="row">
                                            <div class="col-xs-4"><strong>Product:</strong></div>
                                            <div class="col-xs-4"><?=$product_list[$k2]?></div>
                                            <div class="col-xs-4"><?=$v2?></div>
                                        </div>
                                        <?php }?>
                                    </td>
                                </tr>
                                <?php											
										}}
									?>
                            </table>
                        	<div>
                                <a class="btn btn-default" href="<?=base_url('en/agora/administrator/unpack_pdf')?>" target="_blank">Download PDF</a>
                                </div>
                            <?php
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
								?>
                                                       
                        </div>
                </div>
                    
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
<script>
</script>