<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-truck fa-fw"></i> <?=$head_title?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                       		<!-- Button trigger modal -->
                            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" <?=$mode=='Edit' || $job_id!=0?'disabled':''?>>
                                Seach from Order
                            </button>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Search Orders</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Search</label>
                                                <input class="form-control" placeholder="By Company Name or Order No." id="search" name="search">
                                            </div>
                                            <table id="table" class="table table-striped table-bordered table-hover" id="dataTables">
                                            </table>
                                            <div id="pagination"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <form action="<?=base_url($init['langu'].'/agora/ground_staff/packing_list_submit');?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" id="order_id" name="order_id" value="<?=$mode=='Edit'?$result['order_id']:0?>"/>
            <input type="hidden" id="order_no" name="order_no" value="<?=$mode=='Edit'?$result['order_no']:0?>"/>
            <input type="hidden" id="requestor_id" name="requestor_id" value="<?=$mode=='Edit'?$result['requestor_id']:$staff_info['id']?>"/>
            <input type="hidden" id="item_counter" name="item_counter" value="0"/>
            <input type="hidden" id="job_id" name="job_id" value=""/>
            
            <div class="row">
                
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Packing List Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Packing List No.</label>
                                    <input class="form-control" value="<?=$mode=='Edit'?$result['packing_no']:''?>" disabled>
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Date of Packing List *</label>
                                    <input class="form-control" placeholder="Calendar Input" id="packing_date" name="packing_date" value="<?=$mode=='Edit'?$result['packing_date']:''?>">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Staff Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Staff Name</label>
                                    <input id="staff_name" name="staff_name" class="form-control" value="<?=$mode=='Edit'?$result['staff_name']:$staff_info['full_name']?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Role</label>
                                     <?php 
									 if($mode=='Edit'){
										 
                                    	if(strpos($result['staff_role'],',')){
											$roleId = explode(',',$result['staff_role']);	
										}else{
											$roleId = array($result['staff_role']);	
										}
											
                                    }else{
										$roleId = $userdata['role_id'];
									}
									$role_name = '';
									foreach($roleId as $v){
										$role_name .= $role_list[$v].',';
									}
									
									$role_name = substr($role_name,0,strlen($role_name)-1);
									
									?>
                                    <input class="form-control" value="<?=$role_name?>"disabled>
                                    <input id="staff_role" name="staff_role" type="hidden" value="<?=$mode=='Edit'?$result['staff_role']:$staff_info['role_id']?>" readonly>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Customer Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input id="customer_id" name="customer_id" type="hidden" value="<?=$mode=='Edit'?$customer_info['id']:''?>">
                                    <input id="customer_name" name="customer_name" class="form-control" value="<?=$mode=='Edit'?$customer_info['company_name']:''?>" readonly>
                                </div>
                                <?php if($userdata['role_id'] != 4){?>
                                <div class="form-group">
                                    <label>Attention</label>
                                    <input id="attention" name="attention" class="form-control" value="<?=$mode=='Edit'?$customer_info['primary_attention_to']:''?>" readonly>
                                </div>
                                <?php }?>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input id="address" name="address" class="form-control" value="<?=$mode=='Edit'?$customer_info['address']:''?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Postal Code</label>
                                    <input id="postal" name="postal" class="form-control" value="<?=$mode=='Edit'?$customer_info['postal']:''?>" readonly>
                                </div>
                                <?php if($userdata['role_id'] != 4){?>
                                <div class="form-group">
                                    <label>Country</label>
                                    <input id="country" name="country" class="form-control" value="<?=$mode=='Edit'?$customer_info['country']:''?>" readonly>
                                </div>
                                <div style="display:none">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input id="email" name="email" class="form-control" value="<?=$mode=='Edit'?$customer_info['primary_contact_email']:''?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Contact No.</label>
                                    <input id="contact_no" name="contact_no" class="form-control" value="<?=$mode=='Edit'?$customer_info['primary_contact_info']:''?>" readonly>
                                </div>
                                </div>
                                <?php }?>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
            </div>
            <!-- /.row -->
            <hr />
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Items 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body" style="overflow:auto">
                            <div class="table-responsive">
                                <table class="table table-hover" >
                                    <thead>
                                        <tr>
                                            <th width="5%">Pack *</th>
                                            <th width="15%">Product Name</th>
                                            <th style="display:none" width="15%">Country</th>
                                            <th width="200px">Order Balance</th>
                                            <th width="200px">Qty Packed *</th>
                                            <th width="200px">Inventory Balance</th>
                                            <th width="200px">Inventory Deduction *</th>
                                            <th width="80px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody">
                                    <?php if($mode =='Edit'){?>
                                    <?php foreach($result['product'] as $k => $v){?>
                                    	<tr>
                                        <td>
                                        <input type="checkbox" class="form-control" id="is_checked<?=$k?>" name="is_checked<?=$k?>" checked="checked">
                                        </td>
                                        <td>
                                        <input type="text" class="form-control" value="<?=$product_list[$v['product_id']]?>" id="product_name<?=$k?>" name="product_name<?=$k?>" readonly>
                                        <input type="hidden" value="<?=$v['product_id']?>" id="product_id<?=$k?>" name="product_id<?=$k?>" >
                                        </td>
                                        <td>
                                        <input type="text" class="form-control" value="<?=$v['model_name']?>" id="model_name<?=$k?>" name="model_name<?=$k?>" readonly>
                                        </td>
                                        <td>
                                        <div class="form-group input-group">
                                        <input type="text" class="form-control" value="<?=$v['quantity_order']?>" id="quantity<?=$k?>" name="quantity<?=$k?>" readonly>
                                        <span class="input-group-addon"><?=$v['uom']?>(s)</span>
                                        </div>
                                        </td>
                                        
                                        <td>
                                        <div class="error form-group input-group">
                                        <input type="text" class="form-control" value="<?=$v['quantity_pack']?>" id="quantityPack<?=$k?>" name="quantityPack<?=$k?>">
                                        <span class="input-group-addon"><?=$v['uom']?>(s)</span>
                                        </div>
                                        </td>
                                        
                                        <td>
                                        <div class="error form-group input-group">
                                        <input type="text" class="form-control" id="quantityStock<?=$k?>" name="quantityStock<?=$k?>" value="<?=$v['quantity_stock']?>" readonly>
                                        <span class="input-group-addon"><?=$v['inventory_uom']?>(s)</span>
                                        </div>
                                        </td>
                                        
                                        <td>
                                        <div class="error form-group input-group">
                                        <input type="text" class="form-control" id="inventory_deduct<?=$k?>" name="inventory_deduct<?=$k?>" value="<?=$v['inventory_deduct']?>" readonly>
                                        <span class="input-group-addon"><?=$v['inventory_uom']?>(s)</span>
                                        </div>
                                        </td>
                                        
                                        <td>
                                        <a href="<?=base_url($init['langu'].'/agora/administrator/products_edit/'.$v['product_id'])?>" target="_blank"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></a>
                                        </td>
                                        </tr>
                                    <?php }?>
                                    <?php }?>
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                        
                        
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Remarks
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <textarea id="remarks" name="remarks" class="form-control"><?=$mode=='Edit'?$result['remark']:''?></textarea>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    <div class="panel">
                                
                        <?php if($mode=='Add'){?>
                    
                    	<?php if($btn_type == 'create'){?>
                        <div class="form-group">
                        <button id="btnSubmit" type="submit" class="btn btn-primary" disabled="disabled">Submit</button>
                        <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                        </div>
                    	<?php }?>
                        
                    <?php }else if($mode=='Edit'){?>
                    	<?php switch($btn_type){
							case 'edit':
							?>
                        		<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Submit</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            <?php 
							break;
							case 'approval':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Update</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'approval_update':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'sendmail_update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-default">Save</button>
                                	<a class="btn btn-default" href="<?=base_url('en/agora/sales/tender_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/tender_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'send_email':
							?>
                            	<div class="form-group">
                                	<a class="btn btn-default" href="<?=base_url('en/agora/sales/tender_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/tender_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                </div>
                            
                            <?php 
							break;
							case 'upload':
							?>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="confirm_file" name="confirm_file" type="file">
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                		<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            <?php 
							break;
							case 'complete':
							?>
                            	<?php switch($last_action){
								case 'upload':
								?>

                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="confirm_file" name="confirm_file" type="file"> 
                                            <a target="_blank" href="<?=$result['confirm_file']?>"><?=$result['confirm_file']?></a>
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Reupload</button>
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
								
                                <?php 
								break;
								case 'pdf':
								?>
                                <div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/agora/ground_staff/packing_list_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                            	</div>
                                <?php 
								break;
								}?>
                                
                                
                            <?php 
							break;
                        }?>

                    <div class="panel panel-primary" style="display:none">
                    	<div class="panel-heading">
                            Status
                        </div>
                        <div class="panel-body">
                            <div class="form-group input-group">
							<?php 
                                //如果不是not approved, 也不是 completed 就顯示目前正在等誰
                                if($result['status'] != 0 && $result['status'] != $completed_status_id){
                            ?>
                            <label>
                                    <?php
                                    if(!empty($result['awaiting_table'])){
                                    echo 'Awaiting ';
                                    foreach($result['awaiting_table'] as $x){
                                          if(isset($role_list[$x])){
                                              echo '<br>'.$role_list[$x];
                                          }
                                    }
                                    }
                                    ?>
                            </label><br>
                            <?php 
                            } 
                            ?>
                            <label><?=$status_list[$result['status']]?></label>
                            </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                    
                    <?php } //Edit mode end ?>
                    
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->
        
        
<script>
var mode ='<?=$mode?>';
var status = '<?=isset($result['status'])?$status_list[$result['status']]:''?>';
var requestor_id = <?=isset($result['requestor_id'])?$result['requestor_id']:0?>;
var user_id = <?=isset($userdata['id'])?$userdata['id']:0?>;

$( "#packing_date" ).datepicker({
	dateFormat:'dd/mm/yy',
});

//set datepicker get current date
if(mode == 'Add'){
	$('#packing_date').datepicker('setDate', 'today');
}else{
	if(status == 'Completed' || requestor_id != user_id){
	$(".form-control").each(function() {
		$(this).prop('disabled', 'disabled');
	});
	}	
}

$( "#search" ).keyup(function(){			
	ajax_quotation(1);
});
		
$('#myModal').on('show.bs.modal', function (event) {
	ajax_quotation(1);
})
		
function changePage(i){
	ajax_quotation(i);
}

function ajax_quotation(i){
			
	keyword = $("#search").val();
	term = {keyword:keyword,limit:10,page:i};
	url = '<?=base_url($init['langu'].'/agora/ground_staff/getSearch')?>';
			
	$.ajax({
		type:"POST",
		url: url,
		data: term,
		dataType : "json"
		}).done(function(r) {
			if(r.status == "OK") {
				tmp = '';
					
				if(r.data != false){
					for(var i=0;i<r.data.length;i++){
						
						tmp+='<tr><td>'+r.data[i].customer_name+'</td><td><a href="javascript:getSearchData('+r.data[i].id+',1)">'+r.data[i].order_no+'</a></td><td>'+r.data[i].modified_date+'</td><td>'+r.data[i].status+'</td></tr>';
					
					}
					
					html = '<thead><tr><th>Company Name</th><th>Order No.</th><th>Date Last Updated</th><th>Status</th></tr></thead><tbody>'+tmp+'</tbody>';
					
					pagination = r.paging;
					
					$('#table').html('');
					$('#pagination').html('');
					$('#table').append(html);
					$('#pagination').append(pagination);
					
				}else{
					
					$('#table').html('No search result');
						$('#pagination').html('');
					}
					
				}
	});	
	
}


function getSearchData(id,mode){
			
	if(mode == 1){
		$('#myModal').modal('hide');
	}
			
	term = {id:id};
	url = '<?=base_url($init['langu'].'/agora/ground_staff/getSearchData')?>';
			
		$.ajax({
		  type:"POST",
		  url: url,
		  data: term,
		  dataType : "json"
		}).done(function(r) {
			if(r.status == "OK") {
				console.log(r);

				$('#order_id').val(r.data.id);
				$('#order_no').val(r.data.order_no);
				$('#customer_id').val(r.data.customer_id);
				$('#customer_name').val(r.data.customer_name);
				$('#attention').val(r.data.attention);
				$('#address').val(r.data.address);
				$('#postal').val(r.data.postal);
				$('#country').val(r.data.country);
				$('#email').val(r.data.email);
				$('#contact_no').val(r.data.contact_no);
				$('#job_id').val(r.data.latest_job_id);
					
				$('#tbody').html('');
				
				var html = '';
				for(var x=0;x<r.product.length;x++){
					
					//if packed qty < order qty
					if((r.product[x].quantity-r.product[x].packed) > 0){
					
					var error = '';
					var order_bal = r.product[x].quantity-r.product[x].packed;
					var d_pack = order_bal;
					
					//if quantity < stock quantity add red line
					if(r.product[x].stock_qty < order_bal){
						error = 'has-error';
					}
					
					//set default qty packed
					if(r.product[x].stock_qty < order_bal){
						
						//number not negative
						if(r.product[x].stock_qty > 0){
							d_pack = r.product[x].stock_qty;
						}else{
							d_pack = 0;
						}
						
						
					}
					
					html+='<tr><td><input type="checkbox" class="form-control" id="is_checked'+x+'" name="is_checked'+x+'"></td><td><input type="text" class="form-control" value="'+r.product[x].product_name+'" id="product_name'+x+'" name="product_name'+x+'" readonly><input type="hidden" value="'+r.product[x].product_id+'" id="product_id'+x+'" name="product_id'+x+'" ><input type="hidden" value="'+r.product[x].price+'" id="price'+x+'" name="price'+x+'" ></td><td style="display:none"><input type="text" class="form-control" value="'+r.product[x].model_name+'" id="model_name'+x+'" name="model_name'+x+'" readonly></td><td><div class="form-group input-group"><input type="text" class="form-control order-balance" value="'+order_bal+'" id="quantity'+x+'" name="quantity'+x+'" readonly><span class="input-group-addon"> '+r.product[x].uom+'(s)</span></div></td><td><div class="form-group input-group"><input type="text" class="form-control quantity" value="'+d_pack+'" id="quantityPack'+x+'" name="quantityPack'+x+'"><span class="input-group-addon"> '+r.product[x].uom+'(s)</span><input type="hidden" name="uom[]" value="'+r.product[x].uom+'"></div></td><td><div class="'+error+' form-group input-group"><input type="text" class="form-control" id="quantityStock'+x+'" name="quantityStock'+x+'" value="'+r.product[x].stock_qty+'" readonly><span class="input-group-addon"> '+r.product[x].inventory_uom+'(s)</span></div></div></td><td><div class="form-group input-group"><input type="text" class="form-control deduct_qty" value="0" id="inventory_deduct'+x+'" name="inventory_deduct'+x+'"><span class="input-group-addon"> '+r.product[x].inventory_uom+'(s)</span><input type="hidden" name="inventory_uom[]" value="'+r.product[x].inventory_uom+'"></div></td><td><a href="'+'<?=base_url($init['langu'].'/agora/administrator/products_edit/')?>'+'/'+r.product[x].product_id+'" target="_blank"><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></td></a></tr>';
					}
				}
				
				$('#item_counter').val(r.product.length);
				
				$('#tbody').append(html);
				
				
				//if checkbox no checked cant submit
				$( "input[type='checkbox']" ).click(function() {
					var check = 0;
				 	$( "input[type='checkbox']" ).each(function() {
						
					  	if(this.checked){
							check =1;
							return false;
						}
					});
					
					if(check == 1){
						$('#btnSubmit').attr('disabled',false);
					}else{
						$('#btnSubmit').attr('disabled',true);	
					}
					
				});
			}
		});	
			
}

$('#btnSubmit').click(function(){
				
				
		var check = 0;
		$( "input[type='checkbox']" ).each(function() {
						
			if(this.checked){
				var quantity_input = $(this).closest('tr').find('.quantity').val();
				var uom = $(this).closest('tr').find('input[name="uom[]"]').val();
				var order_balance = $(this).closest('tr').find('.order-balance').val();
				//alert(quantity_input);
				if(quantity_input <= 0 || quantity_input ==''){
					check =1;
					alert('Quantity cant be zero,negative or blank');
					$(this).closest('tr').find('.quantity').focus();
					return false;
				}
				
				if(quantity_input %1!=0 && uom !='kg'){
					check =1;
					alert('Quantity wrong format');
					$(this).closest('tr').find('.quantity').focus();
					return false;
				}

				if(parseFloat(quantity_input) > parseFloat(order_balance)){
					
					check =1;
					alert('Quantity packed can\'t more than order balance');
					$(this).closest('tr').find('.quantity').focus();
					return false;
				}
				
				
				var uom = $(this).closest('tr').find('input[name="inventory_uom[]"]').val();
				var quantity_input = $(this).closest('tr').find('.deduct_qty').val();
				if(quantity_input <= 0 || quantity_input ==''){
					check =1;
					alert('Inventory Deduction cant be zero,negative or blank');
					$(this).closest('tr').find('.deduct_qty').focus();
					return false;
				}
				
				if(quantity_input %1!=0&&uom !='kg'){
					check =1;
					alert('Inventory Deduction wrong format');
					$(this).closest('tr').find('.quantity').focus();
					return false;
				}
				
				
			}
		});
					
		if(check == 1){
			event.preventDefault();
			//alert('Quantity & Inventory Deduction cant be zero,negative or blank');
		}

});

var order_id =<?=isset($order_id)?$order_id:0?>;
		
if(order_id != 0){
	getSearchData(order_id,2);
}
</script>