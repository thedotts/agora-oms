<div id="page-wrapper">
        	<form action="<?=base_url($init['langu'].'/anexus/sales/quotation_submit');?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-briefcase fa-fw"></i> <?=$head_title?>
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Quotation Request Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>QR Serial No.</label>
                                    <input class="form-control"  value="<?=$mode=='Edit'?$result['qr_serial']:''?>" disabled>
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Date Received</label>
                                    <input id="received_date" name="received_date" class="form-control" placeholder="Calendar Input" value="<?=$mode=='Edit'?$result['received_date']:''?>">
                                </div>
                                <?php
								if($userdata['role_id']==1) {
								?>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status" class="form-control">
                                    	<?php
										foreach($quotation_status_list as $k=>$v) {
										?>
                                        <option value="<?=$k?>" <?=$mode=='Edit'&&$k==$result['status']?'selected="selected"':''?>><?=$v?></option>
                                        <?php	
										}
										?>
                                    </select>
                                </div>
                                <?php	
								}
								?>
                                
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Staff Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Requestor Name</label>
                                    <input class="form-control" value="<?=$mode=='Edit'?$result['requestor_name']:isset($staff_info['full_name'])?$staff_info['full_name']:''?>" disabled>
                                    <input type="hidden" id="requestor_id" name="requestor_id" value="<?=$mode=='Edit'?$result['requestor_id']:isset($staff_info['id'])?$staff_info['id']:''?>"/>
                                </div>
                                <div class="form-group">
                                    <label>Date of Request</label>
                                    <input id="request_date" name="request_date" class="form-control" placeholder="Calendar Input" value="<?=$mode=='Edit'?$result['request_date']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Department</label>
                                    <input class="form-control" value="<?=$mode=='Edit'?$result['requestor_department_name']:isset($staff_info['department_name'])?$staff_info['department_name']:''?>" disabled>
                                    <input type="hidden" id="department_id" name="department_id" value="<?=$mode=='Edit'?$result['department_id']:isset($staff_info['department_id'])?$staff_info['department_id']:''?>"/>
                                </div>
                                <div class="form-group">
                                    <label>Requestor To</label>
                                    <input id="requestor_to" name="requestor_to" class="form-control" value="<?=$mode=='Edit'?$result['requestor_to']:(isset($manager_data)&&!empty($manager_data)?$manager_data['name']:'')?>" readonly="readonly">
                                    <input type="hidden" name="requestor_to_roleid" value="<?=$mode=='Edit'?$result['requestor_to_roleid']:(isset($manager_data)&&!empty($manager_data)?$manager_data['id']:'')?>"/>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Customer Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Company Name *</label>
                                    <select id="company" name="company" class="form-control">
                                    <option value="none">Select</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Attention</label>
                                    <input id="attention" class="form-control" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input id="address" class="form-control" value="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Postal Code</label>
                                    <input id="postal" class="form-control" value="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input id="email" class="form-control" value="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Contact</label>
                                    <input id="contact" class="form-control" value="" disabled>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Shipper Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Shipper</label>
                                    <select id="shipper" name="shipper" class="form-control">
                                        <option>Select</option>
                                        <?php foreach($shipping_list as $k => $v){ ?>
                                        	<option value="<?=$v['id']?>" <?=$mode=='Edit'?($v['id']==$result['shipper']?'selected':''):''?>><?=$v['shipper_name']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Est. Shipping</label>
                                    <div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input id="shippingFee" name="est_shipping" type="text" class="form-control" value="<?=$mode=='Edit'?$result['est_shipping']:''?>"></div>
                                    <?php if($mode=='Edit'){?>
                                    <div class="form-group">
                                    <label class="checkbox-inline">
                                    <input id="auto_est_shipping" name="auto_est_shipping" type="checkbox"> Automatic Est Shipping Fee
                                    </label>
                                	</div>
                                    <?php }?>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
            </div>
            <!-- /.row -->
            
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Items 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                            	<button id="addProduct" class="btn btn-default btn-sm" type="button" onclick="addGroup('product')">Add New Product Item</button>
                            	<button id="addService" class="btn btn-default btn-sm" type="button" onclick="addGroup('service')">Add New Service Item</button>
                                <input id="groupCount" name="groupCount" type="hidden" value="0"/>
                                <input id="totalPrice" name="totalPrice" type="hidden" value="<?=$mode=='Edit'?$result['total_price']:''?>"/>
                                <input id="product_total" name="product_total" type="hidden" value="0"/>
                                <input id="service_total" name="service_total" type="hidden" value="0"/>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Principal/Supplier</th>
                                            <th width="20%">Model No.</th>
                                            <th width="20%">Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Extended Price</th>
                                            <th width="80px">Action</th>
                                        </tr>
                                    </thead>
                                   <tbody id="tbody">

                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                        
                        
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Remarks
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <textarea id="qr_remarks" name="qr_remarks" class="form-control"><?=$mode=='Edit'?$result['qr_remarks']:''?></textarea>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
					<?php if($mode=='Add'){?>
                    
                    	<?php if($btn_type == 'create'){?>
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                        </div>
                    	<?php }?>
                        
                    <?php }else if($mode=='Edit'){?>
                    
                    	<?php switch($btn_type){
							case 'edit':
							?>
                        		<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Submit</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            <?php 
							break;
							case 'approval':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Update</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'approval_update':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'send_email':
							?>
                            	<div class="form-group">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/sales/quotation_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/quotation_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                </div>
                            
                            <?php 
							break;
							case 'upload':
							?>
                            
                            	<div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/sales/quotation_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/quotation_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file">
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                		<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            
                            <?php 
							break;
							case 'complete':
							?>
                            	<?php if($last_action == 'upload'){?>
                            	<div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/sales/quotation_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/sales/quotation_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file"> 
                                            <a target="_blank" href="<?=$result['comfirm_file']?>"><?=$result['comfirm_file']?></a>
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Reupload</button>
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                                <?php }?>
                            
                            <?php 
							break;
                        }?>
                        

                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Status
                        </div>
                        <div class="panel-body">
                            <div class="form-group input-group">
							<?php 
                                //如果不是not approved, 也不是 completed 就顯示目前正在等誰
                                if($result['status'] != 0 && $result['status'] != $completed_status_id){
                            ?>
                            <label>
                                                    <?php
                                                    if(!empty($result['awaiting_table'])){
                                                        echo 'Awaiting ';
                                                        foreach($result['awaiting_table'] as $x){
                                                            if(isset($role_list[$x])){
                                                            echo '<br>'.$role_list[$x];
                                                            }
                                                        }
                                                    }
                                                    ?>
                            </label><br>
                            <?php 
                            } 
                            ?>
                            <label><?=$quotation_status_list[$result['status']]?></label>
                            </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                    
                    <?php } //Edit mode end ?>

                    

                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->

<script>
var company_json = <?=$company_list?>;
var groupJson =<?=isset($result['product'])?$result['product']:'[]'?>;
var mode = '<?=$mode?>';

var shipping_fee =<?=$mode=='Edit'?$result['est_shipping']:0?>;

var status = <?=isset($result['status'])?$result['status']:0?>;
var role = <?=$userdata['role_id']?>;




$(document).ready(function(){
	//get all company list
	for(i=0;i<company_json.length;i++){
		var temp = '';
		temp = $('<option>').attr('value',company_json[i].id+'|'+company_json[i].company_name).html(company_json[i].company_name);
		$('#company').append(temp);
	}
	//onload get selected company data
	var company_id = '<?=$mode=='Edit'?$result['customer_id'].'|'.$result['customer_name']:0?>';
	if(company_id != 0){
		$('#company').val(company_id);
	}
	var company_id = $('#company').val();
	get_companyItem(company_id);
	
	$( "#received_date" ).datepicker({
		dateFormat:'dd/mm/yy',
	});
	
	$( "#request_date" ).datepicker({
		dateFormat:'dd/mm/yy',
	});
	
	//set datepicker get current date
	if(mode == 'Add'){
		 $('#received_date').datepicker('setDate', 'today');
		 $('#request_date').datepicker('setDate', 'today');
	}
	
	var typeItem ='';
	//console.log(groupJson.length);
	if(mode=='Edit'){
		
		for(var i=0;i<groupJson.length;i++){
			if(groupJson[i].type == 1){
				typeItem = 'product';
			}else{
				typeItem = 'service';
			}
			addGroup(typeItem);

		}	
	} 

});

$('#company').change(function() {
	var id = $(this).val();
	get_companyItem(id);	
});

function get_companyItem(id){
	
	value = id.split("|");
	id = value[0];
	
	if(id == 'none'){
		$('#attention').val('');
		$('#address').val('');
		$('#postal').val('');
		$('#email').val('');
		$('#contact').val('');
	}else{
		for(i=0;i<company_json.length;i++){
		
			if(company_json[i].id == id){
					
				$('#attention').val(company_json[i].primary_attention_to);
				$('#address').val(company_json[i].address);
				$('#postal').val(company_json[i].postal);
				$('#email').val(company_json[i].email);
				$('#contact').val(company_json[i].primary_contact_info);
				break;
			}
		
		}
	}
}


//add new product or service
var groupCounter = 0;
var supplier =<?=$supplier_list?>;
var service =<?=$service?>;
var btn_type = '<?=$btn_type?>';

function addGroup(type){
	html='';
	var no = groupCounter+1;
	if(type == 'product'){
		
		tmp_supplier='<option>select</option>';
		for(i=0;i<supplier.length;i++){
			tmp_supplier+='<option value='+supplier[i].id+'>'+supplier[i].name+'</option>';
		}
		
		
		
		html='<tr id="group'+groupCounter+'"><td>'+no+'<input type="hidden" id="type'+groupCounter+'" name="type'+groupCounter+'" value="product"/></td><td><select id="supplier'+groupCounter+'" name="supplier'+groupCounter+'" class="form-control" onchange="getModel('+groupCounter+',1)">'+tmp_supplier+'</select></td><td><select id="modelNo'+groupCounter+'" name="modelNo'+groupCounter+'" class="form-control" onchange="getQuantity('+groupCounter+',1)" disabled></select><input type="hidden" name="modelNo2'+groupCounter+'" id="modelNo2'+groupCounter+'"/></td><td><select id="quantity'+groupCounter+'" name="quantity'+groupCounter+'" class="form-control" onchange="estShipping()" disabled></select></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input id="unitPrice'+groupCounter+'" name="unitPrice'+groupCounter+'" type="text" class="form-control" value="-" readonly></div></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input id="extendedPrice'+groupCounter+'" name="extendedPrice'+groupCounter+'" type="text" class="form-control" value="-" readonly></div</td><td><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="delGroup('+groupCounter+')"><i class="fa fa-times"></i></button><input id="type'+groupCounter+'" name="type'+groupCounter+'" type="hidden" value="1"><input id="is_deleted'+groupCounter+'" name="is_deleted'+groupCounter+'" type="hidden" value="0"><input id="old_id'+groupCounter+'" name="old_id'+groupCounter+'" type="hidden" value=""></td></tr>';

	}else{
		
		tmp_service='<option>select</option>';
		for(i=0;i<service.length;i++){
			tmp_service+='<option value="'+service[i].id+'">'+service[i].model_no+'</option>';
		}
		
		html='<tr id="group'+groupCounter+'"><td>'+no+'<input type="hidden" id="type'+groupCounter+'" name="type'+groupCounter+'" value="service"/></td><td><input id="supplier'+groupCounter+'" name="supplier'+groupCounter+'" type="text" class="form-control" value="aNexus" disabled></td><td><select onchange="getQuantity('+groupCounter+',2)" id="modelNo'+groupCounter+'" name="modelNo'+groupCounter+'" class="form-control">'+tmp_service+'</select><input type="hidden" name="modelNo2'+groupCounter+'" id="modelNo2'+groupCounter+'"/></td><td><select onchange="getTotalPrice()" id="quantity'+groupCounter+'" name="quantity'+groupCounter+'" class="form-control" disabled></select></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input id="unitPrice'+groupCounter+'" name="unitPrice'+groupCounter+'" type="text" class="form-control" value="-" readonly></div></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input id="extendedPrice'+groupCounter+'" name="extendedPrice'+groupCounter+'" type="text" class="form-control" value="-" readonly></div></td><td><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="delGroup('+groupCounter+')"><i class="fa fa-times"></i></button><input id="type'+groupCounter+'" name="type'+groupCounter+'" type="hidden" value="2"><input id="is_deleted'+groupCounter+'" name="is_deleted'+groupCounter+'" type="hidden" value="0"><input id="old_id'+groupCounter+'" name="old_id'+groupCounter+'" type="hidden" value=""></td></tr>';
		
	}
	
	$('#tbody').append(html);
	
	if(mode == 'Edit' && groupCounter < groupJson.length){
		
		//Product
		if(groupJson[groupCounter].type == 1){
			
			$('#supplier'+groupCounter).val(groupJson[groupCounter].supplier_id);
			
			//$('#quantity'+groupCounter).prop('disabled', false);
			$('#quantity'+groupCounter).val(groupJson[groupCounter].quantity);
			
			$('#old_id'+groupCounter).val(groupJson[groupCounter].id);
			
			console.log(groupCounter+ ' ' + groupJson[groupCounter].supplier_id + ' ' + $('#supplier'+groupCounter).val());			
			getModel(groupCounter,1);
												
		//Service	
		}else{
			
			$('#quantity'+groupCounter).val(groupJson[groupCounter].quantity);
			
			$('#old_id'+groupCounter).val(groupJson[groupCounter].id);
			
			console.log(groupCounter+ ' ' + groupJson[groupCounter].supplier_id + ' ' + $('#supplier'+groupCounter).val());			
			getModel(groupCounter,2);
			
		}
		
		
		
	}
	
	groupCounter++;
	$('#groupCount').val(groupCounter);
}

function getModel(id, type){
	
	
	var company = $('#supplier'+id).val();
	
	if(company != 'select'){
		
		term = {id:company, type:type};
		url = '<?=base_url($init['langu'].'/anexus/sales/getModel')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
										
					//console.log(r);
					tmp_model='<option>select</option>';
					
					//product
					if(r.type == 1) {						

						for(i=0;i<r.data.length;i++){
							tmp_model+='<option value="'+r.data[i].id+'">'+r.data[i].model_no+'</option>';
						}
					
					} else if (r.type == 2) {
						
						for(i=0;i<r.data.length;i++){
							tmp_model+='<option value="'+r.data[i].id+'">'+r.data[i].serial+' '+r.data[i].model_no+'</option>';
						}
						
					}
					
					$('#unitPrice'+id).val('-');
					//$('#quantity'+id).html(groupJson[id].quantity);
					//$('#quantity'+id).prop('disabled', true);
					$('#modelNo'+id).html('');
					$('#modelNo'+id).append(tmp_model);
					$('#modelNo'+id).prop('disabled', false);
					//$('#quantity'+id).prop('disabled', 'disabled');
					
					if(mode =='Edit'){
					$('#modelNo'+id).val(groupJson[id].product_id);
					getQuantity(id,groupJson[id].type);
					}
					
					estShipping();
					
					
					
				}
			});	
			
	}else{
		$('#unitPrice'+id).val('-');
		//$('#quantity'+id).html('');
		//$('#quantity'+id).prop('disabled', true);
		$('#modelNo'+id).html('');
		$('#modelNo'+id).prop('disabled', true);
		estShipping();
	}
	
}

function getQuantity(id,type){
	
	var model = $('#modelNo'+id).val();
	var modelText = $('#modelNo'+id+' option:selected').text();
	
	if(model != 'select'){
	
		term = {id:model};
		url = '<?=base_url($init['langu'].'/anexus/sales/getProductDetail')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					//console.log(r);
					tmp_quantity='<option>select</option>';
					
					var uPrice = 0;
					var exPrice = '-';
					
					if(type == 1){
						for(i=1;i<=r.product.current_stock_qty;i++){
							tmp_quantity+='<option value="'+i+'">'+i+'</option>';
							uPrice = r.product.selling_price;
						}
					}else{
						for(i=1;i<=50;i++){
							tmp_quantity+='<option value="'+i+'">'+i+'</option>';
							uPrice = r.product.unit_price;
						}
					}
					$('#quantity'+id).html('');
					$('#quantity'+id).append(tmp_quantity);
					$('#quantity'+id).prop('disabled', false);
					$('#unitPrice'+id).val(uPrice);
					$('#modelNo2'+id).val(modelText);
					
					if(mode == 'Edit') {
						$('#quantity'+id).val(groupJson[id].quantity);	
					}
					estShipping();
					
					//change input to read only
					if(btn_type != 'create' && btn_type != 'edit'){
						
						$(".form-control").each(function() {
						  $(this).prop('disabled', 'disabled');
						});
						
						$(".btn-circle").each(function() {
						  $(this).hide();
						});
						$('#addProduct').hide();
						$('#addService').hide();
						if(btn_type == 'update'){
							
							var target_colum = <?=isset($target_colum)?$target_colum:'[]'?>;
							
							for(var i=0;i<target_colum.length;i++){
								var colum = target_colum[i].name;
								if(target_colum[i].type == 'single'){
									$("input[name='"+colum+"']").prop('disabled', false);
									
								}else if(target_colum[i].type == 'multiple'){
									$("."+colum).prop('disabled', false);
								}
							}
							
							
							
						}
					}
					
				}
			});	
			
	}else{
		$('#quantity'+id).html('');
		$('#quantity'+id).prop('disabled', true);
		$('#unitPrice'+id).val('-');
		$('#modelNo2'+id).val('');
	}
	
	
	
	
	
}

function estShipping(){
		getTotalPrice();
		var est_check = $('#auto_est_shipping').prop('checked');
		
		if(mode != 'Edit' && est_check != false){
			var companyId = $('#company').val();
		
		var shipper = $('#shipper').val();
		var groupCount = $('#groupCount').val()
		var groupItem = [];
		var term = "";
		
		if(shipper != 'select' && companyId != 'none'){
			
			companyId = companyId.split('|');
			companyId = companyId[0];
			
			if(groupCount > 0){
				
				for(i=0;i<groupCount;i++){
					
					supplierId = $('#supplier'+i).val();
					modelNo = $('#modelNo'+i).val();
					item_type = $('#type'+i).val();

					if(supplierId != null && modelNo != null){
					modelNo = modelNo.split('|');
					modelNo = modelNo[0];
					
					quantity = $('#quantity'+i).val();
					tmp = {supplierId:supplierId,modelNo:modelNo,quantity:quantity,item_type:item_type};
					groupItem.push(tmp);
					
					}
				}
					
			}
			//console.log(groupItem);
			
			term = {companyId:companyId,shipperId:shipper,groupItem:JSON.stringify(groupItem)};
			url = '<?=base_url($init['langu'].'/anexus/sales/quotation_estShipping')?>';
	
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					//console.log(r);	
					$('#shippingFee').val(r.estShipping);
					
					//for status 3 logistic
					/*if(role==6 && status==3){
						$( "input,select,textarea" ).each(function() {
						  $( this ).prop('disabled', 'disabled');
						});
						$('#shippingFee').prop('disabled', false);
						$('#addProduct').prop('disabled', 'disabled');
						$('#addService').prop('disabled', 'disabled');
					}else if(status==4){
						$( "input,select,textarea" ).each(function() {
						  $( this ).prop('disabled', 'disabled');
						});
						$('#addProduct').prop('disabled', 'disabled');
						$('#addService').prop('disabled', 'disabled');
					}else if(status==5){
						$( "input,select,textarea" ).each(function() {
						  $( this ).prop('disabled', 'disabled');
						});
						$('#addProduct').prop('disabled', 'disabled');
						$('#addService').prop('disabled', 'disabled');
						$('#comfirm_file').prop('disabled', false);	
					}*/
					
				}
			});	
			
		}else{
			//alert('Please select shipper');
			$('#shippingFee').val(0);
		}
		}
	
}

function getTotalPrice(){

	var counterItem = $('#groupCount').val();
	var unitPrice = 0;
	var quantity = 0;
	var extendedPrice = 0;
	var sum = 0;
	var service_sum = 0;
	var product_sum = 0;
	var eachTotal = 0;
	
	for(var i=0;i<counterItem;i++){
		unitPrice = parseInt($('#unitPrice'+i).val());
		quantity = parseInt($('#quantity'+i).val());
		is_del = $("#is_deleted"+i).val();
		type = $("#type"+i).val();
		//alert(unitPrice);
		//alert(quantity);
		if(is_del != 1){
			if(!isNaN(unitPrice) && !isNaN(quantity)){
	
				eachTotal = (unitPrice*quantity);
				$('#extendedPrice'+i).val(eachTotal);
				if(type == 'product'){
					product_sum += eachTotal;
				}else{
					service_sum += eachTotal;
				}
				sum += eachTotal;
			}
		}
	
	}
	//alert(sum)
	$("#totalPrice").val(sum);
	$("#product_total").val(product_sum);
	$("#service_total").val(service_sum);
	
}


function delGroup(id){
	$("#is_deleted"+id).val(1);
	$('#group'+id).hide();
	getTotalPrice();
	
}

function downloadPDF(url){
	location.href=url;
}

function sentMail(url){
	
	var product_total =$('#product_total').val();
	var service_total =$('#service_total').val();
	//alert(url+'/'+product_total+'/'+service_total);
	location.href=url+'/'+product_total+'/'+service_total;
}





</script>