<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?=isset($webpage['web_title'])?$webpage['web_title']:"Anexus ERP System"?></title>

    <!-- Core CSS - Include with every page -->
    <link href="<?=base_url('assets/anexus/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/anexus/font-awesome/css/font-awesome.css')?>" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="<?=base_url('assets/anexus/css/plugins/morris/morris-0.4.3.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/anexus/css/plugins/timeline/timeline.css')?>" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="<?=base_url('assets/anexus/css/sb-admin.css')?>" rel="stylesheet">

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                
                    	<?php
                        	if(isset($status)){
								
								if($status == 0){		
									echo '<div class="alert alert-danger alert-dismissable">Reset failed</div>';
								}else if($status == 1){
									echo '<div class="alert alert-success alert-dismissable">Reset successful</div>';
								}
								
							}
						?>
            </div>
        </div>
    </div>

    <!-- Core Scripts - Include with every page -->
    <script src="<?=base_url('assets/anexus/js/jquery-1.10.2.js')?>"></script>
    <script src="<?=base_url('assets/anexus/js/bootstrap.min.js')?>"></script>
    <script src="<?=base_url('assets/anexus/js/plugins/metisMenu/jquery.metisMenu.js')?>"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="<?=base_url('assets/anexus/js/sb-admin.js')?>"></script>

</body>

</html>

<script>
window.setTimeout(function(){

    window.location.href = "<?=base_url('/en/login')?>";

}, 3000);
</script>
