
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-briefcase fa-fw"></i> Tenders
                       <div class="col-lg-3 pull-right">
                       		<form role="form">
                                <div class="form-group">
                                	<input id="q" class="form-control" placeholder="Search" value="<?=$q?>" onblur="filter();">
                                </div>
                                <!-- /.form-group -->
                            </form>
                        </div>
                		<!-- /.col-lg-3 -->
                        <script>
							function filter(){
								var q = $("#q").val();
								if(q == '') {
									q = 'ALL';	
								}								
								location.href='<?=base_url('en/anexus/'.$group_name.'/'.$model_name)?>/'+q;
							}
							</script>
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                            	<a href="<?=base_url('en/anexus/'.$group_name.'/'.$model_name.'_add')?>"><button class="btn btn-default btn-sm" type="button">Create <?=$common_name?></button></a><p></p>
                    <div class="panel panel-default">
                            <div class="table-responsive">
                            	<?php
								if(!empty($results)) {
								?>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>                                            
                                            <th>Customer</th>
                                            <th>Last Updated</th>
                                            <th>Last Updated By</th>
                                            <th>Awaiting</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                   <tbody>
                                    	<?php
										foreach($results as $v) {
										?>
                                        <tr>
                                            <td><?=$v['qr_serial']?></td>
                                            <td><a href="<?=base_url($init['langu'].'/anexus/administrator/customers_view/'.$v['customer_id'])?>" target="_blank"><?=isset($customer_list[$v['customer_id']])?$customer_list[$v['customer_id']]:''?></a></td>
                                            <td><?=$v['modified_date'] != '0000-00-00 00:00:00'?date('d/m/Y',strtotime($v['modified_date'])):''?></td>
                                            <td><?=isset($user_list[$v['lastupdate_user_id']])?$user_list[$v['lastupdate_user_id']]:''?></td>
                                            <td>
                                            <?php
											if(!empty($v['awaiting_table'])){
                                            	foreach($v['awaiting_table'] as $x){
													if(isset($role_list[$x])){
													echo $role_list[$x].'<br>';
													}
												}
											}
											?>
                                            </td>
                                            <td><?=isset($tender_status_list[$v['status']])?$tender_status_list[$v['status']]:''?></td>
                                            <td>                                            	
                                                <button  class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="Edit" type="button" onclick="location.href='<?=base_url('en/anexus/sales/tender_edit/'.$v['id'])?>'"><i class="fa fa-edit"></i></button>
                                                <button  class="btn btn-primary btn-circle btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="deleteData('<?=base_url('en/anexus/sales/tender_del/'.$v['id'])?>')"><i class="fa fa-times"></i></button>
                                             </td>
                                        </tr>
                                        <?php
										}
										?>
                                        
                                    </tbody>
                                </table>
                                 <?php
								} else {
								?>
                                <p>No data yet</p>
                                <?php	
								}
								?>
                      </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                    <?=$paging?>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
        <script>
		function deleteData(url){
			var c = confirm("Are you sure you want to delete?");
			if(c){
				location.href=url;	
			}
		}
		</script>