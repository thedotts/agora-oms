<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-wrench fa-fw"></i> <?=$head_title?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                       		<!-- Button trigger modal -->
                            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal"<?=$mode=='Edit'?'disabled':''?>>
                                Search from Service Request
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Search Service Request</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Search</label>
                                                <input class="form-control" placeholder="By Company Name or Reference No." id="search" name="search">
                                            </div>
                                            <table class="table table-striped table-bordered table-hover" id="sr_table">
                                            </table>
                                            <div id="pagination"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        	<form action="<?=base_url($init['langu'].'/anexus/service/field_service_order_submit');?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" id="customer_id" name="customer_id" value="<?=$mode=='Edit'?$result['customer_id']:''?>"/>
            <input type="hidden" id="requestor_id" name="requestor_id" value="<?=$mode=='Edit'?$result['requestor_id']:$staff_info['id']?>"/>
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Field Service Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Serial No.</label>
                                    <input id="field_service_no" name="field_service_no" class="form-control" value="<?=$mode=='Edit'?$result['field_service_no']:''?>" readonly>
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Product/Model</label>
                                    <input id="product_id" name="product_id" class="form-control" value="aNexus" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Serial No</label>
                                    <input id="product_serial" name="product_serial" class="form-control" value="<?=$mode=='Edit'?$result['product_serial']:''?>" readonly>
                                    <!-- <select class="form-control">
                                        <option>A-111</option>
                                        <option>B-222</option>
                                    </select>-->
                                </div>
                                <div class="form-group">
                                    <label>Service</label>
                                    <select id="service" name="service" class="form-control">
                                        <option value="No Charge" <?=$mode=='Edit'?($result['service']=='No Charge'?'selected':''):''?>>No Charge</option>
                                        <option value="Manufacturer Warranty" <?=$mode=='Edit'?($result['service']=='Manufacturer Warranty'?'selected':''):''?>>Manufacturer Warranty</option>
                                        <option value="Service Warranty" <?=$mode=='Edit'?($result['service']=='Service Warranty'?'selected':''):''?>>Service Warranty</option>
                                        <option value="Paid Service" <?=$mode=='Edit'?($result['service']=='Paid Service'?'selected':''):''?>>Paid Service</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Service Type</label>
                                    <select id="service_type" name="service_type" class="form-control">
                                        <option value="Standard Engineer" <?=$mode=='Edit'?($result['service_type']=='Standard Engineer'?'selected':''):''?>>Standard Engineer</option>
                                        <option value="Operator" <?=$mode=='Edit'?($result['service_type']=='Operator'?'selected':''):''?>>Operator</option>
                                        <option value="Technician" <?=$mode=='Edit'?($result['service_type']=='Technician'?'selected':''):''?>>Technician</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Tool ID</label>
                                    <input id="tool_id" name="tool_id" class="form-control" placeholder="" value="<?=$mode=='Edit'?$result['tool_id']:''?>">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Customer Information
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label>Company Name *</label>
                                <input id="company_name" name="company_name" class="form-control" value="<?=$mode=='Edit'?$customer['company_name']:''?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Name</label>
                                <input id="name" name="name" class="form-control" value="<?=$mode=='Edit'?$customer['contact_person']:''?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <input id="address" name="address" class="form-control" value="<?=$mode=='Edit'?$customer['delivery_address']:''?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Postal Code</label>
                                <input id="postal_code" name="postal_code" class="form-control" value="<?=$mode=='Edit'?$customer['postal']:''?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input id="email" name="email" class="form-control" value="<?=$mode=='Edit'?$customer['email']:''?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Contact</label>
                                <input id="contact" name="contact" class="form-control" value="<?=$mode=='Edit'?$customer['contact_info']:''?>" readonly>
                            </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
            </div>
            <!-- /.row -->
            
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            More Information
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="form-group">
                                <label>Faults</label>
                                <textarea id="faults" name="faults" class="form-control"><?=$mode=='Edit'?$result['faults']:''?></textarea>
                                <label>Fault's Reference Images</label>
                                <input id="faults_img" name="faults_img" type="file">
                                <?php if($mode=='Edit' && $result['faults_reference'] != ''){?>
                                <a href="<?=$result['faults_reference']?>" target="_blank"><img class="thumbnail" width="200px" src="<?=$result['faults_reference']?>"/></a>
                                <?php }?>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label>Actions and Remedy</label>
                                <textarea id="act_remedy" name="act_remedy" class="form-control"><?=$mode=='Edit'?$result['actions_remedy']:''?></textarea>
                                <label>Actions and Remedy's Reference Images</label>
                                <input id="act_remedy_img" name="act_remedy_img" type="file">
                                <?php if($mode=='Edit' && $result['actions_remedy_ref'] != ''){?>
                                <a href="<?=$result['actions_remedy_ref']?>" target="_blank"><img class="thumbnail" width="200px" src="<?=$result['actions_remedy_ref']?>"/></a>
                                <?php }?>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label>Spare Parts Used</label>
                                <textarea id="spare_part" name="spare_part" class="form-control"><?=$mode=='Edit'?$result['spare_part_used']:''?></textarea>
                                <label>Spare Parts Used's Reference Images</label>
                                <input id="spare_part_img" name="spare_part_img" type="file">
                                <?php if($mode=='Edit' && $result['spare_part_ref'] != ''){?>
                                <a href="<?=$result['spare_part_ref']?>" target="_blank"><img class="thumbnail" width="200px" src="<?=$result['spare_part_ref']?>"/></a>
                                <?php }?>
                            </div>
                            
                            
                    
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Time Schedule
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                            			<button id="addTime" class="btn btn-default btn-sm" type="button" onclick="addPeriod()">Add New Time Period</button>
                                        <input type="hidden" id="addCounter" name="addCounter" value="0">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Day</th>
                                                    <th>Start Time</th>
                                                    <th>End Time</th>
                                                    <th>Total Hours</th>
                                                    <th width="80px">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="period_body">
                                                
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                    <!-- /.table-responsive -->
                                    
                                </div>
                                <!-- /.panel-body -->
                                
                            </div>
                            <!-- /.panel -->
                            
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                    
                    <?php if($mode=='Add'){?>
                    
                    	<?php if($btn_type == 'create'){?>
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                        </div>
                    	<?php }?>
                        
                    <?php }else if($mode=='Edit'){?>
                    	<?php switch($btn_type){
							case 'edit':
							?>
                        		<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Submit</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            <?php 
							break;
							case 'approval':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Update</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'approval_update':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'sendmail_update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-default">Save</button>
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/service/fsr_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/service/fsr_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'send_email':
							?>
                            	<div class="form-group">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/service/fsr_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/service/fsr_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                </div>
                            
                            <?php 
							break;
							case 'upload':
							?>
                            
                            	<div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/service/fsr_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/service/fsr_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file">
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                		<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            
                            <?php 
							break;
							case 'complete':
							?>
                            	<?php if($last_action == 'upload'){?>
                            	<div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/service/fsr_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/service/fsr_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file"> 
                                            <a target="_blank" href="<?=$result['customer_confirmation_document']?>"><?=$result['customer_confirmation_document']?></a>
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Reupload</button>
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            	<?php }?>
                            <?php 
							break;
                        }?>

                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Status
                        </div>
                        <div class="panel-body">
                            <div class="form-group input-group">
							<?php 
                                //如果不是not approved, 也不是 completed 就顯示目前正在等誰
                                if($result['status'] != 0 && $result['status'] != $completed_status_id){
                            ?>
                            <label>
                                                    <?php
                                                    if(!empty($result['awaiting_table'])){
                                                        echo 'Awaiting ';
                                                        foreach($result['awaiting_table'] as $x){
                                                            if(isset($role_list[$x])){
                                                            echo '<br>'.$role_list[$x];
                                                            }
                                                        }
                                                    }
                                                    ?>
                            </label><br>
                            <?php 
                            } 
                            ?>
                            <label><?=$fso_status_list[$result['status']]?></label>
                            </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                    
                    <?php } //Edit mode end ?>
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->
		
        <script>
		var mode ='<?=$mode?>';
		var schedule = <?=$mode=='Edit'?$result['time_schedule']:'[]'?>;
		var addCounter = 0;
		var btn_type = '<?=$btn_type?>';
		
		for(var i=0;i<schedule.length;i++){
			addPeriod();
		}
					//change input to read only
					if(btn_type != 'create' && btn_type != 'edit' && btn_type != 'sendmail_update'){
						
						$(".form-control").each(function() {
						  $(this).prop('disabled', 'disabled');
						});
						
						$(".btn-circle").each(function() {
						  $(this).hide();
						});
						$('#addProduct').hide();
						$('#addService').hide();
						$('#addTime').hide();
						if(btn_type == 'update' || btn_type == 'approval_update' ){
							
							var target_colum = <?=isset($target_colum)?$target_colum:'[]'?>;
							
							for(var i=0;i<target_colum.length;i++){
								var colum = target_colum[i].name;
								if(target_colum[i].type == 'single'){
									$("input[name='"+colum+"']").prop('disabled', false);
									
								}else if(target_colum[i].type == 'multiple'){
									$("."+colum).prop('disabled', false);
								}
							}
							
							
						}
					}
		
        $( "#search" ).blur(function(){
			
			ajax_quotation(1);
			
		});
		
		$('#myModal').on('show.bs.modal', function (event) {
			ajax_quotation(1);
		})
		
		function changePage(i){
			ajax_quotation(i);
		}
		
		function ajax_quotation(i){
			keyword = $("#search").val();
			term = {keyword:keyword,limit:10,page:i};
			url = '<?=base_url($init['langu'].'/anexus/service/getServiceRequest')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					console.log(r);
					tmp = '';
					
					if(r.data != false){
					for(var i=0;i<r.data.length;i++){
						
						tmp+='<tr><td><a href="javascript:getPR('+r.data[i].id+')">aNexus</a></td><td>'+r.data[i].service_serial+'</td><td>'+r.data[i].modified_date+'</td><td>'+r.data[i].status+'</td></tr>';
					
					}
					
					html = '<thead><tr><th>Company Name</th><th>Reference No.</th><th>Date Last Updated</th><th>Status</th></tr></thead><tbody>'+tmp+'</tbody>';
					
					pagination = r.paging;
					
					$('#sr_table').html('');
					$('#pagination').html('');
					$('#sr_table').append(html);
					$('#pagination').append(pagination);
					}else{
						$('#sr_table').html('No search result');
						$('#pagination').html('');
					}
					
				}
			});	
		}
		
		function getPR(id){
			
			$('#myModal').modal('hide');
			
			term = {id:id};
			url = '<?=base_url($init['langu'].'/anexus/service/getSR')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					console.log(r);
					
					$('#product_serial').val(r.data.service_serial);
					$('#company_name').val(r.customer_data.company_name);
					$('#customer_id').val(r.customer_data.id);
					$('#name').val(r.customer_data.contact_person);
					$('#address').val(r.customer_data.delivery_address);
					$('#postal_code').val(r.customer_data.postal);
					$('#email').val(r.customer_data.email);
					$('#contact').val(r.customer_data.contact_person);

					
					
				}
			});	
			
		}
		
		
		function addPeriod(){
			addCounter++;
			var tmp_opt ='';
			for(var i=0;i<24;i++){
				for(var j=0;j<4;j++){
					
					time_hour=i.toString();
					if(time_hour.length<2){
						time_hour='0'+i;
					}
					
					time_min =(j*15);
					time_min =time_min.toString();
					if(time_min.length<2){
						time_min='0'+time_min;
					}
					
					tmp_opt += '<option value="'+time_hour+':'+time_min+'">'+time_hour+':'+time_min+'</option>';
				}
			}
			
			
			html='<tr id="period_'+addCounter+'"><td><input id="schedule_date'+addCounter+'" name="schedule_date'+addCounter+'" class="form-control" placeholder="Calendar Input" data-myid="'+addCounter+'"></td><td><input id="schedule_day'+addCounter+'" name="schedule_day'+addCounter+'" type="text" class="form-control" value="" readonly></td><td><select onchange="totalHour('+addCounter+')" id="schedule_startTime'+addCounter+'" name="schedule_startTime'+addCounter+'" class="form-control">'+tmp_opt+'</select></td><td><select onchange="totalHour('+addCounter+')" id="schedule_endTime'+addCounter+'" name="schedule_endTime'+addCounter+'" class="form-control">'+tmp_opt+'</select></td><td><input id="totalHour'+addCounter+'" name="totalHour'+addCounter+'" type="text" class="form-control" placeholder=""></td><td><button class="btn btn-danger btn-circle" data-placement="top" data-toggle="tooltip" data-original-title="Delete" type="button" onclick="period_del('+addCounter+')"><i class="fa fa-times"></i></button></td></tr>';
			
			$('#period_body').append(html);
			
			if(mode == 'Edit' && addCounter<=schedule.length){
				$('#schedule_date'+addCounter).val(schedule[(addCounter-1)].schedule_date);
				$('#schedule_day'+addCounter).val(schedule[(addCounter-1)].schedule_day);
				$('#schedule_startTime'+addCounter).val(schedule[(addCounter-1)].schedule_startTime);
				$('#schedule_endTime'+addCounter).val(schedule[(addCounter-1)].schedule_endTime1);
				$('#totalHour'+addCounter).val(schedule[(addCounter-1)].totalHour);
			}
			
			
			$('#addCounter').val(addCounter);
			
			$('#schedule_date'+addCounter).datepicker({
				dateFormat:'dd/mm/yy',
				onClose: function(selectedDate) {
					date = selectedDate.split('/');
					
					var d = new Date();
					d.setFullYear(date[2], date[1]-1, date[0]);
					dayName = getDateName(d);
					
					var current_id = $(this).data("myid");
					//alert(current_id);
					$('#schedule_day'+current_id).val(dayName);

					//alert(addCounter);
				}
			});
			
			//set datepicker get current date
			if(mode == 'Add'){
				 	$('#schedule_date'+addCounter).datepicker('setDate', 'today');
					date = $('#schedule_date'+addCounter).val();
				 	date = date.split('/');
				 	var d = new Date();
					d.setFullYear(date[2], date[1]-1, date[0]);
					dayName = getDateName(d);
					$('#schedule_day'+addCounter).val(dayName);
			}
			
			
			
		}
		
		var start = '';
		var end = '';
		function totalHour(id){
			start = $('#schedule_startTime'+id).val();
			end = $('#schedule_endTime'+id).val();
			
			start_split = start.split(':');
			end_split = end.split(':');
			
			s_hour_sec = parseInt(start_split[0])*3600;
			s_min_sec = parseInt(start_split[1])*60;
			total_s_sec = s_hour_sec+s_min_sec;
			
			e_hour_sec = parseInt(end_split[0])*3600;
			e_min_sec = parseInt(end_split[1])*60;
			total_e_sec = e_hour_sec+e_min_sec;
			
			hour = (total_e_sec-total_s_sec)/3600;
			console.log(start+','+end);
			$('#totalHour'+id).val(hour);
		}
		
		function period_del(id){
			$('#period_'+id).remove();
		}
		
		function getDateName(d) {
			var weekday = new Array(7);
			weekday[0] = "Sun";
			weekday[1] = "Mon";
			weekday[2] = "Tue";
			weekday[3] = "Wed";
			weekday[4] = "Thur";
			weekday[5] = "Fri";
			weekday[6] = "Sat";
		
			var n = weekday[d.getDay()];
			return n;
		}
		
		function sentMail(url){
			location.href=url;
		}
		
        </script>