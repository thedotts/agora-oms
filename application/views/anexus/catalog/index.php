<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-shopping-cart fa-fw"></i> Product Catalog
                	</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                    		<button onclick="changeView()" class="btn btn-default btn-sm pull-right"> <i class="fa fa-tasks fa-fw"></i> Sim/Adv View</button>
                            <!-- Nav tabs -->
                            <ul class="nav nav-pills">
                            <?php $count = 0;?>
                            <?php foreach($result as $k => $v){ ?>
                            	<?php if(isset($category_list[$k])){?>
                                	<?php if($count == 0){?>
                                	<li class="active"><a href="#<?=$k?>" data-toggle="tab"><?=$category_list[$k]?></a>
                                	</li>
                                	<?php }else{ ?>
                                    <li><a href="#<?=$k?>" data-toggle="tab"><?=$category_list[$k]?></a>
                                	</li>
                                    <?php } ?>
                                <?php } ?>
                                <?php $count++;?>
                            <?php } ?>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                            	<?php $count_item = 0;?>
                            	<?php foreach($result as $k => $v){ ?>
                                <?php if($count_item == 0){?>
                                <div class="tab-pane fade in active" id="<?=$k?>">
                                <?php }else{ ?>
                                <div class="tab-pane fade" id="<?=$k?>">
                                <?php } ?>
                                	
                                	<div class="row">
                                       <div class="col-lg-3 pull-right">
                                            <form role="form">
                                                <div class="form-group">
                                                    <input class="form-control" id="searchWord<?=$k?>" onblur="searchTab(<?=$k?>)" placeholder="Search">
                                                </div>
                                                <!-- /.form-group -->
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.col-lg-3 -->
                                    <div id="tabContent<?=$k?>">
                                    <?php foreach($v as $k2 => $v2){ ?>
                                    <div class="panel-body">
                                        <div class="row">
                                            <span class="col-lg-3 col-sm-4 col-xs-12 pull-left">
                                                <img src="<?=$v2['pic_path']?>" class="img-responsive" />
                                            </span>
                                            <div class="col-lg-9 clearfix">
                                            	<h4><?=$v2['product_name']?></h4>
                                                <p><strong>Price:</strong> $<?=number_format($v2['selling_price'],2)?> per <?=ucfirst($v2['uom'])?></p>
                                                <p><?=$v2['description']?></p>
                                            </div>
                                        </div>
                                        <hr class="invisible">
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="form-group input-group">
                                                    <span class="input-group-addon"> Country </span>
                                                    <input id="model_name<?=$count_item?>" class="form-control" value="<?=$v2['model_name']?>" readonly="readonly"/>

                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-sm-3">
                                                <div class="form-group input-group">
                                                	<input id="promo<?=$count_item?>" type="hidden" class="form-control" value="<?=$v2['promo_val']?>">
                                                	<input id="p_id<?=$count_item?>" type="hidden" class="form-control" value="<?=$v2['id']?>">
                                                    <input id="p_name<?=$count_item?>" type="hidden" class="form-control" value="<?=$v2['product_name']?>">
                                                    <input id="price<?=$count_item?>" type="hidden" class="form-control" value="<?=$v2['selling_price']?>">
                                                    <input id="qty<?=$count_item?>" class="form-control" value=""> 
                                                    <span class="input-group-addon"> <?=ucfirst($v2['uom'])?>(s)</span>
                                                    <input type="hidden" id="uom_val<?=$count_item?>" value="<?=$v2['uom']?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-sm-3">
                                                <button class="btn btn-primary" onclick="addItem(<?=$count_item?>)">Add To Cart</button>
                                            </div>
                                        </div>
                                        <!-- /.row -->
                                    </div>
                                    <!-- /.panel -->
                                    <hr>
                                    <?php $count_item++;?>
                                    <?php } ?>
                                    </div>
                                    
                                </div>
                                <?php } ?>
                                
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

 <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<h4 class="modal-title" id="myModalLabel">Add to Cart</h4>
</div>
<div class="modal-body">
<table class="table table-striped table-bordered table-hover" id="dataTables">
<thead>
<tr>
<th>Quantity</th>
</tr>
</thead>
<tbody>
<tr>
<td>
<div class="form-group input-group">
<input id="promo" type="hidden" class="form-control" value=""> 
<input id="p_id" type="hidden" class="form-control" value=""> 
<input id="p_name" type="hidden" class="form-control" value=""> 
<input id="model_name" type="hidden" class="form-control" value=""> 
<input id="price" type="hidden" class="form-control" value=""> 
<input id="qty" class="form-control" value=""> 
<input type="hidden" id="uom_val" class="form-control" value=""> 
<span class="input-group-addon" id="uom"> Carton(s)</span>
</div>
</td>
</tr>
</tbody>
</table>
</div>
<div class="modal-footer">
<button type="submit" class="btn btn-primary" onclick="modal_addItem()">Add</button>
<button type="cancel" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<script>
var result = <?=json_encode($result2)?>;
var view = 2;
var count_item = <?=$count_item?>;

function changeView(){
	
	
	
	if(view == 1){
		console.log(result);
		for(i=0;i<result.length;i++){
			var html = '';
			for(j=0;j<result[i].length;j++){

				cat_id = result[i][j].product_category;
				
				html +='<div class="panel-body"><div class="row"><span class="col-lg-3 col-sm-4 col-xs-12 pull-left"><img src="'+result[i][j].pic_path+'" class="img-responsive" /></span><div class="col-lg-9 clearfix"><h4>'+result[i][j].product_name+'</h4><p><strong>Price:</strong> $'+formatNumber(result[i][j].selling_price)+' per '+capitalizeFirstLetter(result[i][j].uom)+'</p><p>'+result[i][j].description+'</p></div></div><hr class="invisible"><div class="row clearfix"><div class="col-lg-6 col-sm-6"><div class="form-group input-group"><span class="input-group-addon"> Description </span><input id="model_name'+count_item+'" class="form-control" value="'+result[i][j].model_name+'" readonly="readonly"/></div></div><div class="col-lg-3 col-sm-3"><div class="form-group input-group"><input id="promo'+count_item+'" type="hidden" class="form-control" value="'+result[i][j].promo_val+'"><input id="p_id'+count_item+'" type="hidden" class="form-control" value="'+result[i][j].id+'"><input id="p_name'+count_item+'" type="hidden" class="form-control" value="'+result[i][j].product_name+'"><input id="qty'+count_item+'" class="form-control" value=""><input type="hidden" id="price'+count_item+'" class="form-control" value="'+result[i][j].selling_price+'"><span class="input-group-addon"> '+capitalizeFirstLetter(result[i][j].uom)+'(s)</span><input type="hidden" id="uom_val'+count_item+'" value="'+result[i][j].uom+'"></div></div><div class="col-lg-3 col-sm-3"><button class="btn btn-primary" onclick="addItem('+count_item+')">Add To Cart</button></div></div><!-- /.row --></div><!-- /.panel --><hr>';
				
				count_item++;
				
			}
			//console.log(html);
			$('#tabContent'+cat_id).html('');
			$('#tabContent'+cat_id).append(html);
			$('#searchWord'+cat_id).val('');
			view = 2;
		}
	}else{

		for(i=0;i<result.length;i++){
			
			var html = '';
			var content = '';
			
			for(j=0;j<result[i].length;j++){
				
				cat_id = result[i][j].product_category;
				
				content +='<tr><td>'+(j+1)+'</td><td><a href="#">'+result[i][j].product_name+'</a></td><td>'+result[i][j].model_name+'</td><td>$'+formatNumber(result[i][j].selling_price)+' per '+capitalizeFirstLetter(result[i][j].uom)+'</td><td><!-- Button trigger modal --><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" data-productId="'+result[i][j].id+'" data-productName="'+result[i][j].product_name+'" data-modelName="'+result[i][j].model_name+'" data-price="'+result[i][j].selling_price+'" data-uom="'+result[i][j].uom+'" data-promo="'+result[i][j].promo_val+'"><i class="fa fa-shopping-cart"></i> Add To Cart</button></td></tr>';
				
			}
			
			html +='<div class="panel-body"><div class="table-responsive"><table class="table table-striped table-bordered table-hover"><thead><tr><th>#</th><th>Product</th><th>Description</th><th>Price</th><th>Action</th></tr></thead><tbody>'+content+'</tbody></table></div><!-- /.table-responsive --></div><!-- /.panel -->';
			
			$('#tabContent'+cat_id).html('');
			$('#tabContent'+cat_id).append(html);
			$('#searchWord'+cat_id).val('');
			view = 1;
		}
		
	}
	
	
	
}

function searchTab(i){
	
	keyword = $('#searchWord'+i).val();
	
	$.ajax({
	  method: "POST",
	  url: "<?=base_url($init['langu'].'/agora/customer/catalog_search')?>",
	  data: { "category": i, "keyword": keyword },
	  dataType: "JSON"
	})
	.done(function(r) {
		
		if(r.status == 'OK'){
			$('#tabContent'+i).html('');
			
			var html ='';
			
			if(view == 2){
			
				for(j=0;j<r.result.length;j++){
				
					html +='<div class="panel-body"><div class="row"><span class="col-lg-3 col-sm-4 col-xs-12 pull-left"><img src="'+r.result[j].pic_path+'" class="img-responsive" /></span><div class="col-lg-9 clearfix"><h4>'+r.result[j].product_name+'</h4><p><strong>Price:</strong> $'+formatNumber(r.result[j].selling_price)+' per '+capitalizeFirstLetter(r.result[j].uom)+'</p><p>'+r.result[j].description+'</p></div></div><hr class="invisible"><div class="row clearfix"><div class="col-lg-6 col-sm-6"><div class="form-group input-group"><span class="input-group-addon"> Model </span><input id="model_name'+count_item+'" class="form-control" value="'+r.result[j].model_name+'" readonly="readonly"/></div></div><div class="col-lg-3 col-sm-3"><div class="form-group input-group"><input id="promo'+count_item+'" type="hidden" class="form-control" value="'+r.result[j].promo_val+'"><input id="p_id'+count_item+'" type="hidden" class="form-control" value="'+r.result[j].id+'"><input id="p_name'+count_item+'" type="hidden" class="form-control" value="'+r.result[j].product_name+'"><input id="qty'+count_item+'" class="form-control" value=""><input type="hidden" id="price'+count_item+'" class="form-control" value="'+r.result[j].selling_price+'"><span class="input-group-addon"> '+capitalizeFirstLetter(r.result[j].uom)+'(s)</span><input type="hidden" id="uom_val'+count_item+'" value="'+r.result[j].uom+'"></div></div><div class="col-lg-3 col-sm-3"><button class="btn btn-primary" onclick="addItem('+count_item+')">Add To Cart</button></div></div><!-- /.row --></div><!-- /.panel --><hr>';
					count_item++;
				}
			
			}else{
				
				var html = '';
				var content = '';
			
				for(j=0;j<r.result.length;j++){

					content +='<tr><td>'+(j+1)+'</td><td><a href="#">'+r.result[j].product_name+'</a></td><td>'+r.result[j].model_name+'</td><td>$'+formatNumber(r.result[j].selling_price)+' per '+capitalizeFirstLetter(r.result[j].uom)+'</td><td><!-- Button trigger modal --><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" data-productId="'+r.result[j].id+'" data-productName="'+r.result[j].product_name+'" data-modelName="'+r.result[j].model_name+'" data-price="'+r.result[j].selling_price+'" data-uom="'+r.result[j].uom+' data-promo="'+r.result[j].promo_val+'"><i class="fa fa-shopping-cart"></i> Add To Cart</button></td></tr>';
				
				}
			
				html +='<div class="panel-body"><div class="table-responsive"><table class="table table-striped table-bordered table-hover"><thead><tr><th>#</th><th>Product</th><th>Model</th><th>Price</th><th>Action</th></tr></thead><tbody>'+content+'</tbody></table></div><!-- /.table-responsive --></div><!-- /.panel -->';
			
			}
			
			$('#tabContent'+i).append(html);
		}
		
	});
	
}

function formatNumber(number)
{	
	var number = parseFloat(number);
    number = number.toFixed(2) + '';
    var x = number.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

$('#myModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var promo = button.data('promo')
  var id = button.data('productid')
  var name = button.data('productname')
  var model_name = button.data('modelname')
  var price = button.data('price')
  var uom = button.data('uom')
  
  var modal = $(this)
  modal.find('.modal-title').text('Add to Cart ' + name)
  modal.find('#promo').val(promo)
  modal.find('#p_id').val(id)
  modal.find('#p_name').val(name)
  modal.find('#model_name').val(model_name)
  modal.find('#price').val(price)
  modal.find('#uom').text(capitalizeFirstLetter(uom)+'(s)')
  modal.find('#uom_val').val(uom)
})

function addItem(i){
	
	var promo = $('#promo'+i).val();
	var p_id = $('#p_id'+i).val();
	var p_name = $('#p_name'+i).val();
	var qty = $('#qty'+i).val();
	var price = $('#price'+i).val();
	var model_name = $('#model_name'+i).val();
	var uom = $('#uom_val'+i).val();
	
	var check = 1;
	if(uom != 'kg' && qty%1!=0){
		check = 0;
	}
	
	if(check==1){
	
	if(qty != ''){
	
	$.ajax({
	  method: "POST",
	  url: "<?=base_url($init['langu'].'/agora/customer/catalog_addCart')?>",
	  data: { "p_name": p_name, "p_id": p_id, "qty": qty, "price": price, "model_name": model_name, "promo":promo },
	  dataType: "JSON"
	})
	.done(function(r) {
		
		if(r.status == 'OK'){
			
			$('#qty'+i).val('');
			$('#cart').html('');
			
			var html = '';
			for(var x=0;x<r.data.length;x++){
				html +='<li><a href="#"><div><strong>'+r.data[x].name+'</strong><span class="pull-right text-muted"><em>Qty: '+r.data[x].qty+'</em></span><div>'+r.data[x].model_name+'</div></div></a></li>';
			}
			
			html+='<li class="divider"></li><li><a class="text-center" href="'+'<?=base_url($init['langu'].'/agora/customer/cart')?>'+'"><strong>Shopping Cart</strong><i class="fa fa-angle-right"></i></a></li>';
			
			$('#cart').append(html);
			
			alert("Items have been added to Cart");
		}
		
	});
	
	}else{
		alert("Quantity cant be blank!");
	}
	
	}else{
		
		alert("Quantity wrong format!");
		
	}

}

function modal_addItem(){
	
	var promo = $('#promo').val();
	var p_id = $('#p_id').val();
	var p_name = $('#p_name').val();
	var qty = $('#qty').val();
	var price = $('#price').val();
	var model_name = $('#model_name').val();
	
	var uom = $('#uom_val').val();

	var check = 1;
	if(uom != 'kg' && qty%1!=0){
		check = 0;
	}
	
	if(check == 1){
	
	$.ajax({
	  method: "POST",
	  url: "<?=base_url($init['langu'].'/agora/customer/catalog_addCart')?>",
	  data: { "p_name": p_name, "p_id": p_id, "qty": qty, "price": price, "model_name": model_name, "promo":promo },
	  dataType: "JSON"
	})
	.done(function(r) {
		
		if(r.status == 'OK'){
			$('#cart').html('');
			
			var html = '';
			for(var x=0;x<r.data.length;x++){
				html +='<li><a href="#"><div><strong>'+r.data[x].name+'</strong><span class="pull-right text-muted"><em>Qty: '+r.data[x].qty+'</em></span><div>'+r.data[x].model_name+'</div></div></a></li>';
			}
			url ='';
			
			html+='<li class="divider"></li><li><a class="text-center" href="'+'<?=base_url($init['langu'].'/agora/customer/cart')?>'+'"><strong>Shopping Cart</strong><i class="fa fa-angle-right"></i></a></li>';
			
			$('#cart').append(html);

			$('#myModal').modal('hide');
			alert("Items have been added to Cart");
			$('#qty').val('');
		}
		
	});
	
	}else{
		
		alert('Quantity wrong format');
			
	}

}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
var cat_count = <?=count($result)?>;
//enter search
		$(document).ready(function() {
		  $(window).keydown(function(event){
			if(event.keyCode == 13) {
			  event.preventDefault();
			  
			  for(var i=1;i<=3;i++){
				  $('#searchWord'+i).blur();
			  }
			  

			  return false;
			}
		  });
		});

</script>