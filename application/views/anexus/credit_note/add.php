<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-usd fa-fw"></i> <?=$head_title?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                       		<!-- Button trigger modal -->
                            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" <?=$mode=='Edit'?'disabled':''?>>
                                Seach from Quotation
                            </button>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Search Quotation</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Search</label>
                                                <input class="form-control" placeholder="By Company Name or Reference No." id="search" name="search">
                                            </div>
                                            <table id="quotation_table" class="table table-striped table-bordered table-hover" id="dataTables">
                                            </table>
                                            <div id="pagination"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            
            <form action="<?=base_url($init['langu'].'/anexus/finance/credit_note_submit');?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="mode" value="<?=$mode?>"/>
            <input type="hidden" name="id" value="<?=$mode=='Edit'?$result['id']:''?>"/>
            <input type="hidden" id="quotation_id" name="quotation_id" value="<?=$mode=='Edit'?$result['quotation_id']:''?>"/>
            <input type="hidden" id="sale_person_id" name="sale_person_id" value="<?=$mode=='Edit'?$result['sales_person_id']:''?>"/>
            <input type="hidden" id="requestor_id" name="requestor_id" value="<?=$mode=='Edit'?$result['requestor_id']:$staff_info['id']?>"/>
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Invoice Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Invoice No.</label>
                                    <input class="form-control" value="<?=$mode=='Edit'?$result['invoice_no']:''?>" readonly>
                                    <p class="help-block"><i>Number will be assigned only after form submission</i></p>
                                </div>
                                <div class="form-group">
                                    <label>Date of Issue</label>
                                    <input id="issue_date" name="issue_date" class="form-control" placeholder="Calendar Input" value="<?=$mode=='Edit'?date('d/m/Y',strtotime($result['issue_date'])):''?>">
                                </div>
                                <div class="form-group">
                                    <label>Customer Purchase Order Ref</label>
                                    <input id="CP_Order_Ref" name="CP_Order_Ref" class="form-control" value="<?=$mode=='Edit'?$result['purchase_order_ref']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Originator</label>
                                    <input id="Originator" name="Originator" class="form-control" value="<?=$mode=='Edit'?$user_list[$result['sales_person_id']]:''?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Sales Order No.</label>
                                    <input id="S_Order_No" name="S_Order_No" class="form-control" value="<?=$mode=='Edit'?$result['sales_order_no']:''?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Delivery Order No.</label>
                                    <input id="DeliveryOrderNo" name="DeliveryOrderNo" class="form-control" value="<?=$mode=='Edit'?$result['delivery_order_no']:''?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Currency</label>
                                    <input id="Currency" name="Currency" class="form-control" value="<?=$mode=='Edit'?$result['currency']:''?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Terms of Payment</label>
                                    <input id="TermsofPayment" name="TermsofPayment" class="form-control"  value="<?=$mode=='Edit'?$result['term_of_payment']:''?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Payment Due Date</label>
                                    <input id="payment_date" name="payment_date" class="form-control" placeholder="Calendar Input" value="<?=$mode=='Edit'?date('d/m/Y',strtotime($result['payment_due_day'])):''?>">
                                </div>
                                
                                <div class="form-group">
                                    <label>Purchase Order No.</label>
                                    <input id="PurchaseOrderNo" name="PurchaseOrderNo" class="form-control" value="<?=$mode=='Edit'?$result['purchase_order_no']:''?>" readonly>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
                <div class="col-lg-6"> 
                    
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Customer Information
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Invoice To</label>
                                    <input id="invoiceTo" name="invoiceTo" class="form-control" value="<?=$mode=='Edit'?$result['invoice_to']:''?>" readonly>
                                    <input type="hidden" id="customer_id" name="customer_id" value="<?=$mode=='Edit'?$result['customer_id']:'0'?>">
                                </div>
                                <div class="form-group">
                                    <label>Account No.</label>
                                    <input id="accountNo" name="accountNo" class="form-control" value="<?=$mode=='Edit'?$result['account_no']:''?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Delivered To</label>
                                    <input id="DeliveredTo" name="DeliveredTo" class="form-control" value="<?=$mode=='Edit'?$result['delivered_to']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Attention To</label>
                                    <input id="AttentionTo" name="AttentionTo" class="form-control" value="<?=$mode=='Edit'?$result['primary_attention_to']:''?>">
                                </div>
                                <div class="form-group">
                                   	<label>Contact</label>
                                    <input id="Contact" name="Contact" class="form-control" value="<?=$mode=='Edit'?$result['primary_contact']:''?>">
                                </div>
                                <div class="form-group">
                                    <label>Attention To</label>
                                    <input id="AttentionTo2" name="AttentionTo2" class="form-control" value="<?=$mode=='Edit'?$result['secoundary_attention_to']:''?>">
                                </div>
                                <div class="form-group">
                                   	<label>Contact</label>
                                    <input id="Contact2" name="Contact2" class="form-control" value="<?=$mode=='Edit'?$result['secoundary_contact']:''?>">
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                
                
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Total Amount
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label>Sub-Total</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-usd"></i>
                                        </span>
                                        <input id="sub_total" name="sub_total" type="text" class="form-control" value="<?=$mode=='Edit'?$result['subtotal']:''?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>GST @ <?=$gst['value']?>%</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-usd"></i>
                                        </span>
                                        <input id="gst" name="gst" type="text" class="form-control" value="<?=$mode=='Edit'?$result['gst']:''?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Grand Total</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-usd"></i>
                                        </span>
                                        <input id="grand_total" name="grand_total" type="text" class="form-control" value="<?=$mode=='Edit'?$result['grand_total']:''?>" readonly>
                                    </div>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
                <!-- /.col-lg-6 -->
                
            </div>
            <!-- /.row -->
            <hr />
            
            <div class="row">
                <div class="col-lg-12">
                
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Commercial Invoice Items
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                            <input id="invoiceCount" name="invoiceCount" value="0" type="hidden"/>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th width="15%">UOM</th>
                                            <th>Quantity</th>
                                            <th width="20%">Item Description</th>
                                            <th>Unit Price</th>
                                            <th>Extended Price</th>
                                            <th>Total Price</th>
                                            <th width="80px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="invoice_item">
                                        
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                    
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->    
                        
                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Remarks
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <textarea id="remarks" name="remarks" class="form-control"><?=$mode=='Edit'?$result['remarks']:''?></textarea>
                                </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                	
                    <?php if($mode=='Add'){?>
                    
                    	<?php if($btn_type == 'create'){?>
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                        </div>
                    	<?php }?>
                        
                    <?php }else if($mode=='Edit'){?>
                    	<?php switch($btn_type){
							case 'edit':
							?>
                        		<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Submit</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            <?php 
							break;
							case 'approval':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-primary">Update</button>
                                	<button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'approval_update':
							?>
                            	<div class="form-group">	
                                   <label class="checkbox-inline">
                                     <input name="correct_check" type="checkbox"> I have checked the aboved information to be correct
                                    </label>
                                </div>
            
                                <div class="form-group">
                                	<button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                	<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                </div>
                            <?php 
							break;
							case 'sendmail_update':
							?>
                            	<div class="form-group">
                                	<button type="submit" class="btn btn-default">Save</button>
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/finance/cn_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/finance/cn_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                    <button type="cancel" class="btn btn-default" onClick="history.go(-1); return false;">Cancel</button>
                                </div>
                            
                            <?php 
							break;
							case 'send_email':
							?>
                            	<div class="form-group">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/finance/cn_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/finance/cn_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                                </div>
                            
                            <?php 
							break;
							case 'upload':
							?>
                            
                            	<div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/finance/cn_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/finance/cn_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file">
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Confirm to Next Step</button>
                                		<button type="submit" class="btn btn-danger" name="confirm" value="0">Not Approved</button>		
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            
                            <?php 
							break;
							case 'complete':
							?>
                            	<?php if($last_action == 'upload'){?>
                            	<div class="panel">
                                	<a class="btn btn-default" href="<?=base_url('en/anexus/finance/cn_pdf/'.$result['id'])?>" target="_blank">Download PDF</a>
                                	<button type="button" class="btn btn-default" onclick="sentMail('<?=base_url('en/anexus/finance/cn_send_mail/'.$result['id'])?>')">Send <?=$workflow_title?> in email</button>
                            	</div>
                            
                            	<div class="panel panel-primary">
                                	<div class="panel-heading">
                                    <?=$workflow_title?> Confirmation by Customer
                                	</div>
                                	<div class="panel-body">
                                        <div class="form-group input-group">
                                            <label>Upload Confirmation Document</label>
                                            <input id="comfirm_file" name="comfirm_file" type="file"> 
                                            <a target="_blank" href="<?=$result['comfirm_file']?>"><?=$result['comfirm_file']?></a>
                                        </div>
                                        <button type="submit" class="btn btn-success" name="confirm" value="1">Reupload</button>
                                 	</div>
                				<!-- /.panel-body -->
                    			</div>
                            	<?php }?>
                            <?php 
							break;
                        }?>

                    <div class="panel panel-primary">
                    	<div class="panel-heading">
                            Status
                        </div>
                        <div class="panel-body">
                            <div class="form-group input-group">
							<?php 
                                //如果不是not approved, 也不是 completed 就顯示目前正在等誰
                                if($result['status'] != 0 && $result['status'] != $completed_status_id){
                            ?>
                            <label>
                                                    <?php
                                                    if(!empty($result['awaiting_table'])){
                                                        echo 'Awaiting ';
                                                        foreach($result['awaiting_table'] as $x){
                                                            if(isset($role_list[$x])){
                                                            echo '<br>'.$role_list[$x];
                                                            }
                                                        }
                                                    }
                                                    ?>
                            </label><br>
                            <?php 
                            } 
                            ?>
                            <label><?=$cn_status_list[$result['status']]?></label>
                            </div>
                         </div>
                		 <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                    
                    <?php } //Edit mode end ?>
                    
                    
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            </form>
            
        </div>
        <!-- /#page-wrapper -->
		<script>
		var mode = '<?=$mode?>';
		var invoice_item = <?=$mode=='Edit'?$invoice_item:'[]'?>;
		var btn_type = '<?=$btn_type?>';
		
		if(mode == 'Edit'){
			for(var i=0;i<invoice_item.length;i++){
				
				html='<tr><td>'+(i+1)+'<input name="product_id'+i+'" type="hidden" value="'+invoice_item[i].product_id+'"></td><td><input name="uom'+i+'" class="form-control" value="'+invoice_item[i].uom+'" readonly></td><td><input name="quantity'+i+'" class="form-control" value="'+invoice_item[i].quantity+'" readonly></td><td><textarea name="description'+i+'" class="form-control" readonly>'+invoice_item[i].Item_description+'</textarea></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" name="unitPrice'+i+'" class="form-control" value="'+invoice_item[i].unit_price+'" readonly></div></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" class="form-control" name="extendPrice'+i+'" value="'+invoice_item[i].extended_price+'" readonly></div></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" class="form-control" name="total_price'+i+'" value="'+invoice_item[i].total_price+'" readonly></div></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></td></tr>';	
						
				$('#invoice_item').append(html);
				$('#invoiceCount').val(i);
				
			}
			
			//change input to read only
					if(btn_type != 'create' && btn_type != 'edit' && btn_type != 'sendmail_update'){
						
						$(".form-control").each(function() {
						  $(this).prop('disabled', 'disabled');
						});
						
						$(".btn-circle").each(function() {
						  $(this).hide();
						});
						$('#addProduct').hide();
						$('#addService').hide();
						if(btn_type == 'update' || btn_type == 'approval_update' ){
							
							var target_colum = <?=isset($target_colum)?$target_colum:'[]'?>;
							
							for(var i=0;i<target_colum.length;i++){
								var colum = target_colum[i].name;
								if(target_colum[i].type == 'single'){
									$("input[name='"+colum+"']").prop('disabled', false);
									
								}else if(target_colum[i].type == 'multiple'){
									$("."+colum).prop('disabled', false);
								}
							}
							
							
						}
					}
		}
		
		$( "#search" ).blur(function(){
			ajax_quotation(1);
		});
		
		$('#myModal').on('show.bs.modal', function (event) {
			ajax_quotation(1);
		})
		
		function changePage(i){
			ajax_quotation(i);
		}
		
        function ajax_quotation(i){
			keyword = $("#search").val();
			term = {keyword:keyword,limit:10,page:i};
			url = '<?=base_url($init['langu'].'/anexus/finance/getQuotation')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					//console.log(r);
					tmp = '';
					
					if(r.data != false){
					for(var i=0;i<r.data.length;i++){
						
						tmp+='<tr><td><a href="javascript:getQuotation('+r.data[i].id+')">'+r.data[i].customer_name+'</a></td><td>'+r.data[i].qr_serial+'</td><td>'+r.data[i].modified_date+'</td><td>'+r.data[i].status+'</td></tr>';
					
					}
					
					html = '<thead><tr><th>Company Name</th><th>Reference No.</th><th>Date Last Updated</th><th>Status</th></tr></thead><tbody>'+tmp+'</tbody>';
					
					pagination = r.paging;
					
					$('#quotation_table').html('');
					$('#pagination').html('');
					$('#quotation_table').append(html);
					$('#pagination').append(pagination);
					}else{
						$('#quotation_table').html('No search result');
						$('#pagination').html('');
					}
					
				}
			});	
		}
		
		function getQuotation(id){
			var dateRequisition = '';
			var html = '';
			var temp_item = '';
			var company_name = '';
			var pic = '';
			
			$('#myModal').modal('hide');
			
			term = {id:id};
			url = '<?=base_url($init['langu'].'/anexus/finance/getQuotationData')?>';
			
			$.ajax({
			  type:"POST",
			  url: url,
			  data: term,
			  dataType : "json"
			}).done(function(r) {
				if(r.status == "OK") {
					console.log(r);
					
					$('#quotation_id').val(r.data.id);
					$('#sale_person_id').val(r.data.create_user_id);
					
					//invoice info
					$('#Originator').val(r.invoiceInfo.Originator);
					$('#S_Order_No').val(r.invoiceInfo.SalesOrderNo);
					$('#DeliveryOrderNo').val(r.invoiceInfo.DeliveryOrderNo);
					$('#Currency').val(r.customer_data.currency);
					$('#TermsofPayment').val(r.invoiceInfo.TermsofPayment);
					$('#PurchaseOrderNo').val(r.invoiceInfo.PurchaseOrderNo);
					
					//customer info
					$('#customer_id').val(r.customer_data.id);
					$('#invoiceTo').val(r.customer_data.company_name);
					$('#accountNo').val(r.customer_data.account_no);
					$('#DeliveredTo').val(r.customer_data.delivery_address);
					$('#AttentionTo').val(r.customer_data.primary_attention_to);
					$('#Contact').val(r.customer_data.primary_contact_info);
					$('#AttentionTo2').val(r.customer_data.secondary_attention_to);
					$('#Contact2').val(r.customer_data.secondary_contact_info);
					
					//total amount
					var sub_total = r.data.total_price;
					var gst_value =<?=$gst['value']?>;
					var gst = Math.round((r.data.total_price*(gst_value/1000))*100)/100;
					var grand_total = sub_total+gst;
					
					$('#sub_total').val(sub_total);
					$('#gst').val(gst);
					$('#grand_total').val(grand_total);
					
					//invoice item
					$('#invoice_item').html('');
					for(var i=0;i<r.qutation_related_item.length;i++){
						unitPrice = r.qutation_related_item[i].unit_price;
						quantity = r.qutation_related_item[i].quantity;
						extendPrice = r.qutation_related_item[i].extended_price;
						totalPrice = (unitPrice*quantity)+parseInt(extendPrice);
						
						if(extendPrice == 0){
							extendPrice='-';	
						}
						
						html='<tr><td>'+(i+1)+'<input name="product_id'+i+'" type="hidden" value="'+r.qutation_related_item[i].product_id+'"></td><td><input name="uom'+i+'" class="form-control" value="'+r.qutation_related_item[i].uom+'" readonly></td><td><input name="quantity'+i+'" class="form-control" value="'+quantity+'" readonly></td><td><textarea name="description'+i+'" class="form-control" readonly>'+r.qutation_related_item[i].description+'</textarea></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" name="unitPrice'+i+'" class="form-control" value="'+unitPrice+'" readonly></div></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" class="form-control" name="extendPrice'+i+'" value="'+extendPrice+'" readonly></div></td><td><div class="form-group input-group"><span class="input-group-addon"><i class="fa fa-usd"></i></span><input type="text" class="form-control" name="total_price'+i+'" value="'+totalPrice+'" readonly></div></td><td><button class="btn btn-primary btn-circle btn-default" data-placement="top" data-toggle="tooltip" data-original-title="View More" type="button"><i class="fa fa-folder-open-o"></i></button></td></tr>';	
						
						$('#invoice_item').append(html);
						$('#invoiceCount').val(i);
					}
					
					
				}
			});	
			
		}
		
		$( "#issue_date" ).datepicker({
		dateFormat:'dd/mm/yy',
		});
		
		$( "#payment_date" ).datepicker({
		dateFormat:'dd/mm/yy',
		});
		
		//set datepicker get current date
		if(mode == 'Add'){
			 $('#issue_date').datepicker('setDate', 'today');
			 $('#payment_date').datepicker('setDate', 'today');
		}
		
		function sentMail(url){
			location.href=url;
		}
        </script>