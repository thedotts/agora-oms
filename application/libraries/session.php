<?php
class session {

    function session(){
        $this->_set_session();
    }

    function _set_session(){
        session_start();
    }

    function set_userdata($session_name,$session_value){
        $_SESSION[$session_name] = $session_value;
    }

    function userdata($session_name){
        if(isset($_SESSION[$session_name]))
            return $_SESSION[$session_name];
        return false;
    }

    function unset_userdata($session_name){
        if(isset($_SESSION[$session_name]))
            unset($_SESSION[$session_name]);
    }

}
?>