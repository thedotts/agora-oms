<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/


//登入頁面, 登入後送出的頁面
$route['^(en|zh)/login'] = "login"; 
$route['^(en|zh)/forgot_pw'] = "login/forgot_pw"; 
$route['^(en|zh)/forgot_pw/(:num)'] = "login/forgot_pw/$2";
$route['^(en|zh)/reset_email'] = "login/reset_email";
$route['^(en|zh)/reset_pw/(:any)'] = "login/reset_pw/$2";
$route['^(en|zh)/reset_submit'] = "login/reset_submit";
$route['^(en|zh)/reset_done/(:num)'] = "login/reset_done/$2";
$route['^(en|zh)/logout'] = "logout";

//Installer / Configurator
$route['default_controller'] = "configurator";
$route['^(en|zh)$'] = "configurator";
$route['^(en|zh)/configurator/dashboard'] = "configurator/dashboard";
$route['^(en|zh)/configurator/access'] = "configurator/access";
$route['^(en|zh)/configurator/complete'] = "configurator/complete";
$route['^(en|zh)/configurator/data'] = "configurator/data";
$route['^(en|zh)/configurator/roles'] = "configurator/roles";
$route['^(en|zh)/configurator/settings'] = "configurator/settings";
$route['^(en|zh)/configurator/workflow'] = "configurator/workflow";
$route['^(en|zh)/configurator/workflow_order'] = "configurator/workflow_order";
$route['^(en|zh)/configurator/db_connect_test'] = "configurator/db_connect_test";
$route['^(en|zh)/configurator/save_config'] = "configurator/saving_config";
$route['^(en|zh)/configurator/upload'] = "configurator_upload/index";

//Loading Page for installation
$route['^(en|zh)/loading'] = "loading";

//Anexus Core
$route['^(en|zh)/agora'] = "anexus";
$route['^(en|zh)/agora/tasklist'] = "anexus/tasklist";
$route['^(en|zh)/agora/tasklist/(:any)'] = "anexus/tasklist/$2";
$route['^(en|zh)/agora/task_update'] = "anexus/task_update";

$route['^(en|zh)/agora/profile'] = "profile";
$route['^(en|zh)/agora/profile/success'] = "profile/index/success";
$route['^(en|zh)/agora/profile_submit'] = "profile/submit";

//Sales 

//Tender
$route['^(en|zh)/anexus/sales/tender'] = "tender_manage";
$route['^(en|zh)/anexus/sales/tender/(:any)'] = "tender_manage/index/$2";
$route['^(en|zh)/anexus/sales/tender/(:any)/(:num)'] = "tender_manage/index/$2/$3";
$route['^(en|zh)/anexus/sales/tender_add'] = "tender_manage/add";
$route['^(en|zh)/anexus/sales/tender_edit/(:num)'] = "tender_manage/add/$2";
$route['^(en|zh)/anexus/sales/tender_submit'] = "tender_manage/submit";
$route['^(en|zh)/anexus/sales/tender_del/(:num)'] = "tender_manage/del/$2";
$route['^(en|zh)/anexus/sales/getQuotation_tender'] = "tender_manage/ajax_getQuotation";
$route['^(en|zh)/anexus/sales/getQuotationData_tender'] = "tender_manage/ajax_getQuotationData";
$route['^(en|zh)/anexus/sales/tender_estShipping'] = "tender_manage/ajax_estShipping";
$route['^(en|zh)/anexus/sales/getModel'] = "tender_manage/ajax_getModel";
$route['^(en|zh)/anexus/sales/getProductDetail'] = "tender_manage/ajax_getProductDetail";
$route['^(en|zh)/anexus/sales/tender_pdf/(:num)'] = "tender_manage/export_pdf/$2";
$route['^(en|zh)/anexus/sales/tender_send_mail/(:num)'] = "tender_manage/sent_mail/$2";

//Order
$route['^(en|zh)/agora/sales/order'] = "order2_manage";
$route['^(en|zh)/agora/sales/order/(:any)'] = "order2_manage/index/$2";
$route['^(en|zh)/agora/sales/order/(:any)/(:num)/(:num)'] = "order2_manage/index/$2/$3/$4";
$route['^(en|zh)/agora/sales/order_add'] = "order2_manage/add";
$route['^(en|zh)/agora/sales/order_edit/(:num)'] = "order2_manage/add/$2";
$route['^(en|zh)/agora/sales/order_submit'] = "order2_manage/submit";
$route['^(en|zh)/agora/sales/order_del/(:num)'] = "order2_manage/del/$2";
$route['^(en|zh)/agora/sales/order_pdf/(:num)'] = "order2_manage/export_pdf/$2";
$route['^(en|zh)/agora/sales/searchProduct/(:any)'] = "order2_manage/searchProduct/$2";

//Packing List
$route['^(en|zh)/agora/ground_staff/packing_list'] = "packing_list_manage";
$route['^(en|zh)/agora/ground_staff/packing_list/(:any)'] = "packing_list_manage/index/$2";
$route['^(en|zh)/agora/ground_staff/packing_list/(:any)/(:num)/(:num)'] = "packing_list_manage/index/$2/$3/$4";
$route['^(en|zh)/agora/ground_staff/packing_list_add'] = "packing_list_manage/add";
$route['^(en|zh)/agora/ground_staff/packing_list_add/(:num)/(:num)'] = "packing_list_manage/add/$2/$3";
$route['^(en|zh)/agora/ground_staff/packing_list_edit/(:num)'] = "packing_list_manage/add/$2";
$route['^(en|zh)/agora/ground_staff/packing_list_submit'] = "packing_list_manage/submit";
$route['^(en|zh)/agora/ground_staff/packing_list_del/(:num)'] = "packing_list_manage/del/$2";
$route['^(en|zh)/agora/ground_staff/getSearch'] = "packing_list_manage/ajax_getSearch";
$route['^(en|zh)/agora/ground_staff/getSearchData'] = "packing_list_manage/ajax_getSearchData";
$route['^(en|zh)/agora/ground_staff/packing_list_pdf/(:num)'] = "packing_list_manage/export_pdf/$2";


//Quotation 
$route['^(en|zh)/anexus/sales/quotation'] = "quotation_manage";
$route['^(en|zh)/anexus/sales/quotation/(:any)/(:any)'] = "quotation_manage/index/$2/$3";
$route['^(en|zh)/anexus/sales/quotation/(:any)/(:any)/(:num)'] = "quotation_manage/index/$2/$3/$4";
$route['^(en|zh)/anexus/sales/quotation_add'] = "quotation_manage/add";
$route['^(en|zh)/anexus/sales/quotation_edit/(:num)'] = "quotation_manage/add/$2";
$route['^(en|zh)/anexus/sales/quotation_submit'] = "quotation_manage/submit";
$route['^(en|zh)/anexus/sales/quotation_del/(:num)'] = "quotation_manage/del/$2";
$route['^(en|zh)/anexus/sales/quotation_estShipping'] = "tender_manage/ajax_estShipping";
$route['^(en|zh)/anexus/sales/getModel'] = "tender_manage/ajax_getModel";
$route['^(en|zh)/anexus/sales/getProductDetail'] = "tender_manage/ajax_getProductDetail";
$route['^(en|zh)/anexus/sales/quotation_pdf/(:num)'] = "quotation_manage/export_pdf/$2";
$route['^(en|zh)/anexus/sales/quotation_send_mail/(:num)/(:num)/(:num)'] = "quotation_manage/sent_mail/$2/$3/$4";

//Sales (Purchase) (TODO)
$route['^(en|zh)/anexus/sales/purchase'] = "purchase_request_manage";
$route['^(en|zh)/anexus/sales/purchase/(:any)/(:any)'] = "purchase_request_manage/index/$2/$3";
$route['^(en|zh)/anexus/sales/purchase/(:any)/(:any)/(:num)'] = "purchase_request_manage/index/$2/$3/$4";
$route['^(en|zh)/anexus/sales/purchase_add'] = "purchase_request_manage/add";
$route['^(en|zh)/anexus/sales/purchase_add/(:num)/(:num)'] = "purchase_request_manage/add/$2/$3";
$route['^(en|zh)/anexus/sales/purchase_edit/(:num)'] = "purchase_request_manage/add/$2";
$route['^(en|zh)/anexus/sales/purchase_submit'] = "purchase_request_manage/submit";
$route['^(en|zh)/anexus/sales/purchase_del/(:num)'] = "purchase_request_manage/del/$2";
$route['^(en|zh)/anexus/sales/getQuotation'] = "purchase_request_manage/ajax_getQuotation";
$route['^(en|zh)/anexus/sales/getQuotationData'] = "purchase_request_manage/ajax_getQuotationData";
$route['^(en|zh)/anexus/sales/getSupplierData'] = "purchase_request_manage/ajax_getSupplierData";
$route['^(en|zh)/anexus/sales/pr_pdf/(:num)'] = "purchase_request_manage/export_pdf/$2";
$route['^(en|zh)/anexus/sales/pr_send_mail/(:num)'] = "purchase_request_manage/sent_mail/$2";
$route['^(en|zh)/anexus/sales/getSupplier'] = "purchase_request_manage/ajax_getSupplier";
$route['^(en|zh)/anexus/sales/getItem'] = "purchase_request_manage/ajax_getItem";
$route['^(en|zh)/anexus/sales/getItemDetail'] = "purchase_request_manage/ajax_getItemDetail";
$route['^(en|zh)/anexus/sales/getCompanyDetail'] = "purchase_request_manage/ajax_getCompanyDetail";
$route['^(en|zh)/anexus/sales/pr_pdf/(:num)/(:num)'] = "purchase_request_manage/export_pdf/$2/$3";
$route['^(en|zh)/anexus/sales/pr_send_mail/(:num)/(:num)'] = "purchase_request_manage/sent_mail/$2/$3";

//Sales (Service Request)
$route['^(en|zh)/anexus/sales/service_request'] = "service_request_manage";
$route['^(en|zh)/anexus/sales/service_request/(:any)/(:any)'] = "service_request_manage/index/$2/$3";
$route['^(en|zh)/anexus/sales/service_request/(:any)/(:any)/(:num)'] = "service_request_manage/index/$2/$3/$4";
$route['^(en|zh)/anexus/sales/service_request_add'] = "service_request_manage/add";
$route['^(en|zh)/anexus/sales/service_request_add/(:num)/(:num)'] = "service_request_manage/add/$2/$3";
$route['^(en|zh)/anexus/sales/service_request_edit/(:num)'] = "service_request_manage/add/$2";
$route['^(en|zh)/anexus/sales/service_request_submit'] = "service_request_manage/submit";
$route['^(en|zh)/anexus/sales/service_request_del/(:num)'] = "service_request_manage/del/$2";
$route['^(en|zh)/anexus/sales/getQuotationService'] = "service_request_manage/ajax_getQuotation";
$route['^(en|zh)/anexus/sales/getQuotationDataService'] = "service_request_manage/ajax_getQuotationData";

//Logistics

//Commercial invoice (TODO)
$route['^(en|zh)/anexus/logistics/commercial_invoice'] = "commercial_invoice_manage";
$route['^(en|zh)/anexus/logistics/commercial_invoice/(:any)'] = "commercial_invoice_manage/index/$2";
$route['^(en|zh)/anexus/logistics/commercial_invoice/(:any)/(:num)'] = "commercial_invoice_manage/index/$2/$3";
$route['^(en|zh)/anexus/logistics/commercial_invoice_add'] = "commercial_invoice_manage/add";
$route['^(en|zh)/anexus/logistics/commercial_invoice_edit/(:num)'] = "commercial_invoice_manage/add/$2";
$route['^(en|zh)/anexus/logistics/commercial_invoice_submit'] = "commercial_invoice_manage/submit";
$route['^(en|zh)/anexus/logistics/commercial_invoice_del/(:num)'] = "commercial_invoice_manage/del/$2";
$route['^(en|zh)/anexus/logistics/getQuotation'] = "commercial_invoice_manage/ajax_getQuotation";
$route['^(en|zh)/anexus/logistics/getQuotationData'] = "commercial_invoice_manage/ajax_getQuotationData";
$route['^(en|zh)/anexus/logistics/ci_pdf/(:num)'] = "commercial_invoice_manage/export_pdf/$2";
$route['^(en|zh)/anexus/logistics/ci_send_mail/(:num)'] = "commercial_invoice_manage/sent_mail/$2";
$route['^(en|zh)/anexus/logistics/getItemDetail'] = "commercial_invoice_manage/getItemDetail";

//delivery order
$route['^(en|zh)/agora/admin/delivery_order'] = "delivery_order_manage";
$route['^(en|zh)/agora/admin/delivery_order/(:any)'] = "delivery_order_manage/index/$2";
$route['^(en|zh)/agora/admin/delivery_order/(:any)/(:num)/(:num)'] = "delivery_order_manage/index/$2/$3/$4";
$route['^(en|zh)/agora/admin/delivery_order_add'] = "delivery_order_manage/add";
$route['^(en|zh)/agora/admin/delivery_order_add/(:num)/(:num)'] = "delivery_order_manage/add/$2/$3";
$route['^(en|zh)/agora/admin/delivery_order_edit/(:num)'] = "delivery_order_manage/add/$2";
$route['^(en|zh)/agora/admin/delivery_order_submit'] = "delivery_order_manage/submit";
$route['^(en|zh)/agora/admin/delivery_order_del/(:num)'] = "delivery_order_manage/del/$2";
$route['^(en|zh)/agora/admin/getSearch'] = "delivery_order_manage/ajax_getSearch";
$route['^(en|zh)/agora/admin/getSearchData'] = "delivery_order_manage/ajax_getSearchData";
$route['^(en|zh)/agora/admin/do_pdf/(:num)'] = "delivery_order_manage/export_pdf/$2";

//Service

//Field Service Order (TODO)
$route['^(en|zh)/anexus/service/field_service_order'] = "field_service_order_manage";
$route['^(en|zh)/anexus/service/field_service_order/(:any)/(:any)'] = "field_service_order_manage/index/$2/$3";
$route['^(en|zh)/anexus/service/field_service_order/(:any)/(:any)/(:num)'] = "field_service_order_manage/index/$2/$3/$4";
$route['^(en|zh)/anexus/service/field_service_order_add'] = "field_service_order_manage/add";
$route['^(en|zh)/anexus/service/field_service_order_edit/(:num)'] = "field_service_order_manage/add/$2";
$route['^(en|zh)/anexus/service/field_service_order_submit'] = "field_service_order_manage/submit";
$route['^(en|zh)/anexus/service/field_service_order_del/(:num)'] = "field_service_order_manage/del/$2";
$route['^(en|zh)/anexus/service/getServiceRequest'] = "field_service_order_manage/ajax_getServiceRequest";
$route['^(en|zh)/anexus/service/getSR'] = "field_service_order_manage/ajax_getSR";
$route['^(en|zh)/anexus/service/fsr_pdf/(:num)'] = "field_service_order_manage/export_pdf/$2";
$route['^(en|zh)/anexus/service/fsr_send_mail/(:num)'] = "field_service_order_manage/sent_mail/$2";

//Inventory Request (TODO, 缺create的UI, 要從DOC上面去思考怎麼做)
$route['^(en|zh)/anexus/service/inventory_request'] = "inventory_request_manage";
$route['^(en|zh)/anexus/service/inventory_request/(:any)'] = "inventory_request_manage/index/$2";
$route['^(en|zh)/anexus/service/inventory_request/(:any)/(:num)'] = "inventory_request_manage/index/$2/$3";
$route['^(en|zh)/anexus/service/inventory_request_add'] = "inventory_request_manage/add";
$route['^(en|zh)/anexus/service/inventory_request_edit/(:num)'] = "inventory_request_manage/add/$2";
$route['^(en|zh)/anexus/service/inventory_request_submit'] = "inventory_request_manage/submit";
$route['^(en|zh)/anexus/service/inventory_request_del/(:num)'] = "inventory_request_manage/del/$2";

//Return Material Authorization (TODO)
$route['^(en|zh)/anexus/service/return_material_authorization'] = "return_material_authorization_manage";
$route['^(en|zh)/anexus/service/return_material_authorization/(:any)'] = "return_material_authorization_manage/index/$2";
$route['^(en|zh)/anexus/service/return_material_authorization/(:any)/(:num)'] = "return_material_authorization_manage/index/$2/$3";
$route['^(en|zh)/anexus/service/return_material_authorization_add'] = "return_material_authorization_manage/add";
$route['^(en|zh)/anexus/service/return_material_authorization_edit/(:num)'] = "return_material_authorization_manage/add/$2";
$route['^(en|zh)/anexus/service/return_material_authorization_submit'] = "return_material_authorization_manage/submit";
$route['^(en|zh)/anexus/service/return_material_authorization_del/(:num)'] = "return_material_authorization_manage/del/$2";
$route['^(en|zh)/anexus/service/getQuotation'] = "return_material_authorization_manage/ajax_getQuotation";
$route['^(en|zh)/anexus/service/getQuotationData'] = "return_material_authorization_manage/ajax_getQuotationData";
$route['^(en|zh)/anexus/service/return_material_authorization_pdf/(:num)/(:num)'] = "return_material_authorization_manage/export_pdf/$2/$3";
$route['^(en|zh)/anexus/service/return_material_authorization_send_mail/(:num)/(:num)'] = "return_material_authorization_manage/sent_mail/$2/$3";

//Finance

//Invoice
$route['^(en|zh)/agora/finance/invoice'] = "invoice_manage";
$route['^(en|zh)/agora/finance/invoice/(:any)'] = "invoice_manage/index/$2";
$route['^(en|zh)/agora/finance/invoice/(:any)/(:num)/(:num)'] = "invoice_manage/index/$2/$3/$4";
$route['^(en|zh)/agora/finance/invoice_add'] = "invoice_manage/add";
$route['^(en|zh)/agora/finance/invoice_add/(:num)/(:num)'] = "invoice_manage/add/$2/$3";
$route['^(en|zh)/agora/finance/invoice_edit/(:num)'] = "invoice_manage/add/$2";
$route['^(en|zh)/agora/finance/invoice_submit'] = "invoice_manage/submit";
$route['^(en|zh)/agora/finance/invoice_del/(:num)'] = "invoice_manage/del/$2";
$route['^(en|zh)/agora/finance/getSearch'] = "invoice_manage/ajax_getSearch";
$route['^(en|zh)/agora/finance/getSearchData'] = "invoice_manage/ajax_getSearchData";
$route['^(en|zh)/agora/finance/invoice_pdf/(:num)'] = "invoice_manage/export_pdf/$2";

//Credit Note (TODO)
$route['^(en|zh)/anexus/finance/credit_note'] = "credit_note_manage";
$route['^(en|zh)/anexus/finance/credit_note/(:any)'] = "credit_note_manage/index/$2";
$route['^(en|zh)/anexus/finance/credit_note/(:any)/(:num)'] = "credit_note_manage/index/$2/$3";
$route['^(en|zh)/anexus/finance/credit_note_add'] = "credit_note_manage/add";
$route['^(en|zh)/anexus/finance/credit_note_edit/(:num)'] = "credit_note_manage/add/$2";
$route['^(en|zh)/anexus/finance/credit_note_submit'] = "credit_note_manage/submit";
$route['^(en|zh)/anexus/finance/credit_note_del/(:num)'] = "credit_note_manage/del/$2";
$route['^(en|zh)/anexus/finance/getQuotation'] = "credit_note_manage/ajax_getQuotation";
$route['^(en|zh)/anexus/finance/getQuotationData'] = "credit_note_manage/ajax_getQuotationData";
$route['^(en|zh)/anexus/finance/cn_pdf/(:num)'] = "credit_note_manage/export_pdf/$2";
$route['^(en|zh)/anexus/finance/cn_send_mail/(:num)'] = "credit_note_manage/sent_mail/$2";

//Business Trip (TODO)
$route['^(en|zh)/anexus/finance/business_trip'] = "business_trip_manage";
$route['^(en|zh)/anexus/finance/business_trip/(:any)'] = "business_trip_manage/index/$2";
$route['^(en|zh)/anexus/finance/business_trip/(:any)/(:num)'] = "business_trip_manage/index/$2/$3";
$route['^(en|zh)/anexus/finance/business_trip_add'] = "business_trip_manage/add";
$route['^(en|zh)/anexus/finance/business_trip_edit/(:num)'] = "business_trip_manage/add/$2";
$route['^(en|zh)/anexus/finance/business_trip_submit'] = "business_trip_manage/submit";
$route['^(en|zh)/anexus/finance/business_trip_del/(:num)'] = "business_trip_manage/del/$2";

//Administrator

//Manage Department
$route['^(en|zh)/anexus/administrator/department'] = "department_manage";
$route['^(en|zh)/anexus/administrator/department/(:any)'] = "department_manage/index/$2";
$route['^(en|zh)/anexus/administrator/department/(:any)/(:num)'] = "department_manage/index/$2/$3";
$route['^(en|zh)/anexus/administrator/department_add'] = "department_manage/add";
$route['^(en|zh)/anexus/administrator/department_edit/(:num)'] = "department_manage/add/$2";
$route['^(en|zh)/anexus/administrator/department_del/(:num)'] = "department_manage/del/$2";
$route['^(en|zh)/anexus/administrator/department_submit'] = "department_manage/submit";


//Manage Employee
$route['^(en|zh)/agora/administrator/employee'] = "employee_manage";
$route['^(en|zh)/agora/administrator/employee/(:any)'] = "employee_manage/index/$2";
$route['^(en|zh)/agora/administrator/employee/(:any)/(:num)/(:num)'] = "employee_manage/index/$2/$3/$4";
$route['^(en|zh)/agora/administrator/employee_add'] = "employee_manage/add";
$route['^(en|zh)/agora/administrator/employee_edit/(:num)'] = "employee_manage/add/$2";
$route['^(en|zh)/agora/administrator/employee_del/(:num)'] = "employee_manage/del/$2";
$route['^(en|zh)/agora/administrator/employee_submit'] = "employee_manage/submit";
$route['^(en|zh)/agora/administrator/employee_check_email'] = "employee_manage/check_email";

//Manage Customer
$route['^(en|zh)/agora/administrator/customers'] = "customers_manage";
$route['^(en|zh)/agora/administrator/customers/(:any)'] = "customers_manage/index/$2";
$route['^(en|zh)/agora/administrator/customers/(:any)/(:num)/(:num)'] = "customers_manage/index/$2/$3/$4";
$route['^(en|zh)/agora/administrator/customers_add'] = "customers_manage/add";
$route['^(en|zh)/agora/administrator/customers_view/(:num)'] = "customers_manage/view/$2";
$route['^(en|zh)/agora/administrator/customers_edit/(:num)'] = "customers_manage/add/$2";
$route['^(en|zh)/agora/administrator/customers_del/(:num)'] = "customers_manage/del/$2";
$route['^(en|zh)/agora/administrator/customers_submit'] = "customers_manage/submit";
$route['^(en|zh)/agora/administrator/customers_all'] = "customers_manage/all_customer";
$route['^(en|zh)/agora/administrator/customers_all/(:any)'] = "customers_manage/all_customer/$2";
$route['^(en|zh)/agora/administrator/customer_check_email'] = "customers_manage/check_email";

//unpack list
$route['^(en|zh)/agora/administrator/unpack_list'] = "packing_list_manage/unpack_list";
$route['^(en|zh)/agora/administrator/unpack_pdf'] = "packing_list_manage/unpack_pdf";

//assign customer
$route['^(en|zh)/agora/manager/assign_customer'] = "customers_manage/assign_customer";
$route['^(en|zh)/agora/manager/assign_customer_submit'] = "customers_manage/assign_customer_submit";



//Reports
$route['^(en|zh)/agora/manager/reports'] = "report_manage";
$route['^(en|zh)/agora/manager/order_report'] = "report_manage/ajax_order";
$route['^(en|zh)/agora/manager/product_report'] = "report_manage/ajax_product";

//Manage Suppliers
$route['^(en|zh)/anexus/administrator/suppliers'] = "suppliers_manage";
$route['^(en|zh)/anexus/administrator/suppliers/(:any)'] = "suppliers_manage/index/$2";
$route['^(en|zh)/anexus/administrator/suppliers/(:any)/(:num)'] = "suppliers_manage/index/$2/$3";
$route['^(en|zh)/anexus/administrator/suppliers_add'] = "suppliers_manage/add";
$route['^(en|zh)/anexus/administrator/suppliers_view/(:num)'] = "suppliers_manage/view/$2";
$route['^(en|zh)/anexus/administrator/suppliers_edit/(:num)'] = "suppliers_manage/add/$2";
$route['^(en|zh)/anexus/administrator/suppliers_del/(:num)'] = "suppliers_manage/del/$2";
$route['^(en|zh)/anexus/administrator/suppliers_submit'] = "suppliers_manage/submit";

//Manage Product Category
$route['^(en|zh)/agora/administrator/product_category'] = "product_category_manage";
$route['^(en|zh)/agora/administrator/product_category/(:any)'] = "product_category_manage/index/$2";
$route['^(en|zh)/agora/administrator/product_category/(:any)/(:num)/(:num)'] = "product_category_manage/index/$2/$3/$4";
$route['^(en|zh)/agora/administrator/product_category_add'] = "product_category_manage/add";
$route['^(en|zh)/agora/administrator/product_category_view/(:num)'] = "product_category_manage/view/$2";
$route['^(en|zh)/agora/administrator/product_category_edit/(:num)'] = "product_category_manage/add/$2";
$route['^(en|zh)/agora/administrator/product_category_del/(:num)'] = "product_category_manage/del/$2";
$route['^(en|zh)/agora/administrator/product_category_submit'] = "product_category_manage/submit";
$route['^(en|zh)/agora/administrator/product_category_check'] = "product_category_manage/check_category";

//Manage Products
$route['^(en|zh)/agora/administrator/products'] = "products_manage";
$route['^(en|zh)/agora/administrator/products/(:any)'] = "products_manage/index/$2";
$route['^(en|zh)/agora/administrator/products/(:any)/(:num)/(:num)'] = "products_manage/index/$2/$3/$4";
$route['^(en|zh)/agora/administrator/products_add'] = "products_manage/add";
$route['^(en|zh)/agora/administrator/products_view/(:num)/(:num)'] = "products_manage/view/$2/$3";
$route['^(en|zh)/agora/administrator/products_edit/(:num)'] = "products_manage/add/$2";
$route['^(en|zh)/agora/administrator/products_del/(:num)'] = "products_manage/del/$2";
$route['^(en|zh)/agora/administrator/products_submit'] = "products_manage/submit";
$route['^(en|zh)/agora/administrator/update_qty'] = "products_manage/update_qty";
$route['^(en|zh)/agora/administrator/update_reason'] = "products_manage/update_reason";
$route['^(en|zh)/agora/administrator/ajax_inventory_log'] = "products_manage/ajax_inventory_log";
$route['^(en|zh)/agora/administrator/products_check'] = "products_manage/products_check";

//Manage Promotion
$route['^(en|zh)/agora/administrator/promotion'] = "promotion_manage";
$route['^(en|zh)/agora/administrator/promotion/(:num)'] = "promotion_manage/index/$2";
$route['^(en|zh)/agora/administrator/promotion_submit'] = "promotion_manage/submit";

//Manage Audit Log
$route['^(en|zh)/agora/administrator/audit_log'] = "audit_log_manage";
$route['^(en|zh)/agora/administrator/audit_log/(:any)/(:any)/(:any)'] = "audit_log_manage/index/$2/$3/$4";
$route['^(en|zh)/agora/administrator/audit_log/(:any)/(:any)/(:any)/(:num)'] = "audit_log_manage/index/$2/$3/$4/$5";
$route['^(en|zh)/agora/administrator/audit_log_export/(:any)/(:any)/(:any)'] = "audit_log_manage/export_data/$2/$3/$4";

//Manage Export Data
$route['^(en|zh)/agora/administrator/export_data'] = "export_data_manage";

//Manage Settings
$route['^(en|zh)/agora/administrator/settings'] = "settings_manage";
$route['^(en|zh)/agora/administrator/settings/(:num)'] = "settings_manage/index/$2";
$route['^(en|zh)/agora/administrator/settings_submit'] = "settings_manage/submit";

//Manage Services
$route['^(en|zh)/anexus/administrator/services'] = "services_manage";
$route['^(en|zh)/anexus/administrator/services/(:any)'] = "services_manage/index/$2";
$route['^(en|zh)/anexus/administrator/services/(:any)/(:num)'] = "services_manage/index/$2/$3";
$route['^(en|zh)/anexus/administrator/services_add'] = "services_manage/add";
$route['^(en|zh)/anexus/administrator/services_view/(:num)'] = "services_manage/view/$2";
$route['^(en|zh)/anexus/administrator/services_edit/(:num)'] = "services_manage/add/$2";
$route['^(en|zh)/anexus/administrator/services_del/(:num)'] = "services_manage/del/$2";
$route['^(en|zh)/anexus/administrator/services_submit'] = "services_manage/submit";

//Manage Shipping
$route['^(en|zh)/anexus/administrator/shipping'] = "shipping_manage";
$route['^(en|zh)/anexus/administrator/shipping/(:any)'] = "shipping_manage/index/$2";
$route['^(en|zh)/anexus/administrator/shipping/(:any)/(:num)'] = "shipping_manage/index/$2/$3";
$route['^(en|zh)/anexus/administrator/shipping_add'] = "shipping_manage/add";
$route['^(en|zh)/anexus/administrator/shipping_view/(:num)'] = "shipping_manage/view/$2";
$route['^(en|zh)/anexus/administrator/shipping_edit/(:num)'] = "shipping_manage/add/$2";
$route['^(en|zh)/anexus/administrator/shipping_del/(:num)'] = "shipping_manage/del/$2";
$route['^(en|zh)/anexus/administrator/shipping_submit'] = "shipping_manage/submit";

//Jobs
$route['^(en|zh)/agora/jobs'] = "jobs_manage";
$route['^(en|zh)/agora/jobs/(:any)'] = "jobs_manage/index/$2";
$route['^(en|zh)/agora/jobs/(:any)/(:num)'] = "jobs_manage/index/$2/$3";
$route['^(en|zh)/agora/jobs/(:any)'] = "jobs_manage/index/$2";
$route['^(en|zh)/agora/jobs/(:any)/(:num)'] = "jobs_manage/index/$2/$3";
$route['^(en|zh)/agora/job_detail/(:num)'] = "jobs_manage/job_detail/$2";
$route['^(en|zh)/agora/customers'] = "customers_manage";


//customer

//new order
$route['^(en|zh)/agora/customer/new_order'] = "new_order_manage/add";
$route['^(en|zh)/agora/customer/new_order/submit'] = "new_order_manage/submit_to_cart";

//Pending Order
$route['^(en|zh)/agora/customer/pending_order'] = "pending_order_manage";
$route['^(en|zh)/agora/customer/pending_order/(:any)'] = "pending_order_manage/index/$2";
$route['^(en|zh)/agora/customer/pending_order/(:any)/(:num)/(:num)'] = "pending_order_manage/index/$2/$3/$4";
$route['^(en|zh)/agora/customer/pending_order_add'] = "pending_order_manage/add";
$route['^(en|zh)/agora/customer/pending_order_edit/(:num)'] = "pending_order_manage/add/$2";
$route['^(en|zh)/agora/customer/pending_order_submit'] = "pending_order_manage/submit";
$route['^(en|zh)/agora/customer/pending_order_del/(:num)'] = "pending_order_manage/del/$2";

//All Order
$route['^(en|zh)/agora/customer/all_order'] = "all_order_manage";
$route['^(en|zh)/agora/customer/all_order/(:any)'] = "all_order_manage/index/$2";
$route['^(en|zh)/agora/customer/all_order/(:any)/(:num)/(:num)'] = "all_order_manage/index/$2/$3/$4";
$route['^(en|zh)/agora/customer/all_order_add'] = "all_order_manage/add";
$route['^(en|zh)/agora/customer/all_order_edit/(:num)'] = "all_order_manage/add/$2";
$route['^(en|zh)/agora/customer/all_order_reorder/(:num)'] = "all_order_manage/reorder/$2";
$route['^(en|zh)/agora/customer/all_order_submit'] = "all_order_manage/submit";
$route['^(en|zh)/agora/customer/all_order_submit2'] = "all_order_manage/submit2";
$route['^(en|zh)/agora/customer/all_order_del/(:num)'] = "all_order_manage/del/$2";

//Catalog
$route['^(en|zh)/agora/customer/catalog'] = "catalog_manage";
$route['^(en|zh)/agora/customer/catalog/(:any)'] = "catalog_manage/index/$2";
$route['^(en|zh)/agora/customer/catalog/(:any)/(:num)'] = "catalog_manage/index/$2/$3";
$route['^(en|zh)/agora/customer/catalog_add'] = "catalog_manage/add";
$route['^(en|zh)/agora/customer/catalog_edit/(:num)'] = "catalog_manage/add/$2";
$route['^(en|zh)/agora/customer/catalog_submit'] = "catalog_manage/submit";
$route['^(en|zh)/agora/customer/catalog_del/(:num)'] = "catalog_manage/del/$2";
$route['^(en|zh)/agora/customer/catalog_search'] = "catalog_manage/ajax_search";
$route['^(en|zh)/agora/customer/catalog_addCart'] = "catalog_manage/addItem";

//customer promotion
$route['^(en|zh)/agora/customer/promotion'] = "customer_promotion_manage";
$route['^(en|zh)/agora/customer/promotion/(:any)'] = "customer_promotion_manage/index/$2";
$route['^(en|zh)/agora/customer/promotion/(:any)/(:num)'] = "customer_promotion_manage/index/$2/$3";
$route['^(en|zh)/agora/customer/promotion_add'] = "customer_promotion_manage/add";
$route['^(en|zh)/agora/customer/promotion_edit/(:num)'] = "customer_promotion_manage/add/$2";
$route['^(en|zh)/agora/customer/promotion_submit'] = "customer_promotion_manage/submit";
$route['^(en|zh)/agora/customer/promotion_del/(:num)'] = "customer_promotion_manage/del/$2";
$route['^(en|zh)/agora/customer/promotion_search'] = "customer_promotion_manage/ajax_search";
$route['^(en|zh)/agora/customer/promotion_addCart'] = "customer_promotion_manage/addItem";

//cart
$route['^(en|zh)/agora/customer/cart'] = "cart_manage";
$route['^(en|zh)/agora/customer/cart_del'] = "cart_manage/del";
$route['^(en|zh)/agora/customer/cart_update'] = "cart_manage/update";
$route['^(en|zh)/agora/customer/cart_submit'] = "cart_manage/submit";
$route['^(en|zh)/agora/customer/cart_reorder'] = "cart_manage/reorder";

//landing page customer(mobile)
$route['^(en|zh)/agora/customer/customer_m'] = "customer_promotion_manage/customer_m";

//reminder
$route['^(en|zh)/agora/reminder'] = "reminder_manage";

$route['^(en|zh)/test_email/(:any)'] = "email_tester/index/$2";
$route['^(en|zh)/test_email/(:any)/(:any)/(:any)/(:num)'] = "email_tester/index/$2/$3/$4/$5";

/* End of file routes.php */
/* Location: ./application/config/routes.php */
