<?php

class User_model extends CI_Model {

      public function __construct() {
            $this->load->database();
            date_default_timezone_set("Asia/Taipei");
      }

      public function record_count($level,$parent_id=false,$searchword='',$userlevel='') {
            
          if($level != -1){
              $this->db->where('level',$level+1);
          }
          if($parent_id !== false){
              $this->db->where('parent_id',$parent_id);
          }
          
          if($searchword != ''){
              $this->db->like('email',$searchword);
          }
          
          if($userlevel != ''){
             $this->db->where('level',$userlevel); 
          }
          
          $query = $this->db->get("user");
          
          return $query->num_rows();
      }
      
      public function get($level,$parent_id=false)
      {
           if($level != -1){
              $this->db->where('level',$level+1);
           }
           if($parent_id !== false){
              $this->db->where('parent_id',$parent_id);
           }
           $this->db->order_by("created_date","DESC");
           $query = $this->db->get("user");
           if ($query->num_rows() > 0) {
                return $query->result_array();
           }
           return false;
      }
      
      public function getparent()
      {
           $this->db->where('level <= ',2);
          
           $this->db->order_by("created_date","DESC");
           $query = $this->db->get("user");
           if ($query->num_rows() > 0) {
                return $query->result_array();
           }
           return false;
      }
      
      public function fetch($limit, $start,$level,$parent_id=false,$searchword='',$userlevel='') {
           if($level != -1){
              $this->db->where('level',$level+1);
           }
           if($parent_id !== false){
              $this->db->where('parent_id',$parent_id);
           }
           
           if($searchword != ''){
              $this->db->like('email',$searchword);
          }
          
          if($userlevel != ''){
             $this->db->where('level',$userlevel); 
          }
          
           $this->db->order_by("created_date","DESC");
            $this->db->limit($limit, $start);
            $query = $this->db->get("user");
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
            return false;
      }
      
      public function getAllUsers(){
            $this->db->order_by("created_date","DESC");
            $query = $this->db->get('user');
            return $query->result_array();
      }

      public function get_user($user_id = false) {
            if ($user_id === false) {
                  $query = $this->db->get('user');
                  return $query->result_array();
            }

            $query = $this->db->get_where('user', array('id' => $user_id));
            return $query->row_array();
      }
	  public function get_sales(){
			$query = $this->db->get_where('user', array('level' => '2'));
			return $query->result_array();
	  }
      
      public function getUserByEmail($email){
            $query = $this->db->get_where('user', array(
                'email' 		=> $email,             
				'is_deleted'	=> 0,  
                   ));
            if($query->num_rows() > 0) {
                  return $query->row_array();
            } else {
                  return FALSE;
            }
      }
	  
	  public function getUserByEmail2($email,$id){
            $query = $this->db->get_where('user', array(
                'email' 		=> $email,             
				'is_deleted'	=> 0, 
				'id !='			=> $id, 
                   ));
            if($query->num_rows() > 0) {
                  return $query->row_array();
            } else {
                  return FALSE;
            }
      }
	  
	  public function getUserByEmail3($email,$id){
		  
		  	$array = array(
                'email' 		=> $email,             
				'is_deleted'	=> 0,
            );
		  
		  	if($id!=''){
				$array['customer_id !='] = $id;
			}
			
			$query = $this->db->get_where('user',$array);
			
            if($query->num_rows() > 0) {
                  return $query->row_array();
            } else {
                  return FALSE;
            }
      }
	  
	  public function getUserBy_retrieval($pwd_retrieval){
            $query = $this->db->get_where('user', array(
                'pwd_retrieval' 		=> $pwd_retrieval,             
				'is_deleted'	=> 0,  
                   ));
            if($query->num_rows() > 0) {
                  return $query->row_array();
            } else {
                  return FALSE;
            }
      }
      
      public function updateUserPassword($md5_password, $user_id){
             $data = array(                
                'passwd' => $md5_password,
                'pwd_retrieval' => '', //must empty to prevent security issue
                'modified_date' => date("Y-m-d H:i:s"),
            );
            $this->db->where('id', $user_id);
            return $this->db->update('user', $data);
      }
      
      //generate code for serial usage
      public function randCodeGen() {
            return rand(10000, 99999);
      }
      
      //To confirm whether the code and the id is match
      public function getPwdResetCode($code, $user_id) {
            $query = $this->db->get_where('user', array(
                'id' => $user_id,    
                'pwd_retrieval'    => $code,
                   ));
            if($query->num_rows() > 0) {
                  return TRUE;
            } else {
                  return FALSE;
            }
      }
	  
	  public function getPwdResetCode2($code) {
            $query = $this->db->get_where('user', array(
                'pwd_retrieval'    => $code,
            ));
			
            if($query->num_rows() > 0) {
                  return TRUE;
            } else {
                  return FALSE;
            }
      }
      
      //To set the retrieval code of a user
      public function updatePwdResetCode($pwd_retrieval, $user_id){
             $data = array(                
                'pwd_retrieval' => $pwd_retrieval,
                'modified_date' => date("Y-m-d H:i:s"),
            );
            $this->db->where('id', $user_id);
            return $this->db->update('user', $data);
      }
      
      //To activate a user
      public function activateUser($user_id){
             $data = array(                
                'activated' => 1,
                'modified_date' => date("Y-m-d H:i:s"),
            );
            $this->db->where('id', $user_id);
            return $this->db->update('user', $data);
      }
      
	  public function insert($array) {
		  	$this->db->insert('user', $array);
            $insert_id = $this->db->insert_id();
            return $insert_id;
	  }
	  
	  public function update($id, $array) {
		  	$this->db->where('id', $id);
		  	$this->db->update('user', $array);            
            return;
	  }
	  
      //insert a new user
      public function insertUser($array) {

            $data = array(
                'parent_id'     => $array['parent_id'],
                'level'         => $array['level'],
                'email'         => $array['email'],
				'mobile'        => $array['mobile'],
                'gender'        => $array['gender'],
                'password'      => md5($array['password']),
                'firstname'     => $array['firstname'],
                'lastname'      => $array['lastname'],
                'created_date'  => date("Y-m-d H:i:s"),
                'activated'     => $array['activated'],
                'emailcode'     => $this->randCodeGen(),
            );
            $this->db->insert('user', $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
      }
      
      public function updateUser($id,$array) {

            $data = array(
                'email'         => $array['email'],
				'mobile'        => $array['mobile'],
                'gender'        => $array['gender'],
                'firstname'     => $array['firstname'],
                'lastname'      => $array['lastname'],
                'modified_date' => date("Y-m-d H:i:s"),
                'emailcode'     => $this->randCodeGen(),
            );
            if(isset($array['parent_id'])){
                $data['parent_id'] = $array['parent_id'];
            }
            if(isset($array['level'])){
                $data['level'] = $array['level'];
            }
            if(isset($array['activated'])){
                $data['activated'] = $array['activated'];
            }
            
            if($array['password'] != ''){
                $data['password'] = md5($array['password']);
            }
            $this->db->where('id',$id);
			$this->db->update('user', $data);
            
            return $id;
      }
      
      public function get_users_email($id)
      {
          $d = array();
          $users = $this->get_user();
          if(!empty($users)){
              foreach ($users as $v):
                  if($v['id'] != $id){
                    $d[] = $v['email'];
                  }
              endforeach;
          }
          
          return $d;
      }
      
      public function delete($id){
		
		$data = array(
				'id' => $id,
				'is_deleted' => '1'
			);
			$this->db->where('id',$id);
			$this->db->update('user', $data);
		
      }
	  
	  public function getIDKeyArray($column="title") {
		  	$id_list = array();
		  	$list = $this->getAllUsers();
			foreach($list as $k=>$v) {
				$id_list[$v['id']] = $v[$column];
			}			
			return $id_list;					  
	  	}
		
		public function name_list(){
			$query = $this->db->get('user');
            $data = $query->result_array();
			$array = array();
			foreach($data as $v){
				$array[$v['id']] = $v['name'];
			}
			return $array;
		}

}

?>