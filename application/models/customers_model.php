<?php
class Customers_model extends CI_Model {
	  private $table_name = "";
	
      	public function __construct() {
            $this->load->database();
            date_default_timezone_set("Asia/Taipei");
			$this->table_name = "customer";						
      	}

		public function currency_list(){
			return array(
				'USD'	=> 'USD',
				'SGD'	=> 'SGD',
				'EUR'	=> 'EUR',
				'JPY'	=> 'JPY',
				'MYR'	=> 'MYR',
				'THB'	=> 'THB',
			);
		}
		
		public function status_list($withAll=true){
			if($withAll){
				return array(				
					'1'		=> "Active",				
					'0'		=> "Suspended",
					
				);
			} else {
				return array(					
					'1'		=> "Active",					
					'0'		=> "Suspended",
					
				);
			}
		}
		
		public function get_where($where=array(), $like="") {
			
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('company_name', $like); 	
				$this->db->or_like('contact_person', $like); 	
			}
			
			$this->db->order_by("id","DESC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
            
		}
	
	   	public function get($id=false) {
			if ($id === false) {
                  $query = $this->db->get($this->table_name);
                  return $query->result_array();
            }			
		    $query = $this->db->get_where($this->table_name, array('id' => $id));
            return $query->row_array();
		}
		
		public function get2() {
			$this->db->where('is_deleted',0);
            $query = $this->db->get($this->table_name);
            return $query->result_array();

		}
	
    	public function record_count($where=array(), $like="",$customer = false) {          
			$this->db->where($where);
			
			if($customer != false){
				$this->db->where_in('id',$customer);	
			}
			
			if(!empty($like)) {
				$this->db->like('company_name', $like); 	
				$this->db->or_like('custom_code', $like);  	
			}
			
          	$query = $this->db->get($this->table_name);          
          	return $query->num_rows();
      	}	
	  
      	public function fetch($where=array(), $like="", $limit, $start,$customer = false) {
			$this->db->where($where);
			
			if($customer != false){
				$this->db->where_in('id',$customer);	
			}
			
			if(!empty($like)) {
				$this->db->like('company_name', $like); 	
				$this->db->or_like('custom_code', $like); 
				
			}
			            
           	$this->db->order_by("id","DESC");
            $this->db->limit($limit, $start);
            $query = $this->db->get($this->table_name);
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
            return false;
      	}
      
      	public function getAll(){
			$this->db->where('is_deleted',0);
            $this->db->order_by("id","ASC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
      	}

		
		public function insert($array)
		{
        	$this->db->insert($this->table_name, $array);			
			$insert_id = $this->db->insert_id();
			//echo $this->db->last_query();
			//exit;
        	return $insert_id;			
		}
		
		public function update($id, $array)
		{		
        	$this->db->where('id',$id);
			$this->db->update($this->table_name, $array);			
		}
		
		public function delete($id)
		{
			$data = array(
				'id' => $id,
				'is_deleted' => '1'
			);
			$this->db->where('id',$id);
			$this->db->update($this->table_name, $data);
		}	
		public function getIDKeyArray($column="company_name") {
		  	$id_list = array();
		  	$list = $this->getAll();
			foreach($list as $k=>$v) {
				$id_list[$v['id']] = $v[$column];
			}			
			return $id_list;					  
	  	}
		
		public function zerofill($input, $strlength=5){
			
			$query = $this->db->get_where('settings', array('id' => 13));
            $json = $query->row_array();
			$json = json_decode($json['value'],true);
			
			$prefix ='';
			$has_yr = false;
			foreach($json as $k => $v){
				if($v['table_name'] == $this->table_name){
					$prefix = $v['prefix'];
					if($v['YY']) {
						$has_yr = true;
					}
				}
			}
			if($has_yr) {
				$prefix = $prefix.date("y");
			}
			
			$total_str = strlen($input);
			$balance_space = $strlength - $total_str;
			$tmp = "";
			if($balance_space > 0) {				
				$tmp = str_repeat("0", $balance_space).$input;				
			} else {
				$tmp = $input;	
			}
			return $prefix.$tmp;						
			
		}
		
		public function customer_whereIn($array){
			
			$this->db->where_in('id', $array);
			$this->db->order_by("id","DESC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
            
		}
		
		public function getCustomerByEmail($email){
            $query = $this->db->get_where('customer', array(
                'primary_contact_email' 		=> $email,             
				'is_deleted'	=> 0,  
                   ));
            if($query->num_rows() > 0) {
                  return $query->row_array();
            } else {
                  return FALSE;
            }
      }
	  
	  
	  public function getCustomerByEmail2($email,$id){
            $query = $this->db->get_where('customer', array(
                'primary_contact_email' 		=> $email,             
				'is_deleted'	=> 0, 
				'user_id !='	=> $id, 
                   ));
            if($query->num_rows() > 0) {
                  return $query->row_array();
            } else {
                  return FALSE;
            }
      }
	  
	  public function getCustomerByEmail3($email,$id){
		  
		  
		  	$array = array(
                'primary_contact_email' => $email,             
				'is_deleted'	=> 0, 
            );
		  
		  	if($id!=''){
				$array['id !='] = $id;
			}
			
			$query = $this->db->get_where('customer',$array);
		  
            if($query->num_rows() > 0) {
                  return $query->row_array();
            } else {
                  return FALSE;
            }
      }
				
			
}
?>
