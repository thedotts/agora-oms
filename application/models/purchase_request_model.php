<?php
class Purchase_request_model extends CI_Model {
	  private $table_name = "";
	
      	public function __construct() {
            $this->load->database();
            date_default_timezone_set("Asia/Taipei");
			$this->table_name = "purchase";						
      	}
		
		public function status_list(){
			return array(
				'ALL'	=> "All",
				'1'		=> "Pending Est. Shipping Cost",
				'2'		=> "Pending Confirmation of Quotation",
			);
		}
		
		public function purchases_status_list(){
			return array(
				'0'		=> "No Approved",
				'1'		=> "Pending Approval by Sales Manager",
				'2'		=> "Pending Approval by General Manager",
				'3'		=> "Pending Approval by Finance",
				'4'		=> "Pending Approval by Logistics",
				'5'		=> "Waiting sendmail to suppliers by Logistics",
				'6'		=> "Waiting for quantity update & receival by Logistics",
				'7'		=> "Waiting for confirmation & document upload",
				'8'		=> "Completed",
			);
			
		}
		
		public function get_where($where=array(), $like="") {
			
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('job_id', $like); 	
				$this->db->or_like('customer_name', $like); 	
			}
			
			$this->db->order_by("id","DESC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
            
		}
	
	   	public function get($id=false) {
			if ($id === false) {
                  $query = $this->db->get($this->table_name);
                  return $query->result_array();
            }			
		    $query = $this->db->get_where($this->table_name, array('id' => $id));
            return $query->row_array();
		}
	
		public function getBySerial($serial=false) {
			if ($serial === false) {
                  $query = $this->db->get($this->table_name);
                  return $query->result_array();
            }			
		    $query = $this->db->get_where($this->table_name, array('purchase_serial' => $serial));
            return $query->row_array();
		}
	
		public function getRelatedPurchase($id) {		
		    $query = $this->db->get_where($this->table_name, array('quotation_id' => $id));
            return $query->result_array();
		}
	
    	public function record_count($where=array(), $like="") {          
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('purchase_serial', $like); 	
				//$this->db->or_like('customer_name', $like); 	
			}
			
          	$query = $this->db->get($this->table_name);          
          	return $query->num_rows();
      	}	
	  
      	public function fetch($where=array(), $like="", $limit, $start) {
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('purchase_serial', $like); 	
				$this->db->or_like('customer_name', $like); 
			}
			$this->db->where('is_deleted',0);           
           	$this->db->order_by("id","DESC");
            $this->db->limit($limit, $start);
            $query = $this->db->get($this->table_name);
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
            return false;
      	}
      
      	public function getAll(){
			$this->db->where('is_deleted',0);
            $this->db->order_by("id","ASC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
      	}

		
		public function insert($array)
		{
        	$this->db->insert($this->table_name, $array);						
        	$insert_id = $this->db->insert_id();
        	return $insert_id;			
		}
		
		public function update($id, $array)
		{		
        	$this->db->where('id',$id);
			$this->db->update($this->table_name, $array);			
		}
		
		public function delete($id)
		{
			$data = array(
				'id' => $id,
				'is_deleted' => '1'
			);
			$this->db->where('id',$id);
			$this->db->update($this->table_name, $data);
		}	
		public function getIDKeyArray($column="company_name") {
		  	$id_list = array();
		  	$list = $this->getAll();
			foreach($list as $k=>$v) {
				$id_list[$v['id']] = $v[$column];
			}			
			return $id_list;					  
	  	}
		
		public function zerofill($input, $strlength=5, $is_adhoc=false){
			
			$query = $this->db->get_where('settings', array('id' => 13));
            $json = $query->row_array();
			$json = json_decode($json['value'],true);
			
			$prefix ='';
			$has_yr = false;
			foreach($json as $k => $v){
				if($v['table_name'] == $this->table_name){
					$prefix = $v['prefix'];
					if($v['YY']) {
						$has_yr = true;
					}
				}
			}
			
			//only purchaser has special serial number
			if($is_adhoc) {
				$prefix	= "OP-";
			}
			
			if($has_yr) {
				$prefix = $prefix.date("y");
			}
			
			
			$total_str = strlen($input);
			$balance_space = $strlength - $total_str;
			$tmp = "";
			if($balance_space > 0) {				
				$tmp = str_repeat("0", $balance_space).$input;				
			} else {
				$tmp = $input;	
			}
			return $prefix.$tmp;						
			
		}
		
		public function ajax_purchase_request($like="", $limit, $start, $where=array()) {
			
			
			$sql = "1";
			
			if(!empty($like)) {				
				$sql .= " AND (`purchase_serial` LIKE '%".$like."%' OR `customer_name` LIKE '%".$like."%') ";								
			}
			
			if(!empty($where)){
				
				foreach($where as $k=>$v){
					$sql .= " AND (`".$k."` = '".$v."') ";
				}
				
			}
			
			$sql .= " AND (`is_deleted` = '0') ";
			
			$this->db->where($sql, NULL, false);						         
           	$this->db->order_by("id","DESC");
            $this->db->limit($limit, $start);
            $query = $this->db->get($this->table_name);
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
            return false;
      	}
		
		public function ajax_record_count($like="", $where=array()) {          
			
			$sql = "1";
			
			if(!empty($like)) {				
				$sql .= " AND (`purchase_serial` LIKE '%".$like."%' OR `customer_name` LIKE '%".$like."%') ";								
			}
			
			if(!empty($where)){
				
				foreach($where as $k=>$v){
					$sql .= " AND (`".$k."` = '".$v."') ";
				}
				
			}
			
			$sql .= " AND (`is_deleted` = '0') ";
			
			$this->db->where($sql, NULL, false);
          	$query = $this->db->get($this->table_name);          
          	return $query->num_rows();
      	}
						
}
?>
