<?php
class Job_model extends CI_Model {
	  private $table_name = "";
	
      	public function __construct() {
            $this->load->database();
            date_default_timezone_set("Asia/Taipei");
			$this->table_name = "job";						
      	}
		
		public function status_list(){
			$this->db->where('id',1);
            $query = $this->db->get('workflow');
            $data = $query->row_array();
			$status_json = json_decode($data['status_json'],true);
			
			$array = array();
			foreach($status_json as $k => $v){
				$array[$k] = $v;
			}
			return $array;
		}
		
		public function get_where($where=array(),$role_id = FALSE) {
			
			if($role_id != FALSE){
				//$this->db->like('awaiting_role_id',$role_id.','); 
				foreach($role_id as $k => $v){
					$role_id[$k] = $v.',';
				}
				
				$this->db->where_in('awaiting_role_id', $role_id);
				
			}
			$this->db->where($where);
			$this->db->order_by("id","DESC");
            $query = $this->db->get($this->table_name);						
			
            return $query->result_array();
            
		}
		
		public function get_where_sales($where=array(),$role_id = FALSE,$where_in) {
			
			if($role_id != FALSE){
				//$this->db->like('awaiting_role_id',$role_id.','); 
				foreach($role_id as $k => $v){
					$role_id[$k] = $v.',';
				}
				
				$this->db->where_in('awaiting_role_id', $role_id);
				
			}
			$this->db->where_in('customer_id', $where_in);
			$this->db->where($where);
			$this->db->order_by("id","DESC");
            $query = $this->db->get($this->table_name);						
			
            return $query->result_array();
            
		}
	
	   	public function get($id=false) {
			if ($id === false) {
                  $query = $this->db->get($this->table_name);
                  return $query->result_array();
            }			
		    $query = $this->db->get_where($this->table_name, array('id' => $id));
            return $query->row_array();
		}
	
    	public function record_count($where=array(),$role_id="",$order_by="", $ascdesc = "", $group_by="",$like="") {   					
			
			if(!empty($role_id)) {
				//$this->db->like('awaiting_role_id',$role_id.',');  
				/*
				foreach($role_id as $k => $v){
					$role_id[$k] = $v.',';
				}
				*/
				
				$this->db->where_in('awaiting_role_id', $role_id);
				
				     
			}
			
			$this->db->where($where);	
			
			if(!empty($like)) {
				$this->db->like('order_no', $like); 	
				$this->db->or_like('customer_name', $like); 	
			}		
			
			if(!empty($order_by) && !empty($ascdesc)) {
				$this->db->order_by($order_by, $ascdesc);
			}
			
			if(!empty($group_by)) {
				$this->db->group_by($group_by);
			}
				
          	$query = $this->db->get($this->table_name);    
			//echo $this->db->last_query();
			      
			//print_r($role_id);exit;
          	return $query->num_rows();
      	}	
		
		public function record_count2($where=array(),$role_id="",$a_customer_array) {   					
			

			if(!empty($role_id)) {
				//$this->db->like('awaiting_role_id',$role_id.',');  
				/*
				foreach($role_id as $k => $v){
					$role_id[$k] = $v.',';
				}
				*/
				
				$this->db->where_in('awaiting_role_id', $role_id);
				     
			}
			
			if(empty($a_customer_array)){
				$a_customer_array = array(0);
			}

			$this->db->where_in('customer_id',$a_customer_array);
			
			$this->db->where($where);
				
          	$query = $this->db->get($this->table_name);    
			//echo $this->db->last_query();
			      
			//print_r($a_customer_array);exit;
			
          	return $query->num_rows();
      	}	
	  
      	public function fetch($limit, $start, $where=array(),$group_by="",$like="") {
			
			if(!empty($like)) {
				$this->db->like('order_no', $like); 	
				$this->db->or_like('customer_name', $like); 	
			}
			
			$this->db->where($where);            
           	$this->db->order_by("id","ASC");
            $this->db->limit($limit, $start);
			
			if(!empty($group_by)) {
				$this->db->group_by($group_by);
			}
			
            $query = $this->db->get($this->table_name);	
			//echo $this->db->last_query();exit;				
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
            return false;
      	}
      
      	public function getAll($where=array()){
			$this->db->where($where);
			$this->db->where('is_deleted',0);
            $this->db->order_by("id","ASC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
      	}

		
		public function insert($array)
		{
        	$this->db->insert($this->table_name, $array);
        	$insert_id = $this->db->insert_id();
        	return $insert_id;			
		}
		
		public function update($id, $array)
		{		
        	$this->db->where('id',$id);
			$this->db->update($this->table_name, $array);			
		}
		
		public function delete($id)
		{
			$data = array(
				'id' => $id,
				'is_deleted' => '1'
			);
			$this->db->where('id',$id);
			$this->db->update($this->table_name, $data);
		}	
		public function getIDKeyArray($column="title") {
		  	$id_list = array();
		  	$list = $this->getAll();
			foreach($list as $k=>$v) {
				$id_list[$v['id']] = $v[$column];
			}			
			return $id_list;					  
	  	}
		
		public function zerofill($input, $strlength=5){
			
			$query = $this->db->get_where('settings', array('id' => 13));
            $json = $query->row_array();
			$json = json_decode($json['value'],true);
			
			$prefix ='';
			$has_yr = false;
			foreach($json as $k => $v){
				if($v['table_name'] == $this->table_name){
					$prefix = $v['prefix'];
					if($v['YY']) {
						$has_yr = true;
					}
				}
			}
			if($has_yr) {
				$prefix = $prefix.date("y");
			}
			
			$total_str = strlen($input);
			$balance_space = $strlength - $total_str;
			$tmp = "";
			if($balance_space > 0) {				
				$tmp = str_repeat("0", $balance_space).$input;				
			} else {
				$tmp = $input;	
			}
			return $prefix.$tmp;						
			
		}
		
		public function getRelatedJob($id){
			
			$this->db->where(array('id'=>$id));									
          	$query = $this->db->get($this->table_name);  
			if ($query->num_rows() > 0) {
                $data = $query->row_array();
				return $id.",".$this->getRelatedJob($data['parent_id']);
            } 
			
		}
		
		public function getJobWherein($in_array) {
			
			$this->db->where_in('id', $in_array);	
			$this->db->order_by('id','DESC');
			$query = $this->db->get($this->table_name);  
			if ($query->num_rows() > 0) {
                return $query->result_array();
			}
			
		}
		
		//return Array
		public function getRelatedRoleID($latest_job_id){
			
			$this->load->model('Employee_model');
			
			//追溯到最根, 把全部相關的job都抓出來
			$trace_jobs = $this->getRelatedJob($latest_job_id);
			
			//把它們變成ARRAY, 因為要做IN 搜尋	
			$related_job_array = explode(",", substr($trace_jobs,0,-1));
			$related_jobs = $this->getJobWherein($related_job_array);
			
			$related_roles = "";
			$ancestor_user_id = 0;
			if(!empty($related_jobs)){
				foreach($related_jobs as $v) {
					$related_roles .= substr($v['awaiting_role_id'],0,-1).",";
					$ancestor_user_id = $v['user_id'];
				}
			}
			//最後根部的建立者ROLE
			$requestor_employee = $this->Employee_model->get($ancestor_user_id);
			if(!empty($requestor_employee)){
				$related_roles .= $requestor_employee['role_id'];
			}
			
			return explode(",", $related_roles);
			
		}
		
		public function get_where_groupBy($where=array()){
			
			$this->db->group_by('serial');
			
			$this->db->where($where);
			$this->db->order_by("id","DESC");
            $query = $this->db->get($this->table_name);						
			
            return $query->result_array();
            
		}
		
		public function get_where_groupBy2($where=array()){
			
			$this->db->group_by('order_no');
			
			$this->db->where($where);
			$this->db->order_by("id","DESC");
            $query = $this->db->get($this->table_name);						
			
            return $query->result_array();
            
		}
		
		private function workflowTable(){
			return array(
				'Order' 			=> "Order_model",
				'Packing List' 		=> "Packing_list_model",
				'Delivery Order' 	=> "Delivery_order_model",
				'Invoice' 			=> "Invoice_model",
			);
		}
		
		public function getModelNameByType($type) {
			
			$tmp = $this->workflowTable();
			return $tmp[$type];
			
		}
		
		public function getTypeByModelName($model_name){
			
			$tmp = $this->workflowTable();
			foreach($tmp as $k=>$v) {
				if($v == $model_name) {
					return $k;	
				}
			}
			
		}
		
		public function job_remind($reminder1,$reminder2){
			
			$today = date('Y-m-d H:i:s');
			$today2 = date('Y-m-d');
			
			$this->db->where('is_completed',0);
			$this->db->where('DATE_ADD(created_date,INTERVAL '.$reminder1.' HOUR) <= \''.$today.'\'');
			$this->db->where('DATE_ADD(created_date,INTERVAL '.$reminder2.' HOUR) >= \''.$today.'\'');
			$this->db->where('send_date < \''.$today2.'\'');
			
			$query = $this->db->get($this->table_name);	
            return $query->result_array();
			
		}
		
		public function job_remind_urgent($reminder1,$reminder2){
			
			$today = date('Y-m-d H:i:s');
			$today2 = date('Y-m-d');
			
			$this->db->where('is_completed',0);
			$this->db->where('DATE_ADD(created_date,INTERVAL '.$reminder2.' HOUR) <= \''.$today.'\'');
			$this->db->where('send_date < \''.$today2.'\'');
			
			$query = $this->db->get($this->table_name);
            return $query->result_array();
			
		}
				
			
}
?>