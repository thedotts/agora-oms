<?php
class Function_model extends CI_Model{

	public function __construct()
	{
		//$this->load->model('article_model');
		//$this->load->model('category_model');
		$this->load->model('settings_model');
        date_default_timezone_set("Asia/Taipei");
	}			
    
    public function page_init()
	{
		
		$this->lang->load('langfile');		
        
        $salesman = '';
        if($this->session->userdata("userdata") !== false){
            $userdata = $this->session->userdata("userdata");
            $this->load->model('user_model');            
        }        
		
		$array = array(
			'langu'				=> $this->lang->lang(),
			'lang'				=> $this->lang->language,
            'lang_js'			=> json_encode($this->lang->language),
            'current_url'       => $this->get_lang_chg_url($this->lang->lang()),
            'lang_type'         => ($this->lang->lang() == 'zh-cn')?'_zh':'',      
		);
        
		return $array;
	}
    
    public function get_current_url() {

        $protocol = 'http';
        if ($_SERVER['SERVER_PORT'] == 443 || (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
            $protocol .= 's';
            $protocol_port = $_SERVER['SERVER_PORT'];
        } else {
            $protocol_port = 80;
        }

        $host = $_SERVER['HTTP_HOST'];
        $port = $_SERVER['SERVER_PORT'];
        $request = $_SERVER['PHP_SELF'];
        //$query = isset($_SERVER['argv']) ? substr($_SERVER['argv'][0], strpos($_SERVER['argv'][0], ';') + 1) : '';

        $toret = $protocol . '://' . $host . ($port == $protocol_port ? '' : ':' . $port) . $request . (empty($query) ? '' : '?' . $query);

        return $toret;
    }
    
    public function get_lang_chg_url($langu){
        $url = array();
        $url = explode('/',$_SERVER['REQUEST_URI']);
       
        $string = '';
        foreach($url as $v):
            if($v == $langu){
                $string .= (($v=='en')?'zh':'zh').'/';
            }else if($string != ''){
                $string .= $v.'/';
            }
        endforeach;
        
        return $string;
    }
    
    public function item_per_page()
    {
        return 12;
    }
	
	public function isLogin(){
                            
							 
		if( $this->session->userdata("userdata") ){               
			return true;	
		} else {
			
			if(isset($_COOKIE['chl_token'])) {
				$cookie = explode('/',$_COOKIE['chl_token']);
				
				$userdata = $this->User_model->getUserByEmail($cookie[0]);
				if($userdata['passwd'] != $cookie[1]) {
                        
                    setcookie("chl_token", "", time() - 3600);
					return false;
						
                } else {
                    $user_id = $userdata['id'];
                    //Load Sean in Default
                    $this->session->set_userdata("userdata", $this->User_model->get_user($user_id));
                    return true;
                }
				  
			} else {
				return false;	
			}
			
		}
	}
	
	public function get_menu() {
		$data = array();
		$data = $this->category_model->get_parent_category('display');
		if(!empty($data)){
			foreach ($data as $k=>$v):
				$data[$k]['submenu'] =  $this->category_model->get_child_category($v['category_id'],'display');
				foreach ($data[$k]['submenu'] as $t=>$a):
					$article = $this->article_model->get_article_use_category($a['category_id']);
					$data[$k]['submenu'][$t]['article'] = $article;
				endforeach;
			endforeach;
		}
        return $data;
    }
	
	public function get_web_setting() {
		$array = array();
		$array = array(
			'web_title'			=> $this->settings_model->get_web_title(),			
			'web_logo'			=> $this->settings_model->get_web_logo(),			
		);
		return $array;
	}
	
	
	public function set_breadcrumb($array, $name, $link)
	{
		$array[] = array(
			'name'	=> $name,
			'link'	=> $link,
		);
		
		return $array;
	}
    
    public function get_paging($item_per_page,$pagenum,$total_item,$page,$url)
	{
	
		$start = (int)(($page-1)/$pagenum)*$pagenum+1;
		$end = $start+$pagenum-1;
		$next = $page+1;
		$pre = $page-1;
		
		$total_page = ceil( $total_item / $item_per_page );
                $paging = '';
		if($total_item > $item_per_page){
                    $paging .= '<ul class="pagination">';

                    if($page > 1){
                            $paging .= '<li><a href="'.$url.'1">&laquo;</a></li>';
                            $paging .= '<li><a href="'.$url.$pre.'">&lsaquo;</li>';
                    }

                    if($total_page <= $pagenum){

                            for($i=$start;$i<=$total_page;$i++){
                                    if($i == $page){

                                            $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                    }else{

                                            $paging .= '<li><a href="'.$url.$i.'">'.$i.'</a></li>';
                                    }
                            }
                    }else{
                            if($page > 5){
                                    $end = $page+5;
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    $start = $end - ($pagenum - 1);

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="'.$url.$i.'">'.$i.'</a></li>';
                                            }
                                    }
                            }else{
                                    if($end > $total_page){
                                            $end = $total_page;
                                    }

                                    for($i=$start;$i<=$end;$i++){
                                            if($i == $page){
                                                    $paging .= '<li class="active"><a href="javascript:void(0)">'.$i.'</a></li>';
                                            }else{
                                                    $paging .= '<li><a href="'.$url.$i.'">'.$i.'</a></li>';
                                            }
                                    }
                            }	
                    }

                    if($page < $total_page){
                            $paging .= '<li><a href="'.$url.$next.'">&rsaquo;</a></li>';
                            $paging .= '<li><a href="'.$url.$total_page.'">&raquo;</a></li>';
                    }

                    $paging .= '</ul>';
                }
		
		return $paging;
	}
    
    function get_current_page(){
        $url = $_SERVER['REQUEST_URI'];
        $array = array();
        $array = explode('/', $url);
        
        if(isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']=="www.chlconcepts.com") {
            return $array[1];
        } else if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']=="chlconcepts.com") {
            return $array[1];
		} else if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']=="chlconcepts.local") {
			return $array[1];
        } else {
            return $array[2];
        }
        
    }
    
    public function upload($filename,$postname)
    {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name'] = $filename;
        $config['overwrite'] = TRUE;
        $config['max_size'] = '0';
        $config['max_width']  = '0';
        $config['max_height']  = '0';   

        $this->load->library('upload', $config);

        if(!is_dir($config['upload_path'])){
            mkdir($config['upload_path'], 0755, TRUE);
        }

        if (!$this->upload->do_upload($postname)){ //Upload file
            redirect("errorhandler"); //If error, redirect to an error page
        }else{
            return $this->upload->data();
        }
    }

    public function multi_upload($filename,$postname)
    {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name'] = $filename;
        $config['overwrite'] = TRUE;
        $config['max_size'] = '0';
        $config['max_width']  = '0';
        $config['max_height']  = '0';   

        $this->load->library('upload');
        $this->upload->initialize($config);

        if(!is_dir($config['upload_path'])){
            mkdir($config['upload_path'], 0755, TRUE);
        }

        if (!$this->upload->do_multi_upload($postname, TRUE)){ //Upload file
            redirect("errorhandler"); //If error, redirect to an error page
        }else{
            return $this->upload->get_multi_upload_data();
        }
    }
         
    public function img_resize($upload_data,$nwidth,$nheight,$new_path){
        $image_config["image_library"] = "gd2";
        $image_config["source_image"] = $upload_data["full_path"];
        $image_config['create_thumb'] = FALSE;
        $image_config['maintain_ratio'] = TRUE;
        $image_config['new_image'] = $new_path;
        $image_config['quality'] = "100%";
        $image_config['width'] = $nwidth;
        $image_config['height'] = $nheight;
        $dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
        $image_config['master_dim'] = ($dim > 0)? "height" : "width";

        $this->load->library('image_lib');
        $this->image_lib->initialize($image_config);

        if(!$this->image_lib->resize()){ //Resize image
            redirect("errorhandler"); //If error, redirect to an error page
        }

    }
    
    public function cut_content($string,$length){
        $string = strip_tags($string); //去除HTML標籤
        $sub_content = mb_substr($string, 0, $length, 'UTF-8'); //擷取子字串

        //顯示 "......"
        if (strlen($string) > strlen($sub_content)) $sub_content."...";

        return $sub_content;
    }
    
}
?>