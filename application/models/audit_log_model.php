<?php

class Audit_log_model extends CI_Model {

      public function __construct() {
            $this->load->database();
            date_default_timezone_set("Asia/Taipei");
			$this->table_name = "audit_log";	
      }

      public function getAll(){
		  	$this->db->where('is_deleted',0);
            $this->db->order_by("id","ASC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();    
      }
	  
	  public function get_where($where=array(), $like="") {
			
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('name', $like); 	
			}
			
			$this->db->order_by("id","DESC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
            
		}
	
	   	public function get($id=false) {
			if ($id === false) {
                  $query = $this->db->get($this->table_name);
                  return $query->result_array();
            }			
		    $query = $this->db->get_where($this->table_name, array('id' => $id));
            return $query->row_array();
		}
	
    	public function record_count($where=array(), $like="",$start_date='',$end_date='') { 
		
			if($start_date != ''){
				$tmp = explode("/", $start_date);
			  	$start_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];	
			}
			if($end_date != ''){
				$tmp = explode("/", $end_date);
			  	$end_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];	
			}
		
		    $this->db->join('user', 'audit_log.user_trigger = user.id');
			
			if($start_date != '' && $end_date != ''){
				$this->db->where('audit_log.created_date between \''.$start_date.' 00:00:00\' and \''.$end_date.' 23:59:59\'');
			}else if($start_date != ''){
				$this->db->where('audit_log.created_date >= \''.$start_date.' 00:00:00\'');
			}else if($end_date != ''){
				$this->db->where('audit_log.created_date <= \''.$end_date.' 00:00:00\'');
			}
			 
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->where('(`name` LIKE \'%'.$like.'%\' OR `table_affect` LIKE \'%'.$like.'%\')'); 	
			}
			
          	$query = $this->db->get($this->table_name);      
			//echo $this->db->last_query();exit;    
          	return $query->num_rows();
      	}	
	  
      	public function fetch($where=array(), $like="",$start_date='',$end_date='', $limit, $start) {
			
			if($start_date != ''){
				$tmp = explode("/", $start_date);
			  	$start_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];	
			}
			if($end_date != ''){
				$tmp = explode("/", $end_date);
			  	$end_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];	
			}
			
			
			$this->db->join('user', 'audit_log.user_trigger = user.id');
			$this->db->select('*,audit_log.created_date as audit_date');
			
			if($start_date != '' && $end_date != ''){
				$this->db->where('audit_log.created_date between \''.$start_date.' 00:00:00\' and \''.$end_date.' 23:59:59\'');
			}else if($start_date != ''){
				$this->db->where('audit_log.created_date >= \''.$start_date.' 00:00:00\'');
			}else if($end_date != ''){
				$this->db->where('audit_log.created_date <= \''.$end_date.' 00:00:00\'');
			}
			
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->where('(`name` LIKE \'%'.$like.'%\' OR `table_affect` LIKE \'%'.$like.'%\')'); 	
			}
			            
           	$this->db->order_by("audit_log.id","DESC");
            $this->db->limit($limit, $start);
            $query = $this->db->get($this->table_name);
			//echo $this->db->last_query();exit;
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
            return false;
      	}
		
		public function export_data($where=array(), $like="",$start_date='',$end_date='') {
			
			if($start_date != ''){
				$tmp = explode("/", $start_date);
			  	$start_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];	
			}
			if($end_date != ''){
				$tmp = explode("/", $end_date);
			  	$end_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];	
			}
			
			
			$this->db->join('user', 'audit_log.user_trigger = user.id');
			//$this->db->select('audit_log.id as ID,audit_log.log_no as Log_No,audit_log.ip_address as IP_Address,user.email as User_Trigger,audit_log.table_affect as Table_affect,audit_log.description as Description,audit_log.created_date as Created_Date');
			$this->db->select('audit_log.id,audit_log.log_no,audit_log.ip_address,user.email,audit_log.table_affect,audit_log.description,audit_log.created_date');
			
			if($start_date != '' && $end_date != ''){
				$this->db->where('audit_log.created_date between \''.$start_date.' 00:00:00\' and \''.$end_date.' 23:59:59\'');
			}else if($start_date != ''){
				$this->db->where('audit_log.created_date >= \''.$start_date.' 00:00:00\'');
			}else if($end_date != ''){
				$this->db->where('audit_log.created_date <= \''.$end_date.' 00:00:00\'');
			}
			
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->where('(`name` LIKE \'%'.$like.'%\' OR `table_affect` LIKE \'%'.$like.'%\')'); 	
			}
			            
           	$this->db->order_by("audit_log.id","DESC");
            $query = $this->db->get($this->table_name);
			//echo $this->db->last_query();exit;
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
            return false;
      	}
		
      
		public function insert($array)
		{
        	$this->db->insert($this->table_name, $array);
        	$insert_id = $this->db->insert_id();
        	return $insert_id;			
		}
		
		public function update($id, $array)
		{		
        	$this->db->where('id',$id);
			$this->db->update($this->table_name, $array);			
		}
		
		public function delete($id)
		{
			$data = array(
				'id' => $id,
				'is_deleted' => '1'
			);
			$this->db->where('id',$id);
			$this->db->update($this->table_name, $data);
		}	
		
		public function getIDKeyArray($column="company_name") {
		  	$id_list = array();
		  	$list = $this->getAll();
			foreach($list as $k=>$v) {
				$id_list[$v['id']] = $v[$column];
			}			
			return $id_list;					  
	  	}
		
		public function zerofill($input, $strlength=5){
			
			$query = $this->db->get_where('settings', array('id' => 13));
            $json = $query->row_array();
			$json = json_decode($json['value'],true);
			
			$prefix ='';
			$has_yr = false;
			foreach($json as $k => $v){
				if($v['table_name'] == $this->table_name){
					$prefix = $v['prefix'];
					if($v['YY']) {
						$has_yr = true;
					}
				}
			}
			if($has_yr) {
				$prefix = $prefix.date("y");
			}
			
			$total_str = strlen($input);
			$balance_space = $strlength - $total_str;
			$tmp = "";
			if($balance_space > 0) {				
				$tmp = str_repeat("0", $balance_space).$input;				
			} else {
				$tmp = $input;	
			}
			return $prefix.$tmp;						
			
		}
		
}

?>