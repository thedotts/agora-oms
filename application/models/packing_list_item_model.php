<?php
class Packing_list_item_model extends CI_Model {
	  private $table_name = "";
	
      	public function __construct() {
            $this->load->database();
            date_default_timezone_set("Asia/Taipei");
			$this->table_name = "packing_list_item";						
      	}
		
		public function insert($array){
			$this->db->insert($this->table_name, $array);
        	$insert_id = $this->db->insert_id();
        	return $insert_id;
		}
		
		public function update($id, $array)
		{		
        	$this->db->where('id',$id);
			$this->db->update($this->table_name, $array);			
		}
		
		public function getRelatedItem($id,$product_id = false) {
			
			if($product_id != false){
				$this->db->where('product_id',$product_id);
			}
			
		    $query = $this->db->get_where($this->table_name, array('packing_list_id' => $id, 'is_deleted' => 0));
            return $query->result_array();
		}
		
		public function getRelatedItem_join($id,$product_id = false) {
			
			if($product_id != false){
				$this->db->where('product_id',$product_id);
			}

			$this->db->join('product', 'product.id = packing_list_item.product_id');
		    $query = $this->db->get_where($this->table_name, array('packing_list_id' => $id, 'packing_list_item.is_deleted' => 0));
			//echo $this->db->last_query();exit;
            return $query->result_array();
			
		}
		
		public function sumRelated_packing($order_id,$product_id) {
			$this->db->select('sum(quantity_pack) as total_packed');
		    $query = $this->db->get_where($this->table_name, array('order_id' => $order_id, 'product_id' => $product_id, 'is_deleted' => 0));
            $data = $query->row_array();
			return $data['total_packed'];
		}
		
		public function delete($id)
		{
			$data = array(
				'id' => $id,
				'is_deleted' => '1'
			);
			$this->db->where('id',$id);
			$this->db->update($this->table_name, $data);
		}
		
		public function item_packed($id,$p_id){
			$this->db->select('sum(quantity_pack) as total_packed');
			$query = $this->db->get_where($this->table_name, array('order_id' => $id,'product_id' => $p_id, 'is_deleted' => 0));
            $data = $query->row_array();
			return $data['total_packed'];
		}
		
						
}
?>
