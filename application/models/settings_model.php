<?php
class Settings_model extends CI_Model{
	
	var $table_name = "";
	
	public function __construct()
	{
		$this->load->database();
        date_default_timezone_set("Asia/Taipei");
		$this->table_name = "settings";
		
	}
	
	/*
	public function record_count() {
        return $this->db->count_all($this->table_name);
    }
	*/
	
	public function get_settings($settings_id = FALSE)
	{
		if ($settings_id === FALSE)
		{
			$query = $this->db->get($this->table_name);
			return $query->result_array();
		}
		
		$query = $this->db->get_where($this->table_name, array('id' => $settings_id));
		return $query->row_array();
	}
	
	public function get_settings_ra()
	{

		$query = $this->db->get($this->table_name);
		$tmp = $query->result_array();
		
		$data = array();
		foreach($tmp as $k => $v){
			$data[$v['name']] = $v['value'];
		}
		
		return $data;

		
	}
	
	public function fetch_settings($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get($this->table_name);
 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }
	
	public function get_web_title()
	{
		$query = $this->db->get_where($this->table_name, array('id' => 1));
		if($query !== FALSE) {
			if ($query->num_rows() > 0) {
				$data = $query->row_array();		
				return $data['value'];
			} else {
				return array();	
			}
		} else {
			return array();	
		}
	}
	
	public function get_web_logo()
	{
		$query = $this->db->get_where($this->table_name, array('id' => 2));
		if($query !== FALSE) {
			if ($query->num_rows() > 0) {
				$data = $query->row_array();		
				return $data['value'];
			} else {
				return array();	
			}
		} else {
			return array();	
		}
	}
	
	public function get_where($where=array(), $like="") {
			
			$this->db->where($where);
						
			$this->db->order_by("id","DESC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
            
		}
	
	   	public function get($id=false) {
			if ($id === false) {
                  $query = $this->db->get($this->table_name);
                  return $query->result_array();
            }			
		    $query = $this->db->get_where($this->table_name, array('id' => $id));
            return $query->row_array();
		}
	
    	public function record_count($where=array(), $like="") {          
			$this->db->where($where);						
			
          	$query = $this->db->get($this->table_name);  						        
          	return $query->num_rows();
      	}	
	  
      	public function fetch($where=array(), $like="", $limit, $start) {
			$this->db->where($where);
			       
           	$this->db->order_by("id","ASC");
            $this->db->limit($limit, $start);
            $query = $this->db->get($this->table_name);
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
            return false;
      	}
      
      	public function getAll(){			
            $this->db->order_by("id","ASC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
      	}

		
		public function insert($array)
		{
        	$this->db->insert($this->table_name, $array);			
			$insert_id = $this->db->insert_id();
			//echo $this->db->last_query();
			//exit;
        	return $insert_id;			
		}
		
		public function update($id, $array)
		{		
        	$this->db->where('id',$id);
			$this->db->update($this->table_name, $array);		
			//echo $this->db->last_query();
			//exit;
			
				
		}
		
		
	
	/*
	public function get_web_footer()
	{
		$query = $this->db->get_where('settings', array('id' => 2));
		$data = $query->row_array();
		return $data['value'];
	}
	
	public function get_web_meta()
	{
		$query = $this->db->get_where('settings', array('id' => 3));
		$data = $query->row_array();
		return $data['value'];
	}
	
	public function get_web_mobile()
	{
		$query = $this->db->get_where('settings', array('id' => 4));
		$data = $query->row_array();
		return $data['value'];
	}
	
	public function get_web_address()
	{
		$query = $this->db->get_where('settings', array('id' => 5));
		$data = $query->row_array();
		return $data['value'];
	}
	
	public function get_web_email()
	{
		$query = $this->db->get_where('settings', array('id' => 6));
		$data = $query->row_array();
		return $data['value'];
	}
	
	public function get_homepage_slogan()
	{
		$query = $this->db->get_where('settings', array('id' => 8));
		$data = $query->row_array();
		return $data['value'];
	}*/
    
    public function delete($id){
        $this->db->delete('settings', array('id' => $id)); 
    }
}
?>