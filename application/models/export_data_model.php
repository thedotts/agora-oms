<?php

class Export_data_model extends CI_Model {

      public function __construct() {
            $this->load->database();
            date_default_timezone_set("Asia/Taipei");
			$this->table_name = "";	
      }
	  
	  public function table(){
		$table = array(
			'order',
			'order_item',
			'packing_list',
			'packing_list_item',
			'delivery_order',
			'delivery_order_item',
			'invoice',
			'invoice_item',
		);  
		
		return $table;
	  }
	  
	  public function getSelectedData($table_name,$start_date,$end_date) {
		  
		    $this->db->where('created_date between \''.$start_date.' 00:00:00\' and \''.$end_date.' 00:00:00\'');
			
			$query = $this->db->get($table_name);
			//echo $this->db->last_query();exit;
			return $query->result_array();
	  }

      public function getAll(){
		  	$this->db->where('is_deleted',0);
            $this->db->order_by("id","ASC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();    
      }
	  
	  public function get_where($where=array(), $like="") {
			
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('name', $like); 	
			}
			
			$this->db->order_by("id","DESC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
            
		}
	
	   	public function get($id=false) {
			if ($id === false) {
                  $query = $this->db->get($this->table_name);
                  return $query->result_array();
            }			
		    $query = $this->db->get_where($this->table_name, array('id' => $id));
            return $query->row_array();
		}
	
    	public function record_count($where=array(), $like="") {          
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('name', $like); 	
			}
			
          	$query = $this->db->get($this->table_name);          
          	return $query->num_rows();
      	}	
	  
      	public function fetch($where=array(), $like="", $limit, $start) {
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('name', $like); 
			}
			            
           	$this->db->order_by("id","DESC");
            $this->db->limit($limit, $start);
            $query = $this->db->get($this->table_name);
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
            return false;
      	}
      
		public function insert($array)
		{
        	$this->db->insert($this->table_name, $array);
        	$insert_id = $this->db->insert_id();
        	return $insert_id;			
		}
		
		public function update($id, $array)
		{		
        	$this->db->where('id',$id);
			$this->db->update($this->table_name, $array);			
		}
		
		public function delete($id)
		{
			$data = array(
				'id' => $id,
				'is_deleted' => '1'
			);
			$this->db->where('id',$id);
			$this->db->update($this->table_name, $data);
		}	
		
		public function getIDKeyArray($column="company_name") {
		  	$id_list = array();
		  	$list = $this->getAll();
			foreach($list as $k=>$v) {
				$id_list[$v['id']] = $v[$column];
			}			
			return $id_list;					  
	  	}
		
}

?>