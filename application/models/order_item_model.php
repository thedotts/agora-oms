<?php
class Order_item_model extends CI_Model {
	  private $table_name = "";
	
      	public function __construct() {
            $this->load->database();
            date_default_timezone_set("Asia/Taipei");
			$this->table_name = "order_item";						
      	}
		
		public function get($id=false) {
			if ($id === false) {
                  $query = $this->db->get($this->table_name);
                  return $query->result_array();
            }			
		    $query = $this->db->get_where($this->table_name, array('id' => $id));
            return $query->row_array();
		}
		
		public function insert($array){
			$this->db->insert($this->table_name, $array);
        	$insert_id = $this->db->insert_id();
        	return $insert_id;
		}
		
		public function update($id, $array)
		{		
        	$this->db->where('id',$id);
			$this->db->update($this->table_name, $array);			
		}
		
		public function getRelatedItem($id) {
		    $query = $this->db->get_where($this->table_name, array('order_id' => $id, 'is_deleted' => 0));
            return $query->result_array();
		}
		
		public function getRelatedItem2($id) {
			
			$this->db->join('product', 'order_item.product_id = product.id');
			$this->db->select('*,order_item.id as order_item_id');
		    $query = $this->db->get_where($this->table_name, array('order_item.order_id' => $id, 'order_item.is_deleted' => 0));
            return $query->result_array();
		}
		
		public function getRelatedItem_join($id) {
			$this->db->where('order_item.is_deleted',0);
			$this->db->where('order_id',$id);
			$this->db->from($this->table_name);
			$this->db->join('product', 'product.id = order_item.product_id');
		    $query = $this->db->get();
            return $query->result_array();
		}
		
		public function delete($id)
		{
			$data = array(
				'id' => $id,
				'is_deleted' => '1'
			);
			$this->db->where('id',$id);
			$this->db->update($this->table_name, $data);
		}
		
		public function delete_by_order_id($order_id)
		{
			$data = array(
				'is_deleted' => '1'
			);
			$this->db->where('order_id',$order_id);
			$this->db->update($this->table_name, $data);
		}
		
		
		public function count_promo_qty($product_id,$s_date,$e_date){
			
			$this->db->select('sum(quantity) as total');
			$this->db->where('product_id',$product_id);
			$this->db->where('promo',1);
			$this->db->where('`created_date` between \''.$s_date.' 00:00:00\' and \''.$e_date.' 00:00:00\'');
			$query = $this->db->get($this->table_name);
			//echo $this->db->last_query();echo "<br>";
			$tmp = $query->row_array();
			return $tmp['total'];
			
		}
		
						
}
?>
