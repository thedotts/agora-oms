<?php
class Workflow_order_model extends CI_Model {
	  private $table_name = "";
	
      	public function __construct() {
            $this->load->database();
            date_default_timezone_set("Asia/Taipei");
			$this->table_name = "workflow_order";						
      	}
		
		public function get_where($where=array(), $like="") {
			
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('invoice_no', $like); 	
				//$this->db->or_like('customer_name', $like); 	
			}
			
			$this->db->order_by("id","DESC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
            
		}
	
	   	public function get($id=false) {
			if ($id === false) {
                  $query = $this->db->get($this->table_name);
                  return $query->result_array();
            }			
		    $query = $this->db->get_where($this->table_name, array('id' => $id));
            return $query->row_array();
		}
	
    	public function record_count($where=array(), $like="") {          
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('invoice_no', $like); 	
				//$this->db->or_like('customer_name', $like); 	
			}
			
          	$query = $this->db->get($this->table_name);    
          	return $query->num_rows();
      	}	
	  
      	public function fetch($where=array(), $like="", $limit, $start) {
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('invoice_no', $like); 	
				//$this->db->or_like('customer_name', $like); 	
			}
			$this->db->where('is_deleted',0);           
           	$this->db->order_by("id","DESC");
            $this->db->limit($limit, $start);
            $query = $this->db->get($this->table_name);
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
            return false;
      	}
      
      	public function getAll(){
			$this->db->where('is_deleted',0);
            $this->db->order_by("id","ASC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
      	}

		
		public function insert($array)
		{
        	$this->db->insert($this->table_name, $array);
        	$insert_id = $this->db->insert_id();
        	return $insert_id;			
		}
		
		public function update($id, $array)
		{		
        	$this->db->where('id',$id);
			$this->db->update($this->table_name, $array);			
		}
		
		public function delete($id)
		{
			$data = array(
				'id' => $id,
				'is_deleted' => '1'
			);
			$this->db->where('id',$id);
			$this->db->update($this->table_name, $data);
		}	
		public function getIDKeyArray($column="company_name") {
		  	$id_list = array();
		  	$list = $this->getAll();
			foreach($list as $k=>$v) {
				$id_list[$v['id']] = $v[$column];
			}			
			return $id_list;					  
	  	}
						
		
		public function get_related_workflow($where,$role=false){
			
			if($role !== false){
				
				$this->db->where_in('role_id', $role);
				
			}
			$this->db->where($where);			
            $query = $this->db->get($this->table_name);
            return $query->row_array();
		
		}
		
		public function get_status_edit($workflow_id){
			
			$list = $this->db->get_where($this->table_name, array('workflow_id' => $workflow_id,'requestor_edit' => 1));
			$list = $list->result_array();
			
			$status_list = array();
			
			foreach($list as $k=>$v) {
				$status_list[] = $v['status_id'];
			}
			
			return $status_list;	
			
		}	
		
		public function get_workflow_by_id($id){
			
			$this->db->where('workflow_id',$id);
			$query = $this->db->get($this->table_name);
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
			
		}
		
		
						
}
?>
