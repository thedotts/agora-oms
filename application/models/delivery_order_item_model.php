<?php
class Delivery_order_item_model extends CI_Model {
	  private $table_name = "";
	
      	public function __construct() {
            $this->load->database();
            date_default_timezone_set("Asia/Taipei");
			$this->table_name = "delivery_order_item";						
      	}
		
		public function get($id=false) {
			if ($id === false) {
                  $query = $this->db->get($this->table_name);
                  return $query->result_array();
            }			
		    $query = $this->db->get_where($this->table_name, array('id' => $id));
            return $query->row_array();
		}
		
		public function insert($array){
			$this->db->insert($this->table_name, $array);
        	$insert_id = $this->db->insert_id();
        	return $insert_id;
		}
		
		public function update($id, $array)
		{		
        	$this->db->where('id',$id);
			$this->db->update($this->table_name, $array);			
		}
		
		public function getRelatedItem($id) {
		    $query = $this->db->get_where($this->table_name, array('do_id' => $id, 'is_deleted' => 0));
            return $query->result_array();
		}
		
		public function getRelatedItem_join($id) {
			
			$this->db->join('product','product.id = delivery_order_item.product_id');
		    $query = $this->db->get_where($this->table_name, array('do_id' => $id, 'delivery_order_item.is_deleted' => 0));
            return $query->result_array();
		}
		
		public function delete($id)
		{
			$data = array(
				'id' => $id,
				'is_deleted' => '1'
			);
			$this->db->where('id',$id);
			$this->db->update($this->table_name, $data);
		}
		
						
}
?>
