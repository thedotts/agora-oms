<?php
class Suppliers_model extends CI_Model {
	  private $table_name = "";
	
      	public function __construct() {
            $this->load->database();
            date_default_timezone_set("Asia/Taipei");
			$this->table_name = "supplier";						
      	}
		
		public function status_list($withAll=true){
			if($withAll){
				return array(				
					'1'		=> "Active",				
					'0'		=> "Suspended",
					
				);
			} else {
				return array(					
					'1'		=> "Active",					
					'0'		=> "Suspended",
					
				);
			}
		}
		
		public function get_where($where=array(), $like="") {
			
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('company_name', $like); 	
				$this->db->or_like('contact_person', $like); 	
			}
			
			$this->db->order_by("id","DESC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
            
		}
	
	   	public function get($id=false) {
			if ($id === false) {
                  $query = $this->db->get($this->table_name);
                  return $query->result_array();
            }			
		    $query = $this->db->get_where($this->table_name, array('id' => $id));
            return $query->row_array();
		}
	
    	public function record_count($where=array(), $like="") {          
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('company_name', $like); 	
				$this->db->or_like('supplier_no', $like);  	
			}
			
          	$query = $this->db->get($this->table_name);  						        
          	return $query->num_rows();
      	}	
	  
      	public function fetch($where=array(), $like="", $limit, $start) {
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('company_name', $like); 	
				$this->db->or_like('supplier_no', $like); 
			}
			            
           	$this->db->order_by("id","DESC");
            $this->db->limit($limit, $start);
            $query = $this->db->get($this->table_name);
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
            return false;
      	}
      
      	public function getAll(){
			$this->db->where('is_deleted',0);
            $this->db->order_by("id","ASC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
      	}
				
		public function insert($array)
		{
        	$this->db->insert($this->table_name, $array);			
			$insert_id = $this->db->insert_id();
			//echo $this->db->last_query();
			//exit;
        	return $insert_id;			
		}
		
		public function update($id, $array)
		{		
        	$this->db->where('id',$id);
			$this->db->update($this->table_name, $array);			
		}
		
		public function delete($id)
		{
			$data = array(
				'id' => $id,
				'is_deleted' => '1'
			);
			$this->db->where('id',$id);
			$this->db->update($this->table_name, $data);
		}	
		public function getIDKeyArray($column="company_name") {
		  	$id_list = array();
		  	$list = $this->getAll();
			foreach($list as $k=>$v) {
				$id_list[$v['id']] = $v[$column];
			}			
			return $id_list;					  
	  	}
		
		public function zerofill($input, $strlength=5){
			
			$total_str = strlen($input);
			$balance_space = $strlength - $total_str;
			$tmp = "";
			if($balance_space > 0) {				
				$tmp = str_repeat("0", $balance_space).$input;				
			} else {
				$tmp = $input;	
			}
			return $tmp;						
			
		}
		
		public function getaNexus($name) {
		  	$this->db->where('is_deleted',0);
            $this->db->where('company_name',$name);
            $query = $this->db->get($this->table_name);
            return $query->row_array();				  
	  	}
		
		public function getSupplierList($supplier_id){
			$this->db->where_in('id',$supplier_id);
			$query = $this->db->get($this->table_name);
            return $query->result_array();
		}
		
		

				
			
}
?>
