<?php
class Purchase_request_item_model extends CI_Model {
	  private $table_name = "";
	
      	public function __construct() {
            $this->load->database();
            date_default_timezone_set("Asia/Taipei");
			$this->table_name = "purchase_request_item";						
      	}

		public function insert($array)
		{
        	$this->db->insert($this->table_name, $array);
        	$insert_id = $this->db->insert_id();
        	return $insert_id;			
		}
		
		public function update($id, $array)
		{		
        	$this->db->where('id',$id);
			$this->db->update($this->table_name, $array);			
		}
		
		public function delete($id)
		{
			$data = array(
				'id' => $id,
				'is_deleted' => '1'
			);
			$this->db->where('id',$id);
			$this->db->update($this->table_name, $data);
		}
		
		public function getRelatedProduct($id) {		
		    $query = $this->db->get_where($this->table_name, array('purchase_request_id' => $id));
            return $query->result_array();
		}	
		
		public function getRelatedProduct_bySupplier($id,$supplier_id) {		
		    $query = $this->db->get_where($this->table_name, array('purchase_request_id' => $id,'supplier_id' => $supplier_id));
            return $query->result_array();
		}	
		
		public function get_supplier_groupBy($id){
			$this->db->group_by("supplier_id"); 
			$query = $this->db->get_where($this->table_name, array('purchase_request_id' => $id));
            return $query->result_array();
		}


						
}
?>
