<?php
class Order_model extends CI_Model {
	  private $table_name = "";
	
      	public function __construct() {
            $this->load->database();
            date_default_timezone_set("Asia/Taipei");
			$this->table_name = "order";						
      	}
		
		public function status_list(){
			
			$this->db->where('id',1);
            $query = $this->db->get('workflow');
            $data = $query->row_array();
			$status_json = json_decode($data['status_json'],true);
			
			$array = array();
			foreach($status_json as $k => $v){
				$array[$k] = $v;
			}
			return $array;
		}
		
		public function get_where($where=array(), $like="") {
			
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('job_id', $like); 	
				$this->db->or_like('customer_name', $like); 	
			}
			
			$this->db->order_by("id","DESC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
            
		}
	
	   	public function get($id=false) {
			if ($id === false) {
                  $query = $this->db->get($this->table_name);
                  return $query->result_array();
            }			
		    $query = $this->db->get_where($this->table_name, array('id' => $id));
            return $query->row_array();
		}
	
    	public function record_count($where=array(), $like="",$assign_customer="") {          
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->where('(`order_no` LIKE \'%'.$like.'%\' OR `customer_name` LIKE \'%'.$like.'%\')'); 	 	
			}
			
			if(!empty($assign_customer)) {
				$this->db->where_in('customer_id', $assign_customer); 	
			}
			
          	$query = $this->db->get($this->table_name);          
          	return $query->num_rows();
      	}	
	  
      	public function fetch($like="", $limit, $start,$assign_customer="",$requestor="") {
			
			$sql ="";
			if($requestor!="") {
				
				$sql .="(requestor_id = {$requestor} or customer_create = 1)"; 
				//$this->db->or_where('customer_create', 1); 	
			}
			
			
			if(!empty($like)) {
				if($requestor!="") {
					$sql .= " and ( order_no like '%{$like}%' or customer_name like '%{$like}%' )";
				}else{
					$sql .= "( order_no like '%{$like}%' or customer_name like '%{$like}%' )";
				}
				//$this->db->or_like('customer_name', $like); 	
			}
			
			if(($requestor!="" || !empty($like)) && $assign_customer != "") {
				$sql .= " and ";
			}
			
			if($assign_customer != "") {
				$assign_customer = implode(',',$assign_customer);
				$sql .= "`customer_id` IN ({$assign_customer})";
				//$this->db->where_in('customer_id', $assign_customer); 	
			}
			
			if($requestor!="" || !empty($like) || $assign_customer != "") {
				$this->db->where($sql);
			}
				
			
			$this->db->where('is_deleted',0); 
           	$this->db->order_by("id","DESC");
            $this->db->limit($limit, $start);
            $query = $this->db->get($this->table_name);
			//echo $this->db->last_query();exit;
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
            return false;
      	}
		
		public function fetch2($like="", $limit, $start,$where) {
			$this->db->where($where);
			$this->db->where('is_deleted',0);
			if(!empty($like)) {
				$this->db->where('(`order_no` LIKE \'%'.$like.'%\' OR `customer_name` LIKE \'%'.$like.'%\')'); 	 	
			}
			            
           	$this->db->order_by("id","DESC");
            $this->db->limit($limit, $start);
            $query = $this->db->get($this->table_name);
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
            return false;
      	}
      
      	public function getAll(){
			$this->db->where('is_deleted',0);
            $this->db->order_by("id","ASC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
      	}

		
		public function insert($array)
		{
        	$this->db->insert($this->table_name, $array);
        	$insert_id = $this->db->insert_id();
        	return $insert_id;			
		}
		
		public function update($id, $array)
		{		
        	$this->db->where('id',$id);
			$this->db->update($this->table_name, $array);			
		}
		
		public function delete($id)
		{
			$data = array(
				'id' => $id,
				'is_deleted' => '1'
			);
			$this->db->where('id',$id);
			$this->db->update($this->table_name, $data);
		}	
		public function getIDKeyArray($column="company_name") {
		  	$id_list = array();
		  	$list = $this->getAll();
			foreach($list as $k=>$v) {
				$id_list[$v['id']] = $v[$column];
			}			
			return $id_list;					  
	  	}
		
		public function zerofill($input, $strlength=5){
			
			$query = $this->db->get_where('settings', array('id' => 13));
            $json = $query->row_array();
			$json = json_decode($json['value'],true);
			
			$prefix ='';
			$has_yr = false;
			foreach($json as $k => $v){
				if($v['table_name'] == $this->table_name){
					$prefix = $v['prefix'];
					if($v['YY']) {
						$has_yr = true;
					}
				}
			}
			if($has_yr) {
				$prefix = $prefix.date("y");
			}
			
			$total_str = strlen($input);
			$balance_space = $strlength - $total_str;
			$tmp = "";
			if($balance_space > 0) {				
				$tmp = str_repeat("0", $balance_space).$input;				
			} else {
				$tmp = $input;	
			}
			return $prefix.$tmp;						
			
		}
		
		public function ajax_search($like="", $limit, $start, $where=array()) {
			
			$sql = "1";
			
			if(!empty($like)) {				
				$sql .= " AND (`order_no` LIKE '%".$like."%' OR `customer_name` LIKE '%".$like."%') ";								
			}
			
			if(!empty($where)){
				
				foreach($where as $k=>$v){
					$sql .= " AND (`".$k."` = '".$v."') ";
				}
				
			}
			
			$sql .= " AND (`is_deleted` = '0') ";
			
			$this->db->where($sql, NULL, false);			
           	$this->db->order_by("id","DESC");
            $this->db->limit($limit, $start);
            $query = $this->db->get($this->table_name);
						
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
            return false;
      	}
		
		public function ajax_record_count($like="", $where=array()) {          
			$sql = "1";
			
			if(!empty($like)) {				
				$sql .= " AND (`order_no` LIKE '%".$like."%' OR `customer_name` LIKE '%".$like."%') ";								
			}
			
			if(!empty($where)){
				
				foreach($where as $k=>$v){
					$sql .= " AND (`".$k."` = '".$v."') ";
				}
				
			}
			
			$sql .= " AND (`is_deleted` = '0') ";
			
			$this->db->where($sql, NULL, false);
			$this->db->where('is_deleted',0);
          	$query = $this->db->get($this->table_name);          
          	return $query->num_rows();
      	}
		
		public function order_year(){
			$this->db->select('year(order_date) as year');
			$this->db->order_by("id","DESC");
			$this->db->group_by("year(order_date)");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
		}
		
		public function filter_order($month, $year, $staff) {
			
			$quater = array('1q','2q','3q','4q');
			
			if($month != 'all'){
				if(in_array($month, $quater)){
					
					switch($month){
						case '1q':
							$this->db->where('month(created_date) between 1 and 3');
							break;	
						
						case '2q':
							$this->db->where('month(created_date) between 4 and 6');
							break;	
						
						case '3q':
							$this->db->where('month(created_date) between 7 and 9');
							break;	
							
						case '4q':
							$this->db->where('month(created_date) between 10 and 12');
							break;	
						
					}
					
					
				}else{
					$this->db->where('month(created_date)',$month);
				}
			}
			
			if($year != 'all'){
				$this->db->where('year(created_date)',$year);
			}
			
			if($staff != 'all'){
				$this->db->where('create_user_id',$staff);
			}
			
			$this->db->order_by("id","DESC");
            $query = $this->db->get($this->table_name);
			//echo $this->db->last_query();
            return $query->result_array();
			
            
		}
		
		public function filter_product($customer, $star, $end) {
			
			$this->db->select('*,sum(order_item.quantity) as total_qty');
			
			if($customer != 'all'){
				$this->db->where('order.customer_id',$customer);
			}
			
			if(!empty($star)) {
			  $tmp = explode("/", $star);
			  $start_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
			  $start_date = $start_date.' '.'00:00:00';
		  	}
			
			if(!empty($end)) {
			  $tmp = explode("/", $end);
			  $end_date = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
			  $end_date = $end_date.' '.'00:00:00';
		  	}
			
			$this->db->where('order.created_date between \''.$start_date.'\' and \''.$end_date.'\'');
			$this->db->group_by('order_item.product_id');
			$this->db->from('order_item');
			$this->db->join('order', 'order.id = order_item.order_id');
			
			$query = $this->db->get();
			//echo $this->db->last_query();
            return $query->result_array();
		
		}
		
		public function getId_bySerial($serial){
			
			$this->db->where('order_no',$serial);
			$query = $this->db->get($this->table_name);
			$data = $query->row_array();
			//print_r($data);exit;
			return $data['id'];
			
		}
		
		public function get_order_customer() {
			$this->db->group_by('customer_id');	
			$this->db->where('status',2);	
		    $query = $this->db->get($this->table_name);
            return $query->result_array();
	   }
						
}
?>
