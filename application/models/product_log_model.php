<?php

class Product_log_model extends CI_Model {

      public function __construct() {
            $this->load->database();
            date_default_timezone_set("Asia/Taipei");
			$this->table_name = "product_log";	
      }

      public function getAll(){
		  	$this->db->where('is_deleted',0);
            $this->db->order_by("id","ASC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();    
      }
	  
	  public function get_where($where=array(), $like="") {
			
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('name', $like); 	
			}
			
			$this->db->order_by("id","DESC");
            $query = $this->db->get($this->table_name);
            return $query->result_array();
            
		}
	
	   	public function get($id=false) {
			if ($id === false) {
                  $query = $this->db->get($this->table_name);
                  return $query->result_array();
            }			
		    $query = $this->db->get_where($this->table_name, array('id' => $id));
            return $query->row_array();
		}
	
    	public function record_count($where=array(), $like="") {          
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('name', $like); 	
			}
			
          	$query = $this->db->get($this->table_name);          
          	return $query->num_rows();
      	}	
	  
      	public function fetch($where=array(), $like="", $limit, $start) {
			$this->db->where($where);
			
			if(!empty($like)) {
				$this->db->like('name', $like); 
			}
			            
           	$this->db->order_by("id","DESC");
            $this->db->limit($limit, $start);
            $query = $this->db->get($this->table_name);
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
            return false;
      	}
      
		public function insert($array)
		{
        	$this->db->insert($this->table_name, $array);
        	$insert_id = $this->db->insert_id();
        	return $insert_id;			
		}
		
		public function update($id, $array)
		{		
        	$this->db->where('id',$id);
			$this->db->update($this->table_name, $array);			
		}
		
		public function delete($id)
		{
			$data = array(
				'id' => $id,
				'is_deleted' => '1'
			);
			$this->db->where('id',$id);
			$this->db->update($this->table_name, $data);
		}	
		
		public function getIDKeyArray($column="company_name") {
		  	$id_list = array();
		  	$list = $this->getAll();
			foreach($list as $k=>$v) {
				$id_list[$v['id']] = $v[$column];
			}			
			return $id_list;					  
	  	}
		
		public function getList(){
		  	$this->db->where('is_deleted',0);
            $this->db->order_by("id","ASC");
            $query = $this->db->get($this->table_name);
            $tmp = $query->result_array();  
			
			$array = array();
			foreach($tmp as $k => $v){
				$array[$v['id']] = $v['name'];
			}  
			return $array;
      }
	  
	  public function sum_by_id($id){
		  $this->db->select('sum(qty) as total');
		  $this->db->where('product_id',$id);
		  $query = $this->db->get($this->table_name);
		  $tmp = $query->row_array();
		  return $tmp['total'];
	  }
	  
	  public function get_all_limit($id=false) {
		  	$this->db->order_by("id","DESC");
			$this->db->limit(100);
		    $query = $this->db->get_where($this->table_name, array('product_id' => $id));
            return $query->result_array();
	  }
	  
	  public function get_by_date_range($id,$start_date,$end_date) {
		  	
			$this->db->where('Date(created_date) BETWEEN \''.$start_date.'\' AND \''.$end_date.'\'');
			$this->db->where('product_id',$id);
			$this->db->where('is_deleted',0);
			$this->db->order_by("id","DESC");
		    $query = $this->db->get($this->table_name);
            return $query->result_array();
			
	  }
		
}

?>