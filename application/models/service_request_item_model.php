<?php
class Service_request_item_model extends CI_Model {
	  private $table_name = "";
	
      	public function __construct() {
            $this->load->database();
            date_default_timezone_set("Asia/Taipei");
			$this->table_name = "service_request_item";						
      	}

		public function insert($array)
		{
        	$this->db->insert($this->table_name, $array);
        	$insert_id = $this->db->insert_id();
        	return $insert_id;			
		}
		
		public function update($id, $array)
		{		
        	$this->db->where('id',$id);
			$this->db->update($this->table_name, $array);			
		}
		
		public function delete($id)
		{
			$data = array(
				'id' => $id,
				'is_deleted' => '1'
			);
			$this->db->where('id',$id);
			$this->db->update($this->table_name, $data);
		}
		
		public function getRelatedService($id) {		
		    $query = $this->db->get_where($this->table_name, array('service_request_id' => $id,'is_deleted'=>0));			
			if($query->num_rows() > 0) {
            	return $query->result_array();
			} else {
				return array();	
			}
		}	


						
}
?>
