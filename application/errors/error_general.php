
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/bootstrap-responsive.min.css')?>" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="<?=base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet">        
    
    <link href="<?=base_url('assets/css/ui-lightness/jquery-ui-1.10.0.custom.min.css')?>" rel="stylesheet">
    
    <link href="<?=base_url('assets/css/base-admin-3.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/base-admin-3-responsive.css')?>" rel="stylesheet">
    
    <link href="<?=base_url('assets/css/pages/dashboard.css')?>" rel="stylesheet">   

    <link href="<?=base_url('assets/css/custom.css')?>" rel="stylesheet">
    
    <link href="<?=  base_url('assets/js/plugins/msgbox/jquery.msgbox.css')?>" rel="stylesheet">
    
    <link rel="shortcut icon" href="<?=base_url('assets/img/favicon.ico')?>" type="image/x-icon">
	<link rel="icon" href="<?=base_url('assets/img/favicon.ico')?>" type="image/x-icon">

    <script src="<?=base_url('assets/js/libs/jquery-1.9.1.min.js')?>"></script>

</head>

<body>
	
<nav class="navbar navbar-inverse" role="navigation">

	<div class="container">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      <span class="sr-only">Toggle navigation</span>
      <i class="icon-cog"></i>
    </button>
      <a class="navbar-brand" href="<?=  base_url()?>">Anexus</a>
  </div>

</div> <!-- /.container -->
</nav>


<div class="container">
	
	<div class="row">
		
		<div class="col-md-12">
			
			<div class="error-container">
				<h2><?php echo $heading; ?></h2>
                <?php echo $message; ?>
					
				</div> <!-- /error-actions -->
							
			</div> <!-- /error-container -->			
			
		</div> <!-- /span12 -->
		
	</div> <!-- /row -->
	
</div> <!-- /container -->



<
    
    
<div class="footer" style="margin-top:100px;">
		
	<div class="container">
		
		<div class="row">
			
			<div id="footer-copyright" class="col-md-6">
				&copy; 2014
			</div> <!-- /span6 -->
			
			<div id="footer-terms" class="col-md-6">
				
			</div> <!-- /.span6 -->
			
		</div> <!-- /row -->
		
	</div> <!-- /container -->
	
</div> <!-- /footer -->



    

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?=base_url('assets/js/libs/jquery-ui-1.10.0.custom.min.js')?>"></script>
<script src="<?=base_url('assets/js/libs/bootstrap.min.js')?>"></script>
<script src="<?=base_url('assets/js/Application.js')?>"></script>


  </body>
</html>
