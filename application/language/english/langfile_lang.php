<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['page_title'] = 'HaPlayTour.com';
$lang['language_type'] = 'English';
$lang['welcome_to'] = 'Welcome to Chlconcepts';

//header
$lang['my_profile'] = 'My Profile';
$lang['logout'] = 'Logout';

//header menu
$lang['home'] = 'Home';
$lang['users'] = 'Users';
$lang['user_add'] = 'Add a user';
$lang['user_list'] = 'User list';
$lang['products'] = 'Room';
$lang['product_add'] = 'Add a room';
$lang['product_manage'] = 'Manage rooms';
$lang['category_add'] = 'Add a zone';
$lang['category_manage'] = 'Manage zones';
$lang['order'] = 'Order';
$lang['take_order'] = 'Add Order';
$lang['order_manage'] = 'Order Management';
$lang['marketing'] = 'Marketing Channel';
$lang['marketing_add'] = 'Add Marketing Channel';
$lang['marketing_list'] = 'Marketing Channel List';

$lang['cashflow'] = 'Cashflow Channel';
$lang['cashflow_add'] = 'Add Cashflow Channel';
$lang['cashflow_list'] = 'Cashflow Channel List';
$lang['cashflow_stats'] = 'Cashflow Channel Statistics';

$lang['financial'] = 'Financial';
$lang['financial_add'] = 'Add new financial record';
$lang['financial_list'] = 'Financial Record List & Report';
$lang['financial_sales_stats'] = 'Financial Sales statisitics';

$lang['settings'] = "Settings";
$lang['web_settings'] = "Web Settings";
$lang['mail'] = "Mail Content Settings";
$lang['calendar'] = "Calendar";

//login
$lang['sign_in'] = 'Sign In';
$lang['email'] = 'E-mail';
$lang['password'] = 'Password';
$lang['re_password'] = 'Re-password';
$lang['default_password'] = 'default password';
$lang['keep_sign_in'] = 'Keep me signed in';
$lang['forget_password'] = 'Forget Password';
$lang['sign_in_use_register'] = 'Sign in using your registered account';

//login error
$lang['invalid_login'] = 'Invalid email or password';

//index page
$lang['shortcut_key'] = 'Shortcut Key';
$lang['import'] = 'Import';
$lang['export'] = 'Export';
$lang['stock_list'] = 'Stock list';
$lang['yours'] = 'Your';
$lang['salesman_sub'] = 'dedicated Salesman';

//users
$lang['user'] = 'User';
$lang['all'] = 'All';
$lang['superadmin'] = 'SuperAdmin';
$lang['admin'] = 'Admin';
$lang['salesman'] = 'Salesman';
$lang['customer'] = 'Customer';
$lang['account_not_exist'] = 'This account is not exist.';
$lang['activated'] = 'Activated';
$lang['not_activated'] = 'Not activate';
$lang['search_by_email'] = "please search by user's email";
$lang['level'] = 'Level';
$lang['subordination'] = 'Subordination';
$lang['name'] = 'Name';
$lang['first_name'] = 'Firstname';
$lang['last_name'] = 'Lastname';
$lang['gender'] = 'Gender';
$lang['male'] = 'Male';
$lang['female'] = 'Female';
$lang['tel'] = 'Tel';
$lang['status'] = 'Status';
$lang['email_inuse'] = 'Email already in use!';
$lang['user_type'] = 'User type';

//category
$lang['category'] = 'Zone';
$lang['all_category'] = 'All zone';
$lang['category_type'] = 'Zone type';
$lang['category_list'] = 'Zone List';
$lang['category_title'] = 'Zone title';
$lang['sub_category'] = 'Sub zone';
$lang['no_category'] = 'No zone';
$lang['category_not_exist'] = 'Zone not exist';
$lang['no_sub_category'] = 'No sub zone';

//product
$lang['products'] = 'Rooms';
$lang['product_list'] = 'room list';
$lang['product_title'] = 'Room title';
$lang['serial'] = 'Serial';
$lang['price'] = 'Price';
$lang['details'] = 'Details';
$lang['main_picture'] = 'Main Picture';
$lang['recommended_size'] = 'Recommended size';
$lang['picture'] = 'Picture';
$lang['postdate'] = 'Postdate';
$lang['multi_upload'] = 'Can be upload mutiple image, and recommended size';

//warehouse
$lang['warehouse'] = 'Warehouse';
$lang['warehouse_type'] = 'Warehouse Type';
$lang['warehouse_list'] = 'Warehouse list';
$lang['warehouse_title'] = 'Warehouse title';
$lang['sub_warehouse'] = 'Sub warehouse';
$lang['no_sub_warehouse'] = 'No sub warehouse';
$lang['no_warehouse'] = 'No warehouse';
$lang['warehouse_not_exist'] = 'Warehouse is not exist.';
$lang['address'] = 'Address';
$lang['stock_available'] = 'Stock Available';
$lang['stock_unavailable'] = 'Stock Unavailable';
$lang['stock'] = 'Stock';

//cart
$lang['checkout'] = 'Checkout';
$lang['add_cart'] = 'Add cart';
$lang['my_cart'] = 'My shopping cart';

//order
$lang['order_list'] = 'Order list';
$lang['user_order'] = 'User order';
$lang['transaction_details'] = 'Transaction details';
$lang['transaction_info'] = 'Transaction info';
$lang['product_info'] = 'Product info';
$lang['checkout_date'] = 'Checkout Date';
$lang['qty'] = 'Qty';
$lang['sub_price'] = 'Sub price';
$lang['total_price'] = 'Total Price';
$lang['waiting_for_confirm'] = 'Waiting for confirm';
$lang['confirm'] = 'Confirm';
$lang['update_successed'] = 'Update successed';
$lang['update_failed'] = 'Update failed, Qty is not enough.';
$lang['can_not_zero'] = 'Qty can not be zero.';
$lang['minus_successed'] = 'Minus stock successed.';
$lang['add_successed'] = 'Add stock successed.';
$lang['change_stock'] = 'Please change your stock.';
$lang['status_change'] = 'Transaction status has be change.';
$lang['marketing_account'] = 'Marketing account';
$lang['room_detail'] = 'Room detail';
$lang['nationality'] = 'Nationality';
$lang['checkin_date'] = 'Checkin date';
$lang['checkout_date'] = 'Checkout_date';
$lang['order_amount'] = 'Order amount';
$lang['finish_payment'] = 'Finish payment';
$lang['is_deleted'] = '[to change] is_deleted';
$lang['send_mail'] = '[to change] send_mail';
$lang['view'] = 'View';
$lang['order_log'] = 'Logs of Modification';
$lang['is_checked'] = 'is_checked';
$lang['is_replied'] = 'is_replied';

//order status
$lang['canceled'] = 'Canceled';
$lang['unpaid'] = 'Unpaid';
$lang['paid'] = 'Paid';
$lang['ready_for_checkin'] = 'Ready for check in';
$lang['has_checkedout'] = 'Already checked out';

//marketing
$lang['marketing_type'] = '[to change] Marketing_type';
$lang['marketing_type_a'] = 'Automatically';
$lang['marketing_type_s'] = 'Semi-automatically';
$lang['marketing_type_m'] = 'Mnaually';
$lang['marketing_title'] = 'Marketing channel title';
$lang['marketing_title_en'] = 'Marketing channel title';

//cashflow
$lang['cashflow_title'] = '[to change] Cashflow_channel_title';
$lang['cashflow_title_en'] = '[to change] Cashflow_channel_title_en';

//mail_setting
$lang['mail_settings_type'] = '[to change] mail_setting_type';
$lang['mail_settings_content'] = '[to change] mail_setting_content';
$lang['mail_settings_list'] = '[to change] mail_setting_list';
$lang['mail_settings_add'] = '[to change] mail_setting_add';

//stock
$lang['warehouse_stock_list'] = 'Warehouse Stock List';
$lang['stock_template'] = 'Stock Template List';
$lang['template_type'] = 'Template type';
$lang['template_name'] = 'Template name';
$lang['created_date'] = 'Created date';
$lang['use_template'] = 'Use this template';
$lang['import_product_empty'] = 'Your import product is empty';
$lang['template_name_blank'] = 'Template name can not be blank.';
$lang['warehouse_choice'] = 'Please choice a warehouse.';
$lang['stock_insert_error'] = 'Stock insert error.';
$lang['export_product_empty'] = 'Your export product is empty';
$lang['title'] = 'Title';
$lang['image'] = 'Image';
$lang['sure_to_add'] = 'Are you sure that you want to add the stock?';
$lang['warehouse_import_stock'] = 'Warehouse Import Stock';
$lang['warehouse_export_stock'] = 'Warehouse Export Stock';
$lang['product_import_to_warehouse'] = 'The products below will be imported into the Warehouse';
$lang['product_export_to_warehouse'] = 'The products below will be exported from the Warehouse';
$lang['save_template_for'] = 'Save this Import Template for future usage';
$lang['product_in_list'] = 'Product is already in list.';
$lang['import_info'] = 'Import info';
$lang['export_info'] = 'Export info';
$lang['destination_info'] = 'Destination info';
$lang['import_cant_blank'] = 'Import product can not be blank.';
$lang['export_cant_blank'] = 'Export product can not be blank.';
$lang['where_export_to'] = 'Where is this stock export to?';
$lang['destination'] = 'Destination?';
$lang['product_stock_not_enough'] = 'The product stock is not enough.';
$lang['select_a_destination'] = 'Please select a destination';

//web setting
$lang['setting_variable'] = '[to change] web setting variable';
$lang['setting_value'] = '[to change] web setting value';

//log
$lang['warehouse_export'] = 'Warehouse Export';
$lang['warehouse_import'] = 'Warehouse Import/Take';
$lang['action'] = 'Action';
$lang['action_details'] = 'Action details';
$lang['action_date'] = 'Action date';
$lang['print_pdf'] = 'Print PDF';

//financial
$lang['financial_type'] = 'Financial type';
$lang['financial_type_outcome'] = 'outcome';
$lang['financial_type_income'] = 'income';
$lang['financial_category'] = 'Financial categories';
$lang['financial_remarks'] = 'Financial remarks';
$lang['financial_amount'] = 'Financial amount';
$lang['financial_marketing_channel'] = 'Marketing channel';
$lang['financial_balance_amount'] = 'Financial balance amount';
$lang['financial_summary'] = 'Financial summary';
$lang['financial_summary_amount'] = 'Financial summary amount';
$lang['financial_summary_today'] = 'Financial summary today';
$lang['financial_summary_lastday'] = 'Financial summary last day';
$lang['financial_cash_income_summary_today'] = 'cash income summary';
$lang['financial_cash_outcome_summary_today'] = 'cash outcome summary';
$lang['financial_clean_summary_today'] = 'clean up summary';
$lang['financial_cash_breakfast_summary_today'] = 'breakfast summary';
$lang['financial_cash_bankdeposit_today'] = 'Bankdeposit';
$lang['financial_cash_deposit_summary_today'] = 'Order_deposit';
$lang['financial_performance_summary_today'] = 'Performance today';
$lang['financial_checkin_num_today'] = 'check-in number today';
$lang['financial_report_today'] = 'Daily report';

//pdf print
$lang['export_by'] = 'Exported by';
$lang['export_to'] = 'Exported to';
$lang['approved_by'] = 'Approved by';

//operation
$lang['operation'] = 'Operation';
$lang['list_empty'] = 'list is empty.';
$lang['please_select'] = '--Please select--';
$lang['search'] = 'Search';
$lang['keywords_search'] = 'Input keywords for instant search';
$lang['save'] = 'Save';
$lang['close'] = 'Close';
$lang['delete'] = 'Delete';
$lang['delete_confirm'] = 'Are you sure that you want to permanently delete the selected element?';
$lang['create'] = 'create';
$lang['edit'] = 'Edit';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';
$lang['grid_view'] = 'Grid View';
$lang['list_view'] = 'List View';

//password
$lang['reset_new_password'] = 'Reset my password';
$lang['input_new_password'] = 'Please input your new password';
$lang['new_password'] = 'New Password';
$lang['confirm_new_password'] = 'Confirm New Password';
$lang['retrieve_password'] = 'Retrieve my password';
$lang['input_email'] = 'Please input your email to change your password';
$lang['send_link_password'] = 'Send me the link to Modify my password';
$lang['invalid_email'] = 'Invalid E-mail';
$lang['modify_password'] = 'Modify my Password';
$lang['password_not_same'] = 'Password and Confirm Password is not same';
$lang['reset_successed'] = 'Password Reset successfully';
$lang['login_use_new_password'] = 'Your password has been reset successfully, please login by using new password';

//mail content
$lang['password_modify'] = 'Password Modification';
$lang['hi'] = 'Hi';
$lang['modify_main_content_1'] = "You'd received this email because you have trigger the password modification of CHLConcepts.com.";
$lang['modify_main_content_2'] = 'You may ignore this email if this is not triggered by you.';
$lang['click_to_modify_password'] = 'Click the link below to modify your password';
$lang['click_to_modify'] = 'Click To Modify';
$lang['thank'] = 'Thanks!';
$lang['administrator'] = 'Administrator';
$lang['retrieve_successed'] = 'Password Retrieve successfully';
$lang['modify_main_content_3'] = 'We have sent a password modification link to your email. Please click it to modify your password';
$lang['login_to_account'] = 'Log in to my Account';

//calendar
$lang['click_to_showhide'] = 'Click to show/hide';

$lang['frontend_informPaid_bankacc'] = "Please input the translocation bank and the last five digits of your bank account";
$lang['frontend_informPaid_method'] = "Please input translocation method";
$lang['frontend_informPaid_amount'] = "Please input how much do you translocate";
$lang['frontend_informPaid_time'] = "Please input when do you translocate";

/* End of file messages_lang.php */
/* Location: ./application/language/english/messages_lang.php */