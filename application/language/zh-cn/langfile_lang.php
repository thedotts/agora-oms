<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['page_title'] = '哈旅行';
$lang['language_type'] = '繁體中文';
$lang['welcome_to'] = '歡迎來到 哈旅行';

//header
$lang['my_profile'] = '我的帳號';
$lang['logout'] = '登出';

//header menu
$lang['home'] = '首頁';
$lang['users'] = '使用者';
$lang['user_add'] = '新增使用者';
$lang['user_list'] = '使用者列表';
$lang['products'] = '商品';
$lang['product_add'] = '新增房型';
$lang['product_manage'] = '管理房型';
$lang['category_add'] = '新增館別';
$lang['category_manage'] = '管理館別';
$lang['order'] = '訂單';
$lang['take_order'] = '填單';
$lang['order_manage'] = '訂單管理';
$lang['marketing'] = '行銷管道';
$lang['marketing_add'] = '新增行銷管道';
$lang['marketing_list'] = '行銷管道列表';

$lang['cashflow'] = '現金流';
$lang['cashflow_add'] = '新增現金流管道';
$lang['cashflow_list'] = '現金流管道列表';
$lang['cashflow_stats'] = '現金流統計';

$lang['financial'] = '帳務';
$lang['financial_add'] = '新增帳務';
$lang['financial_list'] = '帳務列表與報表';
$lang['financial_sales_stats'] = '砍單統計';

$lang['mail_settings'] = "設定";
$lang['settings'] = "設定";
$lang['web_settings'] = "網頁參數設定";
$lang['mail'] = "信件內容設定";
$lang['calendar'] = "行事曆";

//login
$lang['sign_in'] = '登入';
$lang['email'] = '電子郵件';
$lang['password'] = '密碼';
$lang['re_password'] = '確認密碼';
$lang['default_password'] = '預設密碼';
$lang['keep_sign_in'] = '維持登入狀態';
$lang['forget_password'] = '忘記密碼';
$lang['sign_in_use_register'] = '請用您的註冊帳號登入';

//login error
$lang['invalid_login'] = '電子郵件或密碼錯誤';

//index page
$lang['shortcut_key'] = '快速鍵';
$lang['import'] = '匯入';
$lang['export'] = '匯出';
$lang['stock_list'] = '庫存列表';
$lang['yours'] = '您的';
$lang['salesman_sub'] = '專屬銷售員';

//users
$lang['user'] = '使用者';
$lang['all'] = '所有';
$lang['superadmin'] = '超級管理員';
$lang['admin'] = '管理員';
$lang['salesman'] = '推銷員';
$lang['customer'] = '客戶';
$lang['account_not_exist'] = '此帳號並不存在';
$lang['activated'] = '啟動';
$lang['not_activated'] = '未啟動';
$lang['search_by_email'] = "以電子郵件搜尋";
$lang['level'] = '級別';
$lang['subordination'] = '附屬';
$lang['name'] = '姓名';
$lang['first_name'] = '姓氏';
$lang['last_name'] = '名';
$lang['gender'] = '性別';
$lang['male'] = '男';
$lang['female'] = '女';
$lang['tel'] = '聯絡號碼';
$lang['status'] = '狀態';
$lang['email_inuse'] = '電子郵件已經被使用!';
$lang['user_type'] = '使用者級別';

//category
$lang['category'] = '館別';
$lang['all_category'] = '所有館別';
$lang['category_type'] = '類型';
$lang['category_list'] = '館別列表';
$lang['category_title'] = '館別標題';
$lang['sub_category'] = '子類別';
$lang['no_category'] = '無類別';
$lang['category_not_exist'] = '類別不存在';
$lang['no_sub_category'] = '無子類別';

//product
$lang['products'] = '房型';
$lang['product_list'] = '房型列表';
$lang['product_title'] = '房型標題';
$lang['serial'] = '房間流水號';
$lang['price'] = '價格';
$lang['details'] = '詳細';
$lang['main_picture'] = '主圖';
$lang['recommended_size'] = '建議大小';
$lang['picture'] = '房型圖';
$lang['postdate'] = '上傳時間';
$lang['multi_upload'] = '一次可上傳多張圖片,建議大小';

//warehouse
$lang['warehouse'] = '倉庫';
$lang['warehouse_type'] = '倉庫類型';
$lang['warehouse_list'] = '倉庫列表';
$lang['warehouse_title'] = '倉庫名稱';
$lang['sub_warehouse'] = '附屬倉庫';
$lang['no_sub_warehouse'] = '無子倉庫';
$lang['no_warehouse'] = '無倉庫';
$lang['warehouse_not_exist'] = '此倉庫並不存在。';
$lang['address'] = '地址';
$lang['stock_available'] = '尚有庫存';
$lang['stock_unavailable'] = '暫無庫存';
$lang['stock'] = '庫存';

//cart
$lang['checkout'] = '結算';
$lang['add_cart'] = '加入購物車';
$lang['my_cart'] = '我的購物車';

//order
$lang['order_list'] = '訂單清單';
$lang['user_order'] = '使用者訂單';
$lang['transaction_details'] = '訂單詳細';
$lang['transaction_info'] = '訂單資訊';
$lang['product_info'] = '商品資訊';
$lang['checkout_date'] = '退房日期';
$lang['qty'] = '數量';
$lang['sub_price'] = '小計';
$lang['total_price'] = '總價';
$lang['waiting_for_confirm'] = '等待確認';
$lang['confirm'] = '確認';
$lang['update_successed'] = '更新成功';
$lang['update_failed'] = '更新失敗, 庫存不足。';
$lang['can_not_zero'] = '數量不能為零';
$lang['minus_successed'] = '成功減少數量。';
$lang['add_successed'] = '成功增加數量。';
$lang['change_stock'] = '數量尚未改變。';
$lang['status_change'] = '成功更改訂單狀態。';
$lang['marketing_channel'] = '請問是透過哪邊訂房呢？';
$lang['marketing_account'] = '承上，請填寫來源的帳號';
$lang['room_detail'] = '訂房細節';
$lang['nationality'] = '國籍';
$lang['checkin_date'] = '入住日期';
$lang['order_amount'] = '訂單總金額';
$lang['finish_payment'] = '通知已付款';
$lang['balance_amount'] = '餘款總數';
$lang['deposit'] = '匯款金額';
$lang['bankin_detail'] = '匯款資料';
$lang['bankin_method'] = '匯款方式';
$lang['is_checked'] = '已對帳';
$lang['checked_remarks'] = '對帳備註';
$lang['is_replied'] = '已回覆';
$lang['is_deleted'] = '已刪除';
$lang['send_mail'] = '發信';
$lang['view'] = '檢視';
$lang['order_log'] = '使用者修改紀錄';

//order status
$lang['canceled'] = '已取消';
$lang['unpaid'] = '未付款';
$lang['paid'] = '已付款';
$lang['ready_for_checkin'] = '準備入住';
$lang['has_checkedout'] = '已退房';

//marketing
$lang['marketing_type'] = '行銷管道類別';
$lang['marketing_type_a'] = '自動';
$lang['marketing_type_s'] = '半自動';
$lang['marketing_type_m'] = '手動';
$lang['marketing_title'] = '行銷管道標題';
$lang['marketing_title_en'] = 'Marketing channel title';

//cashflow
$lang['cashflow_title'] = '現金流管道標題';
$lang['cashflow_title_en'] = 'Cashflow channel title';

//mail_setting
$lang['mail_settings_type'] = '信件內容類別';
$lang['mail_settings_content'] = '信件內容設定';
$lang['mail_settings_list'] = '信件內容列表';
$lang['mail_settings_add'] = '新增信件內容';

//web setting
$lang['setting_variable'] = '網頁設定變數';
$lang['setting_value'] = '網頁設定數值';

//financial
$lang['financial_type'] = '帳務種類';
$lang['financial_type_outcome'] = '支出';
$lang['financial_type_income'] = '收入';
$lang['financial_category'] = '帳務分類';
$lang['financial_remarks'] = '帳務明細';
$lang['financial_amount'] = '收入/支出款項';
$lang['financial_marketing_channel'] = '行銷管道';
$lang['financial_balance_amount'] = '入住收入現金餘款';
$lang['financial_summary'] = '帳務總結';
$lang['financial_summary_amount'] = '帳務總結金額';
$lang['financial_summary_today'] = '當日帳務總結';
$lang['financial_summary_lastday'] = '昨日帳務總結';
$lang['financial_cash_income_summary_today'] = '當日現金收入';
$lang['financial_cash_outcome_summary_today'] = '當日現金開銷';
$lang['financial_clean_summary_today'] = '當日外派打掃';
$lang['financial_cash_breakfast_summary_today'] = '當日早餐';
$lang['financial_cash_bankdeposit_today'] = '當日現金投庫';
$lang['financial_cash_deposit_summary_today'] = '當日訂金';
$lang['financial_performance_summary_today'] = '當日業績總結';
$lang['financial_checkin_num_today'] = '當日總間數';
$lang['financial_report_today'] = '當日業績報表';

$lang['financial_report_marketing_auto_platform'] = '自動下單平台';
$lang['financial_report_marketing_semiauto_platform'] = '半自動下單平台';
$lang['financial_report_marketing_manual_platform'] = '手動下單平台';
$lang['financial_report_performance'] = '業績';
$lang['financial_report_performance_ratio'] = '業績比例';
$lang['financial_report_product_num'] = '間數';
$lang['financial_report_checkin_ratio'] = '住宿比例';
$lang['financial_report_full_checkin_ratio'] = '滿房住宿比例';
$lang['financial_report_total'] = '總計';
$lang['financial_order_source'] = '訂單來源';

//stock
$lang['warehouse_stock_list'] = '倉庫庫存列表';
$lang['stock_template'] = '庫存模版列表';
$lang['template_type'] = '模版類型';
$lang['template_name'] = '模版名稱';

$lang['created_date'] = '新增日期';
$lang['use_template'] = '使用此模版';
$lang['import_product_empty'] = '匯入商品不能為空';
$lang['template_name_blank'] = '模版名稱不能為空';
$lang['warehouse_choice'] = '請先選擇倉庫';
$lang['stock_insert_error'] = '庫存錯誤操作';
$lang['export_product_empty'] = '匯出商品不能為空';
$lang['title'] = '標題';
$lang['image'] = '圖片';
$lang['sure_to_add'] = '確定要增加庫存嗎?';
$lang['warehouse_import_stock'] = '倉庫匯入庫存';
$lang['warehouse_export_stock'] = '倉庫匯出庫存';
$lang['product_import_to_warehouse'] = '以下產品將被匯入到倉庫';
$lang['product_export_to_warehouse'] = '以下產品將被匯出到倉庫';
$lang['save_template_for'] = '儲存此範本用於以後使用';
$lang['product_in_list'] = '此商品已經存在於列表。';
$lang['import_info'] = '匯入資訊';
$lang['export_info'] = '匯出資訊';
$lang['destination_info'] = '目的地資訊';
$lang['import_cant_blank'] = '匯入商品不能為空';
$lang['export_cant_blank'] = '匯出商品不能為空';
$lang['where_export_to'] = '匯出的倉庫';
$lang['destination'] = '目的地?';
$lang['product_stock_not_enough'] = '商品庫存不足';
$lang['select_a_destination'] = '請選擇目的地';

//log
$lang['warehouse_export'] = '匯出倉庫';
$lang['warehouse_import'] = '匯入倉庫／提貨';
$lang['action'] = '操作';
$lang['action_details'] = '操作詳細';
$lang['action_date'] = '操作日期';
$lang['print_pdf'] = '列印 PDF';

//pdf print
$lang['export_by'] = '匯出者';
$lang['export_to'] = '匯出到';
$lang['approved_by'] = '審批者';

//operation
$lang['operation'] = '操作';
$lang['list_empty'] = '暫無任何資料';
$lang['please_select'] = '--請選擇--';
$lang['search'] = '搜尋';
$lang['keywords_search'] = '輸入關鍵字搜索';
$lang['save'] = '儲存';
$lang['close'] = '關閉';
$lang['delete'] = '刪除';
$lang['delete_confirm'] = '確定想要永久刪除所選的資料?';
$lang['create'] = '新增';
$lang['edit'] = '編輯';
$lang['yes'] = '是';
$lang['no'] = '否';
$lang['grid_view'] = '格狀顯示';
$lang['list_view'] = '清單顯示';

//password
$lang['reset_new_password'] = '重置密碼';
$lang['input_new_password'] = '請輸入新的密碼';
$lang['new_password'] = '新密碼';
$lang['confirm_new_password'] = '確認密碼';
$lang['retrieve_password'] = '修改我的密碼';
$lang['input_email'] = '輸入電子郵件以更新密碼';
$lang['send_link_password'] = '更新密碼連結';
$lang['invalid_email'] = '電子郵件無效';
$lang['modify_password'] = '修改我的密碼';
$lang['password_not_same'] = '密碼與確認密碼不符';
$lang['reset_successed'] = '密碼更新成功';
$lang['login_use_new_password'] = '您的密碼已經更新成功，請用新密碼進行登入';

//mail content
$lang['password_modify'] = '修改密碼';
$lang['hi'] = '嗨';
$lang['modify_main_content_1'] = "因為您已觸發密碼修改按鍵，所以收到了此電子郵件從 CHLConcepts.com.";
$lang['modify_main_content_2'] = '如果此信件不是由您所觸發，您可以忽略此電子郵件.';
$lang['click_to_modify_password'] = '點擊下面連結修改您的密碼';
$lang['click_to_modify'] = '點擊修改';
$lang['thank'] = '感謝您!';
$lang['administrator'] = '管理員';
$lang['retrieve_successed'] = '密碼修改成功';
$lang['modify_main_content_3'] = '系統已經寄發修改密碼的連結到您的電子郵件。 請點擊該連結修改您的密碼';
$lang['login_to_account'] = '登入我的帳號';

//calendar
$lang['click_to_showhide'] = '點擊顯示/隱藏';

//frontend, confirm translocation time & amount
$lang['frontend_informPaid_bankin_detail'] = "請填寫匯款銀行名稱與帳戶後五碼或臨櫃匯款姓名，匯款時間";
$lang['frontend_informPaid_method'] = "請填寫付款方式";
$lang['frontend_informPaid_amount'] = "請填寫匯入金額";

/* End of file messages_lang.php */
/* Location: ./application/language/english/messages_lang.php */
